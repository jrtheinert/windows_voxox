﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Feb 2016
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;

namespace SaslUtilityManaged
{
	//	https://tools.ietf.org/html/rfc5802

	//Sample message exchange
	//	C: n,,n=user,r=fyko+d2lbbFgONRv9qkxdawL
	//	S: r=fyko+d2lbbFgONRv9qkxdawL3rfcNHYJY1ZVvWVs7j,s=QSXCR+Q6sek8bf92,i=4096
	//	C: c=biws,r=fyko+d2lbbFgONRv9qkxdawL3rfcNHYJY1ZVvWVs7j,p=v0X8v3Bz2T0CJGbJQyF0X+HI4Ts=
	//	S: v=rmF9pqV8S7suAoZWja4dJRkFsKQ=

	//List of attributes in above messages
	//	n - username
	//	r - nonce, cnonce or both concatenated
	//	c - GS2 header and data binding
	//	s - SALT
	//	i - iteration count
	//	p - Client Proof
	//	v - Server Signature
	//
	//Other attributes per RFC-5802
	//	a - Optional and part of GS2 (RFC-5801) bridge between GSS-API and SASL
	//	m - Reserved for future extensibilty
	//	e - Error (from server on final message)


	public class ScramSha1Base
	{
		protected String mUserName				= String.Empty;
		protected String mPassword				= String.Empty;

		protected int	 mNonceByteLength		=    32;
		protected int	 mMinIterationCount		=  4000;
		protected int	 mMaxIterationCount		=  5000;

		protected const int    mShaOutputLength	=    20;	// this is the length of the output of SHA-1 HMAC
		protected const String GS2_Prefix		= "n,,";	//TODO: This is just one example.  Much more complexity is involved.

		private RNGCryptoServiceProvider mRandomCryptProvider = null;
		private Random					 mRandomGen			  = null;

		//Internal constants
		private const String pairDelimiterS = ",";
		private const String pairSepS       = "=";
		private const Char	 pairDelimiter  = ',';
		private const Char	 pairSep        = '=';

		//Key values
		protected const String mKeyUserName			= "n";
		protected const String mKeyNonce			= "r";
		protected const String mKeyGS2Hdr			= "c";
		protected const String mKeySalt				= "s";
		protected const String mKeyIteration		= "i";
		protected const String mKeyClientProof		= "p";
		protected const String mKeyServerSignature	= "v";
		protected const String mKeyGS2Bridge		= "a";
		protected const String mKeyReserved			= "m";
		protected const String mKeyError			= "e";

		//---------------------------------------------------------------
		//	See https://gist.github.com/markokr/4654875 for test vectors
		//---------------------------------------------------------------
		//Test vectors
		protected const String mTestUserName			= "user";
		protected const String mTestPassword			= "pencil";
		protected const String mTestCNonce				= "fyko+d2lbbFgONRv9qkxdawL";

		protected const String mTestServerNonce			= "3rfcNHYJY1ZVvWVs7j";
		protected const String mTestServerSalt			= "QSXCR+Q6sek8bf92";
		protected const int	   mTestServerIteration		= 4096;

		//Intermediate results for test vector
		protected const String mTestClientFirstMsg		= "n,,n=user,r=fyko+d2lbbFgONRv9qkxdawL";
		protected const String mTestServerFirstMsg		= "r=fyko+d2lbbFgONRv9qkxdawL3rfcNHYJY1ZVvWVs7j,s=QSXCR+Q6sek8bf92,i=4096";
		protected const String mTestClientFinalMsgBare	= "c=biws,r=fyko+d2lbbFgONRv9qkxdawL3rfcNHYJY1ZVvWVs7j";
		protected const String mTestAuthMsg				= "n=user,r=fyko+d2lbbFgONRv9qkxdawL,r=fyko+d2lbbFgONRv9qkxdawL3rfcNHYJY1ZVvWVs7j,s=QSXCR+Q6sek8bf92,i=4096,c=biws,r=fyko+d2lbbFgONRv9qkxdawL3rfcNHYJY1ZVvWVs7j";
		protected const String mTestClientFinalMsg		= "c=biws,r=fyko+d2lbbFgONRv9qkxdawL3rfcNHYJY1ZVvWVs7j,p=v0X8v3Bz2T0CJGbJQyF0X+HI4Ts=";
		protected const String mTestServerFinalMsg		= "v=rmF9pqV8S7suAoZWja4dJRkFsKQ=";

		//Hex equivalents of byte[] members.  Use bytesToString() method.
		protected const String mTestSalt				= "4125c247e43ab1e93c6dff76";					//Hex
		protected const String mTestSaltedPassword		= "1d96ee3a529b5a5f9e47c01f229a2cb8a6e15f7d";	//Hex
		protected const String mTestClientKey			= "e234c47bf6c36696dd6d852b99aaa2ba26555728";	//Hex
		protected const String mTestStoredKey			= "e9d94660c39d65c38fbad91c358f14da0eef2bd6";	//Hex
		protected const	String mTestClientSignature		= "5d7138c486b0bfabdf49e3e2da8bd6e5c79db613";	//Hex
		protected const String mTestClientProof			= "bf45fcbf7073d93d022466c94321745fe1c8e13b";	//Hex
		protected const String mTestServerKey			= "0fe09258b3ac852ba502cc62ba903eaacdbf7d31";	//Hex
		protected const String mTestServerSignature		= "ae617da6a57c4bbb2e0286568dae1d251905b0a4";	//Hex
		//End test vectors

		protected static bool sIsTest = false;


		protected bool inTestMode()
		{
			return ( sIsTest && mUserName.Equals(mTestUserName) && mPassword.Equals(mTestPassword) );
		}

		public static void UseTestVectors( bool value )
		{
			sIsTest = value;
		}

		protected byte[] ComputeHMACHash(byte[] data, String key)
		{
			using (var _hmac_sha_1 = new HMACSHA1(data, true))
			{
				byte[] _hash_bytes = _hmac_sha_1.ComputeHash(Encoding.UTF8.GetBytes(key));
				return _hash_bytes;
			}
		}

		protected byte[] getSha1Hash( byte[] data )
		{
			SHA1 sha1 = SHA1.Create();
			byte[] result =	sha1.ComputeHash( data );
			return result;
		}

		protected byte[] Hi(String password, byte[] salt, int iteration_count)
		{
			Rfc2898DeriveBytes _pdb = new Rfc2898DeriveBytes(password, salt, iteration_count);

			return _pdb.GetBytes( mShaOutputLength );
		}

		protected String GetNonce()
		{
			return Convert.ToBase64String( GetRandomByteArray( mNonceByteLength ) );
		}

		protected byte[] GetRandomByteArray(int byte_array_length)
		{
			byte[] _random_byte_array = new byte[byte_array_length];

			if ( mRandomCryptProvider == null)
				mRandomCryptProvider = new RNGCryptoServiceProvider();

			mRandomCryptProvider.GetBytes(_random_byte_array);

			return _random_byte_array;
		}

		protected int GetRandomInteger()
		{
			if ( mRandomGen == null)
				mRandomGen = new Random();

			int randomInt = mRandomGen.Next( mMinIterationCount, mMaxIterationCount );

			if ( randomInt < 0 )
				randomInt *= -1;

			return randomInt;
		}

		protected String DecodeFrom64( String stringIn )
		{
			String result = String.Empty;

			if ( !String.IsNullOrEmpty( stringIn ) )
			{ 
				byte[] bytesDecoded = Convert.FromBase64String( stringIn );

				result = UTF8Encoding.UTF8.GetString( bytesDecoded );
			}

			return result;
		}

		protected String EncodeTo64( String stringIn )
		{
			String result = String.Empty;

			if ( !String.IsNullOrEmpty( stringIn ) )
			{ 
				byte[] bytesEncoded = UTF8Encoding.UTF8.GetBytes( stringIn );

				result = Convert.ToBase64String( bytesEncoded );
			}

			return result;
		}

		protected byte[] GetClientProof(byte[] buffer_a, byte[] buffer_b)
		{
			if (buffer_a == null || buffer_b == null || buffer_a.Length < 1 || buffer_b.Length < 1)
				return null;

			byte[] buffer = new byte[buffer_a.Length];

			for (int i = 0; i < buffer_a.Length; ++i)
			{ 
				buffer[i] = (byte)(buffer_a[i] ^ buffer_b[i]);
			}

			return buffer;
		}

		protected Dictionary<String, String> splitMessage( String text )
		{
			Dictionary<String, String> result = new Dictionary<String, String>();

			String[] kvps = text.Split( pairDelimiter );

			foreach ( String kvp in kvps )
			{
				//We cannot just split on '=' since that *may* be in the actual 'value'.
				//	So let's just find the first occurrence and substring.
				int	   pos   = kvp.IndexOf( pairSep );
				String key   = String.Empty;
				String value = String.Empty;

				if ( pos > 0 )
				{
					key   = kvp.Substring( 0, pos );
					value = kvp.Substring( pos+1 );
				}
				else 
				{ 
					key   = kvp;
					value = String.Empty;
				}

				result.Add( key, value );
			}

			return result;
		}

		protected String makeMessageKvp( String key, String value, bool quoteValue, bool isLast )
		{
			String result = String.Empty;

			if ( !String.IsNullOrEmpty( value ) )
			{
				String quote = quoteValue ? "\"" : "";
				String delim = isLast ? "" : pairDelimiterS;
			
				result = key + pairSep + quote + value + quote + delim;
			}

			return result;
		}
		
		protected String formatKvps( Dictionary<String,String> kvps )
		{
			String result = String.Empty;

			var keys = kvps.Keys;

			foreach ( String key in keys )
			{
				String value  = String.Empty;
				bool   gotten = kvps.TryGetValue( key, out value );
				String delim  = (String.IsNullOrEmpty( result ) ? "" : pairDelimiterS);	//This is a LEADING delim, so logic appears reversed.

				if ( gotten )
				{
					result +=  delim + key + pairSepS + value;
				}
			}

			return result;
		}

		//For comparing test vectors for byte[] members.
		private String bytesToHex( byte[] data )
		{
			String result = BitConverter.ToString( data );
			result = result.Replace( "-", "" );
			result = result.ToLower();

			return result;
		}

		protected bool verifyTestVector( String name, byte[] data, String testValue )
		{
			bool result = true;

			if ( inTestMode() )
			{ 
				String temp = bytesToHex( data );

				result = temp.Equals( testValue );

				if ( !result )
				{
					throw new Exception( "Test Vector does not match for: " + name );
				}
			}

			return result;
		}

	}	//class ScramSha1Base

	//=========================================================================


	//=========================================================================

	//RFC 5802
	public class ScramSha1Client : ScramSha1Base
	{
		private String mClientFirstMessageBare		= String.Empty;
		private String mClientFirstMessage			= String.Empty;
		private String mClientFinalMessage			= String.Empty;
		private String mClientFinalMessageNoProof	= String.Empty;

		private String mServerFirstMessage			= String.Empty;
		private String mServerFinalMessage			= String.Empty;
		private String mServerIteration				= String.Empty;
		private String mServerSalt					= String.Empty;

		private String mClientNonce					= String.Empty;
		private String mClientServerNonce			= String.Empty;
		private String mReceivedServerSignature64	= String.Empty;
		private String mAuthenticationMessage		= String.Empty;
		private String mComputedServerSignature64	= String.Empty;

		private byte[] mClientKey					= null;
		private byte[] mClientSignature				= null;
		private byte[] mClientProof					= null;
		private byte[] mSaltedPassword				= null;
		private byte[] mStoredKey					= null;
		private byte[] mServerKey					= null;
		private byte[] mServerSignature				= null;

		private int mServerIteration_i				=	  0;
		private int mMaxAllowableIterationCount		= 10000;

        public ScramSha1Client()
		{
		}

        public ScramSha1Client( String userName, String password) : this( userName, password, 32 )
        {
        }

        public ScramSha1Client( String userName, String password, int nonceByteLength )
        {
			if ( String.IsNullOrEmpty( userName )  )
				throw new ArgumentException("User name cannot be null/empty ");

			if ( String.IsNullOrEmpty( password ) )
				throw new ArgumentException("Password cannot be null/empty ");

			if ( nonceByteLength < 1)
				throw new ArgumentException("The nonce byte length cannot be less than 1 ");

			mUserName		 = userName;
			mPassword		 = password;
			mNonceByteLength = nonceByteLength;
			mClientNonce	 = GetNonce();

			//-----------------------------------------------------------------
			//Tester values source: https://gist.github.com/markokr/4654875            
			//-----------------------------------------------------------------
			if ( inTestMode() )
			{
				mClientNonce = mTestCNonce;
			}

			mClientFirstMessageBare = makeMessageKvp( mKeyUserName, mUserName,    false, false ) +
									  makeMessageKvp( mKeyNonce,    mClientNonce, false, true );

			//Example: n,,n=user,r=fyko+d2lbbFgONRv9qkxdawL
			mClientFirstMessage		= GS2_Prefix + mClientFirstMessageBare;
        }

		public String getFirstMessage()
		{
			return mClientFirstMessage;
		}

		//Not tested
		public void SetMaximumIterationCount( int maxAllowableIterationCount )
		{
			mMaxAllowableIterationCount = maxAllowableIterationCount;
		}

		public String GetClientFinalMessage( String serverFirstMessage )
		{
			String result = String.Empty;

			if ( !String.IsNullOrEmpty( serverFirstMessage ) )
			{
				try
				{
					mServerFirstMessage = serverFirstMessage;

					if ( SplitServerFirstMessage( mServerFirstMessage ))
					{
						if ( serverIterationIsValid() )
						{ 
							String normalizedPassword = mPassword.Normalize( NormalizationForm.FormKC );

							mSaltedPassword				= Hi( normalizedPassword, Convert.FromBase64String( mServerSalt ), mServerIteration_i );
							mClientKey					= ComputeHMACHash( mSaltedPassword, "Client Key");
							mStoredKey					= getSha1Hash( mClientKey );
							
							verifyTestVector( "SaltedPassword", mSaltedPassword, mTestSaltedPassword );
							verifyTestVector( "ClientKey",      mClientKey,	     mTestClientKey      );
							verifyTestVector( "StoredKey",      mStoredKey,      mTestStoredKey      );

							mClientFinalMessageNoProof	= makeMessageKvp( mKeyGS2Hdr, EncodeTo64( GS2_Prefix ), false, false ) +
														  makeMessageKvp( mKeyNonce,  mClientServerNonce,		false, true  );

							mAuthenticationMessage		= mClientFirstMessageBare + "," + mServerFirstMessage + "," + mClientFinalMessageNoProof;
							mClientSignature			= ComputeHMACHash( mStoredKey, mAuthenticationMessage );
							mClientProof				= GetClientProof( mClientKey, mClientSignature );

							verifyTestVector( "ClientSignature", mClientSignature, mTestClientSignature );
							verifyTestVector( "ClientProof",     mClientProof,	   mTestClientProof     );

							mClientFinalMessage			= mClientFinalMessageNoProof + "," + 
														  makeMessageKvp( mKeyClientProof, Convert.ToBase64String( mClientProof ), false, true );

							//Data is good, but just some final verifications.
							mServerKey					= ComputeHMACHash( mSaltedPassword, "Server Key");
							mServerSignature			= ComputeHMACHash( mServerKey,  mAuthenticationMessage );
							mComputedServerSignature64	= Convert.ToBase64String( mServerSignature );

							verifyTestVector( "ServerKey",       mServerKey,	   mTestServerKey       );
							verifyTestVector( "ServerSignature", mServerSignature, mTestServerSignature );

							result = mClientFinalMessage;
						}
					}
				}
				catch
				{
					mClientFinalMessage = String.Empty;
				}
			}

			return result;
		}

		private bool serverIterationIsValid()
		{
			bool result = ( sIsTest ? true : (mServerIteration_i <= mMaxAllowableIterationCount ) );

			return result;
		}

		private bool SplitServerFirstMessage( String serverFirstMessage)
		{
			bool result = false;

			// r=fyko+d2lbbFgONRv9qkxdawL3rfcNHYJY1ZVvWVs7j,s=QSXCR+Q6sek8bf92,i=4096
			if ( !String.IsNullOrEmpty( serverFirstMessage ) )
			{ 
				Dictionary<String,String> kvps = splitMessage( serverFirstMessage );

				if ( kvps.Count >= 3 )
				{
					bool gotNonce	  = kvps.TryGetValue( mKeyNonce,	 out mClientServerNonce );
					bool gotSalt	  = kvps.TryGetValue( mKeySalt,		 out mServerSalt		   );
					bool gotIteration = kvps.TryGetValue( mKeyIteration, out mServerIteration   );

					if ( gotIteration )
						mServerIteration_i = Convert.ToInt32( mServerIteration );


					result = !String.IsNullOrEmpty( mClientServerNonce ) &&
							 !String.IsNullOrEmpty( mServerSalt        ) &&
							 !String.IsNullOrEmpty( mServerIteration   );

				}
			}

			return result;
		}

		private bool SplitServerFinalMessage( String serverFinalMessage )
		{
			bool result = false;

			if ( ! String.IsNullOrEmpty( serverFinalMessage ) )
			{ 
				//	v=rmF9pqV8S7suAoZWja4dJRkFsKQ=
				Dictionary<String,String> kvps = splitMessage( serverFinalMessage );

				if ( kvps.Count >= 1 )
				{
					bool gotSignature = kvps.TryGetValue( mKeyServerSignature, out mReceivedServerSignature64 );

					result = gotSignature;
				}
			}

			return result;
		}

		//Used by UnitTest
        public bool VerifyServerSignature( String serverFinalMessage )
        {
            bool result = false;

            if ( !String.IsNullOrEmpty( serverFinalMessage ) )
			{ 
				try
				{
					mServerFinalMessage = serverFinalMessage;

					if ( SplitServerFinalMessage(mServerFinalMessage) )
						result = mReceivedServerSignature64.Equals( mComputedServerSignature64 );
				}
				catch
				{
				}
			}

			return result;
		}

	}

	//=========================================================================

	
	//=========================================================================

	public class ScramSha1Server : ScramSha1Base
	{
		private String mServerFirstMessage		= String.Empty;
        private String mServerFinalMessage		= String.Empty;

		private String mClientFirstMessage		= String.Empty;
		private String mClientFinalMessage		= String.Empty;
		private String mClientFinalMessageNoProof = String.Empty;

		private String mClientNonce				= String.Empty;
        private String mServerNonce				= String.Empty;
		
		private String mClientServerNonce		= String.Empty;
		private String mServerSalt				= String.Empty;
		private String mServerIteration			= String.Empty;

        private String mAuthenticationMessage	= String.Empty;
        private String mClientProof64			= String.Empty;

        private byte[] mServerSaltByte			= null;
		private byte[] mSaltedPassword			= null;
		private byte[] mClientKey				= null;
		private byte[] mServerKey				= null;
		private byte[] mStoredKey				= null;
		private byte[] mClientSignature			= null;
		private byte[] mClientProof				= null;
		private byte[] mServerSignature			= null;

		private int mServerIteration_i			=	  0;
		private int mSaltByteLength				=   320;

		public ScramSha1Server( String clientFirstMessage ) : this( clientFirstMessage, 32, 320 )
		{
		}

		public ScramSha1Server( String clientFirstMessage, int nonceByteLength, int saltByteLength )
		{
			if ( String.IsNullOrEmpty( clientFirstMessage ) )
				throw new ArgumentException("ScramSha1: Client first message String cannot be null/empty ");

			if ( nonceByteLength < 1 )
				throw new ArgumentException("ScramSha1: The nonce byte length cannot be less than 1 ");

			if ( saltByteLength < 1 )
				throw new ArgumentException("ScramSha1: The salt byte length cannot be less than 1 ");

			mNonceByteLength = nonceByteLength;
			mSaltByteLength  = saltByteLength;

			mClientFirstMessage = clientFirstMessage;

			if ( !SplitClientFirstMessage( mClientFirstMessage ) )
				throw new ArgumentException("ScramSha1: Client first message not properly formatted ");
		}

		//Not tested
		public void SetIterationCountLimits( int minIterationCount, int maxIterationCount )
		{
			if ( minIterationCount < 1 )
				throw new ArgumentException("The value of the min iteration count cannot be less than 1");

			if (maxIterationCount < 1 )
				throw new ArgumentException("The value of the max iteration count cannot be less than 1");

			if (minIterationCount > maxIterationCount )
				throw new ArgumentException("The value of the min iteration count cannot be greater than that of the max iteration count");

			mMinIterationCount = minIterationCount;
			mMaxIterationCount = maxIterationCount;
		}

		private bool SplitClientFirstMessage( String clientFirstMessage )
		{
			bool result = false;

			if ( !String.IsNullOrEmpty(clientFirstMessage) )
			{ 
				//Example: n,,n=user,r=fyko+d2lbbFgONRv9qkxdawL
				//If client first message does not BEGIN with n, p or y, then it must fail. See Section 5 of the RFC.
				String firstChars = "npy";

				if ( firstChars.Contains( clientFirstMessage.Substring( 0, 1 ) ) )
				{
					String					   cleanMessage = clientFirstMessage.Replace( GS2_Prefix, "" );		//Remove prefix.
					Dictionary<String, String> kvps         = splitMessage( cleanMessage );

					if ( kvps.Count >= 2 )
					{
						bool gotUserName = kvps.TryGetValue( mKeyUserName, out mUserName    );
						bool gotCNonce   = kvps.TryGetValue( mKeyNonce,    out mClientNonce );

						result = (gotUserName && gotCNonce);
					}
				}
			}

			return result;
		}

		public String GetServerFirstMessage( String password )
		{
			if ( !String.IsNullOrEmpty( mServerFirstMessage) )
				return mServerFirstMessage;

			if ( String.IsNullOrEmpty( mClientNonce ) )
				throw new ArgumentException("GetServerFirstMessage: The received client nounce cannot be null/empty");

			if ( String.IsNullOrEmpty( password ) )
				throw new ArgumentException("GetServerFirstMessage: Password cannot be null/empty ");

			mPassword = password;

			try
			{
				mServerNonce		= GetNonce();
				mClientServerNonce	= mClientNonce + mServerNonce;
				
				mServerSaltByte		= GetRandomByteArray( mSaltByteLength );		//TODO: This appears wrong when compared to the test vectors below.  But don't really need server right now, if ever.
				mServerSalt			= Convert.ToBase64String( mServerSaltByte );
		
				mServerIteration_i	= GetRandomInteger();
				mServerIteration	= mServerIteration_i.ToString();

				//------------------------------------------------------------------
				// Tester values source: https://gist.github.com/markokr/4654875            
				//------------------------------------------------------------------
				if ( inTestMode() )
				{
					mServerNonce		= mTestServerNonce;
					mClientServerNonce	= mClientNonce + mServerNonce;

					mServerSalt			= mTestServerSalt;
					mServerSaltByte		= Convert.FromBase64String( mServerSalt );
					
					mServerIteration_i	= mTestServerIteration;
					mServerIteration	= mServerIteration_i.ToString();
				}

				String normalizedPassword = mPassword.Normalize( NormalizationForm.FormKC) ;

				mSaltedPassword = Hi( normalizedPassword, mServerSaltByte, mServerIteration_i );
				mClientKey		= ComputeHMACHash( mSaltedPassword, "Client Key");
				mServerKey		= ComputeHMACHash( mSaltedPassword, "Server Key");
				mStoredKey		= getSha1Hash( mClientKey );

				verifyTestVector( "SaltedPassword", mSaltedPassword, mTestSaltedPassword );
				verifyTestVector( "ClientKey",      mClientKey,      mTestClientKey      );
				verifyTestVector( "ServerKey",      mServerKey,      mTestServerKey      );
				verifyTestVector( "StoredKey",      mStoredKey,      mTestStoredKey      );

				// e.g., r=fyko+d2lbbFgONRv9qkxdawL3rfcNHYJY1ZVvWVs7j,s=QSXCR+Q6sek8bf92,i=4096
				mServerFirstMessage = makeMessageKvp( mKeyNonce,	 mClientServerNonce, false, false ) +
									  makeMessageKvp( mKeySalt,		 mServerSalt,		 false, false ) +
									  makeMessageKvp( mKeyIteration, mServerIteration,   false, true  );
			}
			catch
			{
				mServerFirstMessage = String.Empty;
			}

			return mServerFirstMessage;
		}

		public String GetServerFinalMessage( String clientFinalMessage, ref bool isClientAuthenticated )
		{
			String result = String.Empty;

			isClientAuthenticated = false;

			if ( !String.IsNullOrEmpty( clientFinalMessage ) )
			{ 
				mClientFinalMessage = clientFinalMessage;

				if ( SplitClientFinalMessage(clientFinalMessage) )
				{ 
					try
					{
						String clientFirstMessageBare = makeMessageKvp( mKeyUserName, mUserName,  false, false ) + 
											  		    makeMessageKvp( mKeyNonce,	mClientNonce, false, true  );

						verifyTestVector( "StoredKey", mStoredKey, mTestStoredKey );
						verifyTestVector( "ClientKey", mClientKey, mTestClientKey );

						mAuthenticationMessage  = clientFirstMessageBare + "," + mServerFirstMessage + "," + mClientFinalMessageNoProof;
						mClientSignature		= ComputeHMACHash( mStoredKey, mAuthenticationMessage );
						mClientProof			= GetClientProof ( mClientKey, mClientSignature       );
						
						verifyTestVector( "ClientSignature", mClientSignature, mTestClientSignature );
						verifyTestVector( "ClinetProof",     mClientProof,     mTestClientProof     );

						String receivedClientProof = Convert.ToBase64String( mClientProof );

						if ( mClientProof64.Equals( receivedClientProof ) )
						{
							isClientAuthenticated = true;

							mServerSignature    = ComputeHMACHash( mServerKey, mAuthenticationMessage );
							mServerFinalMessage = makeMessageKvp( mKeyServerSignature, Convert.ToBase64String( mServerSignature ), false, true );

							verifyTestVector( "ServerSignature", mServerSignature, mTestServerSignature );

							result = mServerFinalMessage;
						}
					}
					catch
					{
						mServerFinalMessage = String.Empty;
					}
				}
			}

			return result;
		}

		private bool SplitClientFinalMessage( String clientFinalMessage )
		{
			bool result = false;

			if ( !String.IsNullOrEmpty( clientFinalMessage ) )
			{ 
				//	c=biws,r=fyko+d2lbbFgONRv9qkxdawL3rfcNHYJY1ZVvWVs7j,p=v0X8v3Bz2T0CJGbJQyF0X+HI4Ts=
				Dictionary<String,String> kvps = splitMessage( clientFinalMessage );

				if ( kvps.Count >= 3 )
				{
					String GS2Hdr    = String.Empty;
					String tempNonce = String.Empty;

					bool   hasGS2Hdr = kvps.TryGetValue( mKeyGS2Hdr,	  out GS2Hdr		 );
					bool   hasNonce  = kvps.TryGetValue( mKeyNonce,		  out tempNonce		 );
					bool   hasProof  = kvps.TryGetValue( mKeyClientProof, out mClientProof64 );

					if ( GS2Hdr.Equals( "biws" ) )	//This is GS2Header as UTF8 and Based64Encoded.	//TODO
					{
						//Rebuild withouth clientProof 'p='.  Do NOT assume parameters will be in any particular order.
						var temp = kvps.Remove( mKeyClientProof );
						mClientFinalMessageNoProof = formatKvps( kvps );

						result = hasGS2Hdr && hasNonce && hasProof;
					}
				}
			}

			return result;
		}
	
	}

	//=========================================================================


	//=========================================================================

	public class ScramSha1Test : ScramSha1Base
	{
		private ScramSha1Client mClient = null;
		private ScramSha1Server mServer = null;

		public ScramSha1Test()
		{
			ScramSha1Client.UseTestVectors( true );
			mClient = new ScramSha1Client( mTestUserName, mTestPassword );
		}

		public void RunTest()
		{
			bool   finalResult = false;
			String clientMsg1  = mClient.getFirstMessage();

			if ( clientMsg1.Equals( mTestClientFirstMsg ) )
			{
				mServer = new ScramSha1Server( clientMsg1 );

				String serverMsg1 = mServer.GetServerFirstMessage( mTestPassword );	//TODO: Why does this need password?

				if ( serverMsg1.Equals( mTestServerFirstMsg ) )
				{
					String clientMsg2 = mClient.GetClientFinalMessage( serverMsg1 );

					if ( clientMsg2.Equals( mTestClientFinalMsg ) )
					{
						bool isAuthed = false;
						String serverMsg2 = mServer.GetServerFinalMessage( clientMsg2, ref isAuthed );

						if ( serverMsg2.Equals( mTestServerFinalMsg ) )
						{
							finalResult = mClient.VerifyServerSignature( serverMsg2 );
						}
					}
				}
			}
		}

	}

}	//namespace
