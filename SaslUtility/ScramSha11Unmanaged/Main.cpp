
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Feb 2016
 */

//A totally unmanaged application

#include "stdafx.h"
#include "../ManagedDll/IScramSha1Client.h"
#include <string>

#include <Windows.h>	//MessageBox

int main(int argc, char **argv)
{
	IScramSha1Client* wrapper = IScramSha1Client::CreateInstance();

//	wrapper->setUseTestVector( true );
	wrapper->initialize( "user", "pencil" );

	std::string msg1 = wrapper->getClientFirstMessage();

	if ( msg1.empty() )
		msg1 = "<empty>";

	::MessageBoxA( nullptr, msg1.c_str(), "UnmanagedApp", MB_OK );

	IScramSha1Client::Destroy( wrapper );

    return (0);
}
