﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Feb 2016
 */

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using SaslUtilityManaged;

namespace SaslUtilityManaged.Test
{
	[TestClass]
	public class ManagedUnitTest1
	{
		[TestMethod]
		public void TestScramSha1()
		{
			ScramSha1Test test = new ScramSha1Test();

			test.RunTest();
		}
	}
}
