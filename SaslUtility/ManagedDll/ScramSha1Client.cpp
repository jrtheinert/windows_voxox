
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Feb 2016
 */

#include "stdafx.h"
#include "ScramSha1Client.h"
#include "Conversion.h"

#include <Windows.h>		//OutputDebugString

ScramSha1Client::ScramSha1Client()
{
}

void ScramSha1Client::setUseTestVector( bool value )
{
//	mClient->UseTestVectors( value );	//TDDO: We do not really need this for production.  Managed code should address most of this.
}

void ScramSha1Client::initialize( const std::string userNameIn, const std::string passwordIn )
{
	String^ userName = Conversion::U2M_String( userNameIn );
	String^ password = Conversion::U2M_String( passwordIn );

	std::string msg = "U: username: " + userNameIn + ", password: " + passwordIn + "\n";
	OutputDebugStringA( msg.c_str() );

	mClient = gcnew SaslUtilityManaged::ScramSha1Client( userName, password );
}

std::string ScramSha1Client::getClientFirstMessage()
{
	String^     temp   = mClient->getFirstMessage();
	std::string result = Conversion::M2U_String( temp );
	
	return result;
}

std::string ScramSha1Client::getClientFinalMessage( const std::string serverFirstMsgIn )
{
	String^     msg    = Conversion::U2M_String( serverFirstMsgIn );
	String^     temp   = mClient->GetClientFinalMessage( msg );
	std::string result = Conversion::M2U_String( temp );
	
	return result;
}

bool ScramSha1Client::verifyServerSignature( const std::string serverFinalMsgIn )
{
	String^     msg      = Conversion::U2M_String( serverFinalMsgIn );
	bool		result   = mClient->VerifyServerSignature( msg );
	
	return result;
}

//-----------------------------------------------------------------------------
//Interface methods
//-----------------------------------------------------------------------------
IScramSha1Client* IScramSha1Client::CreateInstance()
{
	ScramSha1Client* result = new ScramSha1Client();

	return result;
}

void IScramSha1Client::Destroy( IScramSha1Client* instance )
{
	delete instance;
}
