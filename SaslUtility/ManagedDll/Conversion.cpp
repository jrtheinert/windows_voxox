
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Feb 2016
 */

#include "stdafx.h"
#include "Conversion.h"

using namespace System;
using namespace System::Text;	//UTF8Encoding

Conversion::Conversion()
{
}

std::string Conversion::M2U_String(String^ textIn)
{
	using namespace System::Runtime::InteropServices;

	std::string result;

	if ( textIn != nullptr )
	{
		array<Byte>^  encodedBytes;
		pin_ptr<Byte> pinnedBytes;

		try
		{
			// Encode the text as UTF8 which SQLite supports
			encodedBytes = Encoding::UTF8->GetBytes(textIn);		//No BOM in this encoding

			// prevent GC moving the bytes around while this variable is on the stack
			if ( encodedBytes->Length > 0 )
			{
				pinnedBytes = &encodedBytes[0];

				result = reinterpret_cast<char*>(pinnedBytes);
			}
		}
		catch( ... )
		{
			int xxx = 1;
		}
	}

	return result;
}

//static
String^ Conversion::U2M_String( const std::string& textIn )
{
	using namespace System::Runtime::InteropServices;

	String^ result = gcnew String( "" );

	if ( textIn.size() > 0 )
	{
		array<Byte>^ bytes1 = gcnew array<Byte>( textIn.size() );
		
		//convert native pointer to System::IntPtr with C-Style cast
		Marshal::Copy((IntPtr)(char*)textIn.c_str(), bytes1, 0, textIn.size() );

		result = Encoding::UTF8->GetString( bytes1 );
	}

	return result;
}
