
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Feb 2016
 */

#pragma once

#include <string>

using System::Byte;
using System::String;

ref class Conversion
{
public:
	static std::string	M2U_String( String^            textIn );
	static String^		U2M_String( const std::string& textIn );

private:
	Conversion();
};

