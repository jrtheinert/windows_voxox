
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Feb 2016
 */

#include "DllExport.h"
#include "IScramSha1Client.h"

#include <string>
#include <msclr/gcroot.h>

//using namespace SaslUtilityManaged; - Using this causes ambiguities in compile since we have similar names classes.

class ScramSha1Client : public IScramSha1Client
{
public:
	DLLEXPORT ScramSha1Client();
	DLLEXPORT ~ScramSha1Client();

	DLLEXPORT void		setUseTestVector( bool value );
	DLLEXPORT void		initialize( const std::string userNameIn, const std::string passwordIn );

	DLLEXPORT std::string getClientFirstMessage();
	DLLEXPORT std::string getClientFinalMessage( const std::string serverFirstMsgIn );
	DLLEXPORT bool        verifyServerSignature( const std::string serverFinalMsgIn );

private:
	msclr::gcroot<SaslUtilityManaged::ScramSha1Client^> mClient;
};
