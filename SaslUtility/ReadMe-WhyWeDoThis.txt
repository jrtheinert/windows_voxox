
J.R. Theinert - 2016.02.22

Why do we have this project?

Our XMPP stack (LibJingle) does not support any SASL Challenge/Response 
authentication mechanisms, such as DIGEST-MD5 or SCRAM-SHA-1.

A few weeks back IT (Jabber server team) and management decided that
PLAIN text authorization was not secure enough so we had to implement
one of the Challenge/Response mechanisms.

Initial efforts to implement DIGEST-MD5 with LibJingle failed.  Code looked
good, but our jabber servers did not respond as expected.  Further research
on DIGEST-MD5 indicated that it is old, difficult to implement (due to too
many 'allowable' options), and was obsoleted in 2011.

So we shifted to SCRAM-SHA-1, a newer auth mechanism.  Implementing this in
native C++ within the LibJingle lib would have required major addtions to that
code base.  Alternatively, implementing SCRAM-SHA-1 in C#/.Net was relatively
easy.  The main issue was wrapping the managed class(es) to be accessible
from native C++.  That has been done.

Since this SCRAM-SHA-1 code works, we should NOT have to make any changes
going forward, but it is here if we need to.

A couple notes:
 - Because we use std::string, we need to maintain both Release and Debug
   Dlls.  If we do not, then we get crashes due to mismatched CRT libs
   related to the std::string implementations.

If you need to rebuild this project, here is how to deploy it to other
projects:

- From SaslUtility project:
  - For Debug and Release: (necessary because we use std::string)
    - Copy <configuration>/SaslUtilityManaged.dll to Telcentris.Voxox.Desktop.Libs/<configuration>
    - Copy <configuration>/SaslUtilityManaged.pdb to Telcentris.Voxox.Desktop.Libs/<configuration>
    - Copy <configuration>/ScramSha1ClientApi.dll to Telcentris.Voxox.Desktop.Libs/<configuration>
    - Copy <configuration>/ScramSha1ClientApi.pdb to Telcentris.Voxox.Desktop.Libs/<configuration>

  - Copy ScramSha1ClientApi.lib to LibJingleXmpp/Libs  (Either debug or release will do, but use release)
  - Copy IScramSha1Client.h     to LibJingleXmpp/talk/xmpp

  - Rebuild XMPP libs and follow instructions for that.
