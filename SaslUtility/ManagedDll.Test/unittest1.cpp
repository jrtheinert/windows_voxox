
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Feb 2016
 */

#include "stdafx.h"
#include "CppUnitTest.h"

#include "../ManagedDll/IScramSha1Client.h"
#include <string>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ManagedDllTest
{		
	TEST_CLASS(SaslUtilTest1)
	{
		//NOTE: This unit test does not work because it cannot find SaslUtilityManaged.dll
		//	Use ScramSha1UnmanagedApp for testing.
		TEST_METHOD( TestScramSha1 )
		{
			IScramSha1Client* wrapper = IScramSha1Client::CreateInstance();

//			wrapper->setUseTestVector( true );
			wrapper->initialize( "user", "pencil" );

			std::string msg1 = wrapper->getClientFirstMessage();

			IScramSha1Client::Destroy( wrapper );
		}
	};
}