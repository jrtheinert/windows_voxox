
//A small class to hold all the SIP Configuration settings.
//	This will make it easier to pass them around to various classes,
//	and to provide standard validation methods.

#pragma once

#include "ISipAgent.h"
#include <string>

class SipConfig
{
public:
	SipConfig();
	~SipConfig()	{}

	//Gets ---------------------------------------------------------------------------
	int						getLogLevel()						const			{ return d_logLevel;	}
	int						getMaxCalls()						const			{ return d_maxCalls;	}
	bool					getDoProxyCheck()					const			{ return d_doProxyCheck;}
	std::string				getSipLogFile()						const			{ return d_sipLogFile;	}

	std::string				getUserName()						const			{ return d_userName;	}
	std::string				getDisplayName()					const			{ return d_displayName;	}
	std::string				getIdentity()						const			{ return d_identity;	}
	std::string				getPassword()						const			{ return d_password;	}
	std::string				getRealm()							const			{ return d_realm;		}

	std::string				getSipAddress()						const			{ return d_sipAddress;	}
	std::string				getSipHost()						const			{ return d_sipHost;		}	//TODO-PJSIP: May be duplicate of getSipAddress()
	int						getSipPort()						const;

	ISipAgent::Topology		getDiscoveryTopology()				const			{ return d_discoveryTopology;	}
	bool					getEnableIceMedia()					const			{ return d_enableIceMedia;		}
	bool					getEnableIceSecurity()				const			{ return d_enableIceSecurity;	}
 
	bool					getTopologyEncryption()				const			{ return d_topologyEncryption;	}
	bool					getTopologyTurn()					const			{ return d_topologyTurn;		}
		
	bool					getSipUdp()							const			{ return d_sipUdp;		}
	bool					getSipTcp()							const			{ return d_sipTcp;		}
	bool					getSipTls()							const			{ return d_sipTls;		}

	bool					getKeepAlive()						const			{ return d_keepAlive;	}
	bool					getUseRport()						const			{ return d_useRport;	}

	ISipAgent::Encryption	getEncryption()						const			{ return d_encryption;	}

	int						getRegistrationRefreshInterval()	const			{ return d_registrationRefreshInterval;	}
	int						getFailureInterval()				const			{ return d_failureInterval;				}
	int						getOptionsKeepAliveInterval()		const			{ return d_optionsKeepAliveInterval;	}

	int						getMinSessionTime()					const			{ return d_minSessionTime;			}
	bool					getSupportSessionTimer()			const			{ return d_supportSessionTimer;		}
	bool					getInitiateSessionTimer()			const			{ return d_initiateSessionTimer;	}

	std::string				getTagProlog()						const			{ return d_tagProlog;				}

	/** SIP Options */
	bool					getSipP2pPresence()					const			{ return d_sipP2pPresence;			}
	bool					getSipUseOptionsRequest()			const			{ return d_sipUseOptionsRequest;	}
	int						getSipRegisterTimeout()				const			{ return d_sipRegisterTimeout;		}
	int						getSipFailureTimeout()				const			{ return d_sipFailureTimeout;		}
	int						getSipPublishTimeout()				const			{ return d_sipPublishTimeout;		}
	bool					getSipUseTypingState()				const			{ return d_sipUseTypingState;		}
	bool					getSipChatWithoutPresence()			const			{ return d_sipChatWithoutPresence;	}
	
	std::string				getSipUuid()						const			{ return d_sipUuid;	}

	//These are formatted from other data.
	std::string				getSipUserAgent()					const			{ return d_sipUserAgent;	}
	std::string				getSdpOrigin()						const			{ return d_sdpOrigin;		}
	std::string				getSipUserUrl()						const			{ return d_sipUserUrl;		}
	std::string				getProxyUrl()						const			{ return d_proxyUrl;		}

	ISipAgent::ITlsInfo*		getTlsInfo()					const			{ return d_tlsInfo;			}
	ISipAgent::IServerInfoList*	getServers()					const			{ return d_servers;			}

	//Helpers
	bool isEncryptionRequired()		const;
	bool isTransportValidForTls()	const;
	bool hasValidCredentials()		const;

	bool getSipUdpEx() const;
	bool getSipTcpEx() const;
	bool getSipTlsEx() const;

	void ensureValidUuid();

	//Sets ---------------------------------------------------------------------------
	void setLogLevel    ( int                val )				{ d_logLevel	 = val;	}
	void setMaxCalls    ( int                val )				{ d_maxCalls	 = val;	}
	void setDoProxyCheck( int                val )				{ d_doProxyCheck = val;	}
	void setSipLogFile  ( const std::string& val )				{ d_sipLogFile	 = val;	}

	void setUserName   ( const std::string& val )				{ d_userName	= val;	}
	void setDisplayName( const std::string& val )				{ d_displayName	= val;	}
	void setIdentity   ( const std::string& val )				{ d_identity	= val;	}
	void setPassword   ( const std::string& val )				{ d_password	= val;	}
	void setRealm      ( const std::string& val )				{ d_realm		= val;	}

	void setSipAddress ( const std::string& val )				{ d_sipAddress	= val;	}
	void setSipHost    ( const std::string& val )				{ d_sipHost		= val;	}		//TODO-PJSIP: May be same as getSipAddress()

	void setDiscoveryTopology ( ISipAgent::Topology val )		{ d_discoveryTopology	= val;	}
	void setEableIceMedia     ( bool val )						{ d_enableIceMedia		= val;	}
	void setEnableIceSecurity ( bool val )						{ d_enableIceSecurity	= val;	}
	void setTopologyEncryption( bool val )						{ d_topologyEncryption	= val;	}
	void setTopologyTurn      ( bool val )						{ d_topologyTurn		= val;	}

	void setSipUdp( bool val )									{ d_sipUdp		= val;	}
	void setSipTcp( bool val )									{ d_sipTcp		= val;	}
	void setSipTls( bool val )									{ d_sipTls		= val;	}

	void setKeepAlive( bool val )								{ d_keepAlive	= val;	}
	void setUseRport ( bool val )								{ d_useRport	= val;	}

	void setEncryption( ISipAgent::Encryption val )				{ d_encryption	= val;	}

	void setRegistrationRefreshInterval( int val )				{ d_registrationRefreshInterval	= val;	}
	void setFailureInterval            ( int val )				{ d_failureInterval				= val;	}
	void setOptionsKeepAliveInterval   ( int val )				{ d_optionsKeepAliveInterval	= val;	}

	void setMinSessionTime	    ( int  val )					{ d_minSessionTime			= val;	}
	void setSupportSessionTimer ( bool val )					{ d_supportSessionTimer		= val;	}
	void setInitiateSessionTimer( bool val )					{ d_initiateSessionTimer	= val;	}

	void setTagProlog( const std::string& val )					{ d_tagProlog				= val;	}

	/** SIP Options */
	void setSipP2pPresence		  ( bool		 val )			{ d_sipP2pPresence			= val;	}
	void setSipUseOptionsRequest  ( bool		 val )			{ d_sipUseOptionsRequest	= val;	}
	void setSipUseTypingState	  ( bool		 val )			{ d_sipUseTypingState		= val;	}
	void setSipRegisterTimeout	  ( int			 val );
	void setSipFailureTimeout	  ( int			 val );
	void setSipPublishTimeout	  ( int			 val );
	void setSipChatWithoutPresence( bool		 val )			{ d_sipChatWithoutPresence	= val;	}
	
	void setSipUuid( const std::string& val )					{ d_sipUuid					= val;	}

	//These are formatted from other data.
	void setSipUserAgent( const std::string& val )				{ d_sipUserAgent	= val;	}
	void setSdpOrigin   ( const std::string& val )				{ d_sdpOrigin		= val;	}
	void setSipUserUrl  ( const std::string& val )				{ d_sipUserUrl		= val;	}
	void setProxyUrl    ( const std::string& val )				{ d_proxyUrl		= val;	}

	void setTlsInfo( ISipAgent::ITlsInfo*		 val )			{ d_tlsInfo			= val;	}
	void setServers( ISipAgent::IServerInfoList* val )			{ d_servers			= val;	}

private:
	int						d_logLevel;			//Not SIP related.
	int						d_maxCalls;
	bool					d_doProxyCheck;
	std::string				d_sipLogFile;

	std::string				d_userName;			//Credentials user name
	std::string				d_displayName;		
	std::string				d_identity;			//Not sure how this differs from d_userName
	std::string				d_password;			//Credentials password
	std::string				d_realm;			//Realm for the SIP service.

	std::string				d_sipAddress;		//SIP address for the SIP service.
	std::string				d_sipHost;			//May be same as d_sipAddress	//TODO-PJSIP

	ISipAgent::Topology		d_discoveryTopology;
	bool					d_enableIceMedia;
	bool					d_enableIceSecurity;
 
	bool					d_topologyEncryption;
	bool					d_topologyTurn;

	bool					d_sipUdp;
	bool					d_sipTcp;
	bool					d_sipTls;
	bool					d_keepAlive;
	bool					d_useRport;

	ISipAgent::Encryption	d_encryption;

	int						d_registrationRefreshInterval;
	int						d_failureInterval;
	int						d_optionsKeepAliveInterval;

	int						d_minSessionTime;
	bool					d_supportSessionTimer;
	bool					d_initiateSessionTimer;

	std::string				d_tagProlog;

	/** SIP Options */
	bool					d_sipP2pPresence;
	bool					d_sipUseOptionsRequest;
	int						d_sipRegisterTimeout;
	int						d_sipFailureTimeout;
	int						d_sipPublishTimeout;
	bool					d_sipUseTypingState;
	bool					d_sipChatWithoutPresence;

	std::string				d_sipUuid;

	//These are formatted from other data.
	std::string				d_sipUserAgent;
	std::string				d_sdpOrigin;
	std::string				d_sipUserUrl;
	std::string				d_proxyUrl;

	ISipAgent::ITlsInfo*		d_tlsInfo;
	ISipAgent::IServerInfoList*	d_servers;

	//A couple of defined defaults
	const static unsigned int	s_defSipRegisterTimeout = 49 * 60;
	const static unsigned int	s_defSipFailureTimeout  =      30;
	const static unsigned int	s_defSipPublishTimeout  =  5 * 60;
};
