/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Sep 2014
 */

//-----------------------------------------------------------------------------
// This file will contain all the data type definitions used in SipAgent interface.
//
// Most/All of these classes will have C# counterparts in the main app.
//	So we will have to match the C# / C++ classes during data merging.
//
// As such, there will be almost no logic in these classes.  Just set/get methods.
//
// Keeping all the definitions in a single class makes C# intergration easier,
//	but should not be considered an overriding concern.
//
// The logic in existing Android classes should be implemented in the C# classes, 
//	not here.  Except where it will improve readability of SipAgent code.
//
// Let's try to keep the classes in alphabetical order.  In some instances we cannot because
//	we use class types that need to be defined.
//		TODO: Consider moving all the enums into a single defined class.
//
// Many of these classes will also have 'collection' versions, typically a list.
//	I hope that in most cases, std::list<class> will suffice.
//	These may just become typedefs.
//
// Each class should have:
//	- setters/getters
//	- operator=
//	- copy ctor (maybe)
//
//-----------------------------------------------------------------------------
#pragma once

#include "ISipAgent.h"

#include <string>
#include <list>

#include "DllExport.h"

class Codecs;
class LogEntry;
class ServerInfoList;
class SipEventData;

typedef void (*SipEventCallbackFunc)(const SipEventData&);

//=============================================================================
//This class simply gathers log info which is then passed to main app for actual logging
//	Expectation is that each LogEntry will trigger callback to main app, so no need for a LogEntryList
//	For our purposes, LOGGER_COMPONENT will always be the same 'SipAgent'.

class LogSendCallback
{
public:
	virtual void sendLog( const LogEntry& logEntry ) = 0;
};

// Macros for the LogEntry class.
#define LOG_VERBOSE( ... ) { LogEntry::makeAndSend( LogEntry::s_component.c_str(), LogEntry::Verbose, __FUNCTION__, NULL, 0, __VA_ARGS__ );	}
#define LOG_DEBUG( ... )   { LogEntry::makeAndSend( LogEntry::s_component.c_str(), LogEntry::Debug,   __FUNCTION__, NULL, 0, __VA_ARGS__ );	}
#define LOG_INFO( ... )    { LogEntry::makeAndSend( LogEntry::s_component.c_str(), LogEntry::Info,    __FUNCTION__, NULL, 0, __VA_ARGS__ );	}
#define LOG_WARN( ... )    { LogEntry::makeAndSend( LogEntry::s_component.c_str(), LogEntry::Warn,    __FUNCTION__, NULL, 0, __VA_ARGS__ );	}
#define LOG_ERROR( ... )   { LogEntry::makeAndSend( LogEntry::s_component.c_str(), LogEntry::Error,   __FUNCTION__, NULL, 0, __VA_ARGS__ );	}
#define LOG_FATAL( ... )   { LogEntry::makeAndSend( LogEntry::s_component.c_str(), LogEntry::Fatal,   __FUNCTION__, __FILE__, __LINE__, __VA_ARGS__ );	}	

class LogEntry
{
public:
	static std::string	    s_component;
	static LogSendCallback* s_logSendCallback;

	enum Level 
	{
		Fatal	= 1,
		Error	= 2,
		Warn	= 3,
		Info	= 4,
		Debug	= 5,
		Verbose = 6
	};

	LogEntry();
	LogEntry( const std::string & component, Level level, const std::string & className, const std::string & message, const char* filename, int line, unsigned long threadId );

	static void makeAndSend( const char* component, Level level, const char* className, const char* fileName, int line, const std::string& msg );
	static void makeAndSend( const char* component, Level level, const char* className, const char* fileName, int line, const char* format, ... );

	static void	sendEntry( const LogEntry& entry );
	static void setLogSendCallback( LogSendCallback* value )		{ s_logSendCallback = value;}

	static unsigned long getOsThreadId();	//TODO: should be in util class, maybe

	LogEntry operator=( const LogEntry& src );

	Level		 getLevel()		 const				{ return _level;		}
	std::string  getComponent()	 const				{ return _component;	}
	std::string	 getClassName()	 const				{ return _className;	}
	std::string	 getMessage()	 const				{ return _message;		}
	std::string	 getFileName()	 const				{ return _fileName;		}
	int			 getLineNumber() const				{ return _lineNumber;	}
	unsigned int getThreadId()	 const				{ return _threadId;		}
	std::tm		 getTime()		 const				{ return _time;			}

	bool		 hasFileName()	 const				{ return !_fileName.empty();	}

	void setLevel	  ( Level		 val )			{ _level	  = val;	}
	void setComponent ( std::string	 val )			{ _component  = val;	}
	void setClassName ( std::string  val )			{ _className  = val;	}
	void setMessage   ( std::string  val )			{ _message	  = val;	}
	void setFileName  ( std::string  val )			{ _fileName	  = val;	}
	void setLineNumber( int			 val )			{ _lineNumber = val;	}
	void setThreadId  ( unsigned int val )			{ _threadId	  = val;	}
	void setTime      ( std::tm&	 val )			{ _time		  = val;	}

private:
	Level		 _level;
	std::string	 _component;
	std::string	 _className;
	std::string	 _message;
	std::string	 _fileName;
	int			 _lineNumber;
	unsigned int _threadId;
	std::tm		 _time;
};

//=============================================================================


//=============================================================================

class StringList : public std::list<std::string>
{
public:
	static StringList	fromString( const std::string& text, const std::string& separator );

	std::string toString  ( const std::string& separator = " " ) const;
	bool		contains  ( const std::string& tgt );

	std::string operator[](unsigned i) const;
	void		operator+=( const std::string& str );

private:
	std::string	getNthEntry( size_t tgtEntry ) const;
};

//=============================================================================


//=============================================================================

class AudioDevice
{
public:
    DLLEXPORT enum Pos
    {
        Pos_DeviceName  = 0,
        Pos_DeviceId    = 1,
        Pos_DeviceType  = 2,
        Pos_DeviceGuid  = 3,
        Pos_Default     = 4,
        Pos_Max         = 5
    };

    DLLEXPORT AudioDevice()		    {}
	DLLEXPORT AudioDevice( const StringList& data );
    DLLEXPORT AudioDevice( const AudioDevice& audioDevice );

    DLLEXPORT ~AudioDevice();

    DLLEXPORT AudioDevice& operator= ( const AudioDevice& audioDevice );

    DLLEXPORT bool operator==( const AudioDevice& audioDevice );

    DLLEXPORT std::string	getName()		const			{ return _name;				}
    DLLEXPORT std::string	getGuid()		const			{ return _guid;				}
    DLLEXPORT std::string	getType()		const			{ return _type;				}
    DLLEXPORT int			getIndex()		const			{ return _index;			}
    DLLEXPORT bool			isDefault()		const			{ return _default;			}
    DLLEXPORT bool			isUseDefault()	const			{ return (_index == -1);	}	//Indicates we should use the default device, not that this *is* the default device.
    DLLEXPORT bool			isValid()		const			{ return (!_name.empty());	}

    DLLEXPORT StringList	getData() const;

    DLLEXPORT static StringList convertData( const StringList& inputData );

    DLLEXPORT static AudioDevice makeDefaultDevice( const std::string& useDefaultDeviceText, const std::string& type );
	DLLEXPORT static int         getDefaultDeviceIndex();

	//Gets -----------------------------------------------------------------------
	//	Added due to Managed->Unmanaged code conversion
    DLLEXPORT void setName		 ( const std::string& value )				{ _name		= value;	}
    DLLEXPORT void setGuid		 ( const std::string& value )				{ _guid		= value;	}
    DLLEXPORT void setType		 ( const std::string& value )				{ _type		= value;	}
    DLLEXPORT void setIndex		 ( int				  value )				{ _index	= value;	}
    DLLEXPORT void setIsDefault	 ( bool				  value )				{ _default	= value;	}


private:
    std::string _name;
    int         _index;
    std::string _type;
    std::string _guid;
    bool        _default;
};

//=============================================================================

class AudioDeviceList : public std::list<AudioDevice>
{
public:
    AudioDevice findBestMatch( const AudioDevice& deviceIn );
};

//=============================================================================


//=============================================================================

class Codec
{
	friend class Codecs;

	enum Type
	{
		Type_Unknown		= 0,
		Type_Audio			= 1,
		Type_Video			= 2,
		Type_ComfortNoise	= 3,
		Type_TelephoneEvent	= 4,
		Type_RedundantAudio	= 5,
	};

public:
	Codec();
	Codec( const std::string& codecStr );
	virtual ~Codec();

	//Gets ---------------------------------------------------------------------
	std::string		getName()				const				{ return _name;			}
	unsigned int	getSampleRate()			const				{ return _sampleRate;	}
	unsigned int	getChannelCount()		const				{ return _channelCount;	}
	Type			getType()				const				{ return _type;			}
	bool			canBeDisabled()			const				{ return _canBeDisabled;}
	bool			isEnabled()				const				{ return _enabled;		}
	int				getOrder()				const               { return _order;		}

	bool			isTypeAudio()			const				{ return getType() == Type_Audio;			}
	bool			isTypeComfortNoise()	const				{ return getType() == Type_ComfortNoise;	}
	bool			isTypeTelephoneEvent()	const				{ return getType() == Type_TelephoneEvent;	}
	bool			isTypeRedundantAudio()	const				{ return getType() == Type_RedundantAudio;	}

	//Sets ---------------------------------------------------------------------
	void setName         ( const std::string& val )			{ _name			 = val;	}
	void setSampleRate   ( unsigned int       val )			{ _sampleRate	 = val;	}
	void setChannelCount ( unsigned int       val )			{ _channelCount  = val;	}
	void setType         ( Type               val )			{ _type			 = val;	}
	void setCanBeDisabled( bool				  val )			{ _canBeDisabled = val; }
	void setEnabled		 ( bool				  val )			{ _enabled		 = val; }
	void setOrder        ( int                val )			{ _order         = val; }

	void initVars();

	bool		fromString( const std::string& val );
	std::string	toString  ( bool includeChannelCount = true ) const;
	void		determineInfoFromName();

	Codec& operator= ( const Codec& src );
	bool   operator==( const Codec& src );

private:
	std::string		_name;
	unsigned int	_sampleRate;
	unsigned int	_channelCount;
	Type			_type;
	bool			_canBeDisabled;
	bool			_enabled;
	int				_order;

	static const std::string s_separator;
};

//=============================================================================


//=============================================================================

class Codecs : public std::list<Codec>
{
public:
	void add( const Codec& codec );
	void fromDefaultCodecList();
	void fromPreferredCodecList();

	bool hasTypeAudio()			 const							{ return hasType( Codec::Type_Audio          );	}
	bool hasTypeVideo()			 const							{ return hasType( Codec::Type_Video          );	}
	bool hasTypeComfortNoise()	 const							{ return hasType( Codec::Type_ComfortNoise   );	}
	bool hasTypeTelephoneEvent() const							{ return hasType( Codec::Type_TelephoneEvent );	}
	bool hasTypeRedundantAudio() const							{ return hasType( Codec::Type_RedundantAudio );	}

	int getCountAudio()			 const							{ return getCountByType( Codec::Type_Audio          );	}
	int getCountVideo()			 const							{ return getCountByType( Codec::Type_Video          );	}
	int getCountComfortNoise()	 const							{ return getCountByType( Codec::Type_ComfortNoise   );	}
	int getCountTelephoneEvent() const							{ return getCountByType( Codec::Type_TelephoneEvent );	}
	int getCountRedundantAudio() const							{ return getCountByType( Codec::Type_RedundantAudio );	}

	StringList	getComfortNoiseAsStringList  ( bool enabled ) const			{ return getTypeAsStringList( Codec::Type_ComfortNoise,   enabled, true );	}
	StringList	getTelephoneEventAsStringList( bool enabled ) const			{ return getTypeAsStringList( Codec::Type_TelephoneEvent, enabled, true );	}
	StringList	getRedundantAudioAsStringList( bool enabled ) const			{ return getTypeAsStringList( Codec::Type_RedundantAudio, enabled, true );	}

	StringList	toStringList( bool audioOnly, bool includeChannelCount ) const;

	void		reorder   ( const StringList& codecList );
	void		setEnabled( const StringList& codecList );
	
	static StringList  toStringListForSipWrapper( const StringList& codecList );

private:
	bool		setOrder( const std::string& codecStr, int order );

	bool		hasType            ( Codec::Type type ) const;
	int			getCountByType	   ( Codec::Type type ) const;
	StringList	getTypeAsStringList( Codec::Type type, bool enabled, bool includeChannelCount = true ) const;
};

//=============================================================================


//=============================================================================

class EnumDeviceType
{
public:

	enum DeviceType 
	{
		DeviceTypeMasterVolume,		//MIXERLINE_COMPONENTTYPE_DST_SPEAKERS
		DeviceTypeWaveOut,			//MIXERLINE_COMPONENTTYPE_SRC_WAVEOUT
		DeviceTypeWaveIn,			//MIXERLINE_COMPONENTTYPE_DST_WAVEIN
		DeviceTypeCDOut,			//MIXERLINE_COMPONENTTYPE_SRC_COMPACTDISC
		DeviceTypeMicrophoneOut,	//MIXERLINE_COMPONENTTYPE_SRC_MICROPHONE
		DeviceTypeMicrophoneIn		//MIXERLINE_COMPONENTTYPE_DST_WAVEIN + MIXERLINE_COMPONENTTYPE_SRC_MICROPHONE
	};

	static std::string toString(DeviceType deviceType);

	static DeviceType toDeviceType(const std::string & deviceType);

private:
	static void init();
};

//=============================================================================

class EnumPhoneCallState
{
public:
	enum PhoneCallState 
	{
		PhoneCallStateUnknown		=  0,		/** Unknown state. */
		PhoneCallStateError			=  1,		/** An error occured. */
		PhoneCallStateResumed		=  2,		/** Phone call resumed (after holding a call). */
		PhoneCallStateTalking		=  3,		/** Conversation state. */
		PhoneCallStateDialing		=  4,		/** Outgoing phone call: dialing. */
		PhoneCallStateRinging		=  5,		/** Outgoing phone call: ringing. */
		PhoneCallStateClosed		=  6,		/** Phone call closed (call rejected or call hang up). */
		PhoneCallStateIncoming		=  7,		/** Incoming phone call. */
		PhoneCallStateHold			=  8,		/** Phone call hold. */
		PhoneCallStateMissed		=  9,		/** Phone call missed */
		PhoneCallStateRedirected	= 10,		/** Phone call redirected */
		PhoneCallStateRingingStart	= 11,
		PhoneCallStateRingingStop	= 12,
	};

	static std::string toString( PhoneCallState state );
};

//=============================================================================



//=============================================================================

/**
 * Phone line states (server error, timeout, closed...).
 *
 * @author Tanguy Krotoff
 */
class EnumPhoneLineState
{
public:
	enum PhoneLineState 
	{
		PhoneLineStateUnknown				= 0,	/** Unknown state. */
		PhoneLineStateProgress				= 1,	/** Progress state. */
		PhoneLineStateServerError			= 2,	/** Connection to the SIP server failed. */
		PhoneLineStateAuthenticationError	= 3,	/** Connection to the SIP server failed.due to an bad login or password */
		PhoneLineStateTimeout				= 4,	/** Connection to the SIP platform failed due to a timeout. */
		PhoneLineStateOk					= 5,	/** Successfull connection to the SIP platform. */
		PhoneLineStateClosed				= 6		/** Line unregistered. */
	};

	static std::string toString( PhoneLineState state );
};

//=============================================================================


//=============================================================================

class EnumVideoMode
{
public:
	enum VideoMode 
	{
		Nobody,
		Contacts,
		Anybody,
	};

	/**
	 * Converts a VideoQuality into a string.
	 */
	static std::string toString(VideoMode videoMode);

	/**
	 * Converts a string into a VideoQuality.
	 */
	static VideoMode toVideoMode(const std::string & videoMode);

private:
	static void init();
};

//=============================================================================


//=============================================================================

class EnumVideoQuality
{
public:
	enum VideoQuality
	{
		VideoQualityNormal		= 0,	/** Down=0-512kbit/s up=0-128kbit/s. */
		VideoQualityGood		= 1,	/** Down=512-2048kbit/s up=128-256kbit/s. */
		VideoQualityVeryGood	= 2,	/** Down=+2048kbit/s up=+256kbit/s. */
		VideoQualityExcellent	= 3		/** Down=+8192kbit/s up=+1024kbit/s. */
	};

	/**
	 * Converts a VideoQuality into a string.
	 */
	static std::string toString( VideoQuality videoQuality );

	/**
	 * Converts a string into a VideoQuality.
	 */
	static VideoQuality toVideoQuality(const std::string & videoQuality);

private:
	static void init();
};

//=============================================================================


//=============================================================================

class ServerInfo : public ISipAgent::IServerInfo
{
	friend class ServerInfoList;

	//enum Owner
	//{
	//	Owner_None	= 0,
	//	Owner_User  = 1,
	//	Owner_App	= 2,
	//};

public:
	//enum Type
	//{
	//	Type_None		  = 0,
	//	Type_SipRegistrar = 1,
	//	Type_SipProxy	  = 2,
	//	Type_Stun		  = 3,
	//	Type_TurnUdp	  = 4,
	//	Type_TurnTcp	  = 5,
	//	Type_TurnTls	  = 6,
	//	Type_HttpProxy	  = 7,
	//	Type_HttpTunnel	  = 8,
	//	Type_HttpsTunnel  = 9,
	//};

	DLLEXPORT ServerInfo();

	DLLEXPORT virtual ~ServerInfo();

	//Gets ------------------------------------------------------------
	DLLEXPORT Type			getType()			const					{ return _type;			}
	DLLEXPORT std::string	getAddress()		const					{ return _address;		}
	DLLEXPORT unsigned int	getPort()			const					{ return _port;			}
	DLLEXPORT std::string	getUserId()			const					{ return _userId;		}
	DLLEXPORT std::string	getPassword()		const					{ return _password;		}
	DLLEXPORT bool			useSsl()			const					{ return _ssl;			}

	DLLEXPORT ISipAgent::HTTPProxy		getHttpProxyType()	const		{ return _httpProxyType;}
	DLLEXPORT ISipAgent::TopologyHost	getTopologyHost()   const		{ return getTopologyHostFromType( getType() );	}

	DLLEXPORT bool		isTypeNone()		 const						{ return getType() == Type_None;		}
	DLLEXPORT bool		isTypeSipRegistrar() const						{ return getType() == Type_SipRegistrar;}
	DLLEXPORT bool		isTypeSipProxy()	 const						{ return getType() == Type_SipProxy;	}
	DLLEXPORT bool		isTypeStun()		 const						{ return getType() == Type_Stun;		}
	DLLEXPORT bool		isTypeTurnUdp()		 const						{ return getType() == Type_TurnUdp;		}
	DLLEXPORT bool		isTypeTurnTcp()		 const						{ return getType() == Type_TurnTcp;		}
	DLLEXPORT bool		isTypeTurnTls()		 const						{ return getType() == Type_TurnTls;		}
	DLLEXPORT bool		isTypeHttpProxy()    const						{ return getType() == Type_HttpProxy;	}
	DLLEXPORT bool		isTypeHttpTunnel()   const						{ return getType() == Type_HttpTunnel;	}
	DLLEXPORT bool		isTypeHttpsTunnel()  const						{ return getType() == Type_HttpsTunnel;	}

	DLLEXPORT bool		isTopologyHost()		 const;

	//Sets ------------------------------------------------------------
	void setType		 ( Type                 val )     		{ _type			 = val;	}
	void setAddress		 ( const std::string&	val ) 			{ _address		 = val;	}
	void setPort		 ( unsigned int			val )    		{ _port			 = val;	}
	void setUserId		 ( const std::string&	val )	  		{ _userId		 = val;	}
	void setPassword	 ( const std::string&	val ) 			{ _password		 = val;	}
	void setSsl			 ( bool					val )			{ _ssl			 = val;	}
	void setHttpProxyType( ISipAgent::HTTPProxy	val )			{ _httpProxyType = val; }

	void setTypeNone()		 									{ setType( Type_None		 );	}
	void setTypeSipRegistrar()	 								{ setType( Type_SipRegistrar );	}
	void setTypeSipProxy()		 								{ setType( Type_SipProxy	 );	}
	void setTypeStun()		 									{ setType( Type_Stun		 );	}
	void setTypeTurnUdp()		 								{ setType( Type_TurnUdp		 );	}
	void setTypeTurnTcp()		 								{ setType( Type_TurnTcp		 );	}
	void setTypeTurnTls()		 								{ setType( Type_TurnTls		 );	}
	void setTypeHttpProxy() 									{ setType( Type_HttpProxy	 );	}
	void setTypeHttpTunnel()   									{ setType( Type_HttpTunnel	 );	}
	void setTypeHttpsTunnel()   								{ setType( Type_HttpsTunnel	 );	}

//	void setType( const std::string& type )						{ setType( typeFromString( type ) );	}
	DLLEXPORT std::string	getSipProtocol()	const;
	DLLEXPORT std::string	getUrlFormat()		const;
	DLLEXPORT std::string	getHostPortFormat()	const;

	//Misc ------------------------------------------------------------
	void initVars();

	ServerInfo&	operator= ( const ServerInfo& src );
	bool		operator==( const ServerInfo& src );
	bool		isValid() const;


	std::string	getTypeText()    const					{ return getTypeText( getType() );	}


	static ISipAgent::IServerInfo::Type typeFromString( const std::string& typeString );

	static ISipAgent::HTTPProxy proxyTypeFromString( const std::string&   type );
	static std::string			proxyTypeToString  ( ISipAgent::HTTPProxy type );

	static ISipAgent::Topology topologyFromString( const std::string&  topo );
	static std::string		   topologyToString  ( ISipAgent::Topology topo );

	static ISipAgent::TopologyHost getTopologyHostFromType( ServerInfo::Type type );

private:
	Owner		 getOwner()    const					{ return _owner;	}		//Hide implementation details.

	void setOwner   ( Owner              val )	  		{ _owner	= val;	}

	static std::string	getTypeText( Type type );

private:
	Type		_type;
	Owner		_owner;
	
	std::string	 _address;		//IP Address or URL
	unsigned int _port;

	std::string	_userId;		//Not all servers will need creds
	std::string	_password;

	ISipAgent::HTTPProxy	_httpProxyType;	//Proxy
	bool		_ssl;			//Http Tunnel

	//TODO: these should be in map mentioned above.
	const static std::string s_serverType_SipRegistrar;
	const static std::string s_serverType_SipProxy;
	const static std::string s_serverType_Stun;
	const static std::string s_serverType_TurnUdp;
	const static std::string s_serverType_TurnTcp;
	const static std::string s_serverType_TurnTls;
	const static std::string s_serverType_HttpProxy;
	const static std::string s_serverType_HttpTunnel;
	const static std::string s_serverType_HttpsTunnel;
	const static std::string s_serverType_None;

	const static std::string s_proxyType_None;
	const static std::string s_proxyType_Auto;
	const static std::string s_proxyType_System;
	const static std::string s_proxyType_Http;
	const static std::string s_proxyType_Socks4;
	const static std::string s_proxyType_Socks5;

	const static std::string s_topologyType_None;
	const static std::string s_topologyType_Ice;
	const static std::string s_topologyType_Stun;
	const static std::string s_topologyType_Turn;
};

//==============================================================================



//==============================================================================

class ServerInfoList : public std::list<ServerInfo>, public ISipAgent::IServerInfoList
{
public:
	DLLEXPORT bool addSipProxy    ( const std::string& address, unsigned int serverPort, bool ssl = false );	//, unsigned  localPort );
	DLLEXPORT bool addSipRegistrar( const std::string& address, unsigned int port, bool ssl );
	DLLEXPORT bool addStun        ( const std::string& address, unsigned int port );
	DLLEXPORT bool addTurnUdp     ( const std::string& address, unsigned int port, const std::string& login, const std::string& password );
	DLLEXPORT bool addTurnTcp     ( const std::string& address, unsigned int port, const std::string& login, const std::string& password );
	DLLEXPORT bool addTurnTls     ( const std::string& address, unsigned int port, const std::string& login, const std::string& password );
	DLLEXPORT bool addHttpProxy   ( const std::string& address, unsigned int port, const std::string& login, const std::string& password, ISipAgent::HTTPProxy httpProxyType );

	DLLEXPORT ServerInfo*	getFirstSipRegistrar()	const				{ return getFirstByType( ServerInfo::Type_SipRegistrar	);	}
	DLLEXPORT ServerInfo*	getFirstSipProxy()		const				{ return getFirstByType( ServerInfo::Type_SipProxy		);	}
	DLLEXPORT ServerInfo*	getFirstStun()			const				{ return getFirstByType( ServerInfo::Type_Stun			);	}
	DLLEXPORT ServerInfo*	getFirstTurnUdp()		const				{ return getFirstByType( ServerInfo::Type_TurnUdp		);	}
	DLLEXPORT ServerInfo*	getFirstTurnTcp()		const				{ return getFirstByType( ServerInfo::Type_TurnTcp		);	}
	DLLEXPORT ServerInfo*	getFirstTurnTls()		const				{ return getFirstByType( ServerInfo::Type_TurnTls		);	}
	DLLEXPORT ServerInfo*	getFirstHttpProxy()		const				{ return getFirstByType( ServerInfo::Type_HttpProxy		);	}

	DLLEXPORT ServerInfo	getRandomSipRegistrar()	const				{ return getRandomByType( ServerInfo::Type_SipRegistrar	);	}
	DLLEXPORT ServerInfo	getRandomSipProxy()		const				{ return getRandomByType( ServerInfo::Type_SipProxy		);	}
	DLLEXPORT ServerInfo	getRandomStun()			const				{ return getRandomByType( ServerInfo::Type_Stun			);	}
	DLLEXPORT ServerInfo	getRandomTurnUdp()		const				{ return getRandomByType( ServerInfo::Type_TurnUdp		);	}
	DLLEXPORT ServerInfo	getRandomTurnTcp()		const				{ return getRandomByType( ServerInfo::Type_TurnTcp		);	}
	DLLEXPORT ServerInfo	getRandomTurnTls()		const				{ return getRandomByType( ServerInfo::Type_TurnTls		);	}
	DLLEXPORT ServerInfo	getRandomHttpProxy()	const				{ return getRandomByType( ServerInfo::Type_HttpProxy	);	}

	DLLEXPORT bool			hasSipRegistrar()		const				{ return getCountByType( ServerInfo::Type_SipRegistrar	) > 0;	}
	DLLEXPORT bool			hasSipProxy()			const				{ return getCountByType( ServerInfo::Type_SipProxy		) > 0;	}
	DLLEXPORT bool			hasStun()				const				{ return getCountByType( ServerInfo::Type_Stun			) > 0;	}
	DLLEXPORT bool			hasTurnUdp()			const				{ return getCountByType( ServerInfo::Type_TurnUdp		) > 0;	}
	DLLEXPORT bool			hasTurnTcp()			const				{ return getCountByType( ServerInfo::Type_TurnTcp		) > 0;	}
	DLLEXPORT bool			hasTurnTls()			const				{ return getCountByType( ServerInfo::Type_TurnTls		) > 0;	}
	DLLEXPORT bool			hasHttpProxy()			const				{ return getCountByType( ServerInfo::Type_HttpProxy		) > 0;	}

	DLLEXPORT bool			hasTurn()				const				{ return hasTurnUdp() || hasTurnTcp();	}
	DLLEXPORT bool			hasTurns()				const				{ return hasTurnTls();					}

	DLLEXPORT int			merge ( const ServerInfoList& servers );
	DLLEXPORT bool			exists( const ServerInfo& src );

	DLLEXPORT bool			add( const ServerInfo& src );
//	DLLEXPORT void			getTopologyHostInfo( ISipAgent::TopologyHostList* hosts);

private:
	ServerInfo*	getFirstByType ( ServerInfo::Type tgtType ) const;
	ServerInfo	getRandomByType( ServerInfo::Type tgtType ) const;

	ServerInfo	getFirstByOwner ( ServerInfo::Owner tgtOwner ) const;
	ServerInfo	getRandomByOwner( ServerInfo::Owner tgtOwner ) const;

	ServerInfo	getFirstByTypeAndOwner ( ServerInfo::Type tgtType, ServerInfo::Owner tgtOwner ) const;
	ServerInfo	getRandomByTypeAndOwner( ServerInfo::Type tgtType, ServerInfo::Owner tgtOwner ) const;

	//Helpers
	int getRandom( int max ) const;	//1-based
	int getCountByType        ( ServerInfo::Type tgtType   ) const;
	int getCountByOwner       ( ServerInfo::Owner tgtOwner ) const;
	int getCountByTypeAndOwner( ServerInfo::Type tgtType, ServerInfo::Owner tgtOwner ) const;
};

//==============================================================================



//==============================================================================

//A simple class to contain all TLS related info for SipWrapper.

class TlsInfo : public ISipAgent::ITlsInfo
{
public:
	DLLEXPORT TlsInfo();
	DLLEXPORT virtual ~TlsInfo();

	//Gets ------------------------------------------------------------
	ISipAgent::TLSMethod getMethod()				 const						{ return _method;					}
	bool				 getVerifyCertificate()		 const						{ return _verifyCertificate;		}
	unsigned int		 getVerifyDepth()			 const						{ return _verifyDepth;				}
	bool				 getRequireCertificate()	 const						{ return _requireCertificate;		}
	std::string			 getPrivateKey()			 const						{ return _privateKey;				}
	std::string			 getPrivateKeyPassword()	 const						{ return _privateKeyPassword;		}
	std::string			 getCertificateAuthorities() const						{ return _certificateAuthorities;	}
	std::string			 getCertificate()			 const						{ return _certificate;				}
	std::string			 getCrl()					 const						{ return _crl;						}
	std::string			 getCiphers()				 const						{ return _ciphers;					}

	//Sets ------------------------------------------------------------
	void setMethod				  ( ISipAgent::TLSMethod val )					{ _method				  = val;	}
	void setVerifyCertificate	  ( bool                 val )					{ _verifyCertificate	  = val;	}
	void setVerifyDepth			  ( unsigned int         val )					{ _verifyDepth			  = val;	}
	void setRequireCertificate	  ( bool                 val )					{ _requireCertificate	  = val;	}
	void setPrivateKey			  ( const std::string&   val )					{ _privateKey			  = val;	}
	void setPrivateKeyPassword	  ( const std::string&   val )					{ _privateKeyPassword	  = val;	}
	void setCertificateAuthorities( const std::string&   val )					{ _certificateAuthorities = val;	}
	void setCertificate			  ( const std::string&   val )					{ _certificate			  = val;	}
	void setCrl					  ( const std::string&   val )					{ _crl					  = val;	}
	void setCiphers				  ( const std::string&   val )					{ _ciphers				  = val;	}

	//Misc ------------------------------------------------------------
	void initVars();

	TlsInfo&	operator= ( const TlsInfo& src );
	bool		operator==( const TlsInfo& src );

	static ISipAgent::TLSMethod methodFromString( const std::string&   tlsMethod );
	static std::string		    methodToString  ( ISipAgent::TLSMethod tlsMethod );

private:
	ISipAgent::TLSMethod _method;
	bool				 _verifyCertificate;
	unsigned int		 _verifyDepth;
	bool				 _requireCertificate;
	std::string			 _privateKey;
	std::string			 _privateKeyPassword;
	std::string			 _certificateAuthorities;
	std::string			 _certificate;
	std::string			 _crl;
	std::string			 _ciphers;

	static const std::string s_tlsMethodV3;
	static const std::string s_tlsMethodV23;
	static const std::string s_tlsMethodV2;
	static const std::string s_tlsMethodV1;
};

//==============================================================================

//=============================================================================
//Abstract class to be implemented by main app and ptr provided to SipAgent
class DLLEXPORT AppEventHandler
{
public:
	AppEventHandler()		{}

	//Connect and Disconnect
	virtual void connectedEvent   () = 0;
	virtual void disconnectedEvent( bool connectionError, const std::string& reason ) = 0;

	//Line and Call callbacks from old SipWrapper
	virtual void phoneCallStateChangedEvent( int callId, EnumPhoneCallState::PhoneCallState state, const std::string & from, int statusCode ) = 0;
	virtual void phoneLineStateChangedEvent( int lineId, EnumPhoneLineState::PhoneLineState state ) = 0;

	//INFO request received
	virtual void infoRequestReceivedEvent( int callId, const std::string& content, const std::string& contentType ) = 0;

	//Logging
	virtual void sendLog( const LogEntry& logEntry ) = 0;

};
//=============================================================================



//=============================================================================
// SipEvent class
//	- Event type and related data for SIP events handled in UI layer.
//	- The data elements we get from the specific events in AppEventHandler
//		will be used to create this and pass to managed code.
//	- TODO: Consider just using this one class in unmanaged code as well.
//=============================================================================

class SipEventData
{
	enum EventType		//Which type of event occurred.  Allow listeners to select events they care about.
	{
		Unknown			= 0,	//So we can test that all events set type properly
		Connect			= 1,
		Disconnect		= 2,
		CallStateChange = 3,
		LineStateChange = 4,
		InfoRequest		= 5,
		Log				= 6
	};


	SipEventData();								//This is private so user must call static Make methods to ensure data is complete.
	SipEventData( EventType eventType);			//This is private so user must call static Make methods to ensure data is complete.

	void initVars();

public:
	DLLEXPORT static SipEventData*	makeConnectEvent();
	DLLEXPORT static SipEventData*	makeDisconnectEvent( bool connectError, const std::string& disconnectMsg );
	DLLEXPORT static SipEventData*	makeLineStateChangeEvent ( int lineId, EnumPhoneLineState::PhoneLineState state );
	DLLEXPORT static SipEventData*	makeCallStateChangedEvent( int callId, EnumPhoneCallState::PhoneCallState state, const std::string& from, int statusCode );
	DLLEXPORT static SipEventData*  makeInfoRequestReceivedEvent( int callId, const std::string& content, const std::string& contentType );
	DLLEXPORT static SipEventData*	makeLogEvent( const LogEntry& logEntry );

	DLLEXPORT ~SipEventData()        {}

	//Gets ---------------------------------------------------------------------
	DLLEXPORT EventType								getEventType()			const	{ return mEventType;	}

	//Connect event has no extras

	//Disconnect Event extras
	DLLEXPORT	bool								getConnectionError()	const	{ return mConnectionError;	}
	DLLEXPORT	std::string							getDisconnectMsg()		const	{ return mDisconnectMsg;	}

	//Line State changed event extras
	DLLEXPORT int									getLineId()				const	{ return mLineId;			}
	DLLEXPORT EnumPhoneLineState::PhoneLineState	getLineState()			const	{ return mLineState;		}

	//Call State changed event extras
	DLLEXPORT int									getCallId()				const	{ return mCallId;			}
	DLLEXPORT EnumPhoneCallState::PhoneCallState	getCallState()			const	{ return mCallState;		}
	DLLEXPORT std::string							getCallFrom()			const	{ return mCallFrom;			}
	DLLEXPORT int									getCallStatus()			const	{ return mCallStatus;		}

	//Info request event extras
	DLLEXPORT std::string							getContentType()		const	{ return mContentType;		}
	DLLEXPORT std::string							getContent()			const	{ return mContent;			}

	//Log Event extras
	DLLEXPORT LogEntry								getLogEntry()			const	{ return mLogEntry;	}

	//Sets ---------------------------------------------------------------------
	//Do NOT export these
	 void setEventType	  ( EventType						 value )			{ mEventType		= value;	}

	//Connect event has no extras

	//Disconnect Event extras
	void setConnectionError( bool							 value )			{ mConnectionError	= value;	}
	void setDisconnectMsg  ( const std::string&				 value )			{ mDisconnectMsg	= value;	}

	//Line State changed event extras
	void setLineId		 ( int								 value )			{ mLineId			= value;	}
	void setLineState	 ( EnumPhoneLineState::PhoneLineState value )			{ mLineState		= value;	}

	//Call State changed event extras
	void setCallId		  ( int								  value )			{ mCallId			= value;	}
	void setCallState	  ( EnumPhoneCallState::PhoneCallState value )			{ mCallState		= value;	}
	void setCallFrom      ( const std::string&				  value )			{ mCallFrom			= value;	}
	void setCallStatus    ( int								  value )			{ mCallStatus		= value;	}

	//Info request event extras
	void setContentType   ( const std::string&				  value )			{ mContentType		= value;	}
	void setContent       ( const std::string&				  value )			{ mContent			= value;	}

	//Log Event extras
	void setLogEntry	  ( const LogEntry&					  value )			{ mLogEntry			= value;	}

    //Convenience methods -----------------------------------------------------------------------------------------
	DLLEXPORT bool	isConnectEvent()				{ return mEventType == EventType::Connect;			}
	DLLEXPORT bool	isDisconnectEvent()				{ return mEventType == EventType::Disconnect;		}
	DLLEXPORT bool	isLineStateChangeEvent()		{ return mEventType == EventType::LineStateChange;	}
	DLLEXPORT bool	isCallStateChangeEvent()		{ return mEventType == EventType::CallStateChange;	}
	DLLEXPORT bool	isInfoRequestEvent()			{ return mEventType == EventType::InfoRequest;		}
	DLLEXPORT bool	isLogEvent()					{ return mEventType == EventType::Log;				}

private:
	EventType							mEventType;

	//Disconnect Event extras
	bool								mConnectionError;
	std::string							mDisconnectMsg;

	//Line State changed event extras
	int									mLineId;
	EnumPhoneLineState::PhoneLineState	mLineState;

	//Call State changed event extras
	int									mCallId;
	EnumPhoneCallState::PhoneCallState	mCallState;
	std::string							mCallFrom;
	int									mCallStatus;
		
	//Info Request event extras
	std::string							mContentType;
	std::string							mContent;

	//Log Event extras
	LogEntry							mLogEntry;

};

//=============================================================================
