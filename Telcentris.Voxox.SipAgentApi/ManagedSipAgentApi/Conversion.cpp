
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Sep 2014
 */

#include "stdafx.h"
#include "Conversion.h"

using namespace System;
//Since the C++/CLI namespace is like the C++ namespace, we need to declare several nested
//	namespaces to get what we want.
namespace Telcentris
{

namespace Voxox
{

namespace ManagedSipAgentApi 
{

Conversion::Conversion()
{
}

//-----------------------------------------------------------------------------
//Static helpers - These may be better in a utility class, but OK here for now.
//-----------------------------------------------------------------------------
std::string Conversion::M2U_String(String^ textIn)
{
	using namespace System::Runtime::InteropServices;

	std::string result;

	if ( textIn != nullptr )
	{
		const char* chars = (const char*)(Marshal::StringToHGlobalAnsi(textIn)).ToPointer();

		result = chars;

		Marshal::FreeHGlobal(IntPtr((void*)chars));
	}

	return result;
}

//static
String^ Conversion::U2M_String( const std::string& textIn )
{
	using namespace System::Runtime::InteropServices;

	String^ result = gcnew String( textIn.c_str() );

	return result;
}

//static
ManagedSipDataTypes::StringList^ Conversion::U2M_StringList( const StringList& listIn )
{
	ManagedSipDataTypes::StringList^ listOut = gcnew ManagedSipDataTypes::StringList;

	for ( std::string strIn : listIn )
	{
		listOut->Add( U2M_String( strIn ) );
	}

	return listOut;
}

StringList Conversion::M2U_StringList( ManagedSipDataTypes::StringList^ listIn )
{
	StringList result;

	for each ( String^ strIn in listIn )
	{
		std::string str = M2U_String( strIn );
		result.push_back( str );
	}

	return result;
}


ManagedSipDataTypes::AudioDevice^ Conversion::U2M_AudioDevice( const AudioDevice& deviceIn )
{
	ManagedSipDataTypes::AudioDevice^ result = gcnew ManagedSipDataTypes::AudioDevice;

	result->Guid = U2M_String( deviceIn.getGuid() );
	result->Name = U2M_String( deviceIn.getName() );
	result->Type = U2M_String( deviceIn.getType() );
	result->Index = deviceIn.getIndex();
	result->IsDefault = deviceIn.isDefault();

	return result;
}

AudioDevice Conversion::M2U_AudioDevice( ManagedSipDataTypes::AudioDevice^ deviceIn )
{
	AudioDevice result;

	result.setGuid	   ( Conversion::M2U_String( deviceIn->Guid ) );
	result.setName     ( Conversion::M2U_String( deviceIn->Name ) );
	result.setType     ( Conversion::M2U_String( deviceIn->Type ) );
	result.setIndex    ( deviceIn->Index );
	result.setIsDefault( deviceIn->IsDefault );

	return result;
}

ManagedSipDataTypes::AudioDeviceList^	Conversion::U2M_AudioDeviceList( const AudioDeviceList& deviceListIn )
{
	ManagedSipDataTypes::AudioDeviceList^ result = gcnew ManagedSipDataTypes::AudioDeviceList;

	for ( AudioDevice deviceIn : deviceListIn )
	{
		ManagedSipDataTypes::AudioDevice^ device = U2M_AudioDevice( deviceIn );
		result->Add( device );
	}

	return result;
}


ManagedSipDataTypes::ServerInfo^ Conversion::U2M_ServerInfo( const ServerInfo& serverInfoIn )
{
	ManagedSipDataTypes::ServerInfo^ result = gcnew ManagedSipDataTypes::ServerInfo;

	ManagedSipDataTypes::SipAgent::HTTPProxy httpProxyType = (ManagedSipDataTypes::SipAgent::HTTPProxy)serverInfoIn.getHttpProxyType();

	result->Address		  = U2M_String( serverInfoIn.getAddress() );
	result->UserId		  = U2M_String( serverInfoIn.getUserId()  );
	result->Password	  = U2M_String( serverInfoIn.getPassword() );
	result->Port		  = serverInfoIn.getPort();
	result->UseSsl		  = serverInfoIn.useSsl();
	result->HttpProxyType = httpProxyType;

	return result;
}

ServerInfo Conversion::M2U_ServerInfo( ManagedSipDataTypes::ServerInfo^ serverInfoIn )
{
	ServerInfo result;

	ISipAgent::HTTPProxy proxyType	= (ISipAgent::HTTPProxy)serverInfoIn->HttpProxyType;
	ServerInfo::Type	type		= (ServerInfo::Type)    serverInfoIn->Type;

	result.setType    ( type );
	result.setAddress ( M2U_String( serverInfoIn->Address  ) );
	result.setUserId  ( M2U_String( serverInfoIn->UserId   ) );
	result.setPassword( M2U_String( serverInfoIn->Password ) );
	result.setPort    ( serverInfoIn->Port );
	result.setSsl     ( serverInfoIn->UseSsl );
	result.setHttpProxyType( proxyType  );

	return result;
}

ManagedSipDataTypes::ServerInfoList^ Conversion::U2M_ServerInfoList( const ServerInfoList& serversIn )
{
	ManagedSipDataTypes::ServerInfoList^ result = gcnew ManagedSipDataTypes::ServerInfoList;

	for ( ServerInfo serverInfoIn : serversIn )
	{
		ManagedSipDataTypes::ServerInfo^ serverInfo = U2M_ServerInfo( serverInfoIn );
		result->Add( serverInfo );
	}

	return result;
}

ServerInfoList Conversion::M2U_ServerInfoList( ManagedSipDataTypes::ServerInfoList^ serversIn )
{
	ServerInfoList result;

	for each ( ManagedSipDataTypes::ServerInfo^ serverInfoIn in serversIn )
	{
		ServerInfo si = M2U_ServerInfo( serverInfoIn );
		result.push_back( si );
	};

	return result;
}

ManagedSipDataTypes::TlsInfo^ Conversion::U2M_TlsInfo( const TlsInfo& tlsInfoIn )
{
	ManagedSipDataTypes::TlsInfo^ result = gcnew ManagedSipDataTypes::TlsInfo;

	ManagedSipDataTypes::SipAgent::TLSMethod tlsMethod = (ManagedSipDataTypes::SipAgent::TLSMethod) tlsInfoIn.getMethod();

	result->Certificate				= U2M_String( tlsInfoIn.getCertificate()			);
	result->CertificateAuthorities	= U2M_String( tlsInfoIn.getCertificateAuthorities() );
	result->Ciphers					= U2M_String( tlsInfoIn.getCiphers()				);
	result->Crl						= U2M_String( tlsInfoIn.getCrl()					);
	result->PrivateKey				= U2M_String( tlsInfoIn.getPrivateKey()				);
	result->PrivateKeyPassword		= U2M_String( tlsInfoIn.getPrivateKeyPassword()     );
	result->Method					= tlsMethod;
	result->RequireCertificate		= tlsInfoIn.getRequireCertificate();
	result->VerifyCertificate		= tlsInfoIn.getVerifyCertificate();
	result->VerifyDepth			    = tlsInfoIn.getVerifyDepth();

	return result;
}

TlsInfo Conversion::M2U_TlsInfo( ManagedSipDataTypes::TlsInfo^ tlsInfoIn )
{
	TlsInfo result;

	ISipAgent::TLSMethod tlsMethod = (ISipAgent::TLSMethod)tlsInfoIn->Method;

	result.setCertificate			( M2U_String( tlsInfoIn->Certificate			) );
	result.setCertificateAuthorities( M2U_String( tlsInfoIn->CertificateAuthorities	) );
	result.setCiphers				( M2U_String( tlsInfoIn->Ciphers				) );
	result.setCrl					( M2U_String( tlsInfoIn->Crl					) );
	result.setPrivateKey			( M2U_String( tlsInfoIn->PrivateKey 			) );
	result.setPrivateKeyPassword    ( M2U_String( tlsInfoIn->PrivateKeyPassword  	) );
	result.setMethod				( tlsMethod );
	result.setRequireCertificate    ( tlsInfoIn->RequireCertificate );
	result.setVerifyCertificate     ( tlsInfoIn->VerifyCertificate  );
	result.setVerifyDepth			( tlsInfoIn->VerifyDepth        );

	return result;
}


ManagedSipDataTypes::LogEntry^ Conversion::U2M_LogEntry( const LogEntry& dataIn )
{
	ManagedSipDataTypes::LogEntry^ dataOut = gcnew ManagedSipDataTypes::LogEntry;

	ManagedSipDataTypes::LogEntry::LogLevel level = (ManagedSipDataTypes::LogEntry::LogLevel) dataIn.getLevel();

	dataOut->ClassName	= U2M_String( dataIn.getClassName() );
	dataOut->Component	= U2M_String( dataIn.getComponent() );
	dataOut->FileName	= U2M_String( dataIn.getFileName()  );
	dataOut->Level		= level;
	dataOut->LineNumber	= dataIn.getLineNumber();
	dataOut->Message	= U2M_String( dataIn.getMessage() );
	dataOut->ThreadId	= dataIn.getThreadId();

	return dataOut;
}

ManagedSipDataTypes::SipEventData^ Conversion::U2M_SipEventData( const SipEventData& dataIn )
{
	ManagedSipDataTypes::SipEventData^ dataOut = gcnew ManagedSipDataTypes::SipEventData;

	ManagedSipDataTypes::SipEventData::EventType	eventType = (ManagedSipDataTypes::SipEventData::EventType  ) dataIn.getEventType();
	ManagedSipDataTypes::PhoneLineState::LineState	lineState = (ManagedSipDataTypes::PhoneLineState::LineState) dataIn.getLineState();
	ManagedSipDataTypes::PhoneCallState::CallState	callState = (ManagedSipDataTypes::PhoneCallState::CallState) dataIn.getCallState();

	dataOut->Event			 = eventType;

	dataOut->ConnectionError = dataIn.getConnectionError();
	dataOut->DisconnectMsg   = U2M_String( dataIn.getDisconnectMsg() );

	dataOut->LineId			 = dataIn.getLineId();
	dataOut->LineState		 = lineState;

	dataOut->CallId			 = dataIn.getCallId();
	dataOut->CallState		 = callState;
	dataOut->CallFrom		 = U2M_String( dataIn.getCallFrom() );
	dataOut->CallStatusCode  = dataIn.getCallStatus();

	dataOut->ContentType	 = U2M_String( dataIn.getContentType() );
	dataOut->Content		 = U2M_String( dataIn.getContent()	   );

	dataOut->LogEntry		 = U2M_LogEntry( dataIn.getLogEntry() );

	return dataOut;
}

}	//namespace ManagedSipAgentApi 

}	//namespace Voxox

}	//Telcentris
