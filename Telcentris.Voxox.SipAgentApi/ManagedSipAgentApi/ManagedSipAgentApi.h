
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Sep 2014
 */

#pragma once

#include "NativeApi/src/DataTypes.h"		//For SipEventCallbackFunc, AppEventHandler

using namespace System;

using System::EventArgs;
using System::Object;
using System::Runtime::InteropServices::UnmanagedFunctionPointerAttribute;
using System::Runtime::InteropServices::CallingConvention;

class SipAgentApiWrapper;	//Fwd declaration
class SipEventData;

namespace Telcentris
{

namespace Voxox
{

namespace ManagedSipAgentApi 
{

class MyAppEventHandler : public AppEventHandler
{
public:
	MyAppEventHandler();

	void setCallback( SipEventCallbackFunc callback );

	//We hit this line registration and making phone call.
	void connectedEvent();
		
	//I have NOT hit this yet.
	void disconnectedEvent( bool connectionError, const std::string& reason );

	//We hit this during line registration and phone call.
	void phoneLineStateChangedEvent( int lineId, EnumPhoneLineState::PhoneLineState state );

	//We hit this during phone call.
	void phoneCallStateChangedEvent( int callId, EnumPhoneCallState::PhoneCallState state, const std::string& from, int status );

	//We hit this during phone call.
	void infoRequestReceivedEvent( int callId, const std::string& content, const std::string& contentType );

	//Logging - We hit this throughout API calls.
	void sendLog( const LogEntry& logEntry );
	
private:
	SipEventCallbackFunc mCallback;
};

public ref class SipEventArgs : EventArgs
{
public:
	SipEventArgs( ManagedSipDataTypes::SipEventData^ data )
	{
		mData = data;
	}

	property ManagedSipDataTypes::SipEventData^ Data
	{
		ManagedSipDataTypes::SipEventData^ get()	{ return mData; }
	}

private:
	ManagedSipDataTypes::SipEventData^ mData;
};

//Set proper calling convention for delegate used as callback to avoid stack corruption.
[UnmanagedFunctionPointerAttribute( CallingConvention::Cdecl)]	
delegate void SipEventDelegate( const SipEventData& );

public delegate void SipEventHandler( Object^ sender, SipEventArgs^ e );

//=============================================================================

public ref class SipAgentApi
{
public:
	static SipAgentApi^ getInstance();
	~SipAgentApi();

	void setMaxCalls    ( Int32   val );
	void setDoProxyCheck( Boolean val );
	void setSipLogFile  ( String^ sipAddress );

	//Note: We do NOT use the setAppEventHandler as we do in native code.
	//		Instead we have the SipEventDelegate, SipEventCallbackFunc, etc.
	//
	//Callback to handle SIP events, including logging from the SIP stack.
	void setSipEventCallback   ();
	void setSipEventCallback   ( SipEventCallbackFunc callback );
	void sipEventCallback      ( const SipEventData& data );
	void addSipEventListener   ( SipEventHandler^ sipEventDelegate );
	void removeSipEventListener( SipEventHandler^ sipEventDelegate );

	void onSipEvent( SipEventArgs^ e );		//SipEvent handler


    //-------------------------------
	//	Initialization Methods
    //-------------------------------

    /** Initializes the SIPWrapper. */
    void init( int logLevel );

    /** Terminates the SIP connection. */
    void terminate();

    /**
     * @return  True if is Initialized
     */
    Boolean isInitialized();


    //-------------------------------
	//	Virtual Line Methods
    //-------------------------------

     /**
     * Creates and adds a phone line.
     *
     * @param displayName display name inside the SIP URI (e.g tanguy inside "tanguy <sip:tanguy_k@wengo.fr>")
     * @param username most of the time same as identity
     * @param identity first part of the SIP URI (e.g tanguy_k inside "sip:tanguy_k@wengo.fr")
     * @param password password corresponding to the SIP URI
     * @param realm realm/domain
     * @param proxyServer SIP proxy server
     * @param registerServer SIP register server
     * @return the corresponding id of the line just created or VirtualLineIdError if failed to create the line
     */
//    Int32 addVirtualLine( String^ displayName, String^ username, String^ identity, String^ password, String^ realm,
//						  ManagedSipDataTypes::ServerInfo^ proxyServer, ManagedSipDataTypes::ServerInfo^ registrarServer );
    Int32 addVirtualLine();

    /**
     * Registers a given phone line.
     *
     * @param   lineId id of the phone line to register
     * @return  0 if making a register succeeds, otherwise 1
     */
    Int32 registerVirtualLine( Int32 lineId );

    /**
     * Removes a given phone line.
     *
     * @param   lineId id of the phone line to remove
     * @param   force if true, forces the removal without waiting for the unregister
                if false, removal is not forced
     */
    void removeVirtualLine( Int32 lineId, Boolean force );

    //-------------------------------
	//	Call Methods
	//-------------------------------

    /**
     * Dials a phone number.
     *
     * @param lineId line to use to dial the phone number
     * @param sipAddress SIP address to call (e.g phone number to dial)
     * @param enableVideo enable/disable video usage
     * @return the phone call id (callId)
     */
    Int32 makeCall( Int32 lineId, String^ sipAddress, Boolean enableVideo );

    /**
     * Notifies the remote side (the caller) that this phone is ringing.
     *
     * This corresponds to the SIP code "180 Ringing".
     *
     * @param callId id of the phone call to make ringing
     * @param enableVideo enable/disable video usage
     */
    void sendRingingNotification( Int32 callId, Boolean enableVideo );

    /**
     * Accepts a given phone call.
     *
     * @param callId id of the phone call to accept
     * @param enableVideo enable/disable video usage
     */
    void acceptCall( Int32 callId, Boolean enableVideo );

    /**
     * Rejects a given phone call.
     *
     * @param callId id of the phone call to reject
     */
    void rejectCall( Int32 callId );

    /**
     * Closes a given phone call.
     *
     * @param callId id of the phone call to close
     */
    void closeCall( Int32 callId );

    /**
     * Holds a given phone call.
     *
     * @param callId id of the phone call to hold
     */
    void holdCall(Int32 callId);

    /**
     * Resumes a given phone call.
     *
     * @param callId id of the phone call to resume
     */
    void resumeCall( Int32 callId );

    /**
     * Mute/Unmute call
     *
     * @param callId id of the phone call to transfer
     * @param muteOn - desired state of mute
     */
    void muteCall( Int32 callId,  Boolean muteOn );

    /**
     * Blind transfer the specified call to another party.
     *
     * @param callId id of the phone call to transfer
     * @param sipAddress transfer target
     */
    void blindTransfer( Int32 callId,  String^ sipAddress );

    /**
     * Sends a DTMF to a given phone call.
     *
     * @param callId phone call id to send a DTMF
     * @param dtmf DTMF tone to send
     */
    void playDtmf( Int32 callId, Char dtmf );

    /**
     * Sends and plays a sound file to a given phone call.
     *
     * @param callId phone call id to play the sound file
     * @param soundFile sound file to play
     */
    void playSoundFile( Int32 callId,  String^ soundFile );

    /**
     * Gets the audio codec in use by a given phone call.
     *
     * @param callId phone call id
     * @return audio codec in use
     */
//    String^ getAudioCodecUsed( Int32 callId );

    /**
     * Gets the video codec in use by a given phone call.
     *
     * @param callId phone call id
     * @return video codec in use
     */
//    String^ getVideoCodecUsed( Int32 callId );

    /**
     * @param callId phone call id
     * @return  True if encrytion is activated for the given call
     */
    Boolean isCallEncrypted( Int32 callId );

    /**
     * Gets the SIP CallId for a given phone call.
     *
     * @param callId phone call id
     * @return SIP Callid To be used with HTTP Interactionse
     */
    String^ getSipCallId( Int32 callId );


	//---------------------------------------------------------------------------------
	//Start SIP configuration set methods
	//---------------------------------------------------------------------------------

    /**
     * Sets calls encryption.
     *
     * @param enable if True encryption is activated
     */
	void setCallsEncryption( ManagedSipDataTypes::SipAgent::CallEncryption val );

    /**
     * Sets proxy parameters.
     */
    void addProxy(  String^ address,  UInt32 port,  String^ login,  String^  password, ManagedSipDataTypes::SipAgent::HTTPProxy httpProxyType );

    /**
     * Sets HTTP tunnel parameters.
     */
//    void addTunnel(  String^ address, UInt32 port, Boolean ssl );	//VOXOX - JRT - 2012.02.03 - Not currently called anywhere.

    /**
     * Sets the SIP parameters.
     */
	void setTlsInfo(  ManagedSipDataTypes::TlsInfo^        tlsInfo );
	void addServers(  ManagedSipDataTypes::ServerInfoList^ servers );			//Bulk add servers, typically from SSO.

    void addSipProxy(  String^ server, UInt32 serverPort );

	//VOXOX - JRT - 2012.02.03 - These are not currently called anywhere. Set via addServers().
//	void addStun   (  String^ address, UInt32 port );
//	void addTurnUdp(  String^ address, UInt32 port,  String^ login,  String^ password );
//	void addTurnTcp(  String^ address, UInt32 port,  String^ login,  String^ password );
//	void addTurnTls(  String^ address, UInt32 port,  String^ login,  String^ password );


	//Discover Topology
	void setDiscoveryTopology( ManagedSipDataTypes::SipAgent::Topology topology );
	void enableIceMedia      ( Boolean val );
	void enableIceSecurity   ( Boolean val );

	void setTopologyEncryption( Boolean val );
	void setTopologyTurn      ( Boolean val );

	void setSipUdp   ( Boolean val );
	void setSipTcp   ( Boolean val );
	void setSipTls   ( Boolean val );
	void setKeepAlive( Boolean val );
	void setUseRport ( Boolean val );

	void setRegistrationRefreshInterval( Int32   val );
	void setFailureInterval			   ( Int32   val );
	void setOptionsKeepAliveInterval   ( Int32   val );
	void setMinSessionTime			   ( Int32   val );
	void setSupportSessionTimer		   ( Boolean val );
	void setInitiateSessionTimer	   ( Boolean val );

	void setTagProlog(  String^ val );

	//Replaced generic method with specific methods.
	void setSipOptionRegisterTimeout    ( UInt32  value );		//In seconds
	void setSipOptionFailureTimeout     ( UInt32  value );		//In seconds
	void setSipOptionPublishTimeout     ( UInt32  value );		//In seconds
	void setSipOptionUseOptionsRequest  ( Boolean value );
	void setSipOptionUseTypingState     ( Boolean value );
	void setSipOptionP2pPresence        ( Boolean value );
	void setSipOptionUuid               ( String^ value );
	void setSipOptionChatWithoutPresence( Boolean value );				//This was not coded for in older setSipOptions() method, so I'm not sure we need it.

    //------------------------------------------
    //	Audio Methods
    //------------------------------------------

    /**
     * Sets the audio codec list sorted by preference
     *
     * @param audioCodecList the audio codec list
     */
//	ManagedSipDataTypes::SipAgent::Result setAudioCodecs( ManagedSipDataTypes::StringList^ codecs, Boolean cn, Boolean te, Boolean red );

//	void		getAudioCodecs( ManagedSipDataTypes::AudioCodecInfo^ codecInfo );
//	void		setPreferredAudioCodecOrder();

//	ManagedSipDataTypes::StringList^	getPreferredAudioCodecList() ;

    /**
     * Sets the call ringer/alerting device.
     *
     * @param device ringer device
     * @return true if no error, false otherwise
     */
	void setRingerOutputAudioDevice( ManagedSipDataTypes::AudioDevice^ device );

    /**
     * Sets the call input device (in-call microphone).
     *
     * @param device input device
     * @return true if no error, false otherwise
     */
	void setCallInputAudioDevice( ManagedSipDataTypes::AudioDevice^ device );

    /**
     * Sets the call output device (in-call speaker).
     *
     * @param device output device
     * @return true if no error, false otherwise
     */
	void setCallOutputAudioDevice( ManagedSipDataTypes::AudioDevice^ device );

    /**
     * Enables or disables Acoustic Echo Cancellation (AEC).
     *
     * @param enable true if AEC enable, false if AEC should be disabled
     */
    void enableAEC( Boolean enable );
	void enableAGC( Boolean enable );
	void enableNoiseSuppression( Boolean enable );

	void setCredentials( String^ displayName, String^ userName, String^ identify, String^ password, String^ realm );



    //-----------------------------------------
	//	Video Methods
	//	Videos not supported yet.
    //-----------------------------------------

    ///**
    // * Sets the video device.
    // *
    // * @param deviceName the name of the video device
    // */
    //void setVideoDevice( String^ deviceName );

    ///**
    // * Sets the Video Quality type.
    // *
    // * @see EnumVideoQuality
    // */
    //void setVideoQuality( EnumVideoQuality::VideoQuality videoQuality );

    ///**
    // * Set video image flip.
    // * This parameter is dynamic so it can be set during a call.
    // *
    // * @param flip if true flip the image
    // */
    //void flipVideoImage( Boolean flip );

	//---------------------------------------------------------------------------------
	//End SIP configuration set methods
	//---------------------------------------------------------------------------------


	//---------------------------------------------------------------------------------
	//Start SIP configuration get methods
	//---------------------------------------------------------------------------------

	//These gets were added to support unit tests
	ManagedSipDataTypes::ServerInfoList^ getServers();
	ManagedSipDataTypes::TlsInfo^		 getTlsInfo();

	Int32			 getLineCount();

	//---------------------------------------------------------------------------------
	//End SIP configuration get methods
	//---------------------------------------------------------------------------------


	//---------------------------------------------------------------------------------
	//Call audio related methods
	//---------------------------------------------------------------------------------
	//Output
	Boolean setSpeakerVolume( UInt32 volume );			// Sets the speaker |volume| level. Valid range is [0,255].
    Int32	getSpeakerVolume();							// Gets the speaker |volume| level.

	//Ringing
    Boolean setRingingVolume( UInt32 volume );			// Sets the ringing |volume| level. Valid range is [0,255].
    Int32  getRingingVolume();							// Gets the ringing |volume| level.

//    Boolean setSystemRingingMute( Boolean enable );		// Mutes the ringing device completely in the operating system.
//    Boolean getSystemRingingMute();						// Gets the ringing device mute state in the operating system.

	//Input
	Boolean setMicVolume( UInt32 volume );				// Sets the microphone volume level. Valid range is [0,255].
    Int32  getMicVolume();								// Gets the microphone volume level.

    Int32  getSpeechInputLevelFullRange();				// Gets the microphone speech |level|, mapped linearly to the range [0,32768].

	//---------------------------------------------------------------------------------
	//End call audio related methods
	//---------------------------------------------------------------------------------

	SipAgentApi();

private:
	SipAgentApiWrapper*		mApi;
	MyAppEventHandler*		mAppEventHandler;

	SipEventDelegate^		mSipEventDelegate;
    event SipEventHandler^	mSipEventHandler;

	static SipAgentApi^ mInstance = gcnew SipAgentApi();
};


}	//namespace SipAgentApi 

}	//namespace Voxox

}	//Telcentris

