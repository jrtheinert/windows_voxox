
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Sep 2014
 */

// This is the main DLL file.

#include "stdafx.h"

#include "ManagedSipAgentApi.h"
#include "Conversion.h"

#include "NativeApi/src/SipAgentApiWrapper.h"

using namespace System::Runtime::InteropServices;	//For Marshal of callback ptr.

static GCHandle gch;	//To lock the DbChange callback in place.  We may need to revisit this.

//Since the C++/CLI namespace is like the C++ namespace, we need to declare several nested
//	namespaces to get what we want.
namespace Telcentris
{

namespace Voxox
{

namespace ManagedSipAgentApi 
{

MyAppEventHandler::MyAppEventHandler()		
{
	mCallback = nullptr;
}

void MyAppEventHandler::setCallback( SipEventCallbackFunc callback )
{
	mCallback = callback;
}

//We hit this line registration and making phone call.
void MyAppEventHandler::connectedEvent()
{
	if ( mCallback != nullptr )
	{
		SipEventData* data = SipEventData::makeConnectEvent();
		mCallback( *data );
	}
}
		
//I have NOT hit this yet.
void MyAppEventHandler::disconnectedEvent( bool connectionError, const std::string& reason )
{
	if ( mCallback != nullptr )
	{
		SipEventData* data = SipEventData::makeDisconnectEvent( connectionError, reason );
		mCallback( *data );
	}
}

//We hit this during line registration and phone call.
void MyAppEventHandler::phoneLineStateChangedEvent( int lineId, EnumPhoneLineState::PhoneLineState state )
{
	if ( mCallback != nullptr )
	{
		SipEventData* data = SipEventData::makeLineStateChangeEvent( lineId, state );
		mCallback( *data );
	}
}

//We hit this during phone call.
void MyAppEventHandler::phoneCallStateChangedEvent( int callId, EnumPhoneCallState::PhoneCallState state, const std::string& from, int status )
{
	if ( mCallback != nullptr )
	{
		SipEventData* data = SipEventData::makeCallStateChangedEvent( callId, state, from, status );
		mCallback( *data );
	}
}

//We hit this on incoming INFO request, typically from our server HTTP Listener Events
void MyAppEventHandler::infoRequestReceivedEvent( int callId, const std::string& content, const std::string& contentType )
{
	if ( mCallback != nullptr )
	{
		SipEventData* data = SipEventData::makeInfoRequestReceivedEvent( callId, content, contentType );
		mCallback( *data );
	}
}

//Logging - We hit this throughout API calls.
void MyAppEventHandler::sendLog( const LogEntry& logEntry )
{
	if ( mCallback != nullptr )
	{
		SipEventData* data = SipEventData::makeLogEvent( logEntry );
		mCallback( *data );
	}
}


//-----------------------------------------------------------------------------
//Admin methods
//-----------------------------------------------------------------------------

//static
SipAgentApi^ SipAgentApi::getInstance()
{
	return mInstance;
}

SipAgentApi::SipAgentApi()
{
	mAppEventHandler = new MyAppEventHandler();
	mApi			 = SipAgentApiWrapper::getInstance();

	mApi->setAppEventHandler( mAppEventHandler );

	setSipEventCallback();
}

SipAgentApi::~SipAgentApi()
{
	delete mApi;
	mApi = nullptr;
}

void SipAgentApi::setMaxCalls( Int32 val )
{
	mApi->setMaxCalls( val );
}

void SipAgentApi::setDoProxyCheck( Boolean val )
{
	mApi->setDoProxyCheck( val );
}

void SipAgentApi::setSipLogFile( String^ fileNameIn )
{
	std::string fileName = Conversion::M2U_String( fileNameIn );

	mApi->setSipLogFile( fileName );
}

//-----------------------------------------------------------------------------
//SIP event related methods
//-----------------------------------------------------------------------------

void SipAgentApi::setSipEventCallback()
{
	//Get Callback to SipAgentApi
	mSipEventDelegate = gcnew SipEventDelegate( this, &SipAgentApi::sipEventCallback );
	gch = GCHandle::Alloc( mSipEventDelegate );
	IntPtr ip = Marshal::GetFunctionPointerForDelegate( mSipEventDelegate );
	SipEventCallbackFunc cb = static_cast<SipEventCallbackFunc>(ip.ToPointer());
	setSipEventCallback( cb );
}

void SipAgentApi::setSipEventCallback( SipEventCallbackFunc callback )
{
	if ( mAppEventHandler != nullptr )
	{
		mAppEventHandler->setCallback( callback );
	}
}

void SipAgentApi::sipEventCallback( const SipEventData& dataIn )
{
	ManagedSipDataTypes::SipEventData^ data = Conversion::U2M_SipEventData( dataIn );

	SipEventArgs^ args = gcnew SipEventArgs( data );
	onSipEvent( args );
}

void SipAgentApi::onSipEvent( SipEventArgs^ e )
{
	mSipEventHandler( this, e );
}

void SipAgentApi::addSipEventListener( SipEventHandler^ sipEventDelegate )
{
	mSipEventHandler += sipEventDelegate;
}

void SipAgentApi::removeSipEventListener( SipEventHandler^ sipEventDelegate )
{
	mSipEventHandler -= sipEventDelegate;
}

//-------------------------------
//	Initialization Methods
//-------------------------------

void SipAgentApi::init( int logLevel )
{
	mApi->init( logLevel );
}

void SipAgentApi::terminate()
{ 
	mApi->terminate();
}

Boolean SipAgentApi::isInitialized()
{
	return mApi->isInitialized();
}


//-------------------------------
//	Virtual Line Methods
//-------------------------------

void SipAgentApi::setCredentials( String^ displayNameIn, String^ userNameIn, String^ identityIn, String^ passwordIn, String^ realmIn )
{
	std::string displayName = Conversion::M2U_String( displayNameIn );
	std::string userName    = Conversion::M2U_String( userNameIn    );
	std::string identity    = Conversion::M2U_String( identityIn    );
	std::string password    = Conversion::M2U_String( passwordIn    );
	std::string realm       = Conversion::M2U_String( realmIn       );

	mApi->setCredentials( displayName, userName, identity, password, realm );
}

Int32 SipAgentApi::addVirtualLine()
{
	return mApi->addVirtualLine();
}

Int32 SipAgentApi::registerVirtualLine( Int32 lineId )
{
	return mApi->registerVirtualLine( lineId );
}

void SipAgentApi::removeVirtualLine( Int32 lineId, Boolean force )
{
	mApi->removeVirtualLine( lineId, force );
}

//-------------------------------
//	Call Methods
//-------------------------------

Int32 SipAgentApi::makeCall( Int32 lineId, String^ sipAddressIn, Boolean enableVideo )
{
	std::string sipAddress = Conversion::M2U_String( sipAddressIn );
	return mApi->makeCall( lineId, sipAddress, enableVideo );
}

void SipAgentApi::sendRingingNotification( Int32 callId, Boolean enableVideo )
{
	mApi->sendRingingNotification( callId, enableVideo );
}

void SipAgentApi::acceptCall( Int32 callId, Boolean enableVideo )
{
	mApi->acceptCall( callId, enableVideo );
}

void SipAgentApi::rejectCall( Int32 callId )
{
	mApi->rejectCall( callId );
}

void SipAgentApi::closeCall( Int32 callId )
{
	mApi->closeCall( callId );
}

void SipAgentApi::holdCall( Int32 callId )
{
	mApi->holdCall( callId );
}

void SipAgentApi::resumeCall( Int32 callId )
{
	mApi->resumeCall( callId );
}

void SipAgentApi::muteCall( Int32 callId,  Boolean muteOn )
{
	mApi->muteCall( callId, muteOn );
}

void SipAgentApi::blindTransfer( Int32 callId,  String^ sipAddressIn )
{
	std::string sipAddress = Conversion::M2U_String( sipAddressIn );

	mApi->blindTransfer( callId, sipAddress );
}

void SipAgentApi::playDtmf( Int32 callId, Char dtmfIn )
{
	char dtmf = (char)dtmfIn;
	mApi->playDtmf( callId, dtmf );
}

void SipAgentApi::playSoundFile( Int32 callId,  String^ soundFileIn )
{
	std::string soundFile = Conversion::M2U_String( soundFileIn );
	
	mApi->playSoundFile( callId, soundFile );
}

//String^ SipAgentApi::getAudioCodecUsed( Int32 callId )
//{
//	std::string result = mApi->getAudioCodecUsed( callId );
//	return Conversion::U2M_String( result );
//}
//
//String^ SipAgentApi::getVideoCodecUsed( Int32 callId )
//{
//	std::string result = mApi->getVideoCodecUsed( callId );
//	return Conversion::U2M_String( result );
//}

Boolean SipAgentApi::isCallEncrypted( Int32 callId )
{
	return mApi->isCallEncrypted( callId );
}

String^ SipAgentApi::getSipCallId( Int32 callId )
{
	std::string result = mApi->getSipCallId( callId );
	return Conversion::U2M_String( result );
}

//ISipAgent::CallCallbacks* SipAgentApi::newCall( Int32 lineID, ISipAgent::Call* call )	//TODO-SIP
//{
//
//}


//---------------------------------------------------------------------------------
//Start SIP configuration set methods
//---------------------------------------------------------------------------------

void SipAgentApi::setCallsEncryption( ManagedSipDataTypes::SipAgent::CallEncryption valueIn )
{
	ISipAgent::Encryption value = (ISipAgent::Encryption)valueIn;
	mApi->setCallsEncryption( value );
}

void SipAgentApi::addProxy( String^ addressIn, UInt32 port, String^ loginIn, String^ passwordIn, ManagedSipDataTypes::SipAgent::HTTPProxy httpProxyTypeIn )
{
	std::string address  = Conversion::M2U_String( addressIn  );
	std::string login    = Conversion::M2U_String( loginIn    );
	std::string password = Conversion::M2U_String( passwordIn );

	ISipAgent::HTTPProxy httpProxyType = (ISipAgent::HTTPProxy)httpProxyTypeIn;

	mApi->addProxy( address, port, login, password, httpProxyType );
}

//void SipAgentApi::addTunnel(  String^ address, UInt32 port, Boolean ssl );	//VOXOX - JRT - 2012.02.03 - Not currently called anywhere.

void SipAgentApi::setTlsInfo( ManagedSipDataTypes::TlsInfo^ tlsInfoIn )
{
	TlsInfo tlsInfo = Conversion::M2U_TlsInfo( tlsInfoIn );
	mApi->setTlsInfo( tlsInfo );
}

void SipAgentApi::addServers( ManagedSipDataTypes::ServerInfoList^ serversIn )
{
	ServerInfoList servers = Conversion::M2U_ServerInfoList( serversIn );
	mApi->addServers( servers );
}

void SipAgentApi::addSipProxy( String^ serverIn, UInt32 serverPort )
{
	std::string server = Conversion::M2U_String( serverIn );

	mApi->addSipProxy( server, serverPort );
}

//VOXOX - JRT - 2012.02.03 - These are not currently called anywhere. Set via addServers().
//void SipAgentApi::addStun   (  String^ address, UInt32 port );
//void SipAgentApi::addTurnUdp(  String^ address, UInt32 port,  String^ login,  String^ password );
//void SipAgentApi::addTurnTcp(  String^ address, UInt32 port,  String^ login,  String^ password );
//void SipAgentApi::addTurnTls(  String^ address, UInt32 port,  String^ login,  String^ password );


//Discover Topology
void SipAgentApi::setDiscoveryTopology( ManagedSipDataTypes::SipAgent::Topology topologyIn )
{
	ISipAgent::Topology topology = (ISipAgent::Topology)topologyIn;
	mApi->setDiscoveryTopology( topology );
}

void SipAgentApi::enableIceMedia( Boolean val )
{
	mApi->enableIceMedia( val );
}

void SipAgentApi::enableIceSecurity( Boolean val )
{
	mApi->enableIceSecurity( val );
}

void SipAgentApi::setTopologyEncryption( Boolean val )
{
	mApi->setTopologyEncryption( val );
}

void SipAgentApi::setTopologyTurn( Boolean val )
{
	mApi->setTopologyTurn( val );
}

void SipAgentApi::setSipUdp( Boolean val )
{
	mApi->setSipUdp( val );
}

void SipAgentApi::setSipTcp( Boolean val )
{
	mApi->setSipTcp( val );
}

void SipAgentApi::setSipTls( Boolean val )
{
	mApi->setSipTls( val );
}

void SipAgentApi::setKeepAlive( Boolean val )
{
	mApi->setKeepAlive( val );
}

void SipAgentApi::setUseRport( Boolean val )
{
	mApi->setUseRport( val );
}

void SipAgentApi::setRegistrationRefreshInterval( Int32 val )
{
	mApi->setRegistrationRefreshInterval( val );
}

void SipAgentApi::setFailureInterval( Int32  val )
{
	mApi->setFailureInterval( val );
}

void SipAgentApi::setOptionsKeepAliveInterval( Int32 val )
{
	mApi->setOptionsKeepAliveInterval( val );
}

void SipAgentApi::setMinSessionTime( Int32 val )
{
	mApi->setMinSessionTime( val );
}

void SipAgentApi::setSupportSessionTimer( Boolean val )
{
	mApi->setSupportSessionTimer( val );
}

void SipAgentApi::setInitiateSessionTimer( Boolean val )
{
	mApi->setInitiateSessionTimer( val );
}

void SipAgentApi::setTagProlog( String^ valIn )
{
	std::string val = Conversion::M2U_String( valIn );
	mApi->setTagProlog( val );
}

void SipAgentApi::setSipOptionRegisterTimeout( UInt32 value )
{
	mApi->setSipOptionRegisterTimeout( value );
}

void SipAgentApi::setSipOptionFailureTimeout( UInt32 value )
{
	mApi->setSipOptionFailureTimeout( value );
}

void SipAgentApi::setSipOptionPublishTimeout( UInt32 value )
{
	mApi->setSipOptionPublishTimeout( value );
}

void SipAgentApi::setSipOptionUseOptionsRequest( Boolean value )
{
	mApi->setSipOptionUseOptionsRequest( value );
}

void SipAgentApi::setSipOptionUseTypingState( Boolean value )
{
	mApi->setSipOptionUseTypingState( value );
}

void SipAgentApi::setSipOptionP2pPresence( Boolean value )
{
	mApi->setSipOptionP2pPresence( value );
}

void SipAgentApi::setSipOptionUuid( String^ valueIn )
{
	std::string value = Conversion::M2U_String( valueIn );

	mApi->setSipOptionUuid( value );
}

void SipAgentApi::setSipOptionChatWithoutPresence( Boolean value )
{
	mApi->setSipOptionChatWithoutPresence( value );
}


//------------------------------------------
//	Audio Methods
//------------------------------------------

//ManagedSipDataTypes::SipAgent::Result SipAgentApi::setAudioCodecs( ManagedSipDataTypes::StringList^ codecsIn, Boolean cn, Boolean te, Boolean red )
//{
//	StringList		  codecs = Conversion::M2U_StringList( codecsIn );
//	ISipAgent::Result result = mApi->setAudioCodecs( codecs, cn, te, red );
//
//	return (ManagedSipDataTypes::SipAgent::Result) result;
//}
//
//void SipAgentApi::getAudioCodecs( ManagedSipDataTypes::AudioCodecInfo^ codecInfo )
//{
//	bool cn  = true;
//	bool te  = false;
//	bool red = false;
//
//	std::string temp = mApi->getAudioCodecs( &cn, &te, &red);
//
//	codecInfo->ComfortNoise = cn;
//	codecInfo->TelephoneEvents = te;
//	codecInfo->RedundantAudio  = red;
//
//	codecInfo->Codecs = Conversion::U2M_String( temp );
//}
//
//void SipAgentApi::setPreferredAudioCodecOrder()
//{
//	mApi->setPreferredAudioCodecOrder();
//}
//
//ManagedSipDataTypes::StringList^ SipAgentApi::getPreferredAudioCodecList()
//{
//	StringList temp = mApi->getPreferredAudioCodecList();
//
//	ManagedSipDataTypes::StringList^ result = Conversion::U2M_StringList( temp );
//	return result;
//}

void SipAgentApi::setRingerOutputAudioDevice( ManagedSipDataTypes::AudioDevice^ deviceIn )
{
	AudioDevice device = Conversion::M2U_AudioDevice( deviceIn );
	mApi->setRingerOutputAudioDevice( device );
}

void SipAgentApi::setCallInputAudioDevice( ManagedSipDataTypes::AudioDevice^ deviceIn )
{
	AudioDevice device = Conversion::M2U_AudioDevice( deviceIn );
	mApi->setCallInputAudioDevice( device );
}

void SipAgentApi::setCallOutputAudioDevice( ManagedSipDataTypes::AudioDevice^ deviceIn )
{
	AudioDevice device = Conversion::M2U_AudioDevice( deviceIn );
	mApi->setCallOutputAudioDevice( device );
}

void SipAgentApi::enableAEC( Boolean enable )
{
	mApi->enableAEC( enable );
}

void SipAgentApi::enableAGC( Boolean enable )
{
	mApi->enableAGC( enable );
}

void SipAgentApi::enableNoiseSuppression( Boolean enable )
{
	mApi->enableNoiseSuppression( enable );
}


//-----------------------------------------
//	Video Methods
//	Videos not supported yet.
//-----------------------------------------

//void SipAgentApi::setVideoDevice( String^ deviceName );

//void SipAgentApi::setVideoQuality( EnumVideoQuality::VideoQuality videoQuality );
//void SipAgentApi::flipVideoImage( Boolean flip );

//---------------------------------------------------------------------------------
//End SIP configuration set methods
//---------------------------------------------------------------------------------


//---------------------------------------------------------------------------------
//Start SIP configuration get methods
//---------------------------------------------------------------------------------

//Video - Not yet supported
//	String^							SipAgentApi::getVideoDevice();
//	EnumVideoQuality::VideoQuality	SipAgentApi::getVideoQuality(;
//	Boolean							SipAgentApi::shouldFlipVideoImage();

//These gets were added to support unit tests
ManagedSipDataTypes::ServerInfoList^ SipAgentApi::getServers()
{
	ServerInfoList temp = mApi->getServers();
	return Conversion::U2M_ServerInfoList( temp );
}

ManagedSipDataTypes::TlsInfo^	SipAgentApi::getTlsInfo()
{
	TlsInfo temp = mApi->getTlsInfo();
	return Conversion::U2M_TlsInfo( temp );
}

Int32 SipAgentApi::getLineCount()
{
	return mApi->getLineCount();
}

//---------------------------------------------------------------------------------
//End SIP configuration get methods
//---------------------------------------------------------------------------------


//---------------------------------------------------------------------------------
//Call audio related methods
//---------------------------------------------------------------------------------
//Output
Boolean SipAgentApi::setSpeakerVolume( UInt32 volume )
{
	return mApi->setSpeakerVolume( volume );
}

Int32	SipAgentApi::getSpeakerVolume()
{
	return mApi->getSpeakerVolume();
}

//Ringing
Boolean SipAgentApi::setRingingVolume( UInt32 volume )
{
	return mApi->setRingingVolume( volume );
}

Int32  SipAgentApi::getRingingVolume()
{
	return mApi->getRingingVolume();
}

//Input
Boolean SipAgentApi::setMicVolume( UInt32 volume )
{
	return mApi->setMicVolume( volume );
}

Int32  SipAgentApi::getMicVolume()
{
	return mApi->getMicVolume();
}

Int32  SipAgentApi::getSpeechInputLevelFullRange()
{
	return mApi->getSpeechInputLevelFullRange();
}

//---------------------------------------------------------------------------------
//End call audio related methods
//---------------------------------------------------------------------------------

}	//namespace SipAgentApi 

}	//namespace Voxox

}	//Telcentris
