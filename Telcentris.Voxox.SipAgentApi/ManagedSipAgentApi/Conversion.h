
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Sep 2014
 */

#pragma once

#include "NativeApi/src/DataTypes.h"

#include <string>

using System::String;

//Since the C++/CLI namespace is like the C++ namespace, we need to declare several nested
//	namespaces to get what we want.
namespace Telcentris
{

namespace Voxox
{

namespace ManagedSipAgentApi 
{

ref class Conversion
{
public:
	//Bi-directional
	static String^								U2M_String( const std::string& textIn );
	static std::string							M2U_String( String^            textIn );

	static ManagedSipDataTypes::StringList^		U2M_StringList( const StringList&			  listIn );
	static StringList							M2U_StringList( ManagedSipDataTypes::StringList^ listIn );

	static ManagedSipDataTypes::AudioDevice^		U2M_AudioDevice( const AudioDevice&             deviceIn );
	static AudioDevice							M2U_AudioDevice( ManagedSipDataTypes::AudioDevice^ deviceIn );

	static ManagedSipDataTypes::AudioDeviceList^	U2M_AudioDeviceList( const AudioDeviceList&     deviceListIn );

	static ManagedSipDataTypes::ServerInfo^		U2M_ServerInfo    ( const ServerInfo&             serverInfoIn );
	static ServerInfo							M2U_ServerInfo    ( ManagedSipDataTypes::ServerInfo^ serverInfoIn );

	static ManagedSipDataTypes::ServerInfoList^	U2M_ServerInfoList( const ServerInfoList&             serversIn );
	static ServerInfoList						M2U_ServerInfoList( ManagedSipDataTypes::ServerInfoList^ serversIn );

	static ManagedSipDataTypes::TlsInfo^			U2M_TlsInfo   ( const TlsInfo&			   tlsInfoIn    );
	static TlsInfo								M2U_TlsInfo   ( ManagedSipDataTypes::TlsInfo^ tlsInfoIn    );

	//Unmanaged -> managed
	static ManagedSipDataTypes::LogEntry^			U2M_LogEntry    ( const LogEntry&     logEntryIn     );
	static ManagedSipDataTypes::SipEventData^		U2M_SipEventData( const SipEventData& sipEventDataIn );


private:
	Conversion();
};


}	//namespace ManagedSipAgentApi 

}	//namespace Voxox

}	//Telcentris
