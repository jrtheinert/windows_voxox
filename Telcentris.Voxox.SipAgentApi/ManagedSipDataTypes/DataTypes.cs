﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Sep 2014
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Telcentris.Voxox.ManagedSipAgentApi.ManagedSipDataTypes
{ 

//=============================================================================
// LogEntry - Info the using app may log
//=============================================================================

public class LogEntry
{
	public enum LogLevel   //These values come from native code.
	{
		//Debug	= 0,
		//Info	= 1,
		//Warn	= 2,
		//Error	= 3,
		//Fatal	= 4
		Fatal	= 1,
		Error	= 2,
		Warn	= 3,
		Info	= 4,
		Debug	= 5,
		Verbose = 6
	};

	public LogEntry()
    {
        Level       = LogLevel.Debug;
        LineNumber  = 0;
        ThreadId    = 0;
    }


//TODO?	LogEntry operator=( LogEntry& src );

    //Properties ------------------------------------------------------------------
	public  LogLevel    Level           { get; set;     }    //Required for unmanaged class
	public  String      Component	    { get; set;     }    //Required for unmanaged class
	public  String      ClassName	    { get; set;     }    //Required for unmanaged class
	public  String      Message	        { get; set;     }    //Required for unmanaged class
	public  String      FileName	    { get; set;     }    //Required for unmanaged class
	public  Int32       LineNumber	    { get; set;     }    //Required for unmanaged class
	public  UInt32      ThreadId	    { get; set;     }    //Required for unmanaged class
//	std::tm		        Time	        { get; set;     }    //Required for unmanaged class     //TODO: tm to .NET time class.

	Boolean	hasFileName()
    { 
        return String.IsNullOrEmpty( FileName );
    }
};

//=============================================================================


//=============================================================================
// Generic StringList class - Essentially a typedef.
//=============================================================================

public class StringList : System.Collections.Generic.List<String>
{
//	StringList()		{}
//	~StringList()		{}

    //From unmanaged code.  My not need
//    static StringList	fromString(  std::string& text,  std::string& separator );

//    std::string toString  (  std::string& separator = " " ) ;
//    Boolean		contains  (  std::string& tgt );

//    std::string operator[](unsigned i) ;
//    void		operator+=(  std::string& str );

//private:
//    std::string	getNthEntry( size_t tgtEntry ) ;
};

//=============================================================================


//=============================================================================

public class AudioDevice
{
    public enum ParamPos        //TODO: From implementation in old app.  Determine if we really need it.
    {
        Pos_DeviceName  = 0,
        Pos_DeviceId    = 1,
        Pos_DeviceType  = 2,
        Pos_DeviceGuid  = 3,
        Pos_Default     = 4,
        Pos_Max         = 5
    };

    public AudioDevice()
    {
		Index		 = -1;
		IsDefault	 = false;
		IsUseDefault = false;
    }

//TODO?	AudioDevice(  StringList& data );
//TODO?    AudioDevice(  AudioDevice& audioDevice );

//TODO?    AudioDevice& operator= ( AudioDevice& audioDevice );

	public Boolean Equals( AudioDevice src )
	{
		Boolean result = false;

		if ( String.IsNullOrEmpty( Guid ) )
		{
			result = src.Name.Equals ( Name ) &&
					 src.Type.Equals ( Type ) &&
					 src.Index.Equals( Index );
		}
		else
		{
			result = src.Guid.Equals( Guid );
		}

		return result;
	}

    public Boolean isValid()
    { 
        return ( String.IsNullOrEmpty( Name ) );	
    }

	public String toString()
	{
		String result = "Guid: "			+ Guid 
					  + ", Index: "			+ Index 
					  + ", IsDefault: "		+ IsDefault 
					  + ", IsUseDefault: "	+ IsUseDefault 
					  + ", Name: "			+ Name 
					  + ", Type: "			+ Type
					  ;

		return result;
	}

//    StringList	getData() ;

//    static StringList convertData( StringList inputData );

//    static AudioDevice makeDefaultDevice( String useDefaultDeviceText, String type );
	static Int32         getDefaultDeviceIndex()        { return -1;    }


    //Properties ------------------------------------------------------------------
    public  String  Name	            { get; set;     }    //Required for unmanaged class
    public  String  Guid                { get; set;     }    //Required for unmanaged class
    public  String  Type                { get; set;     }    //Required for unmanaged class
    public  Int32   Index               { get; set;     }    //Required for unmanaged class
    public  Boolean IsDefault           { get; set;     }    //Required for unmanaged class
    public  Boolean IsUseDefault        { get; set;     }    //Required for unmanaged class	//Indicates we should use the default device, not that this *is* the default device.
};

//=============================================================================

public class AudioDeviceList : System.Collections.Generic.List<ManagedSipDataTypes.AudioDevice>
{
//	AudioDeviceList()		{}
//	~AudioDeviceList()		{}

    AudioDevice findBestMatch( AudioDevice tgtDevice )
    {
        AudioDevice result  = new AudioDevice();
        String      tgtName = tgtDevice.Name;

        foreach ( AudioDevice device in this )
        {
            String  tmpName = device.Name;
            Boolean match   = ( tmpName == tgtName );

            if ( !match )   //Try match on shortest length
            {
                if ( !String.IsNullOrEmpty( tmpName ) && !String.IsNullOrEmpty( tgtName ) )
                {
                    int minLen = tmpName.Length < tgtName.Length ? tmpName.Length : tgtName.Length;

                    match = tmpName.Substring(0, minLen ) == tgtName.Substring( 0, minLen );
                }
            }

            if ( match )
            {
                result = device;
                break;
            }
        }

        return result;
    }
};

//=============================================================================

//=============================================================================
//A small class to facilitate gathering of audio codec info.  Primarily used in getAudioCodecs()
public class AudioCodecInfo
{
	public AudioCodecInfo()
	{
		ComfortNoise    = false;
		TelephoneEvents = false;
		RedundantAudio  = false;
	}

	//Properties
	public String	Codecs							{ get; set;     }    //Required for unmanaged class
	public Boolean	ComfortNoise	                { get; set;     }    //Required for unmanaged class
	public Boolean	TelephoneEvents	                { get; set;     }    //Required for unmanaged class
	public Boolean	RedundantAudio	                { get; set;     }    //Required for unmanaged class
};

//=============================================================================

//=============================================================================
//  Codec class
//  - Much of this class supports allowing user to adjust codec preferences
//      within the UI.  We are not supporting that currently.
//=============================================================================

public class Codec
{
    //These are defined in native SIP stack.  Do not alter them.
	public enum CodecType          
	{
		Unknown		    = 0,
		Audio			= 1,
		Video			= 2,
		ComfortNoise	= 3,
		TelephoneEvent	= 4,
		RedundantAudio	= 5,
	};

	public Codec()
    {
        initVars();
    }

//	Codec(  std::string& codecStr );

	Boolean			isTypeAudio()							{ return Type == CodecType.Audio;	        }
	Boolean			isTypeComfortNoise()					{ return Type == CodecType.ComfortNoise;	}
	Boolean			isTypeTelephoneEvent()					{ return Type == CodecType.TelephoneEvent;	}
	Boolean			isTypeRedundantAudio()					{ return Type == CodecType.TelephoneEvent;	}

	void initVars()
    {
        Name          = "";
	    SampleRate    = 0;
	    ChannelCount  = 0;
	    Type          = CodecType.Unknown;
	    CanBeDisabled = false;
	    IsEnabled     = true;
	    Order         = 0;

    }

    //Boolean fromString( String val )
    //{
    //    Boolean result = false;
	
    //    //NOTE: if this changes, be sure to review Codecs::setEnabled()
    //    StringList temp = StringList.fromString( val, s_separator );	//Looks like ISAC/16000/1 or ISAC/16000/1/0

    //    Name         = temp[0];
    //    SampleRate   = temp[1];
    //    ChannelCount = temp[2];

    //    if ( temp.size() >= 4 )
    //    {
    //        IsEnabled = StringUtil::stringToBool( temp[3] );
    //    }

    //    determineInfoFromName();

    //    return result;
    //}

    String toString( Boolean includeChannelCount = true )
    {
	    String result = "";

	    result += Name;
	    result += s_separator;
	    result += SampleRate.ToString();

	    if ( includeChannelCount )
	    {
		    result += s_separator;
		    result += ChannelCount.ToString();
	    }

	    return result;
    }

	void determineInfoFromName()
    {
	    String    name		   = Name;
	    Boolean   canBeDisabled = true;
	    CodecType type		  = CodecType.Unknown;

	    if		( name == "CN" )
	    {
		    type = CodecType.ComfortNoise;
	    }
	    else if ( name == "telephone-event" )
	    {
		    type = CodecType.TelephoneEvent;
	    }
	    else if ( name == "red" )
	    {
		    type = CodecType.RedundantAudio;
	    }
	    else
	    {
		    type = CodecType.Audio;

		    if ( name == "PCMU" )
		    {
			    canBeDisabled = false;
		    }
		    else if ( name == "PCMA" )
		    {
			    canBeDisabled = false;
		    }
	    }

	    Type          = type;
	    CanBeDisabled = canBeDisabled;
    }


//	Codec& operator= (  Codec& src );
//	Boolean   operator==(  Codec& src );

    //Properties ------------------------------------------------------------------
	public  String      Name	                { get; set;     }    //Required for unmanaged class
	public  UInt32      SampleRate	            { get; set;     }    //Required for unmanaged class
	public  UInt32      ChannelCount	        { get; set;     }    //Required for unmanaged class
	public  CodecType	Type	                { get; set;     }    //Required for unmanaged class
	public  Boolean     CanBeDisabled	        { get; set;     }    //Required for unmanaged class
	public  Boolean     IsEnabled	            { get; set;     }    //Required for unmanaged class
	public  Int32		Order	                { get; set;     }    //Required for unmanaged class

    private static readonly  String s_separator = "/";
};

//=============================================================================


//=============================================================================
//  - Much of this class supports allowing user to adjust codec preferences
//      within the UI.  We are not supporting that currently.
public class CodecList : System.Collections.Generic.List<ManagedSipDataTypes.Codec>
{
//	CodecList()		{}
//	~CodecList()		{}

//    void add(  Codec& codec );
//    void fromDefaultCodecList();
//    void fromPreferredCodecList();

//    Boolean hasTypeAudio()			 							{ return hasType( Codec::Type_Audio          );	}
//    Boolean hasTypeVideo()			 							{ return hasType( Codec::Type_Video          );	}
//    Boolean hasTypeComfortNoise()	 							{ return hasType( Codec::Type_ComfortNoise   );	}
//    Boolean hasTypeTelephoneEvent() 							{ return hasType( Codec::Type_TelephoneEvent );	}
//    Boolean hasTypeRedundantAudio() 							{ return hasType( Codec::Type_RedundantAudio );	}

//    int getCountAudio()			 							{ return getCountByType( Codec::Type_Audio          );	}
//    int getCountVideo()			 							{ return getCountByType( Codec::Type_Video          );	}
//    int getCountComfortNoise()	 							{ return getCountByType( Codec::Type_ComfortNoise   );	}
//    int getCountTelephoneEvent() 							{ return getCountByType( Codec::Type_TelephoneEvent );	}
//    int getCountRedundantAudio() 							{ return getCountByType( Codec::Type_RedundantAudio );	}

//    StringList	getComfortNoiseAsStringList  ( Boolean enabled ) 			{ return getTypeAsStringList( Codec::Type_ComfortNoise,   enabled, true );	}
//    StringList	getTelephoneEventAsStringList( Boolean enabled ) 			{ return getTypeAsStringList( Codec::Type_TelephoneEvent, enabled, true );	}
//    StringList	getRedundantAudioAsStringList( Boolean enabled ) 			{ return getTypeAsStringList( Codec::Type_RedundantAudio, enabled, true );	}

//    StringList	toStringList( Boolean audioOnly, Boolean includeChannelCount ) ;

//    void		reorder   (  StringList& codecList );
//    void		setEnabled(  StringList& codecList );
	
//    static StringList  toStringListForSipWrapper(  StringList& codecList );

//private:
//    Boolean		setOrder(  std::string& codecStr, int order );

//    Boolean		hasType            ( Codec::Type type ) ;
//    int			getCountByType	   ( Codec::Type type ) ;
//    StringList	getTypeAsStringList( Codec::Type type, Boolean enabled, Boolean includeChannelCount = true ) ;
};

//=============================================================================


//=============================================================================

public class EnumDeviceType
{
	public enum DeviceType 
	{
		MasterVolume,	//MIXERLINE_COMPONENTTYPE_DST_SPEAKERS
		WaveOut,		//MIXERLINE_COMPONENTTYPE_SRC_WAVEOUT
		WaveIn,			//MIXERLINE_COMPONENTTYPE_DST_WAVEIN
		CDOut,			//MIXERLINE_COMPONENTTYPE_SRC_COMPACTDISC
		MicrophoneOut,	//MIXERLINE_COMPONENTTYPE_SRC_MICROPHONE
		MicrophoneIn	//MIXERLINE_COMPONENTTYPE_DST_WAVEIN + MIXERLINE_COMPONENTTYPE_SRC_MICROPHONE
	};

	public static String toString( DeviceType deviceType )
    {
        String result = "Unknown DeviceType";
        mDataDict.TryGetValue( deviceType, out result );
        return result;
    }

	public static DeviceType toDeviceType( String deviceType )
    {
        DeviceType result = DeviceType.CDOut;

        foreach ( KeyValuePair<DeviceType, String> kv in mDataDict )
        {
            if ( kv.Value == deviceType )
            {
                result = kv.Key;
                break;
            }
        }

        return result;
    }

    //Declare and initialize the Dictionary
    private static readonly System.Collections.Generic.Dictionary<DeviceType, String> mDataDict 
        = new Dictionary<DeviceType,string>()
        {
   		    { DeviceType.MasterVolume,  "DeviceTypeMasterVolume"  },
		    { DeviceType.WaveOut,	    "DeviceTypeWaveOut"       },
		    { DeviceType.WaveIn,		"DeviceTypeWaveIn"        },
		    { DeviceType.CDOut,		    "DeviceTypeCDOut"         },
		    { DeviceType.MicrophoneOut, "DeviceTypeMicrophoneOut" },
		    { DeviceType.MicrophoneIn,  "DeviceTypeMicrophoneIn"  }
       };
};

//=============================================================================

public class PhoneCallState
{
	public enum CallState 
	{
		Unknown		 =  0,		/** Unknown state. */
		Error		 =  1,		/** An error occured. */
		Resumed		 =  2,		/** Phone call resumed (after holding a call). */
		Talking		 =  3,		/** Conversation state. */
		Dialing		 =  4,		/** Outgoing phone call: dialing. */
		Ringing		 =  5,		/** Outgoing phone call: ringing. */
		Closed		 =  6,		/** Phone call closed (call rejected or call hang up). */
		Incoming     =  7,		/** Incoming phone call. */
		Hold		 =  8,		/** Phone call hold. */
		Missed		 =  9,		/** Phone call missed */
		Redirected	 = 10,		/** Phone call redirected */
		RingingStart = 11,
		RingingStop	 = 12,
	};

	public static String toString( CallState state )
    {
        String result = "Unknown DeviceType";
        mDataDict.TryGetValue( state, out result );
        return result;
    }
   
    //Declare and initialize the Dictionary
    private static readonly System.Collections.Generic.Dictionary<CallState, String> mDataDict 
        = new Dictionary<CallState,string>()
        {
   		    { CallState.Unknown,       "Unknown"       },
		    { CallState.Error,         "Error"         },
		    { CallState.Resumed,       "Resumed"       },
		    { CallState.Talking,       "Talking"       },
		    { CallState.Dialing,       "Dialing"       },
		    { CallState.Ringing,       "Ringing"       },
		    { CallState.Closed,        "Closed"        },
		    { CallState.Incoming,      "Incoming"      },
		    { CallState.Hold,          "Hold"          },
		    { CallState.Missed,        "Missed"        },
		    { CallState.Redirected,    "Redirected"    },
		    { CallState.RingingStart,  "Ringing Start" },
		    { CallState.RingingStop,   "Ringing Stop"  }
       };

};

//=============================================================================



//=============================================================================
//  Phone line states (server error, timeout, closed...).
//=============================================================================

public class PhoneLineState
{
	public enum LineState 
	{
		
		Unknown		= 0,	/** Unknown state. */
		Progress	= 1,	/** Progress state. */
		ServerError	= 2,	/** Connection to the SIP server failed. */
		AuthError	= 3,	/** Connection to the SIP server failed.due to an bad login or password */
		Timeout		= 4,	/** Connection to the SIP platform failed due to a timeout. */
		Ok			= 5,	/** Successfull connection to the SIP platform. */
		Closed		= 6		/** Line unregistered. */
	};

	public static String toString( LineState state )
    {
        String result = "Unknown PhoneLineState";
        mDataDict.TryGetValue( state, out result );
        return result;
    }

    //Declare and initialize the Dictionary
    private static readonly System.Collections.Generic.Dictionary<LineState, String> mDataDict 
        = new Dictionary<LineState, String>()
        {
   		    { LineState.Unknown,     "Unknown"       },
		    { LineState.Progress,    "Progress"      },
		    { LineState.ServerError, "Server Error"  },
		    { LineState.AuthError,   "Auth Error"    },
		    { LineState.Timeout,     "Dialing"       },
		    { LineState.Ok,          "Ringing"       },
		    { LineState.Closed,      "Closed"        },
       };
};

//=============================================================================


//=============================================================================
//Video not yet supported
//public class EnumVideoMode
//{
//    public enum VideoMode 
//    {
//        Nobody,
//        Contacts,
//        Anybody,
//    };

//    public static String    toString  ( VideoMode videoMode );
//    public static VideoMode fromString( String    videoMode );

//    private static void init();
//};

//=============================================================================


//=============================================================================
//Video not yet supported
//public class EnumVideoQuality
//{
//    public enum VideoQuality
//    {
//        VideoQualityNormal		= 0,	/** Down=0-512kbit/s up=0-128kbit/s. */
//        VideoQualityGood		= 1,	/** Down=512-2048kbit/s up=128-256kbit/s. */
//        VideoQualityVeryGood	= 2,	/** Down=+2048kbit/s up=+256kbit/s. */
//        VideoQualityExcellent	= 3		/** Down=+8192kbit/s up=+1024kbit/s. */
//    };

//    public static String        toString  ( VideoQuality videoQuality );
//    public static  VideoQuality fromString( String       videoQuality );

//    private static void init();
//};

//=============================================================================

    
//=============================================================================
// This class defines enums in the native ISipAgent interface
//=============================================================================

public class SipAgent
{ 
    //Call Encryption Enumerator
    public enum CallEncryption
    {
        ENCRYPTION_NONE = 0,    ///< SRTP should not be used.
        ENCRYPTION_OPTIONAL,    ///< SRTP may be used if supported by both parties.
        ENCRYPTION_REQUIRED     ///< SRTP must be used.
    };

    //TLS Method Enumerator - Specifies protocol methods supported by TLS/SSL connections
    public enum TLSMethod
    {
        TLS_SSLv23 = 0, ///< TLS/SSL connection will support SSLv2, SSLv3, and TLSv1 protocols
        TLS_SSLv2,      ///< TLS/SSL connection will only support SSLv2 protocols
        TLS_SSLv3,      ///< TLS/SSL connection will only support SSLv3 protocols
        TLS_TLSv1       ///< TLS/SSL connection will only support TLSv1 protocols
    };

    //HTTP Proxy Enumerator - pecifies HTTP Proxy type used to setup TCP connections
    public enum HTTPProxy
    {
        HTTPPROXY_NONE = 0, ///< HTTP Proxy is not used
        HTTPPROXY_AUTO,     ///< HTTP Proxy is automatically detected
        HTTPPROXY_SYSTEM,   ///< HTTP Proxy settings from system configuration are used
        HTTPPROXY_HTTP,     ///< HTTP Proxy
        HTTPPROXY_SOCKSv4,  ///< SOCKSv4 Proxy
        HTTPPROXY_SOCKSv5   ///< SOCKSv5 Proxy
    };


    //Function Result Enumerator - Specifies function result codes
    public enum Result
    {
        RESULT_OK = 0,          ///< Success Response
        RESULT_FAILURE,         ///< Unspecified Error
        RESULT_NOT_IMPLEMENTED, ///< Function or feature not implemented
        RESULT_OUT_OF_MEMORY,   ///< Out of memory
        RESULT_INVALID_ARGS,    ///< One or more arguments are invalid
        RESULT_BUFFER_OVERFLOW  ///< String buffer is too small
    };

    //Topology Discovery Method Enumerator - Specifies topology discovery method used for RTP/RTCP traffic
    public enum Topology
    {
        TOPOLOGY_NONE = 0,  ///< No topology discovery -- use local IP
        TOPOLOGY_ICE,       ///< Use ICE (RFC 5245)
        TOPOLOGY_STUN,      ///< Use STUN to determine local IP (RFC 3489/RFC 5780)
        TOPOLOGY_TURN       ///< Use TURN to proxy all RTP/RTCP traffic (RFC 5766)
    };

    //Topology Host Enumerator - Specifies topology host type in TopologyHostList
    public enum TopologyHost
    {
        TOPOLGY_HOST_STUN = 0,  ///< UDP STUN Server (RFC 3489/RFC 5780)
        TOPOLGY_HOST_TURN_UDP,  ///< UDP TURN Server (RFC 5766)
        TOPOLGY_HOST_TURN_TCP,  ///< TCP TURN Server (RFC 5766)
        TOPOLGY_HOST_TURN_TLS   ///< TLS TURNS Server (RFC 5766)
    };

    //Http Proxy -------------------------------------------------------------------------------------
    //Declare and initialize the Http Proxy Dictionary
    private static readonly System.Collections.Generic.Dictionary<HTTPProxy, String> mHttpProxyDict 
        = new Dictionary<HTTPProxy, String>()
        {
   		    { HTTPProxy.HTTPPROXY_NONE,    "none"    },
		    { HTTPProxy.HTTPPROXY_AUTO,    "auto"    },
		    { HTTPProxy.HTTPPROXY_SYSTEM,  "system"  },
		    { HTTPProxy.HTTPPROXY_HTTP,    "http"    },
		    { HTTPProxy.HTTPPROXY_SOCKSv4, "socks4"  },
		    { HTTPProxy.HTTPPROXY_SOCKSv5, "socks5"  },
       };

    public static HTTPProxy httpProxyFromString( String tgtString)
    {
        HTTPProxy result = HTTPProxy.HTTPPROXY_NONE;

        foreach ( KeyValuePair<HTTPProxy, String> kv in mHttpProxyDict )
        {
            if ( kv.Value == tgtString )
            {
                result = kv.Key;
                break;
            }
        }

        return result;
    }

    public static String httpProxyToString( HTTPProxy tgtProxy )
    {
        String result = "Unknown HttpProxy";
        mHttpProxyDict.TryGetValue( tgtProxy, out result );
        return result;
    }


    //Topology -------------------------------------------------------------------------------------
    //Declare and initialize the Topology Dictionary
    private static readonly System.Collections.Generic.Dictionary<Topology, String> mToplogyDict 
        = new Dictionary<Topology, String>()
        {
   		    { Topology.TOPOLOGY_NONE, "none" },
		    { Topology.TOPOLOGY_ICE,  "ice"  },
		    { Topology.TOPOLOGY_STUN, "stun" },
		    { Topology.TOPOLOGY_TURN, "turn" }
       };

    public static Topology topologyFromString( String tgtString)
    {
        Topology result = Topology.TOPOLOGY_NONE;

        foreach ( KeyValuePair<Topology, String> kv in mToplogyDict )
        {
            if ( kv.Value == tgtString )
            {
                result = kv.Key;
                break;
            }
        }

        return result;
    }

    public static String topologyToString( Topology tgtTopology )
    {
        String result = "Unknown Topology";
        mToplogyDict.TryGetValue( tgtTopology, out result );
        return result;
    }

    //Tls -------------------------------------------------------------------------------------
    //Declare and initialize the TLS Dictionary
    private static readonly System.Collections.Generic.Dictionary<TLSMethod, String> mTlsDict 
        = new Dictionary<TLSMethod, String>()
        {
   		    { TLSMethod.TLS_SSLv23, "sslv3"  },
		    { TLSMethod.TLS_SSLv2,  "sslv23" },
		    { TLSMethod.TLS_SSLv3,  "sslv2"  },
		    { TLSMethod.TLS_TLSv1,  "tlsv1"  }
       };

    public static TLSMethod tlsMethodFromString( String tgtString)
    {
        TLSMethod result = TLSMethod.TLS_SSLv2;

        foreach ( KeyValuePair<TLSMethod, String> kv in mTlsDict )
        {
            if ( kv.Value == tgtString )
            {
                result = kv.Key;
                break;
            }
        }

        return result;
    }

    public static String tlsMethodToString( TLSMethod tgtTlsMethod )
    {
        String result = "Unknown TLS method";
        mTlsDict.TryGetValue( tgtTlsMethod, out result );
        return result;
    }


}

//=============================================================================

public class ServerInfo
{
    //These values are defined in native SIP stack. DO NOT change them.
	public enum ServerType
	{
		None		 = 0,
		SipRegistrar = 1,
		SipProxy	 = 2,
		Stun		 = 3,
		TurnUdp	     = 4,
		TurnTcp	     = 5,
		TurnTls	     = 6,
		HttpProxy	 = 7,
		HttpTunnel	 = 8,
		HttpsTunnel  = 9,
	};

    //These values are defined in native SIP stack. DO NOT change them.
	public enum ServerOwner
	{
		None = 0,
		User = 1,
		App	 = 2,
	};

	public ServerInfo()
    {
		initVars();
    }

    //Properties ------------------------------------------------------------------
	public  String      Address                 { get; set;     }    //Required for unmanaged class
	public  UInt32      Port                    { get; set;     }    //Required for unmanaged class
	public  String      UserId                  { get; set;     }    //Required for unmanaged class
	public  String      Password                { get; set;     }    //Required for unmanaged class
	public  Boolean     UseSsl                  { get; set;     }    //Required for unmanaged class
	public  ServerType	Type                    { get; set;     }    //Required for unmanaged class
	public  ServerOwner	Owner                   { get; set;     }    //Required for unmanaged class

	public  SipAgent.HTTPProxy HttpProxyType    { get; set;     }    //Required for unmanaged class

//	public SipAgent.TopologyHost	getTopologyHost()   		{ return getTopologyHostFromType( Type );	}       //Used internally in native code

	public Boolean	isTypeNone()		 						{ return Type == ServerType.None;		    }
	public Boolean	isTypeSipRegistrar() 						{ return Type == ServerType.SipRegistrar;   }
	public Boolean	isTypeSipProxy()	 						{ return Type == ServerType.SipProxy;	    }
	public Boolean	isTypeStun()		 						{ return Type == ServerType.Stun;		    }
	public Boolean	isTypeTurnUdp()		 						{ return Type == ServerType.TurnUdp;		}
	public Boolean	isTypeTurnTcp()		 						{ return Type == ServerType.TurnTcp;		}
	public Boolean	isTypeTurnTls()		 						{ return Type == ServerType.TurnTls;		}
	public Boolean	isTypeHttpProxy()    						{ return Type == ServerType.HttpProxy;	    }
	public Boolean	isTypeHttpTunnel()   						{ return Type == ServerType.HttpTunnel;	    }
	public Boolean	isTypeHttpsTunnel()  						{ return Type == ServerType.HttpsTunnel;	}

	public Boolean	isTopologyHost()
    {
       	Boolean result = ( isTypeTurnUdp() || isTypeTurnTcp() || isTypeTurnTls() || isTypeStun() );
        return result;
    }

	//Sets ------------------------------------------------------------

	public void setTypeNone()		 							{ Type = ServerType.None;	        }
	public void setTypeSipRegistrar()	 						{ Type = ServerType.SipRegistrar;	}
	public void setTypeSipProxy()		 						{ Type = ServerType.SipProxy;	    }
	public void setTypeStun()		 							{ Type = ServerType.Stun;	        }
	public void setTypeTurnUdp()		 						{ Type = ServerType.TurnUdp;	    }
	public void setTypeTurnTcp()		 						{ Type = ServerType.TurnTcp;	    }
	public void setTypeTurnTls()		 						{ Type = ServerType.TurnTls;    	}
	public void setTypeHttpProxy() 								{ Type = ServerType.HttpProxy;	    }
	public void setTypeHttpTunnel()   							{ Type = ServerType.HttpTunnel; 	}
	public void setTypeHttpsTunnel()   							{ Type = ServerType.HttpsTunnel;	}

	public void setType( String type )					        { Type = typeFromString( type );	}

	//Misc ------------------------------------------------------------
	void initVars()
    {
	    Type          = ServerType.None;
	    Owner         = ServerOwner.None;
	    Address       = "";
	    Port          = 0;
	    UserId        = "";
	    Password      = "";
	    UseSsl        = false;
	    HttpProxyType = SipAgent.HTTPProxy.HTTPPROXY_NONE;
    }

//	ServerInfo&	operator= (  ServerInfo& src );
    public Boolean equals( ServerInfo src )
    {
        Boolean result = Address == src.Address &&
				         Port    == src.Port    &&
				         Type    == src.Type;

        return result;
    }

	public Boolean	isValid()
    {
	    Boolean result = false;

	    if ( !isTypeNone() )
	    {
		    result = (!String.IsNullOrEmpty( Address ) && ( Port != 0 ) );
	    }

	    return result;
    }

	public String getSipProtocol()
    {
       	String result = (UseSsl ? "sips:" : "sip:" );
        return result;
    }

	public String getUrlFormat()
    {
        String result = "";

	    result += getTypeText();
	    result += ":";
	    result += Address;
	    result += ":";
	    result += Port.ToString();

	    return result;
    }

	public String	getTypeText()
    { 
        return getTypeText( Type );	
    }


	public static ServerInfo.ServerType typeFromString( String tgtType )
    {
        ServerType result = ServerType.None;

        foreach ( KeyValuePair<ServerType, String> kv in mTypeDict )
        {
            if ( kv.Value == tgtType )
            {
                result = kv.Key;
                break;
            }
        }

        return result;
    }

	public static SipAgent.HTTPProxy proxyTypeFromString( String  type )
    {
        return SipAgent.httpProxyFromString( type );
    }

	public static String proxyTypeToString( SipAgent.HTTPProxy type )
    {
        return SipAgent.httpProxyToString( type );
    }

	public static SipAgent.Topology topologyFromString( String  topo )
    {
        return SipAgent.topologyFromString( topo );
    }

	public static String topologyToString( SipAgent.Topology topo )
    {
        return SipAgent.topologyToString( topo );
    }

//	public static SipAgent.TopologyHost getTopologyHostFromType( ServerInfo.ServerType type );  //Used only in native code

    private static String getTypeText( ServerType tgtType )
    {
        String result = "Unknown ServerType";
        mTypeDict.TryGetValue( tgtType, out result );
        return result;
    }

        //Declare and initialize the Dictionary
    //These values are defined in native SIP stack. DO NOT change them.
    private static readonly System.Collections.Generic.Dictionary<ServerType, String> mTypeDict 
        = new Dictionary<ServerType, String>()
        {
   		    { ServerType.None,          "none"          },
		    { ServerType.SipRegistrar,  "sip registrar" },
		    { ServerType.SipProxy,      "sip proxy"     },
		    { ServerType.Stun,          "stun"          },
		    { ServerType.TurnUdp,       "turn udp"      },
		    { ServerType.TurnTcp,       "turn tcp"      },
		    { ServerType.TurnTls,       "turns"         },
		    { ServerType.HttpProxy,     "http proxy"    },
		    { ServerType.HttpTunnel,    "http tunnel"   },
		    { ServerType.HttpsTunnel,   "https tunnel"  },
       };
};

//==============================================================================



//==============================================================================

public class ServerInfoList : System.Collections.Generic.List<ManagedSipDataTypes.ServerInfo>
{
	public void addSipProxy( String address, UInt32 serverPort, Boolean ssl = false )
    {
	    ServerInfo server = new ServerInfo();

	    server.Type    = ServerInfo.ServerType.SipProxy;
	    server.Address = address;
	    server.Port    = serverPort;
	    server.UseSsl  = ssl;

	    Add( server );
    }

	public void addSipRegistrar( String address, UInt32 port, Boolean ssl )
    {
	    ServerInfo server = new ServerInfo();

	    server.Type = ServerInfo.ServerType.SipRegistrar;
	    server.Address = address;
	    server.Port    = port;
	    server.UseSsl  = ssl;

	    Add( server );
    }

	public void addStun( String address, UInt32 port )
    {
	    ServerInfo server = new ServerInfo();

	    server.Type     = ServerInfo.ServerType.Stun;
	    server.Address  = address;
	    server.Port     = port;

	    Add( server );
    }

	public void addTurnUdp( String address, UInt32 port,  String login, String password )
    {
	    ServerInfo server = new ServerInfo();

	    server.Type = ServerInfo.ServerType.TurnUdp;
	    server.Address  = address;
	    server.Port     = port;
	    server.UserId   = login;
	    server.Password = password;

	    Add( server );
    }

	public void addTurnTcp( String address, UInt32 port, String login, String password )
    {
	    ServerInfo server = new ServerInfo();

	    server.Type = ServerInfo.ServerType.TurnTcp;
	    server.Address  = address;
	    server.Port     = port;
	    server.UserId   = login;
	    server.Password = password;

	    Add( server );
    }

	public void addTurnTls( String address, UInt32 port, String login, String password )
    {
	    ServerInfo server = new ServerInfo();

	    server.Type     = ServerInfo.ServerType.TurnTls;
	    server.Address  = address;
	    server.Port     = port;
	    server.UserId   = login;
	    server.Password = password;
	    server.UseSsl   = true;

	    Add( server );
    }

	public void addHttpProxy( String address, UInt32 port,  String login, String password, SipAgent.HTTPProxy httpProxyType )
    {
	    ServerInfo server = new ServerInfo();

	    server.Type          = ServerInfo.ServerType.HttpProxy;
	    server.Address       = address;
	    server.Port          = port;
	    server.UserId        = login;
	    server.Password      = password;
        server.HttpProxyType = httpProxyType;

	    Add( server );
    }

	public ServerInfo	getFirstSipRegistrar()					{ return getFirstByType( ServerInfo.ServerType.SipRegistrar	);	}
	public ServerInfo	getFirstSipProxy()						{ return getFirstByType( ServerInfo.ServerType.SipProxy		);	}
	public ServerInfo	getFirstStun()							{ return getFirstByType( ServerInfo.ServerType.Stun			);	}
	public ServerInfo	getFirstTurnUdp()						{ return getFirstByType( ServerInfo.ServerType.TurnUdp		);	}
	public ServerInfo	getFirstTurnTcp()						{ return getFirstByType( ServerInfo.ServerType.TurnTcp		);	}
	public ServerInfo	getFirstTurnTls()						{ return getFirstByType( ServerInfo.ServerType.TurnTls		);	}
	public ServerInfo	getFirstHttpProxy()						{ return getFirstByType( ServerInfo.ServerType.HttpProxy		);	}

	public Boolean	    hasSipRegistrar()					    { return getCountByType( ServerInfo.ServerType.SipRegistrar	) > 0;	}
	public Boolean	    hasSipProxy()						    { return getCountByType( ServerInfo.ServerType.SipProxy		) > 0;	}
	public Boolean		hasStun()							    { return getCountByType( ServerInfo.ServerType.Stun			) > 0;	}
	public Boolean		hasTurnUdp()						    { return getCountByType( ServerInfo.ServerType.TurnUdp		) > 0;	}
	public Boolean		hasTurnTcp()						    { return getCountByType( ServerInfo.ServerType.TurnTcp		) > 0;	}
	public Boolean		hasTurnTls()						    { return getCountByType( ServerInfo.ServerType.TurnTls		) > 0;	}
	public Boolean		hasHttpProxy()						    { return getCountByType( ServerInfo.ServerType.HttpProxy	) > 0;	}

	public Int32 merge ( ServerInfoList servers )
    {
        int result = 0;

	    foreach ( ServerInfo serverInfo in this )
	    {
		    if ( !exists( serverInfo ) )
		    {
			    Add( serverInfo );
			    result++;
		    }
	    }

	    return result;
    }

	public Boolean exists( ServerInfo src )
    {
        Boolean result  = false;

	    foreach ( ServerInfo si in this )
	    {
		    if ( si.equals( src ) )
		    {
			    result = true;
			    break;
		    }
	    }

	    return result;
    }


	public Boolean add( ServerInfo src )
    {
	    Boolean result  = false;
	    Boolean matched = false;

	    //If we match on type, server and port, then we replace.  Otherwise, add.
	    foreach ( ServerInfo serverInfo in this )
	    {
		    if ( serverInfo.isValid() )
		    {
			    if ( serverInfo.equals( src ) )
			    {
                    int index = this.IndexOf( serverInfo );
                    this[index] = src;
					result  = false;	//False indicates 'replace' rather than 'add'.
					matched = true;
			    }
		    }
	    }

	    if ( !matched )
	    {
		    Add( src );
		    result = true;
	    }

	    return result;
    }

    //Private methods
	private ServerInfo	getFirstByType ( ServerInfo.ServerType tgtType )
    {
	    ServerInfo result = new ServerInfo();

	    foreach ( ServerInfo serverInfo in this )
	    {
		    if ( serverInfo.Type == tgtType )
		    {
			    result = serverInfo;
			    break;
		    }
	    }

	    return result;
    }


//	private ServerInfo	getFirstByOwner ( ServerInfo.ServerOwner tgtOwner ) ;

//	private ServerInfo	getFirstByTypeAndOwner ( ServerInfo.ServerType tgtType, ServerInfo.ServerOwner tgtOwner ) ;

	//Helpers
	private Int32 getCountByType( ServerInfo.ServerType  tgtType )
    {
	    Int32 result = 0;

	    foreach ( ServerInfo serverInfo in this )
	    {
		    if ( serverInfo.Type == tgtType )
		    {
			    result++;
		    }
	    }

	    return result;
    }

//	private Int32 getCountByOwner       ( ServerInfo.ServerOwner tgtOwner ) ;
//  private Int32 getCountByTypeAndOwner( ServerInfo.ServerType  tgtType, ServerInfo.ServerOwner tgtOwner ) ;
};

//==============================================================================



//==============================================================================
//A simple class to contain all TLS related info for SipWrapper.
//==============================================================================

public class TlsInfo
{
	public TlsInfo()
    {
        initVars();
    }

    void initVars()
    {
	    Method	 = SipAgent.TLSMethod.TLS_SSLv23;
	    VerifyCertificate      = false;
	    VerifyDepth		       = 0;
	    RequireCertificate     = false;
	    PrivateKey		       = "";
	    PrivateKeyPassword	   = "";
	    CertificateAuthorities = "";
	    Certificate			   = "";
	    Crl					   = "";
	    Ciphers				   = "";

    }

//	TlsInfo 	operator= (  TlsInfo& src );
	public Boolean Equals( TlsInfo src )
	{
		Boolean result = false;

		if ( src.Method				== Method			 &&
			 src.VerifyCertificate	== VerifyCertificate &&
			 src.VerifyDepth		== VerifyDepth		 &&
			 src.RequireCertificate == RequireCertificate )
		{
			result = true;
		}

		return result;
	}

	static SipAgent.TLSMethod methodFromString( String tlsMethod )
    {
        return SipAgent.tlsMethodFromString( tlsMethod );
    }

	static String methodToString( SipAgent.TLSMethod tlsMethod )
    {
        return SipAgent.tlsMethodToString( tlsMethod );
    }


    //Properties ------------------------------------------------------------------
	public  SipAgent.TLSMethod Method                           { get; set;     }    //Required for unmanaged class

	public  Boolean		VerifyCertificate                       { get; set;     }    //Required for unmanaged class
	public  UInt32      VerifyDepth                             { get; set;     }    //Required for unmanaged class
	public  Boolean     RequireCertificate                      { get; set;     }    //Required for unmanaged class
	public  String      PrivateKey                              { get; set;     }    //Required for unmanaged class
	public  String      PrivateKeyPassword                      { get; set;     }    //Required for unmanaged class
	public  String      CertificateAuthorities                  { get; set;     }    //Required for unmanaged class
	public  String      Certificate                             { get; set;     }    //Required for unmanaged class
	public  String      Crl                                     { get; set;     }    //Required for unmanaged class
	public  String      Ciphers                                 { get; set;     }    //Required for unmanaged class
};

//==============================================================================


//=============================================================================
//Abstract class to be implemented by main app and ptr provided to SipAgent
//=============================================================================
abstract public class AppEventHandler
{
	public AppEventHandler()
    {
    }

	//Connect and Disconnect
	public abstract void connectedEvent   ();
	public abstract void disconnectedEvent( Boolean connectionError, String reason );

	//Line and Call callbacks from old SipWrapper
	public abstract void phoneCallStateChangedEvent( int callId, PhoneCallState.CallState state, String from, Int32 statusCode );
	public abstract void phoneLineStateChangedEvent( int lineId, PhoneLineState.LineState state );

	//Info Request
	public abstract void infoRequestReceivedEvent( int callId, String contentType, String content );

	//Logging
	public abstract void sendLog( LogEntry logEntry );

};

//=============================================================================


//=============================================================================
// SipEvent class
//	- Event type and related data for SIP events handled in UI layer.
//=============================================================================

public class SipEventData
{
	public enum EventType	//Which type of event occurred.  Allow listeners to select event(s) they care about.
	{
		Unknown			= 0,	//So we can test that all events set type properly
		Connect			= 1,
		Disconnect		= 2,
		CallStateChange = 3,
		LineStateChange = 4,
		InfoRequest		= 5,
		Log				= 6
	};

	public SipEventData()
	{
		Event			= EventType.Unknown;
	
		ConnectionError	= false;
		DisconnectMsg	= "";

		LineId			= -1;		
		LineState		= PhoneLineState.LineState.Unknown;

		CallId			= -1;
		CallState		= PhoneCallState.CallState.Unknown;	
		CallFrom		= "";
		CallStatusCode	= 0;

		ContentType		= "";
		Content			= "";
	}

	//Properties ---------------------------------------------------------------------
	public EventType				Event		                { get;  set;    }     //Required for unmanaged class

	//Connect event has no extras

	//Disconnect Event extras
	public	Boolean					ConnectionError				{ get;  set;    }     //Required for unmanaged class
	public	String					DisconnectMsg				{ get;  set;    }     //Required for unmanaged class

	//Line State changed event extras
	public Int32					LineId						{ get;  set;    }     //Required for unmanaged class
	public PhoneLineState.LineState	LineState					{ get;  set;    }     //Required for unmanaged class

	//Call State changed event extras
	public Int32					CallId						{ get;  set;    }     //Required for unmanaged class
	public PhoneCallState.CallState	CallState					{ get;  set;    }     //Required for unmanaged class
	public String					CallFrom					{ get;  set;    }     //Required for unmanaged class
	public Int32					CallStatusCode				{ get;  set;    }     //Required for unmanaged class

	//Info Request event extras
	public String					ContentType					{ get;  set;    }     //Required for unmanaged class
	public String					Content						{ get;  set;    }     //Required for unmanaged class

	//Log Event extras
	public LogEntry					LogEntry					{ get;  set;    }     //Required for unmanaged class

    //Methods ---------------------------------------------------------------------------------------------
	public Boolean		    	isConnectEvent()				{ return Event == EventType.Connect;		 }
	public Boolean	    		isDisconnectEvent()				{ return Event == EventType.Disconnect;		 }
	public Boolean				isLineStateChangeEvent()		{ return Event == EventType.LineStateChange; }
	public Boolean				isCallStateChangeEvent()		{ return Event == EventType.CallStateChange; }
	public Boolean				isInfoRequestEvent()			{ return Event == EventType.InfoRequest;	 }
	public Boolean				isLogEvent()					{ return Event == EventType.Log;			 }
	public String				typeAsString()
	{
		string result = "Unknown";

		switch ( Event )
		{
		case EventType.Unknown:
			result = "Unknown";
			break;
		case EventType.Connect:
			result = "Connect";
			break;
		case EventType.Disconnect:
			result = "Disconnect";
			break;
		case EventType.CallStateChange:
			result = "CallStateChange";
			break;
		case EventType.LineStateChange:
			result = "LineStateChange";
			break;
		case EventType.InfoRequest:
			result = "InfoRequest";
			break;
		case EventType.Log:
			result = "Log";
			break;
		default:
			result = "Unexpected: " + Convert.ToString( (int)Event );
			break;

		}

		return result;
	}
};

//=============================================================================

}   //namespace Telcentris.Voxox.ManagedSipAgentApi.ManagedSipDataTypes


