﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Sep 2014
 */

using System;
using System.Diagnostics;		//For Debug

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Telcentris.Voxox.ManagedSipAgentApi.ManagedSipDataTypes;
using Telcentris.Voxox.ManagedSipAgentApi;

//-------------------------------------------------------------------------------------------------
//We currently have 101 public methods to test, in these categories
//	- SIP stack setup / breakdown:	 7
//	- SIP callback handlers:		 5	- See AppEventHandler class above
//	- SIP phone line:				 3
//	- SIP call:						13
//	- Sip Configuration set/get:	73	- These are in several methods, depending on when they are typically set.
//
//This will leave xxx methods to be tested separately
//-------------------------------------------------------------------------------------------------

//#define USE_TLS 1			//TODO: To test TLS, we must initialize OpenSSL.  Defer that for now.

namespace ManagedSipAgentApi.Test
{

public class TestSipEventHandler
{ 
	public TestSipEventHandler( String name ) 
	{
		mName = name;
	}

	public void HandleSipEvent( Object sender, SipEventArgs e )		//Same signature as SipEventHandler defined in ManagedSipAgentApi.h
	{
		Console.WriteLine( "In HandleSipEvent() for instance name: " + mName );

		if ( e.Data.isConnectEvent() )
		{
			Console.WriteLine( "  SipEvent: Connect" );
		}
		else if ( e.Data.isDisconnectEvent() )
		{
			Console.WriteLine( "  SipEvent: Disconnect" );
		}
		else if ( e.Data.isLineStateChangeEvent() )
		{
			Console.WriteLine( "  SipEvent: Line State Change" );
		}
		else if ( e.Data.isCallStateChangeEvent() )
		{
			Console.WriteLine( "  SipEvent: Call State Change" );
		}
		else if ( e.Data.isInfoRequestEvent() )
		{
			Console.WriteLine( "  SipEvent: Info Request" );
		}
		else if ( e.Data.isLogEvent() )
		{
			String msg = ">>>>> " + e.Data.LogEntry.Message;
			
			Debug.WriteLine(  msg );
		}
		else
		{
			int xxx = 1;		//New event type?
			xxx++;
		}
	}

	public String getName()
	{
		return mName;
	}

	private String mName;
};


[TestClass]
public class ManagedSipAgentApiUnitTest
{
	static ServerInfoList		s_servers = new ServerInfoList();

//	String	mSipAgentName = "Voxox-3.x.x.xxxx";	//Not used yet, but we need a way to add SipAgentName

	//These are used by some servers for credentials
//	String	mSipUserId    = "19505005170";	//@voxox.com";
//	String	mSipPassword  = "knxgj2307b6ow81y";
	String	mSipUserId    = "19501010555";	//@voxox.com";
	String	mSipPassword  = "bilx6dca2zph371k";

	//These items come from SSO, but are stored in separate SipAccount class until they are needed.
	//	Primarily used for addVirtualLine().
	String	mUserName	 = "19505005170";		//I am not sure of what this is supposed to be.
	String	mDisplayName = "jrtheinert";		//This is what user entered on login.  It is NOT case-sensitive, so you may see different capitalization.
	String	mRealm		 = "voxox.com";

//	String	mSipServer    = "vsip.voxox.com";			//From SSO login.
	String	mSipServer    = "tcsbuslab06.voxox.com";	//From new VxApi23 auth
	String	mStunServer   = "voxox.com";
	String	mTurnServer   = "turn.voxox.com";	
	String	mTurnUserId	  = "angora";
	String	mTurnPassword = "JNMBckSreQ";


	//App-level values
	String	mSipUserUuid = "XXXX-3333-4444-5555-AAAA";

	//These SIP configuration values came from old desktop app SSO 
	SipAgent.Topology mTopology = SipAgent.Topology.TOPOLOGY_ICE;

	Boolean mEnableIceMedia		= false;
	Boolean mEnableIceSecurity	= true;
	Boolean mTopologyEncryption = true;
	Boolean mTopologyTurn		= true;

#if USE_TLS
	Boolean mSipUdp = false;
	Boolean mSipTcp = false;
	Boolean mSipTls = true;
#else
	Boolean mSipUdp = true;
	Boolean mSipTcp = true;
	Boolean mSipTls = false;
#endif

	Boolean	mSipKeepAlive				 = true;
	Boolean	mSipUseRport				 = true;
	Int32	mRegistrationRefreshInterval =    0;
	Int32	mFailureInterval			 =    0;
	Int32	mOptionKeepAlive			 =   30;
	Int32	mMinimumSessionTime			 = 1800;
	Boolean	mSupportSessionTimer		 = true;
	Boolean	mInitiateSessionTimer		 = false;
	String	mTagProlog					 = "voxox";
	UInt32  mRegisterTimeout			 = 2940;		//In seconds
	UInt32  mPublishTimeout				 =  300;		//In seconds
	Boolean	mUseOptionsRequest			 = true;
	Boolean	mOptionsP2PPresence			 = false;
	Boolean	mOptionsUseTypingState		 = true;
	Boolean	mChatWithoutPresence		 = false;
	//End SSO configuration options.

	//PhoneLine configuration options.  These are typically set with default values,
	//	but application may allow user to change them.
	Boolean mAec				= true;
	Boolean mAgc				= true;
	Boolean mNoiseSuppression	= true;
	SipAgent.CallEncryption mCallEncryption = SipAgent.CallEncryption.ENCRYPTION_NONE;
	//End PhoneLine configuration options.

	SipAgentApi	mSipAgentApi = null;
	Int32				mLineId      = -1;				//LineId from most recent registration.

	SipAgentApi getApi()
	{
		if ( mSipAgentApi == null )
		{
			mSipAgentApi  = SipAgentApi.getInstance();
		}

		return mSipAgentApi;
	}

	void initApi()
	{
		getApi();

		if ( !mSipAgentApi.isInitialized() )
		{
			mSipAgentApi.init( -1 );
			addSipEventHandlers();
		}
	}

	void terminateApi()
	{
		getApi().terminate();
		mSipAgentApi = null;
	}

    void addSipEventHandlers()
    {
	    TestSipEventHandler test1 = new TestSipEventHandler( "Jeff" );
	    SipEventHandler sipDel1 = (SipEventHandler)System.Delegate.CreateDelegate( typeof(SipEventHandler), test1, "HandleSipEvent" );
	    getApi().addSipEventListener( sipDel1 );

		//Uncomment to test with multiple listeners to demonstrate it works.
//	    TestSipEventHandler test2 = new TestSipEventHandler( "Pam" );
//	    SipEventHandler sipDel2 = (SipEventHandler)System.Delegate.CreateDelegate( typeof(SipEventHandler), test2, "HandleSipEvent" );
//	    getApi().addSipEventListener( sipDel2 );
    }

	ServerInfoList getServers()
	{
		if ( s_servers.Count == 0 )
		{
			//These are values for old desktop app SSO.
			//Not using HTTP Tunnel for now
#if USE_TLS
			s_servers.addSipRegistrar( mSipServer, 5061, true );	//TODO: Some discussion of using port 443 here.  Check with Roman if that does not work cleanly.
			s_servers.addSipProxy    ( mSipServer, 5061, true );
#else
			s_servers.addSipRegistrar( mSipServer, 5060, false );
			s_servers.addSipProxy    ( mSipServer, 5060, false );
#endif
			s_servers.addStun        ( mStunServer,      5050 );
			s_servers.addTurnUdp     ( mTurnServer,		 3478, mTurnUserId, mTurnPassword );
			s_servers.addTurnTcp     ( mTurnServer,		   80, mTurnUserId, mTurnPassword );
			s_servers.addTurnTls     ( mTurnServer,		  443, mTurnUserId, mTurnPassword );
		}

		return s_servers;
	}

	TlsInfo getTlsInfo()
	{
		TlsInfo tlsInfo = new TlsInfo();

#if USE_TLS
		tlsInfo.Method( SipAgent.TLSMethod.TLS_SSLv23 );
#else
		tlsInfo.Method = SipAgent.TLSMethod.TLS_SSLv2;
#endif
		tlsInfo.VerifyCertificate  = false;		//Change to true after we have CA file
		tlsInfo.RequireCertificate = false;		//Change to true after we have CA file
		tlsInfo.VerifyDepth		   = 9;
		//Remaining values are empty string by default.

		return tlsInfo;
	}

	ServerInfo getSipProxyServer()
	{
		return getServers().getFirstSipProxy();
	}

	ServerInfo getSipRegistrarServer()
	{
		return getServers().getFirstSipRegistrar();
	}

	//This method does app-level configuration.  It should be called by some test methods to get proper setup
	void doAppConfiguration()
	{
		//Called from app initialization.  Uses stored UUID or creates a new one if one does not exist.
		getApi().setSipOptionUuid  ( mSipUserUuid );
	}

	//This method does configuration which would normally be done during SSO is old app.
	//	It will need to be called by some test methods to get proper setup.
	//	These values are also used Int32 testSsoConfigurationSetsAndGets().
	void doSsoConfiguration()
	{
		ServerInfoList serversIn = getServers();

		getApi().addServers( serversIn );			//Bulk add servers, typically from SSO.

//		void addTunnel(  String address, unsigned Int32 port, Boolean ssl );	//Not currently called anywhere in native code.
		//No matching get

//		void addSipProxy(  String server, unsigned Int32 serverPort );	//Tested above
		//No matching get

		TlsInfo tlsInfoIn = getTlsInfo();

		getApi().setTlsInfo( tlsInfoIn );

		//Discovery Topology
		getApi().setDiscoveryTopology			( mTopology );
		getApi().enableIceMedia					( mEnableIceMedia );
		getApi().enableIceSecurity				( mEnableIceSecurity );
		getApi().setTopologyEncryption			( mTopologyEncryption );
		getApi().setTopologyTurn				( mTopologyTurn );
		getApi().setSipUdp						( mSipUdp );
		getApi().setSipTcp						( mSipTcp );
		getApi().setSipTls						( mSipTls );
		getApi().setKeepAlive					( mSipKeepAlive );
		getApi().setUseRport					( mSipUseRport );
		getApi().setRegistrationRefreshInterval	( mRegistrationRefreshInterval );
		getApi().setFailureInterval				( mFailureInterval );
		getApi().setOptionsKeepAliveInterval	( mOptionKeepAlive );
		getApi().setMinSessionTime				( mMinimumSessionTime );
		getApi().setSupportSessionTimer			( mSupportSessionTimer );
		getApi().setInitiateSessionTimer		( mInitiateSessionTimer );
		getApi().setTagProlog					( mTagProlog );
		getApi().setSipOptionRegisterTimeout	( mRegisterTimeout );
		getApi().setSipOptionPublishTimeout		( mPublishTimeout );
		getApi().setSipOptionUseOptionsRequest	( mUseOptionsRequest );
		getApi().setSipOptionP2pPresence		( mOptionsP2PPresence );
		getApi().setSipOptionUseTypingState		( mOptionsUseTypingState );
		getApi().setSipOptionChatWithoutPresence( mChatWithoutPresence );

		//Not called in old desktop app.  Do we need this?
//		void		 setSipOptionFailureTimeout     ( unsigned Int32 value );		//In seconds
//		unsigned Int32 getSipOptionFailureTimeout     () 
	}
	//-------------------------------------------------------------------------
	//This method tests values set during phone line configuration.
	//	In old desktop app, many of these values came from Config file,
	//	but that does NOT mean they were all user configureable.
	//-------------------------------------------------------------------------
	void doPhoneLineConfiguration()
	{
		//We are NOT supporting HTTP proxy server in new client, at least in phase 1.
//		void addProxy(  String address, unsigned Int32 port,  String login,  String  password, SipAgent.HTTPProxy httpProxyType );
		//No matching get

		getApi().enableAEC( mAec );
		getApi().enableAGC( mAgc );
		getApi().enableNoiseSuppression( mNoiseSuppression );
		getApi().setCallsEncryption    ( mCallEncryption );

		//UI would typically set user selections based on persisted values.  
		//	We will NOT do that here, but have other tests for these.
//		StringList codecs = Codecs::toStringListForSipWrapper( config.getAudioCodecList() );	//Adjust format for SIP wrapper
//		_sipWrapper->setAudioCodecs   ( codecs, config.getEnableComfortNoise(), config.getEnableTelephoneEvent(), config.getEnableRedundantAudio() );

		//Setting audio devices
//		_sipWrapper->setCallOutputAudioDevice  ( AudioDevice(config.getAudioOutputDevice() ) );
//		_sipWrapper->setCallInputAudioDevice   ( AudioDevice(config.getAudioInputDevice()  ) );
//		_sipWrapper->setRingerOutputAudioDevice( AudioDevice(config.getAudioRingerDevice() ) );
	}

	void doRegistration()
	{
		//These are used to verify API call fails if servers are invalid.
//		ServerInfo proxyServer	   = new ServerInfo();
//		ServerInfo registrarServer = new ServerInfo();
//		ServerInfo registrarServer = new ServerInfo();
//		registrarServer.Address = mSipServer;
//		registrarServer.Port	= 5060;
//		registrarServer.UseSsl  = false;


		//These use valid server info.
		ServerInfo proxyServer     = getSipProxyServer();
		ServerInfo registrarServer = getSipRegistrarServer();

		//This registers with SIP server.
		getApi().setCredentials( mDisplayName, mUserName, mSipUserId, mSipPassword, mRealm );
		mLineId = getApi().addVirtualLine();

		System.Threading.Thread.Sleep( 2000 );	//Wait for SIP stack events, if any.

		if ( mLineId >= 0 )
		{
			// Registers a given phone line.
			//	LineId comes from addVirtualLine()
			//  0 == valid result.
			Int32 result = getApi().registerVirtualLine( mLineId );
		}
	}



	[TestMethod]
	public void testInitialization ()
	{
		//Instantiation
		SipAgentApi api = getApi();

		Assert.IsTrue( api != null, "API not properly instantiated." );

		//Test initialization and isInitialized() methods
		api.init( -1 );
		Boolean isInitialized = api.isInitialized();

		Assert.IsTrue( isInitialized, "API not initialized." );

		api.terminate();

		Assert.IsFalse( api.isInitialized(), "API is still initialized after terminate() call." );
	}


	//------------------------------------------------------------------------------------------------
	//Begin line level tests
	//	I am not sure how we do this in unit tests
	//------------------------------------------------------------------------------------------------
	[TestMethod]
	public void testLineMethods()
	{
		initApi();
		doAppConfiguration();
		doSsoConfiguration();
		doPhoneLineConfiguration();

		ServerInfo proxyServer     = getSipProxyServer();
		ServerInfo registrarServer = getSipRegistrarServer();

		//This registers with SIP server.
		getApi().setCredentials( mDisplayName, mUserName, mSipUserId, mSipPassword, mRealm);
		Int32 lineId = getApi().addVirtualLine();

		System.Threading.Thread.Sleep( 2000 );	//Wait for SIP stack events, if any.

		if ( lineId >= 0 )
		{
			// Registers a given phone line.
			//	LineId comes from addVirtualLine()
			//  0 == valid result.
			Int32 result = getApi().registerVirtualLine( lineId );

			System.Threading.Thread.Sleep( 2000 );	//Wait for SIP stack events, if any.

			if ( result == 0 )
			{
				Int32 lineCount1 = getApi().getLineCount();
				Assert.AreEqual( lineCount1, 1, "Wrong line count after registerVirtualLine()." );

				// Remove a given phone line, previously registered.
				//	- force if true, forces the removal without waiting for the unregister
				getApi().removeVirtualLine( lineId, false );
				Int32 lineCount2 = getApi().getLineCount();
				Assert.AreEqual( lineCount2, 0, "Wrong line count after removeVirtualLine()." );
	
				System.Threading.Thread.Sleep( 2000 );	//Wait for SIP stack events, if any.
			}
			else
			{
				Assert.Fail( "registerVirtualLine() failed." );
			}
		}
		else
		{
			Assert.Fail( "LineId is isvalid." );
		}

		terminateApi();	
	}


	//------------------------------------------------------------------------------------------------
	//Begin call level tests
	//	These all require a callId which means we need to start a call and have it answered
	//	I am not sure how we do this in unit tests.
	//	Seems more like integration testing.
	//
	//	Using code below I made a successful call with two-way audio!
	//------------------------------------------------------------------------------------------------
	[TestMethod]
	public void testCallOptions()
	{
		initApi();
		doAppConfiguration();
		doSsoConfiguration();
		doPhoneLineConfiguration();
		doRegistration();

		System.Threading.Thread.Sleep( 5000 );		//Testing.  Wait for remote end to answer.  Looking for hits in AppCallback object.

		// Dials a phone number.
		//	sipAddress SIP address to call (e.g phone number to dial)
		String	number   = "17602775395";	//Please enter your OWN test number here.  Do NOT call me.  ;-)
		Boolean	useVideo = false;	//Not supported
		Int32	callId   = getApi().makeCall( mLineId, number, useVideo );
		String  sipCallId = "";

		System.Threading.Thread.Sleep( 10000 );		//Testing.  Wait for remote end to answer.  Looking for hits in AppCallback object.

		if ( callId >= 0 )
		{
			sipCallId = getApi().getSipCallId( callId );
		}

		System.Threading.Thread.Sleep( 20000 );		//Testing.  Wait for remote end to answer.  Looking for hits in AppCallback object.

		terminateApi();	//Move this when testing items below.
	}




//		// Notifies the remote side (the caller) that this phone is ringing.
//		//	This corresponds to the SIP code "180 Ringing".
//		void sendRingingNotification( Int32 callId, Boolean enableVideo );
//
//		// Accepts a given phone call.
//		void acceptCall( Int32 callId, Boolean enableVideo );
//
//		// Rejects a given phone call.
//		void rejectCall(Int32 callId);
//
//		//Closes a given phone call.
//		void closeCall(Int32 callId);
//
//		// Holds a given phone call.
//		void holdCall(Int32 callId);
//
//		// Resumes a given phone call.
//		void resumeCall(Int32 callId);
//
//		// Blind transfer the specified call to another party.
//		void blindTransfer(Int32 callId,  String sipAddress);
//
//		// Sends a DTMF to a given phone call.
//		void playDtmf( Int32 callId, char dtmf );
//
//		 //Sends and plays a sound file to a given phone call.
//		void playSoundFile( Int32 callId,  String soundFile );
//
//		// Gets the audio codec in use by a given phone call.
//		String getAudioCodecUsed(Int32 callId);
//
//		// Gets the video codec in use by a given phone call.
////		String getVideoCodecUsed(Int32 callId);		//Video not currently supported
//
//		//True if encrytion is activated for the given call
//		Boolean isCallEncrypted(Int32 callId);
//
//		SipAgent.CallCallbacks* newCall( Int32 lineID, SipAgent.Call* call ) throw();
//	}


//		//Registration and notification methods
//		SipAgent.SubscriptionCallbacks* newSubscription( Int32 lineId, SipAgent.ServerSubscription* subscription ) throw();
//
//		void serverSubStateChanged( Int32 sid, SipAgent.State state ) throw();
//
//		void message(  char* from,  char* content, size_t contentSize,  char* contentType ) throw();
//
//		void notification(  char* eventIn,  char* content, size_t contentSize,  char* contentType ) throw();
//
//		void registrationChanged( Int32 lineId, SipAgent.State state, SipAgent.Event regEvent ) throw();
//
//		Boolean isRegistered();
//
//		Int32 getPresenceLine();
//
//	//---------------------------------------------------------------------------------
//	//Call audio related methods
//	//---------------------------------------------------------------------------------
//	[TestMethod]
//	public void testAudio() 
//	{
//		//Output
//		Boolean setSpeakerVolume( unsigned Int32 volume );		// Sets the speaker |volume| level. Valid range is [0,255].
//		Int32  getSpeakerVolume();								// Gets the speaker |volume| level.
//
//		//Ringing
//		Boolean setRingingVolume( unsigned Int32 volume );		// Sets the ringing |volume| level. Valid range is [0,255].
//		Int32  getRingingVolume();								// Gets the ringing |volume| level.
//
//		Boolean setSystemRingingMute( Boolean enable );			// Mutes the ringing device completely in the operating system.
//		Boolean getSystemRingingMute();							// Gets the ringing device mute state in the operating system.
//
//		//Input
//		Boolean setMicVolume( unsigned Int32 volume );			// Sets the microphone volume level. Valid range is [0,255].
//		Int32  getMicVolume();									// Gets the microphone volume level.
//
//		Int32  getSpeechInputLevelFullRange();					// Gets the microphone speech |level|, mapped linearly to the range [0,32768].
//	}

	//---------------------------------------------------------------------------------
	//Start SIP configuration set/get test
	//	- I am testing default settings based on old desktop SIP configuration items from SSO
	//---------------------------------------------------------------------------------
	[TestMethod]
	public void testSsoConfigurationSetsAndGets()
	{
		initApi();
		doAppConfiguration();

		//-----------------------------------------------------------------------
		//This group gets values from SSO in old desktop.
		//	The values here are from that SSO data
		//-----------------------------------------------------------------------

		//Not using HTTP Tunnel for phase 1.
		ServerInfoList serversIn = getServers();

		getApi().addServers( serversIn );			//Bulk add servers, typically from SSO.
		ServerInfoList serversOut = getApi().getServers();
		//TODO: Verify all serversIn are in serversOut.  There may be more in serversOut;

//		void addTunnel(  String address, unsigned Int32 port, Boolean ssl );	//Not currently called anywhere in native code
		//No matching get

//		void addSipProxy(  String server, unsigned Int32 serverPort );	//Tested above
		//No matching get

		TlsInfo tlsInfoIn = getTlsInfo();

		getApi().setTlsInfo( tlsInfoIn );
		TlsInfo tlsInfoOut = getApi().getTlsInfo();
		Boolean tlsInfoEqual = tlsInfoIn.Equals( tlsInfoOut );
		Assert.IsTrue( tlsInfoEqual, "TlsInfo mismatch." );

		//TODO-PJSIP: Change how we do these unit tests
		//Discovery Topology
		//getApi().setDiscoveryTopology( mTopology );
		//SipAgent.Topology topoOut = getApi().getDiscoveryTopology();
		//Assert.AreEqual( (Int32)mTopology, (Int32)topoOut, "Discovery Topology mismatch." );

		//getApi().enableIceMedia( mEnableIceMedia );
		//Boolean enableIceMediaOut = getApi().getEnableIceMedia();
		//Assert.AreEqual( mEnableIceMedia, enableIceMediaOut, "Discovery Topology EnableProxyMedia mismatch." );

		//getApi().enableIceSecurity( mEnableIceSecurity );
		//Boolean enableIceSecurityOut = getApi().getEnableIceSecurity();
		//Assert.AreEqual( mEnableIceSecurity, enableIceSecurityOut, "Discovery Topology EnablSecurity mismatch." );

		//getApi().setTopologyEncryption( mTopologyEncryption );
		//Boolean topoEncryptionOut = getApi().getTopologyEncryption();
		//Assert.AreEqual( mTopologyEncryption, topoEncryptionOut, "Discovery Topology Encryption mismatch." );
		
		//getApi().setTopologyTurn( mTopologyTurn );
		//Boolean topoTurnOut = getApi().getTopologyTurn();
		//Assert.AreEqual( mTopologyTurn, topoTurnOut, "Discovery Topology Turn mismatch." );

		//getApi().setSipUdp( mSipUdp );
		//Boolean sipUpdOut = getApi().getSipUdp();
		//Assert.AreEqual( mSipUdp, sipUpdOut, "SIP UDP mismatch." );

		//getApi().setSipTcp( mSipTcp );
		//Boolean sipTcpOut = getApi().getSipTcp();
		//Assert.AreEqual( mSipTcp, sipTcpOut, "SIP TCP mismatch." );

		//getApi().setSipTls( mSipTls );
		//Boolean sipTlsOut = getApi().getSipTls();
		//Assert.AreEqual( mSipTls, sipTlsOut, "SIP TLS mismatch." );

		//getApi().setKeepAlive( mSipKeepAlive );
		//Boolean sipKeepAliveOut = getApi().getKeepAlive();
		//Assert.AreEqual( mSipKeepAlive, sipKeepAliveOut, "SIP KeepAlive mismatch." );

		//getApi().setUseRport( mSipUseRport );
		//Boolean sipUseRportOut = getApi().getUseRport();
		//Assert.AreEqual( mSipUseRport, sipUseRportOut, "SIP UseRport mismatch." );
		
		//getApi().setRegistrationRefreshInterval( mRegistrationRefreshInterval );
		//Int32 regRefreshIntervalOut = getApi().getRegistrationRefreshInterval();
		//Assert.AreEqual( mRegistrationRefreshInterval, regRefreshIntervalOut, "SIP Reg. Refresh Interval mismatch." );

		//getApi().setFailureInterval( mFailureInterval );
		//Int32 failureIntervalOut = getApi().getFailureInterval();
		//Assert.AreEqual( mFailureInterval, failureIntervalOut, "SIP Failure Interval mismatch." );

		//getApi().setOptionsKeepAliveInterval( mOptionKeepAlive );
		//Int32 optionKeepAliveOut = getApi().getOptionsKeepAliveInterval();
		//Assert.AreEqual( mOptionKeepAlive, optionKeepAliveOut, "SIP Options Keep Alive Internal mismatch." );

		//getApi().setMinSessionTime( mMinimumSessionTime );
		//Int32 minSessionTimeOut = getApi().getMinSessionTime();
		//Assert.AreEqual( mMinimumSessionTime, minSessionTimeOut, "SIP Min. Session Time mismatch." );

		//getApi().setSupportSessionTimer( mSupportSessionTimer );
		//Boolean supportSessionTimerOut = getApi().getSupportSessionTimer();
		//Assert.AreEqual( mSupportSessionTimer, supportSessionTimerOut, "SIP Support Session Timer mismatch." );

		//getApi().setInitiateSessionTimer( mInitiateSessionTimer );
		//Boolean initiateSessionTimerOut = getApi().getInitiateSessionTimer();
		//Assert.AreEqual( mInitiateSessionTimer, initiateSessionTimerOut, "SIP Initiate Session Timer mismatch." );

		//getApi().setTagProlog( mTagProlog );
		//String tagPrologOut = getApi().getTagProlog();
		//Assert.AreEqual( mTagProlog, tagPrologOut, "SIP TagProlog mismatch." );

		//getApi().setSipOptionRegisterTimeout( mRegisterTimeout );
		//UInt32 registerTimeoutOut = getApi().getSipOptionRegisterTimeout();
		//Assert.AreEqual( mRegisterTimeout, registerTimeoutOut, "SIP Option Register Timeout mismatch." );

		//getApi().setSipOptionPublishTimeout( mPublishTimeout );
		//UInt32 publishTimeoutOut = getApi().getSipOptionPublishTimeout();
		//Assert.AreEqual( mPublishTimeout, publishTimeoutOut, "SIP Option Publish Timeout mismatch." );

		//getApi().setSipOptionUseOptionsRequest( mUseOptionsRequest );
		//Boolean useOptionsRequestOut = getApi().getSipOptionUseOptionsRequest();
		//Assert.AreEqual( mUseOptionsRequest, useOptionsRequestOut, "SIP Use Options Request mismatch." );

		//getApi().setSipOptionP2pPresence( mOptionsP2PPresence );
		//Boolean optionsP2PPresenceOut = getApi().getSipOptionP2pPresence();
		//Assert.AreEqual( mOptionsP2PPresence, optionsP2PPresenceOut, "SIP Options P2P Presence mismatch." );

		//getApi().setSipOptionUseTypingState( mOptionsUseTypingState );
		//Boolean optionsUseTypingStateOut = getApi().getSipOptionUseTypingState();
		//Assert.AreEqual( mOptionsUseTypingState, optionsUseTypingStateOut, "SIP Options Typing State mismatch." );

		//getApi().setSipOptionChatWithoutPresence( mChatWithoutPresence );
		//Boolean chatWithoutPresenceOut = getApi().getSipOptionChatWithoutPresence();
		//Assert.AreEqual( mChatWithoutPresence, chatWithoutPresenceOut, "SIP Options Chat Without Presence mismatch." );

		//Not called in old desktop app.  Do we need this?
//		void		 setSipOptionFailureTimeout     ( unsigned Int32 value );		//In seconds
//		unsigned Int32 getSipOptionFailureTimeout     () 

		terminateApi();		//Call this or unit test keeps running
	}

	//-------------------------------------------------------------------------
	//This method tests values set during app level configuration.
	//-------------------------------------------------------------------------
	[TestMethod]
	public void testAppLevelConfigurationSetsAndGets()
	{
		//TODO-PJSIP: Change how we do these unit tests
		//Called from app initialization.  Uses stored UUID or creates a new one if one does not exist.
		//getApi().setSipOptionUuid( mSipUserUuid );
		//String uuidOut1 = getApi().getSipOptionUuid();
		//String uuidOut2 = getApi().getUUID();				//Same as getSipOptionUuid()
		//Assert.AreEqual( mSipUserUuid, uuidOut1, "SIP Option UUID mismatch 1." );
		//Assert.AreEqual( uuidOut1,		uuidOut2, "SIP Option UUID mismatch 2." );
	}


	//-------------------------------------------------------------------------
	//This method tests values set during phone line configuration.
	//	In old desktop app, many of these values came from Config file,
	//	but that does NOT mean they were all user configureable.
	//-------------------------------------------------------------------------
	[TestMethod]
	public void testPhoneLineConfigurationSetsAndGets()
	{
		initApi();
		doAppConfiguration();
		doSsoConfiguration();

		//We are NOT supporting HTTP proxy server in new client, at least for Phase 1
//		void addProxy(  String address, unsigned Int32 port,  String login,  String  password, SipAgent.HTTPProxy httpProxyType );
		//No matching get

		//TODO-PJSIP: Change how we do these unit tests
		//getApi().enableAEC( mAec );
		//Boolean aecOut = getApi().isAecEnabled();
		//Assert.AreEqual( mAec, aecOut, "Auto. Echo Cancellation (AEC) mismatch." );

		//getApi().enableAGC( mAgc );
		//Boolean agcOut = getApi().isAgcEnabled();
		//Assert.AreEqual( mAgc, agcOut, "Auto. Gain Control (AGC) mismatch." );

		//getApi().enableNoiseSuppression( mNoiseSuppression );
		//Boolean noiseSuppressionOut = getApi().isNoiseSuppressionEnabled();
		//Assert.AreEqual( mNoiseSuppression, noiseSuppressionOut, "Noise Suppression mismatch." );

		//getApi().setCallsEncryption( mCallEncryption );
		//SipAgent.CallEncryption valueOut = getApi().getCallsEncryption();
		//Assert.AreEqual( (Int32)mCallEncryption, (Int32)valueOut, "Call Encryption mismatch." );

		//----------------------------------------------------------
		//Audio settings
		//----------------------------------------------------------
		// Sets the audio codec list sorted by preference
		//Boolean comfortNoiseIn		= true;
		//Boolean telephoneEventsIn	= true;
		//Boolean redundantAudioIn	= true;
		//StringList audioCodecsIn	= new StringList();			//This is typically empty, if user has never re-ordered codecs, which I don't think we are allowing.
		//SipAgent.Result result1 = getApi().setAudioCodecs( audioCodecsIn, comfortNoiseIn, telephoneEventsIn, redundantAudioIn );

		//if ( result1 == SipAgent.Result.RESULT_OK )
		//{
		//	//StringList audioCodecsOut1 = getApi().getAudioCodecList();
		//	////TODO: How to test this?

		//	//AudioCodecInfo codecInfo = new AudioCodecInfo();

		//	//getApi().getAudioCodecs( codecInfo );	//This will have a comma-delimited list.  TODO: How to test.
	
		//	//Assert.AreEqual( comfortNoiseIn,    codecInfo.ComfortNoise,    "Comfort Noise mismatch." );
		//	//Assert.AreEqual( telephoneEventsIn, codecInfo.TelephoneEvents, "Telephone Events mismatch." );
		//	//Assert.AreEqual( redundantAudioIn,  codecInfo.RedundantAudio,   "RED mismatch." );
		
		//	//Both gets need to be tested
		//}
		//else
		//{
		//	Assert.Fail( "setAudioCodecs failed." );
		//}

//		getApi().setPreferredAudioCodecOrder();	//This order is hard-coded in SIP stack

//		StringList preferredAudioCodecs = getApi().getPreferredAudioCodecList();	//Used in Config.Audio.Advanced.
//		StringList defaultAudioCodecs   = getApi().getDefaultAudioCodecList();		//Used interally by getPreferredAudioCodecList().  May not need.

		//TODO: How to test this?  Used only in UI?
		//All combinations need to be tested

		//Get list of devices we can play with.
//		AudioDeviceList inputDevices  = getApi().getSystemAudioInputDevices() ;			//Used in Config.Audio, PhoneCall UI
//		AudioDeviceList outputDevices = getApi().getSystemAudioOutputDevices();			//Used in Config.Audio, PhoneCall UI

//		AudioDevice inputDevice  = inputDevices[0];		//Just use first one
//		AudioDevice outputDevice = outputDevices[0];	//Just use first one
//		AudioDevice ringerDevice = outputDevices[0];	//Just use first one

		//Sets the call ringer/alerting device.
//		getApi().setRingerOutputAudioDevice( ringerDevice );
//		AudioDevice ringerOut = getApi().getRingerOutputAudioDevice();
//		Boolean ringerEqual = ringerDevice.Equals( ringerOut );
//		Assert.IsTrue( ringerEqual, "Ringer device mismatch." );

		//Sets the call input device (in-call microphone).
//		getApi().setCallInputAudioDevice( inputDevice );
//		AudioDevice inputOut = getApi().getCallInputAudioDevice();
//		Boolean inputEqual = inputDevice.Equals( inputOut );
//		Assert.IsTrue( inputEqual, "Input device mismatch." );

		//Sets the call output device (in-call speaker).
//		getApi().setCallOutputAudioDevice( outputDevice );
//		AudioDevice outputOut = getApi().getCallOutputAudioDevice();
//		Boolean outputEqual = outputDevice.Equals( outputOut );
//		Assert.IsTrue( outputEqual, "Output device mismatch." );

//		Boolean testFail = outputDevice.Equals( inputDevice );
//		Assert.IsFalse( testFail, "AudioDevice Equals() does not work." );



		//----------------------------------------------------------
		//Video settings - not supported at this time
		//----------------------------------------------------------
//		void					setVideoDevice(  String deviceName );
//		String						getVideoDevice();
//
//		void					setVideoQuality( EnumVideoQuality::VideoQuality videoQuality );
//		EnumVideoQuality::VideoQuality	getVideoQuality();
//
//		void					flipVideoImage( Boolean flip );
//		Boolean							shouldFlipVideoImage();

		terminateApi();		//Call this or unit test keeps running
	}

	//-------------------------------------------------------------------------
	//This method tests misc. API calls
	//-------------------------------------------------------------------------
//	[TestMethod]
//	public void testMisc()
//	{
//		initApi();
//		doAppConfiguration();
//		doSsoConfiguration();
//
//		String	realmOut = getApi().getRealm();	//Not used in old app.  Returns empty string.
	
//		terminateApi();		//Call this or unit test keeps running
//	}

		//NOT SUPPORTED

		///**
		// * @name IMConnect Implementation
		// * @{
		// */
		//void connect();
		//void disconnect(Boolean force = false);
		///** @} */

		///**
		// * @name IMChat Implementation
		// * @{
		// */
		//void sendMessage
		//(
		//    IMChatSession&      chatSession,
		//     String  message
		//);
		//void changeTypingState
		//(
		//    IMChatSession&      chatSession,
		//    IMChat::TypingState state
		//);
		//void createSession
		//(
		//    IMChat&             imChat,
		//     IMContactSet& imContactSet,
		//    IMChat::IMChatType  imChatType,
		//     String  userChatRoomName
		//);
		//void closeSession(IMChatSession& chatSession);
		//void addContact
		//(
		//    IMChatSession&      chatSession,
		//     String  contactId
		//);
		//void removeContact
		//(
		//    IMChatSession&      chatSession,
		//     String  contactId
		//);
		///** @} */

		///**
		// * @name IMPresence Implementation
		// * @{
		// */
		//void changeMyPresence
		//(
		//    EnumPresenceState::PresenceState    state,
		//     String                  note
		//);

		//void setMyIcon( String iconFilename);

		//void sendMyIcon( String contactId);

		//void subscribeToPresenceOf( String contactId);

		//void unsubscribeToPresenceOf( String contactId);

		//void blockContact( String contactId);

		//void unblockContact( String contactId);

		//void acceptSubscription(Int32 sid);

		//void rejectSubscription(Int32 sid);

}	//namespace SipAgentApiTest

}   //namespace ManagedSipAgentApi.Test

