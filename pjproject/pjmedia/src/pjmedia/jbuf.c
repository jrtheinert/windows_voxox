#include <pjmedia/jbuf.h>
#include <pjmedia/errno.h>
#include <pj/pool.h>
#include <pj/assert.h>
#include <pj/log.h>
#include <pj/math.h>
#include <pj/string.h>


#define THIS_FILE   "jbuf.c"

#define JB_CAPACITY                     32
#define JB_INDEX_MASK                   31

#define JB_BUFFER_LOW_MIN               5
#define JB_BUFFER_LOW_MAX               14
#define JB_BUFFER_LOW                   8
#define JB_BUFFER_HIGH                  16
#define JB_BUFFER_VERY_HIGH             28
#define JB_PREFETCH                     8

#define JB_MAX_FRAME_SIZE               500

#define JB_FALSE                        0
#define JB_TRUE                         1
#define JB_ADJUST_COUNTER               50


#define JB_FILING_CAPACITY               (JB_CAPACITY - 2)
#define JB_FILING_FULL                   (JB_CAPACITY - 4)
#define JB_MAX_DROPED_OUT_PACKETS_COUNT  5

#define JB_DBG_INFO     0

#if 0
#  define DBG_TRACE(args)	    PJ_LOG(5,args)
#else
#  define DBG_TRACE(args)
#endif


struct pjmedia_jbuf_frame
{
    pj_uint32_t     size;
    pj_uint32_t     bit_info;
    pj_uint32_t     ts;
    char            data[JB_MAX_FRAME_SIZE];

};

struct pjmedia_jbuf
{
    /* Settings (consts) */
    pj_str_t        _name;                  /* jitter buffer name           */
    unsigned        _frame_ptime;           /* frame duration.              */
    pj_size_t       _min_frame_size;        /* min frame size               */
    pj_size_t       _max_frame_size;        /* max frame size               */
    pj_size_t       _set_frame_size;        /* max frame size               */

    pj_bool_t       _restart;
    pj_bool_t       _playing_now;
    int             _last_frame_seq;
    int             _first_frame_seq;

    struct pjmedia_jbuf_frame _frame[JB_CAPACITY];

    unsigned        _prefetch;
    unsigned        _buffer_low;
    unsigned        _buffer_high;
    unsigned        _buffer_medium;

    int             _adjustValue;
    int             _adjustCounter;
    int             _adjustMaxControlDelta;
    int             _adjustMaxDelta;
    int             _adjustMinSize;
    int             _adjustMaxSize;
    int             _adjustPrevFillSize;

    unsigned        _dropped_out_packets;

    unsigned        _qs_low_buffer_sends;
    unsigned        _qs_normal_buffer_sends;
    unsigned        _qs_high_buffer_sends;
    
    unsigned        _qs_exhausted;
    unsigned        _qs_out_of_range;
    unsigned        _qs_discard;
    unsigned        _qs_empty;
    unsigned        _qs_lost;
    
};


PJ_INLINE(unsigned) jbuf_get_fill_size(const pjmedia_jbuf* jb)
{
     return  (jb->_last_frame_seq - jb->_first_frame_seq) & JB_INDEX_MASK;
}


#if JB_DBG_INFO
static int s_size  = 0;
static int s_exhausted  = 0;
static int s_out_range = 0;
static int s_discard = 0;
static int s_lost = 0;
static int s_low_buffer_sends = 0;
static int s_high_buffer_sends  = 0;
static int s_low = 0;
static int s_high  = 0;

static void pjmedia_set_info(pjmedia_jbuf *jb)
{
    s_size = jbuf_get_fill_size(jb);
    s_exhausted  = jb->_qs_exhausted;
    s_out_range = jb->_qs_out_of_range;
    s_discard = jb->_qs_discard;
    s_lost = jb->_qs_lost;
    s_low_buffer_sends = jb->_qs_low_buffer_sends;
    s_high_buffer_sends = jb->_qs_high_buffer_sends;
    s_low = jb->_buffer_low;
    s_high = jb->_buffer_high;
}
#else
PJ_INLINE(void) pjmedia_set_info(pjmedia_jbuf* jb) { }

#endif


PJ_DEF(pj_bool_t) pjmedia_jbuf_is_full(const pjmedia_jbuf *jb)
{
    unsigned fill_size = jbuf_get_fill_size(jb);
    return  fill_size >= JB_FILING_FULL;
}


PJ_DEF(pj_status_t) pjmedia_jbuf_create(pj_pool_t* pool,
                    const pj_str_t* name,
				      unsigned frame_size,
                    unsigned ptime,
                    unsigned max_count,
                    pjmedia_jbuf **p_jb)
{
    pjmedia_jbuf* jb;
    pj_status_t status;

    jb = PJ_POOL_ZALLOC_T(pool, pjmedia_jbuf);

    pj_strdup_with_null(pool, &jb->_name, name);

    jb->_frame_ptime        = ptime;
    jb->_set_frame_size     = frame_size;
    jb->_min_frame_size     = 1000000;
    jb->_max_frame_size     = 0;

    jb->_prefetch           = JB_PREFETCH;
    
    jb->_buffer_low         = JB_BUFFER_LOW;
    jb->_buffer_high        = JB_BUFFER_HIGH;
    jb->_buffer_medium      = (jb->_buffer_low + jb->_buffer_high)/2;

    jb->_adjustValue        = 0;
    jb->_adjustCounter      = JB_ADJUST_COUNTER;
    jb->_adjustMaxControlDelta = 20;
    jb->_adjustMaxDelta     = 0;
    jb->_adjustPrevFillSize = 0;
    jb->_adjustMinSize = jb->_adjustMaxSize = 0;



    pjmedia_jbuf_reset(jb);

    *p_jb = jb;
    return PJ_SUCCESS;
}

PJ_DEF(pj_status_t) pjmedia_jbuf_destroy(pjmedia_jbuf *jb)
{
    PJ_LOG(5, (jb->_name.ptr, ""
               "JB summary:\n"
               "  min/max/set frame_size=%d/%d/%d\n"
               "  send %u+%u+%u = %u\n"
               "  lost=%d discard=%d empty=%d",
               jb->_max_frame_size,

               jb->_qs_low_buffer_sends,
               jb->_qs_normal_buffer_sends,
               jb->_qs_high_buffer_sends,
               jb->_qs_low_buffer_sends + jb->_qs_normal_buffer_sends + jb->_qs_high_buffer_sends,
               
               jb->_qs_lost,jb->_qs_discard, jb->_qs_empty));
               

    return PJ_SUCCESS;
}


PJ_DEF(pj_status_t) pjmedia_jbuf_reset(pjmedia_jbuf *jb)
{
    jb->_restart = JB_TRUE;
    return PJ_SUCCESS;
}

PJ_DEF(void) pjmedia_jbuf_put_frame(
        pjmedia_jbuf*   jb,
        const void*     frame,
        pj_size_t       frame_size,
        int             frame_seq)
{
    pjmedia_jbuf_put_frame3(jb, frame, frame_size, 0, frame_seq, 0, NULL);
}


PJ_DEF(void) pjmedia_jbuf_put_frame2(
        pjmedia_jbuf*   jb,
        const void*     frame,
        pj_size_t       frame_size,
        pj_uint32_t     bit_info,
        int             frame_seq,
        pj_bool_t*  discarded)
{
    pjmedia_jbuf_put_frame3(jb, frame, frame_size, bit_info, frame_seq, 0, discarded);
}


PJ_DEF(void) pjmedia_jbuf_put_frame3(
        pjmedia_jbuf*   jb,
        const void*     frame_data,
        pj_size_t       frame_size,
        pj_uint32_t     bit_info,
        int             frame_seq,
        pj_uint32_t     ts,
        pj_bool_t*      discarded)
{
    if (discarded)
        *discarded = JB_TRUE;

    ++jb->_qs_discard;

    if (frame_size > jb->_max_frame_size)
        jb->_max_frame_size = frame_size;

    if (frame_size < jb->_min_frame_size)
        jb->_min_frame_size = frame_size;

    if (frame_size > JB_MAX_FRAME_SIZE)
    {
        PJ_LOG(2, (THIS_FILE, "ERROR frame_size(%d) > JB_MAX_FRAME_SIZEe(%d)", frame_size, JB_MAX_FRAME_SIZE));
        return;
    }

    if (jb->_restart)
        return;

    if (jbuf_get_fill_size(jb) == 0)
    {
        jb->_qs_exhausted ++;

        DBG_TRACE((THIS_FILE, "New sequence number 0x%x", frame_seq));
        jb->_first_frame_seq = jb->_last_frame_seq = frame_seq;
    }


    int position = frame_seq - jb->_first_frame_seq;

    if ((position >=0) && (position < JB_FILING_CAPACITY)) 
    {
        jb->_dropped_out_packets = 0;

        int i;
        for (i=jb->_last_frame_seq; i <= frame_seq; ++i)
            jb->_frame[i & JB_INDEX_MASK].size = 0;

        struct pjmedia_jbuf_frame* frame = &jb->_frame[frame_seq & JB_INDEX_MASK];

        if (frame->size != 0) {
            DBG_TRACE((THIS_FILE, "Duplicate RTP packet with seqn# 0x%x detected. Drop later", frame_seq));
	} else {
            if (discarded)
                *discarded = JB_FALSE;

            --jb->_qs_discard;

            memcpy(frame->data, frame_data, frame_size);
            frame->bit_info = bit_info;
            frame->ts = ts;
            frame->size = frame_size;
            jb->_last_frame_seq = frame_seq+1;
		}
	    }
    else
    {
        jb->_qs_out_of_range++;
        if (++jb->_dropped_out_packets > JB_MAX_DROPED_OUT_PACKETS_COUNT)
        {
            DBG_TRACE((THIS_FILE, "_dropped_out_packets over JB_MAX_DROPED_OUT_PACKETS_COUNT(%d)", JB_MAX_DROPED_OUT_PACKETS_COUNT));
            jb->_restart = JB_TRUE;
	}
    }
}


/*
* Get frame from jitter buffer.
*/
PJ_DEF(void) pjmedia_jbuf_get_frame( pjmedia_jbuf *jb,
                                    void *frame,
                                    char *p_frame_type)
{
    pjmedia_jbuf_get_frame3(jb, frame, NULL, p_frame_type, NULL, NULL, NULL);
}

/*
 * Get frame from jitter buffer.
 */
PJ_DEF(void) pjmedia_jbuf_get_frame2(pjmedia_jbuf *jb,
                                     void *frame,
                                     pj_size_t *size,
                                     char *p_frame_type,
                                     pj_uint32_t *bit_info)
{
    pjmedia_jbuf_get_frame3(jb, frame, size, p_frame_type, bit_info, NULL, NULL);
}



void adjust(pjmedia_jbuf* jb)
{
    int fill_size = jbuf_get_fill_size(jb);
    if (fill_size == 0)
    {
        jb->_buffer_low = JB_BUFFER_LOW;
        jb->_buffer_high = JB_BUFFER_HIGH;
        jb->_buffer_medium = (jb->_buffer_low + jb->_buffer_high)/2;
        jb->_adjustValue = 0;
        jb->_adjustCounter = 40;
        jb->_adjustMaxControlDelta = JB_ADJUST_COUNTER;
        jb->_adjustMaxDelta = 0;
        jb->_adjustPrevFillSize = 0;
        jb->_adjustMinSize = jb->_adjustMaxSize = 0;

    }

    int delta = jb->_adjustPrevFillSize - fill_size;
    if (delta > jb->_adjustMaxDelta)
        jb->_adjustMaxDelta = delta;

     jb->_adjustPrevFillSize = fill_size;

    if (jb->_adjustMinSize > fill_size)
        jb->_adjustMinSize = fill_size;

    if (jb->_adjustMaxSize < fill_size)
        jb->_adjustMaxSize = fill_size;

    if (--jb->_adjustCounter < 0) {
        jb->_adjustCounter = JB_ADJUST_COUNTER;

        jb->_adjustMaxDelta = jb->_adjustMaxSize - jb->_adjustMinSize;

        if (jb->_adjustMaxControlDelta >= jb->_adjustMaxDelta)
            jb->_adjustMaxControlDelta = jb->_adjustMaxDelta;
        else
            -- jb->_adjustMaxControlDelta;


        int i = jb->_adjustMaxControlDelta + 4;
        if (i < JB_BUFFER_LOW_MIN)
            i = JB_BUFFER_LOW_MIN;
        else if (i > JB_BUFFER_LOW_MAX)
            i= JB_BUFFER_LOW_MAX;

        jb->_adjustMaxDelta = 0;
        jb->_buffer_low = i;
        jb->_buffer_high = i*2;
        jb->_buffer_medium  = i + (i>>1);
        jb->_adjustMinSize = fill_size;

        jb->_adjustMinSize = jb->_adjustMaxSize = fill_size;
    }
    pjmedia_set_info(jb);
}

/*
 * Get frame from jitter buffer.
 */
PJ_DEF(void) pjmedia_jbuf_get_frame3(
        pjmedia_jbuf*   jb,
        void*           frame_data,
        pj_size_t*      frame_size,
        char*           p_frame_type,
        pj_uint32_t*    bit_info,
        pj_uint32_t*    ts,
        int*            seq)
{
    adjust(jb);

    *p_frame_type = PJMEDIA_JB_ZERO_EMPTY_FRAME;

    ++jb->_qs_empty;

    if (frame_size)
        *frame_size = 0;

    if (jb->_restart) {
        jb->_playing_now = JB_FALSE;
        jb->_first_frame_seq = jb->_last_frame_seq;
        jb->_restart = JB_FALSE;
        return;
    }

    int fill_size = jbuf_get_fill_size(jb);

    if (fill_size == 0) {
        jb->_playing_now = JB_FALSE;
        return;
    }

    if (!jb->_playing_now) {
        if (fill_size < jb->_prefetch) {
            *p_frame_type = PJMEDIA_JB_ZERO_PREFETCH_FRAME;
            return;
        }
        jb->_playing_now = JB_TRUE;
    }

    struct pjmedia_jbuf_frame* frame = &jb->_frame[jb->_first_frame_seq & JB_INDEX_MASK];

    if (frame_size)
        *frame_size = frame->size;

    if (seq)
        *seq = jb->_first_frame_seq;

    if (frame->size == 0)
    {
        ++ jb->_qs_lost;
        *p_frame_type = PJMEDIA_JB_MISSING_FRAME;
    } else {
        if (bit_info)
            *bit_info = frame->bit_info;
         if (ts)
            *ts = frame->ts;

        memcpy(frame_data, frame->data, frame->size);
        *p_frame_type = PJMEDIA_JB_NORMAL_FRAME;
        -- jb->_qs_empty;
    }

//    frame->size = 0;
    ++jb->_first_frame_seq;

    if (jbuf_get_fill_size(jb) == 0)
        jb->_playing_now = JB_FALSE;
}




PJ_DEF(int) pjmedia_jbuf_get_adjust_freq( pjmedia_jbuf* jb)
{
    if (!jb->_playing_now) {
        jb->_adjustValue = 0;
        return 0;
    }

    int fill_size = jbuf_get_fill_size(jb);

    int adjust = jb->_adjustValue;

    if (adjust < 0) {
        if (fill_size >= jb->_buffer_medium)
         adjust = 0;
    } else if (adjust == 0) {
        if (fill_size < jb->_buffer_low)
            adjust = -1;
        else if (fill_size < jb->_buffer_high)
            adjust = 0;
        else if (fill_size < JB_BUFFER_VERY_HIGH)
            adjust = 1;
        else
            adjust = 2;
    } else {
        if (fill_size <= jb->_buffer_medium)
            adjust = 0;
        else if (fill_size < jb->_buffer_high)
            adjust = 1;
        else
            adjust = 2;
    }

    jb->_adjustValue = adjust;


    if (adjust < 0)
        ++ jb->_qs_low_buffer_sends;
    else if (adjust == 0)
        ++ jb->_qs_normal_buffer_sends;
    else
        ++ jb->_qs_high_buffer_sends;

    return adjust;
}





/*
* Get jitter buffer state.
*/

extern void xopus_stat(unsigned*);

PJ_DEF(pj_status_t) pjmedia_jbuf_get_state( const pjmedia_jbuf *jb, pjmedia_jb_state *state )
{
    PJ_ASSERT_RETURN(jb && state, PJ_EINVAL);

#if JB_DBG_INFO

    unsigned* p = (unsigned*) state;

    *p ++ = s_size;
    *p ++ = s_exhausted;
    *p ++ = s_out_range;
    *p ++ = s_discard;
    *p ++ = s_lost;
    *p ++ = s_low_buffer_sends;
    *p ++ = s_high_buffer_sends;
    *p ++ = s_low;
    *p ++ = s_high;
#else

    state->frame_size   = jb->_max_frame_size;
    state->min_prefetch = jb->_prefetch;
    state->max_prefetch = jb->_prefetch;

    state->burst        = 0;
    state->prefetch     = jb->_prefetch;              //jb->jb_prefetch;
    state->size         = jbuf_get_fill_size(jb);     //     jb_framelist_eff_size(&jb->jb_framelist);

    state->avg_delay    = 0;    //jb->jb_delay.mean;
    state->min_delay    = 0;    //jb->jb_delay.min;
    state->max_delay    = 0;    //jb->jb_delay.max;
    state->dev_delay    = 0;    //pj_math_stat_get_stddev(&jb->jb_delay);

    state->avg_burst    = 0;//jb->_burst.mean;
    state->empty        = jb->_qs_empty;
    state->discard      = jb->_qs_discard;
    state->lost         = jb->_qs_lost;


#endif
    return PJ_SUCCESS;
}


PJ_DEF(unsigned) pjmedia_jbuf_remove_frame(pjmedia_jbuf *jb, unsigned frame_cnt)
{
#if 0
     unsigned count, last_discard_num;

     last_discard_num = jb->jb_framelist.discarded_num;
     count = jb_framelist_remove_head(&jb->jb_framelist, frame_cnt);

     /* Remove some more when there were discarded frames included */
    while (jb->jb_framelist.discarded_num < last_discard_num) {
        /* Calculate frames count to be removed next */
        frame_cnt = last_discard_num - jb->jb_framelist.discarded_num;

        /* Normalize non-discarded frames count just been removed */
        count -= frame_cnt;

        /* Remove more frames */
        last_discard_num = jb->jb_framelist.discarded_num;
        count += jb_framelist_remove_head(&jb->jb_framelist, frame_cnt);
    }

    return count;
#endif 
    return 0;
}


/*
* Set the jitter buffer to adaptive mode.
*/
PJ_DEF(pj_status_t) pjmedia_jbuf_set_adaptive(
        pjmedia_jbuf *jb,
        unsigned prefetch,
        unsigned min_prefetch,
        unsigned max_prefetch)
{
    return PJ_SUCCESS;
}


/*
 * Set the jitter buffer to fixed delay mode. The default behavior
 * is to adapt the delay with actual packet delay.
 *
	 */
PJ_DEF(pj_status_t) pjmedia_jbuf_set_fixed( pjmedia_jbuf *jb, unsigned prefetch)
{
    return PJ_SUCCESS;
}


PJ_DEF(pj_status_t) pjmedia_jbuf_set_discard( pjmedia_jbuf *jb, pjmedia_jb_discard_algo algo)
{
    return PJ_SUCCESS;
}


PJ_DEF(void) pjmedia_jbuf_peek_frame(
         pjmedia_jbuf*      jb,
         unsigned           offset,
         const void**       frame,
         pj_size_t*         size,
         char*              p_frm_type,
         pj_uint32_t*       bit_info,
         pj_uint32_t*       ts,
         int*               seq)
{
/*
    pjmedia_jb_frame_type ftype;
    pj_bool_t res;

    res = jb_framelist_peek(&jb->jb_framelist, offset, frame, size, &ftype,
                            bit_info, ts, seq);
    if (!res)
        *p_frm_type = PJMEDIA_JB_ZERO_EMPTY_FRAME;
    else if (ftype == PJMEDIA_JB_NORMAL_FRAME)
        *p_frm_type = PJMEDIA_JB_NORMAL_FRAME;
    else
        *p_frm_type = PJMEDIA_JB_MISSING_FRAME;
	 */
}


#if 0

#define JB_EMPTY 1
#define JB_LOW 3
#define JB_HIGH 7

#define JB_MIN_COUNT_PACKET_FOR_START_TO_PALY (JB_LOW + 1)
#define DEFAULT_PLAYBACK_GAIN 1.0

#define JITTER_PAYLOAD_BUFFER_SIZE      1600



#define JB_STATUS_INITIALIZING    0
#define JB_STATUS_PROCESSING    1



/* Progressive discard algorithm introduced to reduce JB latency
 * by discarding incoming frames with adaptive aggressiveness based on
 * actual burst level.
 */
#define PROGRESSIVE_DISCARD 1

/* Internal JB frame flag, discarded frame will not be returned by JB to
 * application, it's just simply discarded.
	     */
#define PJMEDIA_JB_DISCARDED_FRAME 1024


#endif //0















