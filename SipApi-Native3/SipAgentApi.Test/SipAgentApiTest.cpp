
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Sep 2014
 */

#include "stdafx.h"
#include "CppUnitTest.h"

#include "../SipAgentAPI/SipAgentApiWrapper.h"
#include "../SipAgentAPI/DataTypes.h"
#include "../SipAgentApi/ISipAgent.h"

#include "Windows.h"	//For Sleep();

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

class MyAppEventHandler : public AppEventHandler
{
public:
	MyAppEventHandler()		{}

	//Connect and Disconnect
	void connectedEvent()
	{
		int xxx = 1;	//We hit this line registration and making phone call.
	}

	void disconnectedEvent( bool connectionError, const std::string& reason )
	{
		int xxx = 1;	//I have NOT hit this yet.
	}

	//Line and Call events
	void phoneCallStateChangedEvent( int callId, EnumPhoneCallState::PhoneCallState state, const std::string & from, int statusCode )
	{
		int xxx = 1;	//We hit this during phone call
	}

	void phoneLineStateChangedEvent( int lineId, EnumPhoneLineState::PhoneLineState state )
	{
		int xxx = 1;	//We hit this during line registration and phone call.
	}

	void infoRequestReceivedEvent( int callId, const std::string& content, const std::string& contentType )
	{
		int xxx = 1;	//We hit this during a call when specific events occur.
	}

	//Logging
	void sendLog( const LogEntry& logEntry )
	{
		std::string msg = ">>>> " + logEntry.getMessage() + "\n";
		OutputDebugStringA( msg.c_str() );
	}

};

static  MyAppEventHandler*  s_eventHandler = new MyAppEventHandler();
static 	ServerInfoList		s_servers;

//#define USE_TLS 1			//TODO: To test TLS, we must initialize OpenSSL.  Defer that for now.

namespace SipAgentApiTest
{		

//-------------------------------------------------------------------------------------------------
//We currently have 101 public methods to test, in these categories
//	- SIP stack setup / breakdown:	 7
//	- SIP callback handlers:		 5	- See AppEventHandler class above
//	- SIP phone line:				 3
//	- SIP call:						13
//	- Sip Configuration set/get:	73	- These are in several methods, depending on when they are typically set.
//
//This will leave xxx methods to be tested separately
//-------------------------------------------------------------------------------------------------

TEST_CLASS(SipAgentApiTest1 )
{
public:
	std::string	mSipAgentName = "Voxox-3.x.x.xxxx";	//Not used yet, but we need a way to add SipAgentName

	//These are used by some servers for credentials
//	std::string	mSipUserId    = "19501010555";	//@voxox.com";
//	std::string mSipPassword  = "bilx6dca2zph371k";
//	std::string	mSipUserId    = "14153473422";	//@voxox.com";
//	std::string mSipPassword  = "v61g3s74ak0w2ecy";
	std::string	mSipUserId = "19508966850";	//@voxox.com";
	std::string mSipPassword = "pelv6ot9bsz845rd";

	//These items come from SSO, but are stored in separate SipAccount class until they are needed.
	//	Primarily used for addVirtualLine().
//	std::string mUserName	 = "19505005170";		//I am not sure of what this is supposed to be.
//	std::string mDisplayName = "jrtheinert";		//This is what user entered on login.  It is NOT case-sensitive, so you may see different capitalization.
//	std::string mUserName	 = "19501033258";		//I am not sure of what this is supposed to be.
//	std::string mDisplayName = "jrt1@tcoff.net";		//This is what user entered on login.  It is NOT case-sensitive, so you may see different capitalization.
	std::string mUserName	 = "19508966850";		//I am not sure of what this is supposed to be.
	std::string mDisplayName = "beta@yk.com";		//This is what user entered on login.  It is NOT case-sensitive, so you may see different capitalization.
	std::string mRealm = "voxox.com";

//	std::string	mSipServer    = "vsip.voxox.com";			//From SSO login.
//	std::string	mSipServer    = "tcsbuslab06.voxox.com";	//From new VxApi23 auth
	std::string	mSipServer    = "8.38.43.130";			//From new VxApi23 auth
//	std::string	mSipServer	  = "vsip.voxox.com";			//
	std::string	mStunServer	  = "turn.voxox.com";
	std::string mTurnServer   = "turn.voxox.com";	
	std::string	mTurnUserId	  = "angora";
	std::string mTurnPassword = "JNMBckSreQ";

	//App-level values
	std::string mSipUserUuid = "XXXX-3333-4444-5555-AAAA";
	bool		mDebugLogging = true;		//Enables additional logging from SIP stack.

	//These SIP configuration values came from old desktop app SSO 
	ISipAgent::Topology mTopology = ISipAgent::TOPOLOGY_ICE;

	bool mEnableIceMedia	 = false;
	bool mEnableIceSecurity	 = true;
	bool mTopologyEncryption = true;
	bool mTopologyTurn		 = true;

#ifdef USE_TLS
	bool mSipUdp = false;
	bool mSipTcp = false;
	bool mSipTls = true;
#else
	bool mSipUdp = true;
	bool mSipTcp = true;
	bool mSipTls = false;
#endif

	bool		 mSipKeepAlive				  = true;
	bool		 mSipUseRport				  = true;
	int			 mRegistrationRefreshInterval = 0;
	int			 mFailureInterval			  = 0;
	int			 mOptionKeepAlive			  = 30;
	int			 mMinimumSessionTime		  = 1800;
	bool		 mSupportSessionTimer		  = true;
	bool		 mInitiateSessionTimer		  = false;
	std::string	 mTagProlog					  = "voxox";
	unsigned int mRegisterTimeout			  = 2940;		//In seconds
	unsigned int mPublishTimeout			  = 300;		//In seconds
	bool		 mUseOptionsRequest			  = true;
	bool		 mOptionsP2PPresence		  = false;
	bool		 mOptionsUseTypingState		  = 1;
	bool		 mChatWithoutPresence		  = false;
	//End SSO configuration options.

	//PhoneLine configuration options.  These are typically set with default values,
	//	but application may allow user to change them.
	bool mAec				= true;
	bool mAgc				= true;
	bool mNoiseSuppression	= true;
	ISipAgent::Encryption mCallEncryption = ISipAgent::ENCRYPTION_NONE;		//TODO: Is this the proper default?
	//End PhoneLine configuration options.

	SipAgentApiWrapper* mSipAgentApi = nullptr;
	int					mLineId      = -1;				//LineId from most recent registration.

	//This is hit for each and every Unit Test, I think.
	SipAgentApiTest1()
	{
		if ( mSipAgentApi == nullptr )
		{
			mSipAgentApi  = SipAgentApiWrapper::getInstance();
			initApi();
		}
	}

	SipAgentApiWrapper* getApi()
	{
		return mSipAgentApi;
	}

	void initApi()
	{
		if ( !mSipAgentApi->isInitialized() )
		{
			mSipAgentApi->init();
		}
	}

	void terminateApi()
	{
		getApi()->terminate();
		mSipAgentApi = nullptr;
	}

	ServerInfoList getServers()
	{
		if ( s_servers.empty() )
		{
			//These are values for old desktop app SSO.
			//TODO: Not using HTTP Tunnel?
#ifdef USE_TLS
			s_servers.addSipRegistrar( mSipServer, 443, true );
			s_servers.addSipProxy    ( mSipServer, 443, true );
#else
			s_servers.addSipRegistrar( mSipServer, 5060, false );
			s_servers.addSipProxy    ( mSipServer, 5060, false );
#endif
			s_servers.addStun        ( mStunServer,      5050 );
			s_servers.addTurnUdp     ( mTurnServer,		 3478, mSipUserId, mSipPassword );
			s_servers.addTurnTcp     ( mTurnServer,		   80, mSipUserId, mSipPassword );
			s_servers.addTurnTls     ( mTurnServer,		  443, mSipUserId, mSipPassword );
		}

		return s_servers;
	}

	TlsInfo getTlsInfo()
	{
		TlsInfo tlsInfo;
#ifdef USE_TLS
		tlsInfo.setMethod( ISipAgent::TLS_SSLv23 );
#else
		tlsInfo.setMethod( ISipAgent::TLS_SSLv2 );
#endif
		tlsInfo.setVerifyCertificate ( false );		//Change to true after we have CA file
		tlsInfo.setRequireCertificate( false );		//Change to true after we have CA file
		tlsInfo.setVerifyDepth		 ( 9 );
		//Remaining values are empty string by default.

		return tlsInfo;
	}

	ServerInfo getSipProxyServer()
	{
//		return getServers().getFirstSipProxy();
		return ServerInfo();		//TODO
	}

	ServerInfo getSipRegistrarServer()
	{
//		return getServers().getFirstSipRegistrar();	
		return ServerInfo();		//TODO
	}

	//This method does app-level configuration.  It should be called by some test methods to get proper setup
	void doAppConfiguration()
	{
		//Called from app initialization.  Uses stored UUID or creates a new one if one does not exist.
		getApi()->setSipOptionUuid  ( mSipUserUuid );
		getApi()->setAppEventHandler( s_eventHandler );
	}

	//This method does configuration which would normally be done during SSO is old app.
	//	It will need to be called by some test methods to get proper setup.
	//	These values are also used int testSsoConfigurationSetsAndGets().
	void doSsoConfiguration()
	{
		ServerInfoList serversIn = getServers();

		getApi()->addServers( serversIn );			//Bulk add servers, typically from SSO.

//		void addTunnel( const std::string& address, unsigned int port, bool ssl );	//VOXOX - JRT - 2012.02.03 - Not currently called anywhere.
		//No matching get

//		void addSipProxy( const std::string& server, unsigned int serverPort );	//Tested above
		//No matching get

		TlsInfo tlsInfoIn = getTlsInfo();

		getApi()->setTlsInfo( tlsInfoIn );

		//Discovery Topology
		getApi()->setDiscoveryTopology			( mTopology );
		getApi()->enableIceMedia				( mEnableIceMedia );
		getApi()->enableIceSecurity				( mEnableIceSecurity );
		getApi()->setTopologyEncryption			( mTopologyEncryption );
		getApi()->setTopologyTurn				( mTopologyTurn );
		getApi()->setSipUdp						( mSipUdp );
		getApi()->setSipTcp						( mSipTcp );
		getApi()->setSipTls						( mSipTls );
		getApi()->setKeepAlive					( mSipKeepAlive );
		getApi()->setUseRport					( mSipUseRport );
		getApi()->setRegistrationRefreshInterval( mRegistrationRefreshInterval );
		getApi()->setFailureInterval			( mFailureInterval );
		getApi()->setOptionsKeepAliveInterval	( mOptionKeepAlive );
		getApi()->setMinSessionTime				( mMinimumSessionTime );
		getApi()->setSupportSessionTimer		( mSupportSessionTimer );
		getApi()->setInitiateSessionTimer		( mInitiateSessionTimer );
		getApi()->setTagProlog					( mTagProlog );
		getApi()->setSipOptionRegisterTimeout	( mRegisterTimeout );
		getApi()->setSipOptionPublishTimeout	( mPublishTimeout );
		getApi()->setSipOptionUseOptionsRequest	( mUseOptionsRequest );
		getApi()->setSipOptionP2pPresence		( mOptionsP2PPresence );
		getApi()->setSipOptionUseTypingState	( mOptionsUseTypingState );
		getApi()->setSipOptionChatWithoutPresence( mChatWithoutPresence );

		//Not called in old desktop app.  Do we need this?
//		void		 setSipOptionFailureTimeout     ( unsigned int value );		//In seconds
//		unsigned int getSipOptionFailureTimeout     () 
	}
	//-------------------------------------------------------------------------
	//This method tests values set during phone line configuration.
	//	In old desktop app, many of these values came from Config file,
	//	but that does NOT mean they were all user configureable.
	//-------------------------------------------------------------------------
	void doPhoneLineConfiguration()
	{
		//TODO: Are we supporting HTTP proxy server in new client?
//		void addProxy( const std::string& address, unsigned int port, const std::string& login, const std::string&  password, ISipAgent::HTTPProxy httpProxyType );
		//No matching get

		getApi()->enableAEC( mAec );
		getApi()->enableAGC( mAgc );
		getApi()->enableNoiseSuppression( mNoiseSuppression );
		getApi()->setCallsEncryption    ( mCallEncryption );

		//TODO: More to come.
	}

	void doRegistration()
	{
		ServerInfo proxyServer     = getSipProxyServer();
		ServerInfo registrarServer = getSipRegistrarServer();

		//This registers with SIP server.
//		mLineId = getApi()->addVirtualLine( mDisplayName, mUserName, mSipUserId, mSipPassword, mRealm, proxyServer, registrarServer );
		getApi()->setCredentials( mDisplayName, mUserName, mUserName, mSipPassword, mRealm );
		mLineId = getApi()->addVirtualLine();

		Sleep( 2000 );	//Wait for SIP stack events, if any.

		if ( mLineId >= 0 )
		{
			// Registers a given phone line.
			//	LineId comes from addVirtualLine()
			//  0 == valid result.
			int result = getApi()->registerVirtualLine( mLineId );
		}
	}

	TEST_METHOD( testInitialization )
	{
		//Instantiation
		SipAgentApiWrapper* api = getApi();

		Assert::IsTrue( api != nullptr, L"API not properly instantiated.", LINE_INFO() );

		//Test initialization and isInitialized() methods
		api->init();
		bool isInitialized = api->isInitialized();

		Assert::IsTrue( isInitialized, L"API not initialized.", LINE_INFO() );

		if ( s_eventHandler != nullptr )
		{
			//Here, we only test the get/set of the AppEventHandler.
			//	The actual functioning of the AppEventHandler is tested elsewhere, likely manually in our test class above.
			api->setAppEventHandler( s_eventHandler );
			AppEventHandler* eventHandler = api->getAppEventHandler();

			Assert::IsTrue( s_eventHandler == eventHandler, L"AppEventHandler set/get values do not match.", LINE_INFO() );
		}
		else
		{
			Assert::Fail( L"MyEventHandler is null.  Ensure it is initialized.", LINE_INFO() );
		}

		api->terminate();

		Assert::IsFalse( api->isInitialized(), L"API is still initialized after terminate() call.", LINE_INFO() );
	}


	//------------------------------------------------------------------------------------------------
	//Begin line level tests
	//	I am not sure how we do this in unit tests
	//------------------------------------------------------------------------------------------------
	TEST_METHOD( testLineMethods )
	{
		initApi();
		doAppConfiguration();
		doSsoConfiguration();
		doPhoneLineConfiguration();

		ServerInfo proxyServer     = getSipProxyServer();
		ServerInfo registrarServer = getSipRegistrarServer();

		//This registers with SIP server.
//		int lineId = getApi()->addVirtualLine( mDisplayName, mUserName, mSipUserId, mSipPassword, mRealm, proxyServer, registrarServer );
		getApi()->setCredentials( mDisplayName, mUserName, mUserName, mSipPassword, mRealm );
		int lineId = getApi()->addVirtualLine();

		Sleep( 2000 );	//Wait for SIP stack events, if any.

		if ( lineId >= 0 )
		{
			// Registers a given phone line.
			//	LineId comes from addVirtualLine()
			//  0 == valid result.
			int result = getApi()->registerVirtualLine( lineId );

			Sleep( 2000 );	//Wait for SIP stack events, if any.

			if ( result == 0 )
			{
				int lineCount1 = getApi()->getLineCount();
				Assert::AreEqual( lineCount1, 1, L"Wrong line count after registerVirtualLine().", LINE_INFO() );

				// Remove a given phone line, previously registered.
				//	- force if true, forces the removal without waiting for the unregister
				getApi()->removeVirtualLine( lineId, false );
				int lineCount2 = getApi()->getLineCount();
				Assert::AreEqual( lineCount2, 0, L"Wrong line count after removeVirtualLine().", LINE_INFO() );
	
				Sleep( 2000 );	//Wait for SIP stack events, if any.
			}
			else
			{
				Assert::Fail( L"registerVirtualLine() failed.", LINE_INFO() );
			}
		}
		else
		{
			Assert::Fail( L"LineId is isvalid.", LINE_INFO() );
		}

		terminateApi();	
	}


	//------------------------------------------------------------------------------------------------
	//Begin call level tests
	//	These all require a callId which means we need to start a call and have it answered
	//	I am not sure how we do this in unit tests.
	//	Seems more like integration testing.
	//	Using code below I made a successful call with two-way audio!
	//------------------------------------------------------------------------------------------------
	TEST_METHOD( testCallOptions )
	{
		initApi();
		doAppConfiguration();
		doSsoConfiguration();
		doPhoneLineConfiguration();
		doRegistration();

		Sleep( 2000 );

		// Dials a phone number.
		//	sipAddress SIP address to call (e.g phone number to dial)
		std::string number = "sip:15168008216@voxox.com";			//Change this number for your tests.  DO NOT CALL ME!  ;-)
		bool		useVideo = false;	//Not supported
		int callId = getApi()->makeCall( mLineId, number, useVideo );
		std::string sipCallId;

		Sleep( 10000 );	//Wait for call to connect.
		if ( callId >= 0 )
		{
			sipCallId = getApi()->getSipCallId( callId );
		}

		bool testDtmf = false;	//Change to 'true' to test DTMF.
		
		if ( testDtmf )
		{
			getApi()->playDtmf( callId, '1') ;
			Sleep( 250 );
			getApi()->playDtmf( callId, '2') ;
			Sleep( 250 );
			getApi()->playDtmf( callId, '3') ;
			Sleep( 250 );
			getApi()->playDtmf( callId, '4') ;
			Sleep( 250 );
			getApi()->playDtmf( callId, '5') ;
			Sleep( 250 );
			getApi()->playDtmf( callId, '6') ;
			Sleep( 250 );
			getApi()->playDtmf( callId, '7') ;
			Sleep( 250 );
			getApi()->playDtmf( callId, '8') ;
			Sleep( 250 );
			getApi()->playDtmf( callId, '9') ;
			Sleep( 250 );
			getApi()->playDtmf( callId, '*') ;
			Sleep( 250 );
		}

		Sleep( 50000 );		//Testing.  Wait for remote end to answer.  Looking for hits in AppCallback object.

		terminateApi();	//Move this when testing items below.
	}


//		// Notifies the remote side (the caller) that this phone is ringing.
//		//	This corresponds to the SIP code "180 Ringing".
//		void sendRingingNotification( int callId, bool enableVideo );
//
//		// Accepts a given phone call.
//		void acceptCall( int callId, bool enableVideo );
//
//		// Rejects a given phone call.
//		void rejectCall(int callId);
//
//		//Closes a given phone call.
//		void closeCall(int callId);
//
//		// Holds a given phone call.
//		void holdCall(int callId);
//
//		// Resumes a given phone call.
//		void resumeCall(int callId);
//
//		// Blind transfer the specified call to another party.
//		void blindTransfer(int callId, const std::string& sipAddress);
//
//		// Sends a DTMF to a given phone call.
//		void playDtmf( int callId, char dtmf );
//
//		 //Sends and plays a sound file to a given phone call.
//		void playSoundFile( int callId, const std::string& soundFile );
//
//		// Gets the audio codec in use by a given phone call.
//		std::string getAudioCodecUsed(int callId);
//
//		// Gets the video codec in use by a given phone call.
////		std::string getVideoCodecUsed(int callId);		//Video not currently supported
//
//		//True if encrytion is activated for the given call
//		bool isCallEncrypted(int callId);
//
//		ISipAgent::CallCallbacks* newCall( int lineID, ISipAgent::Call* call ) throw();
//	}


//		//Registration and notification methods
//		ISipAgent::SubscriptionCallbacks* newSubscription( int lineId, ISipAgent::ServerSubscription* subscription ) throw();
//
//		void serverSubStateChanged( int sid, ISipAgent::State state ) throw();
//
//		void message( const char* from, const char* content, size_t contentSize, const char* contentType ) throw();
//
//		void notification( const char* eventIn, const char* content, size_t contentSize, const char* contentType ) throw();
//
//		void registrationChanged( int lineId, ISipAgent::State state, ISipAgent::Event regEvent ) throw();
//
//		bool isRegistered();
//
//		int getPresenceLine();
//
//	//---------------------------------------------------------------------------------
//	//Call audio related methods
//	//---------------------------------------------------------------------------------
//	TEST_METHOD( testAudio ) 
//	{
//		//Output
//		bool setSpeakerVolume( unsigned int volume );		// Sets the speaker |volume| level. Valid range is [0,255].
//		int  getSpeakerVolume();							// Gets the speaker |volume| level.
//
//		bool setSystemOutputMute( bool enable );			// Mutes the speaker device completely in the operating system.
//		bool getSystemOutputMute();							// Gets the output device mute state in the operating system.
//
//		//Ringing
//		bool setRingingVolume( unsigned int volume );		// Sets the ringing |volume| level. Valid range is [0,255].
//		int  getRingingVolume();							// Gets the ringing |volume| level.
//
//		bool setSystemRingingMute( bool enable );			// Mutes the ringing device completely in the operating system.
//		bool getSystemRingingMute();						// Gets the ringing device mute state in the operating system.
//
//		//Input
//		bool setMicVolume( unsigned int volume );			// Sets the microphone volume level. Valid range is [0,255].
//		int  getMicVolume();								// Gets the microphone volume level.
//
//		bool setSystemInputMute( bool enable );				// Mutes the microphone device completely in the operating system.
//		bool getSystemInputMute();							// Gets the mute state of the input device in the operating system.
//
//		int  getSpeechInputLevelFullRange();				// Gets the microphone speech |level|, mapped linearly to the range [0,32768].
//	}

	//---------------------------------------------------------------------------------
	//Start SIP configuration set/get test
	//	- I am testing default settings based on old desktop SIP configuration items from SSO
	//---------------------------------------------------------------------------------
	TEST_METHOD(testSsoConfigurationSetsAndGets)
	{
		initApi();
		doAppConfiguration();

		//-----------------------------------------------------------------------
		//This group gets values from SSO in old desktop.
		//	The values here are from that SSO data
		//-----------------------------------------------------------------------

		//TODO: Not using HTTP Tunnel?
		ServerInfoList serversIn = getServers();

		getApi()->addServers( serversIn );			//Bulk add servers, typically from SSO.
		ServerInfoList serversOut = getApi()->getServers();
		//TODO: Verify all serversIn are in serversOut.  There may be more in serversOut;

//		void addTunnel( const std::string& address, unsigned int port, bool ssl );	//VOXOX - JRT - 2012.02.03 - Not currently called anywhere.
		//No matching get

//		void addSipProxy( const std::string& server, unsigned int serverPort );	//Tested above
		//No matching get

		TlsInfo tlsInfoIn = getTlsInfo();

		getApi()->setTlsInfo( tlsInfoIn );
		TlsInfo tlsInfoOut = getApi()->getTlsInfo();
		//TODO: Compare

		//JRT - 2015.07.02 - We changed how we store configuration items in SipAgentApiWrapper.   TODO: Update this test.
		//Discovery Topology
		//getApi()->setDiscoveryTopology( mTopology );
		//ISipAgent::Topology topoOut = getApi()->getDiscoveryTopology();
		//Assert::AreEqual( (int)mTopology, (int)topoOut, L"Discovery Topology mismatch.", LINE_INFO() );

		//getApi()->enableIceMedia( mEnableIceMedia );
		//bool enableIceMediaOut = getApi()->getEnableIceMedia();
		//Assert::AreEqual( mEnableIceMedia, enableIceMediaOut, L"Discovery Topology EnableProxyMedia mismatch.", LINE_INFO() );

		//getApi()->enableIceSecurity( mEnableIceSecurity );
		//bool enableIceSecurityOut = getApi()->getEnableIceSecurity();
		//Assert::AreEqual( mEnableIceSecurity, enableIceSecurityOut, L"Discovery Topology EnablSecurity mismatch.", LINE_INFO() );

		//getApi()->setTopologyEncryption( mTopologyEncryption );
		//bool topoEncryptionOut = getApi()->getTopologyEncryption();
		//Assert::AreEqual( mTopologyEncryption, topoEncryptionOut, L"Discovery Topology Encryption mismatch.", LINE_INFO() );
		//
		//getApi()->setTopologyTurn( mTopologyTurn );
		//bool topoTurnOut = getApi()->getTopologyTurn();
		//Assert::AreEqual( mTopologyTurn, topoTurnOut, L"Discovery Topology Turn mismatch.", LINE_INFO() );

		//getApi()->setSipUdp( mSipUdp );
		//bool sipUpdOut = getApi()->getSipUdp();
		//Assert::AreEqual( mSipUdp, sipUpdOut, L"SIP UDP mismatch.", LINE_INFO() );

		//getApi()->setSipTcp( mSipTcp );
		//bool sipTcpOut = getApi()->getSipTcp();
		//Assert::AreEqual( mSipTcp, sipTcpOut, L"SIP TCP mismatch.", LINE_INFO() );

		//getApi()->setSipTls( mSipTls );
		//bool sipTlsOut = getApi()->getSipTls();
		//Assert::AreEqual( mSipTls, sipTlsOut, L"SIP TLS mismatch.", LINE_INFO() );

		//getApi()->setKeepAlive( mSipKeepAlive );
		//bool sipKeepAliveOut = getApi()->getKeepAlive();
		//Assert::AreEqual( mSipKeepAlive, sipKeepAliveOut, L"SIP KeepAlive mismatch.", LINE_INFO() );

		//getApi()->setUseRport( mSipUseRport );
		//bool sipUseRportOut = getApi()->getUseRport();
		//Assert::AreEqual( mSipUseRport, sipUseRportOut, L"SIP UseRport mismatch.", LINE_INFO() );
		//
		//getApi()->setRegistrationRefreshInterval( mRegistrationRefreshInterval );
		//int regRefreshIntervalOut = getApi()->getRegistrationRefreshInterval();
		//Assert::AreEqual( mRegistrationRefreshInterval, regRefreshIntervalOut, L"SIP Reg. Refresh Interval mismatch.", LINE_INFO() );

		//getApi()->setFailureInterval( mFailureInterval );
		//int failureIntervalOut = getApi()->getFailureInterval();
		//Assert::AreEqual( mFailureInterval, failureIntervalOut, L"SIP Failure Interval mismatch.", LINE_INFO() );

		//getApi()->setOptionsKeepAliveInterval( mOptionKeepAlive );
		//int optionKeepAliveOut = getApi()->getOptionsKeepAliveInterval();
		//Assert::AreEqual( mOptionKeepAlive, optionKeepAliveOut, L"SIP Options Keep Alive Internal mismatch.", LINE_INFO() );

		//getApi()->setMinSessionTime( mMinimumSessionTime );
		//int minSessionTimeOut = getApi()->getMinSessionTime();
		//Assert::AreEqual( mMinimumSessionTime, minSessionTimeOut, L"SIP Min. Session Time mismatch.", LINE_INFO() );

		//getApi()->setSupportSessionTimer( mSupportSessionTimer );
		//bool supportSessionTimerOut = getApi()->getSupportSessionTimer();
		//Assert::AreEqual( mSupportSessionTimer, supportSessionTimerOut, L"SIP Support Session Timer mismatch.", LINE_INFO() );

		//getApi()->setInitiateSessionTimer( mInitiateSessionTimer );
		//bool initiateSessionTimerOut = getApi()->getInitiateSessionTimer();
		//Assert::AreEqual( mInitiateSessionTimer, initiateSessionTimerOut, L"SIP Initiate Session Timer mismatch.", LINE_INFO() );

		//getApi()->setTagProlog( mTagProlog );
		//std::string tagPrologOut = getApi()->getTagProlog();
		//Assert::AreEqual( mTagProlog, tagPrologOut, L"SIP TagProlog mismatch.", LINE_INFO() );

		//getApi()->setSipOptionRegisterTimeout( mRegisterTimeout );
		//unsigned int registerTimeoutOut = getApi()->getSipOptionRegisterTimeout();
		//Assert::AreEqual( mRegisterTimeout, registerTimeoutOut, L"SIP Option Register Timeout mismatch.", LINE_INFO() );

		//getApi()->setSipOptionPublishTimeout( mPublishTimeout );
		//unsigned int publishTimeoutOut = getApi()->getSipOptionPublishTimeout();
		//Assert::AreEqual( mPublishTimeout, publishTimeoutOut, L"SIP Option Publish Timeout mismatch.", LINE_INFO() );

		//getApi()->setSipOptionUseOptionsRequest( mUseOptionsRequest );
		//bool useOptionsRequestOut = getApi()->getSipOptionUseOptionsRequest();
		//Assert::AreEqual( mUseOptionsRequest, useOptionsRequestOut, L"SIP Use Options Request mismatch.", LINE_INFO() );

		//getApi()->setSipOptionP2pPresence( mOptionsP2PPresence );
		//bool optionsP2PPresenceOut = getApi()->getSipOptionP2pPresence();
		//Assert::AreEqual( mOptionsP2PPresence, optionsP2PPresenceOut, L"SIP Options P2P Presence mismatch.", LINE_INFO() );

		//getApi()->setSipOptionUseTypingState( mOptionsUseTypingState );
		//bool optionsUseTypingStateOut = getApi()->getSipOptionUseTypingState();
		//Assert::AreEqual( mOptionsUseTypingState, optionsUseTypingStateOut, L"SIP Options Typing State mismatch.", LINE_INFO() );

		//getApi()->setSipOptionChatWithoutPresence( mChatWithoutPresence );
		//bool chatWithoutPresenceOut = getApi()->getSipOptionChatWithoutPresence();
		//Assert::AreEqual( mChatWithoutPresence, chatWithoutPresenceOut, L"SIP Options Chat Without Presence mismatch.", LINE_INFO() );

		//Not called in old desktop app.  Do we need this?
//		void		 setSipOptionFailureTimeout     ( unsigned int value );		//In seconds
//		unsigned int getSipOptionFailureTimeout     () 

		terminateApi();		//Call this or unit test keeps running
	}

	//-------------------------------------------------------------------------
	//This method tests values set during app level configuration.
	//-------------------------------------------------------------------------
	TEST_METHOD(testAppLevelConfigurationSetsAndGets)
	{
		//TODO: Update this unit test
		//Called from app initialization.  Uses stored UUID or creates a new one if one does not exist.
		//getApi()->setSipOptionUuid( mSipUserUuid );
		//std::string uuidOut1 = getApi()->getSipOptionUuid();
		//std::string uuidOut2 = getApi()->getUUID();				//Same as getSipOptionUuid()
		//Assert::AreEqual( mSipUserUuid, uuidOut1, L"SIP Option UUID mismatch 1.", LINE_INFO() );
		//Assert::AreEqual( uuidOut1,		uuidOut2, L"SIP Option UUID mismatch 2.", LINE_INFO() );

		//terminateApi();		//Call this or unit test keeps running
	}


	//-------------------------------------------------------------------------
	//This method tests values set during phone line configuration.
	//	In old desktop app, many of these values came from Config file,
	//	but that does NOT mean they were all user configureable.
	//-------------------------------------------------------------------------
	TEST_METHOD(testPhoneLineConfigurationSetsAndGets)
	{
		initApi();
		doAppConfiguration();
		doSsoConfiguration();

		//TODO: Are we supporting HTTP proxy server in new client?
//		void addProxy( const std::string& address, unsigned int port, const std::string& login, const std::string&  password, ISipAgent::HTTPProxy httpProxyType );
		//No matching get

		//TODO: update this unit test for Call settings
		//getApi()->enableAEC( mAec );
		//bool aecOut = getApi()->isAecEnabled();
		//Assert::AreEqual( mAec, aecOut, L"Auto. Echo Cancellation (AEC) mismatch.", LINE_INFO() );

		//getApi()->enableAGC( mAgc );
		//bool agcOut = getApi()->isAgcEnabled();
		//Assert::AreEqual( mAgc, agcOut, L"Auto. Gain Control (AGC) mismatch.", LINE_INFO() );

		//getApi()->enableNoiseSuppression( mNoiseSuppression );
		//bool noiseSuppressionOut = getApi()->isNoiseSuppressionEnabled();
		//Assert::AreEqual( mNoiseSuppression, noiseSuppressionOut, L"Noise Suppression mismatch.", LINE_INFO() );

		//getApi()->setCallsEncryption( mCallEncryption );
		//ISipAgent::Encryption valueOut = getApi()->getCallsEncryption();
		//Assert::AreEqual( (int)mCallEncryption, (int)valueOut, L"Call Encryption mismatch.", LINE_INFO() );

		//----------------------------------------------------------
		//Audio settings
		//----------------------------------------------------------
		// Sets the audio codec list sorted by preference
		//bool comfortNoiseIn		= true;
		//bool telephoneEventsIn	= true;
		//bool redundantAudioIn	= true;
		//StringList audioCodecsIn;			//This is typically empty, if user has never re-ordered codecs, which I don't think we are allowing.
		//ISipAgent::Result result1 = getApi()->setAudioCodecs( audioCodecsIn, comfortNoiseIn, telephoneEventsIn, redundantAudioIn );

		//if ( result1 == ISipAgent::RESULT_OK )
		//{
		//	StringList audioCodecsOut1 = getApi()->getAudioCodecList();
		//	//TODO: How to test this?

		//	bool cnOut  = false;
		//	bool teOut  = false;
		//	bool redOut = false;
		//	std::string	codecsOut = getApi()->getAudioCodecs( &cnOut, &teOut, &redOut );	//This is a comma-delimited list.  TODO: How to test.
	
		//	//TODO
		//	Assert::AreEqual( comfortNoiseIn,    cnOut,  L"Comfort Noise mismatch.",	LINE_INFO() );
		//	Assert::AreEqual( telephoneEventsIn, teOut,  L"Telephone Events mismatch.", LINE_INFO() );
		//	Assert::AreEqual( redundantAudioIn,  redOut, L"RED mismatch.",				LINE_INFO() );

		//
		//	//Both gets need to be tested
		//}
		//else
		//{
		//	Assert::Fail( L"setAudioCodecs failed.", LINE_INFO() );
		//}

		//getApi()->setPreferredAudioCodecOrder();	//This order is hard-coded in SIP stack

		//StringList preferredAudioCodecs = getApi()->getPreferredAudioCodecList();	//Used in Config->Audio->Advanced.
		//StringList defaultAudioCodecs   = getApi()->getDefaultAudioCodecList();		//Used interally by getPreferredAudioCodecList().  May not need.
//		std::string audioCodecs			= getApi()->getAudioCodecListAsString();	//Not called in old app
		//TODO: How to test this?  Used only in UI?
		//All combinations need to be tested

		//Get list of devices we can play with.
//		AudioDeviceList inputDevices  = getApi()->getSystemAudioInputDevices() ;			//Used in Config->Audio, PhoneCall UI
//		AudioDeviceList outputDevices = getApi()->getSystemAudioOutputDevices();			//Used in Config->Audio, PhoneCall UI

//		AudioDevice inputDevice  = inputDevices.front();		//Just use first one
//		AudioDevice outputDevice = outputDevices.front();		//Just use first one
//		AudioDevice ringerDevice = outputDevices.front();		//Just use first one

		//Sets the call ringer/alerting device.
//		getApi()->setRingerOutputAudioDevice( ringerDevice );
//		AudioDevice ringerOut = getApi()->getRingerOutputAudioDevice();
//		bool ringerEqual = (ringerDevice == ringerOut);
//		Assert::IsTrue( ringerEqual, L"Ringer device mismatch.", LINE_INFO() );

		//Sets the call input device (in-call microphone).
//		getApi()->setCallInputAudioDevice( inputDevice );
//		AudioDevice inputOut = getApi()->getCallInputAudioDevice();
//		bool inputEqual = (inputDevice == inputOut);
//		Assert::IsTrue( inputEqual, L"Input device mismatch.", LINE_INFO() );

		//Sets the call output device (in-call speaker).
//		getApi()->setCallOutputAudioDevice( outputDevice );
//		AudioDevice outputOut = getApi()->getCallOutputAudioDevice();
//		bool outputEqual = (outputDevice == outputOut);
//		Assert::IsTrue( outputEqual, L"Output device mismatch.", LINE_INFO() );

//		bool testFail = (outputDevice == inputDevice);
//		Assert::IsFalse( testFail, L"AudioDevice operator==() does not work.", LINE_INFO() );



		//----------------------------------------------------------
		//Video settings - not supported at this time
		//----------------------------------------------------------
//		void					setVideoDevice( const std::string& deviceName );
//		std::string						getVideoDevice();
//
//		void					setVideoQuality( EnumVideoQuality::VideoQuality videoQuality );
//		EnumVideoQuality::VideoQuality	getVideoQuality();
//
//		void					flipVideoImage( bool flip );
//		bool							shouldFlipVideoImage();

		terminateApi();		//Call this or unit test keeps running
	}

	//-------------------------------------------------------------------------
	//This method tests misc. API calls
	//-------------------------------------------------------------------------
//	TEST_METHOD(testMisc)
//	{
//		initApi();
//		doAppConfiguration();
//		doSsoConfiguration();
//
//		std::string	realmOut = getApi()->getRealm();	//Not used in old app.  Returns empty string.
	
//		terminateApi();		//Call this or unit test keeps running
//	}

		//NOT SUPPORTED

		///**
		// * @name IMConnect Implementation
		// * @{
		// */
		//void connect();
		//void disconnect(bool force = false);
		///** @} */

		///**
		// * @name IMChat Implementation
		// * @{
		// */
		//void sendMessage
		//(
		//    IMChatSession&      chatSession,
		//    const std::string&  message
		//);
		//void changeTypingState
		//(
		//    IMChatSession&      chatSession,
		//    IMChat::TypingState state
		//);
		//void createSession
		//(
		//    IMChat&             imChat,
		//    const IMContactSet& imContactSet,
		//    IMChat::IMChatType  imChatType,
		//    const std::string&  userChatRoomName
		//);
		//void closeSession(IMChatSession& chatSession);
		//void addContact
		//(
		//    IMChatSession&      chatSession,
		//    const std::string&  contactId
		//);
		//void removeContact
		//(
		//    IMChatSession&      chatSession,
		//    const std::string&  contactId
		//);
		///** @} */

		///**
		// * @name IMPresence Implementation
		// * @{
		// */
		//void changeMyPresence
		//(
		//    EnumPresenceState::PresenceState    state,
		//    const std::string&                  note
		//);

		//void setMyIcon(const std::string& iconFilename);

		//void sendMyIcon(const std::string& contactId);

		//void subscribeToPresenceOf(const std::string& contactId);

		//void unsubscribeToPresenceOf(const std::string& contactId);

		//void blockContact(const std::string& contactId);

		//void unblockContact(const std::string& contactId);

		//void acceptSubscription(int sid);

		//void rejectSubscription(int sid);
};

}	//namespace SipAgentApiTest