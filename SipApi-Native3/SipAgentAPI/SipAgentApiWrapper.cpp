
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by Roman Sphount, jeff.theinert@voxox.com, Sep 2014
 */

#include "SipAgentApiWrapper.h"
#include "SipAgentHelpers.h"	//AutoRef, RefCounterHelper, LibraryHelper
#include "SipAgentUtils.h"		//sipAgentResult
#include "SipAgentCallbacks.h"	//Our defined callbacks
#include "CallStateInfo.h"
#include "StringUtil.h"

#include <string>
#include <stdio.h>

#include <mutex>	//Recursive mutex and lock_guard

class MyRecursiveMutex : public std::recursive_mutex
{
};

typedef std::lock_guard<MyRecursiveMutex> LockGuard;	//Formerly RecursiveMutex::ScopedLock

#ifdef _WINDOWS
#   define WIN32_LEAN_AND_MEAN
#   include <windows.h>
#   define snprintf _snprintf
#endif

#define STRINGIZE2(x) #x
#define STRINGIZE(x) STRINGIZE2(x)

/********************************* Local Constants ***********************************************/

//static const char* const s_pidfPresenceTemplate =
//"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
//"<presence xmlns=\"urn:ietf:params:xml:ns:pidf\"\n"
//"entity=\"%s\">\n"
//"<tuple id=\"azersdqre\">\n"
//"<status><basic>%s</basic></status>\n"
//"<note>%s</note>\n"
//"<contact priority=\"1\">%s"
//"</contact>\n"
//"</tuple>\n"
//"</presence>\n";

//static const char* const s_pidfContentType = "application/pidf+xml";

#ifdef _WINDOWS
static const char* const s_sipAgentLibrary = "SipAgent.dll";
#endif

enum { URI_BUFF_SIZE      =  256 };
enum { PRESENCE_BODY_SIZE = 1024 };

class MyLogSendCallback : public LogSendCallback
{
public:
	MyLogSendCallback( SipAgentApiWrapper* sipWrapper)
	{
		mSipWrapper = sipWrapper;
	}

	void sendLog( const LogEntry& logEntry )
	{
		if ( mSipWrapper != nullptr )
		{
			mSipWrapper->sendLog( logEntry );
		}
	}

private:
	SipAgentApiWrapper* mSipWrapper;
};



/********************************* Local Functions ***********************************************/

//static bool sendPresence( ISipAgent::ServerSubscription* subscription, bool online, const char* note )
//{
//    char bodyBuf[PRESENCE_BODY_SIZE];
//
//    AutoRef<ISipAgent::NameAddress> remoteContact(subscription->getRemoteContact(), false);
//    if(remoteContact == 0)
//    {
//        LOG_ERROR("SIP subscription get remote conact failed");
//        return false;
//    }
//
//    const char* uri = remoteContact->getURI();
//    snprintf(bodyBuf, sizeof(bodyBuf), s_pidfPresenceTemplate, uri, online ? "open" : "closed", note, uri);
//
//    ISipAgent::Result result = subscription->sendRequest
//    (
//        "NOTIFY",
//        s_pidfContentType,
//        bodyBuf,
//        strlen(bodyBuf),
//        0,                  // headers
//        0,                  // connectTimeout
//        0,                  // timeout
//        0                   // callbacks
//    );
//
//    if(result != ISipAgent::RESULT_OK)
//    {
//        LOG_ERROR("SIP subscription send request failed: %s", SipAgentUtils::sipAgentResult(result));
//        return false;
//    }
//    return true;
//}


//-------------------------------------------------------------------------------------------------
// SipAgentApiWrapper
//-------------------------------------------------------------------------------------------------

SipAgentApiWrapper::SipAgentApiWrapper()
 :  d_agent(nullptr)
,	d_libHelper(nullptr)
,   d_appEventHandler(nullptr)
,	d_logSendCallback(nullptr)
,	d_mutex(nullptr)				//See note in header
,   d_registered(false)
,   d_mustReinitNetwork(false)

,   d_aec(true)
,   d_agc(false)
,   d_noiseSuppression(false)

//,   d_videoDevice(),                        //std::string
,   d_videoQuality( EnumVideoQuality::VideoQualityNormal)
,   d_flipVideoImage(false)
{
	d_mutex = new MyRecursiveMutex();

	d_sipConfig.setTlsInfo( &d_tlsInfo );
	d_sipConfig.setServers( &d_servers );
}

//Start event handlers
void SipAgentApiWrapper::connectedEvent()
{
	if ( d_appEventHandler != nullptr )
	{
		d_appEventHandler->connectedEvent();
	}
}

void SipAgentApiWrapper::disconnectedEvent( bool connectionError, const std::string& reason )
{
	if ( d_appEventHandler != nullptr )
	{
		d_appEventHandler->disconnectedEvent( connectionError, reason );
	}
}

void SipAgentApiWrapper::phoneCallStateChangedEvent( int callId, EnumPhoneCallState::PhoneCallState state, const std::string & from, int statusCode )
{
	if ( d_appEventHandler != nullptr )
	{
		d_appEventHandler->phoneCallStateChangedEvent( callId, state, from, statusCode );
	}
}

void SipAgentApiWrapper::phoneLineStateChangedEvent( int lineId, EnumPhoneLineState::PhoneLineState state )
{
	if ( d_appEventHandler != nullptr )
	{
		d_appEventHandler->phoneLineStateChangedEvent( lineId, state );
	}
}

void SipAgentApiWrapper::infoRequestReceivedEvent( int callId, const std::string& content, const std::string& contentType )
{
	if ( d_appEventHandler != nullptr )
	{
		d_appEventHandler->infoRequestReceivedEvent( callId, content, contentType );
	}
}

void SipAgentApiWrapper::sendLog( const LogEntry& logEntry )
{
	if ( d_appEventHandler != nullptr )
	{
		d_appEventHandler->sendLog( logEntry );
	}
}

//-----------------------------------------------------------------------------
//End event handlers
//-----------------------------------------------------------------------------


std::string SipAgentApiWrapper::makeSipAddress(const std::string& contactId)
{
    std::string sipAddress;

    if(!contactId.empty())
    {
        if ( StringUtil::beginsWith( contactId, "sip:", false ) )
            sipAddress = "sip:" + contactId;
        else
            sipAddress = contactId;

        const char* p = contactId.c_str();
        while(*p && *p != '@')
        {
            ++p;
        }

        if(*p == 0) // did not find '@'
        {
            sipAddress += "@" + d_sipConfig.getRealm();
        }

        sipAddress = StringUtil::toLowerCase( sipAddress );
    }

    return sipAddress;
}

void SipAgentApiWrapper::doTerminate()
{
    if (d_agent)
    {
		d_agent->clearLines();		//PJSIP: This will also clear all calls.  May not need this here.

        LOG_DEBUG("Terminating SIP Agent");
		delete d_agent;
        d_agent = nullptr;
    }
}

SipAgentApiWrapper* SipAgentApiWrapper::getInstance()
{
    static SipAgentApiWrapper s_sipAgentApiInstance;
    return &s_sipAgentApiInstance;
}

SipAgentApiWrapper::~SipAgentApiWrapper()
{
    doTerminate();
	delete d_mutex;
//	delete d_libHelper;		//Crashes on logging, so accept memory leak for now, or remove logging
}

void SipAgentApiWrapper::setLogLevel( int level )
{
	LockGuard lock( *d_mutex );
    
	d_sipConfig.setLogLevel( level );
}

void SipAgentApiWrapper::init( int logLevel )
{
	d_logSendCallback = new MyLogSendCallback( this );
	LogEntry::setLogSendCallback( d_logSendCallback );

	setLogLevel( logLevel );

	LockGuard lock( *d_mutex );
    if(d_agent)
    {
        LOG_ERROR("Sip wrapper is already initialized");
        return;
    }

	if ( createAgent() )
	{
		d_sipConfig.ensureValidUuid();

		d_agent->configure( &d_sipConfig );
	}
}

//-----------------------------------------------------------------------------

//This is key interface into the main SIP code via ISipAgent interface definitions.
bool SipAgentApiWrapper::createAgent()
{
	bool			success = false;
	std::string		errMsg;
	
	d_libHelper = new LibraryHelper();	//Will be removed in dtor.

	if ( d_libHelper )
	{
		//Load library 
		if ( d_libHelper->init(s_sipAgentLibrary) )
		{
			//Locate 'sipAgentCreate' function
			ISipAgent::CreateFunction createFunc = d_libHelper->getSymbol<ISipAgent::CreateFunction>("sipAgentCreate");

			if ( createFunc != nullptr)
			{
				VxCallback::AgentCallbacks* callbacks = new VxCallback::AgentCallbacks();

				if ( callbacks != nullptr)
				{
					ISipAgent::Result result = createFunc(&d_agent, callbacks, &d_sipConfig );

					if ( result == ISipAgent::RESULT_OK )
					{
						success = true;
					}
					else
					{
						errMsg = "Cannot create SIP agent";
					}
				}
				else
				{
					errMsg = "Cannot allocate agent callbacks.";
				}
			}
			else
			{
				errMsg = "Cannot find creatSipAgent function.";
			}
		}
		else
		{
			errMsg = "Cannot load Sip Agent library.";
		}
	}
	else
	{
		errMsg = "Cannot allocate library helper.";
	}

	if ( ! success )
	{
		doTerminate();
		LOG_ERROR( errMsg );
	}

	return success;
}

void SipAgentApiWrapper::terminate()
{
	LockGuard lock( *d_mutex );
	doTerminate();

//	agent->shutdown();	//TODO: This Roman's SIP stack and hangs.  Commenting for now when using PJSIP.
}

//-----------------------------------------------------------------------------

bool SipAgentApiWrapper::isInitialized()
{
	LockGuard lock( *d_mutex );
    return(d_agent != 0);
}

//-----------------------------------------------------------------------------

std::string SipAgentApiWrapper::formatSipUserAgent()
{
    if ( d_sipConfig.getSipUserAgent().empty() )
    {
        const char* version = STRINGIZE(VOXOXVERSION);
        const char* rev     = STRINGIZE(SUBVERSION_REVISION);

        char ua[50] = "Voxox ";    //TODO JRT: Do we need to co-brand this?
        strcat( ua, version );
        strcat( ua, ".");
        strcat( ua, rev);

        d_sipConfig.setSipUserAgent( ua );
    }

    return d_sipConfig.getSipUserAgent();
}

//-----------------------------------------------------------------------------

std::string SipAgentApiWrapper::formatSdpOrigin()
{
    std::string result = StringUtil::replace( formatSipUserAgent(), " ", "_", true );

    d_sipConfig.setSdpOrigin( result );

    return d_sipConfig.getSdpOrigin();
}

//-----------------------------------------------------------------------------
//TODO-PJSIP: Move this to SipConfig
std::string SipAgentApiWrapper::formatSipUserUrl( const std::string& displayName, const std::string& userName, const ISipAgent::IServerInfo* registrarServer )
{
    std::string result;

    result += "\"" + displayName + "\"";
//    result += " <" + registrarServer->getSipProtocol();	//TODO-PJSIP: having issue with 'sips:', so force 'sip:' for now.
    result += " <sip:";
    result += userName;
    result += "@";
    result += registrarServer->getAddress();
    
	if(registrarServer->getPort())
        result += ":" + std::to_string( registrarServer->getPort() );

    result += ">";

	d_sipConfig.setSipUserUrl( result );

    return d_sipConfig.getSipUserUrl();
}

//-----------------------------------------------------------------------------
//TODO-PJSIP: Move this to SipConfig
std::string SipAgentApiWrapper::formatProxyUrl( const ISipAgent::IServerInfo* proxyServer )
{
    std::string result;
    std::string address = proxyServer->getAddress();

    if ( !address.empty() )
    {
        result += proxyServer->getSipProtocol();
        result += address;

        if(proxyServer->getPort())
            result += ":" + std::to_string( proxyServer->getPort() );
        
		result += ";lr";

        d_sipConfig.setProxyUrl( result );
    }

    return d_sipConfig.getProxyUrl();
}

//-----------------------------------------------------------------------------

bool SipAgentApiWrapper::setCredentials( const std::string& displayName, const std::string& userName, const std::string& identity, 
										 const std::string& password, 	 const std::string& realm )
{
	bool result = false;

	d_sipConfig.setDisplayName( displayName );
	d_sipConfig.setUserName   ( userName );
	d_sipConfig.setIdentity   ( identity );
	d_sipConfig.setPassword   ( password );
	d_sipConfig.setRealm	  ( realm    );

	ISipAgent::IServerInfo* registrarServer = d_sipConfig.getServers()->getFirstSipRegistrar();
	ISipAgent::IServerInfo* proxyServer     = d_sipConfig.getServers()->getFirstSipProxy();

	if ( registrarServer != nullptr && proxyServer != nullptr )
	{
	    formatSipUserAgent();		//SIP User Agent Header
	    formatSdpOrigin();			//SDP origin
	    formatSipUserUrl( displayName, userName, registrarServer );
	    formatProxyUrl  ( proxyServer );

		result = true;
	}
	else
	{
		LOG_ERROR( "Must add Registrar and Proxy servers before setting credentials.");
	}

	return result;
}

int SipAgentApiWrapper::addVirtualLine()
{
	LockGuard lock( *d_mutex );

    if ( d_agent == nullptr )
    {
        LOG_ERROR("Sip wrapper is not initialized");
        return (-1);
    }

	int lineId = -1;

   VxCallback::LineCallbacks* lineCallbacks = new VxCallback::LineCallbacks(this, lineId);

    if(lineCallbacks == 0)
    {
        LOG_ERROR("Cannot allocate line LineCallbacks");
        return (-1);
    }

    lineId = d_agent->createLine( lineCallbacks);		//This creates, populates, and registers the 'line/account'
	lineCallbacks->setLineId( lineId );

    if ( lineId < 0 )
    {
        LOG_ERROR( "Cannot create SIP line" );
        return (-1);
    }

    return lineId;
}

//-----------------------------------------------------------------------------

bool SipAgentApiWrapper::populateLineConfig( ISipAgent::Line* line )	
{
	ISipAgent::Result result = line->configure( &d_sipConfig );

    return true;
}

//-----------------------------------------------------------------------------

int SipAgentApiWrapper::registerVirtualLine(int lineId)
{
    phoneLineStateChangedEvent( /* *this,*/ lineId, EnumPhoneLineState::PhoneLineStateProgress);

	LockGuard lock( *d_mutex );

	return ( d_agent->registerLine( lineId ) ? 0 : -1 );
}

void SipAgentApiWrapper::removeVirtualLine( int lineId, bool force )
{
    {
		LockGuard lock( *d_mutex );
        d_mustReinitNetwork = force;
        d_registered = false;

		d_agent->removeLine( lineId, force );
    }

    phoneLineStateChangedEvent( /* *this,*/ lineId, EnumPhoneLineState::PhoneLineStateClosed);
}

int SipAgentApiWrapper::makeCall( int lineId, const std::string& sipAddress, bool enableVideo )
{
	LockGuard lock( *d_mutex );

    LOG_DEBUG("call=" + sipAddress);			

	VxCallback::CallCallbacks*	callbacks = new VxCallback::CallCallbacks(this, -1);
	int							callId	  = d_agent->makeCall( lineId, callbacks, sipAddress, enableVideo );

	callbacks->setCallId( callId );

	return callId;
}

std::string SipAgentApiWrapper::getSipCallId(int callId)
{
	LockGuard lock( *d_mutex );
	
	std::string sipCallId = d_agent->getSipCallId( callId );

	return sipCallId;
}


void SipAgentApiWrapper::sendRingingNotification( int callId, bool enableVideo )
{
	LockGuard lock( *d_mutex );

	d_agent->sendRingingNotification( callId, enableVideo );
}

void SipAgentApiWrapper::acceptCall( int callId, bool enableVideo )
{
	LockGuard lock( *d_mutex );

	d_agent->acceptCall( callId, enableVideo );
}

void SipAgentApiWrapper::rejectCall(int callId)
{
	LockGuard lock( *d_mutex );

	d_agent->rejectCall( callId );
}

void SipAgentApiWrapper::closeCall(int callId)
{
	LockGuard lock( *d_mutex );
	d_agent->closeCall( callId );
}

void SipAgentApiWrapper::holdCall(int callId)
{
	LockGuard lock( *d_mutex );

	d_agent->holdCall( callId );
}

void SipAgentApiWrapper::resumeCall(int callId)
{
	LockGuard lock( *d_mutex );

	d_agent->resumeCall( callId );
}

void SipAgentApiWrapper::muteCall( int callId, bool muteOn )
{
	LockGuard lock( *d_mutex );

	d_agent->muteCall( callId, muteOn );
}

void SipAgentApiWrapper::setCallInputVolume( int callId, unsigned int volume )
{
	LockGuard lock( *d_mutex );

	d_agent->setCallInputVolume( callId, volume );
}

void SipAgentApiWrapper::blindTransfer( int callId, const std::string& sipAddress )
{
	LockGuard lock( *d_mutex );

	d_agent->blindTransferCall( callId, sipAddress );
}

void SipAgentApiWrapper::playDtmf( int callId, char dtmfIn )
{
	LockGuard lock( *d_mutex );

	std::string dtmf( &dtmfIn );
	
	d_agent->playDtmfOnCall( callId, dtmf );
}

void SipAgentApiWrapper::playSoundFile( int callId, const std::string& soundFile )
{
	LockGuard lock( *d_mutex );

	d_agent->playSoundFileOnCall( callId, soundFile );
}

bool SipAgentApiWrapper::isCallEncrypted(int callId)
{
	LockGuard lock( *d_mutex );

	bool result = d_agent->isCallEncrypted( callId );

    return result;
}

ISipAgent::ICallCallbacks* SipAgentApiWrapper::newCall		//TODO-PJSIP: Do we need this?
(
    int                 lineID,
    ISipAgent::Call*    call
) throw()
{
    VxCallback::CallCallbacks* callbacks = nullptr;

    try
    {
		if ( call->getRemoteParty() == nullptr )
		{
            LOG_DEBUG("SIP call cannot get remote party");
            call->disconnect();
            return 0;
		}

		std::string remoteParty( call->getRemoteParty() );	//JRT - 2015.06.18

        LOG_DEBUG("call=%s", remoteParty.c_str() );

        int cid = 0;
        {
			LockGuard lock( *d_mutex );

            callbacks = new VxCallback::CallCallbacks( this, cid );
            if(callbacks == 0)
            {
                LOG_ERROR("Cannot allocate Call Callbacks");
                call->disconnect();
                return 0;
            }
        }

        phoneCallStateChangedEvent( /* *this, */ cid, EnumPhoneCallState::PhoneCallStateDialing, remoteParty, 0);
    }
    catch(...)
    {
        call->disconnect();
        LOG_FATAL("Unexpected Exception.");
    }

    return callbacks;
}

ISipAgent::ICallCallbacks* SipAgentApiWrapper::newCallCallback( int callId ) throw()
{
    VxCallback::CallCallbacks* callbacks = new VxCallback::CallCallbacks( this, callId );
	return callbacks;
}

void SipAgentApiWrapper::setCallsEncryption( ISipAgent::Encryption val )
{
	LockGuard lock( *d_mutex );
    d_sipConfig.setEncryption( val );
}

void SipAgentApiWrapper::addProxy( const std::string& address, unsigned port, const std::string& login, const std::string& password, ISipAgent::HTTPProxy httpProxyType )
{
	LockGuard lock( *d_mutex );

    d_servers.addHttpProxy( address, port, login, password, httpProxyType );
}

void SipAgentApiWrapper::addTunnel( const std::string& address, unsigned port, bool ssl )
{
    assert(false);        //VOXOX - Not supporting right now.

//    bool result = _servers.addHttpTunnel( address, port, ssl );
}

void SipAgentApiWrapper::addSipProxy( const std::string& server, unsigned serverPort /*, unsigned  localPort */ )
{
	LockGuard lock( *d_mutex );

    d_servers.addSipProxy( server, serverPort );    //, localPort );
}

void SipAgentApiWrapper::setTlsInfo( const TlsInfo& tlsInfo )
{
	LockGuard lock( *d_mutex );
    d_tlsInfo = tlsInfo;
	d_sipConfig.setTlsInfo( &d_tlsInfo );
}

//-----------------------------------------------------------------------------

TlsInfo SipAgentApiWrapper::getTlsInfo() const
{
	LockGuard lock( *d_mutex );
    return d_tlsInfo;
}

//-----------------------------------------------------------------------------

void SipAgentApiWrapper::addServers( const ServerInfoList& servers )
{
	LockGuard lock( *d_mutex );
    d_servers.merge( servers );
}

//-----------------------------------------------------------------------------

ServerInfoList SipAgentApiWrapper::getServers() const
{ 
	LockGuard lock( *d_mutex );
	return d_servers; 
}

//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------

void SipAgentApiWrapper::setCallInputAudioDevice(const AudioDevice& device)
{
	LockGuard lock( *d_mutex );

	d_inputAudioDevice = device;

    if ( d_agent )
    {
        d_agent->setRecordingDevice( d_inputAudioDevice.getGuid() );
    }
}

//-----------------------------------------------------------------------------

void SipAgentApiWrapper::setCallOutputAudioDevice(const AudioDevice& device)
{
	LockGuard lock( *d_mutex );

	d_outputAudioDevice = device;

    if ( d_agent )
    {
        d_agent->setPlayoutDevice( d_outputAudioDevice.getGuid() );
    }
}

//-----------------------------------------------------------------------------

void SipAgentApiWrapper::setRingerOutputAudioDevice(const AudioDevice& device)
{
	LockGuard lock( *d_mutex );

	d_ringerAudioDevice = device;            //TODO JRT/Roman: do we support this?

    if ( d_agent )
    {
        d_agent->setRingingDevice( d_ringerAudioDevice.getGuid() );
    }
}

//-----------------------------------------------------------------------------

void SipAgentApiWrapper::enableAEC( bool enable )
{
	LockGuard lock( *d_mutex );

	d_aec = enable;

    if ( d_agent )
    {
        d_agent->setEC( enable );
    }
}

//-----------------------------------------------------------------------------

void SipAgentApiWrapper::enableAGC( bool enable )
{
	LockGuard lock( *d_mutex );

	d_agc = enable;

    if ( d_agent )
    {
        d_agent->setAGC( enable );
    }
}

//-----------------------------------------------------------------------------

void SipAgentApiWrapper::enableNoiseSuppression( bool enable )
{
	LockGuard lock( *d_mutex );

	d_noiseSuppression = enable;

    if ( d_agent )
    {
        d_agent->setNS( enable );
    }
}

//-----------------------------------------------------------------------------

void SipAgentApiWrapper::setVideoDevice( const std::string& deviceName )
{
	LockGuard lock( *d_mutex );
    d_videoDevice = deviceName;
}

//-----------------------------------------------------------------------------

void SipAgentApiWrapper::setVideoQuality( EnumVideoQuality::VideoQuality videoQuality )
{
	LockGuard lock( *d_mutex );
    d_videoQuality = videoQuality;
}

//-----------------------------------------------------------------------------

void SipAgentApiWrapper::flipVideoImage( bool flip )
{
	LockGuard lock( *d_mutex );
    d_flipVideoImage = flip;
}

//-----------------------------------------------------------------------------
//Added to support unit test
int SipAgentApiWrapper::getLineCount()	const
{
	LockGuard lock( *d_mutex );
	return d_agent->getLineCount();
}

//-----------------------------------------------------------------------------

void SipAgentApiWrapper::registrationChanged( int lineId, ISipAgent::State state, ISipAgent::Event event ) throw()
{
	if ( d_agent && ! d_agent->hasLineId( lineId ) )	//May not need this check, since main app needs to know about registration change.
    {
        LOG_DEBUG("Cannot find line %d", lineId);
        return;
    }

    switch(state)
    {
        case ISipAgent::STATE_PENDING:
        {
            {
				LockGuard lock( *d_mutex );

				d_registered = false;
            }

            phoneLineStateChangedEvent( lineId, EnumPhoneLineState::PhoneLineStateUnknown);
            break;
        }
        case ISipAgent::STATE_ACTIVE:
        {
            bool registered = false;
            {
				LockGuard lock( *d_mutex );

                if(!d_registered)
                {
                    d_registered		= true;
                    registered			= true;
                }
            }

            if(registered)
            {
                phoneLineStateChangedEvent( lineId, EnumPhoneLineState::PhoneLineStateOk);
                connectedEvent();
            }
            break;
        }

        case ISipAgent::STATE_TERMINATING:
            break;

        case ISipAgent::STATE_TERMINATED:
        {
            bool unregistered = false;
            {
				LockGuard lock( *d_mutex );

                if(d_registered)
                {
                    d_registered = false;
                    unregistered = true;
                }
            }

            if(unregistered)
            {
                switch(event)
                {
                    case ISipAgent::EVENT_OK:
                        phoneLineStateChangedEvent( lineId, EnumPhoneLineState::PhoneLineStateClosed);
                        break;
                    case ISipAgent::EVENT_TERMINATED:
                        phoneLineStateChangedEvent( lineId, EnumPhoneLineState::PhoneLineStateClosed);
                        break;
                    case ISipAgent::EVENT_AUTH_ERROR:
                        phoneLineStateChangedEvent( lineId, EnumPhoneLineState::PhoneLineStateAuthenticationError);
                        disconnectedEvent( true, "Bad login or password");
                        break;
                    case ISipAgent::EVENT_TIMEOUT:
                        phoneLineStateChangedEvent( lineId, EnumPhoneLineState::PhoneLineStateTimeout);
                        disconnectedEvent( true, "No response from server (could not connect)");
                        break;
                    case ISipAgent::EVENT_ERROR:
                        phoneLineStateChangedEvent( lineId, EnumPhoneLineState::PhoneLineStateServerError);
                        disconnectedEvent( true, "Unexpected server error");
                        break;
                    case ISipAgent::EVENT_EXPIRED:
                        phoneLineStateChangedEvent( lineId, EnumPhoneLineState::PhoneLineStateClosed);
                        break;
                }
            }
            break;
        }
    }
}

//-----------------------------------------------------------------------------

bool SipAgentApiWrapper::isRegistered() const
{
	LockGuard lock( *d_mutex );
    return d_registered;
}

//-----------------------------------------------------------------------------

bool SipAgentApiWrapper::needHttpProxy() 
{
    return d_servers.hasHttpProxy();
}

//-----------------------------------------------------------------------------

unsigned int SipAgentApiWrapper::getFirstSipServerPort()
{
    ServerInfo* server = d_servers.getFirstSipProxy();

	return ( server == nullptr ? 0 : server->getPort() );
}

//-----------------------------------------------------------------------------


//---------------------------------------------------------------------------------
//Call audio related methods
//---------------------------------------------------------------------------------
//Output
bool SipAgentApiWrapper::setSpeakerVolume( unsigned int  volume )
{
    bool result = false;

    if ( d_agent )
    {
        ISipAgent::Result agentResult = d_agent->setSpeakerVolume( volume );

        if ( agentResult != ISipAgent::RESULT_OK )
        {
            LOG_ERROR( "ISipAgent::Result = " + std::to_string( agentResult ) );
        }

        result = (agentResult == ISipAgent::RESULT_OK);
    }

    return result;
}

//-----------------------------------------------------------------------------

int SipAgentApiWrapper::getSpeakerVolume()
{
    int          result = -1;
    unsigned int volume =  0;

    if ( d_agent )
    {
        ISipAgent::Result agentResult = d_agent->getSpeakerVolume( &volume );

        if ( agentResult == ISipAgent::RESULT_OK )
        {
            result = (int)volume;
        }
        else
        {
            result = -1;
            LOG_ERROR( "ISipAgent::Result = " + std::to_string( agentResult ) );
        }
    }

    return result;
}

//-----------------------------------------------------------------------------
//Ringing
bool SipAgentApiWrapper::setRingingVolume( unsigned int volume )
{
    bool result = false;

    if ( d_agent )
    {
        //Valid range: 0-255 - Does ISipAgent check for this?
        ISipAgent::Result agentResult = d_agent->setRingingVolume( volume );

        if ( agentResult != ISipAgent::RESULT_OK )
        {
            LOG_ERROR( "ISipAgent::Result = " + std::to_string( agentResult ) );
        }

        result = (agentResult == ISipAgent::RESULT_OK);
    }

    return result;
}

//-----------------------------------------------------------------------------

int SipAgentApiWrapper::getRingingVolume()
{
    int               result = -1;
    unsigned int      volume =  0;

    if ( d_agent )
    {
        ISipAgent::Result agentResult = d_agent->getRingingVolume( &volume );

        if ( agentResult == ISipAgent::RESULT_OK )
        {
            result = (int)volume;
        }
        else
        {
            result = -1;
            LOG_ERROR( "ISipAgent::Result = " + std::to_string( agentResult ) );
        }
    }

    return result;
}

//-----------------------------------------------------------------------------
//Input
bool SipAgentApiWrapper::setMicVolume( unsigned int volume )
{
    bool result = false;

    if ( d_agent )
    {
        ISipAgent::Result agentResult = d_agent->setMicVolume( volume );

        if ( agentResult != ISipAgent::RESULT_OK )
        {
            LOG_ERROR( "ISipAgent::Result = " + std::to_string( agentResult ) );
        }

        result = (agentResult == ISipAgent::RESULT_OK);
    }

    return result;
}

//-----------------------------------------------------------------------------

int SipAgentApiWrapper::getMicVolume()
{
    int             result = -1;
    unsigned int volume    =  0;

    if ( d_agent )
    {
        ISipAgent::Result agentResult = d_agent->getMicVolume( &volume );

        if ( agentResult == ISipAgent::RESULT_OK )
        {
            result = (int)volume;
        }
        else
        {
            result = -1;
            LOG_ERROR( "ISipAgent::Result = " + std::to_string( agentResult ) );
        }
    }

    return result;
}

//-----------------------------------------------------------------------------

int SipAgentApiWrapper::getSpeechInputLevelFullRange()
{
    int             result = 0;
    unsigned int level  = 0;

    if ( d_agent )
    {
        ISipAgent::Result agentResult = d_agent->getSpeechInputLevelFullRange( &level );

        if ( agentResult == ISipAgent::RESULT_OK )
        {
            result = (int)level;
        }
        else
        {
            result = -1;
            LOG_ERROR( "ISipAgent::Result = " + std::to_string( agentResult ) );
        }
    }

    return result;
}

//---------------------------------------------------------------------------------
//End call audio related methods
//---------------------------------------------------------------------------------
