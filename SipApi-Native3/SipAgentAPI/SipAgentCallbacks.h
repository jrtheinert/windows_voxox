
#pragma once

#include "SipAgentHelpers.h"
#include "CallStateInfo.h"
#include "DataTypes.h"

class SipAgentApiWrapper;

namespace VxCallback
{

class AgentCallbacks: public ISipAgent::IAgentCallbacks
{
public:
    AgentCallbacks();

    virtual void verbose( const std::string& message ) throw();
    virtual void debug	( const std::string& message ) throw();
    virtual void info	( const std::string& message ) throw();
    virtual void warning( const std::string& message ) throw();
    virtual void error	( const std::string& message ) throw();
    virtual void failure( const std::string& message ) throw();	//JRT - Added because PJSIP reports some Errors as Fatal.
    
	virtual void failure() throw();
};

//=============================================================================

class LineCallbacks: public ISipAgent::ILineCallbacks
{
private:
    SipAgentApiWrapper*     d_wrapper;
    int                     d_lineId;

public:
    LineCallbacks( SipAgentApiWrapper* wrapper, int lineId );

	void setLineId( int val );
	
	ISipAgent::ICallCallbacks* newCallCallback   ( int                  callId	) throw();
	ISipAgent::ICallCallbacks* newCall           ( ISipAgent::Call*     call	) throw();

	void handleInfoRequestReceived( int callId, const std::string& content, const std::string& contentType );

    virtual void registrationChanged( ISipAgent::State state, ISipAgent::Event event, unsigned status, const std::string& reason ) throw();
};

//=============================================================================

class CallCallbacks: public ISipAgent::ICallCallbacks
{
private:
    SipAgentApiWrapper*     d_wrapper;
    int                     d_callId;
    bool                    d_hold;
    bool                    d_disconnected;
    bool                    d_ringback;

public:
    CallCallbacks( SipAgentApiWrapper* wrapper, int cid );

	void setCallId( int val );
    void stateChanged( const CallStateInfo& csi ) throw();

private:
	void sendEvent( EnumPhoneCallState::PhoneCallState callState, const std::string& remoteUri, int statusCode );

	//This method expects the SIP stack to parse the INFO stanza and provide needed data.
	void handleInfoRequestReceived( int callId, const std::string& content, const std::string& contentType );

	void referState( ISipAgent::ReferState, unsigned , const char* ) throw();
};

//=============================================================================

} // namespace VxCallback
