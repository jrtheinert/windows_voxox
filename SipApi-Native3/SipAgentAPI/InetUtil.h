#ifndef TELURIX_SHARED_INETUTIL_H
#define TELURIX_SHARED_INETUTIL_H
/*****************************************************************************************************/
/*                                                                                                   */
/* InetUtil.h                                                                                        */
/* Utilities to work with inet address and number format conversion.                                 */
/* (c) 2005-2011 Telurix LLC.                                                                        */
/*                                                                                                   */
/*****************************************************************************************************/

namespace InetUtil
{

#ifdef _BIG_ENDIAN

inline unsigned long hostToNet(unsigned long in) throw()
{
    return in;
}

inline unsigned long netToHost(unsigned long in) throw()
{
    return in;
}

inline unsigned short hostToNet(unsigned short in) throw()
{
    return in;
}

inline unsigned short netToHost(unsigned short in) throw()
{
return in;
}

#else /* _BIG_ENDIAN */

inline unsigned long hostToNet(unsigned long in) throw()
{
    return  (unsigned long)((in & (unsigned long)0xff000000) >> 24) +
            (unsigned long)((in & (unsigned long)0x00ff0000) >> 8) +
            (unsigned long)((in & (unsigned long)0x0000ff00) << 8) +
            (unsigned long)((in & (unsigned long)0x000000ff) << 24);
}

inline unsigned long netToHost(unsigned long in) throw()
{
    return  (unsigned long)((in & (unsigned long)0xff000000) >> 24) +
            (unsigned long)((in & (unsigned long)0x00ff0000) >> 8) +
            (unsigned long)((in & (unsigned long)0x0000ff00) << 8) +
            (unsigned long)((in & (unsigned long)0x000000ff) << 24);
}

inline unsigned short hostToNet(unsigned short in) throw()
{
    return (unsigned short)(
           (unsigned short)(((in & 0xff00) >> 8) & 0xff) |
           (unsigned short)((in & 0xff) << 8));
}

inline unsigned short netToHost(unsigned short in) throw()
{
    return (unsigned short)(
           (unsigned short)(((in & 0xff00) >> 8) & 0xff) |
           (unsigned short)((in & 0xff) << 8));
}

#endif /* _BIG_ENDIAN */

char *netToAddress(unsigned long in, char* buff) throw();

// Internet address interpretation routine.
// The value returned is in network order.
unsigned long addressToNet(const char* address, bool* valid = 0) throw();

//HostName to ulong
unsigned long hostNameToNet(const char* hostName, bool* valid = 0) throw();

}

#endif // TELURIX_SHARED_INETUTIL_H
