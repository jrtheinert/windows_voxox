
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Sep 2014
 */

#include "DataTypes.h"
#include "StringUtil.h"

#include <map>
#include <assert.h>	

#include <ctime>

#include <stdarg.h>		//For variable args in LogEntry::Helper
#include "Windows.h"	//For ThreadId

//LogEntry
std::string	     LogEntry::s_component       = "SipAgent";
LogSendCallback* LogEntry::s_logSendCallback = nullptr;

//Codec
const std::string Codec::s_separator = "/";

//ServerInfo
const std::string ServerInfo::s_serverType_None			= "none";
const std::string ServerInfo::s_serverType_SipRegistrar = "sip registrar";
const std::string ServerInfo::s_serverType_SipProxy     = "sip proxy";
const std::string ServerInfo::s_serverType_Stun			= "stun";
const std::string ServerInfo::s_serverType_TurnUdp		= "turn udp";
const std::string ServerInfo::s_serverType_TurnTcp		= "turn tcp";
const std::string ServerInfo::s_serverType_TurnTls		= "turns";
const std::string ServerInfo::s_serverType_HttpProxy	= "http proxy";
const std::string ServerInfo::s_serverType_HttpTunnel	= "http tunnel";
const std::string ServerInfo::s_serverType_HttpsTunnel	= "https tunnel";

const std::string ServerInfo::s_proxyType_None   = "none";
const std::string ServerInfo::s_proxyType_Auto   = "auto";
const std::string ServerInfo::s_proxyType_System = "system";
const std::string ServerInfo::s_proxyType_Http   = "http";
const std::string ServerInfo::s_proxyType_Socks4 = "socks4";
const std::string ServerInfo::s_proxyType_Socks5 = "socks5";

const std::string ServerInfo::s_topologyType_None = "none";
const std::string ServerInfo::s_topologyType_Ice  = "ice";
const std::string ServerInfo::s_topologyType_Stun = "stun";
const std::string ServerInfo::s_topologyType_Turn = "turn";

//TLSInfo
const std::string TlsInfo::s_tlsMethodV3  = "sslv3";
const std::string TlsInfo::s_tlsMethodV23 = "sslv23";
const std::string TlsInfo::s_tlsMethodV2  = "sslv2";
const std::string TlsInfo::s_tlsMethodV1  = "tlsv1";

//=============================================================================

std::string StringList::toString( const std::string& separator ) const 
{
	std::string result;

	for (const_iterator it = begin(); it != end(); ++it) 
	{
		if (it != begin()) 
		{
			result += separator;
		}

		result += *it;
	}

	return result;
}

StringList StringList::fromString( const std::string& text, const std::string& separator )
{
	StringList tokens;

	std::string str = text;

	//Skip separator at beginning.
	std::string::size_type lastPos = str.find_first_not_of(separator, 0);

	//Find first "non-separator".
	std::string::size_type pos = str.find_first_of(separator, lastPos);


	while (std::string::npos != pos || std::string::npos != lastPos) 
	{
		tokens.push_back( str.substr(lastPos, pos - lastPos) );		//Found a token, add it to the vector.
		lastPos = str.find_first_not_of(separator, pos);			//Skip delimiters. Note the "not_of"
		pos     = str.find_first_of(separator, lastPos);			//Find next "non-delimiter"

	}

	return tokens;
}

std::string StringList::operator[]( size_t tgtEntry ) const 
{
	return getNthEntry( tgtEntry );
}

void StringList::operator+=( const std::string& str ) 
{
	push_back( str );
}

std::string StringList::getNthEntry( size_t tgtEntry ) const
{
	std::string result;

	if ( size() > tgtEntry )
	{
		int pos = 0;

		for ( std::string str : *this )
		{
			if ( pos == tgtEntry )
			{
				result = str;
				break;
			}

			pos++;
		}
	}

	return result;
}

bool StringList::contains( const std::string& tgt )
{
	bool result = false;

	for (StringList::const_iterator it = begin(); it != end(); it++ )
	{
		if ( *it == tgt )
		{
			result = true;
			break;
		}
	}

	return result;
}

//=============================================================================


//=============================================================================

LogEntry::LogEntry()
{
	setLevel	 ( Debug );
	setComponent ( "" );
	setClassName ( "" );
	setMessage   ( "" );
	setFileName  ( "" );
	setLineNumber( 0  );
	setThreadId  ( 0  );
	setTime		 ( std::tm() );
}

LogEntry::LogEntry( const std::string& component, Level level, const std::string& className, const std::string& message, 
					const char* filename, int line, unsigned long threadId )
{
	setLevel	 ( level	 );
	setComponent ( component );
	setClassName ( className );
	setMessage   ( message	 );
	setLineNumber( line		 );
	setThreadId  ( threadId  );

	//Get current time
	std::time_t curTime = time(nullptr);					//Current time.
	struct std::tm * timeinfo = std::localtime(&curTime);	//Convert to local
	setTime		 ( *timeinfo    );							//Save

	if ( filename )
	{
		setFileName  ( filename  );
	}
}

LogEntry LogEntry::operator=( const LogEntry& src )
{
	if ( this != &src )
	{
		setLevel	 ( src.getLevel()	   );
		setComponent ( src.getComponent()  );
		setClassName ( src.getClassName()  );
		setMessage   ( src.getMessage()    );
		setFileName  ( src.getFileName()   );
		setLineNumber( src.getLineNumber() );
		setThreadId  ( src.getThreadId()   );
		setTime		 ( src.getTime()	   );
	}

	return *this;
}

//static 
void LogEntry::makeAndSend( const char* component, Level level, const char* className, const char* fileName, int line, const std::string& message )
{
	unsigned long threadId = getOsThreadId();
	LogEntry entry = LogEntry( component, level, className, message, fileName, line, threadId );
	sendEntry( entry );
}

//static 
void LogEntry::makeAndSend( const char* component, Level level, const char* className, const char* fileName, int line, const char* format, ...  )
{
	std::string   message;
	unsigned long threadId = getOsThreadId();
	const LogEntry entry = LogEntry( component, level, className, message, fileName, line, threadId );
	sendEntry( entry );
}

//static
void LogEntry::sendEntry( const LogEntry& entry )
{
	if ( s_logSendCallback != nullptr )
	{
		s_logSendCallback->sendLog( entry );
	}
}

//static
unsigned long LogEntry::getOsThreadId()
{
	unsigned long result = -1;
#ifdef _WINDOWS
	result = ::GetCurrentThreadId();
#endif

	return result;
}

//=============================================================================


//=============================================================================
//This class essentially copies and encapsulated SipAgent Enums so we
//	do NOT need to include SipAgent.h (Which doesn't work well with CLR code.
//=============================================================================



//=============================================================================

AudioDevice::AudioDevice( const StringList& data )
{
    switch ( data.size() )
    {
    case Pos_Max:
        //New SIP stack
        _name    = data[Pos_DeviceName];
        _index   = std::stoi( data[Pos_DeviceId] );
        _type    = data[Pos_DeviceType];
        _guid    = data[Pos_DeviceGuid];
        _default = StringUtil::stringToBool( data[Pos_Default] );
        break;
 
	case 3:
        _name   = data[Pos_DeviceName];
        _index  = getDefaultDeviceIndex();
        _type   = data[Pos_DeviceType];
        _guid   = "";
        _default= false;
        break;

    default:
        assert(false);
    }
}

AudioDevice::AudioDevice( const AudioDevice& src )
	:   _name   (src._name)
	,   _index  (src._index)
	,   _type   (src._type)
	,   _guid   (src._guid)
	,   _default(src._default)
{
}

AudioDevice::~AudioDevice()
{
}

AudioDevice& AudioDevice::operator= ( const AudioDevice& src )
{
	if ( this != &src )
	{
		_name    = src._name;
		_index   = src._index;
		_type    = src._type;
		_guid    = src._guid;
		_default = src._default;
	}

    return *this;
}

bool AudioDevice::operator==( const AudioDevice& audioDevice )
{
    if ( _guid.empty() || audioDevice._guid.empty() )
    {
        return (_name  == audioDevice._name  &&
                _index == audioDevice._index &&
                _type  == audioDevice._type);
    }
    return (_guid == audioDevice._guid);
}

StringList AudioDevice::getData() const
{
    StringList result;
    result += _name;
    result += std::to_string( _index );
    result += _type;
    result += _guid;
    result += StringUtil::boolToString( _default );

    return result;
}

StringList AudioDevice::convertData( const StringList& inputData )
{
    StringList result;

    switch( inputData.size() )
    {
        case 0:
        case Pos_Max:
            break;

        case 3:
            result += inputData[0]; //name
        #if defined(OS_WINDOWS)
            result += inputData[1]; //id/index
        #else
            if (inputData[1] == "-1")
                result += "0"; //id/index
            else
                result += inputData[1]; //id/index
        #endif
            result += inputData[2]; //type
            result += "";           //GUID
            result += "";           //isDefault;
            break;

        default:
            assert(false);  //Unknown format
    }
    return result;
}

//-----------------------------------------------------------------------------

AudioDevice AudioDevice::makeDefaultDevice( const std::string& useDefaultDeviceText, const std::string& type )
{
    StringList  temp;

    temp += useDefaultDeviceText;
    temp += std::to_string( getDefaultDeviceIndex() );
    temp += type;
    temp += "";
    temp += StringUtil::boolToString( false );

    AudioDevice result( temp );

    return result;
}

//-----------------------------------------------------------------------------
//static 
int AudioDevice::getDefaultDeviceIndex()
{
#if defined( _WINDOWS )
	int defaultIndex = -1;
#else
	int defaultIndex = 0;
#endif

	return defaultIndex;
}

//=============================================================================


//=============================================================================

AudioDevice AudioDeviceList::findBestMatch( const AudioDevice& tgtDevice )
{
    AudioDevice result;
    std::string tgtName = tgtDevice.getName();

    for ( AudioDeviceList::iterator it = begin(); it != end(); it++ )
    {
        std::string itName = (*it).getName();
        bool match  = itName == tgtName;

        if ( !match )   //Try match on shortest lenght
        {
            if ( !itName.empty() && !tgtName.empty() )
            {
                int minLen = itName.size() < tgtName.size() ? itName.size() : tgtName.size();

                match = itName.compare( 0, minLen, tgtName ) == 0;
            }
        }

        if ( match )
        {
            result = *it;
            break;
        }
    }

    return result;
}

//=============================================================================


//=============================================================================

Codec::Codec()
{
	initVars();
}

//-----------------------------------------------------------------------------
	
Codec::Codec( const std::string& codecStr )
{
	initVars();
	fromString( codecStr );
}

//-----------------------------------------------------------------------------

Codec::~Codec()
{
}

//-----------------------------------------------------------------------------

void Codec::initVars()
{
	setName         ( "" );
	setSampleRate   ( 0  );
	setChannelCount ( 0  );
	setType         ( Type_Unknown );
	setCanBeDisabled( false );
	setEnabled      ( true  );
	setOrder        ( 0  );
}

//-----------------------------------------------------------------------------

Codec& Codec::operator= ( const Codec& src )
{
	if ( this != &src )
	{
		setName         ( src.getName()         );
		setSampleRate   ( src.getSampleRate()   );
		setChannelCount ( src.getChannelCount() );
		setType         ( src.getType()		    );
		setCanBeDisabled( src.canBeDisabled()   );
		setEnabled      ( src.isEnabled()		);
		setOrder        ( src.getOrder()		);
	}

	return *this;
}

//-----------------------------------------------------------------------------

bool Codec::operator==( const Codec& src )
{
	return getName()         == src.getName()			&&
		   getSampleRate()   == src.getSampleRate()		&&
		   getChannelCount() == src.getChannelCount();
}

//-----------------------------------------------------------------------------

bool Codec::fromString( const std::string& val )
{
	bool result = false;
	
	//NOTE: if this changes, be sure to review Codecs::setEnabled()
	StringList temp = StringList::fromString( val, s_separator );	//Looks like ISAC/16000/1 or ISAC/16000/1/0

	setName        ( temp[0]		   	    );
	setSampleRate  ( std::stoi( temp[1] ) );
	setChannelCount( std::stoi( temp[2] ) );

	if ( temp.size() >= 4 )
	{
		setEnabled( StringUtil::stringToBool( temp[3] ) );
	}

	determineInfoFromName();

	return result;
}

//-----------------------------------------------------------------------------

std::string Codec::toString( bool includeChannelCount ) const
{
	std::string result;

	result += getName();
	result += s_separator;
	result += std::to_string( getSampleRate() );

	if ( includeChannelCount )
	{
		result += s_separator;
		result += std::to_string( getChannelCount() );
	}

	return result;
}

//-----------------------------------------------------------------------------

void Codec::determineInfoFromName()
{
	std::string name		  = getName();
	bool		canBeDisabled = true;
	Type		type		  = Type_Unknown;

	if		( name == "CN" )
	{
		type = Type_ComfortNoise;
	}
	else if ( name == "telephone-event" )
	{
		type = Type_TelephoneEvent;
	}
	else if ( name == "red" )
	{
		type = Type_RedundantAudio;
	}
	else
	{
		type = Type_Audio;

		if ( name == "PCMU" )
		{
			canBeDisabled = false;
		}
		else if ( name == "PCMA" )
		{
			canBeDisabled = false;
		}
	}

	setType         ( type          );
	setCanBeDisabled( canBeDisabled );
}

//=============================================================================



//=============================================================================

void Codecs::add( const Codec& codec )
{
	push_back( codec );
}

//-----------------------------------------------------------------------------

int Codecs::getCountByType( Codec::Type type ) const
{
	int result = 0;

	for ( Codecs::const_iterator it = begin(); it != end(); it++ )
	{
		if ( (*it).getType() == type )
		{
			result++;
		}
	}

	return result;
}

//-----------------------------------------------------------------------------

bool Codecs::hasType( Codec::Type type ) const
{
	return (getCountByType( type ) > 0);
}

//-----------------------------------------------------------------------------

void Codecs::fromDefaultCodecList()	//TODO-SIP
{
	clear();

	//SipWrapper* sipWrapper = SipWrapperFactory::getFactory().createSipWrapper();		//Just gets instance, if it exists.
	//
	//StringList temp = sipWrapper->getDefaultAudioCodecList();

	//for (StringList::iterator it = temp.begin(); it != temp.end(); it++ )
	//{
	//	Codec codec;

	//	codec.fromString( *it );
	//	add( codec );
	//}
}

//-----------------------------------------------------------------------------

void Codecs::fromPreferredCodecList()	//TODO-SIP
{
	clear();

	//SipWrapper* sipWrapper = SipWrapperFactory::getFactory().createSipWrapper();		//Just gets instance, if it exists.
	//
	//StringList temp = sipWrapper->getPreferredAudioCodecList();		//We now set preferred order in SipWrapper::init(), so do NOT get default codec list.

	//for (StringList::iterator it = temp.begin(); it != temp.end(); it++ )
	//{
	//	Codec codec;

	//	codec.fromString( *it );
	//	add( codec );
	//}
}

//-----------------------------------------------------------------------------

StringList Codecs::getTypeAsStringList( Codec::Type type, bool enabled, bool includeChannelCount ) const
{
	StringList result;

	for ( Codecs::const_iterator it = begin(); it != end(); it++ )
	{
		if ( (*it).getType() == type )
		{
			std::string temp = (*it).toString( includeChannelCount ) + Codec::s_separator + StringUtil::boolToString( enabled );
			result.push_back( temp );
		}
	}

	return result;
}

//-----------------------------------------------------------------------------

StringList Codecs::toStringList( bool audioOnly, bool includeChannelCount ) const
{
	StringList result;

	for ( Codecs::const_iterator it = begin(); it != end(); it++ )
	{
		bool isAudio = (*it).getType() == Codec::Type_Audio;
		bool use     = (audioOnly ? isAudio : true );

		if ( use )
		{
			std::string temp = (*it).toString( includeChannelCount );
			result.push_back( temp );
		}
	}

	return result;
}

//-----------------------------------------------------------------------------

bool Codecs::setOrder( const std::string& codecStr, int order )
{
	bool  result = false;
	Codec codec;

	codec.fromString( codecStr );

	for ( Codecs::iterator it = begin(); it != end(); it++ )
	{
		if ( (*it) == codec )
		{
			(*it).setOrder( order );
			result = true;
			break;
		}
	}

	return result;
}

//-----------------------------------------------------------------------------

void Codecs::setEnabled( const StringList& codecList )
{
	for (StringList::const_iterator it = codecList.begin(); it != codecList.end(); ++it) 
	{
		//NOTE: If this changes, be sure to review Codec::fromString()
		StringList temp = StringList::fromString( *it, Codec::s_separator );	//Looks like ISAC/16000/1 or ISAC/16000/1/0

		if ( temp.size() >= 4 )
		{
			bool  enable = StringUtil::stringToBool( temp[3] );
			bool  found  = false;
			Codec codec;
			codec.fromString( *it );

			for ( Codecs::iterator itc = begin(); itc != end(); itc++ )
			{
				if ( (*itc) == codec )
				{
					(*itc).setEnabled( enable );
					found = true;
					break;
				}
			}

//			if ( !found )
//			{
//				LOG_WARN( "Could not find codec: " + *it );	//TODO-LOG
//			}
		}
	}
}

//-----------------------------------------------------------------------------

void Codecs::reorder( const StringList& codecList )
{
	int order = 0;

	//Set order for each codec
	for (StringList::const_iterator it = codecList.begin(); it != codecList.end(); ++it) 
	{
		setOrder( *it, ++order );
	}

	//Reorder
	//Only Audio codecs are ordered.  All zeros go at bottom.
	Codecs temp;

	for ( int x = 1; x <= order; x++ )
	{
		for ( Codecs::iterator it = begin(); it != end(); it++ )
		{
			if ( (*it).isTypeAudio() )
			{
				if ( (*it).getOrder() == x )
				{
					temp.add( *it );
					break;
				}
			}
		}
	}

	//Audio, with order == 0
	for ( Codecs::iterator it = begin(); it != end(); it++ )
	{
		if ( (*it).isTypeAudio() )
		{
			if ( (*it).getOrder() == 0 )
			{
				temp.add( *it );
			}
		}
	}

	//Comfort noise
	for ( Codecs::iterator it = begin(); it != end(); it++ )
	{
		if ( (*it).isTypeComfortNoise() )
		{
			temp.add( *it );
		}
	}

	//Telephone event
	for ( Codecs::iterator it = begin(); it != end(); it++ )
	{
		if ( (*it).isTypeTelephoneEvent() )
		{
			temp.add( *it );
		}
	}

	//Redundant audio
	for ( Codecs::iterator it = begin(); it != end(); it++ )
	{
		if ( (*it).isTypeRedundantAudio() )
		{
			temp.add( *it );
		}
	}

	*this = temp;
}

//-----------------------------------------------------------------------------
//static
StringList Codecs::toStringListForSipWrapper( const StringList& codecList )
{
	StringList result;
	Codecs     codecs;

	for ( StringList::const_iterator its = codecList.begin(); its != codecList.end(); its++ )
	{
		Codec codec ( *its );
		codecs.add( codec );
	}

	
	for ( Codecs::const_iterator it = codecs.begin(); it != codecs.end(); it++ )
	{
		if ( (*it).isEnabled() )
		{
			result.push_back( (*it).toString() );
		}
	}

	return result;
}

//=============================================================================


//=============================================================================

typedef std::map<EnumDeviceType::DeviceType, std::string> DeviceTypeMap;
static DeviceTypeMap _deviceTypeMap;

void EnumDeviceType::init() 
{
	_deviceTypeMap[EnumDeviceType::DeviceTypeMasterVolume]	= "DeviceTypeMasterVolume";
	_deviceTypeMap[EnumDeviceType::DeviceTypeWaveOut]		= "DeviceTypeWaveOut";
	_deviceTypeMap[EnumDeviceType::DeviceTypeWaveIn]		= "DeviceTypeWaveIn";
	_deviceTypeMap[EnumDeviceType::DeviceTypeCDOut]			= "DeviceTypeCDOut";
	_deviceTypeMap[EnumDeviceType::DeviceTypeMicrophoneOut] = "DeviceTypeMicrophoneOut";
	_deviceTypeMap[EnumDeviceType::DeviceTypeMicrophoneIn]  = "DeviceTypeMicrophoneIn";
}

std::string EnumDeviceType::toString(DeviceType deviceType) 
{
	init();
	std::string tmp = _deviceTypeMap[deviceType];

//	if (tmp.empty()) {
//		LOG_WARN("unknown DeviceType=" + String::fromNumber(deviceType));	//TODO-LOG
//	}

	return tmp;
}

EnumDeviceType::DeviceType EnumDeviceType::toDeviceType(const std::string & deviceType) 
{
	init();
	for (DeviceTypeMap::const_iterator it = _deviceTypeMap.begin();
		it != _deviceTypeMap.end();
		++it) {

		if ((*it).second == deviceType) {
			return (*it).first;
		}
	}

//	LOG_WARN("unknown DeviceType=" + deviceType);	//TODO-LOG
	return DeviceTypeMasterVolume;
}

//=============================================================================


//=============================================================================

std::string EnumPhoneCallState::toString(PhoneCallState state) 
{
	switch(state) 
	{
	case PhoneCallStateUnknown:
		return "PhoneCallStateUnknown";

	case PhoneCallStateError:
		return "PhoneCallStateError";

	case PhoneCallStateResumed:
		return "PhoneCallStateResumed";

	case PhoneCallStateTalking:
		return "PhoneCallStateTalking";

	case PhoneCallStateDialing:
		return "PhoneCallStateDialing";

	case PhoneCallStateRinging:
		return "PhoneCallStateRinging";

	case PhoneCallStateClosed:
		return "PhoneCallStateClosed";

	case PhoneCallStateIncoming:
		return "PhoneCallStateIncoming";

	case PhoneCallStateHold:
		return "PhoneCallStateHold";

	case PhoneCallStateMissed:
		return "PhoneCallStateMissed";

	case PhoneCallStateRedirected:
		return "PhoneCallStateRedirected";

	case PhoneCallStateRingingStart:
		return "PhoneCallStateRingingStart";

	case PhoneCallStateRingingStop:
		return "PhoneCallStateRingingStop";

	default:
//		LOG_FATAL("unknown PhoneCallState=" + String::fromNumber(state));	//TODO-LOG
		return "PhoneCallStateUnknown";
	}

	return "Unknown Phone Call State";
}

//=============================================================================


//=============================================================================

std::string EnumPhoneLineState::toString( PhoneLineState state ) 
{
	switch ( state )
	{
	case PhoneLineStateUnknown:
		return "PhoneLineStateUnknown";

	case PhoneLineStateProgress:
		return "PhoneLineStateProgress";

	case PhoneLineStateServerError:
		return "PhoneLineStateServerError";

	case PhoneLineStateAuthenticationError:
		return "PhoneLineStateAuthenticationError";

	case PhoneLineStateTimeout:
		return "PhoneLineStateTimeout";

	case PhoneLineStateOk:
		return "PhoneLineStateOk";

	case PhoneLineStateClosed:
		return "PhoneLineStateClosed";

	default:
//		LOG_FATAL("unknown PhoneLineState=" + String::fromNumber(state));	//TODO-LOG
		return "PhoneLineStateUnknown";
	}

	return "Unknown Phone Line State";
}

//=============================================================================



//=============================================================================

typedef std::map<EnumVideoMode::VideoMode, std::string> VideoModeMap;
static VideoModeMap _videoModeMap;

void EnumVideoMode::init() 
{
	_videoModeMap[EnumVideoMode::Nobody]   = "Nobody";			//TODO-TRANSLATE
	_videoModeMap[EnumVideoMode::Contacts] = "Contacts";		//TODO-TRANSLATE
	_videoModeMap[EnumVideoMode::Anybody]  = "Anybody";			//TODO-TRANSLATE
}

std::string EnumVideoMode::toString(VideoMode videoMode) 
{
	init();
	std::string tmp = _videoModeMap[videoMode];

//	if (tmp.empty()) {
//		LOG_FATAL("unknown VideoMode");	//TODO-LOG
//	}

	return tmp;
}

EnumVideoMode::VideoMode EnumVideoMode::toVideoMode(const std::string & videoMode) 
{
	init();

	for (VideoModeMap::const_iterator it = _videoModeMap.begin(); it != _videoModeMap.end(); ++it) 
	{
		if ((*it).second == videoMode) 
		{
			return (*it).first;
		}
	}

//	LOG_FATAL("unknown VideoMode=" + videoMode);
	return Nobody;
}

//=============================================================================


//=============================================================================

typedef std::map<EnumVideoQuality::VideoQuality, std::string> VideoQualityMap;
static VideoQualityMap _videoQualityMap;

void EnumVideoQuality::init() 
{
	_videoQualityMap[EnumVideoQuality::VideoQualityNormal]		= "VideoQualityNormal";			//TODO-TRANSLATE?
	_videoQualityMap[EnumVideoQuality::VideoQualityGood]		= "VideoQualityGood";			//TODO-TRANSLATE?
	_videoQualityMap[EnumVideoQuality::VideoQualityVeryGood]	= "VideoQualityVeryGood";		//TODO-TRANSLATE?
	_videoQualityMap[EnumVideoQuality::VideoQualityExcellent]	= "VideoQualityExcellent";		//TODO-TRANSLATE?
}

std::string EnumVideoQuality::toString(VideoQuality videoQuality) 
{
	init();

	std::string tmp = _videoQualityMap[videoQuality];
	
//	if (tmp.empty()) {
//		LOG_FATAL("unknown VideoQuality=" + String::fromNumber(videoQuality));	//TODO-LOG
//	}
	
	return tmp;
}

EnumVideoQuality::VideoQuality EnumVideoQuality::toVideoQuality(const std::string & videoQuality) 
{
	init();

	for (VideoQualityMap::const_iterator it = _videoQualityMap.begin(); it != _videoQualityMap.end(); ++it) 
	{
		if ((*it).second == videoQuality) 
		{
			return (*it).first;
		}
	}

//	LOG_FATAL("unknown VideoQuality=" + videoQuality);	//TODO-LOG
	return VideoQualityNormal;
}

//=============================================================================


//=============================================================================

ServerInfo::ServerInfo()
{
	initVars();
}

//-----------------------------------------------------------------------------

ServerInfo::~ServerInfo()
{
}

//-----------------------------------------------------------------------------

void ServerInfo::initVars()
{
	setType     ( Type_None  );
	setOwner    ( Owner_None );
	setAddress  ( "" );
	setPort     (  0 );
//	setLocalPort(  0 );
	setUserId   ( "" );
	setPassword ( "" );
	setSsl      ( false );
	setHttpProxyType( ISipAgent::HTTPPROXY_NONE );
}

//-----------------------------------------------------------------------------

ServerInfo&	ServerInfo::operator=( const ServerInfo& src )
{
	if ( this != &src )
	{
		setType			( src.getType()			 );
		setOwner		( src.getOwner()		 );
		setAddress		( src.getAddress()		 );
		setPort			( src.getPort()			 );
//		setLocalPort	( src.getLocalPort()	 );
		setUserId		( src.getUserId()		 );
		setPassword		( src.getPassword()		 );
		setSsl          ( src.useSsl()			 );
		setHttpProxyType( src.getHttpProxyType() );
	}

	return *this;
}

//-----------------------------------------------------------------------------

bool ServerInfo::operator==( const ServerInfo& src )
{
	bool result = getAddress() == src.getAddress() &&
				  getPort()    == src.getPort()    &&
				  getType()    == src.getType();

	return result;
}

//-----------------------------------------------------------------------------
//May need to change based on type, but this is good for now.
bool ServerInfo::isValid() const
{
	bool result = false;

	if ( !isTypeNone() )
	{
		result = (!getAddress().empty()) && (getPort() != 0);
	}

	return result;
}

//-----------------------------------------------------------------------------

std::string ServerInfo::getSipProtocol() const
{
	std::string result = (useSsl() ? "sips:" : "sip:" );

	return result;
}

//-----------------------------------------------------------------------------

std::string ServerInfo::getUrlFormat() const
{
	std::string result;

	result += getTypeText();
	result += ":";
	result += getAddress();
	result += ":";
	result += std::to_string( getPort() );

	return result;
}

//-----------------------------------------------------------------------------

std::string ServerInfo::getHostPortFormat() const
{
	std::string result;

	result += getAddress();

	if ( getPort() > 0 )
	{
		result += ":";
		result += std::to_string( getPort() );
	}

	return result;
}

//-----------------------------------------------------------------------------

bool ServerInfo::isTopologyHost() const
{ 
	bool result = ( isTypeTurnUdp() || isTypeTurnTcp() || isTypeTurnTls() || isTypeStun() );

	return result;
}

//-----------------------------------------------------------------------------
//static
std::string ServerInfo::getTypeText( ServerInfo::Type type )
{
	std::string result;

	switch ( type )
	{
	case Type_SipRegistrar:
		result = s_serverType_SipRegistrar;
		break;

	case Type_SipProxy:
		result = s_serverType_SipProxy;
		break;

	case Type_Stun:
		result = s_serverType_Stun;
		break;

	case Type_TurnUdp:
		result = s_serverType_TurnUdp;
		break;

	case Type_TurnTcp:
		result = s_serverType_TurnTcp;
		break;

	case Type_TurnTls:
		result = s_serverType_TurnTls;
		break;

	case Type_HttpProxy:
		result = s_serverType_HttpProxy;
		break;

	case Type_HttpTunnel:
		result = s_serverType_HttpTunnel;
		break;

	case Type_HttpsTunnel:
		result = s_serverType_HttpsTunnel;
		break;

	case Type_None:
		assert(false);
		result = s_serverType_None;
		break;

	default:
		assert(false);		//New type?
		result = "unknown";
		break;
	}

	return result;
}

//-----------------------------------------------------------------------------
//static
ISipAgent::IServerInfo::Type ServerInfo::typeFromString( const std::string& typeString )
{
	ServerInfo::Type result = ServerInfo::Type_None;

	if (  typeString == s_serverType_SipRegistrar )
	{
		result = Type_SipRegistrar;
	}
	else if (  typeString == s_serverType_SipProxy )
	{
		result = Type_SipProxy;
	}
	else if (  typeString == s_serverType_Stun )
	{
		result = Type_Stun;
	}
	else if (  typeString == s_serverType_TurnUdp )
	{
		result = Type_TurnUdp;
	}
	else if (  typeString == s_serverType_TurnTcp )
	{
		result = Type_TurnTcp;
	}
	else if (  typeString == s_serverType_TurnTls )
	{
		result = Type_TurnTls;
	}
	else if (  typeString == s_serverType_HttpProxy )
	{
		result = Type_HttpProxy;
	}
	else if (  typeString == s_serverType_HttpTunnel )
	{
		result = Type_HttpTunnel;
	}
	else if (  typeString == s_serverType_HttpsTunnel )
	{
		result = Type_HttpsTunnel;
	}
	else if (  typeString == s_serverType_None )
	{
		result = Type_None;
	}
	else 
	{
		assert( false );		//New type or bad text.
		result = ServerInfo::Type_None;
	}

	return result;
}

//-----------------------------------------------------------------------------
//static
ISipAgent::HTTPProxy ServerInfo::proxyTypeFromString( const std::string& type )
{
	ISipAgent::HTTPProxy result = ISipAgent::HTTPPROXY_NONE;

	if ( type == s_proxyType_None )
	{
		result = ISipAgent::HTTPPROXY_NONE;
	}
	else if ( type == s_proxyType_Auto )
	{
		result = ISipAgent::HTTPPROXY_AUTO;
	}
	else if ( type == s_proxyType_System )
	{
		result = ISipAgent::HTTPPROXY_SYSTEM;
	}
	else if ( type == s_proxyType_Http )
	{
		result = ISipAgent::HTTPPROXY_HTTP;
	}
	else if ( type == s_proxyType_Socks4 )
	{
		result = ISipAgent::HTTPPROXY_SOCKSv4;
	}
	else if ( type == s_proxyType_Socks5 )
	{
		result = ISipAgent::HTTPPROXY_SOCKSv5;
	}

	return result;
}
//-----------------------------------------------------------------------------
//static
std::string ServerInfo::proxyTypeToString( ISipAgent::HTTPProxy type )
{
	std::string result;

	switch( type )
	{
	case ISipAgent::HTTPPROXY_NONE:
		result = s_proxyType_None;
		break;

	case ISipAgent::HTTPPROXY_AUTO:
		result = s_proxyType_Auto;
		break;

	case ISipAgent::HTTPPROXY_SYSTEM:
		result = s_proxyType_System;
		break;

	case ISipAgent::HTTPPROXY_HTTP:
		result = s_proxyType_Http;
		break;

	case ISipAgent::HTTPPROXY_SOCKSv4:
		result = s_proxyType_Socks4;
		break;

	case ISipAgent::HTTPPROXY_SOCKSv5:
		result = s_proxyType_Socks5;
		break;

	default:
		assert( false );		//New type?
		result = "unknown";
	}

	return result;
}

//-----------------------------------------------------------------------------
//static
ISipAgent::Topology ServerInfo::topologyFromString( const std::string& topo )
{
	ISipAgent::Topology result = ISipAgent::TOPOLOGY_NONE;

	if		( topo == s_topologyType_None || topo.empty() )
	{
		result = ISipAgent::TOPOLOGY_NONE;
	}
	else if ( topo == s_topologyType_Ice )
	{
		result = ISipAgent::TOPOLOGY_ICE;
	}
	else if ( topo == s_topologyType_Stun ) 
	{
		result = ISipAgent::TOPOLOGY_STUN;
	}
	else if ( topo == s_topologyType_Turn )
	{
		result = ISipAgent::TOPOLOGY_TURN;
	}

	return result;
}

//-----------------------------------------------------------------------------
//static
ISipAgent::TopologyHost ServerInfo::getTopologyHostFromType( ServerInfo::Type type )
{
	ISipAgent::TopologyHost result = (ISipAgent::TopologyHost) (-1);		//None is not defined!

	switch ( type )
	{
	case Type_Stun:
		result = ISipAgent::TOPOLGY_HOST_STUN;
		break;

	case Type_TurnUdp:
		result = ISipAgent::TOPOLGY_HOST_TURN_UDP;
		break;

	case Type_TurnTcp:
		result = ISipAgent::TOPOLGY_HOST_TURN_TCP;
		break;

	case Type_TurnTls:
		result = ISipAgent::TOPOLGY_HOST_TURN_TLS;
		break;

	case Type_SipRegistrar:
	case Type_SipProxy:
	case Type_HttpProxy:
	case Type_HttpTunnel:
	case Type_HttpsTunnel:
	case Type_None:
		break;

	default:
		assert(false);		//New type?
		break;
	}

	return result;
}

//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//static 
std::string ServerInfo::topologyToString( ISipAgent::Topology topo )
{
	std::string result;

	switch ( topo )
	{
	case ISipAgent::TOPOLOGY_NONE:
		result = s_topologyType_None;
		break;

	case ISipAgent::TOPOLOGY_ICE:
		result = s_topologyType_Ice;
		break;

	case ISipAgent::TOPOLOGY_STUN:
		result = s_topologyType_Stun;
		break;

	case ISipAgent::TOPOLOGY_TURN:
		result = s_topologyType_Turn;
		break;

	default:
		assert(false);
		result = "unknown";
	}

	return result;
}

//=============================================================================


//=============================================================================
	
bool ServerInfoList::addSipProxy( const std::string& serverIn, unsigned int serverPort, bool ssl )
{
	ServerInfo server;

	server.setTypeSipProxy();
	server.setAddress  ( serverIn   );
	server.setPort     ( serverPort );
	server.setSsl	   ( ssl );

	return add( server );
}

//-----------------------------------------------------------------------------
	
bool ServerInfoList::addSipRegistrar( const std::string& address, unsigned int port, bool ssl )
{
	ServerInfo server;

	server.setTypeSipRegistrar();
	server.setAddress  ( address );
	server.setPort     ( port    );
	server.setSsl	   ( ssl );

	return add( server );
}

//-----------------------------------------------------------------------------

bool ServerInfoList::addStun( const std::string& address, unsigned int port )
{
	ServerInfo server;

	server.setTypeStun();
	server.setAddress  ( address );
	server.setPort     ( port    );

	return add( server );
}

//-----------------------------------------------------------------------------

bool ServerInfoList::addTurnUdp( const std::string& address, unsigned int port, const std::string& login, const std::string& password )
{
	ServerInfo server;

	server.setTypeTurnUdp();
	server.setAddress ( address  );
	server.setPort    ( port     );
	server.setUserId  ( login    );
	server.setPassword( password );

	return add( server );
}

//-----------------------------------------------------------------------------

bool ServerInfoList::addTurnTcp( const std::string& address, unsigned int port, const std::string& login, const std::string& password )
{
	ServerInfo server;

	server.setTypeTurnTcp();
	server.setAddress ( address  );
	server.setPort    ( port     );
	server.setUserId  ( login    );
	server.setPassword( password );

	return add( server );
}

//-----------------------------------------------------------------------------

bool ServerInfoList::addTurnTls( const std::string& address, unsigned int port, const std::string& login, const std::string& password )
{
	ServerInfo server;

	server.setTypeTurnTls();
	server.setAddress ( address  );
	server.setPort    ( port     );
	server.setUserId  ( login    );
	server.setPassword( password );
	server.setSsl     ( true );

	return add( server );
}

//-----------------------------------------------------------------------------
	
bool ServerInfoList::addHttpProxy( const std::string& address, unsigned int port, const std::string& login, const std::string& password, ISipAgent::HTTPProxy httpProxyType )
{
	ServerInfo server;

	server.setTypeHttpProxy();
	server.setAddress	   ( address		);
	server.setPort		   ( port			);
	server.setUserId	   ( login			);
	server.setPassword	   ( password		);
	server.setHttpProxyType( httpProxyType	);

	return add( server );
}

//-----------------------------------------------------------------------------

bool ServerInfoList::exists( const ServerInfo& src )
{
	bool result  = false;

	for ( ServerInfoList::iterator it = begin(); it != end(); it++ )
	{
		if ( (*it) == src )
		{
			result = true;
			break;
		}
	}

	return result;
}

//-----------------------------------------------------------------------------

bool ServerInfoList::add( const ServerInfo& src )
{
	bool result  = false;
	bool matched = false;

	//If we match on type, server and port, then we replace.  Otherwise, add.
	for ( ServerInfoList::iterator it = begin(); it != end(); it++ )
	{
		if ( (*it).isValid() )
		{
			if ( (*it).getType() == src.getType() )
			{
				if ( (*it) == src )		//Server and port match
				{
					(*it)   = src;
					result  = false;	//False indicates 'replace' rather than 'add'.
					matched = true;
				}
			}
		}
		else
		{
//			LOG_WARN( "Found invalid entry." );	//TODO-LOG
		}
	}

	if ( !matched )
	{
		push_back( src );
		result = true;
	}

	return result;
}

//-----------------------------------------------------------------------------

int ServerInfoList::merge( const ServerInfoList& servers )
{
	int result = 0;

	for ( ServerInfoList::const_iterator it = servers.begin(); it != servers.end(); it++ )
	{
		if ( !exists( (*it) ) )
		{
			add( *it );
			result++;
		}
	}

	return result;
}


int ServerInfoList::getRandom( int max ) const	//1-based
{
	int result = 0;
	
//	srand(time(NULL));			//TODO-SIP
	result = (rand() % max ) + 1;

	return result;
}

//-----------------------------------------------------------------------------

int ServerInfoList::getCountByType( ServerInfo::Type tgtType ) const
{
	int result = 0;

	for ( ServerInfoList::const_iterator it = begin(); it != end(); it++ )
	{
		if ( (*it).getType() == tgtType )
		{
			result++;
		}
	}

	return result;
}

//-----------------------------------------------------------------------------

int ServerInfoList::getCountByOwner( ServerInfo::Owner tgtOwner ) const
{
	int result = 0;

	for ( ServerInfoList::const_iterator it = begin(); it != end(); it++ )
	{
		if ( (*it).getOwner() == tgtOwner )
		{
			result++;
		}
	}

	return result;
}

//-----------------------------------------------------------------------------

int ServerInfoList::getCountByTypeAndOwner( ServerInfo::Type tgtType, ServerInfo::Owner tgtOwner ) const
{
	int result = 0;

	for ( ServerInfoList::const_iterator it = begin(); it != end(); it++ )
	{
		if ( (*it).getType() == tgtType )
		{
			if ( (*it).getOwner() == tgtOwner )
			{
				result++;
			}
		}
	}

	return result;
}

//-----------------------------------------------------------------------------

ServerInfo* ServerInfoList::getFirstByType ( ServerInfo::Type tgtType ) const
{
	ServerInfo* result = nullptr;

	for ( ServerInfoList::const_iterator it = begin(); it != end(); it++ )
	{
		if ( (*it).getType() == tgtType )
		{
			result = const_cast<ServerInfo*>(&(*it) );
			break;
		}
	}

	return result;
}

//-----------------------------------------------------------------------------

ServerInfo ServerInfoList::getRandomByType( ServerInfo::Type tgtType ) const
{
	ServerInfo result;

	//Get Count by type, get random number, iterate to proper entry.
	int count	= getCountByType( tgtType );

	if ( count > 0 )
	{
		int tgtItem = getRandom( count );
		int item	= 0;

		for ( ServerInfoList::const_iterator it = begin(); it != end(); it++ )
		{
			if ( (*it).getType() == tgtType )
			{
				item++;

				if ( item == tgtItem )
				{
					result = *it;
					break;
				}
			}
		}
	}

	return result;
}

//-----------------------------------------------------------------------------

ServerInfo ServerInfoList::getFirstByOwner( ServerInfo::Owner tgtOwner ) const
{
	ServerInfo result;

	for ( ServerInfoList::const_iterator it = begin(); it != end(); it++ )
	{
		if ( (*it).getOwner() == tgtOwner )
		{
			result = *it;
			break;
		}
	}

	return result;
}

//-----------------------------------------------------------------------------

ServerInfo ServerInfoList::getRandomByOwner( ServerInfo::Owner tgtOwner ) const
{
	ServerInfo result;

	//Get Count by type, get random number, iterate to proper entry.
	int count	= getCountByOwner( tgtOwner );

	if ( count > 0 )
	{
		int tgtItem = getRandom( count );
		int item	= 0;

		for ( ServerInfoList::const_iterator it = begin(); it != end(); it++ )
		{
			if ( (*it).getOwner() == tgtOwner )
			{
				item++;

				if ( item == tgtItem )
				{
					result = *it;
					break;
				}
			}
		}
	}

	return result;
}

//-----------------------------------------------------------------------------

ServerInfo ServerInfoList::getFirstByTypeAndOwner ( ServerInfo::Type tgtType, ServerInfo::Owner tgtOwner ) const
{
	ServerInfo result;

	for ( ServerInfoList::const_iterator it = begin(); it != end(); it++ )
	{
		if ( (*it).getType() == tgtType )
		{
			if ( (*it).getOwner() == tgtOwner )
			{
				result = *it;
				break;
			}
		}
	}

	return result;
}

//-----------------------------------------------------------------------------

ServerInfo ServerInfoList::getRandomByTypeAndOwner( ServerInfo::Type tgtType, ServerInfo::Owner tgtOwner ) const
{
	ServerInfo result;

	//Get Count by type, get random number, iterate to proper entry.
	int count	= getCountByTypeAndOwner( tgtType, tgtOwner );

	if ( count > 0 )
	{
		int tgtItem = getRandom( count );
		int item	= 0;

		for ( ServerInfoList::const_iterator it = begin(); it != end(); it++ )
		{
			if ( (*it).getType() == tgtType )
			{
				if ( (*it).getOwner() == tgtOwner )
				{
					item++;

					if ( item == tgtItem )
					{
						result = *it;
						break;
					}
				}
			}
		}
	}

	return result;
}

//-----------------------------------------------------------------------------

//void ServerInfoList::getTopologyHostInfo( ISipAgent::TopologyHostList* hosts)
//{
//	for ( ServerInfoList::const_iterator it = begin(); it != end(); it++ )
//	{
//		if ( (*it).isTopologyHost() )
//		{
//			std::string userId = it->getUserId();
//			std::string realm;
//			size_t found = userId.find('@');
//			if (found != std::string::npos)
//			{
//				realm = userId.substr(found + 1);
//				userId = userId.substr(0, found);
//			}
//			hosts->add
//			(
//				it->getTopologyHost(),
//				it->getAddress().c_str(),
//				it->getPort(),
//				userId.c_str(),
//				realm.c_str(),
//				it->getPassword().c_str()
//			);
//		}
//	}
//}

//=============================================================================


//=============================================================================

TlsInfo::TlsInfo()
{
	initVars();
}

//-----------------------------------------------------------------------------

TlsInfo::~TlsInfo()
{
}

//-----------------------------------------------------------------------------

void TlsInfo::initVars()
{
	setMethod				 ( ISipAgent::TLS_SSLv23 );
	setVerifyCertificate	 ( false );
	setVerifyDepth			 ( 0 );
	setRequireCertificate	 ( false );
	setPrivateKey			 ( "" );
	setPrivateKeyPassword	 ( "" );
	setCertificateAuthorities( "" );
	setCertificate			 ( "" );
	setCrl					 ( "" );
	setCiphers				 ( "" );
}

//-----------------------------------------------------------------------------

TlsInfo& TlsInfo::operator=( const TlsInfo& src )
{
	if ( this != &src )
	{
		setMethod				 ( src.getMethod()					);
		setVerifyCertificate	 ( src.getVerifyCertificate()		);
		setVerifyDepth			 ( src.getVerifyDepth()				);
		setRequireCertificate	 ( src.getRequireCertificate()		);
		setPrivateKey			 ( src.getPrivateKey()				);
		setPrivateKeyPassword	 ( src.getPrivateKeyPassword()		);
		setCertificateAuthorities( src.getCertificateAuthorities()	);
		setCertificate			 ( src.getCertificate()				);
		setCrl					 ( src.getCrl()						);
		setCiphers				 ( src.getCiphers()					);
	}

	return *this;
}

//-----------------------------------------------------------------------------

bool TlsInfo::operator==( const TlsInfo& src )
{
	return true;	//TODO
}

//-----------------------------------------------------------------------------
//static
ISipAgent::TLSMethod TlsInfo::methodFromString( const std::string& tlsMethod )
{
	ISipAgent::TLSMethod result = ISipAgent::TLS_SSLv3;

	if ( tlsMethod == s_tlsMethodV3  || tlsMethod.empty() ) 
	{
		result = ISipAgent::TLS_SSLv3;
	}
	else if ( tlsMethod == s_tlsMethodV23 )
	{
		result = ISipAgent::TLS_SSLv23;
	}
	else if ( tlsMethod == s_tlsMethodV2 )
	{
		result = ISipAgent::TLS_SSLv2;
	}
	else if ( tlsMethod == s_tlsMethodV1 )
	{
		result = ISipAgent::TLS_TLSv1;
	}

	return result;
}

//-----------------------------------------------------------------------------
//static 
std::string TlsInfo::methodToString( ISipAgent::TLSMethod tlsMethod )
{
	std::string result;

	switch ( tlsMethod )
	{
	case ISipAgent::TLS_SSLv3:
		result = s_tlsMethodV3;
		break;

	case ISipAgent::TLS_SSLv23:
		result = s_tlsMethodV23;
		break;

	case ISipAgent::TLS_SSLv2:
		result = s_tlsMethodV2;
		break;

	case ISipAgent::TLS_TLSv1:
		result = s_tlsMethodV1;
		break;

	default:
		assert(false);
		result = "unknown";
	}

	return result;
}

//=============================================================================


//=============================================================================
// SipEvent class
//=============================================================================

SipEventData::SipEventData()
{
	initVars();
}

SipEventData::SipEventData( EventType eventType )
{
	initVars();
	setEventType( eventType );
}

void SipEventData::initVars()
{
	mEventType			= EventType::Unknown;
	
	mConnectionError	= false;
	mDisconnectMsg		= "";

	mLineId				= -1;		
	mLineState			= EnumPhoneLineState::PhoneLineState::PhoneLineStateUnknown;

	mCallId				= -1;
	mCallState			= EnumPhoneCallState::PhoneCallState::PhoneCallStateUnknown;	
	mCallFrom			= "";
	mCallStatus			= 0;

	//Info request
	mContent			= "";
	mContentType		= "";
}

//static 
SipEventData* SipEventData::makeConnectEvent()
{
	SipEventData* result = new SipEventData( EventType::Connect );

	return result;
}

//static 
SipEventData* SipEventData::makeDisconnectEvent( bool connectError, const std::string& disconnectMsg )
{
	SipEventData* result = new SipEventData( EventType::Disconnect );

	result->setConnectionError( connectError  );
	result->setDisconnectMsg  ( disconnectMsg );

	return result;
}

//static 
SipEventData* SipEventData::makeLineStateChangeEvent( int lineId, EnumPhoneLineState::PhoneLineState state )
{
	SipEventData* result = new SipEventData( EventType::LineStateChange );

	result->setLineId   ( lineId );
	result->setLineState( state  );

	return result;
}

//static
SipEventData* SipEventData::makeCallStateChangedEvent( int callId, EnumPhoneCallState::PhoneCallState state, const std::string& from, int status )
{
	SipEventData* result = new SipEventData( EventType::CallStateChange );

	result->setCallId    ( callId );
	result->setCallState ( state  );
	result->setCallFrom  ( from   );
	result->setCallStatus( status );

	return result;
}

//static 
SipEventData* SipEventData::makeInfoRequestReceivedEvent( int callId, const std::string& content, const std::string& contentType )
{
	SipEventData* result = new SipEventData( EventType::InfoRequest );

	result->setCallId     ( callId      );
	result->setContentType( contentType );
	result->setContent    ( content     );

	return result;
}

//static
SipEventData* SipEventData::makeLogEvent( const LogEntry& logEntry )
{
	SipEventData* result = new SipEventData( EventType::Log );

	result->setLogEntry( logEntry );

	return result;
}

//=============================================================================
