/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by Roman Sphount, jeff.theinert@voxox.com, Sep 2014
 */

#ifndef SIP_AGENT_API_WRAPPER_H
#define SIP_AGENT_API_WRAPPER_H

#include "DataTypes.h"
#include "DllExport.h"

#include "ISipAgent.h"
#include "SipConfig.h"

#include <set>
#include <map>
//#include <mutex>	//Recursive mutex and lock_guard	//We need to include this in Managed code, but CLR does not like this
class MyRecursiveMutex;

#include <assert.h>

namespace
{
	class LibraryHelper;
}

//-----------------------------------------------------------------------------

class SipAgentApiWrapper
{
private:
    SipAgentApiWrapper();

	bool populateLineConfig( ISipAgent::Line* line );

    std::string makeSipAddress(const std::string& contactId);

	bool createAgent();
    void doTerminate();

    void publishPresence( bool online, const std::string& note );

	mutable MyRecursiveMutex* d_mutex;

	AppEventHandler*	d_appEventHandler;	//Set by using app to interface with SIP events
	LogSendCallback*	d_logSendCallback;	//Small interface between SipWrapper and log events

    LibraryHelper*      d_libHelper;		/** load SipAgent lib */
    ISipAgent::Agent*   d_agent;			/** SIP User Agent Interface*/

	//SIP Configuration ------------------
	SipConfig			d_sipConfig;

	TlsInfo			    d_tlsInfo;			//We keep this as local memvar since we can only pass ITlsInfo        pointer to SipConfig.
	ServerInfoList		d_servers;			//We keep this as local memvar since we can only pass IServerInfoList pointer to SipConfig.

	//Audio ------------------------------
	AudioDevice d_inputAudioDevice;			/** Record audio device.	*/
	AudioDevice d_outputAudioDevice;		/** Playback audio device.	*/
	AudioDevice d_ringerAudioDevice;		/** Ringer audio device		*/

    bool		d_aec;
	bool		d_agc;						/** Auto Gain Control */
	bool		d_noiseSuppression;

	//Video -------------------------------
	std::string						d_videoDevice;
	EnumVideoQuality::VideoQuality	d_videoQuality;
	bool							d_flipVideoImage;

	//Misc and/or dynamic --------------------------
    bool d_registered;
    bool d_mustReinitNetwork;

private:
	//Helpers
	bool		 needHttpProxy();
	unsigned int getFirstSipServerPort();

	std::string formatSipUserAgent();
	std::string formatSdpOrigin();
	std::string formatSipUserUrl( const std::string& displayName, const std::string& userName, const ISipAgent::IServerInfo* registrarServer );
	std::string formatProxyUrl  ( const ISipAgent::IServerInfo* proxyServer );

public:
    static DLLEXPORT SipAgentApiWrapper* getInstance();

    DLLEXPORT ~SipAgentApiWrapper();


	DLLEXPORT void				setAppEventHandler( AppEventHandler* value )			{ d_appEventHandler = value;	}
	DLLEXPORT AppEventHandler*	getAppEventHandler()	const							{ return d_appEventHandler;		}

	//Connect and Disconnect callbacks from above
	void connectedEvent   ();
	void disconnectedEvent( bool connectionError, const std::string& reason );

	//Line and Call callbacks from old SipWrapper
	void phoneCallStateChangedEvent( int callId, EnumPhoneCallState::PhoneCallState state, const std::string & from, int statusCode );
	void phoneLineStateChangedEvent( int lineId, EnumPhoneLineState::PhoneLineState state );

	//INFO request received
	void infoRequestReceivedEvent( int callId, const std::string& content, const std::string& contentType );

	//LogEvent
	void sendLog( const LogEntry& logEntry );


	DLLEXPORT void setLogLevel( int level );
    DLLEXPORT void init( int logLevel = -1 );		/** Initializes the SIPWrapper. */
    DLLEXPORT void terminate();						/** Terminates the SIP connection. */


    /**
     * @return  True if is Initialized
     */
    DLLEXPORT bool isInitialized(void);

    /** @} */

    /**
     * @name Virtual Line Methods
     * @{
     */

     /**
     * Creates and adds a virtual phone line.
     *
     * @param displayName display name inside the SIP URI (e.g tanguy inside "tanguy <sip:tanguy_k@wengo.fr>")
     * @param username most of the time same as identity
     * @param identity first part of the SIP URI (e.g tanguy_k inside "sip:tanguy_k@wengo.fr")
     * @param password password corresponding to the SIP URI
     * @param realm realm/domain
     * @param proxyServer SIP proxy server
     * @param registerServer SIP register server
	 *
     * @return the corresponding id of the virtual line just created or VirtualLineIdError if failed to create the virtual line
     */
	
	DLLEXPORT bool setCredentials( const std::string& displayName, const std::string& username, const std::string& identity, 
								   const std::string& password,    const std::string& realm );

	DLLEXPORT int addVirtualLine();

    /**
     * Registers a given virtual phone line.
     *
     * @param   lineId id of the virtual phone line to register
     * @return  0 if making a register succeeds, otherwise 1
     */
    DLLEXPORT int registerVirtualLine(int lineId);

    /**
     * Removes a given virtual phone line.
     *
     * @param   lineId id of the virtual phone line to remove
     * @param   force if true, forces the removal without waiting for the unregister
                if false, removal is not forced
     */
    DLLEXPORT void removeVirtualLine( int lineId, bool force = false );
    /** @} */

    /**
     * @name Call Methods
     * @{
     */

    /**
     * Dials a phone number.
     *
     * @param lineId line to use to dial the phone number
     * @param sipAddress SIP address to call (e.g phone number to dial)
     * @param enableVideo enable/disable video usage
     * @return the phone call id (callId)
     */
    DLLEXPORT int makeCall( int lineId, const std::string& sipAddress, bool enableVideo );


    /**
     * Gets the SIP Dialog callId
     *
     * @param callId phone call id
     * @return SIP Dialog callId
     */
    DLLEXPORT std::string getSipCallId(int callId);

    /**
     * Notifies the remote side (the caller) that this phone is ringing.
     *
     * This corresponds to the SIP code "180 Ringing".
     *
     * @param callId id of the phone call to make ringing
     * @param enableVideo enable/disable video usage
     */
    DLLEXPORT void sendRingingNotification( int callId, bool enableVideo );

    /**
     * Accepts a given phone call.
     *
     * @param callId id of the phone call to accept
     * @param enableVideo enable/disable video usage
     */
    DLLEXPORT void acceptCall( int callId, bool enableVideo );

    /**
     * Rejects a given phone call.
     *
     * @param callId id of the phone call to reject
     */
    DLLEXPORT void rejectCall(int callId);

    /**
     * Closes a given phone call.
     *
     * @param callId id of the phone call to close
     */
    DLLEXPORT void closeCall(int callId);

    /**
     * Holds a given phone call.
     *
     * @param callId id of the phone call to hold
     */
    DLLEXPORT void holdCall(int callId);

    /**
     * Resumes a given phone call.
     *
     * @param callId id of the phone call to resume
     */
    DLLEXPORT void resumeCall(int callId);

    DLLEXPORT void muteCall( int callId, bool muteOn );

	DLLEXPORT void setCallInputVolume( int callId, unsigned int volume );			// Sets the microphone volume level. Valid range is [0, 200].
//  DLLEXPORT int  getMicVolume();										// Gets the microphone volume level.

    /**
     * Blind transfer the specified call to another party.
     *
     * @param callId id of the phone call to transfer
     * @param sipAddress transfer target
     */
    DLLEXPORT void blindTransfer(int callId, const std::string& sipAddress);

    /**
     * Sends a DTMF to a given phone call.
     *
     * @param callId phone call id to send a DTMF
     * @param dtmf DTMF tone to send
     */
    DLLEXPORT void playDtmf( int callId, char dtmf );

    /**
     * Sends and plays a sound file to a given phone call.
     *
     * @param callId phone call id to play the sound file
     * @param soundFile sound file to play
     */
    DLLEXPORT void playSoundFile( int callId, const std::string& soundFile );


    /**
     * @param callId phone call id
     * @return  True if encrytion is activated for the given call
     */
    DLLEXPORT bool isCallEncrypted(int callId);

    DLLEXPORT ISipAgent::ICallCallbacks* newCall( int lineID, ISipAgent::Call* call ) throw();

	DLLEXPORT ISipAgent::ICallCallbacks* newCallCallback( int callId ) throw();
    /** @} */


	//---------------------------------------------------------------------------------
	//Start SIP configuration set methods
	//---------------------------------------------------------------------------------
    /**
     * @name Configuration Methods
     * @{
     */

    /**
     * Sets calls encryption.
     *
     * @param enable if True encryption is activated
     */
	DLLEXPORT void setCallsEncryption( ISipAgent::Encryption val );

    /**
     * Sets proxy parameters.
     */
    DLLEXPORT void addProxy( const std::string& address, unsigned int port, const std::string& login, const std::string&  password, ISipAgent::HTTPProxy httpProxyType );

    /**
     * Sets HTTP tunnel parameters.
     */
    DLLEXPORT void addTunnel( const std::string& address, unsigned int port, bool ssl );	//VOXOX - JRT - 2012.02.03 - Not currently called anywhere.

    /**
     * Sets the SIP parameters.
     */
	DLLEXPORT void setTlsInfo( const TlsInfo& tlsInfo );
	DLLEXPORT void addServers( const ServerInfoList& servers );			//Bulk add servers, typically from SSO.

    DLLEXPORT void addSipProxy( const std::string& server, unsigned int serverPort );

	DLLEXPORT void setMaxCalls    ( int  val )								{ d_sipConfig.setMaxCalls    ( val );	}
	DLLEXPORT void setDoProxyCheck( bool val )								{ d_sipConfig.setDoProxyCheck( val );	}
	DLLEXPORT void setSipLogFile  ( const std::string& val )				{ d_sipConfig.setSipLogFile  ( val );	}

	//Discover Topology
	DLLEXPORT void setDiscoveryTopology ( ISipAgent::Topology val )			{ d_sipConfig.setDiscoveryTopology ( val );	}
	DLLEXPORT void enableIceMedia       ( bool val )						{ d_sipConfig.setEableIceMedia	   ( val );	}
	DLLEXPORT void enableIceSecurity    ( bool val )						{ d_sipConfig.setEnableIceSecurity ( val );	}
	DLLEXPORT void setTopologyEncryption( bool val )						{ d_sipConfig.setTopologyEncryption( val );	}
	DLLEXPORT void setTopologyTurn      ( bool val )						{ d_sipConfig.setTopologyTurn	   ( val ); }

	DLLEXPORT void setSipUdp   ( bool val )									{ d_sipConfig.setSipUdp   ( val );	}
	DLLEXPORT void setSipTcp   ( bool val )									{ d_sipConfig.setSipTcp   ( val );	}
	DLLEXPORT void setSipTls   ( bool val )									{ d_sipConfig.setSipTls   ( val );	}
	DLLEXPORT void setKeepAlive( bool val )									{ d_sipConfig.setKeepAlive( val );	}
	DLLEXPORT void setUseRport ( bool val )									{ d_sipConfig.setUseRport ( val );	}

	DLLEXPORT void setRegistrationRefreshInterval( int  val )				{ d_sipConfig.setRegistrationRefreshInterval( val );	}
	DLLEXPORT void setFailureInterval			 ( int  val )				{ d_sipConfig.setSipFailureTimeout          ( val );	}
	DLLEXPORT void setOptionsKeepAliveInterval   ( int  val )				{ d_sipConfig.setOptionsKeepAliveInterval   ( val );	}
	DLLEXPORT void setMinSessionTime			 ( int  val )				{ d_sipConfig.setMinSessionTime             ( val );	}
	DLLEXPORT void setSupportSessionTimer		 ( bool val )				{ d_sipConfig.setSupportSessionTimer        ( val );	}
	DLLEXPORT void setInitiateSessionTimer	     ( bool val )				{ d_sipConfig.setInitiateSessionTimer       ( val );	}

	DLLEXPORT void setTagProlog( const std::string& val )					{ d_sipConfig.setTagProlog( val );	}

	//Replaced generic method with specific methods.
	DLLEXPORT void setSipOptionRegisterTimeout    ( int  value )				{ d_sipConfig.setSipRegisterTimeout    ( value );	}	//In seconds
	DLLEXPORT void setSipOptionFailureTimeout     ( int  value )				{ d_sipConfig.setSipFailureTimeout     ( value );	}	//In seconds	
	DLLEXPORT void setSipOptionPublishTimeout     ( int  value )				{ d_sipConfig.setSipPublishTimeout     ( value );	}	//In seconds
	DLLEXPORT void setSipOptionUseOptionsRequest  ( bool value )				{ d_sipConfig.setSipUseOptionsRequest  ( value );	}
	DLLEXPORT void setSipOptionUseTypingState     ( bool value )				{ d_sipConfig.setSipUseTypingState	   ( value );	}
	DLLEXPORT void setSipOptionP2pPresence        ( bool value )				{ d_sipConfig.setSipP2pPresence        ( value );	}
	DLLEXPORT void setSipOptionChatWithoutPresence( bool value )				{ d_sipConfig.setSipChatWithoutPresence( value );	}	//Not sure we need this.
	DLLEXPORT void setSipOptionUuid               ( const std::string& value )	{ d_sipConfig.setSipUuid			   ( value );	}

    /**
     * Sets the call ringer/alerting device.
     *
     * @param device ringer device
     * @return true if no error, false otherwise
     */
	DLLEXPORT void setRingerOutputAudioDevice( const AudioDevice& device );

    /**
     * Sets the call input device (in-call microphone).
     *
     * @param device input device
     * @return true if no error, false otherwise
     */
	DLLEXPORT void setCallInputAudioDevice( const AudioDevice& device );

    /**
     * Sets the call output device (in-call speaker).
     *
     * @param device output device
     * @return true if no error, false otherwise
     */
	DLLEXPORT void setCallOutputAudioDevice( const AudioDevice& device );

    /**
     * Enables or disables Acoustic Echo Cancellation (AEC).
     *
     * @param enable true if AEC enable, false if AEC should be disabled
     */
    DLLEXPORT void enableAEC( bool enable );
	DLLEXPORT void enableAGC( bool enable );
	DLLEXPORT void enableNoiseSuppression( bool enable );


    /** @} */

    /**
     * @name Video Methods
     * @{
     */

    /**
     * Sets the video device.
     *
     * @param deviceName the name of the video device
     */
    DLLEXPORT void setVideoDevice( const std::string& deviceName );

    /**
     * Sets the Video Quality type.
     *
     * @see EnumVideoQuality
     */
    DLLEXPORT void setVideoQuality( EnumVideoQuality::VideoQuality videoQuality );

    /**
     * Set video image flip.
     * This parameter is dynamic so it can be set during a call.
     *
     * @param flip if true flip the image
     */
    DLLEXPORT void flipVideoImage( bool flip );
    /** @} */

	//---------------------------------------------------------------------------------
	//End SIP configuration set methods
	//---------------------------------------------------------------------------------


	//---------------------------------------------------------------------------------
	//Start SIP configuration get methods
	//	Most SIP settings are now in SipConfig and can be access via that object
	//---------------------------------------------------------------------------------
	DLLEXPORT AudioDevice	getRingerOutputAudioDevice() const					{ return d_ringerAudioDevice;		}
	DLLEXPORT AudioDevice	getCallInputAudioDevice   () const					{ return d_inputAudioDevice;		}
	DLLEXPORT AudioDevice	getCallOutputAudioDevice  () const					{ return d_outputAudioDevice;		}

	DLLEXPORT ServerInfoList getServers()	const;
	DLLEXPORT TlsInfo		 getTlsInfo()	const;

	DLLEXPORT int			 getLineCount()	const;


	//---------------------------------------------------------------------------------
	//End SIP configuration get methods
	//---------------------------------------------------------------------------------


	//---------------------------------------------------------------------------------
	//Call audio related methods
	//---------------------------------------------------------------------------------
	//Output
	DLLEXPORT bool setSpeakerVolume( unsigned int volume );		// Sets the speaker |volume| level. Valid range is [0,255].
    DLLEXPORT int  getSpeakerVolume();							// Gets the speaker |volume| level.

	//Ringing
    DLLEXPORT bool setRingingVolume( unsigned int volume );		// Sets the ringing |volume| level. Valid range is [0,255].
    DLLEXPORT int  getRingingVolume();							// Gets the ringing |volume| level.

	//Input
	DLLEXPORT bool setMicVolume( unsigned int volume );			// Sets the microphone volume level. Valid range is [0,255].
    DLLEXPORT int  getMicVolume();								// Gets the microphone volume level.

    DLLEXPORT int  getSpeechInputLevelFullRange();				// Gets the microphone speech |level|, mapped linearly to the range [0,32768].

	//---------------------------------------------------------------------------------
	//End call audio related methods
	//---------------------------------------------------------------------------------

    DLLEXPORT void registrationChanged( int lineId, ISipAgent::State state, ISipAgent::Event regEvent ) throw();

    DLLEXPORT bool isRegistered() const;
};

#endif  //SIP_AGENT_API_WRAPPER_H
