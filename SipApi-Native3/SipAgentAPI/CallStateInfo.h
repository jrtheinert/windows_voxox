
//Small class to contain information related to callState changes
#pragma once

#include "../SipAgentAPI/ISipAgent.h"
#include <string>

class CallStateInfo
{
public:
	CallStateInfo() 
		: mIsIncoming( false )
		, mLineId( -1 )
		, mCallId( -1 )
		, mStatus ( -1 )
		, mUserData( nullptr )
		, mCallState( ISipAgent::CallState::CALL_STATE_IDLE )
	{
	}

	//gets --------------------------------------------------------------------
	bool					isIncoming()		const		{ return mIsIncoming;	}	
	int						getLineId()			const		{ return mLineId;		}
	int						getCallId()			const		{ return mCallId;		}
	ISipAgent::CallState	getCallState()		const		{ return mCallState;	}
	int						getStatus()			const		{ return mStatus;		}
	void*					getUserData()		const		{ return mUserData;		}

	std::string				getReason()			const		{ return mReason;			}
	std::string				getRemoteContact()	const		{ return mRemoteContact;	}
	std::string				getRemoteParty()	const		{ return mRemoteParty;		}

	//sets --------------------------------------------------------------------
	void setIsIncoming( bool				 val )			{ mIsIncoming		= val;	}
	void setLineId	  ( int					 val )			{ mLineId			= val;	}
	void setCallId	  ( int				     val )			{ mCallId			= val;	}
	void setCallState ( ISipAgent::CallState val )			{ mCallState		= val;	}
	void setStatus	  ( int					 val )			{ mStatus			= val;	}
	void setUserData  ( void*				 val )			{ mUserData			= val;	}	

	void setReason       ( const std::string& val )			{ mReason       	= val;	}
	void setRemoteContact( const std::string& val )			{ mRemoteContact	= val;	}
	void setRemoteParty  ( const std::string& val )			{ mRemoteParty		= val;	}

private:
	bool					mIsIncoming;
	int						mLineId;
	int						mCallId;
	ISipAgent::CallState	mCallState;
	int						mStatus;
	void*					mUserData;
	
	std::string				mReason;
	std::string				mRemoteContact;
	std::string				mRemoteParty;
};
