
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <winsock2.h>
#include <windows.h>
#include <wininet.h>
#include <shlobj.h> 

#include "ProxyDetect.h"
#include "InetUtil.h"
#include "StringUtil.h"

#pragma comment(lib, "wininet.lib") 

static const size_t s_maxLineLength = 1024;

typedef struct
{
	DWORD  dwAccessType;      // see WINHTTP_ACCESS_* types below
	LPWSTR lpszProxy;         // proxy server list
	LPWSTR lpszProxyBypass;   // proxy bypass list
} WINHTTP_PROXY_INFO, *LPWINHTTP_PROXY_INFO;

typedef struct
{
	DWORD   dwFlags;
	DWORD   dwAutoDetectFlags;
	LPCWSTR lpszAutoConfigUrl;
	LPVOID  lpvReserved;
	DWORD   dwReserved;
	BOOL    fAutoLogonIfChallenged;
} WINHTTP_AUTOPROXY_OPTIONS;

typedef struct
{
	BOOL    fAutoDetect;
	LPWSTR  lpszAutoConfigUrl;
	LPWSTR  lpszProxy;
	LPWSTR  lpszProxyBypass;
} WINHTTP_CURRENT_USER_IE_PROXY_CONFIG;

typedef HINTERNET(WINAPI * pfnWinHttpOpen)
(
	IN LPCWSTR pwszUserAgent,
	IN DWORD   dwAccessType,
	IN LPCWSTR pwszProxyName   OPTIONAL,
	IN LPCWSTR pwszProxyBypass OPTIONAL,
	IN DWORD   dwFlags
);

typedef BOOL(STDAPICALLTYPE * pfnWinHttpCloseHandle)
(
	IN HINTERNET hInternet
);

typedef BOOL(STDAPICALLTYPE * pfnWinHttpGetProxyForUrl)
(
	IN  HINTERNET                   hSession,
	IN  LPCWSTR                     lpcwszUrl,
	IN  WINHTTP_AUTOPROXY_OPTIONS*	pAutoProxyOptions,
	OUT WINHTTP_PROXY_INFO*         pProxyInfo
);

typedef BOOL(STDAPICALLTYPE* pfnWinHttpGetIEProxyConfig)
(
    IN OUT WINHTTP_CURRENT_USER_IE_PROXY_CONFIG * pProxyConfig
);

#define WINHTTP_AUTOPROXY_AUTO_DETECT           0x00000001
#define WINHTTP_AUTOPROXY_CONFIG_URL            0x00000002
//#define WINHTTP_AUTOPROXY_RUN_INPROCESS         0x00010000
//#define WINHTTP_AUTOPROXY_RUN_OUTPROCESS_ONLY   0x00020000
#define WINHTTP_AUTO_DETECT_TYPE_DHCP           0x00000001
#define WINHTTP_AUTO_DETECT_TYPE_DNS_A          0x00000002
//#define WINHTTP_ACCESS_TYPE_DEFAULT_PROXY               0
#define WINHTTP_ACCESS_TYPE_NO_PROXY                    1
//#define WINHTTP_ACCESS_TYPE_NAMED_PROXY                 3
#define WINHTTP_NO_PROXY_NAME					NULL
#define WINHTTP_NO_PROXY_BYPASS					NULL


ProxyDetect::ProxyDetect( const char* host, unsigned short port, ProxyInfo* proxyInfo, ILogger* logger )
	: mHost( host )
	, mPort( port )
	, mProxyInfo( proxyInfo )
	, mLogger( logger )
{
}

// Determines whether the simple wildcard pattern matches target.
// Alpha characters in pattern match case-insensitively.
// Asterisks in pattern match 0 or more characters.
// Ex: string_match("www.TEST.GOOGLE.COM", "www.*.com") -> true 
bool ProxyDetect::stringMatch( const char* target, const char* pattern )
{
    while (*pattern)
    {
        if (*pattern == '*')
        {
            if (!*++pattern)
                return true;

            while (*target)
            {
                if ( StringUtil::areEqualNoCase( pattern, target ) && stringMatch( target + 1, pattern + 1 ) )
                {
                    return true;
                }

                ++target;
            }

            return false;
        }
        else
        {
            if (StringUtil::toUpper(*pattern) != StringUtil::toUpper(*target))
                return false;

            ++target;
            ++pattern;
        }
    }

    return !*target;
}

bool ProxyDetect::proxyItemMatch( const char* urlHost, unsigned short urlPort, char* item, size_t len )
{
    // hostname:443
    char * port = ::strchr(item, ':');

    if (port)
    {
        *port++ = '\0';
        if (urlPort != atol(port))
            return false;
    }

    // A.B.C.D or A.B.C.D/24
    int a;
	int b;
	int c;
	int d;
	int m;
    int match = sscanf(item, "%d.%d.%d.%d/%d", &a, &b, &c, &d, &m);
    
	if (match >= 4)
    {
        unsigned ip = ((a & 0xFF) << 24) | ((b & 0xFF) << 16) | ((c & 0xFF) << 8) | (d & 0xFF);

        if ((match < 5) || (m > 32))
            m = 32;
        else if (m < 0)
            m = 0;

        unsigned mask = (m == 0) ? 0 : (~0UL) << (32 - m);
        unsigned long addr = InetUtil::addressToNet(urlHost);
        return addr != (unsigned long)(-1) && ((InetUtil::netToHost(addr) & mask) == (ip & mask));
    }

    // .foo.com
    if (*item == '.')
    {
        size_t hostLen = strlen(urlHost);
        return (hostLen > len) && (StringUtil::areEqualNoCase( urlHost + (hostLen - len), item) );
    }

    // localhost or www.*.com
    if (!stringMatch(urlHost, item))
        return false;

    return true;
}

bool ProxyDetect::proxyListMatch( char sep ) 
{
	return proxyListMatch( mHost.c_str(), mPort, mProxyInfo->bypassList.c_str(), sep );
}

bool ProxyDetect::proxyListMatch( const char* urlHost, unsigned short urlPort, const char* list, char sep ) 
{
    const size_t BUFSIZE = 256;
    char buffer[BUFSIZE];

    while ( *list )
    {
        // Remove leading space
        if (StringUtil::isSpace(*list))
        {
            ++list;
            continue;
        }
    
		// Break on separator
        size_t		len	  = 0;
        const char* start = list;
        
		if (const char * end = ::strchr(list, sep))
        {
            len = (end - list);
            list += len + 1;
        }
        else
        {
            len = strlen(list);
            list += len;
        }

        // Remove trailing space
        while ((len > 0) && StringUtil::isSpace(start[len - 1]))
            --len;

        // Check for oversized entry
        if (len >= BUFSIZE)
            continue;
        
		memcpy(buffer, start, len);
        buffer[len] = 0;
        
		if (!proxyItemMatch(urlHost, urlPort, buffer, len))
            continue;
        
		return true;
    }

    return false;
}

bool ProxyDetect::better( ProxyType lhs, const ProxyType rhs )
{
    // PROXY_NONE, PROXY_HTTPS, PROXY_SOCKS5, PROXY_UNKNOWN
    const int PROXY_VALUE[5] = { 0, 2, 3, 1 };
    return (PROXY_VALUE[lhs] > PROXY_VALUE[rhs]);
}

bool ProxyDetect::parseProxy( const char* address )
{
	if ( address == nullptr )
		return false;

    const size_t kMaxAddressLength = 1024;
    const char* const kAddressSeparator = " ;\t";    // Allow semicolon, space, or tab as an address separator

    ProxyType		ptype;
	std::string		host;
    unsigned short	port;

    while (*address)
    {
        size_t		len	  = 0;
        const char* start = address;
        const char* sep   = address;

        while ( *sep != 0 && *sep != ' ' && *sep != ';' && *sep != '\t' )
            ++sep;

        if (*sep)
        {
            len      = (sep - address);
            address += len + 1;

            while (*address != '\0' && (*address == ' ' || *address == '\t' || *address == ';'))
                ++address;
        }
        else
        {
            len      = strlen(address);
            address += len;
        }

        if (len > kMaxAddressLength - 1)
        {
            // Proxy address too long 
            continue;
        }

        char buffer[kMaxAddressLength];
        memcpy(buffer, start, len);
        buffer[len] = 0;

        char * colon = ::strchr(buffer, ':');
        if (!colon)
        {
            continue;            // Proxy address without port
        }

        char* endptr = nullptr;

        *colon = 0;
        port   = static_cast<unsigned short>(strtol(colon + 1, &endptr, 0));

        if (*endptr != 0) 
        {
            continue;            // Proxy address with invalid port
        }

        char * equals = ::strchr(buffer, '=');
        if (equals)
        {
            *equals = 0;
            host    = equals + 1;

            if ( StringUtil::areEqualNoCase( buffer, "socks" ) )
                ptype = PROXY_SOCKS5;
            else if ( StringUtil::areEqualNoCase( buffer, "https" ) )
                ptype = PROXY_HTTPS;
            else
            {
                ptype = PROXY_UNKNOWN;                // Proxy address with unknown protocol
            }
        }
        else
        {
            host = buffer;
            ptype = PROXY_UNKNOWN;
        }

        if (better(ptype, mProxyInfo->type))
        {
            mProxyInfo->type		= ptype;
            mProxyInfo->address		= host;
            mProxyInfo->addressLong = InetUtil::addressToNet( host.c_str() );

			if ( mProxyInfo->addressLong == -1 )	//Indicates 'host' was/had invalid dot-notation, so it maybe a hostname
			{
				//Some test/debug code
				bool useTest = false;

				if ( useTest)
				{
					mProxyInfo->addressLong = InetUtil::addressToNet ("16.216.235.20");
					mProxyInfo->addressLong = InetUtil::hostNameToNet( "www.google.com" );
					mProxyInfo->addressLong = InetUtil::hostNameToNet( "proxy-ccy.houston.hp.com" );
				}
				else
				{
					//Actual code.
					mProxyInfo->address     = host;
					mProxyInfo->addressLong = InetUtil::hostNameToNet( host.c_str() );
				}
			}

            mProxyInfo->port =  port;
        }
    }

    return mProxyInfo->type != PROXY_NONE;
}


//-----------------------------------------------------------------------------
//Begin Firefox specific methods
//-----------------------------------------------------------------------------

std::string ProxyDetect::updatePath( const char* path )
{
    std::string result = StringUtil::trim( path );

    if ( !result.empty() )
    {
		//Replace backslash with forward slash.
		result = StringUtil::replace( result, "\\", "/", false );

		//Add trailing forward slash
		if ( !StringUtil::endsWith( result, "/", false ) )
            result += '/';
    }
    return result;
}

bool ProxyDetect::getFirefoxProfilePath( std::string& path )
{
    wchar_t w_path[MAX_PATH];

	if (SHGetFolderPath(0, CSIDL_APPDATA, 0, SHGFP_TYPE_CURRENT, w_path) != S_OK)
    {
        return false;        // SHGetFolderPath failed
    }
    
	path = updatePath( StringUtil::toUtf8( w_path ).c_str() );
    path += "/Mozilla/Firefox/";

	return true;
}

bool ProxyDetect::getDefaultFirefoxProfile( std::string& profilePath )
{
    std::string path;

    if ( !getFirefoxProfilePath( path ) )
        return false;

    // [Profile0]
    // Name=default
    // IsRelative=1
    // Path=Profiles/2de53ejb.default
    // Default=1

    // Note: we are looking for the first entry with "Default=1", or the last entry in the file.
    path += "profiles.ini";
    FILE* fs = fopen( path.c_str(), "r");
    if (!fs)
        return false;

    std::string candidate;
    bool		relative = true;
    char		line[s_maxLineLength];

    while ( fgets(line, s_maxLineLength, fs) != 0 )
    {
        size_t len = strlen(line);

        if (len == 0)
            continue;

        if (line[0] == '[')
        {
            relative = true;
            candidate.clear();
        }
        else if ( StringUtil::beginsWith( line, "IsRelative=", true ) )
        {
            // TODO: The initial Linux public launch revealed a fairly
            // high number of machines where IsRelative= did not have anything after
            // it. Perhaps that is legal profiles.ini syntax?
            relative = (line[11] != '0');
        }
        else if ( StringUtil::beginsWith( line, "Path=", true ) )
        {
            if (relative)
                candidate = path;
            else
                candidate.clear();

            candidate  = updatePath( candidate.c_str() );
            candidate += StringUtil::trim( line + 5 );
            candidate  = updatePath (candidate.c_str() );
        }
        else if ( StringUtil::beginsWith( line, "Default=", true ) )
        {
            if ((line[8] != '0') && !candidate.empty())
                break;
        }
    }

    fclose(fs);
    
	if (candidate.empty())
        return false;

    profilePath = candidate;

	return true;
}

bool ProxyDetect::readFirefoxPrefs( const std::string& filename, const char* prefix )
{
    FILE* fs = fopen( filename.c_str(), "r");

    if (!fs)
    {
        return false;        // Failed to open file
    }

    char buffer[s_maxLineLength];

    while (fgets(buffer, s_maxLineLength, fs) != 0)
    {
        size_t prefix_len = strlen(prefix);
        size_t lineLen    = strlen(buffer);

        // Skip blank lines and too long lines.
        if ((lineLen == 0) 
            || (lineLen > s_maxLineLength)
            || (buffer[0] == '#') 
            || (buffer[0] == '/' && buffer[1] == '*')
            || (buffer[0] == ' ' && buffer[1] == '*'))
        {
            continue;
        }

        int nstart = 0, nend = 0, vstart = 0, vend = 0;
        sscanf(buffer, "user_pref(\"%n%*[^\"]%n\", %n%*[^)]%n);", &nstart, &nend, &vstart, &vend);

        if (vend > 0)
        {
            char* name = buffer + nstart;
            name[nend - nstart] = 0;

            if ((vend - vstart >= 2) && (buffer[vstart] == '"'))
            {
                vstart += 1;
                vend -= 1;
            }

            char* value = buffer + vstart;
            value[vend - vstart] = 0;
            
			if ((strncmp(name, prefix, prefix_len) == 0) && *value)
            {
//                SimpleMap<CStr, CStr, MapStrICmp>::Iterator it = settings->pushBack();
//                it->d_first = name + prefix_len;
//                it->d_second = value;
				mSettings.add( name + prefix_len, value );
            }
        }
        else
        {
            // Unparsed pref
        }
    }

    fclose(fs);
    
	return true;
}

std::string ProxyDetect::getFirefoxSetting( const std::string& key, const std::string& def )
{
    std::string result = def;
	
	if ( mSettings.hasKey( key ) )
	{
		result = mSettings.getValue( key );
	}

	return result;
}

bool ProxyDetect::getFirefoxProxySettings()
{
    std::string path;
    bool		success = false;
    
	if ( getDefaultFirefoxProfile( path) )
    {
        path += "prefs.js";

        if (readFirefoxPrefs( path, "network.proxy." ) )
        {
            success = true;
            mProxyInfo->bypassList = getFirefoxSetting( "no_proxies_on", "localhost, 127.0.0.1" );

            std::string settingType = getFirefoxSetting( "type", "" );

			int typeInt = atoi( settingType.c_str() );

			switch ( typeInt )
			{
			case 1:
                // User has manually specified a proxy, try to figure out what type it is.
                if ( proxyListMatch( ',' ) )
                {
                    // Our url is in the list of url's to bypass proxy.
                }
                else if ( StringUtil::areEqualNoCase( getFirefoxSetting( "share_proxy_settings", "" ), "true") )
                {
					setFireFoxProxyInfo( PROXY_UNKNOWN, "http", "http_port" );
                }
                else if (mSettings.hasKey("socks"))
                {
					setFireFoxProxyInfo( PROXY_SOCKS5, "socks", "socks_port" );
                }
                else if (mSettings.hasKey("ssl"))
                {
					setFireFoxProxyInfo( PROXY_HTTPS, "ssl", "ssl_port" );
                }
                else if (mSettings.hasKey("http"))
                {
					setFireFoxProxyInfo( PROXY_HTTPS, "http", "http_port" );
                }
				break;

			case 2:
                // Browser is configured to get proxy settings from a given url.
                mProxyInfo->autoconfigUrl = getFirefoxSetting( "autoconfig_url", "" );
				break;

			case 4:
                // Browser is configured to auto detect proxy config.
                mProxyInfo->autodetect = true;
				break;

			default:
                // No proxy set.
				break;
            }
        }
    }
    return success;
}

void ProxyDetect::setFireFoxProxyInfo( ProxyType type, const std::string& typeKey, const std::string& typePortKey )
{
	std::string address = getFirefoxSetting( typeKey,     "" );
	std::string port	= getFirefoxSetting( typePortKey, "" );

    mProxyInfo->type		= type;
    mProxyInfo->address		= address;
    mProxyInfo->addressLong = InetUtil::addressToNet( address.c_str() );
    mProxyInfo->port		= atoi( port.c_str() );
}

//-----------------------------------------------------------------------------
//End Firefox specific methods
//-----------------------------------------------------------------------------



//-----------------------------------------------------------------------------
//Begin generic Proxy methods
//-----------------------------------------------------------------------------

// Explorer proxy settings.

static BOOL myWinHttpGetProxyForUrl( pfnWinHttpGetProxyForUrl pWHGPFU,   HINTERNET hWinHttp, LPCWSTR url,
									 WINHTTP_AUTOPROXY_OPTIONS* options, WINHTTP_PROXY_INFO* info )
{
    // WinHttpGetProxyForUrl() can call plugins which can crash.
    // In the case of McAfee scriptproxy.dll, it does crash in
    // older versions. Try to catch crashes here and treat as an error.
    BOOL success = FALSE;

#if (_HAS_EXCEPTIONS == 0)
    __try 
    {
        success = pWHGPFU(hWinHttp, url, options, info);
    }
    __except(EXCEPTION_EXECUTE_HANDLER)
    {
        // This is a separate function to avoid
        // Visual C++ error 2712 when compiling with C++ EH
    }
#else
    success = pWHGPFU(hWinHttp, url, options, info);
#endif  // (_HAS_EXCEPTIONS == 0)

    return success;
}

bool ProxyDetect::isDefaultBrowserFirefox()
{
    bool success = false;
    HKEY key;
    LONG result = RegOpenKeyExA( HKEY_CLASSES_ROOT,  "http\\shell\\open\\command", 0, KEY_READ, &key );

    if ( ERROR_SUCCESS == result )
	{
		DWORD size;
		DWORD type;

		result = RegQueryValueExA(key, "", NULL, &type, NULL, &size);	//Determine key is valid and the string length.

		if (result == ERROR_SUCCESS && type == REG_SZ)
		{
			char* value  = new char[size];
			BYTE* buffer = reinterpret_cast<BYTE*>( value );
		
			result = RegQueryValueExA(key, "", 0, &type, buffer, &size);	//Get key value.

			if (result == ERROR_SUCCESS)
			{
				buffer[size] = '\0';
				success = (StringUtil::contains( value, "firefox.exe", false ) );
			}
		}
	}

	RegCloseKey(key);
    
	return success;
}

bool ProxyDetect::getWinHttpProxySettings()
{
	logDebug( "ProxyDetect::getWinHttpProxySettings(): LoadLibrary()");

    HMODULE winhttp_handle = LoadLibrary( L"winhttp.dll"  );

    if (winhttp_handle == NULL)
    {
        return false;        // Failed to load winhttp.dll
    }

	logDebug( "ProxyDetect::getWinHttpProxySettings(): GetProcAddress()");
    WINHTTP_CURRENT_USER_IE_PROXY_CONFIG iecfg;
    memset(&iecfg, 0, sizeof(iecfg));
    pfnWinHttpGetIEProxyConfig pWHGIEPC = reinterpret_cast<pfnWinHttpGetIEProxyConfig>( GetProcAddress(winhttp_handle, "WinHttpGetIEProxyConfigForCurrentUser"));
    
	bool success = false;
    
	logDebug( "ProxyDetect::getWinHttpProxySettings(): if ( pWHGIEPC && pWHGIEPC(&iecfg)");
	if ( pWHGIEPC && pWHGIEPC(&iecfg) )
    {
        // We were read proxy config successfully.
        success = true;

        if (iecfg.fAutoDetect)
		{
			logDebug( "ProxyDetect::getWinHttpProxySettings(): mProxyInfo->autodetect");
			mProxyInfo->autodetect = true;
		}

        if (iecfg.lpszAutoConfigUrl)
        {
			logDebug( "ProxyDetect::getWinHttpProxySettings(): mProxyInfo->autoconfigUrl");
            mProxyInfo->autoconfigUrl = StringUtil::toUtf8(iecfg.lpszAutoConfigUrl);
            GlobalFree(iecfg.lpszAutoConfigUrl);
        }

        if (iecfg.lpszProxyBypass)
        {
			logDebug( "ProxyDetect::getWinHttpProxySettings(): Before mProxyInfo->bypassList" );
            mProxyInfo->bypassList = StringUtil::toUtf8(iecfg.lpszProxyBypass);
            GlobalFree(iecfg.lpszProxyBypass);
        }

        if (iecfg.lpszProxy)
        {
			logDebug( "ProxyDetect::getWinHttpProxySettings(): Before proxyListMatch()" );
            if (!proxyListMatch( ';'))
			{
				logDebug( "ProxyDetect::getWinHttpProxySettings(): Before parseProxy()");
                parseProxy( StringUtil::toUtf8(iecfg.lpszProxy).c_str() );
			}

            GlobalFree(iecfg.lpszProxy);
        }
    }

	logDebug( "ProxyDetect::getWinHttpProxySettings(): FreeLibrary()");
    FreeLibrary(winhttp_handle);

	logDebug( "ProxyDetect::getWinHttpProxySettings(): Before return");
    return success;
}

// Uses the WinHTTP API to auto detect proxy for the given url. Firefox and IE
// have slightly different option dialogs for proxy settings. 
//	- In Firefox, either a location of a proxy configuration file can be specified
//		or auto detection can be selected. 
//	- In IE these two options can be independently selected. For the case where 
//		both options are selected (only IE) we try to fetch the config file first, 
//		and if that fails we'll perform an auto detection.
//
// Returns true if we successfully performed an auto detection not depending on
// whether we found a proxy or not. Returns false on error.

bool ProxyDetect::autoDetectProxyForUrl()
{
    bool    success        = true;
    HMODULE winhttp_handle = LoadLibrary(L"winhttp.dll");

    if (winhttp_handle == NULL)
    {
        return false;        // Failed to load winhttp.dll."
    }

    pfnWinHttpOpen			 pWHO	 = reinterpret_cast<pfnWinHttpOpen>			 ( GetProcAddress(winhttp_handle, "WinHttpOpen"			  ) );
    pfnWinHttpCloseHandle	 pWHCH	 = reinterpret_cast<pfnWinHttpCloseHandle>	 ( GetProcAddress(winhttp_handle, "WinHttpCloseHandle"	  ) );
    pfnWinHttpGetProxyForUrl pWHGPFU = reinterpret_cast<pfnWinHttpGetProxyForUrl>( GetProcAddress(winhttp_handle, "WinHttpGetProxyForUrl" ) );

    if (pWHO && pWHCH && pWHGPFU)
    {
        HINTERNET hWinHttp = pWHO( 0, WINHTTP_ACCESS_TYPE_NO_PROXY, WINHTTP_NO_PROXY_NAME, WINHTTP_NO_PROXY_BYPASS, 0 );

        if (hWinHttp)
        {
            BOOL				result = FALSE;
            WINHTTP_PROXY_INFO	info;
            wchar_t				url[128];

            memset(&info, 0, sizeof(info));
            wsprintf(url, L"http://%hs:%hu", mHost.c_str(), mPort );

            if ( mProxyInfo->autodetect)
            {
                // Use DHCP and DNS to try to find any proxy to use.
                WINHTTP_AUTOPROXY_OPTIONS options;
                memset(&options, 0, sizeof(options));

                options.fAutoLogonIfChallenged = TRUE;
				options.dwFlags				  |= WINHTTP_AUTOPROXY_AUTO_DETECT;
                options.dwAutoDetectFlags	  |= WINHTTP_AUTO_DETECT_TYPE_DHCP | WINHTTP_AUTO_DETECT_TYPE_DNS_A;

                result = myWinHttpGetProxyForUrl( pWHGPFU, hWinHttp, url, &options, &info );
            }

            if (!result && !mProxyInfo->autoconfigUrl.empty() )
            {
                // We have the location of a proxy config file. Download it and
                // execute it to find proxy settings for our url.
                WINHTTP_AUTOPROXY_OPTIONS options;
                memset(&options, 0, sizeof(options));
                memset(&info, 0, sizeof(info));
                options.fAutoLogonIfChallenged = TRUE;

                int		 len8			= static_cast<int>( mProxyInfo->autoconfigUrl.size()+1);	//JRT
                int		 len16			= ::MultiByteToWideChar(CP_UTF8, 0, mProxyInfo->autoconfigUrl.c_str(), len8, NULL, 0);
                wchar_t* autoConfigUrl	= static_cast<wchar_t*>(::alloca(((len16 + 1) * sizeof(wchar_t))));

                ::MultiByteToWideChar(CP_UTF8, 0, mProxyInfo->autoconfigUrl.c_str(), len8, autoConfigUrl, len16);

                autoConfigUrl[len16]	   = 0;

                options.lpszAutoConfigUrl  = autoConfigUrl;
                options.dwFlags			  |= WINHTTP_AUTOPROXY_CONFIG_URL;

                result = myWinHttpGetProxyForUrl(pWHGPFU, hWinHttp, url, &options, &info);
            }

            if (result)
            {
                // Either the given auto config url was valid or auto
                // detection found a proxy on this network.
                if (info.lpszProxy)
                {
                    // TODO: Does this bypass list differ from the list
                    // retreived from getWinHttpProxySettings earlier?
                    if (info.lpszProxyBypass)
                    {
                        mProxyInfo->bypassList = StringUtil::toUtf8(info.lpszProxyBypass);
                        GlobalFree(info.lpszProxyBypass);
                    }
                    else
					{
                        mProxyInfo->bypassList.clear();
					}

                    if (!proxyListMatch( ';'))
                    {
                        // Found proxy for this URL. If parsing the address turns out OK then we are successful.
                        success = parseProxy( StringUtil::toUtf8(info.lpszProxy).c_str() );
                    }
                    
					GlobalFree(info.lpszProxy);
                }
            }
            else
            {
                // We could not find any proxy for this url.
            }

            pWHCH(hWinHttp);
        }
    }
    else
    {
        success = false;
    }

    FreeLibrary(winhttp_handle);
    
	return success;
}

// Uses the InternetQueryOption function to retrieve proxy settings
// from the registry. This will only give us the 'static' settings,
// ie, not any information about auto config etc.

bool ProxyDetect::getIeLanProxySettings()
{
    bool success = false;

    char buffer[2048];
    memset(buffer, 0, sizeof(buffer));

    INTERNET_PROXY_INFO* info = reinterpret_cast<INTERNET_PROXY_INFO *>(buffer);
    DWORD				 dwSize = sizeof(buffer);

    if (!InternetQueryOption(0, INTERNET_OPTION_PROXY, info, &dwSize))
    {
        // InternetQueryOption failed
    }
    else if (info->dwAccessType == INTERNET_OPEN_TYPE_DIRECT)
    {
        success = true;
    }
    else if (info->dwAccessType == INTERNET_OPEN_TYPE_PROXY)
    {
        success = true;
        const char* str = reinterpret_cast<const char*>(info->lpszProxyBypass);

        if (str == 0 || !proxyListMatch( mHost.c_str(), mPort, str, ' ') )
        {
            str = reinterpret_cast<const char*>(info->lpszProxy);
        
			if(str)
                parseProxy(str );
        }
    }
    else
    {
        // unknown internet access type
    }

    return success;
}

bool ProxyDetect::detect()
{
	bool result = false;

	logDebug( "ProxyDetect::detect():  Before isDefaultBrowserFirefox()" );
    if (isDefaultBrowserFirefox())
	{
		logDebug( "ProxyDetect::detect():  Before getFirefoxProxySettings()" );
        result = getFirefoxProxySettings();
		logDebug( "ProxyDetect::detect():  After isDefaultBrowserFirefox()" );
	}
    else
    {
		logDebug( "ProxyDetect::detect():  Before getWinHttpProxySettings()" );
        result = getWinHttpProxySettings();
		logDebug( "ProxyDetect::detect():  After getWinHttpProxySettings()" );

        if (!result)
        {
            // WinHttp failed. Try using the InternetOptionQuery method instead.
			logDebug( "ProxyDetect::detect():  Before getIeLanProxySettings()" );
            result = getIeLanProxySettings();
			logDebug( "ProxyDetect::detect():  After getIeLanProxySettings()" );
        }
    }

    if (result && ( mProxyInfo->autodetect || !mProxyInfo->autoconfigUrl.empty()))
    {
        // Use WinHTTP to auto detect proxy for us.
		logDebug( "ProxyDetect::detect():  Before autoDetectProxyForUrl()" );
        result = autoDetectProxyForUrl();
		logDebug( "ProxyDetect::detect():  After autoDetectProxyForUrl()" );

        if (!result)
        {
            // Either auto detection is not supported or we simply didn't find any proxy, reset type.
            mProxyInfo->type = PROXY_NONE;
        }
    }

	logDebug( "ProxyDetect::detect():  Before return" );
	return result;
}

void ProxyDetect::logDebug( const std::string& msg )
{
	if ( mLogger )
	{
		mLogger->debug( msg );
	}
}
