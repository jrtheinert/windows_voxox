
#pragma once

/********************************************************************/
/* ProxyDetect.h                                                    */
/* Implementation of HTTP Proxy Detection                           */
/* (c) 2011 Telurix LLC.											*/
/*																	*/
/* Modifed by Jeff Theinert, Voxox	2015.07.31						*/
/*                                                                  */
/********************************************************************/

#include <map>
#include <string>

#include "../SipAgentAPI/DataTypes.h"

class ProxyDetect
{
public:
	enum ProxyType
	{
		PROXY_NONE    = 0,
		PROXY_HTTPS	  = 1,
		PROXY_SOCKS5  = 2,
		PROXY_UNKNOWN = 3
	};

	struct ProxyInfo
	{
		ProxyType		type;
		std::string		address;
		unsigned long	addressLong;
		unsigned short	port;
		std::string		autoconfigUrl;
		bool			autodetect;
		std::string		bypassList;
		std::string		username;
		std::string		password;

		ProxyInfo() throw()
		:   type(PROXY_NONE)
		,   addressLong(0)
		,   port(0)
		,   autodetect(false)
		{}
	};

	class Settings :public std::map< std::string, std::string >
	{
	public:
		void add( const std::string& key, const std::string& value )
		{
			std::pair< std::string, std::string> pair( key, value );
			insert( pair );
		}

		bool hasKey( const std::string& key )
		{
			std::map<std::string, std::string>::iterator it = this->find( key );

			return it != end();
		}

		std::string getValue( const std::string& key )
		{
			std::string result;

			std::map<std::string, std::string>::iterator it = this->find( key );

			if ( it != end() )
			{
				result = (*it).second;
			}

			return result;
		}

	};
 
	// Auto-detect the proxy server.  Returns true if a proxy is configured,
	// although hostname may be empty if the proxy is not required for the given URL.
//	static bool getProxySettingsForUrl( const char* host, unsigned short port, ProxyInfo* proxyInfo );

	ProxyDetect( const char* host, unsigned short port, ProxyInfo* proxyInfo, ILogger* logger );

	bool detect();

private:
	bool stringMatch   ( const char* target, const char* pattern );									//Used in proxyItemMatch()
	bool proxyItemMatch( const char* urlHost, unsigned short urlPort, char* item, size_t len );		//Used in proxyListMatch()
	bool proxyListMatch( const char* urlHost, unsigned short urlPort, const char* list, char sep );	//Main matching logic
	bool proxyListMatch( char sep );

	bool better    ( ProxyType lhs, const ProxyType rhs );				//Used in parseProxy()
	bool parseProxy( const char* address );								//Used in getXXXProxySettings()

	//Firefox support
	std::string	updatePath				( const char* path );			//Used for Firefox profile path
	bool		getFirefoxProfilePath   ( std::string& path );			//Get Firefox profile path
	bool		getDefaultFirefoxProfile( std::string& profilePath );
	bool		readFirefoxPrefs		( const std::string& filename, const char* prefix );
	bool		getFirefoxProxySettings ();
	std::string getFirefoxSetting		(  const std::string& key, const std::string& def );
	void		setFireFoxProxyInfo		( ProxyType type, const std::string& typeKey, const std::string& typePortKey );

	//Windows IE and WinHttp support
	bool getWinHttpProxySettings();
	bool getIeLanProxySettings();

	//Helper
	bool isDefaultBrowserFirefox();

	//Main methods
	bool autoDetectProxyForUrl();

	//Log methods
	void	logDebug( const std::string& msg );

private:
	std::string		mHost;
	unsigned short	mPort;
	ProxyInfo*		mProxyInfo;

	Settings		mSettings;
	ILogger*		mLogger;
};