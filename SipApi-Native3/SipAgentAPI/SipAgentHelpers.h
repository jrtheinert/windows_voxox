
//Location for helper classes
//	- LibraryHelper

#pragma once

#include <Windows.h>	//For ::LoadLibrary

#include "DataTypes.h"	//For logging

/********************************* Local Utility Classes *****************************************/
namespace
{

class LibraryHelper
{
private:
//    RefCountHelper  d_refCount;

#ifdef _WINDOWS
    HMODULE d_library;
#else
    void*   d_library;
#endif

public:
    LibraryHelper()
    :   d_library(0)
    {}

    ~LibraryHelper()
    {
        LOG_DEBUG("Releasing the library");
#ifdef _WINDOWS
        if(d_library)
            ::FreeLibrary(d_library);
#else
        if(d_library)
            ::dlclose(d_library);
#endif
    }

    bool init(const char* libraryName)
    {
        LOG_DEBUG("Loading library: %s", libraryName);
#ifdef _WINDOWS
        d_library = ::LoadLibraryA(libraryName);
        if(d_library == 0)
            LOG_ERROR("Cannot load module %s.  Error: %u", libraryName, ::GetLastError());
#else
        d_library = ::dlopen(libraryName, RTLD_NOW | RTLD_LOCAL);
        if(d_library == 0)
            LOG_ERROR("Cannot load module %s.  Error: %s", libraryName, ::dlerror());
#endif
        return (d_library != 0);
    }

    template <class T>
    T getSymbol(const char* name)
    {
        if(d_library == 0)
        {
            LOG_ERROR("Library Helper uninitizlized. Cannot get symbol %s", name);
            return 0;
        }
#ifdef _WINDOWS
        T result = reinterpret_cast<T>(::GetProcAddress(d_library, name));
        if(result == 0)
            LOG_ERROR("Cannot load symbol %s Error: %u", name, ::GetLastError());
#else
        T result = reinterpret_cast<T>(::dlsym(d_library, name));
        if(result == 0)
            LOG_ERROR("Cannot load symbol %s Error: %s", name, ::dlerror());
#endif
        return result;
    }
};

}	//namespace
