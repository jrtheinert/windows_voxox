#ifndef TELURIX_SIPAGENT_ISIPAGENT_H
#define TELURIX_SIPAGENT_ISIPAGENT_H

class CallStateInfo;
class SipConfig;

#include <string>

/**
 * @brief Interface of ISipAgent, a SIP User Agent Library
 *
 * @author Roman Shpount
 * @copyright &copy; 2011-2015 Telurix LLC
 */
namespace ISipAgent
{
/**
 * @brief Encryption Enumerator
 *
 * Specifies if SRTP should be used for call media.
 */
enum Encryption
{
    ENCRYPTION_NONE = 0,    ///< SRTP should not be used.
    ENCRYPTION_OPTIONAL,    ///< SRTP may be used if supported by both parties.
    ENCRYPTION_REQUIRED     ///< SRTP must be used.
};

/**
 * @brief TLS Method Enumerator
 *
 * Specifies protocol methods supported by TLS/SSL connections
 */
enum TLSMethod
{
    TLS_SSLv23 = 0, ///< TLS/SSL connection will support SSLv2, SSLv3, and TLSv1 protocols
    TLS_SSLv2,      ///< TLS/SSL connection will only support SSLv2 protocols
    TLS_SSLv3,      ///< TLS/SSL connection will only support SSLv3 protocols
    TLS_TLSv1,      ///< TLS/SSL connection will only support TLSv1 protocols
    TLS_TLSv1_1,    ///< TLS/SSL connection will only support TLSv1.1 protocols
    TLS_TLSv1_2     ///< TLS/SSL connection will only support TLSv1.2 protocols
};

/**
 * @brief HTTP Proxy Enumerator
 *
 * Specifies HTTP Proxy type used to setup TCP connections
 */
enum HTTPProxy
{
    HTTPPROXY_NONE = 0, ///< HTTP Proxy is not used
    HTTPPROXY_AUTO,     ///< HTTP Proxy is automatically detected
    HTTPPROXY_SYSTEM,   ///< HTTP Proxy settings from system configuration are used
    HTTPPROXY_HTTP,     ///< HTTP Proxy
    HTTPPROXY_SOCKSv4,  ///< SOCKSv4 Proxy
    HTTPPROXY_SOCKSv5   ///< SOCKSv5 Proxy
};

/**
 * @brief Function Result Enumerator
 *
 * Specifies function result codes
 */
enum Result
{
    RESULT_OK = 0,          ///< Success Response
    RESULT_FAILURE,         ///< Unspecified Error
    RESULT_NOT_IMPLEMENTED, ///< Function or feature not implemented
    RESULT_OUT_OF_MEMORY,   ///< Out of memory
    RESULT_INVALID_ARGS,    ///< One or more arguments are invalid
    RESULT_BUFFER_OVERFLOW  ///< String buffer is too small
};

/**
 * @brief Topology Discovery Method Enumerator
 *
 * Specifies topology discovery method used for RTP/RTCP traffic
 */
enum Topology
{
    TOPOLOGY_NONE = 0,  ///< No topology discovery -- use local IP
    TOPOLOGY_ICE,       ///< Use ICE (RFC 5245)
    TOPOLOGY_STUN,      ///< Use STUN to determine local IP (RFC 3489/RFC 5780)
    TOPOLOGY_TURN,      ///< Use TURN to proxy all RTP/RTCP traffic (RFC 5766)
    TOPOLOGY_AUTO_TURN  ///< Use TURN to proxy all RTP/RTCP traffic (RFC 5766) if HTTP proxy is used
};

/**
 * @brief Topology Host Enumerator
 *
 * Specifies topology host type in TopologyHostList
 */
enum TopologyHost
{
    TOPOLGY_HOST_STUN = 0,  ///< UDP STUN Server (RFC 3489/RFC 5780)
    TOPOLGY_HOST_TURN_UDP,  ///< UDP TURN Server (RFC 5766)
    TOPOLGY_HOST_TURN_TCP,  ///< TCP TURN Server (RFC 5766)
    TOPOLGY_HOST_TURN_TLS   ///< TLS TURNS Server (RFC 5766)
};

/**
 * @brief Subscription/Registration State Enumerator
 *
 * Specifies Subscription/Registration state
 */
enum State
{
    STATE_PENDING,      ///< Subscription/Registration is currently being established (pending)
    STATE_ACTIVE,       ///< Subscription/Registration is currently active
    STATE_TERMINATING,  ///< Subscription/Registration is currently being closed (terminating)
    STATE_TERMINATED    ///< Subscription/Registration is closed (terminated)
};

/**
 * @brief Subscription/Registration Event Enumerator
 *
 * Specifies Subscription/Registration related event type
 */
enum Event
{
    EVENT_OK,           ///< Last operation succeeded
    EVENT_TERMINATED,   ///< Subscription/Registration terminated
    EVENT_AUTH_ERROR,   ///< Last operation failed due to authentication error
    EVENT_TIMEOUT,      ///< Last operation timed out
    EVENT_ERROR,        ///< Last operation failed due to unexpected error
    EVENT_EXPIRED       ///< Subscription/Registration expired
};

/**
 * @brief Call State Enumerator
 *
 * Specifies various states that can occur during the call
 */
enum CallState
{
    CALL_STATE_IDLE,            ///< Call was not placed yet
    CALL_STATE_INCOMING,        ///< Incoming call was just received
    CALL_STATE_ANSWERING,       ///< Incoming call with a provisional response sent
    CALL_STATE_DIALING,         ///< Outbound call where no responses were received
    CALL_STATE_INPROGRESS,      ///< Outbound call which received one or more 100 provisional responses
    CALL_STATE_RINGBACK,        ///< Outbound call which received one or more non 100 provisional response
    CALL_STATE_EARLYMEDIA,      ///< Outbound call which received one or more non 100 provisional response with media
    CALL_STATE_CONNECTED,       ///< Connected call which is not on hold
    CALL_STATE_HOLD,            ///< Connected call on hold
    CALL_STATE_REFER,           ///< Call transfer(refer) initiated by our side
    CALL_STATE_DISCONNECTED,    ///< Call was disconnected due to caller or callee hang up
    CALL_STATE_BUSY,            ///< Outbound call failed with status code 486
    CALL_STATE_NOANSWER,        ///< Outbound call failed due to timeout or status code 408
    CALL_STATE_REDIRECTED,      ///< Outbound call was redirected with 3XX status code
    CALL_STATE_ERROR            ///< Outbound call failed due to unexpected error
};

/**
 * @brief Refer (Transfer) State Enumerator
 *
 * Specifies various states that can occur when one client is referring another.
 */
enum ReferState
{
    REFER_STATE_REFERRING,  ///< The client is issuing the referral
    REFER_STATE_ACCEPTED,   ///< The recipient has accepted the referral
    REFER_STATE_ERROR,      ///< An error has occurred during referral
    REFER_STATE_REJECTED,   ///< The recipient has rejected the referral
    REFER_STATE_DROPPED,    ///< The recipient has dropped the referral
    REFER_STATE_DONE        ///< The referral is complete; the recipient has contacted the referred client
};

//-----------------------------------------------------------------------------
//Added by JRT for PJSIP re-work
//-----------------------------------------------------------------------------

class ITlsInfo
{
public:
	//TlsInfo();
	//virtual ~TlsInfo() {}

	//Gets ------------------------------------------------------------
	virtual ISipAgent::TLSMethod getMethod()				 const			= 0;
	virtual bool				 getVerifyCertificate()		 const			= 0;
	virtual unsigned int		 getVerifyDepth()			 const			= 0;
	virtual bool				 getRequireCertificate()	 const			= 0;
	virtual std::string			 getPrivateKey()			 const			= 0;
	virtual std::string			 getPrivateKeyPassword()	 const			= 0;
	virtual std::string			 getCertificateAuthorities() const			= 0;
	virtual std::string			 getCertificate()			 const			= 0;
	virtual std::string			 getCrl()					 const			= 0;
	virtual std::string			 getCiphers()				 const			= 0;
																		
	//Sets ------------------------------------------------------------		
	virtual void setMethod				  ( ISipAgent::TLSMethod val )		= 0;
	virtual void setVerifyCertificate	  ( bool                 val )		= 0;
	virtual void setVerifyDepth			  ( unsigned int         val )		= 0;
	virtual void setRequireCertificate	  ( bool                 val )		= 0;
	virtual void setPrivateKey			  ( const std::string&   val )		= 0;
	virtual void setPrivateKeyPassword	  ( const std::string&   val )		= 0;
	virtual void setCertificateAuthorities( const std::string&   val )		= 0;
	virtual void setCertificate			  ( const std::string&   val )		= 0;
	virtual void setCrl					  ( const std::string&   val )		= 0;
	virtual void setCiphers				  ( const std::string&   val )		= 0;
};

class IServerInfo
{
	//friend class ServerInfoList;

protected:
	enum Owner
	{
		Owner_None	= 0,
		Owner_User  = 1,
		Owner_App	= 2,
	};

public:
	enum Type
	{
		Type_None		  = 0,
		Type_SipRegistrar = 1,
		Type_SipProxy	  = 2,
		Type_Stun		  = 3,
		Type_TurnUdp	  = 4,
		Type_TurnTcp	  = 5,
		Type_TurnTls	  = 6,
		Type_HttpProxy	  = 7,
		Type_HttpTunnel	  = 8,
		Type_HttpsTunnel  = 9,
	};

//	IServerInfo();

//	virtual ~IServerInfo();

	//Gets ------------------------------------------------------------
	virtual Type		 getType()			const	= 0;
	virtual std::string	 getAddress()		const	= 0;
	virtual unsigned int getPort()			const	= 0;
	virtual std::string	 getUserId()		const	= 0;
	virtual std::string	 getPassword()		const	= 0;
	virtual bool		 useSsl()			const	= 0;

	virtual ISipAgent::HTTPProxy		getHttpProxyType()	const	= 0;
	virtual ISipAgent::TopologyHost		getTopologyHost()   const	= 0;

	virtual bool		isTypeNone()		 const	= 0;
	virtual bool		isTypeSipRegistrar() const	= 0;
	virtual bool		isTypeSipProxy()	 const	= 0;
	virtual bool		isTypeStun()		 const	= 0;
	virtual bool		isTypeTurnUdp()		 const	= 0;
	virtual bool		isTypeTurnTcp()		 const	= 0;
	virtual bool		isTypeTurnTls()		 const	= 0;
	virtual bool		isTypeHttpProxy()    const	= 0;
	virtual bool		isTypeHttpTunnel()   const	= 0;
	virtual bool		isTypeHttpsTunnel()  const	= 0;

	virtual bool		isTopologyHost()	 const	= 0;

	//Sets ------------------------------------------------------------
	virtual void setType		 ( Type                 val )	= 0;
	virtual void setAddress		 ( const std::string&	val )	= 0;
	virtual void setPort		 ( unsigned int			val )	= 0;
	virtual void setUserId		 ( const std::string&	val )	= 0;
	virtual void setPassword	 ( const std::string&	val )	= 0;
	virtual void setSsl			 ( bool					val )	= 0;
	virtual void setHttpProxyType( ISipAgent::HTTPProxy	val )	= 0;

	virtual void setTypeNone()		 							= 0;
	virtual void setTypeSipRegistrar()	 						= 0;
	virtual void setTypeSipProxy()		 						= 0;
	virtual void setTypeStun()		 							= 0;
	virtual void setTypeTurnUdp()		 						= 0;
	virtual void setTypeTurnTcp()		 						= 0;
	virtual void setTypeTurnTls()		 						= 0;
	virtual void setTypeHttpProxy() 							= 0;
	virtual void setTypeHttpTunnel()   							= 0;
	virtual void setTypeHttpsTunnel()   						= 0;

//	virtual void setType( const std::string& type )				= 0;

	//Helper methods
	virtual	std::string	getSipProtocol()	const = 0;
	virtual std::string	getUrlFormat()		const = 0;
	virtual std::string	getHostPortFormat()	const = 0;
};

//==============================================================================

class IServerInfoList
{
public:
	virtual IServerInfo*	getFirstSipRegistrar()	const	= 0;
	virtual IServerInfo*	getFirstSipProxy()		const	= 0;
	virtual IServerInfo*	getFirstStun()			const	= 0;
	virtual IServerInfo*	getFirstTurnUdp()		const	= 0;
	virtual IServerInfo*	getFirstTurnTcp()		const	= 0;
	virtual IServerInfo*	getFirstTurnTls()		const	= 0;
	virtual IServerInfo*	getFirstHttpProxy()		const	= 0;

	virtual bool			hasSipRegistrar()		const	= 0;
	virtual bool			hasSipProxy()			const	= 0;
	virtual bool			hasStun()				const	= 0;
	virtual bool			hasTurnUdp()			const	= 0;
	virtual bool			hasTurnTcp()			const	= 0;
	virtual bool			hasTurnTls()			const	= 0;
	virtual bool			hasHttpProxy()			const	= 0;

	virtual bool			hasTurn()				const	= 0;
	virtual bool			hasTurns()				const	= 0;

//	virtual void			getTopologyHostInfo( ISipAgent::TopologyHostList* hosts) = 0;
};

//==============================================================================

/**
 * @brief Callbacks for a SIP Call
 *
 * Defines a call back class that will receive call status notifications.
 */
class ICallCallbacks
{
public:
    ~ICallCallbacks() throw()
    {}
public:
    /** @name Callback Functions*/
    ///@{
    /**
     * Call state change notification.
     */
    virtual void stateChanged( const CallStateInfo& csi ) = 0;

    /**
     * Non-standard dialog SIP request (not an INVITE, BYE, UPDATE, REFER, or CANCEL) notification.
     *
     * If Request is not held or answered in this call back function, an automatic 404 response is generated.
     */
	virtual void handleInfoRequestReceived( int callId, const std::string& content, const std::string& contentType ) = 0;

    /**
     * Change in transfer state of the current call.
     * This function only provides status updates for transfers initiated by our side.
     */
    virtual void referState
    (
        ReferState  state,  ///< [in] New Refer state
        unsigned    status, ///< [in] Latest SIP status received by the transferred party from the transfer destination
        const char* reason  ///< [in] Latest SIP reason received by the transferred party from the transfer destination
    ) throw() = 0;
    //@}
};

/**
 * @brief SIP Call Interface
 *
 * Defines SIP Call interface with all the action methods.
 */
class Call
{
protected:
    ~Call() throw()
    {}
public:
    /** @name Call Methods*/
    ///@{
    /**
     * Disconnect the current call.
     */
    virtual void disconnect() throw() = 0;

    /**
     * Answer (send a response to) an incoming call.
     *
     * @return Operation result
     */
    virtual Result answer
    (
        unsigned    status,     ///< [in] Response status from 100 to 699. Only a single final response (>200) is supported.
        const char* reason,     ///< [in] Response reason. If empty or 0, send default reason for status code.
        bool        sendAudio,  ///< [in] Start sending audio on this call
        bool        recvAudio,  ///< [in] Start receiving audio on this call
        bool        sendVideo,  ///< [in] Start sending video on this call
        bool        recvVideo   ///< [in] Start receiving video on this call
    ) throw() = 0;

    /**
     * Change whether audio and video are sent or received on this call.
     *
     * @return Operation result
     */
    virtual Result hold
    (
        bool    sendAudio,  ///< [in] Send audio on this call
        bool    recvAudio,  ///< [in] Receive audio on this call
        bool    sendVideo,  ///< [in] Send video on this call
        bool    recvVideo   ///< [in] Receive video on this call
    ) throw() = 0;


	virtual Result mute
	(
		bool muteOn			///< [in] Mute On or Off
	) throw() = 0;
    /**
     * Initiate the call transfer.
     *
     * Transfer progress will be reported via ICallCallbacks::referState
     * @return Operation result
     */
    virtual Result transfer
    (
        const char* url ///< [in] Transfer URL
    ) throw() = 0;

    /**
     * Send a custom SIP request in the current call.
     *
     * @return Operation result
     */
    virtual Result sendRequest
    (
        const char*         method,         ///< [in] SIP method
        const char*         contentType,    ///< [in] Content mime type. Must be present if content is specified.  Ignored if no content is provided.
        const char*         content,        ///< [in] Content data. Must be provided if content size is specified
        unsigned            contentSize,    ///< [in] Content size. Must be provided if content data is specified
        unsigned            connectTimeout, ///< [in] Optional timeout until the first non-100 provisional response
        unsigned            timeout	        ///< [in] Optional timeout until the final response
    ) throw() = 0;

    /**
     * Send a single DTMF digit.
     *
     * @return Operation result
     */
    virtual Result sendDTMF
    (
        const char ch   ///< [in] A single DTMF digit. Allowed digits are [0-1],*,#,A,B,C,D
    ) throw() = 0;

    /**
     * Play a wave audio file.
     *
     * @return Operation result
     */
    virtual Result startPlayFile
    (
        const char* name,   ///< [in] File name
        bool        loop    ///< [in] Play the provided file in a loop
    ) throw() = 0;

    /**
     * Stop playing the audio file.
     *
     * @return Operation result
     */
    virtual Result stopPlayFile() throw() = 0;
    //@}

    /** @name Value Access Functions*/
    ///@{
    /**
     * Get a SIP URL that can be used to replace current call.
     *
     * Transferring  a connected call to a direct transfer URL of another call can be used to
     * transfer two connected calls to each, thus implementing consultation transfer.
     *
     * @return SIP URL which points to the current call contact with Replaces header filled in.
     */
    virtual std::string getDirectTransferURL() const throw() = 0;

    /**
     * Check if current call is using SIPS and SRTP (encrypted).
     *
     * @return True if call is using SIPS and SRTP. False otherwise.
     */
    virtual bool getIsEncrypted() const throw() = 0;

    /**
     * Get the the value of Call-ID header.
     *
     * @return Returns the value of Call-ID header
     */
    virtual std::string getCallId() const throw() = 0;
    //@}

    /**
     * Get the remote contact for this call based on SIP Contact Header.
     *
     * @return Returns a NameAddress with the remote contact information.
     */
    virtual const char* getRemoteContact() const throw() = 0;

    /**
     * Get the remote party for this call based on SIP From/To Header.
     *
     * @return Returns a NameAddress with the remote party information.
     */
    virtual const char* getRemoteParty() const throw() = 0;

    /**
     * Get the current call state.
     *
     * @return Returns current call state.
     */
    virtual CallState getState() const throw() = 0;
    //@}

    /** @name Media Related Functions*/
    ///@{
    /**
     * Mutes the microphone input signal completely without affecting the audio device volume.
     *
     * @return Operation result
     */
    virtual Result setInputMute
    (
        bool enable     ///< [in] True to enable input, false to disable
    ) throw() = 0;

    /**
     * Gets the current microphone input mute state.
     *
     * @return Operation result
     */
    virtual Result getInputMute
    (
        bool* enabled   ///< [out] True means input is enabled, false -- disabled
    )  throw() = 0;

    /**
     * Gets the speaker speech |level|, mapped linearly to the range [0,32768].
     *
     * @return Operation result
     */
    virtual Result getSpeechOutputLevelFullRange
    (
        unsigned int* level ///< [out] Speech speaker level
    ) throw() = 0;
};

/**
 * @brief Line Callbacks
 *
 * Defines callbacks to monitor line status and notify about incoming calls, subscriptions,
 * and other SIP requests.
 */
class ILineCallbacks
{
protected:
    ~ILineCallbacks() throw()
    {}
public:
    /** @name Callback Functions*/
    ///@{
    /**
     * New incoming call is received on this line.
     *
     * @return Call status callbacks. Returned object should be addRefed
     */
    virtual ICallCallbacks* newCall
    (
        Call* call  ///< [in] Incoming call
    ) throw() = 0;
  
	virtual ICallCallbacks* newCallCallback( int callId ) throw() = 0;

	
	virtual void handleInfoRequestReceived( int callId, const std::string& content, const std::string& contentType ) = 0;

    /**
     * Line registration state change.
     */
    virtual void registrationChanged
    (
        State				state,      ///< [in] New line registration change
        Event				event,      ///< [in] Event that caused the state change
        unsigned			status,     ///< [in] SIP Status Code for the message that caused this change.
										///< 0 if change not caused by SIP Response.
        const std::string&	reason      ///< [in] SIP Reason for the message that caused this change.
										///< 0 if change not caused by SIP Response.
    ) throw() = 0;
    //@}
};

/**
 * @brief Line Interface
 *
 * Defines interface for a SIP Stack instance -- SIP Line.
 */
class Line
{
protected:
    ~Line() throw()
    {}

public:

	virtual Result configure( SipConfig* config ) = 0;

    /** @name Line Settings Methods*/
    ///@{

    /**
     * Set Local SIP network interface.
     *
     * @return Operation result
     */
    virtual Result setNetworkInterface
    (
        const char*     host,   ///< [in] Local IP address.  If set to 0 or an empty string, stack will guess the local address
        unsigned short  port    ///< [in] Local port. If set to 0, ephemeral port is used.
    ) throw() = 0;


    /** @name Line Operations*/
    ///@{
    /**
     * Start the Line SIP Stack based on configured settings.
     *
     * @return Operation result
     */
    virtual Result connect( int level = -1 ) throw() = 0;

    /**
     * Register SIP Line based on configured settings.
     *
     * @return Operation result
     */
    virtual Result registerLine() throw() = 0;

    /**
     * Unregister SIP Line and shutdown the SIP Stack.
     *
     * @return Operation result
     */
    virtual void disconnect
    (
        bool skipUnregister ///< [in] If true shutdown the line without sending an Un-Register SIP message
    ) throw() = 0;

    /**
     * Create an outbound call
     *
     * @return Operation result
     */
    virtual Result createCall
    (
        Call**          call,       ///< [out] Addrefed created call
        ICallCallbacks* callbacks,  ///< [in] Call progress callbacks
        const char*     url,        ///< [in] Destination URL
        bool            sendAudio,  ///< [in] Send Audio on this call
        bool            recvAudio,  ///< [in] Receive Audio on this call
        bool            sendVideo,  ///< [in] Send Video on this call
        bool            recvVideo//,  ///< [in] Receive Video on this call
//        HeaderList*     headers     ///< [in] Optional headers to add to INVITE message
    ) throw() = 0;

    /**
     * Send a custom SIP request in the current call.
     *
     * @return Operation result
     */
    virtual Result sendRequest
    (
        const char*         url,            ///< [in] Destination URL
        const char*         method,         ///< [in] SIP method
        const char*         contentType,    ///< [in] Content mime type. Must be present if content is specified.
                                            ///< Ignored if no content is provided.
        const char*         content,        ///< [in] Content data. Must be provided if content size is specified
        unsigned            contentSize,    ///< [in] Content size. Must be provided if content data is specified
        unsigned            connectTimeout, ///< [in] Optional timeout until the first non-100 provisional response
        unsigned            timeout         ///< [in] Optional timeout until the final response
    ) throw() = 0;
    //@}
};

/**
 * @brief Agent Callbacks
 *
 * Defines callbacks used for message logging.
 */
class IAgentCallbacks
{
protected:
    ~IAgentCallbacks() throw()
    {}
public:
    /** @name Callback Functions*/
    ///@{
    /**
     * Verbose message.
     */
    virtual void verbose( const std::string& msg ) throw() = 0;

    /** @name Callback Functions*/
    ///@{
    /**
     * Debug message.
     */
    virtual void debug( const std::string& msg ) throw() = 0;

    /**
     * Informational message.
     */
    virtual void info( const std::string& msg ) throw() = 0;

    /**
     * Warning message.
     */
    virtual void warning( const std::string& msg ) throw() = 0;

    /**
     * Error message.
     */
    virtual void error( const std::string& msg ) throw() = 0;

    /**
     * Failure message.		//JRT - This was added because PJSIP reports some Errors as Failures
     */
    virtual void failure( const std::string& msg ) throw() = 0;

    /**
     * Critical failure had occured
     */
    virtual void failure() throw() = 0;
};

/**
 * @brief Agent Interface
 *
 * Defines interface for managing global media resources and creating SIP Lines.
 */
class Agent
{
public:
    virtual ~Agent() throw()		{}

	/** @name System methods*/
    ///@{
    /**
     * Shotdowm the agent and wait for shutdown to complete.
     */
    virtual void shutdown() throw() = 0;

    //@}

    /**
     * Set noise suppression
     *
     * @return Operation result
     */
    virtual Result setNS
    (
        bool enable //< [in] Enable/disable
    ) throw() = 0;

    /**
     * Set experimental noise suppression
     *
     * @return Operation result
     */
    virtual Result setExperimentalNS
    (
        bool enable //< [in] Enable/disable
    ) throw() = 0;

    /**
     * Set Automatic Gain Control
     *
     * @return Operation result
     */
    virtual Result setAGC
    (
        bool enable //< [in] Enable/disable
    ) throw() = 0;

    /**
     * Set Experimental Automatic Gain Control
     *
     * @return Operation result
     */
    virtual Result setExperimentalAGC
    (
        bool enable //< [in] Enable/disable
    ) throw() = 0;

    /**
     * Set Echo Canceller
     *
     * @return Operation result
     */
    virtual Result setEC
    (
        bool enable //< [in] Enable/disable
    ) throw() = 0;

    /**
     * Set Delay Correction
     *
     * @return Operation result
     */
    virtual Result setDelayCorrection
    (
        bool enable //< [in] Enable/disable
    ) throw() = 0;

	/**
     * Set High Pass Filter
     *
     * @return Operation result
     */
    virtual Result setHighPassFilter
    (
        bool enable //< [in] Enable/disable
    ) throw() = 0;

    /**
     * Select call recording device
     *
     * @return Operation result
     */
    virtual Result setRecordingDevice
    (
        const std::string& guid   //< [in] Device guid
    ) throw() = 0;

    /**
     * Select call playout device
     *
     * @return Operation result
     */
    virtual Result setPlayoutDevice
    (
        const std::string& guid   //< [in] Device guid
    ) throw() = 0;

    /**
     * Select notification device
     *
     * @return Operation result
     */
    virtual Result setRingingDevice
    (
        const std::string& guid   //< [in] Device guid
    ) throw() = 0;

    /**
     * Sets the speaker |volume| level. Valid range is [0,255].
     *
     * @return Operation result
     */
    virtual Result setSpeakerVolume
    (
        unsigned int volume //< [in] Volume level
    ) throw() = 0;

    /**
     * Gets the speaker |volume| level.
     *
     * @return Operation result
     */
    virtual Result getSpeakerVolume
    (
        unsigned int* volume    //< [out] Volume level
    ) throw() = 0;


    /**
     * Sets the ringing |volume| level. Valid range is [0,255].
     *
     * @return Operation result
     */
    virtual Result setRingingVolume
    (
        unsigned int volume //< [in] Volume
    ) throw() = 0;

    /**
     * Gets the ringing |volume| level.
     *
     * @return Operation result
     */
    virtual Result getRingingVolume
    (
        unsigned int* volume    //< [out] Volume
    ) throw() = 0;

    /**
     * Plays the audio file over ringing device.
     *
     * @return Operation result
     */
    virtual Result startPlayRingingFile
    (
        const char* name,   ///< [in] File name
        bool        loop    ///< [in] Play the provided file in a loop
    ) throw() = 0;

    /**
     * Stops playback of the audio file over ringing device.
     *
     * @return Operation result
     */
    virtual Result stopPlayRingingFile() throw() = 0;

    /**
     * Sets the microphone volume level. Valid range is [0,255].
     *
     * @return Operation result
     */
    virtual Result setMicVolume
    (
        unsigned int volume //< [in] Volume
    ) throw() = 0;

    /**
     * Gets the microphone volume level.
     *
     * @return Operation result
     */
    virtual Result getMicVolume
    (
        unsigned int* volume    //< [out] Volume
    ) throw() = 0;

    /**
     * Gets the microphone speech |level|, mapped linearly to the range [0,32768].
     *
     * @return Operation result
     */
    virtual Result getSpeechInputLevelFullRange
    (
        unsigned int* level //< [out] Speech input level
    ) throw() = 0;
    //@}

    /** @name Object Creation Functions*/
    ///@{
    /**
     * Create new SIP Line
     *
     * @return lineId
     */
    virtual int createLine
    (
        ILineCallbacks*  callbacks   //< [in] Line callbacks
    ) throw() = 0;
    //@}

	//Logging methods added by JRT - 2015.06.11
	//This was added because PJSIP reports some Errors as Failures.
    virtual void logFailure( const std::string& msg ) throw() = 0;		
    virtual void logError  ( const std::string& msg ) throw() = 0;
    virtual void logWarning( const std::string& msg ) throw() = 0;
    virtual void logInfo   ( const std::string& msg ) throw() = 0;
    virtual void logDebug  ( const std::string& msg ) throw() = 0;
    virtual void logVerbose( const std::string& msg ) throw() = 0;

    virtual void logFailure() throw() = 0;

	virtual Result configure( SipConfig* config ) = 0;

	//-----------------------------------------------------------------------------
	//Line/Account related methods (so API doesn't have to handle it)
	//-----------------------------------------------------------------------------
	virtual int  makeCall( int lineId, ISipAgent::ICallCallbacks* callbacks, const std::string& uri, bool enableVideo ) = 0;

	virtual bool hasLineId   ( int lineId ) = 0;
	virtual bool connectLine ( int lineId ) = 0;
	virtual bool populateLine( int lineId ) = 0;
	virtual bool registerLine( int lineId ) = 0;
	virtual bool removeLine  ( int lineId, bool force ) = 0;

	virtual void clearLines()	 = 0;
	virtual int  getLineCount()  = 0;

	//-----------------------------------------------------------------------------
	//Call related methods (so API doesn't have to handle it)
	//-----------------------------------------------------------------------------
	virtual void		sendRingingNotification( int callId, bool enableVideo ) = 0;
	virtual void		acceptCall			   ( int callId, bool enableVideo ) = 0;
	virtual void		rejectCall			   ( int callId ) = 0;
	virtual void		closeCall			   ( int callId ) = 0;
	virtual void		holdCall			   ( int callId ) = 0;
	virtual void		resumeCall			   ( int callId ) = 0;
	virtual void		muteCall			   ( int callId, bool muteOn ) = 0;
	virtual void		setCallInputVolume	   ( int callId, unsigned int volume	 ) = 0;
	virtual void		blindTransferCall	   ( int callId, const std::string& uri  ) = 0;
	virtual void		playDtmfOnCall		   ( int callId, const std::string& dtmf ) = 0;
	virtual void		playSoundFileOnCall	   ( int callId, const std::string& file ) = 0;
	virtual bool		isCallEncrypted		   ( int callId ) = 0;
	virtual std::string getSipCallId		   ( int callId ) = 0;
};

/**
 * Type of the "sipAgentCreate" function exported by SipAgent shared library
 *
 * This function is used to create an instance of SIP Agent
 *
 * @return Function Result
 */
typedef Result (*CreateFunction)
(
    Agent**          agent,      ///< [out] AddReffed pointer to created SIP Agent
    IAgentCallbacks* callbacks,  ///< [in] SIP Agent callbacks
	SipConfig*		 sipConfig	 ///< [in] SipConfig object
);

} // ISipAgent



#endif // TELURIX_SIPAGENT_ISIPAGENT_H
