/*****************************************************************************************************/
/*                                                                                                   */
/* InetUtil.cpp                                                                                      */
/* Utilities to work with inet address and number format conversion.                                 */
/* (c) 2005-2011 Telurix LLC.                                                                        */
/*                                                                                                   */
/*****************************************************************************************************/
#include <stdio.h>
//#include "CharType.h"
#include "InetUtil.h"
#include "StringUtil.h"

//JRT
//#include <ws2def.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdio.h>

unsigned long hostNameToNetPrivate(const char* hostName, bool* valid) throw();	//Declare

//End JRT

namespace InetUtil
{

char *netToAddress(unsigned long in, char* buff) throw()
{
    unsigned char* p = (unsigned char*)&in;
    (void) sprintf(buff, "%u.%u.%u.%u", (unsigned)(p[0]), (unsigned)(p[1]), (unsigned)(p[2]), (unsigned)(p[3]));
    return (buff);
}

// Internet address interpretation routine.
// The value returned is in network order.
unsigned long addressToNet(const char* cp, bool* valid) throw()
{
    unsigned long val;
    unsigned long base;
    unsigned long n;
    unsigned char c;
    unsigned long parts[4];
    unsigned long *pp = parts;
    if(valid)
        *valid = false;

    if (cp == 0 || *cp == '\0')
        return ((unsigned long)-1);
again:
    // Collect number up to ``.''.
    // Values are specified as for C:
    // 0x=hex, 0=octal, other=decimal.
    val = 0;
    base = 10;
    if (*cp == '0')
    {
        if (*++cp == 'x' || *cp == 'X')
        {
            base = 16;
            cp++;
        }
        else
            base = 8;
    }

    while ((c = (unsigned char)*cp) != 0)
    {
        if (StringUtil::isDigit(c))
        {
            if (c >= '0' + base)
                break;
            val = (val * base) + (c - '0');
            cp++;
            continue;
        }
        if (base == 16 && StringUtil::isHexDigit(c))
        {
            val = (val << 4) + (c + 10 - (StringUtil::isLower(c) ? 'a' : 'A'));
            cp++;
            continue;
        }
        break;
    }

    if (*cp == '.')
    {
        // Internet format:
        //    a.b.c.d
        //    a.b.c    (with c treated as 16-bits)
        //    a.b    (with b treated as 24 bits)
        if ((pp >= parts + 3) || (val > 0xff))
            return ((unsigned long)-1);
        *pp++ = val;
        cp++;
        goto again;
    }

    // Check for trailing characters.
    if (*cp && !StringUtil::isSpace(*cp))
        return ((unsigned long)-1);
    *pp++ = val;

    // Concoct the address according to the number of parts specified.
    n = pp - parts;
    switch (n)
    {
        case 1:
        {
            // a -- 32 bits
            val = parts[0];
            break;
        }
        case 2:
        {
            // a.b -- 8.24 bits
            if (parts[1] > 0xffffff)
                return ((unsigned long)-1);
            val = (parts[0] << 24) | (parts[1] & 0xffffff);
            break;
        }
        case 3:
        {
            // a.b.c -- 8.8.16 bits
            if (parts[2] > 0xffff)
                return ((unsigned long)-1);
            val = (parts[0] << 24) | ((parts[1] & 0xff) << 16) | (parts[2] & 0xffff);
            break;
        }
        case 4:
        {
            // a.b.c.d -- 8.8.8.8 bits
            if (parts[3] > 0xff)
                return ((unsigned long)-1);
            val = (parts[0] << 24) | ((parts[1] & 0xff) << 16) | ((parts[2] & 0xff) << 8) | (parts[3] & 0xff);
            break;
        }
        default:
            return ((unsigned long)-1);
    }

    if(valid)
        *valid = true;
    return hostToNet(val);
}

unsigned long hostNameToNet(const char* hostName, bool* valid) throw()
{
	return hostNameToNetPrivate( hostName, valid );
}

}	//namespace InetUtil

unsigned long hostNameToNetPrivate(const char* hostName, bool* valid) throw()
{
	unsigned long result = 0xffffffff;

	int sockfd;  
	struct addrinfo  hints;
	struct addrinfo* servinfo;
	struct addrinfo* p;
	int rv;

	memset(&hints, 0, sizeof hints);
	hints.ai_family   = AF_UNSPEC; // use AF_INET6 to force IPv6
	hints.ai_socktype = SOCK_STREAM;

	if ((rv = ::getaddrinfo( hostName, "http", &hints, &servinfo)) != 0) 
	{
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		return result;
	}

	// loop through all the results and connect to the first we can
	for( p = servinfo; p != NULL; p = p->ai_next ) 
	{
		if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) 
		{
			perror("socket");
			continue;
		}

		sockaddr* tempAddr = p->ai_addr;

		if ( p->ai_family == AF_INET)
		{
			IN_ADDR* temp = &(((struct sockaddr_in*)tempAddr)->sin_addr);
			result = (unsigned long)temp->S_un.S_addr;
		}
		else
		{
			IN6_ADDR* temp = &(((struct sockaddr_in6*)tempAddr)->sin6_addr);
			result = (unsigned long)temp->u.Word;	//TODO: Not tested.
		}

		break; // if we get here, we must have connected successfully
	}

	//if (p == NULL) 
	//{
	//	// looped off the end of the list with no connection
	//	fprintf(stderr, "failed to connect\n");
	//	exit(2);
	//}

	freeaddrinfo(servinfo); // all done with this structure
	return result;
}

