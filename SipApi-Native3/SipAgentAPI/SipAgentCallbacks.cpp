
#include "SipAgentCallbacks.h"
#include "SipAgentApiWrapper.h"

namespace VxCallback
{

AgentCallbacks::AgentCallbacks()
{
}

void AgentCallbacks::verbose( const std::string& message ) throw()
{
	LOG_VERBOSE( message );
}

void AgentCallbacks::debug( const std::string& message ) throw()
{
	LOG_DEBUG(  message );
}

void AgentCallbacks::info( const std::string& message ) throw()
{
	LOG_INFO( message );
}

void AgentCallbacks::warning( const std::string& message ) throw()
{
	LOG_WARN( message );
}

void AgentCallbacks::error( const std::string& message ) throw()
{
	LOG_ERROR( message );
}

void AgentCallbacks::failure( const std::string& message ) throw()
{
	LOG_FATAL( message );
}

void AgentCallbacks::failure() throw()
{
	LOG_FATAL( "Sip Agent Fatal Error" );
}

//=============================================================================


//=============================================================================

LineCallbacks::LineCallbacks( SipAgentApiWrapper* wrapper, int lineId )
	:   d_wrapper(wrapper)
	,   d_lineId(lineId)
{
}

void LineCallbacks::setLineId( int val )
{
	d_lineId = val;
}
    
ISipAgent::ICallCallbacks* LineCallbacks::newCall(ISipAgent::Call* call) throw()
{
	return d_wrapper->newCall( d_lineId, call );
}

ISipAgent::ICallCallbacks* LineCallbacks::newCallCallback( int callId ) throw()
{
	return d_wrapper->newCallCallback( callId );
}

void LineCallbacks::handleInfoRequestReceived( int callId, const std::string& content, const std::string& contentType )
{
	d_wrapper->infoRequestReceivedEvent( callId, content, contentType );
}

void LineCallbacks::registrationChanged( ISipAgent::State state, ISipAgent::Event event, unsigned status, const std::string& reason ) throw()
{
	d_wrapper->registrationChanged( d_lineId, state, event );
}

//=============================================================================


//=============================================================================

CallCallbacks::CallCallbacks( SipAgentApiWrapper* wrapper, int callId )
	:   d_wrapper(wrapper)
	,   d_callId(callId)
	,   d_hold(false)
	,   d_disconnected(false)
	,   d_ringback(false)
{
}

void CallCallbacks::setCallId( int val )
{
	d_callId = val;
}

void CallCallbacks::stateChanged( const CallStateInfo& csi ) throw()
{
    if(d_disconnected)
        return;

    std::string remotePartyURI      = csi.getRemoteParty();
    std::string remoteContactURI    = csi.getRemoteContact();
	int			status				= csi.getStatus();

    switch ( csi.getCallState() )
    {
        case ISipAgent::CALL_STATE_IDLE:
            // nothing
            break;

        case ISipAgent::CALL_STATE_INCOMING:
			sendEvent( EnumPhoneCallState::PhoneCallStateIncoming, remotePartyURI, status );
            break;
 
		case ISipAgent::CALL_STATE_ANSWERING:
            // nothing
            break;

		case ISipAgent::CALL_STATE_DIALING:
			sendEvent( EnumPhoneCallState::PhoneCallStateDialing, remotePartyURI, status );
            break;

		case ISipAgent::CALL_STATE_INPROGRESS:
			//do nothing.  There is no matching State and using Dialing breaks Eavesdrop in main app.
			//FWIW, this *may* be equivalent to ANSWERING for an incoming call.
            break;

        case ISipAgent::CALL_STATE_RINGBACK:
            if ( !d_ringback )
			{
                d_ringback = true;
				sendEvent( EnumPhoneCallState::PhoneCallStateRingingStart, remotePartyURI, status );
            }
            break;

		case ISipAgent::CALL_STATE_EARLYMEDIA:
            if ( d_ringback )
            {
                d_ringback = false;
 				sendEvent( EnumPhoneCallState::PhoneCallStateRingingStop, remotePartyURI, status );
            }
            break;

		case ISipAgent::CALL_STATE_CONNECTED:
            if ( d_ringback )
            {
                d_ringback = false;
				sendEvent( EnumPhoneCallState::PhoneCallStateRingingStop, remotePartyURI, status );
            }

            if(d_hold)
            {
                d_hold = false;
				sendEvent( EnumPhoneCallState::PhoneCallStateResumed, remotePartyURI, status );
            }
            else
            {
				sendEvent( EnumPhoneCallState::PhoneCallStateTalking, remotePartyURI, status );
            }
            break;

		case ISipAgent::CALL_STATE_HOLD:
            if ( d_ringback )
            {
                d_ringback = false;
				sendEvent( EnumPhoneCallState::PhoneCallStateRingingStop, remotePartyURI, status );
            }

            d_hold = true;
			sendEvent( EnumPhoneCallState::PhoneCallStateHold, remotePartyURI, status );
            break;

		case ISipAgent::CALL_STATE_REFER:
            break;

        case ISipAgent::CALL_STATE_DISCONNECTED:
        case ISipAgent::CALL_STATE_BUSY:
        case ISipAgent::CALL_STATE_NOANSWER:
			sendEvent( EnumPhoneCallState::PhoneCallStateClosed, remotePartyURI, status );
            d_disconnected = true;
            break;

		case ISipAgent::CALL_STATE_REDIRECTED:
			sendEvent( EnumPhoneCallState::PhoneCallStateRedirected, remoteContactURI, status );	//NOTE: This is the only use of 'remoteContactURI'.
            d_disconnected = true;
            break;

		case ISipAgent::CALL_STATE_ERROR:
			sendEvent( EnumPhoneCallState::PhoneCallStateError, remotePartyURI, status );
            d_disconnected = true;
            break;

		default:
            break;
    }	//switch


    if ( d_disconnected )
    {
        if(d_ringback)
        {
            d_ringback = false;
			sendEvent( EnumPhoneCallState::PhoneCallStateRingingStop, remotePartyURI, status );
        }

        d_wrapper->closeCall(d_callId);
    }
}

void CallCallbacks::sendEvent( EnumPhoneCallState::PhoneCallState callState, const std::string& remoteUri, int statusCode )
{
	d_wrapper->phoneCallStateChangedEvent( d_callId, callState, remoteUri, statusCode );
}

//This method expects the SIP stack to parse the INFO stanza and provide needed data.
void CallCallbacks::handleInfoRequestReceived( int callId, const std::string& content, const std::string& contentType )
{
	d_wrapper->infoRequestReceivedEvent( callId, content, contentType );
}

void CallCallbacks::referState( ISipAgent::ReferState, unsigned , const char* ) throw()
{
	// does nothing in the original, so ignore
}


//=============================================================================

};	//namespace VxCallback