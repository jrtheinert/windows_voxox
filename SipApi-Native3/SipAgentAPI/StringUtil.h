/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Sep 2014
 */

#pragma once

#include <string>
#include <list>

//=============================================================================

class StringUtil
{
public:
	//String conversions and helpers
	static bool			beginsWith( const std::string& text, const std::string& str, bool caseSensitive ) ;
	static bool			endsWith  ( const std::string& text, const std::string& str, bool caseSensitive ) ;
	static bool			contains  ( const std::string& text, const std::string& str, bool caseSensitive);
	static std::string	replace   ( const std::string& text, const std::string& before, const std::string& after, bool caseSensitive );
	static std::string	trim      ( const std::string& text );

	static std::string	toLowerCase ( const std::string& val );
	static std::string	toUpperCase ( const std::string& val );

	static std::string	toUtf8( const wchar_t* wide );

	static std::string	boolToString( bool val );
	static bool			stringToBool( const std::string& val );

	static bool			areEqualNoCase( const std::string& left, const std::string& right );

//	static bool			iStrCmp( const char* left, const char* right );					//Carry over from Roman's lib for now.
//	static bool			iStrCmp( const char* left, const char* right, size_t count );	//Carry over from Roman's lib for now.

	//Char-based methods
	static bool isAlpha		  ( char ch );
	static bool isUpper		  ( char ch );
	static bool isLower		  ( char ch );
	static bool isDigit		  ( char ch );
	static bool isHexDigit	  ( char ch );
	static bool isSpace		  ( char ch );
	static bool isAlphaNumeric( char ch );
	static bool isASCII		  ( char ch );
	static char toLower		  ( char ch );
	static char toUpper		  ( char ch );

};

//=============================================================================
