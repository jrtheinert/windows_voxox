
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Sep 2014
 */

#include "StringUtil.h"

#include <algorithm>	//transform(),
#include <cctype>
#include <sstream>

#include <Windows.h>	//For WideCharToMultiByte()

//=============================================================================
//static
bool StringUtil::beginsWith( const std::string& text, const std::string& str, bool caseSensitive ) 
{
	bool		result = false;
	std::string tmp    = text;
	std::string str2   = str;

	if (!caseSensitive) 
	{
		//Converts tmp + str2 to lower case
		tmp  = StringUtil::toLowerCase(tmp);
		str2 = StringUtil::toLowerCase(str2);
	}

	return ( tmp.find(str2) == 0 );
}

//static
bool StringUtil::endsWith( const std::string& text, const std::string& str, bool caseSensitive ) 
{
	bool	result = false;
	size_t	startPos = text.size() - str.size();

	if ( startPos >= 0 )
	{
		std::string tmp    = text;
		std::string str2   = str;

		if (!caseSensitive) 
		{
			//Converts tmp + str2 to lower case
			tmp  = StringUtil::toLowerCase(tmp);
			str2 = StringUtil::toLowerCase(str2);
		}

		return ( tmp.find( str2, startPos ) == 0 );
	}

	return result;
}

//static
bool StringUtil::contains( const std::string& text, const std::string& str, bool caseSensitive)
{
	bool		result = false;
	std::string tmp    = text;
	std::string str2   = str;

	if (!caseSensitive) 
	{
		//Converts tmp + str2 to lower case
		tmp  = StringUtil::toLowerCase(tmp);
		str2 = StringUtil::toLowerCase(str2);
	}

	result = (tmp.find(str2, 0) != std::string::npos) ;

	return result;
}
	
std::string StringUtil::replace( const std::string& text, const std::string& before, const std::string& after, bool caseSensitive )
{
	//Copy this + before to tmp + before2
	std::string result  = text;
	std::string temp    = text;
	std::string before2 = before;

	if (!caseSensitive) 
	{
		//Converts tmp + before2 to lower case
		temp    = StringUtil::toLowerCase( temp    );
		before2 = StringUtil::toLowerCase( before2 );
	}

	//Searches on tmp + before2 rather than this + before
	std::string::size_type pos = 0;

	while ((pos = temp.find(before2, pos)) != std::string::npos) 
	{
		//Replaces in 'result' based on position found in 'temp'.
		result.replace(pos, before2.length(), after);
		temp.replace  (pos, before2.length(), after);	//Replace in temp, so we don't find() it again.
		pos = pos + after.length();
	}

	return result;
}

//static 
std::string StringUtil::toLowerCase( const std::string& val )
{
	std::string result = val;
	std::transform( result.begin(), result.end(), result.begin(), (int(*)(int)) tolower);
	return result;
}

std::string StringUtil::toUpperCase( const std::string& text )
{
	std::string result = text;
	std::transform( result.begin(), result.end(), result.begin(), (int(*)(int)) toupper);
	return result;
}

//static
std::string StringUtil::boolToString( bool val )
{
	std::stringstream converter;
	converter << val;
	return converter.str();
}

//static
bool StringUtil::stringToBool( const std::string& val )
{
	return std::stoi( val ) != 0;
}

//static 
bool StringUtil::areEqualNoCase( const std::string& left, const std::string& right )
{
	return ( toUpperCase( left ) == toUpperCase( right ) );
}

//static
std::string StringUtil::trim( const std::string& s )
{
	auto wsfront = std::find_if_not( s.begin(),  s.end(),  [](int c) {return std::isspace(c);} );
	auto wsback  = std::find_if_not( s.rbegin(), s.rend(), [](int c) {return std::isspace(c);} ).base();

	return (wsback<=wsfront ? std::string() : std::string(wsfront,wsback));
}


//static
//bool StringUtil::iStrCmp( const char* left, const char* right )
//{
//    int leftChar;
//    int rightChar;
//    do
//    {
//        leftChar = *(left++);
//        if( leftChar >= 'A' && leftChar <= 'Z')
//            leftChar -= ('A' - 'a');
//        rightChar = *(right++);
//        if( rightChar >= 'A' && rightChar <= 'Z')
//            rightChar -= ('A' - 'a');
//    } while (leftChar && leftChar == rightChar);
//
//    return(leftChar - rightChar);
//}
//
////static
//bool StringUtil::iStrCmp( const char* left, const char* right, size_t count )
//{
//    if ( count )
//    {
//        int leftChar;
//        int rightChar;
//
//        do
//        {
//            leftChar = *(left++);
//            if( leftChar >= 'A' && leftChar <= 'Z')
//                leftChar -= ('A' - 'a');
//            rightChar = *(right++);
//            if( rightChar >= 'A' && rightChar <= 'Z')
//                rightChar -= ('A' - 'a');
//        } while (--count && leftChar && leftChar == rightChar);
//
//        return ( leftChar - rightChar ) == 0;
//    }
//
//    return true;
//}


std::string StringUtil::toUtf8(const wchar_t* wide)
{
	std::string result;

    int len16 = static_cast<int>( wcslen(wide) );
    int len8  = ::WideCharToMultiByte( CP_UTF8, 0, wide, len16, NULL, 0, NULL, NULL );

	CHAR* ns = new CHAR[len8+1];
    
	if ( ns != nullptr )
	{
        ::WideCharToMultiByte( CP_UTF8, 0, wide, len16, ns, len8, NULL, NULL );
		result = std::string( ns, len8 );
	}
    
	return result;
}

//=============================================================================
//Character-based methods
//=============================================================================

//static
bool StringUtil::isAlpha(char ch)
{
    return (('a' <= ch && ch <= 'z') ||
            ('A' <= ch && ch <= 'Z'));
}

//static
bool StringUtil::isUpper(char ch) throw()
{
    return ('A' <= ch && ch <= 'Z');
}

//static
bool StringUtil::isLower(char ch) throw()
{
    return ('a' <= ch && ch <= 'z');
}

//static
bool StringUtil::isDigit(char ch) throw()
{
    return ('0' <= ch && ch <= '9');
}

//static
bool StringUtil::isHexDigit(char ch) throw()
{
    return (('0' <= ch && ch <= '9') ||
            ('a' <= ch && ch <= 'f') ||
            ('A' <= ch && ch <= 'F'));
}

//static
bool StringUtil::isSpace(char ch) throw()
{
    return ( ch == ' ' || ch == '\n' || ch == '\r'  || ch == '\t' || ch == '\v' || ch == '\f');
}

//static
bool StringUtil::isAlphaNumeric(char ch) throw()
{
    return (('0' <= ch && ch <= '9') ||
            ('a' <= ch && ch <= 'z') ||
            ('A' <= ch && ch <= 'Z'));
}

//static
bool StringUtil::isASCII(char ch) throw()
{
     return ((unsigned char)ch < 0x80);
}

//static
char StringUtil::toLower (char ch) throw()
{
    if( ch >= 'A' && ch <= 'Z')
        ch -= ('A' - 'a');
    return ch;
}

//static
char StringUtil::toUpper (char ch) throw()
{
    if ( (ch >= 'a') && (ch <= 'z') )
        ch -= ('a' - 'A');
    return ch;
}
