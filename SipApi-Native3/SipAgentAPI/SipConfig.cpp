
#include "SipConfig.h"
#include "SipAgentUtils.h"

SipConfig::SipConfig()
	:  	d_logLevel( -1 )	//Typically only overridden during development
	,	d_maxCalls( 4 )		//Typically only overridden during testing.
	,	d_doProxyCheck( true )

	,	d_sipP2pPresence(false)
	,   d_sipUseOptionsRequest(true)
	,   d_sipRegisterTimeout(s_defSipRegisterTimeout)
	,   d_sipFailureTimeout(s_defSipFailureTimeout)
	,   d_sipPublishTimeout(s_defSipPublishTimeout)
	,   d_sipUseTypingState(false)
	,   d_sipChatWithoutPresence(false)

	,   d_discoveryTopology(ISipAgent::TOPOLOGY_NONE)
	,   d_enableIceMedia(false)
	,   d_enableIceSecurity(false)

	,   d_topologyEncryption(true)
	,   d_topologyTurn(true)

	,   d_sipUdp(true)
	,   d_sipTcp(true)
	,   d_sipTls(false)
	,   d_keepAlive(true)
	,   d_useRport(true)

	,	d_encryption( ISipAgent::ENCRYPTION_NONE )

	,   d_tagProlog("")
	,   d_registrationRefreshInterval(0)
	,   d_failureInterval(0)
	,   d_optionsKeepAliveInterval(0)

	,   d_minSessionTime(0)
	,   d_supportSessionTimer(false)
	,   d_initiateSessionTimer(false)

	,	d_tlsInfo( nullptr )
	,	d_servers( nullptr )
{
}

void SipConfig::setSipRegisterTimeout( int value )        //In seconds
{
    d_sipRegisterTimeout = value;

    if (d_sipRegisterTimeout  <= 0)
    {
        d_sipRegisterTimeout = s_defSipRegisterTimeout;
    }
}

void SipConfig::setSipFailureTimeout( int value )        //In seconds
{
    d_sipFailureTimeout = value;
    
    if (d_sipFailureTimeout <= 0)
    {
        d_sipFailureTimeout = s_defSipFailureTimeout;
    }
}

void SipConfig::setSipPublishTimeout( int value )        //In seconds
{
    d_sipPublishTimeout = value;

    if (d_sipPublishTimeout <= 0)
    {
        d_sipPublishTimeout = s_defSipPublishTimeout;
    }
}

bool SipConfig::isEncryptionRequired() const
{
	return getEncryption() == ISipAgent::ENCRYPTION_REQUIRED;
}

bool SipConfig::isTransportValidForTls() const
{
	//We have either:
	//	UDP or TCP and NOT TLS, or
	//	NOT UDP and NOT TCP and TLS
	bool result = ( getSipUdp() || getSipTcp() ) ? !getSipTls() : getSipTls();
	return result;
}

bool SipConfig::getSipUdpEx() const
{
	return ( isEncryptionRequired() ? false : getSipUdp() );
}

bool SipConfig::getSipTcpEx() const
{
	return ( isEncryptionRequired() ? false : getSipTcp() );
}

bool SipConfig::getSipTlsEx() const
{
	return ( isEncryptionRequired() ? true : getSipTls() );
}

//Get port based on server list and designated transport.
int SipConfig::getSipPort()	const
{
	int result = 0;

	if ( getServers() != nullptr )
	{
		ISipAgent::IServerInfo* serverInfo = this->getServers()->getFirstSipRegistrar(); 

		if ( serverInfo != nullptr )
		{
			result = serverInfo->getPort();
		}
	}

	if ( result == 0 )
	{
		result = (getSipTlsEx() ? 443 : 5060);
	}

	return result;
}

bool SipConfig::hasValidCredentials() const
{
	bool result = ( !getUserName().empty() && !getSipHost().empty() );

	return result;
}

void SipConfig::ensureValidUuid()
{
	if ( d_sipUuid.empty() )
	{
		d_sipUuid = SipAgentUtils::generateUuid();
	}
}
