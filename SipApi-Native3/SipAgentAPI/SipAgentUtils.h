
#pragma once

#include "ISipAgent.h"
#include <string>
#include <Rpc.h>		//For UUID struct, and UuidCreate

class SipAgentUtils
{
public:

	static std::string sipAgentResult(ISipAgent::Result result)
	{
		switch(result)
		{
			case ISipAgent::RESULT_OK:
				return "OK";
			case ISipAgent::RESULT_FAILURE:
				return "Failure";
			case ISipAgent::RESULT_NOT_IMPLEMENTED:
				return "Not Implemented";
			case ISipAgent::RESULT_OUT_OF_MEMORY:
				return "Out of Memory";
			case ISipAgent::RESULT_INVALID_ARGS:
				return "Invalid Arguments";
			case ISipAgent::RESULT_BUFFER_OVERFLOW:
				return "Buffer Overflow";
			default:
				return "Unknown Error";
		}
	}

	static std::string  generateUuid()
	{
		std::string result;

		UUID uuid;
		::UuidCreate( &uuid );

		RPC_CSTR szUuid = nullptr;
		if ( ::UuidToStringA( &uuid, &szUuid ) == RPC_S_OK )
		{
			result = (char*) szUuid;
		}

		return result;
	}

};
