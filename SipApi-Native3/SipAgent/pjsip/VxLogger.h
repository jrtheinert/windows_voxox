
#pragma once

#include <pjsua2.hpp>
#include "../SipAgentAPI/ISipAgent.h"

#include <string>

class VxLogger : public pj::LogWriter
{
	enum LogLevel
	{
		Level_Fatal		= 1,
		Level_Error		= 2,
		Level_Warning	= 3,
		Level_Info		= 4,
		Level_Debug		= 5,
		Level_Verbose	= 6,
	};

public:
	void fatal  ( const std::string& msg );
	void error  ( const std::string& msg );
	void warning( const std::string& msg );
	void info   ( const std::string& msg );
	void debug  ( const std::string& msg );
	void verbose( const std::string& msg );

	void setAgentCallbacks( ISipAgent::IAgentCallbacks* val )		{ d_agentCallbacks = val;	}

private:
	pj::LogEntry makeEntry( LogLevel level, const std::string& msg );

	void write       ( const pj::LogEntry& entry );
	void logToMainApp( const pj::LogEntry& entry );

	std::string cleanMsg( const std::string& msgIn );

private:
	ISipAgent::IAgentCallbacks*	d_agentCallbacks;
};
