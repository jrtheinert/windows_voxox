
#include "VxCall.h"
#include "VxAccount.h"
#include "VxUtils.h"

#include <assert.h>

const float MUTE_LEVEL   = 0.0f;
const float NORMAL_LEVEL = 1.0f;
const float MAX_LEVEL	 = 2.0f;		//TODO-PJSIP: Verify this.

float VxCall::s_savedSoundLevel( NORMAL_LEVEL );

//NOTE: For logging, we can use 'd_account' logging facilities.	//TODO-PJSIP: use IAgentCallbacks so we do not rely on d_account.

//static
void VxCall::setInputVolume( unsigned int volume )
{
	s_savedSoundLevel = normalizeSoundLevel( volume );
}

//static
float VxCall::normalizeSoundLevel( unsigned int volumeIn )
{
	float result = (float) volumeIn / 100.f;
	return result;
}

VxCall::VxCall( VxAccount& account, int callId, ISipAgent::ICallCallbacks* callCallbacks ) 
	: Call( account, callId ) 
	, d_account( account )
	, d_callCallbacks( callCallbacks )
{
}

VxCall::~VxCall()
{
	delete d_callCallbacks;
}

void VxCall::onCallState( pj::OnCallStateParam& parm )
{
	pj::CallInfo callInfo = getInfo();

	CallStateInfo csi = VxUtils::toCallStateInfo( callInfo );

	bool active = isActive();

	handleCallStateChange( csi );

	switch ( callInfo.state )
	{
		case  PJSIP_INV_STATE_DISCONNECTED:
			d_account.removeCall( this );
			break;
	}
}

void VxCall::onCallMediaState( pj::OnCallMediaStateParam& parm )	//OnCallMediaStateParam is an empty struct.
{
	//TODO-PJSIP: Per doc, we should call getRemNatType() in this method.

	const std::string logMethod = "VxCall::onCallMediaState()";
	const std::string logSuffix = " - callId: " + std::to_string( getId() );
	std::string		  logMsg;

	pj::CallInfo ci = getInfo();

	CallStateInfo csi = VxUtils::toCallStateInfo( ci );

	pj::CallMediaInfoVector media = ci.media;	//Appears to have just one entry for now, audio only.

	for ( size_t x = 0; x < media.size(); x++ )
	{
		pj::CallMediaInfo cmi = media[x];

		switch ( cmi.status )
		{
		case PJSUA_CALL_MEDIA_NONE:
			logMsg = logMethod + " PJSUA_CALL_MEDIA_NONE: Call currently has no media" + logSuffix;
			logWarning( logMsg );
			break;

		case PJSUA_CALL_MEDIA_ACTIVE:
			logMsg = logMethod + " PJSUA_CALL_MEDIA_ACTIVE: The media is active" + logSuffix;
			logInfo( logMsg );

			// When media is active, connect call to sound device.
			if ( cmi.type == PJMEDIA_TYPE_AUDIO )
			{
				pjsua_conf_connect( cmi.audioConfSlot, 0 );
				pjsua_conf_connect( 0, cmi.audioConfSlot );
				adjustInputVolume( s_savedSoundLevel * 100 );	
			}

			handleCallStateChange( csi );
			break;

		case PJSUA_CALL_MEDIA_LOCAL_HOLD:
			logMsg = logMethod + " PJSUA_CALL_MEDIA_LOCAL_HOLD: The media is currently put on hold by local endpoint" + logSuffix;
			logInfo( logMsg );

			csi.setCallState( ISipAgent::CallState::CALL_STATE_HOLD );	
			handleCallStateChange( csi );
			break;

		case PJSUA_CALL_MEDIA_REMOTE_HOLD:
			logMsg = logMethod + " PJSUA_CALL_MEDIA_REMOTE_HOLD: The media is currently put on hold by remote endpoint" + logSuffix;
			logInfo( logMsg );
			break;

		case PJSUA_CALL_MEDIA_ERROR:
			logMsg = logMethod + " PJSUA_CALL_MEDIA_ERROR: The media has reported error (e.g. ICE negotiation)" + logSuffix;
			logInfo( logMsg );
			break;
		}
	}
}

//Handle incoming INFO message
void VxCall::onCallTsxState( pj::OnCallTsxStateParam& parm )
{
	pj::SipEventBody eventBody = parm.e.body;

//	//Looking for INFO
	if ( eventBody.tsxState.tsx.method == "INFO" )
	{
		pjsip_msg_body* body = NULL;

		if ( eventBody.tsxState.tsx.role == PJSIP_ROLE_UAC )		//TODO-PJSIP: this portion is not currently important
		{
//			if ( parm.e.body.tsx_state.type == PJSIP_EVENT_TX_MSG )
//			if ( parm.e.body.tsxState.type == PJSIP_EVENT_TX_MSG )
//				body = e->body.tsx_state.src.tdata->msg->body;
//				body = parm.e.body.tsxState.src.tdata->msg->body;
//			else
//				body = e->body.tsx_state.tsx->last_tx->msg->body;
//				body = eventBody.tsxState.tsx.lastTx.
//				body = e->body.tsx_state.tsx->last_tx->msg->body;
		}
		else
		{
			if ( eventBody.tsxState.type == PJSIP_EVENT_RX_MSG )
			{
				pjsip_rx_data* rdata = (pjsip_rx_data*)eventBody.tsxState.src.rdata.pjRxData;

				if ( rdata )
					body = rdata->msg_info.msg->body;
			}
		}

		if ( body != nullptr )
		{
			std::string contentType   ( body->content_type.type.ptr,    body->content_type.type.slen    );
			std::string contentSubType( body->content_type.subtype.ptr, body->content_type.subtype.slen );

			std::string content( (char*) ( body->data ), body->len );

			d_account.handleInfoRequestReceived( getId(), content, contentType );
		}
		else
		{
			//TODO?
		}
	}
}

void VxCall::onDtmfDigit( pj::OnDtmfDigitParam& parm )
{
}

void VxCall::onCallMediaTransportState( pj::OnCallMediaTransportStateParam& parm )
{
}

void VxCall::onCallMediaEvent( pj::OnCallMediaEventParam& parm )
{
}

void VxCall::onCreateMediaTransport( pj::OnCreateMediaTransportParam& parm )
{
}

void VxCall::onCallSdpCreated( pj::OnCallSdpCreatedParam& parm )
{
}

//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
// Call 'action' methods
//-----------------------------------------------------------------------------

void VxCall::setRinging()
{
	answerWithStatus( PJSIP_SC_RINGING );
}

void VxCall::accept()
{
	answerWithStatus( PJSIP_SC_OK );
}

void VxCall::answerWithStatus( pjsip_status_code statusCode )
{
	try
	{
		pj::CallOpParam parm(true);
		parm.statusCode = statusCode;

		(static_cast<pj::Call*>(this))->answer( parm );
	}
	catch ( pj::Error& error )
	{
		logError( "answerWithStatus failed: " + error.info() );
	}
}

void VxCall::hangup()
{
	hangupWithStatus( PJSIP_SC_DECLINE );		//If we use zero, PJSIP will send 603/decline anyway
}

void VxCall::decline()
{
	hangupWithStatus( PJSIP_SC_DECLINE );
}

void VxCall::busyEveryWhere()
{
	hangupWithStatus( PJSIP_SC_BUSY_EVERYWHERE );
}

void VxCall::hangupWithStatus( pjsip_status_code statusCode )
{
	try
	{
		pj::CallOpParam parm(true);
		parm.statusCode = statusCode;

		(static_cast<pj::Call*>(this))->hangup( parm );		//TODO-PJSIP: We are hitting this twice, resulting in exception below.  OK for now.
	}
	catch ( pj::Error& error )
	{
		logError( "hangupWithStatus failed: " + error.info() );
	}
}

void VxCall::hold()
{
	pj::CallOpParam parm(true);
//	parm.options = PJSUA_CALL_UPDATE_CONTACT;
//	parm.opt.flag = PJSUA_CALL_UPDATE_CONTACT;

	setHold( parm );
}

void VxCall::resume()
{
	pj::CallOpParam parm(true);
	parm.opt.flag = PJSUA_CALL_UNHOLD;

	reinvite( parm );
}

void VxCall::mute()
{
	s_savedSoundLevel = getRxSoundLevel();	//So we can reset the same sound level when we unmute
	adjustRxSoundLevel( MUTE_LEVEL );
}

void VxCall::unmute()
{
//	adjustSoundLevel( NORMAL_LEVEL );
	adjustRxSoundLevel( s_savedSoundLevel );
}

void VxCall::adjustInputVolume( unsigned int volumeIn )
{
	float volume = normalizeSoundLevel( volumeIn );
	adjustRxSoundLevel( volume );
}


void VxCall::transfer( const std::string& destUri )
{
	pj::CallOpParam parm(true);

	xfer( destUri, parm );
}

void VxCall::sendDtmf( const std::string& digits )
{
	dialDtmf( digits );
}

std::string VxCall::getSipCallId()
{
	std::string result;

	try
	{
		pj::CallInfo ci = getInfo();

		result = ci.callIdString;
	}
	catch ( pj::Error& error )
	{
		logWarning( "getSipCallId() failed: " + error.info() );
	}

	return result;
}

bool VxCall::isCallEncrypted()
{
	bool result = false;

	try
	{
		pj::CallInfo ci = getInfo();

//		result = ci.callIdString;			//TODO-PJSIP
	}
	catch ( pj::Error& error )
	{
		logWarning( "isCallEncrypted() failed: " + error.info() );
	}

	return result;
}

//-----------------------------------------------------------------------------
//Sound level related methods
//-----------------------------------------------------------------------------

void VxCall::adjustRxSoundLevel( float level )
{
	adjustSoundLevel( level, true );
}

void VxCall::adjustTxSoundLevel( float level )
{
	adjustSoundLevel( level, false );
}

void VxCall::adjustSoundLevel( float level, bool isRx )
{
	pj::CallInfo ci = getInfo();

	for ( size_t x = 0; x < ci.media.size(); x++ )
	{
		if ( ci.media[x].type == PJMEDIA_TYPE_AUDIO )
		{
			pj::AudioMedia* audioMedia = (pj::AudioMedia*)getMedia(x);

			if ( audioMedia )
			{
				if ( isRx )
					audioMedia->adjustRxLevel( level );
				else
					audioMedia->adjustTxLevel( level );
			}
		}
	}
}

float VxCall::getRxSoundLevel()
{
	return getSoundLevel( true );
}

float VxCall::getTxSoundLevel()
{
	return getSoundLevel( false );
}

float VxCall::getSoundLevel( bool isRx )
{
	float result = -1.0;

	pj::CallInfo ci = getInfo();

	for ( size_t x = 0; x < ci.media.size(); x++ )
	{
		if ( ci.media[x].type == PJMEDIA_TYPE_AUDIO )
		{
			pj::AudioMedia* audioMedia = (pj::AudioMedia*)getMedia(x);

			if ( audioMedia )
			{
				if ( isRx )
					result = (float) audioMedia->getRxLevel();
				else
					result = (float)audioMedia->getTxLevel();
			}
		}
	}

	return result;
}

//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
//Helper methods
//-----------------------------------------------------------------------------
void VxCall::handleCallStateChange( const CallStateInfo& csi )
{
	d_callCallbacks->stateChanged( csi );
}

void VxCall::logError( const std::string& msg )
{
	d_account.logError( msg );
}

void VxCall::logWarning( const std::string& msg )
{
	d_account.logWarning( msg );
}

void VxCall::logInfo( const std::string& msg )
{
	d_account.logInfo( msg );
}

//=============================================================================



//=============================================================================
// VxCall List
//=============================================================================

bool VxCalls::add( VxCall* callIn )
{
	bool    result   = false;
	VxCall* tempCall = findByCallId( callIn->getId() );

	if ( tempCall )
	{
//		tempCall->logWarning( "VxCalls::add() - CallId already exists: " + std::to_string( callIn->getId() ) );
	}
	else
	{
		this->push_back( callIn );
		result = true;
	}

	return result;
}

bool VxCalls::remove( VxCall* callIn )
{
	bool result = false;

	for ( VxCalls::const_iterator it = begin(); it != end(); it++ )
	{
		if ( (*it)->getId() == callIn->getId() )
		{
			delete *it;
			this->erase( it );
			result = true;
			break;
		}
	}

	if ( ! result )
	{
//		logWarning( "VxCalls::remove() - CallId not found: " + std::to_string( callIn->getId() ) );
	}

	return result;
}

VxCall* VxCalls::findByCallId( int tgtCallId )
{
	VxCall* result = nullptr;

	for ( VxCalls::const_iterator it = begin(); it != end(); it++ )
	{
		if ( (*it)->getId() == tgtCallId )
		{
			result = *it;
			break;
		}
	}

	return result;
}

VxCall* VxCalls::findByFrom( const std::string& tgtFrom )
{
	VxCall* result = nullptr;

	for ( VxCalls::const_iterator it = begin(); it != end(); it++ )
	{
		pj::CallInfo ci = (*it)->getInfo();

		if ( ci.remoteUri == tgtFrom )
		{
			result = *it;
			break;
		}
	}

	return result;
}

void VxCalls::adjustCallInputLevel( unsigned int level )
{
	for ( VxCalls::const_iterator it = begin(); it != end(); it++ )
	{
		if ( *it != nullptr )
		{
			(*it)->adjustInputVolume( level );
		}
	}
}

//=============================================================================
