
#pragma once

#include <pjsua2.hpp>
#include "../SipAgentAPI/CallStateInfo.h"

#include <list>

class VxAccount;
class ISipAgent::ICallCallbacks;

class VxCall : public pj::Call
{
public:
	VxCall( VxAccount& account, int callId, ISipAgent::ICallCallbacks* callbacks );

	~VxCall();

	void onCallState	 ( pj::OnCallStateParam&      parm );	// Notification when call's state has changed.
	void onCallMediaState( pj::OnCallMediaStateParam& parm );	// Notification when call's media state has changed.
	void onCallTsxState  ( pj::OnCallTsxStateParam&   parm );	// Handling incoming INFO requests (and more)
	void onDtmfDigit	 ( pj::OnDtmfDigitParam&      parm );	// Handle incoming DTMF digit, if necessary

	//Testing these
	void onCallMediaTransportState( pj::OnCallMediaTransportStateParam& parm );
	void onCallMediaEvent		  ( pj::OnCallMediaEventParam&			parm );
	void onCreateMediaTransport	  ( pj::OnCreateMediaTransportParam&	parm );	
	void onCallSdpCreated		  ( pj::OnCallSdpCreatedParam&			parm );

	//Call 'actions'
	void setRinging();
	void accept();
	void hangup();
	void decline();
	void busyEveryWhere();

	void hold();
	void resume();	//unhold

	void mute();
	void unmute();
	void adjustInputVolume( unsigned int level );		//Appears to have a range of 0 - 200.

//	void update();		//TODO-PJSIP: What does this do?
	void transfer( const std::string& destUri );
//	void transferReplaces( const std::string& destUri );

	void sendDtmf( const std::string& digits );

	std::string getSipCallId();
	bool		isCallEncrypted();

	void handleCallStateChange( const CallStateInfo& csi );

	static void setInputVolume      ( unsigned int volume );
	static float normalizeSoundLevel( unsigned int volume );	//Convert App range 0-200 to PJSIP range 0.0 to 2.0

private:
	//Helpers
	void answerWithStatus( pjsip_status_code statusCode );
	void hangupWithStatus( pjsip_status_code statusCode );

	void adjustSoundLevel  ( float level, bool isRx );
	void adjustRxSoundLevel( float level );
	void adjustTxSoundLevel( float level );

	float getSoundLevel  ( bool isRx );
	float getRxSoundLevel();
	float getTxSoundLevel();

	void logError  ( const std::string& msg );
	void logWarning( const std::string& msg );
	void logInfo   ( const std::string& msg );

private:
	VxAccount&						d_account;
	ISipAgent::ICallCallbacks*		d_callCallbacks;
	
	static float					s_savedSoundLevel;	//So we can retain levels between calls.
};

//=============================================================================


//=============================================================================

class VxCalls : public std::list<VxCall*>
{
public:
	bool	add   ( VxCall* call );
	bool	remove( VxCall* call );

	VxCall* findByCallId( int tgtCallId );
	VxCall* findByFrom  ( const std::string& from );

	void	adjustCallInputLevel( unsigned int level );

};

//=============================================================================
