
#pragma once

#include <pjsua2.hpp>
#include "VxCall.h"

#include <string>

class VxAccount : public pj::Account
{
	friend VxCall;		//For access to logging

public:
	VxAccount();

	void	setLineCallbacks ( ISipAgent::ILineCallbacks*  val )			{ d_lineCallbacks  = val;	}
	void	setAgentCallbacks( ISipAgent::IAgentCallbacks* val )			{ d_agentCallbacks = val;	}

	VxCall* createCall( ISipAgent::ICallCallbacks* callbacks );
	int     makeCall  ( ISipAgent::ICallCallbacks* callbacks, const std::string& uri, bool enableVideo );

	void	onRegState    ( pj::OnRegStateParam&     parm );
	void	onIncomingCall( pj::OnIncomingCallParam& parm );

	bool	addCall   ( VxCall* callIn );
	bool	removeCall( VxCall* callIn );

	bool	adjustCallInputVolume( unsigned int volume );	//For all current and future calls.

	VxCall* findCallById( int callId, const std::string& method );


	//-----------------------------------------------------------------------------
	//Call related methods (so API doesn't have to handle it)
	//-----------------------------------------------------------------------------
	void		sendRingingNotification( int callId, bool enableVideo );
	void		acceptCall			   ( int callId, bool enableVideo );
	void		rejectCall			   ( int callId );
	void		closeCall			   ( int callId );
	void		holdCall			   ( int callId );
	void		resumeCall			   ( int callId );
	void		muteCall			   ( int callId, bool muteOn );
	void		adjustCallInputVolume  ( int callId, unsigned int volume );
	void		blindTransferCall	   ( int callId, const std::string& uri  );
	void		playDtmfOnCall		   ( int callId, const std::string& dtmf );
	void		playSoundFileOnCall	   ( int callId, const std::string& file );
	bool		isCallEncrypted		   ( int callId );
	std::string getSipCallId		   ( int callId );

	//Callback handlers - typically called from VxCall.
	void handleInfoRequestReceived( int callId, const std::string& content, const std::string& contentType );

private:
	void logError  ( const std::string& msg );
	void logWarning( const std::string& msg );
	void logInfo   ( const std::string& msg );

	//These methods are just to handle Flips from ourselves, since server is not handling that.
	bool isFlipFromMe ( const std::string& incomingNumber );
	bool incomingNumberIsMine    ( const std::string& incomingNumber );	//Outbound call
	bool incomingNumberInCallList( const std::string& incomingNumber );	//Inbound call

	std::string parseIncomingNumber( const std::string& incomingNumber );
	std::string getMyNumber();

private:
	//PJSIP assigns callId in a round-robin fashion, so do NOT try to use your own.  Otherwise you run into config maxcalls issue.
	VxCalls						d_calls;
	ISipAgent::ILineCallbacks*	d_lineCallbacks;
	ISipAgent::IAgentCallbacks*	d_agentCallbacks;	//For logging
};
