
#pragma once

#include <pjsua2.hpp>		
#include "VxAccount.h"
#include "VxEndpoint.h"

#include "../SipAgentAPI/ISipAgent.h"

//TODO-PJSIP: We hope to remove these dependencies
#include "../Shared/Lock.h"

class VxAgent: public ISipAgent::Agent
{
private:
	Lock                             d_lock;
	ISipAgent::IAgentCallbacks*		 d_agentCallbacks;

	bool							 d_pjsipOn;
	VxEndpoint						 d_endpoint;
	VxAccount						 d_pjAccount;

	SipConfig*						 d_sipConfig;

public:
	VxAgent();

    virtual ~VxAgent() throw();

    ISipAgent::Result init(ISipAgent::IAgentCallbacks* callbacks);

    void shutdown() throw();

	//Set properties ---------------------------------------------------------------------------------
	ISipAgent::Result configure( SipConfig* sipConfig );

    ISipAgent::Result setNS				( bool enable )	throw();
    ISipAgent::Result setExperimentalNS ( bool enable ) throw();
    ISipAgent::Result setAGC			( bool enable ) throw();
    ISipAgent::Result setExperimentalAGC( bool enable ) throw();
    ISipAgent::Result setEC				( bool enable ) throw();
    ISipAgent::Result setDelayCorrection( bool enable ) throw();
    ISipAgent::Result setHighPassFilter	( bool enable ) throw();

    ISipAgent::Result setRecordingDevice( const std::string& guid )	throw();
    ISipAgent::Result setPlayoutDevice	( const std::string& guid )	throw();
    ISipAgent::Result setRingingDevice	( const std::string& guid )	throw() ;

    ISipAgent::Result setSpeakerVolume	  ( unsigned int volume ) throw();
    ISipAgent::Result setRingingVolume	  ( unsigned int volume ) throw();
    ISipAgent::Result setMicVolume		  ( unsigned int volume ) throw();

	//Get properties ---------------------------------------------------------------------------------
    ISipAgent::Result getSpeechInputLevelFullRange( unsigned int* level ) throw();  

    ISipAgent::Result getSpeakerVolume	  ( unsigned int* volume ) throw();
    ISipAgent::Result getRingingVolume	  ( unsigned int* volume ) throw();
    ISipAgent::Result getMicVolume		  ( unsigned int* volume ) throw();

    ISipAgent::Result startPlayRingingFile( const char* name, bool  loop ) throw();
    ISipAgent::Result stopPlayRingingFile() throw();

    int				  createLine			( ISipAgent::ILineCallbacks* callbacks ) throw();

	//Logging methods
	void logFailure();
	void logFailure( const std::string& msg );
	void logError  ( const std::string& msg );
	void logWarning( const std::string& msg );
	void logInfo   ( const std::string& msg );
	void logDebug  ( const std::string& msg );
	void logVerbose( const std::string& msg );


	//-----------------------------------------------------------------------------
	//Line/Account related methods (so API doesn't have to handle it)
	//-----------------------------------------------------------------------------
	int makeCall( int lineId, ISipAgent::ICallCallbacks* callbacks, const std::string& uri, bool enableVideo );

	bool hasLineId   ( int lineId );
	bool connectLine ( int lineId );
	bool populateLine( int lineId );
	bool registerLine( int lineId );
	bool removeLine  ( int lineId, bool force );

	void clearLines();
	int  getLineCount();

	//-----------------------------------------------------------------------------
	//Call related methods (so API doesn't have to handle it)
	//-----------------------------------------------------------------------------
	void		sendRingingNotification( int callId, bool enableVideo );
	void		acceptCall			   ( int callId, bool enableVideo );
	void		rejectCall			   ( int callId );
	void		closeCall			   ( int callId );
	void		holdCall			   ( int callId );
	void		resumeCall			   ( int callId );
	void		muteCall			   ( int callId, bool muteOn );
	void		setCallInputVolume	   ( int callId, unsigned int volume	 );
	void		blindTransferCall	   ( int callId, const std::string& uri  );
	void		playDtmfOnCall		   ( int callId, const std::string& dtmf );
	void		playSoundFileOnCall	   ( int callId, const std::string& file );
	bool		isCallEncrypted		   ( int callId );
	std::string getSipCallId		   ( int callId );

private:
	void determineLogLevel();

	std::string prependFileName( const std::string& msgIn );
};
