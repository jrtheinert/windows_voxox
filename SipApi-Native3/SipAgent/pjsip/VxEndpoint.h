
//We subclass Pjsip Endpoint class to instantiate some callbacks
//	'Endpoint' is API equivalent of 'user agent', or at least it appears to be.

#pragma once

#include "VxAccount.h"
#include "VxLogger.h"

#include <pjsua2.hpp>

#include "../SipAgentAPI/SipConfig.h"
#include "../SipAgentAPI/DataTypes.h"	//For ILogger

typedef std::list<pj::AudioDevInfo> AudioDevInfoList;

class CallbackLogger : public ILogger
{
public:
	CallbackLogger( VxLogger* logger );

	void fatal  ( const std::string& msg );
	void error  ( const std::string& msg );
	void warning( const std::string& msg );
	void info   ( const std::string& msg );
	void debug  ( const std::string& msg );
	void verbose( const std::string& msg );
private:
	VxLogger*	mLogger;
};


class VxEndpoint : public pj::Endpoint
{
public:
	VxEndpoint();
	~VxEndpoint();

	//TODO-PJSIP: callback for transport state change, NAT detection

	bool initialize( SipConfig* sipConfig, ISipAgent::IAgentCallbacks* agentCallbacks );
	void shutdown();

	//Audio and Audio Device related methods
	bool setAudioInputDevice ( const std::string& guid );
	bool setAudioOutputDevice( const std::string& guid );
	bool setAudioRingerDevice( const std::string& guid );	//May just be a local thing, to play a local file.

	pj::AudioDevInfo getAudioInputDevice ();
	pj::AudioDevInfo getAudioOutputDevice();
	pj::AudioDevInfo getAudioRingerDevice();				//May just be a local thing, to play a local file.

	bool setAudioInputLevel ( int level );
	bool setAudioOutputLevel( int level );

	int getAudioInputLevel ();			//May just be private or optional
	int getAudioOutputLevel();			//May just be private or optional

	int getAudioInputLevelFullRange();	//May just be private or optional
	int getAudioOutputLevelFullRange();	//May just be private or optional

	void setComfortNoise( bool value );
	bool getComfortNoise();

	//Input Latency
	//Output Latency

	//OutputRoute			//What are these and how are they used?
	//InputRoute

	//Voice Activity Detection
//	void setVad( bool value );							//TODO-PJSIP: Do we need these?
//	bool getVad();

//	void setPacketLoseConcealment( bool value );		//TODO-PJSIP: Do we need these?
//	bool getPacketLoseConcealment();

	void startPlayFile( const std::string file, bool loop );
	void stopPlayFile();


	//Advanced
//	CodecInfoList getAudioCodecs();

	//-----------------------------------------------------------------------------
	//Line/Account related methods (so API doesn't have to handle it)
	//-----------------------------------------------------------------------------
	int  createAccount( ISipAgent::ILineCallbacks* callbacks, ISipAgent::IAgentCallbacks* agentCallbacks );
//	void modifyAccount(pj_turn_tp_type turnConnType);

	bool hasAccountId( int lineId );
	bool removeLine  ( int lineId, bool force );

	int  makeCall( int lineId, ISipAgent::ICallCallbacks* callbacks, const std::string& uri, bool enableVideo );

	void clearLines();
	int  getLineCount();


	//-----------------------------------------------------------------------------
	//Call related methods (so API doesn't have to handle it)
	//-----------------------------------------------------------------------------

	void		sendRingingNotification( int callId, bool enableVideo );
	void		acceptCall			   ( int callId, bool enableVideo );
	void		rejectCall			   ( int callId );
	void		closeCall			   ( int callId );
	void		holdCall			   ( int callId );
	void		resumeCall			   ( int callId );
	void		muteCall			   ( int callId, bool muteOn );
	void		adjustCallInputVolume  ( int callId, unsigned int volume     );
	void		blindTransferCall	   ( int callId, const std::string& uri  );
	void		playDtmfOnCall		   ( int callId, const std::string& dtmf );
	void		playSoundFileOnCall	   ( int callId, const std::string& file );
	bool		isCallEncrypted		   ( int callId );
	std::string getSipCallId		   ( int callId );

private:
	void earlyConfigure();
	void configureAccount();		//'Account' is equivalent of 'line', in our API
	void configureCodecs();
	void configureEndpoint();
	void configureTransport();

	void natDetection();
	bool isNatDetected()			{ return !d_natDetectionFailed; };

	void logCodecs();

	void logError  ( const std::string& msg );
	void logWarning( const std::string& msg );
	void logInfo   ( const std::string& msg );
	void logDebug  ( const std::string& msg );

	//Overrides for callbacks
	void onNatDetectionComplete( const pj::OnNatDetectionCompleteParam& parm );
	void onTransportState      ( const pj::OnTransportStateParam&       parm );

	void onNatCheckStunServersComplete( const pj::OnNatCheckStunServersCompleteParam& parm );

	pj::AudioDevInfo  getAudioDeviceInfoByGuid ( const std::string& tgtGuidIn );
	int				  getAudioDeviceIndexByGuid( const std::string& tgtGuidIn );

	AudioDevInfoList enumDevicesSafe();

	std::string		detectHttpProxyForRegistrar();
	std::string		 getTurnTlsServer();
	pj::StringVector getStunServers();
	pj::StringVector getServers( ISipAgent::IServerInfo::Type type, bool includePort );


private:
	VxLogger*	d_logger;		//This handles messages from PJSIP.  The ptr is passed to Endpoint which then owns it.
	VxAccount*	d_account;		//For now, we just allow one account
	SipConfig*	d_sipConfig;

	CallbackLogger*	d_callbackLogger;		//To be passed to ProxyDetect

	bool		d_natDetectionFailed;
};