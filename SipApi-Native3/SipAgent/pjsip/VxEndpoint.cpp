
#include "VxEndpoint.h"
#include "VxCall.h"
#include "VxUtils.h"
#include "../SipAgentAPI/DataTypes.h"		//ServerInfoList
#include "../SipAgentAPI//ProxyDetect.h"	//ProxyDetect
#define CONSOLE_LOG_LEVEL 5

#define PJMEDIA_HAS_OPUS_CODEC 1		//TODO-PJSIP Why is this needed?  Isn't OPUS codec initialized in PJSIP stack?
#include "pj_sources/pj_opus.h"
#include "rewriters/pjsip_opus_sdp_rewriter.h"

//=============================================================================

CallbackLogger::CallbackLogger(	VxLogger* logger ) : mLogger( logger )
{
}

void CallbackLogger::fatal( const std::string& msg )
{
	if ( mLogger != nullptr )
	{
		mLogger->fatal( msg );
	}
}

void CallbackLogger::error( const std::string& msg )
{
	if ( mLogger != nullptr )
	{
		mLogger->error( msg );
	}
}

void CallbackLogger::warning( const std::string& msg )
{
	if ( mLogger != nullptr )
	{
		mLogger->warning( msg );
	}
}

void CallbackLogger::info( const std::string& msg )
{
	if ( mLogger != nullptr )
	{
		mLogger->info( msg );
	}
}

void CallbackLogger::debug( const std::string& msg )
{
	if ( mLogger != nullptr )
	{
		mLogger->debug( msg );
	}
}

void CallbackLogger::verbose( const std::string& msg )
{
	if ( mLogger != nullptr )
	{
		mLogger->verbose( msg );
	}
}

//=============================================================================

VxEndpoint::VxEndpoint() 
	: d_sipConfig( nullptr )
	, d_account  ( nullptr )
	, d_logger   ( nullptr )
	, d_natDetectionFailed(false)
{
}

VxEndpoint::~VxEndpoint()
{
	shutdown();
}

void VxEndpoint::shutdown()
{
	VxUtils::ensureThreadIsRegistered( "VxEndpoint::shutdown()" );

	libDestroy();
}

bool VxEndpoint::initialize( SipConfig* sipConfig, ISipAgent::IAgentCallbacks* agentCallbacks )
{
	bool result = true;

	try
	{
		d_sipConfig = sipConfig;

		d_logger    = new VxLogger();	//This will be 'owned' by pj::EndPoint when set in configuration
		d_logger->setAgentCallbacks( agentCallbacks );

		d_callbackLogger = new CallbackLogger( d_logger );
		
		earlyConfigure();

		libCreate();					//Initialize Endpoint

		configureEndpoint();			//Configure Logging, Media and UA, then libInit()
		configureTransport();
	
		libStart();						//Start the library (worker threads, etc.)

		logInfo( "PJSUA2 started." );

		configureCodecs();

		natDetection();
	}
	catch( pj::Error& error )
	{
		logError( "VxEndpoint::initialize() failed: " + error.info() );
		result = false;
	}

	return result;
}

//Items not covered in other PJSIP config areas.
void VxEndpoint::earlyConfigure()
{
	pjsip_cfg()->tcp.keep_alive_interval = d_sipConfig->getOptionsKeepAliveInterval();
	pjsip_cfg()->tls.keep_alive_interval = d_sipConfig->getOptionsKeepAliveInterval();
}

void VxEndpoint::configureEndpoint()
{
	pj::EpConfig epCfg;

	//Configure logging
	epCfg.logConfig.msgLogging		= true;
	epCfg.logConfig.level			= d_sipConfig->getLogLevel();
	epCfg.logConfig.consoleLevel	= CONSOLE_LOG_LEVEL;
	epCfg.logConfig.filename		= d_sipConfig->getSipLogFile();
	epCfg.logConfig.decor			= epCfg.logConfig.decor | PJ_LOG_HAS_NEWLINE | PJ_LOG_HAS_CR;
	epCfg.logConfig.writer			= d_logger;		//Now owned by pj::Endpoint.

	//Configure default media
	epCfg.medConfig.noVad = true;
		
	//Configure UA
	epCfg.uaConfig.maxCalls  = d_sipConfig->getMaxCalls();
	epCfg.uaConfig.threadCnt = 10;
	epCfg.uaConfig.userAgent = d_sipConfig->getSipUserAgent();
	
//	epCfg.uaConfig.nameserver	//StringVector
	epCfg.uaConfig.stunServer	= getStunServers();
	epCfg.uaConfig.stunIgnoreFailure = true;
		
	libInit( epCfg );
}

void VxEndpoint::configureCodecs()
{
	pj_status_t status = pjmedia_codec_opus_init( pjsua_get_pjmedia_endpt() );

	logInfo( "initialized opus.  Status: " + std::to_string( status ) );

	//Set priority for OPUS and X-OPUS to top priority.
	//	Others are lower priority or disabled.
	const int disabled = 0;	//PJMEDIA_CODEC_PRIO_DISABLED
	const int lowest   = 1;	//PJMEDIA_CODEC_PRIO_LOWEST

	//This priority matches iOS.  No SDP example received from Android team.
	//Note: As of 2015.10.20, we not yet support vx-opus, so setting the priority will fail.
	char*	   tempCodec[]    = { "vx-opus", "x-opus", "opus", "GSM",  "G722", "PCMU", "PCMA", "iLBC", "speex"  };
	pj_uint8_t tempPriority[] = { 255,       254,      253,    lowest, lowest, lowest, lowest, lowest, disabled };
	int		   count		  = sizeof(tempCodec) / sizeof(char*);

	for ( int x = 0; x < count; x++ )
	{
		pj_str_t identifier = pj_str( tempCodec[x] );

		status = pjsua_codec_set_priority( &identifier, tempPriority[x] );

		if ( status != PJ_SUCCESS )
		{
			logError( "Error setting priority to " + std::string( tempCodec[x] ) + " .  Status: " + std::to_string( status ) );
		}
	}

#if PJMEDIA_HAS_OPUS_CODEC || PJMEDIA_HAS_XOPUS_CODEC
	pjsip_opus_sdp_rewriter_init( 16000 );
#endif

	logCodecs();
}

void VxEndpoint::configureAccount()
{
	std::string sipAoR;
//	std::string regUri;
	std::string outboundUri;

	sipAoR = d_sipConfig->getSipUserUrl();

	outboundUri = d_sipConfig->getProxyUrl();

	std::string transportType;

	if ( d_sipConfig->getSipTls() )
	{
		transportType = "tls";
	}
	else if ( d_sipConfig->getSipTcp() )
	{
		transportType = "tcp";
	}
	else if ( d_sipConfig->getSipUdp() )
	{
		transportType = "udp";
	}

	if ( transportType.empty() )
	{
		logError( "No valid transport found.");
	}
	else
	{
		outboundUri += ";transport=" + transportType;
//		regUri      += ";transport=" + transportType;

		logInfo( "outboundUri = " + outboundUri );

		//NOTE: Curtis had a crash somewhere between here (the last log entry before the crash)
		//	around line 262 where we log "registration_enabled = " ), which was not in the log.
		//	We did NOT have a TURN server, so that log entry would not have been made anyway.
		//	The crash repeated three times.  However, when Curtis renamed <appdata>, thus
		//	forcing app to create a new one, the crash went away.  Very odd.
		//	Curtis' appdata on my computer worked just fine.
		//
		//	To narrow this down, if it occurs again, I am adding some more debug logging.

		
		pj::AuthCredInfo credInfo;
		
		credInfo.scheme = "digest";
		credInfo.realm  = "*";
		credInfo.username = d_sipConfig->getUserName();
		credInfo.dataType = PJSIP_CRED_DATA_PLAIN_PASSWD;
		credInfo.data	  = d_sipConfig->getPassword();

		//Sample values
		//AccountConfig acc_cfg;
		//acc_cfg.idUri = "sip:test1@pjsip.org";
		//acc_cfg.regConfig.registrarUri = "sip:pjsip.org";


		//Added option to not due proxy check, since we have an intermittent crash and need to do demos like MWC.
		if ( d_sipConfig->getDoProxyCheck() )
		{
			logDebug( "Before detectHttpProxyForRegistrar()" );
			std::string httpProxy = detectHttpProxyForRegistrar();
			logDebug( "After detectHttpProxyForRegistrar()" );
		}

		pj::AccountConfig acctCfg;
		
		acctCfg.idUri					= sipAoR;	//Sample: �sip:account@serviceprovider�.
//		acctCfg.idUri					= "<sips:19508966062@8.38.43.130:443>";	//Test - this works on incoming call, 'sip' vs. 'sips'
//		acctCfg.idUri					= "jrt1@tcoff.net <sip:19508966062@8.38.43.130:443>";
		acctCfg.priority				= 10;		//Meaningless with just a single account

		//Account Registration config
		acctCfg.regConfig.registrarUri			= outboundUri;
		acctCfg.regConfig.registerOnAdd			= true;
		acctCfg.regConfig.timeoutSec			= d_sipConfig->getSipRegisterTimeout();
		acctCfg.regConfig.retryIntervalSec		= 30;	//PJSUA_REG_RETRY_INTERVAL;	//d_sipConfig->getRegistrationRefreshInterval();	//TODO-PJSIP: Is this proper value? (default = PJSUA_REG_RETRY_INTERVAL )
		acctCfg.regConfig.delayBeforeRefreshSec = 5;	//d_sipConfig->getRegistrationRefreshInterval();	//TODO-PJSIP: Is this proper value? (default = 5)
		acctCfg.regConfig.proxyUse				= 3;	//Default: 3 (PJSUA_REG_USE_OUTBOUND_PROXY | PJSUA_REG_USE_ACC_PROXY)

		//Account SIP Config
		acctCfg.sipConfig.authCreds.push_back( credInfo );
		acctCfg.sipConfig.proxies.push_back( outboundUri );

//		if ( !httpProxy.empty() )
//			acctCfg.sipConfig.proxies.push_back( httpProxy );	//TODO-PSJIP: or just proxyUrl?

		//Account Call Config
		acctCfg.callConfig.holdType		 = PJSUA_CALL_HOLD_TYPE_DEFAULT;
		acctCfg.callConfig.timerMinSESec = d_sipConfig->getMinSessionTime();
//		acctCfg.callConfig.timerUse		 = d_sipConfig->getSupportSessionTimer();	//TODO-PJSIP:
//		acctCfg.callConfig.timerSessExpiresSec = d_sipConfig->get

		//Account NAT Config TODO-PJSIP: Appears we need some data conversion between our API values and PJSIP values
		//	We may have issue with terminology as in PJSIP and STUN vs. API and ICE.
		
//		acctCfg.natConfig.sipOutboundInstanceId = d_sipConfig->getSipUuid();		//This causes SIP Registration to fail.
		acctCfg.natConfig.sipOutboundRegId		= d_sipConfig->getSipUuid();

		acctCfg.natConfig.sipStunUse		= PJSUA_STUN_USE_DISABLED;	//PJSUA_STUN_USE_DEFAULT;	//Use setting in global pjsua_config
		acctCfg.natConfig.mediaStunUse		= PJSUA_STUN_USE_DEFAULT;

		ISipAgent::IServerInfo* turnServer = d_sipConfig->getServers()->getFirstTurnTls();

		if ( turnServer )
		{
			//ICE
			acctCfg.natConfig.iceEnabled				 = true;	//d_sipConfig->getEnableIceMedia();
//			acctCfg.natConfig.iceMaxHostCands			 = -1;								//Default, no max
//			acctCfg.natConfig.iceAggressiveNomination	 = true;							//Default
//			acctCfg.natConfig.iceNominatedCheckDelayMsec = PJ_ICE_NOMINATED_CHECK_DELAY;	//Default
//			acctCfg.natConfig.iceAlwaysUpdate			 = true;							//default
		
			//TURN
			acctCfg.natConfig.turnEnabled			= true;
			acctCfg.natConfig.turnServer			= turnServer->getHostPortFormat();	//�DOMAIN:PORT� or �HOST:PORT� format. 
			acctCfg.natConfig.turnConnType			= isNatDetected() ? PJ_TURN_TP_UDP : PJ_TURN_TP_TCP;	//PJ_TURN_TP_UDP or PJ_TURN_TP_TCP.  PJ_TURN_TP_TLS seems to be invalid 
			acctCfg.natConfig.turnUserName			= turnServer->getUserId();
			acctCfg.natConfig.turnPassword			= turnServer->getPassword();
			acctCfg.natConfig.turnPasswordType		= 0;								//Must be '0' to indicate plain password

			logInfo( std::string("turn connection type = ") + ( (acctCfg.natConfig.turnConnType == PJ_TURN_TP_UDP) ? "UDP" : "TCP") );
		}

		//Account Media config
		acctCfg.mediaConfig.srtpUse				= PJSUA_DEFAULT_USE_SRTP;
		acctCfg.mediaConfig.srtpSecureSignaling = PJSUA_DEFAULT_SRTP_SECURE_SIGNALING;

		//TODO:  Is this correct?  d_registrationInterval is also used further down.
		logInfo( std::string( "registration_enabled = " ) + ( d_sipConfig->getRegistrationRefreshInterval() ? "TRUE" : "FALSE" ) );

		// Other Account Configuration items to *MAYBE* address.
		//AccountPresConfig,  to specify presence settings, such as whether presence publication (PUBLISH) is enabled.
		//AccountMwiConfig,   to specify MWI (Message Waiting Indication) settings.
		//AccountVideoConfig, to specify video settings, such as default capture and render device.

		//Create account
		try
		{
			assert( d_account != nullptr );

			d_account->create( acctCfg );		//NOTE: we can also modify() the account
			bool temp = d_account->isDefault();
			d_account->setDefault();			//May be redundant
		}
		catch( pj::Error& error )
		{
			logError( "pj::Account::create failed: " + error.info() );
		}
	}
}

void VxEndpoint::configureTransport()
{
	ISipAgent::Result result = ISipAgent::RESULT_OK;

	pj::TransportConfig		transportCfg;
	pjsip_transport_type_e	transportType;
	std::string				logMsg;


	transportCfg.port = d_sipConfig->getSipPort();

	//Determine transport type
	//NOTE: the 'Ex()' versions take into account the ENCRYPTION setting.
	if		( d_sipConfig->getSipTcpEx() )
	{
		transportType = PJSIP_TRANSPORT_TCP;
		logMsg = "adding TCP transport ...";
	}
	else if ( d_sipConfig->getSipUdpEx() )
	{
		transportType = PJSIP_TRANSPORT_UDP;
		logMsg = "adding UDP transport ...";
	}
	else if ( d_sipConfig->getSipTlsEx() )
	{
		transportType = PJSIP_TRANSPORT_TLS;
		logMsg = "adding TLS transport ...";

		transportCfg.tlsConfig.method			 = VxUtils::fromSipAgentTlsMethod( d_sipConfig->getTlsInfo()->getMethod() );
		transportCfg.tlsConfig.verifyServer		 = d_sipConfig->getTlsInfo()->getVerifyCertificate();

		transportCfg.tlsConfig.CaListFile		 = d_sipConfig->getTlsInfo()->getCertificateAuthorities();
		transportCfg.tlsConfig.certFile			 = d_sipConfig->getTlsInfo()->getCertificate();
//		transportCfg.tlsConfig.ciphers			 = d_sipConfig->getTlsInfo()->getCiphers();
//		transportCfg.tlsConfig.msecTimeout		 = d_sipConfig->getTlsInfo()->;
		transportCfg.tlsConfig.password			 = d_sipConfig->getTlsInfo()->getPrivateKeyPassword();
		transportCfg.tlsConfig.privKeyFile		 = d_sipConfig->getTlsInfo()->getPrivateKey();
		transportCfg.tlsConfig.proto			 = PJSIP_SSL_DEFAULT_PROTO;
		transportCfg.tlsConfig.requireClientCert = d_sipConfig->getTlsInfo()->getRequireCertificate();
		transportCfg.tlsConfig.verifyClient		 = d_sipConfig->getTlsInfo()->getVerifyCertificate();
		transportCfg.tlsConfig.verifyServer		 = d_sipConfig->getTlsInfo()->getVerifyCertificate();
	}
	else
	{
		result = ISipAgent::RESULT_FAILURE;
		logError( "No transport has been specified." );
	}

	if ( result == ISipAgent::RESULT_OK )
	{
		try
		{
			logInfo( logMsg );

			transportCreate( transportType, transportCfg );
			
			logInfo( "transport successfully added..." );
		}
		catch( pj::Error& error )
		{
			result = ISipAgent::RESULT_FAILURE;
			logError( "transportCreate() failed: " + error.info() );
		}
	}
}

void VxEndpoint::logCodecs()
{
	pjsua_codec_info c[32];
	unsigned int	 count1 = PJ_ARRAY_SIZE( c );

	pjsua_enum_codecs( c, &count1 );

	//Simply log codec list.
	std::string codecList = "\n";

	for ( unsigned int i = 0; i < count1; i++ )
	{
		codecList += "codec id: " + std::string( c[i].codec_id.ptr ) + ", pri: " + std::to_string( c[i].priority );

		if ( c[i].desc.ptr == nullptr )
		{
			codecList += ", desc: <none>";
		}
		else
		{
			codecList += ", desc: " + std::string( c[i].desc.ptr );
		}

		codecList += "\n";
	}

	codecList += "\n";

	logInfo( codecList );
}

void VxEndpoint::natDetection()
{
	d_natDetectionFailed = false;	//JRT- should be 'false'
	//Async response in onNatDetectionComplete(),
	//	STUN must be enabled and STUN server must be resolve-able, 
	//	or we get an exception.
	try
	{
		natDetectType();				
	}
	catch ( pj::Error& error )
	{
		logWarning( "natDetection failed: " + error.info() );
		d_natDetectionFailed = true;
	}
}

void VxEndpoint::logError( const std::string& msg )
{
	if ( d_logger )
		d_logger->error( msg );
}

void VxEndpoint::logWarning( const std::string& msg )
{
	if ( d_logger )
		d_logger->warning( msg );
}

void VxEndpoint::logInfo( const std::string& msg )
{
	if ( d_logger )
		d_logger->info( msg );
}

void VxEndpoint::logDebug( const std::string& msg )
{
	if ( d_logger )
		d_logger->debug( msg );
}

//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
// Callback overrides
//-----------------------------------------------------------------------------
//NOTE: It appears we do NOT need this for STUN/TURN/ICE support.
//	It is more informational than anything.
void VxEndpoint::onNatDetectionComplete( const pj::OnNatDetectionCompleteParam& parm )
{
	std::string description;

	switch ( parm.natType )
	{
		case PJ_STUN_NAT_TYPE_UNKNOWN:
			description = "PJ_STUN_NAT_TYPE_UNKNOWN: NAT type is unknown because the detection has not been performed.";
			break;

		case PJ_STUN_NAT_TYPE_ERR_UNKNOWN:
			description = "PJ_STUN_NAT_TYPE_ERR_UNKNOWN: NAT type is unknown because there is failure in the detection process, "
						  "possibly because server does not support RFC 3489.";
			break;

		case PJ_STUN_NAT_TYPE_OPEN:
			description = "PJ_STUN_NAT_TYPE_OPEN: This specifies that the client has open access to Internet "
						  "(or at least, its behind a firewall that behaves like a full-cone NAT, but without the translation)";
			break;

		case PJ_STUN_NAT_TYPE_BLOCKED:
			description = "PJ_STUN_NAT_TYPE_BLOCKED: This specifies that communication with server has failed, probably because UDP packets are blocked.";
			break;

		case PJ_STUN_NAT_TYPE_SYMMETRIC_UDP:
			description = "PJ_STUN_NAT_TYPE_SYMMETRIC_UDP: Firewall that allows UDP out, and responses have to come back to the source of the request "
						 "(like a symmetric NAT, but no translation.";
			break;

		case PJ_STUN_NAT_TYPE_FULL_CONE:
			description = "PJ_STUN_NAT_TYPE_FULL_CONE: A full cone NAT is one where all requests from the same internal IP address and port "
						  "are mapped to the same external IP address and port.   Furthermore, any external host can send a packet to the internal host, "
						  "by sending a packet to the mapped external address.";
			break;

		case PJ_STUN_NAT_TYPE_SYMMETRIC:
			description = "PJ_STUN_NAT_TYPE_SYMMETRIC: A symmetric NAT is one where all requests from the same internal IP address and port, "
						  "to a specific destination IP address and port, are mapped to the same external IP address and port. If the same host "
						  "sends a packet with the same source address and port, but to a different destination, a different mapping is used.  "
						  "Furthermore, only the external host that receives a packet can send a UDP packet back to the internal host.";
			break;

		case PJ_STUN_NAT_TYPE_RESTRICTED:
			description = "PJ_STUN_NAT_TYPE_RESTRICTED: A restricted cone NAT is one where all requests from the same internal IP address and port "
						  "are mapped to the same external IP address and port.  Unlike a full cone NAT, an external host (with IP address X) can "
						  "send a packet to the internal host only if the internal host had previously sent a packet to IP address X.";
			break;

		case PJ_STUN_NAT_TYPE_PORT_RESTRICTED:
			description = "PJ_STUN_NAT_TYPE_PORT_RESTRICTED: A port restricted cone NAT is like a restricted cone NAT, but the restriction includes "
						  "port numbers. Specifically, an external host can send a packet, with source IP address X and source port P, to the internal "
						  "host only if the internal host had previously sent a packet to IP address X and port P.: ";
			break;

		default:
			description = "Error: unknown type detected!";
	}

	std::string msg = std::string( parm.natTypeName ) + " - " + std::string( description );

	logInfo( msg );
}

void VxEndpoint::onTransportState( const pj::OnTransportStateParam& parm )
{
	//This appears to handle loss/gain of network.  This is also covered in VxAccount::onRegState()
	//	So nothing to do here, but maybe log event.
	switch ( parm.state )
	{
		case PJSIP_TP_STATE_CONNECTED:		//Transport connected, applicable only to connection-oriented transports such as TCP and TLS.
			break;

		case PJSIP_TP_STATE_DISCONNECTED:	//Transport disconnected, applicable only to connection-oriented transports such as TCP and TLS.
			break;

		case PJSIP_TP_STATE_SHUTDOWN:		//Transport shutdown, either due to TCP/TLS disconnect error from the network, or when shutdown is initiated by PJSIP itself.
			break;

		case PJSIP_TP_STATE_DESTROY:		//Transport destroy, when transport is about to be destroyed.
			break;
	}
}

//-----------------------------------------------------------------------------

void VxEndpoint::onNatCheckStunServersComplete( const pj::OnNatCheckStunServersCompleteParam& parm )
{
	//Testing with TURN
}

//-----------------------------------------------------------------------------
// Audio and Audio Device related methods
//-----------------------------------------------------------------------------

bool VxEndpoint::setAudioInputDevice( const std::string& guid )
{
	bool result = true;

	try
	{
		VxUtils::ensureThreadIsRegistered( "VxEndpoint::setAudioInputDevice" );

		int devIndex = getAudioDeviceIndexByGuid( guid );
		audDevManager().setCaptureDev( devIndex );
	}
	catch (pj::Error& error )
	{
		logWarning( "setAudioInputDevice() failed: " + error.info() );
		result = false;
	}

	return result;
}

bool VxEndpoint::setAudioOutputDevice( const std::string& guid )
{
	bool result = true;

	try
	{
		VxUtils::ensureThreadIsRegistered( "VxEndpoint::setAudioOutputDevice" );

		int devIndex = getAudioDeviceIndexByGuid( guid );		//TODO-PJSIP: Do we care about input vs. output?
		audDevManager().setPlaybackDev( devIndex );
	}
	catch (pj::Error& error )
	{
		logWarning( "setAudioOutputDevice() failed: " + error.info() );
		result = false;
	}

	return result;
}

pj::AudioDevInfo VxEndpoint::getAudioInputDevice()
{
	int devIndex = audDevManager().getCaptureDev();

	pj::AudioMedia temp = audDevManager().getCaptureDevMedia();

	pj::AudioDevInfo result;			//TODO-PJSIP

	return result;
}

pj::AudioDevInfo VxEndpoint::getAudioOutputDevice()
{
	int devIndex = audDevManager().getPlaybackDev();

	pj::AudioMedia temp = audDevManager().getPlaybackDevMedia();

	pj::AudioDevInfo result;			//TODO-PJSIP

	return result;
}

//bool VxEndpoint::setAudioInputLevel( int level )
//{
//	bool result = true;
//	try
//	{
//		bool keep = true;	//Keep setting for future use.
//		audDevManager().setInputVolume( level, keep );
//	}
//	catch( pj::Error& error )
//	{
//		logWarning( "setMicVolume failed: " + error.info() );	//TODO-PJSIP: Always fails with 'invalid capability'.  Does that make sense for 'mic' volume?
//		result = false;
//	}
//
//	return result;
//}

//Instead of adjusting 'device' volume, we adjust call sound level, for current and future calls.
bool VxEndpoint::setAudioInputLevel( int level )
{
	//For all future calls.
	VxCall::setInputVolume( level );

	//For active calls
	if ( d_account )
	{
		d_account->adjustCallInputVolume( level );
	}

	return true;
}

bool VxEndpoint::setAudioOutputLevel( int level )
{
	bool result = true;
	try
	{
		bool keep = true;	//Keep setting for future use.
		audDevManager().setOutputVolume( level, keep );
	}
	catch( ... )
	{
		logWarning( "setMicVolume failed." );
		result = false;
	}

	return result;
}

int VxEndpoint::getAudioInputLevel()			//May just be private or optional
{
	int result = audDevManager().getInputVolume();	//TODO-PJSIP: How does this differ from getInputSignal()?

	return result;
}

int VxEndpoint::getAudioOutputLevel()			//May just be private or optional
{
	int result = audDevManager().getOutputVolume();	//TODO-PJSIP: How does this differ from getOutputSignal()?

	return result;
}

int VxEndpoint::getAudioInputLevelFullRange()	//May just be private or optional
{
	int result = 0;

	return result;
}

int VxEndpoint::getAudioOutputLevelFullRange()	//May just be private or optional
{
	int result = 0;

	return result;
}

void VxEndpoint::setComfortNoise( bool value )
{
	try
	{
		bool keep = true;	//Keep setting
		audDevManager().setCng( value, keep );
	}
	catch( pj::Error& error )
	{
		logError( "setComfortNoise failed.  Check AudioDevInfo.caps for PJMEDIA_AUD_DEV_CAP_CNG. " + error.info() );
	}
}

bool VxEndpoint::getComfortNoise()
{
	bool result = false;

	try
	{
		result = audDevManager().getCng();
	}
	catch( pj::Error& error )
	{
		logError( "setComfortNoise failed.  Check AudioDevInfo.caps for PJMEDIA_AUD_DEV_CAP_CNG. " + error.info() );
	}

	return result;
}


void VxEndpoint::startPlayFile( const std::string file, bool loop )	//TODO-PJSIP
{
}

void VxEndpoint::stopPlayFile()		//TODO-PJSIP
{
}

//-----------------------------------------------------------------------------
//Audio Device helpers
//-----------------------------------------------------------------------------

//Since audDevManager().enumDevs() returns ptrs which may be cleared at any time,
//	specifically if enumDevs() is called again, we create some 'safe' values here.
AudioDevInfoList VxEndpoint::enumDevicesSafe()
{
	AudioDevInfoList result;
	
	audDevManager().refreshDevs();  //TODO-PJSIP: Retest this.

	pj::AudioDevInfoVector temp = audDevManager().enumDev();	//This has ptrs, so copy data to safe areas.

	for ( size_t x = 0; x < temp.size(); x++ )
	{
		//AudioDevInfo::extFmt is an array of ptrs, so we need to dupe that data separately
		pj::AudioDevInfo di = *temp[x];

		di.extFmt.clear();		//We will create new ptrs, as needed.

		//for ( size_t y = 0; y < temp[x]->extFmt.size(); y++ )	//We do not really need extFmt info at this level.
		//{
		//	pj::MediaFormat* mf = new pj::MediaFormat();

		//	mf->id   = temp[x]->extFmt[y]->id;
		//	mf->type = temp[x]->extFmt[y]->type;

		//	di.extFmt.push_back( mf );
		//}

		result.push_back( di );
	}

	return result;
}

//This method is used to internally convert from deviceId 'int' and deviceGuid 'string'
//	to identify the same device.  'int' is used by PJSIP while 'string' is used by Voxox.
//PJSIP AudDevManager already has 'AudioDevInfo getAudioDevInfo( int ) method'
pj::AudioDevInfo VxEndpoint::getAudioDeviceInfoByGuid( const std::string& tgtGuidIn )
{
	pj::AudioDevInfo result;

	if ( !tgtGuidIn.empty() )	//Do NOT match on empty GUID.  This is 'Wave Mapper', not actual device.
	{
		AudioDevInfoList devices = enumDevicesSafe();

		std::wstring tgtGuid = VxUtils::convertToWideString( tgtGuidIn );

		for ( AudioDevInfoList::const_iterator it = devices.begin(); it != devices.end(); it++ )
		{
			if ( (*it).guid == tgtGuid )
			{
				result = *it;
				break;
			}
		}
	}

	return result;
}

int VxEndpoint::getAudioDeviceIndexByGuid( const std::string& tgtGuidIn )
{
	int result = -1;

	pj::AudioDevInfo deviceInfo = getAudioDeviceInfoByGuid( tgtGuidIn );

	if ( !deviceInfo.name.empty() )
	{
		result = audDevManager().lookupDev( deviceInfo.driver, deviceInfo.name );
	}

	return result;
}

//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
//Line/Account related methods (so API doesn't have to handle it)
//-----------------------------------------------------------------------------

int VxEndpoint::createAccount( ISipAgent::ILineCallbacks* lineCallbacks, ISipAgent::IAgentCallbacks* agentCallbacks )
{
	int result = -1;

	try
	{
		if ( d_account == nullptr )
		{
			d_account = new VxAccount();

			d_account->setLineCallbacks ( lineCallbacks );	//We need to set LineCallbacks before we create account since we auto register.
			d_account->setAgentCallbacks( agentCallbacks );	//For logging
			configureAccount();

			result = d_account->getId();
		}
		else
		{
			logWarning( "VxAccount has already been created.");
		}
	}
	catch ( pj::Error& error )
	{
		logError( "VxEndpoint::createAccount() failed: " + error.info() );
	}

	return result;
}

int VxEndpoint::makeCall( int lineId, ISipAgent::ICallCallbacks* callbacks, const std::string& uri, bool enableVideo )
{
	return d_account->makeCall( callbacks, uri, enableVideo );
}

bool VxEndpoint::hasAccountId( int lineId )
{
	return d_account->getId() == lineId;
}

bool VxEndpoint::removeLine( int lineId, bool force )		//TODO-PJSIP
{
	//Clear calls for this line.
	return false;
}

void VxEndpoint::clearLines()		//TODO-PJSIP
{
	//Clear all calls.
}

int VxEndpoint::getLineCount()
{
	return 1;
}


//-----------------------------------------------------------------------------
//Call related methods (so API doesn't have to handle it)
//-----------------------------------------------------------------------------
void VxEndpoint::sendRingingNotification( int callId, bool enableVideo )
{
	d_account->sendRingingNotification( callId, enableVideo );
}

void VxEndpoint::acceptCall( int callId, bool enableVideo )
{
	d_account->acceptCall( callId, enableVideo );
}

void VxEndpoint::rejectCall( int callId )
{
	d_account->rejectCall( callId );
}

void VxEndpoint::closeCall( int callId )
{
	d_account->closeCall( callId );
}

void VxEndpoint::holdCall( int callId )
{
	d_account->holdCall( callId );
}

void VxEndpoint::resumeCall( int callId )
{
	d_account->resumeCall( callId );
}

void VxEndpoint::muteCall( int callId, bool muteOn )
{
	d_account->muteCall( callId, muteOn );
}

void VxEndpoint::adjustCallInputVolume( int callId, unsigned int volume )
{
	d_account->adjustCallInputVolume( callId, volume );
}

void VxEndpoint::blindTransferCall( int callId, const std::string& uri  )
{
	d_account->blindTransferCall( callId, uri );
}

void VxEndpoint::playDtmfOnCall( int callId, const std::string& dtmf )
{
	d_account->playDtmfOnCall( callId, dtmf );
}

void VxEndpoint::playSoundFileOnCall( int callId, const std::string& file )
{
	d_account->playSoundFileOnCall( callId, file );
}

bool VxEndpoint::isCallEncrypted( int callId )
{
	return d_account->isCallEncrypted( callId );
}

std::string VxEndpoint::getSipCallId( int callId )
{
	return d_account->getSipCallId( callId );
}

pj::StringVector VxEndpoint::getStunServers()
{
	pj::StringVector result = getServers( ISipAgent::IServerInfo::Type_Stun, false );
	return result;
}

std::string VxEndpoint::getTurnTlsServer()
{
	std::string result;

	pj::StringVector temp = getServers( ISipAgent::IServerInfo::Type_TurnTls, true );

	if ( temp.size() > 0 )
	{
		result = temp[0];
	}

	return result;
}

pj::StringVector VxEndpoint::getServers( ISipAgent::IServerInfo::Type tgtType, bool includePort )
{
	pj::StringVector result;

	ServerInfoList* servers = static_cast<ServerInfoList*>(d_sipConfig->getServers());

	if ( servers )
	{
		for ( ServerInfoList::const_iterator it = servers->begin(); it != servers->end(); it++ )
		{
			if ( (*it).getType() == tgtType )
			{
				std::string temp = (*it).getAddress();

				if ( includePort && (*it).getPort() > 0 )
				{
					temp += ":" + std::to_string( (*it).getPort() );
				}

				result.push_back( temp );
			}
		}
	}

	return result;
}

std::string VxEndpoint::detectHttpProxyForRegistrar()
{
	std::string result;

	ISipAgent::IServerInfo* regServer = d_sipConfig->getServers()->getFirstSipRegistrar();
	ProxyDetect::ProxyInfo proxyInfo;

	if ( regServer )
	{
		ProxyDetect pd( regServer->getAddress().c_str(), regServer->getPort(), &proxyInfo, d_callbackLogger );
		logDebug( "detectHttpProxyForRegistrar: Before ProxyDetect.detect" );
		pd.detect();
		logDebug( "detectHttpProxyForRegistrar: After ProxyDetect.detect" );

		if ( !proxyInfo.address.empty() )
		{
			logDebug( "detectHttpProxyForRegistrar: Before proxyInfo.address" );
			result = proxyInfo.address;
			logDebug( "detectHttpProxyForRegistrar: After proxyInfo.address" );

			if ( proxyInfo.port != 0 )
			{
				logDebug( "detectHttpProxyForRegistrar: Before proxyInfo.port" );
				result += ":" + std::to_string( proxyInfo.port );
				logDebug( "detectHttpProxyForRegistrar: After proxyInfo.port" );
			}
		}
	}

	logDebug( "detectHttpProxyForRegistrar: Before return" );
	return result;
}
