
#include "VxLogger.h"

//=============================================================================

void VxLogger::fatal( const std::string& msg )
{
	write( makeEntry( Level_Fatal, msg ) );
}

void VxLogger::error( const std::string& msg )
{
	write( makeEntry( Level_Error, msg ) );
}

void VxLogger::warning( const std::string& msg )
{
	write( makeEntry( Level_Warning, msg ) );
}

void VxLogger::info( const std::string& msg )
{
	write( makeEntry( Level_Info, msg ) );
}

void VxLogger::debug( const std::string& msg )
{
	write( makeEntry( Level_Debug, msg ) );
}

void VxLogger::verbose( const std::string& msg )
{
	write( makeEntry( Level_Verbose, msg ) );
}

pj::LogEntry VxLogger::makeEntry( LogLevel level, const std::string& msg )
{
	pj::LogEntry result;

	result.level = level;
	result.msg   = msg;
//	result.threadId = ::currentThreadId();

	return result;
}

void VxLogger::write( const pj::LogEntry& entry )
{
	logToMainApp( entry );
}


void VxLogger::logToMainApp( const pj::LogEntry& entry )
{
	std::string msg = cleanMsg( entry.msg );

	//Do callback to main app.
	if ( d_agentCallbacks )
	{
		switch ( entry.level )
		{
			case 1:
				d_agentCallbacks->failure( msg );
				break;

			case 2:
				d_agentCallbacks->error( msg );
				break;

			case 3:
				d_agentCallbacks->warning( msg );
				break;

			case 4:
				d_agentCallbacks->info( msg );
				break;

			case 5:
				d_agentCallbacks->debug( msg );
				break;

			case 6:
				d_agentCallbacks->verbose( msg );
				break;

			default:
				d_agentCallbacks->debug( msg );
		}
	}
}

std::string VxLogger::cleanMsg( const std::string& msgIn )
{
	std::string result = msgIn;

	char*	dataIn = (char*)msgIn.c_str();
	char*	data   = (char*)msgIn.c_str();
	size_t	len    = msgIn.size();

	//Remove trailing crlf
	if ( data[len - 2] == 13 && data[len - 1] == 10 )
	{
		data[len - 2] = 0;
		data[len - 1] = 0;
		len -= 2;
	}
	else if ( data[len - 1] == 10 )
	{
		data[len - 1] = 0;
		len -= 1;
	}

	//'dataIn' has timestamp prepended.  Let's remove that so main app can do its own timestamping.
	//	To ensure we get this correct, check for two colons (as in timestamp 13:13:13.666)
	//	then take all text after following space character.
	int spacePos     = 0;
	int colonCount   = 0;
	int decimalCount = 0;

	for ( size_t x = 0; x < 15; x++ )
	{
		char c = dataIn[x];

		if ( c == ':' )
		{
			colonCount++;
		}
		else if ( c == '.' )
		{
			decimalCount++;
		}
		else if ( c == ' ' )
		{
			//Since we may have 1 or MORE spaces before message begins, let's find the last one.
			while ( dataIn[x] == ' ' && x < len )
			{
				spacePos = x;
				x++;
			}
			break;
		}
	}

	if ( colonCount == 2 && decimalCount == 1 )
	{
		int startPos = spacePos + 1;
		data = (char*) ( &dataIn[startPos] );
		len -= startPos;
	}

	result = std::string( data, len );

	return result;
}

//=============================================================================
