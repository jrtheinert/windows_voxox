
#include "VxAccount.h"
#include "VxCall.h"
#include "VxUtils.h"

#include "../SipAgentApi/StringUtil.h"
#include <assert.h>

VxAccount::VxAccount() : pj::Account()
{
}

int VxAccount::makeCall( ISipAgent::ICallCallbacks* callbacks, const std::string& uri, bool enableVideo )
{
	VxCall*	call = createCall( callbacks );

	pj::CallOpParam parm( true );	//Default settings

	parm.opt.audioCount = 1;
	parm.opt.videoCount = ( enableVideo ? 1 : 0);

	try
	{
		call->makeCall( uri, parm );
	}
	catch( pj::Error& error )
	{
		logError( "makeCall() failed: " + error.info() );
		removeCall( call );
	}

	return call->getId();
}

VxCall* VxAccount::createCall( ISipAgent::ICallCallbacks* callbacks )
{
	VxCall* call = new VxCall( *this, PJSUA_INVALID_ID, callbacks );	//Use PJSUA_INVALID_ID to allow PJSIP to assign callId in a round-robin fashion.

	addCall( call );

	return call;
}

//For all current and future calls.
bool VxAccount::adjustCallInputVolume( unsigned int volume )
{
	bool result = false;

	try
	{
		d_calls.adjustCallInputLevel( volume );

		result = true;
	}
	catch( pj::Error& error )
	{
		logError( "adjustCallInputVolume() failed: " + error.info() );
	}

	return result;
}

void VxAccount::onRegState( pj::OnRegStateParam& parm )
{
	if ( d_lineCallbacks )
	{
		ISipAgent::State  state		= ISipAgent::STATE_ACTIVE;
		ISipAgent::Event  eventId	= ISipAgent::EVENT_OK;
		unsigned int	  status    = 0;
		std::string		  reason	= parm.reason;

		pj_status_t		  pj_status = parm.status;
		pjsip_status_code pj_code	= parm.code;

		switch ( pj_code )
		{
		case PJSIP_SC_OK:
			if ( parm.expiration > 0 )			//Appears to be 0 for Unregistration.
			{
				state   = ISipAgent::STATE_ACTIVE;
				eventId = ISipAgent::EVENT_OK;
				status  = 0;
			}
			else
			{
				state   = ISipAgent::STATE_TERMINATED;
				eventId = ISipAgent::EVENT_TERMINATED;

				switch ( pj_code )
				{
				case PJSIP_SC_REQUEST_TIMEOUT:
					eventId = ISipAgent::EVENT_TIMEOUT;
					reason  = "Request Timeout";
					break;
				}
			}
			break;

		case PJSIP_SC_SERVICE_UNAVAILABLE:
			state   = ISipAgent::STATE_TERMINATED;
			eventId = ISipAgent::EVENT_ERROR;
			reason  = "Service unavailable, likely network loss.";
			break;
		}

		d_lineCallbacks->registrationChanged( state, eventId, status, reason );
	}
}

void VxAccount::onIncomingCall( pj::OnIncomingCallParam& parm )
{
    VxCall* call = new VxCall( *this, parm.callId, d_lineCallbacks->newCallCallback( parm.callId ) );	//Use parm.callId since it is assigned by PJSIP.

	try
	{
		pj::CallInfo ci = call->getInfo();

		logInfo( "CallId: " + std::to_string( call->getId() ) + ", state: " + ci.stateText );

		if ( ci.state == PJSIP_INV_STATE_INCOMING )
		{
			CallStateInfo csi = VxUtils::toCallStateInfo( ci );		//Convert from PJSIP data to ISIpAgent data.

			//JRT - 2016.01.25 - We are getting incoming call from Flip Call to ourselves!
			//	This is a server issue that needs to be fixed, but since we have big demo tomorrow
			//	I am going to try to fix it.
			if ( isFlipFromMe( csi.getRemoteParty() ) )
			{
//				call->decline();		//Reject call, sends it to voicemail, which is not what we want.
				call->busyEveryWhere();	//This seems to work.
			}
			else
			{
				addCall( call );
				call->handleCallStateChange( csi );
			}
		}
		else
		{
			//existing call
			logWarning( "Unexpected callstate on incoming call.");
		}
	}
	catch ( pj::Error& error )
	{
		logError( "onIncomingCall failed: " + error.info() );
	}
}

//-----------------------------------------------------------------------------
//Call related methods, mostly just list handlers
//-----------------------------------------------------------------------------

bool VxAccount::addCall( VxCall* callIn )
{
	return d_calls.add( callIn );
}

bool VxAccount::removeCall( VxCall* callIn )
{
	return d_calls.remove( callIn );
}

VxCall* VxAccount::findCallById( int callId, const std::string& method )
{
	VxCall* result = d_calls.findByCallId( callId );

	if ( result == nullptr )
	{
		logWarning( "findCall() failed for VxAccount::" + method + "() - " + std::to_string( callId ) );
	}

	return result;
}


//-----------------------------------------------------------------------------
//Call related methods (so API doesn't have to handle it)
//-----------------------------------------------------------------------------
void VxAccount::sendRingingNotification( int callId, bool enableVideo )
{
	VxCall* call = findCallById( callId, "sendRingingNotification" );

	if ( call )
	{
		call->setRinging();
	}
}

void VxAccount::acceptCall( int callId, bool enableVideo )
{
	VxCall* call = findCallById( callId, "acceptCall" );

	if ( call )
	{
		call->accept();
	}
}

void VxAccount::rejectCall( int callId )
{
	VxCall* call = findCallById( callId, "rejectCall" );

	if ( call )
	{
		call->decline();
	}
}

void VxAccount::closeCall( int callId )
{
	VxCall* call = findCallById( callId, "closeCall" );

	if ( call )
	{
		call->hangup();
	}
}

void VxAccount::holdCall( int callId )
{
	VxCall* call = findCallById( callId, "holdCall" );

	if ( call )
	{
		call->hold();
	}
}

void VxAccount::resumeCall( int callId )
{
	VxCall* call = findCallById( callId, "resumeCall" );

	if ( call )
	{
		call->resume();
	}
}

void VxAccount::muteCall( int callId, bool muteOn )
{
	VxCall* call = findCallById( callId, "muteCall" );

	if ( call )
	{
		( muteOn ? call->mute() : call->unmute() );
	}
}

void VxAccount::adjustCallInputVolume( int callId, unsigned int volume )
{
	VxCall* call = findCallById( callId, "muteCall" );

	if ( call )
	{
		call->adjustInputVolume( volume );
	}
}

void VxAccount::blindTransferCall( int callId, const std::string& uri  )
{
	VxCall* call = findCallById( callId, "blindTransferCall" );

	if ( call )
	{
		call->transfer( uri );
	}
}

void VxAccount::playDtmfOnCall( int callId, const std::string& dtmf )
{
	VxCall* call = findCallById( callId, "playDtmfOnCall" );

	if ( call )
	{
		call->sendDtmf( dtmf );
	}
}

void VxAccount::playSoundFileOnCall( int callId, const std::string& file )
{
	VxCall* call = findCallById( callId, "playSoundFileOnCall" );

	if ( call )
	{
//		call->playSoundFile( file );		//TODO-PJSIP
	}
}

bool VxAccount::isCallEncrypted( int callId )
{
	bool result = false;
	VxCall* call = findCallById( callId, "isCallEncrypted" );

	if ( call )
	{
		result = call->isCallEncrypted();
	}

	return result;
}

std::string VxAccount::getSipCallId( int callId )
{
	std::string result;
	VxCall* call = findCallById( callId, "getSipCallId" );

	if ( call )
	{
		result = call->getSipCallId();
	}

	return result;
}

void VxAccount::handleInfoRequestReceived( int callId, const std::string& content, const std::string& contentType )
{
	if ( d_lineCallbacks )
	{
		d_lineCallbacks->handleInfoRequestReceived( callId, content, contentType );	
	}
}

bool VxAccount::isFlipFromMe( const std::string& incomingNumber )
{
	bool result = incomingNumberIsMine( incomingNumber );		//Outbound

	if ( !result )
	{
		result = incomingNumberInCallList( incomingNumber );	//Inbound
	}

	return result;
}

//TODO: This method is a HACK to get through a big demo.
bool VxAccount::incomingNumberIsMine( const std::string& incomingNumber )
{
	bool result = false;

	const std::string SIP = "<sip:";

	std::string from	 = parseIncomingNumber( incomingNumber );
	std::string myNumber = getMyNumber();

	if ( !from.empty() && !myNumber.empty() )
	{
		result = (from == myNumber);
	}

	return result;
}

//TODO: This method is a HACK to get through a big demo.
bool VxAccount::incomingNumberInCallList( const std::string& incomingNumber )
{
	bool result = false;

//	std::string from = parseIncomingNumber( incomingNumber );
	std::string from = incomingNumber;

	if ( !from.empty() )
	{
		VxCall* call = d_calls.findByFrom( from );

		result = (call != nullptr);
	}

	return result;
}

std::string VxAccount::parseIncomingNumber( const std::string& incomingNumber )
{
	const std::string SIP = "<sip:";

	std::string result;

	size_t endPos = incomingNumber.find( SIP );
	
	if ( endPos != std::string::npos )
	{
		result = incomingNumber.substr( 0, endPos );

		result = StringUtil::replace( result, "\"", "", false );	//Replace double-quotes
		result = StringUtil::replace( result, " ",  "", false );	//Replace spaces
		result = StringUtil::replace( result, "+",  "", false );	//Replace +
	}

	return result;
}

std::string VxAccount::getMyNumber()
{
	const std::string SIP = "<sip:";

	std::string result;

	pj::AccountInfo info = getInfo();

	//Get Account URI from this: "jrt1@tcoff.net" <sip:19508966062@8.38.43.130:443>  This uses our SIP userId 19508966062
	//	Result should be jrt1@tcoff.net.
	size_t endPos = info.uri.find( SIP );

	if ( endPos != std::string::npos )
	{
		result = info.uri.substr( 0, endPos );

		result = StringUtil::replace( result, "\"", "", false );	//Replace double-quotes
		result = StringUtil::replace( result, " ",  "", false );	//Replace spaces
		result = StringUtil::replace( result, "+",  "", false );	//Replace +
	}

	return result;
}

//-----------------------------------------------------------------------------
// Logging callbacks
//-----------------------------------------------------------------------------

void VxAccount::logError( const std::string& msg )
{
	if ( d_agentCallbacks )
		d_agentCallbacks->error( msg );
}

void VxAccount::logWarning( const std::string& msg )
{
	if ( d_agentCallbacks )
		d_agentCallbacks->warning( msg );
}

void VxAccount::logInfo( const std::string& msg )
{
	if ( d_agentCallbacks )
		d_agentCallbacks->info( msg );
}
