
#include "VxAgent.h"
#include "VxUtils.h"

#include "../SipAgentApi/SipConfig.h"

#include <assert.h>

#ifdef WIN32
#   ifndef WIN32_LEAN_AND_MEAN
#       define WIN32_LEAN_AND_MEAN
#   endif
#   include <windows.h>
#   include <qos.h>
#endif

#include "../Shared/ExportFunc.h"

#define PJMEDIA_HAS_OPUS_CODEC 1
#include "pj_sources/pj_opus.h"
#include "rewriters/pjsip_opus_sdp_rewriter.h"

//This is INFO-level by default and can be controlled via API call from main app.
//  This is used if App passes -1.
#define LOG_LEVEL 4

#define THIS_FILE			"VxAgent.cpp"
#define USER_AGENT			"Voxox Windows"
#define USER_AGENT_VERSION	"0.0.1"


//These next couple items are used in pjsipLine::init()

/* Notification on incoming request
* Handle requests which are unhandled by pjsua, eg. incoming
* PUBLISH, NOTIFY w/o SUBSCRIBE, ...
*/
//static pj_bool_t default_mod_on_rx_request( pjsip_rx_data *rdata )
//{
//	pjsip_tx_data *tdata;
//	pjsip_status_code status_code;
//	pj_status_t status;
//
//	/* Don't respond to ACK! */
//	if ( pjsip_method_cmp( &rdata->msg_info.msg->line.req.method, &pjsip_ack_method ) == 0 )
//		return PJ_TRUE;
//
//	/* Create basic response. */
//	if ( pjsip_method_cmp( &rdata->msg_info.msg->line.req.method, &pjsip_notify_method ) == 0 )
//	{
//		/* Unsolicited NOTIFY's, send with Bad Request */
//		status_code = PJSIP_SC_BAD_REQUEST;
//	}
//	else
//	{
//		/* Probably unknown method */
//		status_code = PJSIP_SC_METHOD_NOT_ALLOWED;
//	}
//	status = pjsip_endpt_create_response( pjsua_get_pjsip_endpt(), rdata, status_code, NULL, &tdata );
//	if ( status != PJ_SUCCESS )
//	{
//		std::cerr << "Unable to create response, status: " << status << std::endl;		//TODO: Get this to normal logging.
//		return PJ_TRUE;
//	}
//
//	/* Add Allow if we're responding with 405 */
//	if ( status_code == PJSIP_SC_METHOD_NOT_ALLOWED )
//	{
//		const pjsip_hdr *cap_hdr;
//		cap_hdr = pjsip_endpt_get_capability( pjsua_get_pjsip_endpt(), PJSIP_H_ALLOW, NULL );
//		if ( cap_hdr )
//		{
//			pjsip_msg_add_hdr( tdata->msg, (pjsip_hdr*) pjsip_hdr_clone( tdata->pool, cap_hdr ) );
//		}
//	}
//
//	/* Add User-Agent header */
//	{
//		pj_str_t user_agent;
//		char tmp[255];
//		const pj_str_t USER_AGENT_HDR = { "User-Agent", 10 };
//		pjsip_hdr *h;
//
//		pj_ansi_snprintf( tmp, sizeof( tmp ), USER_AGENT " " USER_AGENT_VERSION " (pjproject %s/%s)", pj_get_version(), PJ_OS_NAME );
//		pj_strdup2_with_null( tdata->pool, &user_agent, tmp );
////		pj_strdup2_with_null( tdata->pool, d_sipConfig->getUserAgent().c_str() );	//TODO-PJSIP: use actual userAgent string
//
//		h = (pjsip_hdr*) pjsip_generic_string_hdr_create( tdata->pool, &USER_AGENT_HDR, &user_agent );
//		pjsip_msg_add_hdr( tdata->msg, h );
//	}
//
//	pjsip_endpt_send_response2( pjsua_get_pjsip_endpt(), rdata, tdata, NULL, NULL );
//
//	return PJ_TRUE;
//}
//
//static pjsip_module mod_default_handler =
//{
//	NULL, NULL,								/* prev, next.		*/
//	{ "mod-default-handler", 19 },			/* Name.			*/
//	-1,										/* Id				*/
//	PJSIP_MOD_PRIORITY_APPLICATION + 99,	/* Priority	        */
//	NULL,									/* load()			*/
//	NULL,									/* start()			*/
//	NULL,									/* stop()			*/
//	NULL,									/* unload()			*/
//	default_mod_on_rx_request,				/* on_rx_request()	*/
//	NULL,									/* on_rx_response()	*/
//	NULL,									/* on_tx_request.	*/
//	NULL,									/* on_tx_response()	*/
//	NULL,									/* on_tsx_state()	*/
//
//};

//=============================================================================

VxAgent::VxAgent()
	:	d_pjsipOn(false)

	,	d_sipConfig( nullptr )
	,	d_agentCallbacks( nullptr )
{
}

VxAgent::~VxAgent() throw()
{
    shutdown();
}

//This should:
//	- initialize audio/video support
//	- re-order available codecs as desired (may also be done elsewhere)
//	- initialize 'session'?
//	- start service?
//	- initialize UUID string (may be done elsewhere)
ISipAgent::Result VxAgent::init( ISipAgent::IAgentCallbacks* callbacks )
{
    d_agentCallbacks = callbacks;

	determineLogLevel();

	if ( d_pjsipOn )
	{
		logWarning( "PJSIP stack already initialized." );
		return ISipAgent::RESULT_OK;
	}

	return d_endpoint.initialize( d_sipConfig, d_agentCallbacks ) ? ISipAgent::RESULT_OK : ISipAgent::RESULT_FAILURE;
}

void VxAgent::determineLogLevel()
{
	int logLevelIn = d_sipConfig->getLogLevel();
	int logLevel   = ( logLevelIn < 0 ? LOG_LEVEL : logLevelIn );

	if ( logLevel < 1 || logLevel > 6 )
		logLevel = LOG_LEVEL;

	d_sipConfig->setLogLevel( logLevel );
}

void VxAgent::shutdown() throw()
{
	d_endpoint.shutdown();
	d_pjsipOn = false;
}

ISipAgent::Result VxAgent::configure( SipConfig* sipConfig )
{
	d_sipConfig = sipConfig;

	return ISipAgent::RESULT_OK;
}

ISipAgent::Result VxAgent::setNS(bool enable) throw()
{
    LockGuard guard(&d_lock);

	//TODO-PJSIP: Enable NoiseSuppression
    return ISipAgent::RESULT_FAILURE;
}

ISipAgent::Result VxAgent::setExperimentalNS(bool enable) throw()
{
    LockGuard guard(&d_lock);
	//TODO-PJSIP: Enable Experimental NoiseSuppression.  Is this needed?
    return ISipAgent::RESULT_FAILURE;
}

ISipAgent::Result VxAgent::setAGC(bool enable) throw()
{
    LockGuard guard(&d_lock);

	//TODO-PJSIP: Enable AGC

	return ISipAgent::RESULT_FAILURE;
}

ISipAgent::Result VxAgent::setExperimentalAGC(bool enable) throw()
{
    LockGuard guard(&d_lock);

	//TODO-PJSIP: Enable Experimental AGC.  Is this needed?

	return ISipAgent::RESULT_FAILURE;
}

ISipAgent::Result VxAgent::setEC(bool enable) throw()
{
    LockGuard guard(&d_lock);

	//TODO-PJSIP: Enable AEC

	return ISipAgent::RESULT_FAILURE;
}

ISipAgent::Result VxAgent::setDelayCorrection(bool enable) throw()
{
    LockGuard guard(&d_lock);

	//TODO-PJSIP: Enable Delay Correction.  Is this needed?

    return ISipAgent::RESULT_FAILURE;
}

ISipAgent::Result VxAgent::setHighPassFilter(bool enable) throw()
{
    LockGuard guard(&d_lock);
	
	//TODO-PJSIP: High Pass Filter.  Is this needed?

	return ISipAgent::RESULT_FAILURE;
}

//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
//Begin Audio Device related methods
//-----------------------------------------------------------------------------

ISipAgent::Result VxAgent::setRecordingDevice( const std::string& guid ) throw()
{
    LockGuard guard(&d_lock);

	return d_endpoint.setAudioInputDevice( guid ) ? ISipAgent::RESULT_OK : ISipAgent::RESULT_FAILURE;
}

ISipAgent::Result VxAgent::setPlayoutDevice( const std::string& guid ) throw()	//TODO-PJSIP: may need to use Agent
{
    LockGuard guard(&d_lock);

	return d_endpoint.setAudioOutputDevice( guid ) ? ISipAgent::RESULT_OK : ISipAgent::RESULT_FAILURE;
}

ISipAgent::Result VxAgent::setRingingDevice( const std::string& guid ) throw()	//TODO-PJSIP: Do we support separate ringing device?
{
    LockGuard guard(&d_lock);

	return ISipAgent::RESULT_FAILURE;
}

ISipAgent::Result VxAgent::setSpeakerVolume(unsigned int volume) throw()
{
    LockGuard guard(&d_lock);

	ISipAgent::Result result = d_endpoint.setAudioOutputLevel( volume ) ? ISipAgent::RESULT_OK : ISipAgent::RESULT_FAILURE;
	
	return result;
}

ISipAgent::Result VxAgent::getSpeakerVolume(unsigned int* volume) throw()	//TODO-PJSIP: may need to use Agent
{
    LockGuard guard(&d_lock);

	ISipAgent::Result result = ISipAgent::RESULT_OK;

	if ( volume != nullptr )
	{
		try		//TODO-PJSIP: Move 'try' to d_endpoint
		{
			*volume = d_endpoint.getAudioOutputLevel();
		}
		catch ( pj::Error& error )
		{
			logWarning( "getSpeakerVolume failed: " + error.info() );
			result = ISipAgent::RESULT_FAILURE;
		}
	}

	return result;
}

ISipAgent::Result VxAgent::setRingingVolume(unsigned int volume) throw()	//Do not use this for PJSIP.  Appears RINGER does not have separate volume.
{
    LockGuard guard(&d_lock);

	return ISipAgent::RESULT_FAILURE;
}

ISipAgent::Result VxAgent::getRingingVolume(unsigned int* volume) throw()	//Do not use this for PJSIP.  Appears RINGER does not have separate volume.
{
    LockGuard guard(&d_lock);

//	assert( false );

	//if ( volume != nullptr )
	//{
	//	*volume = d_media.getRingingVolume();
	//	return ISipAgent::RESULT_OK;
	//}

	return ISipAgent::RESULT_FAILURE;
}

ISipAgent::Result VxAgent::setMicVolume(unsigned int volume) throw()
{
    LockGuard guard(&d_lock);

	ISipAgent::Result result = d_endpoint.setAudioInputLevel( volume ) ? ISipAgent::RESULT_OK : ISipAgent::RESULT_FAILURE;

	return result;
}

ISipAgent::Result VxAgent::getMicVolume(unsigned int* volume) throw()	//TODO-PJSIP: may need to use Agent
{
    LockGuard guard(&d_lock);

	ISipAgent::Result result = ISipAgent::RESULT_OK;

	if ( volume != nullptr )
	{
		try
		{
			*volume = d_endpoint.getAudioInputLevel();
		}
		catch ( pj::Error& error )
		{
			logWarning( "getMicVolume failed: " + error.info() );
			result = ISipAgent::RESULT_FAILURE;
		}
	}
	
	return result;
}

ISipAgent::Result VxAgent::getSpeechInputLevelFullRange( unsigned int* level ) throw()	//TODO-PJSIP: 
{
    LockGuard guard(&d_lock);

	ISipAgent::Result result = ISipAgent::RESULT_OK;
	if ( level != nullptr )
	{
		try
		{
			return ISipAgent::RESULT_FAILURE;
		}
		catch( ... )
		{
//			warning( "getSpeechInputLevelFullRange failed.");
			result = ISipAgent::RESULT_FAILURE;
		}
	}

	return result;
}

ISipAgent::Result VxAgent::startPlayRingingFile( const char* name, bool loop ) throw()	// TODO-PJSIP:
{
    return ISipAgent::RESULT_FAILURE;
}

ISipAgent::Result VxAgent::stopPlayRingingFile() throw()	// TODO-PJSIP:
{
    return ISipAgent::RESULT_FAILURE;
}


//-----------------------------------------------------------------------------
// Other methods
//-----------------------------------------------------------------------------

int VxAgent::createLine( ISipAgent::ILineCallbacks* callbacks ) throw()
{
	int lineId = -1;

	try
    {
		lineId = d_endpoint.createAccount( callbacks, d_agentCallbacks );
    }
    catch( pj::Error& error )
    {
        logError( "VxAgent::createLine failed: " + error.info() );
    }

    return lineId;
}


void VxAgent::logFailure()
{
	if(d_agentCallbacks)
		d_agentCallbacks->failure();
}

void VxAgent::logFailure( const std::string& msgIn )
{
	std::string msg = prependFileName( msgIn );
    
	if(d_agentCallbacks)
		d_agentCallbacks->failure( msg );
}

void VxAgent::logError( const std::string& msgIn )
{
	std::string msg = prependFileName( msgIn );
    
	if(d_agentCallbacks)
		d_agentCallbacks->error( msg );
}

void VxAgent::logWarning( const std::string& msgIn )
{
	std::string msg = prependFileName( msgIn );
    
	if(d_agentCallbacks)
		d_agentCallbacks->warning( msg );
}

void VxAgent::logInfo( const std::string& msgIn )
{
	std::string msg = prependFileName( msgIn );
    
	if(d_agentCallbacks)
		d_agentCallbacks->info( msg );
}

void VxAgent::logDebug( const std::string& msgIn )
{
	std::string msg = prependFileName( msgIn );
    
	if(d_agentCallbacks)
		d_agentCallbacks->debug( msg );
}

void VxAgent::logVerbose( const std::string& msgIn )
{
	std::string msg = prependFileName( msgIn );
    
	if(d_agentCallbacks)
		d_agentCallbacks->verbose( msg );
}

std::string VxAgent::prependFileName( const std::string& msgIn )
{
	std::string result = THIS_FILE + std::string( "  " ) + msgIn;

	return result;
}


//-----------------------------------------------------------------------------
//Line/Account related methods (so API doesn't have to handle it).
//-----------------------------------------------------------------------------

int VxAgent::makeCall( int lineId, ISipAgent::ICallCallbacks* callbacks, const std::string& uri, bool enableVideo )
{
	return d_endpoint.makeCall( lineId, callbacks, uri, enableVideo );
}

bool VxAgent::hasLineId( int lineId )
{
	return d_endpoint.hasAccountId( lineId );
}

bool VxAgent::connectLine( int lineId )
{
	return true;
}

bool VxAgent::populateLine( int lineId )
{
	return true;
}

bool VxAgent::registerLine( int lineId )
{
	return true;
}

bool VxAgent::removeLine( int lineId, bool force )
{
	//Clear calls for this line.
	return true;
}

void VxAgent::clearLines()
{
	//Clear all calls.
	d_endpoint.clearLines();
}

int VxAgent::getLineCount()
{
	return d_endpoint.getLineCount();
}

//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
//Call related methods (so API doesn't have to handle it)
//-----------------------------------------------------------------------------

void VxAgent::sendRingingNotification( int callId, bool enableVideo )
{
	d_endpoint.sendRingingNotification( callId, enableVideo );
}

void VxAgent::acceptCall( int callId, bool enableVideo )
{
	d_endpoint.acceptCall( callId, enableVideo );
}

void VxAgent::rejectCall( int callId )
{
	d_endpoint.rejectCall( callId );
}

void VxAgent::closeCall( int callId )		//TODO-PJSIP
{
	d_endpoint.closeCall( callId );
}

void VxAgent::holdCall( int callId )		//TODO-PJSIP
{
	d_endpoint.holdCall( callId );
}

void VxAgent::resumeCall( int callId )		//TODO-PJSIP
{
	d_endpoint.resumeCall( callId );
}

void VxAgent::muteCall( int callId, bool muteOn )
{
	d_endpoint.muteCall( callId, muteOn );
}

void VxAgent::setCallInputVolume( int callId, unsigned int volume )
{
	d_endpoint.adjustCallInputVolume( callId, volume );
}

void VxAgent::blindTransferCall( int callId, const std::string& uri  )
{
	d_endpoint.blindTransferCall( callId, uri );
}

void VxAgent::playDtmfOnCall( int callId, const std::string& dtmf )		//TODO-PJSIP
{
	d_endpoint.playDtmfOnCall( callId, dtmf );
}

void VxAgent::playSoundFileOnCall( int callId, const std::string& file )		//TODO-PJSIP
{
	d_endpoint.playSoundFileOnCall( callId, file );
}

bool VxAgent::isCallEncrypted( int callId )		//TODO-PJSIP
{
	bool result = d_endpoint.isCallEncrypted( callId );
	return result;
}

std::string VxAgent::getSipCallId( int callId )
{
	std::string result = d_endpoint.getSipCallId( callId );

	return result;
}

//-----------------------------------------------------------------------------


/*********************************** sipAgentCreate ************************************************/
EXPORT_FUNC ISipAgent::Result sipAgentCreate( ISipAgent::Agent** agent, ISipAgent::IAgentCallbacks*	callbacks, SipConfig* sipConfig )
{
    *agent = nullptr;

    VxAgent* result = new VxAgent;

    if ( result == nullptr )
	{
		callbacks->error( "SIP Agent: Cannot allocate SIP User Agent." );
		return  ISipAgent::RESULT_FAILURE;
	}

	result->configure( sipConfig );
    result->init(callbacks);

    *agent = result;

    return  ISipAgent::RESULT_OK;
}
