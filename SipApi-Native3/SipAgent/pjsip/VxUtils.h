
#pragma once

#include <pjsua2.hpp>							//pj::CallInfo
#include "../SipAgentAPI/ISipAgent.h"			//ISipAgent::CallState
#include "../SipAgentAPI/CallStateInfo.h"		//CallStateInfo

#include <string>

class VxUtils
{
public:
	static std::string			convertFromWideString( const std::wstring& wide   );
	static std::wstring			convertToWideString  ( const std::string&  narrow );

	static bool					ensureThreadIsRegistered( const std::string& method );

	static ISipAgent::CallState toSipAgentCallState( pjsip_inv_state stateIn, bool& isValid );
	static  pjsip_ssl_method	fromSipAgentTlsMethod( ISipAgent::TLSMethod methodIn );

	static CallStateInfo		toCallStateInfo( const pj::CallInfo& callInfo );

//	static std::string			left( const std::string& textIn, const std::string& delim );
//	static std::string			removeLF( const CStr& str ) throw();
//	static std::string			removeLF( const char* str, size_t size ) throw();
};
