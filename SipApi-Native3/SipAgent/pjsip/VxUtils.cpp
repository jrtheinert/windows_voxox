
#include "vxUtils.h"

#include <locale>
#include <codecvt>
#include <string>

std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;


//To/From wide-string
std::string VxUtils::convertFromWideString( const std::wstring& wide )
{
	std::string result = converter.to_bytes( wide );
	return result;
}


std::wstring VxUtils::convertToWideString( const std::string& narrow )
{
	std::wstring result = converter.from_bytes( narrow );
	return result;
}

//A helper method for PJSIP thread registration.
bool VxUtils::ensureThreadIsRegistered( const std::string& method )
{
	pj_thread_desc aPJThreadDesc;
	bool result = true;		//Assume already registered.

	if ( !pj_thread_is_registered() )
	{
		pj_thread_t *pjThread;
		pj_status_t status = pj_thread_register( NULL, aPJThreadDesc, &pjThread );

		//NOTE that if this check fails, then we will most likely get an exception from PJSIP in calling method.
		if ( status != PJ_SUCCESS )
		{
			std::string msg = "Error registering thread at " + method + ".  Status: " + std::to_string( status );
//			std::cerr << msg << std::endl;	//TODO: Log error or just pass back to caller

			result = false;
		}
	}

	return result;
}

//Convert PJSIP CallState to our API CallState
ISipAgent::CallState VxUtils::toSipAgentCallState( pjsip_inv_state stateIn, bool& isValid )
{
	isValid = true;
	ISipAgent::CallState result = ISipAgent::CALL_STATE_ERROR;

	switch ( stateIn )
	{
		case PJSIP_INV_STATE_NULL:				/**<  Before INVITE is sent or received	    */
			result = ISipAgent::CALL_STATE_IDLE;
			break;

		case PJSIP_INV_STATE_CALLING:			/**< After INVITE is sent		    */
			result = ISipAgent::CALL_STATE_DIALING;
			break;

		case PJSIP_INV_STATE_INCOMING:			/**< After INVITE is received.	    */
			result = ISipAgent::CALL_STATE_INCOMING;
			break;

		case PJSIP_INV_STATE_EARLY:				/**< After response with To tag.	    */
			result = ISipAgent::CALL_STATE_EARLYMEDIA;
			break;

		case PJSIP_INV_STATE_CONNECTING:	    /**< After 2xx is sent/received. */
			result = ISipAgent::CALL_STATE_INPROGRESS;
			break;

		case PJSIP_INV_STATE_CONFIRMED:			/**< After ACK is sent/received.	 */
			result = ISipAgent::CALL_STATE_CONNECTED;
			break;

		case PJSIP_INV_STATE_DISCONNECTED:		/**< Session is terminated.		 */
			result = ISipAgent::CALL_STATE_DISCONNECTED;
			break;
		default:
			isValid = false;		//New PJSIP callstate?
	}

	return result;
}

//Convert our API TlsMethod to PJSIP TlsMethod
pjsip_ssl_method VxUtils::fromSipAgentTlsMethod( ISipAgent::TLSMethod methodIn )
{
	pjsip_ssl_method result = PJSIP_SSL_UNSPECIFIED_METHOD;

	switch ( methodIn )
	{
		case ISipAgent::TLS_TLSv1:
			result = PJSIP_TLSV1_METHOD;
			break;
		case ISipAgent::TLS_TLSv1_1:
			result = PJSIP_TLSV1_1_METHOD;
			break;
		case ISipAgent::TLS_TLSv1_2:
			result = PJSIP_TLSV1_2_METHOD;
			break;
		case ISipAgent::TLS_SSLv2:
			result = PJSIP_SSLV2_METHOD;
			break;
		case ISipAgent::TLS_SSLv23:
			result = PJSIP_SSLV23_METHOD;
			break;
		case ISipAgent::TLS_SSLv3:
			result = PJSIP_SSLV3_METHOD;
			break;
		default:
			result = PJSIP_SSL_UNSPECIFIED_METHOD;
			break;
	}

	return result;
}

CallStateInfo VxUtils::toCallStateInfo( const pj::CallInfo& ci )
{
	bool isValidState = false;

	CallStateInfo csi;

	csi.setLineId   ( ci.accId  );		//It appears accId is same as LineId, but need to verify.
	csi.setCallId   ( ci.id     );
	csi.setStatus   ( ci.lastStatusCode );
	csi.setReason   ( ci.lastReason		);
	csi.setUserData ( pjsua_call_get_user_data( ci.id ) );

	csi.setCallState( VxUtils::toSipAgentCallState( ci.state, isValidState ) );

	csi.setRemoteContact( ci.remoteContact );
	csi.setRemoteParty  ( ci.remoteUri     );	//TODO-PJSIP: Verify

	if ( ! isValidState )
	{
		//TODO: Throw exception here, or elsewhere.
	}

	return csi;
}
