#ifndef TELURIX_SHARED_LOCK_H
#define TELURIX_SHARED_LOCK_H
/*****************************************************************************************************/
/*                                                                                                   */
/* Lock.h                                                                                            */
/* Lock -- a simple lock syncronization class and LockGurad autolock utlity class                    */
/* (c) 2005-2011 Telurix LLC.                                                                        */
/*                                                                                                   */
/*****************************************************************************************************/

class LockData;

class Lock
{
private:
    LockData* d_data;
public:
    Lock();
    Lock(const Lock &other) throw();
    Lock& operator=(const Lock &other) throw();
    ~Lock() throw();
    void lock() throw();
    void unlock() throw();
};

class LockGuard
{
private:
    Lock* d_lock;
    bool  d_locked;

    // not implemented
    LockGuard(const LockGuard& );
    void operator=(const LockGuard& );
public:
    LockGuard(Lock* lock) throw()
    : d_lock(lock)
    , d_locked(false)
    {
        if(d_lock)
        {
            d_lock->lock();
            d_locked = true;
        }
    }

    ~LockGuard() throw()
    {
        if(d_locked)
            d_lock->unlock();
    }

    void lock() throw()
    {
        if(!d_locked && d_lock)
        {
            d_lock->lock();
            d_locked = true;
        }
    }

    void unlock() throw()
    {
        if(d_locked)
        {
            d_lock->unlock();
            d_locked = false;
        }
    }

    bool locked() const throw()
    {
        return d_locked;
    }
};

#endif // TELURIX_SHARED_LOCK_H


