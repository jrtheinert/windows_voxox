#ifndef TELURIX_SHARED_INTERLOCKED_H
#define TELURIX_SHARED_INTERLOCKED_H
/*****************************************************************************************************/
/*                                                                                                   */
/* Interlocked.h                                                                                     */
/* Implementation of Interlocked operations and a Bucket event queue class                           */
/* (c) 2005-2011 Telurix LLC.                                                                        */
/*                                                                                                   */
/*****************************************************************************************************/

//#define NO_INLINE_INTERLOCKED

#ifdef NO_INLINE_INTERLOCKED
#   ifndef TELURIX_SHARED_LOCK_H 
#       include "Lock.h"
#   endif
#endif

namespace Interlocked
{
#ifdef NO_INLINE_INTERLOCKED

long add(long *addend, long value)  throw();
void voidAdd(long *addend, long value)  throw();
long increment(long *addend)  throw();
void voidIncrement(long *addend)  throw();
long decrement(long *addend) throw();
void voidDecrement(long *addend) throw();
void* compareExchange(void** dest, void* exchange, void* comperand) throw();
long compareExchange(long* dest, long exchange, long comperand) throw();
void* exchange(void** dest, void* exchange) throw();
long exchange(long* dest, long exchange) throw();


/* Same as stack, but no pop, just flush. Does not need to deal with ABA problem */
template <class _Node>
class Bucket
{
private:
    _Node*          d_first;
    Lock            d_lock;

public:
    Bucket() throw()
    : d_first(0)
    {}

    _Node* push(_Node* node) throw()
    {
        LockGuard guard(&d_lock);
        node->next = d_first;
        _Node* oldFirst = d_first;
        d_first = node;
        return oldFirst;
    }

    _Node* flush() throw()
    {
        LockGuard guard(&d_lock);
        _Node* tmp = d_first;
        d_first = 0;
        return tmp;
    }

    _Node* flushReverse() throw()
    {
        _Node* tmp = flush();

        // reverese
        _Node* result = 0;
        while(tmp)
        {
            _Node* node = tmp;
            tmp = tmp->next;
            node->next = result;
            result = node;
        }
        return result;
    }
};

#else // NO_INLINE_INTERLOCKED

#ifdef _MSC_VER

#pragma warning(push)
#pragma warning(disable:4035)

inline long add(long *addend, long value)  throw()
{
    __asm
    {
        mov ecx, addend
        mov eax, value
        lock xadd dword ptr [ecx], eax
        add eax, value
    } 
}

inline void voidAdd(long *addend, long value)  throw()
{
    __asm
    {
        mov ecx, addend
        mov eax, value
        lock add dword ptr [ecx], eax
    } 
}

inline long increment(long *addend)  throw()
{
    __asm
    {
        mov ecx, addend
        mov eax, 1
        lock xadd dword ptr [ecx], eax
        inc eax
    } 
}

inline void voidIncrement(long *addend)  throw()
{
    __asm
    {
        mov eax, addend
        lock add dword ptr [eax], 1
    } 
}

inline long decrement(long *addend) throw()
{
    __asm
    {
        mov ecx, addend
        mov eax, 0ffffffffh
        lock xadd dword ptr [ecx], eax
        dec eax
    } 
}

inline void voidDecrement(long *addend) throw()
{
    __asm
    {
        mov eax, addend
        lock add dword ptr [eax], 0ffffffffh
    } 
}

inline void* compareExchange(void** dest, void* exchange, void* comperand) throw()
{
    __asm
    {
        mov ecx, dest
        mov edx, exchange
        mov eax, comperand
        lock cmpxchg dword ptr [ecx], edx
    } 
}

inline long compareExchange(long* dest, long exchange, long comperand) throw()
{
    __asm
    {
        mov ecx, dest
        mov edx, exchange
        mov eax, comperand
        lock cmpxchg dword ptr [ecx], edx
    } 
}

inline void* exchange(void** dest, void* exchange) throw()
{
    __asm
    {
        mov ebx, dest
        mov eax, exchange
        xchg eax, dword ptr [ebx]
    } 
}

inline long exchange(long* dest, long exchange) throw()
{
    __asm
    {
        mov ebx, dest
        mov eax, exchange
        xchg eax, dword ptr [ebx]
    } 
}


/* Same as stack, but no pop, just flush. Does not need to deal with ABA problem */
template <class _Node>
class Bucket
{
private:
    _Node*          d_first;

public:
    Bucket() throw()
    : d_first(0)
    {}

    _Node* push(_Node* node) throw()
    {
        __asm
        {
            mov             ebx, node
            mov             ecx, this
            mov             eax, dword ptr [ecx]this.d_first
        BucketPushLoop:
            mov             dword ptr [ebx]node.next, eax 
            lock cmpxchg    dword ptr [ecx]this.d_first, ebx
            jne             BucketPushLoop
        } 
    }

    _Node* flush() throw()
    {
        __asm
        {
            mov     ecx, this
            xor     eax, eax
            xchg    eax, dword ptr [ecx]this.d_first
        } 
    }

    _Node* flushReverse() throw()
    {
        _Node* tmp = flush();

        // reverese
        _Node* result = 0;
        while(tmp)
        {
            _Node* node = tmp;
            tmp = tmp->next;
            node->next = result;
            result = node;
        }
        return result;
    }
};

#pragma warning(pop)

#endif // ifdef _MSC_VER

#if (defined(__i386__) || defined(__x86_64__)) && defined(__GNUC__)

inline long add
(
    long*   addend,
    long    value
)  throw()
{
    register long result = value;
    asm volatile
    (
        "lock; xaddl %0,%1"
        : "=r"(result), "=m"(*addend)   /* outputs */
        : "0"(result),  "m"(*addend)    /* inputs */
        : "cc"
    );
    return (result + value);
}

inline void voidAdd
(
    long*   addend,
    long    value
)  throw()
{
    asm volatile
    (
        "lock; addl %1,%0"
        : "=m"(*addend)                 /* outputs */
        : "ir"(value),  "m"(*addend)    /* inputs */
        : "cc"
    );
}

inline long increment(long *addend)  throw()
{
    register long result = 1;
    asm volatile
    (
        "lock; xaddl %0,%1"
        : "=r"(result), "=m"(*addend)   /* outputs */
        : "0"(result),  "m"(*addend)    /* inputs */
        : "cc"
    );
    return (result + 1);
}

inline void voidIncrement(long *addend)  throw()
{
    asm volatile
    (
        "lock; addl $1,%0"
        : "=m"(*addend) /* outputs */
        : "m"(*addend)  /* inputs */
        : "cc"
    );
}

inline long decrement(long *addend) throw()
{
    register long result = -1;
    asm volatile
    (
        "lock; xaddl %0,%1"
        : "=r"(result), "=m"(*addend)   /* outputs */
        : "0"(result),  "m"(*addend)    /* inputs */
        : "cc"
    );
    return (result - 1);
}


inline void voidDecrement(long *addend) throw()
{
    asm volatile
    (
        "lock; subl $1,%0"
        : "=m"(*addend) /* outputs */
        : "m"(*addend)  /* inputs */
        : "cc"
    );
}

inline void* compareExchange
(
    void**  dest,
    void*   exchange,
    void*   comperand
) throw()
{
    void* result;
    asm volatile
    (
        "lock; cmpxchgl %2, %1"
        : "=a"(result), "=m"(*dest)                 /* outputs */
        : "r"(exchange), "m"(*dest), "0"(comperand) /* inputs */
        : "cc"
    );
    return result;
}

inline long compareExchange
(
    long*   dest,
    long    exchange,
    long    comperand
) throw()
{
    long result;
    asm volatile
    (
        "lock; cmpxchgl %2, %1"
        : "=a"(result), "=m"(*dest)                 /* outputs */
        : "r"(exchange), "m"(*dest), "0"(comperand) /* inputs */
        : "cc"
    );
    return result;
}

// Note: xchg does not need lock
inline void* exchange
(
    void**  dest,
    void*   exchange
) throw()
{
    asm volatile
    (
        "xchgl %0, %1"
        : "=r"(exchange), "=m"(*dest)   /* outputs */
        : "m" (*dest), "0"(exchange)    /* inputs */
    );    
    return exchange;
}

// Note: xchg does not need lock
inline long exchange
(
    long*   dest,
    long    exchange
) throw()
{
    asm volatile
    (
        "xchgl %0, %1"
        : "=r"(exchange), "=m"(*dest)   /* outputs */
        : "m" (*dest), "0"(exchange)    /* inputs */
    );    
    return exchange;
}

/* Same as stack, but no pop, just flush. Does not need to deal with ABA problem */
template <class _Node>
class Bucket
{
private:
    _Node* d_first;

public:
    Bucket() throw()
    : d_first(0)
    {}
    
    _Node* push(_Node* node) throw()
    {
        _Node* oldFirst;
        asm volatile
        (
            "movl %1, %0\n\t"
            "0:\n\t"
            "movl %0, %2\n\t"
            "lock; cmpxchgl %3, %1\n\t"
            "jne 0b\n\t"
            : "=&a"(oldFirst),   "=m"(d_first), "=m"(node->next)    /* outputs */
            : "r"(node)                                             /* inputs */
            : "cc"
        );
        return oldFirst;
    }
    
    _Node* flush() throw()
    {
        register _Node* result = 0;
        asm volatile
        (
            "xchgl %0, %1"
            : "=r"(result),     "=m"(d_first)   /* outputs */
            : "m" (d_first),   "0"(result)      /* inputs */
        );    
        return result;
    }

    _Node* flushReverse() throw()
    {
        _Node* tmp = flush();

        // reverese
        _Node* result = 0;
        while(tmp)
        {
            _Node* node = tmp;
            tmp = tmp->next;
            node->next = result;
            result = node;
        }
        return result;
    }
};

#endif // if (defined(__i386__) || defined(__x86_64__)) && defined(__GNUC__)

#endif // NO_INLINE_INTERLOCKED
} // namespace Interlocked

#endif // TELURIX_SHARED_INTERLOCKED_H

