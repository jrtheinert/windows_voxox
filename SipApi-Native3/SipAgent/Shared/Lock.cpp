/*****************************************************************************************************/
/*                                                                                                   */
/* Lock.cpp                                                                                          */
/* Lock -- a simple lock syncronization class and LockGurad autolock utlity class                    */
/* (c) 2005-2011 Telurix LLC.                                                                        */
/*                                                                                                   */
/*****************************************************************************************************/
#ifdef _WIN32

#ifndef WIN32_LEAN_AND_MEAN
#   define WIN32_LEAN_AND_MEAN
#endif
#include <windows.h>

#include "Interlocked.h"
#include "Lock.h"

// this is a dummy class which is really a structure
class LockData
{
public:
    CRITICAL_SECTION criticalSection;
    long refCount;
};

Lock::Lock()
{
    d_data = new LockData;

	InitializeCriticalSection(&(d_data->criticalSection));

    d_data->refCount = 1;
}

Lock::Lock(const Lock &other) throw()
:   d_data(other.d_data)
{
    Interlocked::voidIncrement(&(d_data->refCount));
}

Lock& Lock::operator=(const Lock &other) throw()
{
    if(d_data != other.d_data)
    {
        if (Interlocked::decrement(&(d_data->refCount)) == 0)
        {
            DeleteCriticalSection(&(d_data->criticalSection));
            delete d_data;
        }
        d_data  = other.d_data;
        Interlocked::voidIncrement(&(d_data->refCount));
    }
    return *this;
}

Lock::~Lock() throw()
{
    if (Interlocked::decrement(&(d_data->refCount)) == 0)
    {
        DeleteCriticalSection(&(d_data->criticalSection));
        delete d_data;
    }
}

void Lock::lock() throw()
{
    EnterCriticalSection(&(d_data->criticalSection));
}

void Lock::unlock() throw()
{
    LeaveCriticalSection(&(d_data->criticalSection));
}

#endif
