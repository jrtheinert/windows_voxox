#ifndef TELURIX_SHARED_EXPORTFUNC_H
/*****************************************************************************************************/
/*                                                                                                   */
/* ExportFunc.h                                                                                      */
/* Define EXPORT_FUNC macro                                                                          */
/* (c) 2006-2011 Telurix LLC                                                                         */
/*                                                                                                   */
/*****************************************************************************************************/ 
#ifdef _WIN32
#   define EXPORT_FUNC extern "C" __declspec( dllexport )
#else
#   define EXPORT_FUNC extern "C" __attribute__ ((visibility("default")))
#endif
 
#endif // TELURIX_SHARED_EXPORTFUNC_H

