/*****************************************************************************************************/
/*                                                                                                   */
/* Interlocked.cpp                                                                                   */
/* Implementation of Interlocked operations and a Bucket event queue class                           */
/* (c) 2005-2011 Telurix LLC.                                                                        */
/*                                                                                                   */
/*****************************************************************************************************/
#include "Interlocked.h"

#ifdef NO_INLINE_INTERLOCKED

#ifdef _WIN32

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

namespace Interlocked
{
long add(long *addend, long value)  throw()
{
    return InterlockedExchangeAdd(addend, value);
}

void voidAdd(long *addend, long value)  throw()
{
    InterlockedExchangeAdd(addend, value);
}

long increment(long *addend)  throw()
{
    return InterlockedIncrement(addend);
}

void voidIncrement(long *addend)  throw()
{
    InterlockedIncrement(addend);
}

long decrement(long *addend) throw()
{
    return InterlockedDecrement(addend);
}

void voidDecrement(long *addend) throw()
{
    InterlockedDecrement(addend);
}

void* compareExchange(void** dest, void* exchange, void* comperand) throw()
{
    return InterlockedCompareExchange(dest, exchange, comperand);
}

long compareExchange(long* dest, long exchange, long comperand) throw()
{
    return (long)InterlockedCompareExchange((void**)dest, (void*)exchange, (void*)comperand);
}

void* exchange(void** dest, void* exchange) throw()
{
    return (void*)InterlockedExchange((long*)dest, (long)exchange);
}

long exchange(long* dest, long exchange) throw()
{
    return InterlockedExchange(dest, exchange);
}

}

#else

namespace Interlocked
{

#if (defined(__i386__) || defined(__x86_64__)) && defined(__GNUC__)

long add
(
    long*   addend,
    long    value
)  throw()
{
    register long result = value;
    asm volatile
    (
        "lock; xaddl %0,%1"
        : "=r"(result), "=m"(*addend)   /* outputs */
        : "0"(result),  "m"(*addend)    /* inputs */
        : "cc"
    );
    return (result + value);
}

void voidAdd
(
    long*   addend,
    long    value
)  throw()
{
    asm volatile
    (
        "lock; addl %1,%0"
        : "=m"(*addend)                 /* outputs */
        : "ir"(value),  "m"(*addend)    /* inputs */
        : "cc"
    );
}

long increment(long *addend)  throw()
{
    register long result = 1;
    asm volatile
    (
        "lock; xaddl %0,%1"
        : "=r"(result), "=m"(*addend)   /* outputs */
        : "0"(result),  "m"(*addend)    /* inputs */
        : "cc"
    );
    return (result + 1);
}

void voidIncrement(long *addend)  throw()
{
    asm volatile
    (
        "lock; addl $1,%0"
        : "=m"(*addend) /* outputs */
        : "m"(*addend)  /* inputs */
        : "cc"
    );
}

long decrement(long *addend) throw()
{
    register long result = -1;
    asm volatile
    (
        "lock; xaddl %0,%1"
        : "=r"(result), "=m"(*addend)   /* outputs */
        : "0"(result),  "m"(*addend)    /* inputs */
        : "cc"
    );
    return (result - 1);
}


void voidDecrement(long *addend) throw()
{
    asm volatile
    (
        "lock; subl $1,%0"
        : "=m"(*addend) /* outputs */
        : "m"(*addend)  /* inputs */
        : "cc"
    );
}

void* compareExchange
(
    void**  dest,
    void*   exchange,
    void*   comperand
) throw()
{
    void* result;
    asm volatile
    (
        "lock; cmpxchgl %2, %1"
        : "=a"(result), "=m"(*dest)                 /* outputs */
        : "r"(exchange), "m"(*dest), "0"(comperand) /* inputs */
        : "cc"
    );
    return result;
}

long compareExchange
(
    long*   dest,
    long    exchange,
    long    comperand
) throw()
{
    long result;
    asm volatile
    (
        "lock; cmpxchgl %2, %1"
        : "=a"(result), "=m"(*dest)                 /* outputs */
        : "r"(exchange), "m"(*dest), "0"(comperand) /* inputs */
        : "cc"
    );
    return result;
}

// Note: xchg does not need lock
void* exchange
(
    void**  dest,
    void*   exchange
) throw()
{
    asm volatile
    (
        "xchgl %0, %1"
        : "=r"(exchange), "=m"(*dest)   /* outputs */
        : "m" (*dest), "0"(exchange)    /* inputs */
    );
    return exchange;
}

// Note: xchg does not need lock
long exchange
(
    long*   dest,
    long    exchange
) throw()
{
    asm volatile
    (
        "xchgl %0, %1"
        : "=r"(exchange), "=m"(*dest)   /* outputs */
        : "m" (*dest), "0"(exchange)    /* inputs */
    );
    return exchange;
}
#endif

}
#endif

#endif


