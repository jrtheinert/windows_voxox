Jeff Theinert
2014.12.08

Native Crash Handling
------------------------------

1. Typically handled via exception handlers:
   a.  AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(handleException);
   b.  Application.ThreadException += new ThreadExceptionEventHandler( handleThreadException );

2. Since app is in unknown state, we typically just log the exception, which includes the call stack.


Advanced Crash Reporting
-------------------------

1. In order to track the crashes, some data must be uploaded to some server.
   Historically, Voxox did this internally, but there are now vendors that
   provide this service and provide decent grouping, reporting and management
   for very nominal fees (or even free).

2. Key requirements are:
   - Automatic generation and upload of dump files
   - Automatic upload of additional data, such as application log files
   - Grouping by app version
   - Grouping by crash cause
   - Uploading of symbol files (.pdb) for easier debugging.

3. I reviewed HockeyApp and BugSplat (later discovered RayGun, but have not reviewed).
   HockeyApp sends crash info on NEXT login, while BugSplat sends it immediately.
   For this key reason, we chose to go with BugSplat.

4. Implementing BugSplat was pretty straight-forward.  However, the following lessons apply:
   - Because some unhandled exceptions have 'inner exceptions', BugSplat dump files (and
     likely those of other vendors) do NOT always display actually offending lines.
     This is addressed by using the native C# unhandled exception handler to log the 
     exception to our application log, and then uploading the app log along with the 
     dump file when a crash occurs.

   - Note that for this to work, we must set the native C# handler before the BugSplat handler.
     I am not sure this applies to other crash reporting services.

   - I have not yet tested this with native C++ DLLs.  Will that soon.

5. Comments on my implementation
   - I implemented all Crash Reporting and Handling in the new CrashReport class.
     This will keep the main code clean and allow for greater comments within the code.

   - In my experience, it is helpful to know some state info when a crash occurs.
     Specifically, I like to know if there is crash:
     - BEFORE user logs in
     - AFTER user logs out
     - AFTER user quits the app
     - WHILE user is logged in (this is where most crashes will occur, we hope)

   - To achieve this in BugSplat implementation, I write specific text strings
     to a couple data elements that BugSplat collects.  These strings will make
     it obvious when the crash occurred and will allow easy grouping.

   - BugSplat offers a quiet mode which does not prompt the user for any info.
     After discussing this with Curtis, we chose to go with the quiet mode because:
     -- BugSplat 'send' executable would have to be modified/branded and that takes time.
     -- Historically, user almost never add comments, almost nothing will be lost there.
     -- Mobile platforms do not ask for permission.  It is typically covered by the EULA.

   - I modified the Release build for FULL debug, vs. PDB_ONLY
     This provides line numbers in BugSplat, and adds about 700 KB to app size

   - Moved the small amount of code in Desktop.Util.UnhandledExceptionHandler.cs
     to CrashReporter.cs and remove UnhandledExceptionHandler.cs from project.

   - It appears that ANY exception from our native C++ libs will have the same StackKey
     in BugSplat.  This would make reviewing them more difficult since they all appear to
     be the same.  My solution (at least partial):
 	- The complete exception is in the log file which is uploaded to BugSplat, but
	  that is too long be useful in BugSplat interface.
	- So I strip out all but the method names from the call stack and add those to 
	  User Description in BugSplat.  This yields a string like this:
		VoxoxXmppApi.sendChat:Telcentris.Voxox.ManagedXmppApi.XmppApi.sendChat	:Telcentris.Voxox.ManagedXmppApi.XmppApi.sendChat:Desktop.Model.Messaging.XmppManager.sendChat:Desktop.Model.Messaging.MessageManager.sendChatRequest:Desktop.Model.Messaging.MessageManager.doSendMessage:Desktop.Model.Messaging.MessageManager.sendMessage:Desktop.ViewModels.Messages.MessagesViewModel.OnSend
	- This allows use to search by User Description and then (hopefully) tag all with same 'ID'
 	  Need to talk to BugSplat about this.


At this point, I am working under my own BugSplat account.  We need to get a Voxox BugSplat account.
 - Account is 'jrtheinert@tsasg.com'  pw 'jrtjrt'
 - Login URL is: https://www.bugsplatsoftware.com/Login/
   -- I provide this because oddly, if you go to BugSplat home page, there is NO login option.  ;-(


Observations about BugSplat:
 - After crash occurs:
   -- It takes 30-40 seconds to upload data.
   -- Once uploaded, it takes another 60 seconds for new crash to show
   -- Once crash shows (as Pending), it takes another 60 seconds to see the details.
   -- These number are NOT important, unless you are testing BugSplat.


