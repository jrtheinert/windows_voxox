﻿using Vxapi23.Base;

using System;

namespace Vxapi23.Model
{
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public class AuthenticateResponse : BaseApiResponse
    {
        #region | Private Class Variables |

        private bool	mBanned;
        private int		mBalance;
		private string	mMpAlias;
		private string	mCompanyId;

        #endregion

        #region | Public Properties |

        public string voxox_id					{ get; set; }
        public string companyUserId				{ get; set; }
        public string companyName				{ get; set; }
        public string firstName					{ get; set; }
        public string lastName					{ get; set; }
        public object smsNumber					{ get; set; }
        public string partnerId					{ get; set; }
        public string email						{ get; set; }
        public string signupCountry				{ get; set; }
//		banned - handled below
        public string hasValidated				{ get; set; }
        public string smsAttempts				{ get; set; }
        public string userkey					{ get; set; }
        public string key						{ get; set; }
//		mpAlias - handled below
        public string loginEnabled				{ get; set; }
        public string isValidationRequired		{ get; set; }
//		balance - handled below
        public string cid						{ get; set; }
        public string did						{ get; set; }
        public string sipIP						{ get; set; }
        public string sipUsername				{ get; set; }
        public string sipPassword				{ get; set; }
        public string sipServer					{ get; set; }
        public string packetReflectorIp			{ get; set; }
        public string packetReflectorPortBase	{ get; set; }
        public string packetReflectorPortCount	{ get; set; }
        public string turnIP					{ get; set; }
        public string turnServer				{ get; set; }
        public string turnUsername				{ get; set; }
        public string turnPassword				{ get; set; }
        public string xmppUsername				{ get; set; }
        public string xmppPassword				{ get; set; }

		//Because mMpAlias *may* be null/empty and the handling of that is to use companyId, let's set mMpAlias if it is null.
        public string companyId 
		{ 
			get { return mCompanyId; }
			set 
			{
				mCompanyId = value;
				mMpAlias = String.IsNullOrEmpty( mMpAlias ) ? companyId : mMpAlias;
			}
		}

		public string mpAlias 
		{ 
			get { return mMpAlias; }
			set { mMpAlias = String.IsNullOrEmpty( value ) ? companyId : value; }
		}

        public bool banned
        {
            get { return mBanned; }
            set { mBanned = SetNullableBool(value, false); }
        }

        //TODO: This should be float/double.  I know the JSON string shows an int, but that only
        //	because the value is an even dollar amount, e.g. 10.00.  Once we start billing these will change.
        //	Not sure of impact of rest of app, so I did not make this simple change.
        public int balance
        {
            get { return mBalance; }
            set { mBalance = SetNullableInt(value, 0); }
        }

        #endregion
    }
}
