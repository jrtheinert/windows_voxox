﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vxapi23.Model
{
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]        
    public class RichTextMediaResponse
    {
        public MediaResponseData voxoxrichmessage { get; set; }
    }

    public class MediaResponseData
    {
        public MediaData data { get; set; }
        public string type { get; set; }
        public string body { get; set; }
    }
    public class MediaData
    {
        public string file { get; set; }
        public string thumbnail { get; set; }
    }
}
