﻿using Vxapi23.Base;

namespace Vxapi23.Model
{
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public class PlanInfoResponse
    {
        public string planId { get; set; }
        public string resellerId { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string i_subscription { get; set; }
        public string i_vd_plan { get; set; }
        public string i_product { get; set; }
        public string openingBalance { get; set; }
        public string maxUsersOld { get; set; }
        public string tier { get; set; }
        public string isDebit { get; set; }
        public string isDefault { get; set; }
        public string active { get; set; }
        public string categoryId { get; set; }
        public string productCatalogId { get; set; }
        public string modified { get; set; }
        public string created { get; set; }
        public string resellerCode { get; set; }
        public string i_parent { get; set; }
        public string i_customer_class { get; set; }
        public string promoCode { get; set; }
        public string categoryName { get; set; }
        public Subscription subscription { get; set; }
        public Product product { get; set; }
        public string adminRoleId { get; set; }
        public string userRoleId { get; set; }
        public Discount discount { get; set; }
        public Invoice invoice { get; set; }
    }

    public class Subscription : BaseApiResponse
    {
        #region | Private Class Variables |

        public int mI_customer_subscription { get; set; }
        public int mInt_status { get; set; }

        #endregion

        #region | Public Properties |

        public string i_subscription { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string invoice_description { get; set; }
        public object activation_fee { get; set; }
        public string fee { get; set; }
        public string start_date { get; set; }
        public string activation_date { get; set; }
        public string is_finished { get; set; }

        public int i_customer_subscription
        {
            get { return mI_customer_subscription; }
            set { mI_customer_subscription = SetNullableInt(value, 0); }
        }

        public int int_status
        {
            get { return mInt_status; }
            set { mInt_status = SetNullableInt(value, 0); }
        }

        #endregion
    }

    public class Product
    {
        public string i_product { get; set; }
        public string name { get; set; }
        public string iso_4217 { get; set; }
        public string maintenance_fee { get; set; }
        public string maintenance_period { get; set; }
        public object breakage { get; set; }
        public string i_env { get; set; }
        public object i_customer { get; set; }
        public string description { get; set; }
        public string info_url { get; set; }
        public object validation_module { get; set; }
        public object continue_url { get; set; }
        public string subscription_http_referer { get; set; }
        public string default_i_acl { get; set; }
        public string i_vd_plan { get; set; }
        public object subscription_host { get; set; }
        public object maintenance_effective_from { get; set; }
        public string hidden { get; set; }
        public string service_flags { get; set; }
        public string fraud_protection { get; set; }
        public string overage { get; set; }
    }

    public class Discount
    {
        public string i_vd_plan { get; set; }
        public string i_env { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string i_dest_group_set { get; set; }
        public string currency { get; set; }
        public object i_customer { get; set; }
        public string reset_period { get; set; }
        public string destination_lookup { get; set; }
        public string i_vd_dg { get; set; }
        public string i_dest_group { get; set; }
        public string threshold_type { get; set; }
        public string exclusive { get; set; }
        public string i_service { get; set; }
        public string peak_level { get; set; }
        public string max_peak_level { get; set; }
        public string i_vd_threshold { get; set; }
        public string threshold { get; set; }
        public string discount { get; set; }
        public object alter_service { get; set; }
        public string notify_if_exceeded { get; set; }
        public string xdr_split { get; set; }
    }

    public class Invoice : BaseApiResponse
    {
        private int mBalance;

        public int balance
        {
            get { return mBalance; }
            set { mBalance = SetNullableInt(value, 0); }
        }

        public string nextInvoiceDate { get; set; }
    }
}
