﻿namespace Vxapi23.Model
{
    public class CompanyUserExtendedResponse
    {
        public string companyId				{ get; set; }
        public string i_customer			{ get; set; }
        public string company_i_account		{ get; set; }
        public string company_did			{ get; set; }	//Dialable phone number
        public string companyName			{ get; set; }
        public string companyUserId			{ get; set; }
        public string user_i_account		{ get; set; }
        public string user_did				{ get; set; }	//Not dialable, internal use only.
        public string enabled				{ get; set; }
        public string statusId				{ get; set; }
        public string abbreviatedDialingId	{ get; set; }
        public string dialedNumber			{ get; set; }	//CloudPhone Extension.
        public string forwardTo				{ get; set; }	//Same value as 'user_did' above
        public string i_acl_user			{ get; set; }
        public string i_acl_user_parent		{ get; set; }
        public string partnerId				{ get; set; }
		public string partnerIdentifier		{ get; set; }	//This should already be in app because it is used for branding.
        public string login					{ get; set; }
        public string mobileNumber			{ get; set; }	//From account registration
        public string firstName				{ get; set; }
        public string lastName				{ get; set; }
        public string email					{ get; set; }
        public string roleId				{ get; set; }
        public string extensionGroupId		{ get; set; }
        public string sipGroupId			{ get; set; }
        public string followMeGroupId		{ get; set; }
        public string roleName				{ get; set; }
        public string password				{ get; set; }
        public string pin					{ get; set; }

        //TODO: need datatypes\definitions of the following from vxapi 
//		public object mobileNumber			{ get; set; }
//		public object forwardedDialingId	{ get; set; }
//		public object forwardedCompanyUserId { get; set; }
//		public object forwardedFirst		{ get; set; }
//		public object forwardedLast			{ get; set; }
    }
}



