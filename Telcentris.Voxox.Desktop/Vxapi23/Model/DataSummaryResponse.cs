﻿using System.Collections.Generic;
using Vxapi23.Base;

namespace Vxapi23.Model
{
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public class DataSummaryResponse : BaseApiResponse
    {
        #region | Private Class Variables |

        private float mBalance { get; set; }

        #endregion

        #region | Public Properties |

		//These may change if SIP server is changed server side, like may happen during a SIP outage.
		//	We need to notify model layer to redo SIP registration.
        public string sipIP { get; set; }
        public string sipUsername { get; set; }
        public string sipPassword { get; set; }
        public string sipServer { get; set; }

		//This user info will not change, EXCEPT the userKey may change if user changes password.
		//	We need to notify model layer (SessionManager) so proper userKey is used for future API calls
        public string username { get; set; }
        public string userkey { get; set; }
        public string userId { get; set; }
        public string did { get; set; }
        public string cid { get; set; }

		//TODO: This is not currently used, but if is used, some accounts we get 'null'.
		//	In those cases, we should use 'CompanyId', which we do NOT get in this response,
		//	so it will have to be handled externally, likely in User class.
        public string mpAlias { get; set; }		

		//These should be stored in SessionManager for use as needed.
		//	They may be updated via portal or other clients
		//"findmeOn":true,
		//"findmeNumbers":
		//{
		//	"deviceSettings":{"enabled":"1","timeout":30},
		//	"reachMeNumbers":[],
		//	"routeType":"6"
		//}

        public string findmeOn { get; set; }			//TODO: This is boolean
        public FindmeNumbers findmeNumbers { get; set; }
        
		//Updated when ANY balance change event occurs (off-net call, SMS, Fax, purchase credit)
        public float balance
        {
            get { return mBalance; }
            set { mBalance = SetNullableFloat(value, 0); }
        }

		//Badge counts - These ARE same values we would get using the betBadges() API call.
		//	These values should be passed to UI so displayed badge counts can be updated.
		//"counts":
		//{
		//	"voicemail":2,
		//	"fax":0,
		//	"recorded":0,
		//	"sms":3,
		//	"xmpp":1,
		//	"missed_calls":"96"  <<<<<<<<<<<<<<<<<<<<<<<<< Should be 'int' not string.
		//},
		public BadgeCounts	counts { get; set;	}

        // Caller IDs are returned in the following format
        //"callerIds": {
        //    "callerId0": {
        //        "companyAccountId": "XXXXX",
        //        "i_account": "XXXXXX",
        //        "number": "1XXXXXXXXXX",
        //        "name": "My Number"
        //    },
        //    "callerId1": {
        //        "companyAccountId": "YYYY",
        //        "i_account": "YYYYY",
        //        "number": "1YYYYYYYYYY",
        //        "name": "Home"
        //    }
        //}
        public Dictionary<string, CallerID> callerIds { get; set; }

        #endregion

    }

    public class BadgeCounts
	{
		private int mVoicemail	= 0;
		private int mFax		= 0;
		private int mSms		= 0;
		private int mXmpp		= 0;
		private int mRecorded	= 0;
		private int mMissedCall = 0;

		public int Voicemail	
		{ 
			get { return mVoicemail;		}
			set { mVoicemail = BaseApiResponse.SetNullableInt(value, 0); }
		}

		public int Fax	
		{ 
			get { return mFax;		}
			set { mFax = BaseApiResponse.SetNullableInt(value, 0); }
		}

		public int Sms	
		{ 
			get { return mSms;		}
			set { mSms = BaseApiResponse.SetNullableInt(value, 0); }
		}

		public int Xmpp	
		{ 
			get { return mXmpp;		}
			set { mXmpp = BaseApiResponse.SetNullableInt(value, 0); }
		}

		public int Recorded	
		{ 
			get { return mRecorded;		}
			set { mRecorded = BaseApiResponse.SetNullableInt(value, 0); }
		}

//		[System.Xml.Serialization.DeserializeAs(Name = "missed_call")]	//Does not work. TODO: Download updated RestSharp lib
		public int Missed_Calls
		{ 
			get { return mMissedCall;		}
			set { mMissedCall = BaseApiResponse.SetNullableInt(value, 0); }
		}
	}

    public class FindmeNumbers
    {
        public FindmeNumbers()
        {
            ReachMeNumbers = new List<ReachMeNumber>();
        }

        public List<ReachMeNumber> ReachMeNumbers { get; set; }
    }

    public class ReachMeNumber
    {
        private int mEnabled = 0;
        private int mTimeout = 0;
        private int mPriority = 0;

        public int Enabled
        {
            get { return mEnabled; }
            set { mEnabled = BaseApiResponse.SetNullableInt(value, 0); }
        }

        public int Timeout
        {
            get { return mTimeout; }
            set { mTimeout = BaseApiResponse.SetNullableInt(value, 0); }
        }

        public int Priority
        {
            get { return mPriority; }
            set { mPriority = BaseApiResponse.SetNullableInt(value, 0); }
        }

        public string Label { get; set; }

        public string OffnetNumber { get; set; }
    }

    public class CallerID
    {
        public string CompanyAccountId { get; set; }
        public string IAccount { get; set; }
        public string Number { get; set; }
        public string Name { get; set; }
    }
}
