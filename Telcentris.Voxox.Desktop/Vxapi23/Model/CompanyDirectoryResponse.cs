﻿using Vxapi23.Base;

using System;
using System.Collections.Generic;

namespace Vxapi23.Model
{
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public class CompanyDirectoryResponse : BaseApiResponse
    {
        #region | Public Properties |

		public List<DirectoryEntry> entries { get; set; }

		//JRT - 2015.10.23 - It appears we do NOT need most of this, so I am commenting those that we do not need
		//		mostly for clarity of what we do need.  We can easily uncomment them later.
		//	Chris Flora is making changes so we will need to update this shortly
		public class DirectoryEntry
		{ 
			public string companyUserId				{ get; set; }	//":"172",			INT?
			public string companyId					{ get; set; }	//":"164",			INT?	<Relates all entries in company directory>
	//		public string i_acl_user				{ get; set; }	//":"66",			INT?
	//		public string i_account					{ get; set; }	//":"164794",		INT?
	//		public string extensionGroupId			{ get; set; }	//":"396",			INT?
	//		public string sipGroupId				{ get; set; }	//":"397",			INT?
	//		public string followMeGroupId			{ get; set; }	//":"398",			INT?
			public string companyUserDid			{ get; set; }	//":"19502007092",
			public string enabled					{ get; set; }	//":"1",			INT?
			public string statusId					{ get; set; }	//":"4",			INT?
	//		public string roleId					{ get; set; }	//":"105",			INT?
	//		public string abbreviatedDialingId		{ get; set; }	//":"90",			INT?
			public string dialedNumber				{ get; set; }	//":"101",		<Extension>
			public string forwardTo					{ get; set; }	//":"19502007092",
	//		public string i_acl_user_parent			{ get; set; }	//":"0",			INT?
			public string login						{ get; set; }	//":"+17605551111",		<Login by Mobile>
			public string firstName					{ get; set; }	//":"jrt1-cp",
			public string lastName					{ get; set; }	//":"T",
			public string email						{ get; set; }	//":"jrt1@tcoff.net",
			public string mobileNumber				{ get; set; }	//":"+17605551111",
	//		public string smsCode					{ get; set; }	//":"4859",
	//		public string partnerId					{ get; set; }	//":"1",			INT?
	//		public string statusName				{ get; set; }	//":"Standard",
	//		public string statusCode				{ get; set; }	//":"STANDARD",
	//		public string roleName					{ get; set; }	//":null,		<Null?>
	//		public string forwardedFirst			{ get; set; }	//":null,
	//		public string forwardedLast				{ get; set; }	//":null,
	//		public string forwardedDialingId		{ get; set; }	//":null,
	//		public string forwardedCompanyUserId	{ get; set; }	//":null,
	//		public string company_i_account			{ get; set; }	//":"164794"		INT?

			//These are new fields we expect with upcoming change
			public string jabberId					{ get; set; }
			public string extension					{ get; set; }
			public string companyName				{ get; set; }
		}

        #endregion
    }
}
