﻿using System;
using Vxapi23.Base;

namespace Vxapi23.Model
{
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public class ContactResponse : BaseApiResponse
    {
        #region | Private Class Variables |

        private int mContactId { get; set; }
        private int mCompanyUserId { get; set; }
        private int mInvited { get; set; }
        private DateTime mDateInvited { get; set; }
        private int mFavorited { get; set; }
        private int mBlacklisted { get; set; }
        private DateTime mLastSynced { get; set; }
        private DateTime mCreated { get; set; }
        private DateTime mModified { get; set; }
        private int mDeleted { get; set; }
        private int mContactCompanyUserId { get; set; }

        #endregion

        #region | Public Properties |

        public string contactFirstName { get; set; }
        public string contactLastName { get; set; }
        public string contactCompany { get; set; }
        public string contactNumber { get; set; }
        public string label { get; set; }

        public int contactId
        {
            get { return mContactId; }
            set { mContactId = SetNullableInt(value, 0); }
        }

        public int companyUserId
        {
            get { return mCompanyUserId; }
            set { mCompanyUserId = SetNullableInt(value, 0); }
        }

        public int invited
        {
            get { return mInvited; }
            set { mInvited = SetNullableInt(value, 0); }
        }

        public DateTime dateInvited
        {
            get { return mDateInvited; }
            set { mDateInvited = SetNullableDateTime(value, DateTime.Now); }
        }

        public int favorited
        {
            get { return mFavorited; }
            set { mFavorited = SetNullableInt(value, 0); }
        }

        public int blacklisted
        {
            get { return mBlacklisted; }
            set { mBlacklisted = SetNullableInt(value, 0); }
        }

        public DateTime lastSynced
        {
            get { return mLastSynced; }
            set { mLastSynced = SetNullableDateTime(value, DateTime.Now); }
        }

        public DateTime created
        {
            get { return mCreated; }
            set { mCreated = SetNullableDateTime(value, DateTime.Now); }
        }

        public DateTime modified
        {
            get { return mModified; }
            set { mModified = SetNullableDateTime(value, DateTime.Now); }
        }

        public int deleted
        {
            get { return mDeleted; }
            set { mDeleted = SetNullableInt(value, 0); }
        }

        public int contactCompanyUserId
        {
            get { return mContactCompanyUserId; }
            set { mContactCompanyUserId = SetNullableInt(value, 0); }
        }

        #endregion
    }
}
