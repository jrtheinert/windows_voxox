﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vxapi23.Model
{
    public class DeletedMessagesToServer
    {
        // These names should be in sync with server
        public string messageId { get; set; }
        public int messageType { get; set; }
        public int status { get; set; }

        public DeletedMessagesToServer(string Id, int msgStatus, int type)
        {
            messageId = Id;
            messageType = type;
            status = msgStatus;
        }
    }
}
