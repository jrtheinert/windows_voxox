﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vxapi23.Model
{
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]        
    public class RichTextLocationResponse
    {
        public LocationResponseData voxoxrichmessage { get; set; }
    }

    public class LocationResponseData
    {
        public LocationData data { get; set; }
        public string type { get; set; }
        public string body { get; set; }
    }
    public class LocationData
    {
        public object longitude { get; set; }
        public object latitude { get; set; }
    }


}
