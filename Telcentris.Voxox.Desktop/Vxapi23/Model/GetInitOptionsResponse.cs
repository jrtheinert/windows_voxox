﻿using Vxapi23.Base;

using System;
using System.Collections.Generic;

namespace Vxapi23.Model
{
    public class GetInitOptionsResponse : BaseApiResponse
    {
        public List<SignUpInfo> Signup { get; set; }
        public List<LoginInfo>  Login  { get; set; }

		public bool HasSignup() { return Signup.Count > 0; }	//TODO: Will this ever be > 1?
		public bool HasLogin()  { return Login.Count  > 0; }	//TODO: Assume we use hard-coded defaults if this is zero?

		public LoginInfo GetNativeLogin()
		{
			LoginInfo result = null;

			foreach ( LoginInfo li in Login )
			{
				if ( li.TypeIsNative() )
				{
					result = li;
					break;
				}
			}

			return result;
		}

		public LoginInfo GetSsoLogin()
		{
			LoginInfo result = null;

			foreach ( LoginInfo li in Login )
			{
				if ( li.TypeIsSso() )
				{
					result = li;
					break;
				}
			}

			return result;
		}
    }

    public class SignUpInfo
    {
        public string type				{ get; set; }
        public string signupUrl			{ get; set; }

		public bool TypeIsInApp()		{ return type == "inAppSignup"; }
    }

	public class LoginInfo
    {
        public string	  type			{ get; set; }
        public string	  providerCode	{ get; set; }
        public string	  loginUrl		{ get; set; }
        public string	  logoUrl		{ get; set; }
        public LoginParms loginParams	{ get; set; }

		public bool TypeIsNative()		{ return type == "nativeLogin"; }
		public bool TypeIsSso()			{ return type == "ssoLogin";	}
    }

	public class LoginParms
	{
		public string url				{ get; set; }
		public int	  client_id			{ get; set; }
		public string response_type		{ get; set; }
		public string state				{ get; set; }
	}
}
