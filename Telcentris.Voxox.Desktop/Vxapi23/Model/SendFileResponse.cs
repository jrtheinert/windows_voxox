﻿using Vxapi23.Base;

namespace Vxapi23.Model
{
    public class SendFileResponse : BaseApiResponse
    {
        public string file { get; set; }
    }
}
