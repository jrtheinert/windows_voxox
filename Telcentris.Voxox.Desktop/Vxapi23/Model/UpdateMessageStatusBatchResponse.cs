﻿using Vxapi23.Base;

using System.Collections.Generic;

namespace Vxapi23.Model
{
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public class UpdateMessageStatusBatchResponse : BaseApiResponse
    {
        public Dictionary<string, UpdatedMessage> Messages { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public class UpdatedMessage
    {
        public bool updated		{ get; set; }
        public int messageType	{ get; set; }
        public int messageId	{ get; set; }
    }
}
