﻿using Vxapi23.Base;

using System.Collections.Generic;

namespace Vxapi23.Model
{
    public class GetFindMeNumbersResponse : BaseApiResponse
    {
		private string mRouteType;

		public DeviceSettings		deviceSettings	{ get; set;	}
		public List<ReachMeNumber>	reachMeNumbers	{ get; set; }
		public string				routeType			
		{ 
			get	{ return mRouteType; }
			set { mRouteType = value; }
		}
	
		public class DeviceSettings
		{
			public string enabled	{ get; set; }
			public int    timeout	{ get; set; }

			//Some convernsions
			public bool	isEnabled()		{ return stringToBool( enabled ); }
		}

		public class ReachMeNumber
		{
			public string	enabled			{ get; set; }
			public int		timeout			{ get; set; }
			public string	label			{ get; set; }
			public int		priority		{ get; set; }
			public string	offnetNumber	{ get; set; }

			//Some convernsions
			public bool	isEnabled()		{ return stringToBool( enabled ); }
		}

    }
}
