﻿using System;
using Vxapi23.Base;

namespace Vxapi23.Model
{
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public class SMSResponse : BaseApiResponse
    {
        #region | Private Class Variables |

//        private bool mBanned;
//        private int mBalance;

        private DateTime mTimestamp;
        private bool mPushDataSummary;

        #endregion

        #region | Public Properties |

        public string messageId { get; set; }

        public DateTime timestamp
        {
            get { return mTimestamp; }
            set { mTimestamp = SetNullableDateTime(value, DateTime.Now); }
        }

        public bool pushDataSummary
        {
            get { return mPushDataSummary; }
            set { mPushDataSummary = SetNullableBool(value, false); }
        }

        #endregion
    }
}
