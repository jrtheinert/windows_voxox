﻿using Vxapi23.Base;

namespace Vxapi23.Model
{
    public class CallHistoryResponse : BaseApiResponse
    {
        #region | Private Class Variables |

        private long mStartEpoch;
        private long mStopEpoch;
        private int mDuration;
		private int mOutbound;
		private int mMissed;
		private int mBlocked;
		private int mSeen;

        #endregion

        #region | Public Properties |

        public string	uid				{ get; set; }
        public int		companyId		{ get; set; }	//'int' in doc
        public int		companyUserId	{ get; set; }	//'int' in doc
        public string	to				{ get; set; }
        public string	from			{ get; set; }
        public string	callTime		{ get; set; }
        public string	dialedNumber	{ get; set; }	//TODO: Not in current doc
		public string   toName			{ get; set; }

		//Some boolean conversions
		public bool isInbound()			{ return outbound == 0;	}
		public bool isOutbound()		{ return !isInbound();	}

		public bool isMissed()			{  return missed  != 0;	}
		public bool isBlocked()			{  return blocked != 0;	}
		public bool isSeen()			{  return seen    != 0;	}

		public int outbound		//'int' in doc		
		{ 
			get { return mOutbound; }
			set { mOutbound = SetNullableInt( value, 0 ); }
		}

		public int missed		//'int' in doc		
		{ 
			get { return mMissed; }
			set { mMissed = SetNullableInt( value, 0 ); }
		}

        public int blocked		//'int' in doc		
		{ 
			get { return mBlocked; }
			set { mBlocked = SetNullableInt( value, 0 ); }
		}

        public int seen		//'int' in doc		
		{ 
			get { return mSeen; }
			set { mSeen = SetNullableInt( value, 0 ); }
		}

        public long startEpoch //'int' in doc
        {
            get { return mStartEpoch; }
            set { mStartEpoch = SetNullableLong(value, 0); }
        }

        public long stopEpoch //'int' in doc
        {
            get { return mStopEpoch; }
            set { mStopEpoch = SetNullableLong(value, 0); }
        }

        public int duration
        {
            get { return mDuration; }
            set { mDuration = SetNullableInt(value, 0); }
        }

        #endregion
    }
}
