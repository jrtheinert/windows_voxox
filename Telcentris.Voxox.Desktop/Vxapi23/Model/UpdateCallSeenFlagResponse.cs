﻿using Vxapi23.Base;

using System;

namespace Vxapi23.Model
{
	//This response object is not yet set because we are getting invalid JSON from the API call.
	//	Plus we will add the call UID to it after the API call completes so we can handle failures.
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public class UpdateCallSeenFlagResponse : BaseApiResponse
    {
		public String  Uid		{ get; set; }
    }
}

