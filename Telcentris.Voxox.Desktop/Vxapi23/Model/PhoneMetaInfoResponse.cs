﻿
using Vxapi23.Base;

using System;
using System.Collections.Generic;

namespace Vxapi23.Model
{
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]

    public class PhoneMetaInfoResponse : BaseApiResponse
    {
		public List<Number> numbers { get; set; }

		public class Number
		{ 
			public String		phoneNumber		{ get; set; }
			public InNetwork	inNetwork		{ get; set; }
			public List<String> tags			{ get; set; }


			//Tags stanza looks like this:
			//	"tags": [ "blocked", "favorited" ]
			//So we have special handling.

			public Boolean isBlocked()
			{
				return hasTag( "blocked" );
			}

			public Boolean isFavorite()
			{
				return hasTag( "favorited" );
			}

			private Boolean hasTag( String key )
			{
				Boolean result = false;

				if ( tags != null )
				{
					result = tags.Contains(key );
				}

				return result;
			}
		}

		public class InNetwork
		{
			public String did		{ get; set; }
			public String username	{ get; set; }
			public String mobile	{ get; set; }
			public String userId	{ get; set; }
			public String xmppjid	{ get; set; }
		}

		//For easy 'Find()' in main app.
		public static Predicate<Number> ByNumber( String tgtNumber )
		{
			return delegate( Number num )
			{
				return num.phoneNumber == tgtNumber;
			};
		}
    }
}
