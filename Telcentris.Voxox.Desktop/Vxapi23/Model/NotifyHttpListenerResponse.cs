﻿using Vxapi23.Base;
using System;

namespace Vxapi23.Model
{
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public class NotifyHttpListenerResponse : BaseApiResponse
    {

    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public class ConferenceCallHttpListenerResponse : NotifyHttpListenerResponse
    {
		private const string ERROR_NONE								= "200";
		private const string ERROR_NO_NUMBERS_TO_BLAST				= "412";
		private const string ERROR_CONFERENCE_MAX_CONTACTS			= "413";
		private const string ERROR_CONFERENCE_NOT_ENABLED_ON_PLAN	= "414";
		private const string ERROR_CONNECTING_TO_SERVER				= "500";
		
        // Only returns in conference call
        public ConferenceCallData Data { get; set; }

		public string ConferenceId
		{
			get { return (Data == null ? "" : Data.conferenceid);	}
		}

		public Boolean hasNoError()
		{
			return hasError( ERROR_NONE );
		}

		public Boolean hasNoNumbersToBlastError()
		{
			return hasError( ERROR_NO_NUMBERS_TO_BLAST );
		}

		public Boolean hasConferenceMaxContactsError()
		{
			return hasError( ERROR_CONFERENCE_MAX_CONTACTS );
		}

		public Boolean hasConferenceNotEnabledOnPlanError()
		{
			return hasError( ERROR_CONFERENCE_NOT_ENABLED_ON_PLAN );
		}

		public Boolean hasConnectingToServerError()
		{
			return hasError( ERROR_CONNECTING_TO_SERVER );
		}

		private bool hasError( string errorCode )
		{
			return (Code == errorCode);
		}
	
	}

    public class ConferenceCallData
    {
        public string conferenceid { get; set; }
    }

}
