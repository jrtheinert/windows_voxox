﻿using System.Collections.Generic;
using Vxapi23.Base;

namespace Vxapi23.Model
{
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public class MessageHistoryResponse
    {
        // Do not rename the variable as this will get mapped to the response object that application receives from server.
        public List<MessageHistoryData> Response { get; set; }
        public string Status { get; set; }
    }

    public class MessageHistoryData : BaseApiResponse
    {
        #region | Private Class Variables |

        private int		mMessageType;
        private double	mDuration;
        private int		mStatus;

        #endregion

        #region | Public Properties |

        public string messageId { get; set; }
        public string inbound	{ get; set; }
        public string to		{ get; set; }
        public string from		{ get; set; }
        public string body		{ get; set; }
        public string created	{ get; set; }		//This becomes ServerTimestamp
        public string filename	{ get; set; }
		public string clientUid { get; set; }		//Used to match SMS messageId if we get getMessageHistory() response before sendSMSVerbose() response.

        public int messageType //Keep this as string because we have other code that expects it.
        {
            get { return mMessageType; }
            set { mMessageType = SetNullableInt(value, 0); }
        }

        public double duration //Duration is in whole seconds, but presented with decimal, e.g. "5.0".
        {
            get { return mDuration; }
            set { mDuration = SetNullableDouble(value, 0.0); }
        }

        public int status //MsgStatus is enum/int
        {
            get { return mStatus; }
            set { mStatus = SetNullableInt(value, 0); }
        }

        #endregion
    }
}