﻿
using Vxapi23.Base;

using System;
using System.Collections.Generic;

namespace Vxapi23.Model
{
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]

//	{"sources":[
//			{"contactSourceId":"221",
//			 "sourceKey":"355136055949112",
//	 	 	 "description":null,
//			 "modified":"2015-10-06 14:50:41",
//			 "created":"2015-10-06 14:50:41"
//			}
//	]} 

    public class ContactSourcesResponse : BaseApiResponse
    {
		public List<Source> sources { get; set; }
		
		public class Source
		{ 
			private DateTime mCreated		{ get; set; }
			private DateTime mModified		{ get; set; }

			public String ContactSourceId	{ get; set; }	//Server sets this
			public String SourceKey			{ get; set; }	//Set by client.  We use this to retrieve contacts
			public String Description		{ get; set; }	//Optional

			public DateTime Created
			{
				get { return mCreated; }
				set { mCreated = SetNullableDateTime(value, DateTime.Now); }
			}

			public DateTime Modified
			{
				get { return mModified; }
				set { mModified = SetNullableDateTime(value, DateTime.Now); }
			}
		}	//class Source

		public Source getMostRecent()
		{
			Source result = null;

			DateTime recent = new DateTime();

			foreach ( Source src in sources )
			{
				if ( src.Modified > recent )
				{
					result = src;
					recent = src.Modified;
				}
			}

			return result;
		}

		public void addTestData( String desc, String srcId, String key, int daysBack, int hoursBack )
		{
			TimeSpan ts = new TimeSpan( (daysBack * 24) + hoursBack, 0, 0 );
			Source temp = new Source();

			temp.ContactSourceId = srcId;
			temp.Created		 = DateTime.UtcNow - ts;
			temp.Description	 = desc;
			temp.Modified		 =  DateTime.UtcNow - ts;
			temp.SourceKey		 = key;

			sources.Add( temp );
		}
    }
}
