﻿using System.Collections.Generic;

namespace Vxapi23.Model
{
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public class CheckInNetworkContactResponse
    {
        public Dictionary<string, NetworkContact> Contacts { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public class NetworkContact
    {
        public string did { get; set; }
        public string username { get; set; }
        public string mobile { get; set; }
        public string userId { get; set; }
    }
}
