﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vxapi23.Base;

namespace Vxapi23.Model
{
    public class CompanyUserOptionsResponse
    {
        public QuantityCompanyUserOption ADDITIONAL_DID { get; set; }
        public QuantityCompanyUserOption AUTO_ATTENDANT { get; set; }
        public BooleanCompanyUserOption  CALL_RECORDING { get; set; }
        public BooleanCompanyUserOption  CALL_ROUTING_AA { get; set; }
        public BooleanCompanyUserOption  CALL_ROUTING_BYPASS_AA { get; set; }
        public BooleanCompanyUserOption  CALL_ROUTING_EXT { get; set; }
        public BooleanCompanyUserOption  CALL_ROUTING_FAX { get; set; }
        public BooleanCompanyUserOption  CALL_ROUTING_VM { get; set; }
        public BooleanCompanyUserOption  CALL_SCREENING { get; set; }
        public QuantityCompanyUserOption COMPANY_NUMBER { get; set; }
        public QuantityCompanyUserOption CONFERENCE_MEMBERS { get; set; }
        public BooleanCompanyUserOption  CONTACT_MANAGEMENT { get; set; }
        public BooleanCompanyUserOption  FAX_ENDPOINT { get; set; }
        public BooleanCompanyUserOption  ON_DEMAND_CALL_RECORDING { get; set; }
        public QuantityCompanyUserOption PERSON_SEAT { get; set; }
        public QuantityCompanyUserOption REACH_ME_NUMBERS { get; set; }
        public BooleanCompanyUserOption  REGISTERED_DEVICE { get; set; }
        public BooleanCompanyUserOption  SEND_FAX { get; set; }
        public BooleanCompanyUserOption  SMS_ENABLED { get; set; }
        public BooleanCompanyUserOption  TIME_OF_DAY { get; set; }
        public BooleanCompanyUserOption  VM_MACHINE_TRANSCRIBE { get; set; }
        public BooleanCompanyUserOption  WEB_CALLBACK { get; set; }

    }

    public class BooleanCompanyUserOption
    {
        public virtual string ProductTypeCode { get; set; }

        public virtual string FeatureCode { get; set; }

        public virtual string FeatureBehaviorCode { get; set; }

        public virtual string DeclineMessage { get; set; }

        public virtual bool IsFeatureEnabled { get { return FeatureBehaviorCode == "FEATURE_ENABLED"; } }

		public virtual bool IsFeatureDisabled { get { return FeatureBehaviorCode == "FEATURE_DISABLED"; } }

        public virtual bool IsFeatureHidden { get { return FeatureBehaviorCode == "FEATURE_HIDDEN"; } }
    }

    public class QuantityCompanyUserOption : BooleanCompanyUserOption
    {
        private int mBundledLimit;
        private int mQuantityLimit;
        private int mQuantityInUse;

        public virtual int BundledLimit
        {
            get { return mBundledLimit; }
            set { mBundledLimit = BaseApiResponse.SetNullableInt(value, 0); }
        }

        public virtual int QuantityLimit
        {
            get { return mQuantityLimit; }
            set { mQuantityLimit = BaseApiResponse.SetNullableInt(value, 0); }
        }

        public virtual int QuantityInUse
        {
            get { return mQuantityInUse; }
            set { mQuantityInUse = BaseApiResponse.SetNullableInt(value, 0); }
        }

        public virtual string AddBehaviorCode { get; set; }

        public virtual List<NumberOrPerson> BundledObjects { get; set; }

        public virtual List<NumberOrPerson> AddOnObjects { get; set; }

        //public List<string> PurchaseBehavior { get; set; }

        public virtual bool IsAddEnabled { get { return AddBehaviorCode == "ADD_ENABLED"; } }

        public virtual bool IsAddHidden { get { return AddBehaviorCode == "ADD_HIDDEN"; } }
    }

    public class NumberOrPerson
    {
        private decimal mPrice;

        public virtual string FeatureCode { get; set; }

        public virtual string IProduct { get; set; }

        public virtual string IDoBatch { get; set; }

        public virtual decimal Price
        {
            get { return mPrice; }
            set { mPrice = BaseApiResponse.SetNullableDecimal(value, 0); }
        }
    }
}
