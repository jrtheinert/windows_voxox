﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vxapi23.Model
{
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]        
    public class RichTextAudioResponse
    {
        public AudioResponseData voxoxrichmessage { get; set; }
    }

    public class AudioResponseData
    {
        public AudioData data { get; set; }
        public string type { get; set; }
        public string body { get; set; }
    }
    public class AudioData
    {
        public string file { get; set; }
    }
}
