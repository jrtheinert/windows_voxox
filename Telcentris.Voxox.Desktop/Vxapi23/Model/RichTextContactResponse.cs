﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vxapi23.Model
{
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]        
    public class RichTextContactResponse
    {
        public ContactResponseData voxoxrichmessage { get; set; }
    }

    public class ContactResponseData
    {
        //public Data data { get; set; }
        public string type { get; set; }
        public string body { get; set; }
    }

    //public class Data
    //{
        
    //}
}
