﻿
using Vxapi23.Base;

using System;
using System.Collections.Generic;

namespace Vxapi23.Model
{
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]

    public class ContactListResponse : BaseApiResponse
    {
		public List<Contact> contacts { get; set; }

		public class Contact
		{ 
			public String FirstName		{ get; set; }
			public String LastName		{ get; set; }
			public String ContactKey	{ get; set; }
			public String Company		{ get; set; }

			public List<PhoneNumber> numbers { get; set; }
		}

		public class PhoneNumber
		{
			public String Number		{ get; set; }
			public String Label			{ get; set; }
		}

		public String[] getPhoneNumbersAsArray()
		{
			List<String> temp = new List<String>();

			foreach (Contact contact in contacts )
			{
				foreach( PhoneNumber pn in contact.numbers )
				{
					temp.Add( pn.Number );
				}
			}

			String[] result = temp.ToArray();

			return result;
		}

    }
}
