﻿using Vxapi23.Base;

using System;

namespace Vxapi23.Model
{
    public class SendFaxResponse : BaseApiResponse
    {
        public string success { get; set; }

         public new bool succeeded()	//'new' so we can override method in base class.
        {
            return success.Equals( "true", StringComparison.CurrentCultureIgnoreCase);	//Appropriate use of "success".
        }
    }
}
