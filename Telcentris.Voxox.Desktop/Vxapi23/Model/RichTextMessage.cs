﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vxapi23.Model
{
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]        
    public class RichTextMessage
    {
        public Voxoxrichmessage voxoxrichmessage { get; set; }
    }

    public class Voxoxrichmessage
    {
        public Data data { get; set; }
        public string type { get; set; }
        public string body { get; set; }
    }
    // Contains the fields in body section. TODO need to add parameters for Contact response.
    public class Data
    {
        public string file { get; set; }
        public string thumbnail { get; set; }
        public string longitude { get; set; }
        public string latitude { get; set; }
    }
}
