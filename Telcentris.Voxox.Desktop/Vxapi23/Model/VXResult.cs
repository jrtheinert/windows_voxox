﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vxapi23.Model
{
    public class VXResult<T> where T : new()
    {
        public bool Success { get; set; }

        public bool IsNetworkError { get; set; }

        public string ErrorMessage { get; set; }

        public T Data { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("Success: {0}", Success);
            sb.AppendLine();
            sb.AppendFormat("IsNetworkError: {0}", IsNetworkError);
            sb.AppendLine();
            sb.AppendFormat("ErrorMessage: {0}", ErrorMessage);
            sb.AppendLine();
            sb.AppendFormat("Data: {0}", Data);
            sb.AppendLine();

            return sb.ToString();
        }
    }
}
