﻿using Vxapi23.Constants;

using System;						//String
using System.Collections.Generic;	//Dictionary

namespace Vxapi23.Util
{
    class RestfulApiHelper
    {
        private RestfulApiHelper() { }

		//Generic text helpers
		public static String formatJsonContent( String jsonIn, String method )
		{
			String key1 = "";
			String key2 = "";

			//TODO: Define keys for other methods
			if ( method == RestfulConstants.METHOD_GET_MESSAGE_HISTORY )
			{
				key1 = "{\"messageType\"";
			}
			else if ( method == RestfulConstants.METHOD_GET_CALL_HISTORY )
			{
				key1 = "{\"companyId\"";
			}
			else if ( method == RestfulConstants.METHOD_GET_CONTACT_LIST )
			{
				key1 = "{\"firstName\"";
				key2 = "{\"number\"";
			}
			else if ( method == RestfulConstants.METHOD_GET_PHONE_META_INFO )
			{
				key1 = "{\"phoneNumber\"";
			}

			//We want to insert a CRLF BEFORE each key, so that in log, JSON has one entry per line.
			if ( String.IsNullOrEmpty( key1 ) )
			{
				return jsonIn;
			}

			String newKey1 = "\n\t" + key1;
			String result = jsonIn.Replace( key1, newKey1 );

			if ( key2 != "" )
			{
				String newKey2 = "\n\t\t" + key2;
				result = result.Replace( key2, newKey2 );

			}

			return result;
		}

		public static string toUrl( string serviceUrl, string uri, Dictionary<string, string> parameters )
		{
            string result = serviceUrl + "/" + (uri.EndsWith("?") ? uri : (uri + "?")) + parameterListToString(parameters);

			return result;
		}

		private static string parameterListToString( Dictionary<string, string> parameters )
		{
			string result = "";
			string value  = "";
			int    count  = 0;

		    foreach (KeyValuePair<string, string> param in parameters)
            {
				if ( count > 0 )
				{ 
					result += "&";
				}

				value = ( param.Key == "password" ? "xxxxx" : param.Value );	//We may need to enhance this.

				result += param.Key + "=" + value;

				count++;
            }

			return result;
		}

        private static String booleanToText(bool val)
        {
            return (val ? "1" : "0");
        }

		private static Dictionary<string, string> getStdParameters( string method, bool formatJson )
		{
            var parameters = new Dictionary<string, string>();
           
//			parameters.Add( "key", RestfulConstants.ApiKey );

			if ( !String.IsNullOrEmpty( method ))	//parameters for SendFax and SendFile do not use a method.
			{
				parameters.Add( "method", method );
			}

			if ( formatJson )
			{ 
                parameters.Add("format", "json");
			}

			//ClientInfo
			parameters.Add( "clientInfo[clientId]", RestfulConstants.ClientId      );
			parameters.Add( "clientInfo[version]",  RestfulConstants.ClientVersion );

			return parameters;
		}

		private static Dictionary<string, string> getStdParameters( string method, string userKey, string companyUserId, bool formatJson )
		{
			Dictionary<string, string> parameters = getStdParameters( method, formatJson );

            parameters.Add( "userKey",		 userKey );
            parameters.Add( "companyUserId", companyUserId );

			return parameters;
		}

        //The method returns parameters for RESTful API getInitOptions() which must be called before displaying screen to user
        public static Dictionary<string, string> getGetInitOptionsParameters( bool formatJson )
        {
			var parameters = getStdParameters( RestfulConstants.METHOD_GET_INIT_OPTIONS, formatJson );

            return parameters;
        }

        //The method returns parameters for RESTful API login() which validates the credential and return account information
        public static Dictionary<string, string> getLoginParameters(string userName, string password, string partnerIdentifier, bool formatJson )
        {
			var parameters = getStdParameters( RestfulConstants.METHOD_AUTHENTICATE, formatJson );
			parameters.Add( "username", userName );
			parameters.Add( "password", password );
//			parameters.Add( "partnerIdentifier", partnerIdentifier );

            return parameters;
        }

        //The method returns parameters for the RESTful API ping() which echo the message sent.
		//Not called anywhere.
		//public static Dictionary<string, string> getPingParameters(string echoMessage)
		//{
		//	var parameters = getStdParameters( RestfulConstants.METHOD_PING );
		//	parameters.Add( "message", echoMessage );

		//	return parameters;
		//}

		//New Contact Syncing methods
        public static Dictionary<string, string> getContactSourcesParameters(string userKey, string companyUserId, bool formatJson )
        {
			var parameters = getStdParameters( RestfulConstants.METHOD_GET_CONTACT_SOURCES, userKey, companyUserId, formatJson );

            return parameters;
        }

		public static Dictionary<string, string> getContactListParameters(string userKey, string companyUserId, bool formatJson, string sourceKey )
        {
			var parameters = getStdParameters( RestfulConstants.METHOD_GET_CONTACT_LIST, userKey, companyUserId, formatJson );
			parameters.Add( "sourceKey", sourceKey );

            return parameters;
        }

		public static Dictionary<string, string> getPhoneMetaInfoParameters(string userKey, string companyUserId, bool formatJson, string[] phoneNumbers )
        {
			var parameters = getStdParameters( RestfulConstants.METHOD_GET_PHONE_META_INFO, userKey, companyUserId, formatJson );

			int x = 0;
			foreach (var phone in phoneNumbers)
            {
                parameters.Add( string.Format("phoneNumbers[{0}]", x), phone );
				x++;
            }

            return parameters;
        }

        public static Dictionary<string, string> getCompanyDirectoryParameters(string userKey, string companyUserId, bool formatJson )
        {
			var parameters = getStdParameters( RestfulConstants.METHOD_GET_COMPANY_DIRECTORY, userKey, companyUserId, formatJson );

            return parameters;
        }

		public static Dictionary<string, string> getSetBlacklistedParameters(string userKey, string companyUserId, bool formatJson, string phoneNumber, bool blacklist )
        {
			var parameters = getStdParameters( RestfulConstants.METHOD_SET_BLACKLISTED, userKey, companyUserId, formatJson );
			parameters.Add( "phoneNumber", phoneNumber );
			parameters.Add( "blacklisted", booleanToText( blacklist ) );

            return parameters;
        }

		public static Dictionary<string, string> getSetFavoriteParameters(string userKey, string companyUserId, bool formatJson, string phoneNumber, bool favorite )
        {
			var parameters = getStdParameters( RestfulConstants.METHOD_SET_FAVORITE, userKey, companyUserId, formatJson );
			parameters.Add( "phoneNumber", phoneNumber );
			parameters.Add( "favorited", booleanToText( favorite ) );

            return parameters;
        }

        public static Dictionary<string, string> getCompanyUserExtendedParameters(string userKey, string companyUserId, bool formatJson)
        {
			var parameters = getStdParameters( RestfulConstants.METHOD_GET_COMPANY_USER_EXTENDED, userKey, companyUserId, formatJson );

            return parameters;
        }

        public static Dictionary<string, string> getSendSMSParameters(string fromnumber, string to, string body, string userKey, string companyUserId, string messageId, bool formatJson)
        {
			var parameters = getStdParameters( RestfulConstants.METHOD_SEND_SMS_VERBOSE, userKey, companyUserId, formatJson );
			parameters.Add( "fromnumber", fromnumber );
			parameters.Add( "to",		  to );
			parameters.Add( "body",		  body );
			parameters.Add( "clientUid",  messageId );	//So we can match getMessageHistory() in case sendSMSVerbose and getMessageHistory get out of order.

            return parameters;
        }

        public static Dictionary<string, string> getDataSummaryParameters(string userKey, string companyUserId, bool formatJson)
        {
			var parameters = getStdParameters( RestfulConstants.METHOD_GET_DATA_SUMMARY, userKey, companyUserId, formatJson );

            return parameters;
        }

        public static Dictionary<string, string> getMessageHistoryParameters(string userKey, string companyUserId, bool formatJson, string timestamp, int limit, string contactFilter, string contactAddress )
        {
			var parameters = getStdParameters( RestfulConstants.METHOD_GET_MESSAGE_HISTORY, userKey, companyUserId, formatJson );

			if ( !String.IsNullOrEmpty( contactFilter ) )
                parameters.Add("contactFilter", contactFilter);

            if ( !String.IsNullOrEmpty( contactAddress ) )
                parameters.Add("contactAddress", contactAddress);

			if ( !String.IsNullOrEmpty( timestamp ) )
				parameters.Add( "timestamp", timestamp );

			if ( limit > 0 )
				parameters.Add( "limit", limit.ToString() );

            return parameters;
        }

        public static Dictionary<string, string> getCallHistoryParameters(string userKey, string companyUserId, bool formatJson, string timestamp, int limit )
        {
			var parameters = getStdParameters( RestfulConstants.METHOD_GET_CALL_HISTORY, userKey, companyUserId, formatJson );

			if ( !String.IsNullOrEmpty( timestamp ) )
				parameters.Add( "timestamp", timestamp );

			if ( limit > 0 )
				parameters.Add( "limit", limit.ToString() );

            return parameters;
        }

		//GetPlanInfo not called anywhere
		//public static Dictionary<string, string> getPlanInfoParameters(string userKey, string companyUserId, bool formatJson)
		//{
		//	var parameters = getStdParameters( RestfulConstants.METHOD_GET_PLAN_INFO, userKey, companyUserId, formatJson );

		//	return parameters;
		//}


        public static Dictionary<string, string> getNotifyHttpListenerParameters(string companyUserId, string userKey, string httpListenerId, string function, string dtmfDigits, bool formatJson)
        {
			var parameters = getStdParameters( RestfulConstants.METHOD_NOTIFY_HTTP_LISTENERS, userKey, companyUserId, formatJson );
            parameters.Add( "httpListenerId", httpListenerId );
            parameters.Add( "function",		  function		 );
            parameters.Add( "dtmfDigits",	  dtmfDigits	 );

            return parameters;
        }

		//Not called anywhere
		//public static Dictionary<string, string> getCallConnectParameters(string companyUserId, string userKey, string username, string from, string to)
		//{
		//	var parameters = getStdParameters( RestfulConstants.METHOD_CALL_CONNECT, userKey, companyUserId );
		//	parameters.Add( "username", username );
		//	parameters.Add( "from",		from	 );
		//	parameters.Add( "to",	    to		 );

		//	return parameters;
		//}

        public static Dictionary<string, string> getFindMeNumbersParameters(string userKey, string companyUserId, bool formatJson)
        {
			var parameters = getStdParameters( RestfulConstants.METHOD_GET_FIND_ME_NUMBERS, userKey, companyUserId, formatJson );

            return parameters;
        }

        // TODO need to update the list of input parameters.
		//public static Dictionary<string, string> getRichTextMessageParameters(string userKey, string companyUserId)
		//{
		//	var parameters = getStdParameters( RestfulConstants.METHOD_GET_FINDME_NUMBERS, userKey, companyUserId );	//NOT correct method name.

		//	return parameters;
		//}

		//DEPRECATED
		//public static Dictionary<string, string> getCheckForInNetworkContactsParameters(string userKey, string companyUserId, string partnerId, string[] phoneNumbers, bool formatJson)
		//{
		//	var parameters = getStdParameters( RestfulConstants.METHOD_CHECK_NETWORK_CONTACTS, userKey, companyUserId, formatJson );
		//	parameters.Add( "partnerId", partnerId );

		//	foreach (var phone in phoneNumbers)
		//	{
		//		parameters.Add("phoneNumbers[]", phone);
		//	}

		//	return parameters;
		//}

        public static Dictionary<string, string> getUpdateCompanyUserProfileParameters(string userKey, string companyUserId, string firstName, string lastName, string email, bool formatJson)
        {
			var parameters = getStdParameters( RestfulConstants.METHOD_UPDATE_COMPANY_USER_PROFILE, userKey, companyUserId, formatJson );
            parameters.Add( "firstName", firstName );
            parameters.Add( "lastName",  lastName  );
            parameters.Add( "email",	 email     );

            return parameters;
        }

        public static Dictionary<string, string> getCompanyUserOptionsParameters(string userKey, string companyUserId, bool formatJson)
        {
            var parameters = getStdParameters(RestfulConstants.METHOD_GET_COMPANY_USER_OPTIONS, userKey, companyUserId, formatJson);

        	return parameters;
        }

		 //The method returns the parameters for RESTful API getCompanyPhoneByUser()
	     //which returns the Voxox phone number (did) for each of an array of Voxox contacts names.
	     //It allows you to send an array of Voxox names and get back Voxox phone numbers.
	    public static Dictionary<string, string> getCompanyPhoneByUserParameters( string userKey, string companyUserId, string contactUserId, bool formatJson) 
		{
			var parameters = getStdParameters( RestfulConstants.METHOD_GET_COMPANY_PHONE_BY_USER, userKey, companyUserId, formatJson );
		    parameters.Add("contactUserId", contactUserId );

		    return parameters;
	    }

		 //The method returns the parameters for RESTful API updateMessageStatus()
	     //		Returns true/false
	    public static Dictionary<string, string> updateMessageStatusParameters( string userKey, string companyUserId, int msgType, string msgId, int msgStatus, bool formatJson ) 
		{
			var parameters = getStdParameters( RestfulConstants.METHOD_UPDATE_MESSAGE_STATUS, userKey, companyUserId, formatJson );
		    parameters.Add("messageType",	msgType.ToString() );
		    parameters.Add("messageId",		msgId );
		    parameters.Add("status",		msgStatus.ToString() );

		    return parameters;
	    }

        public static Dictionary<string, string> getUpdateMessageStatusBatchParameters(string userKey, string companyUserId, List<Model.DeletedMessagesToServer> messages, bool formatJson)
        {
            var parameters = getStdParameters(RestfulConstants.METHOD_UPDATE_MESSAGE_STATUS_BATCH, userKey, companyUserId, formatJson);

            // This method expects the messages as an array parameter in the messages[INDEX][PROPERTY]=VALUE format for which the JsonSerializer cannot be used.
            for(int ii = 0; ii < messages.Count;ii++)
            {
                parameters.Add(string.Format("messages[{0}][messageType]", ii), messages[ii].messageType.ToString());
                parameters.Add(string.Format("messages[{0}][messageId]", ii), messages[ii].messageId);
                parameters.Add(string.Format("messages[{0}][status]", ii), messages[ii].status.ToString());
            }
            
            return parameters;
        }

		 //The method returns the parameters for RESTful API updateCallSeenFlag()
	     //		Returns true/false
	    public static Dictionary<string, string> updateCallSeenFlagParameters( string userKey, string companyUserId, string callUid, int seenFlag, bool formatJson ) 
		{
			var parameters = getStdParameters( RestfulConstants.METHOD_UPDATE_CALL_SEEN_FLAG, userKey, companyUserId, formatJson );
		    parameters.Add("uid",	callUid );
		    parameters.Add("flag",	seenFlag.ToString() );

		    return parameters;
	    }

        //The method returns the parameters for RESTful API SetCallerId()
        public static Dictionary<string, string> getSetCallerIdParameters(string userKey, string companyUserId, string callerId, bool singleUse, bool formatJson)
        {
            var parameters = getStdParameters(RestfulConstants.METHOD_SET_CALLER_ID, userKey, companyUserId, formatJson);
            parameters.Add("callerId", callerId);
            parameters.Add("singleUse", singleUse.ToString().ToLower());

            return parameters;
        }

		//----------------------------------------------------------------------
		//The following methods are NOT part of VX client
		//----------------------------------------------------------------------
        public static Dictionary<string, string> getSendFaxParameters( string userKey, string companyUserId, string partnerId, string toNumber, string fromNumber, bool formatJson )
		{
			//JRT - Leave this commented code here until we get Fax working.  It is low priority for server team.
			//var parameters = new Dictionary<string, string> {
			//					{"userKey", userKey},
			//					{"companyUserId", companyUserId},

			//					{"partnerId", partnerId },
			//					{"to", toNumber },
			//					{"from", fromNumber }
			//				};

			//NOT VX
			var parameters = getStdParameters( "", userKey, companyUserId, formatJson );	//NO method for this.
            parameters.Add( "partnerId", partnerId );
            parameters.Add( "to", toNumber );
            parameters.Add( "from", fromNumber );

			return parameters;
	   }

		public static Dictionary<string, string> getSendFileParameters( string userKey, string companyUserId, string filePath, bool formatJson )
		{ 
				//var parameters = new Dictionary<string, string> {
				//						{"userKey", userKey},
				//						{"companyUserId", companyUserId}                                        
				//						  };
			var parameters = getStdParameters( "", userKey, companyUserId, formatJson );	//NO method for this.

			return parameters;
		}
		     
		public static Dictionary<string, string> getDownloadFileParameters( string userKey, string companyUserId, int svrMsgType, string msgId, bool formatJson )
		{
			var parameters = getStdParameters( "", userKey, companyUserId, formatJson );	//NO method for this.
			parameters.Add( "messageType", svrMsgType.ToString() );
			parameters.Add( "messageId",   msgId );

			return parameters;
		}
		//----------------------------------------------------------------------
		// End methods NOT part of VX client
		//----------------------------------------------------------------------

	}	// class RestfulApiHelper

}	//namespace 