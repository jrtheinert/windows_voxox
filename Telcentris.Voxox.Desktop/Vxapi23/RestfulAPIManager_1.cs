﻿using Desktop.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vxapi23.Base;
using Vxapi23.Constants;
using Vxapi23.Model;

namespace Vxapi23
{
    /// <summary>
    /// This class provides the various methods to make RestFul API calls to Voxox NOC or 
    /// services. For the RestFul calls are provided in the base class which uses 
    /// RestSharp for all RestFul calls.
    /// </summary>
    public class RestfulAPIManager : BaseService
    {
        //Instance of Logger
        private static VXLogger logger = VXLoggingManager.INSTANCE.GetLogger("RestfulAPIManager");
        private AuthenticateResponse loginResponse;

        /// Initializes a new instance of the <see cref="RestfulAPIManager" /> class. 
        /// </summary>
        /// <remarks>
        /// Uses a private access modifier to prevent external instantiation of this class,
        /// as this class uses Singleton Pattern.
        /// </remarks>		
        private RestfulAPIManager(string apiKey)
        {
            RestfulConstants.ApiKey = apiKey;//initialize the keys
        }


    }
}
