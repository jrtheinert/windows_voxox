﻿using System;

namespace Vxapi23.Base
{
    public class BaseApiResponse
    {
        private bool success;

        public BaseApiResponse()
        {
            success = false;
        }

        //These are/should be in all XML API responses.  We do NOT see them in JSON.
        public string Message { get; set; }
        public string Status { get; set; }

        //We add the following simple types so we don't need more VoxoxApiResponse-derived classes
        public string Code { get; set; }
        public string Response { get; set; }
        public string Type { get; set; }

        public Boolean Success { get; set; }

        public bool succeeded()
        {
            success = Success || Status != null && Status.Equals("success", StringComparison.CurrentCultureIgnoreCase);	//Appropriate use of "success".
            return success;
        }

        #region | Protected SetNullable Routines |

		//A couple helper conversions
		static public bool intToBool( int value )
		{
			return value == 0 ? false : true;
		}

		static public bool stringToBool( string value )
		{
			return value == "0" ? false : true;
		}


		//Let's make these static and public so they can easily be used by nested classes.
        static public byte SetNullableByte(byte? value, byte defaultValue)
        {
            byte result = (value == null ? defaultValue : Convert.ToByte(value));

            return result;
        }

        static public short SetNullableShort(short? value, short defaultValue)
        {
            short result = (value == null ? defaultValue : Convert.ToInt16(value));

            return result;
        }

        static public int SetNullableInt(int? value, int defaultValue)
        {
            int result = (value == null ? defaultValue : Convert.ToInt32(value));

            return result;
        }

        static public long SetNullableLong(long? value, long defaultValue)
        {
            long result = (value == null ? defaultValue : Convert.ToInt64(value));

            return result;
        }

        static public float SetNullableFloat(float? value, float defaultValue)
        {
            float result = (value == null ? defaultValue : Convert.ToSingle(value));

            return result;
        }

        static public double SetNullableDouble(double? value, double defaultValue)
        {
            double result = (value == null ? defaultValue : Convert.ToDouble(value));

            return result;
        }

        static public decimal SetNullableDecimal(decimal? value, decimal defaultValue)
        {
            decimal result = (value == null ? defaultValue : Convert.ToDecimal(value));

            return result;
        }

        static public bool SetNullableBool(bool? value, bool defaultValue)
        {
            bool result = (value == null ? defaultValue : Convert.ToBoolean(value));

            return result;
        }

        static public DateTime SetNullableDateTime(DateTime? value, DateTime defaultValue)
        {
            DateTime result = (value == null ? defaultValue : Convert.ToDateTime(value));

            return result;
        }

        #endregion
    }
}
