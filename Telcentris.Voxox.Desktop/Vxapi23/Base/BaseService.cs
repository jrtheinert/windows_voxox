﻿using Desktop.Utils;

using Vxapi23.Util;

using RestSharp;

using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Vxapi23.Base
{
    public class BaseService
    {
		private string		methodKey	= "method";
        private const int	RETRY_DELAY	= 500;

		private	static IWebProxy mIProxy = null;

        private VXLogger logger = VXLoggingManager.INSTANCE.GetLogger("Vxapi23.BaseService");

		//This will be set/reset from SessionManager.
		public static void setProxy( IWebProxy proxy )
		{
			mIProxy = proxy;
		}

		public BaseService()
		{
		}

        /// <summary>
        /// Invokes the RestFul method and converts the response to the TypeParam.
        /// </summary>
        /// <typeparam name="T">The return type expected, usually one of the Model objects</typeparam>
        /// <param name="serviceUrl">The base URL for the Restful service</param>
        /// <param name="uri">The URI for the Restful call</param>
        /// <param name="methodName">The method name being called</param>
        /// <param name="parameters">The parameters to send\post</param>
        /// <returns>Task<T></returns>
        protected async Task<T> InvokeRestMethodAsync<T>(string serviceUrl, string uri, Dictionary<string, string> parameters, string filePath = null) where T : new()
        {
			String url = RestfulApiHelper.toUrl( serviceUrl, uri, parameters );
			logger.Debug( "API URL: " + url );
		
			String method = (parameters.ContainsKey( methodKey )) ? parameters[methodKey] : "[NONE]";

            var client  = new RestClient(serviceUrl);
            var request = new RestRequest(uri, Method.POST);

			addProxyIfNeeded( client, url );

            foreach (KeyValuePair<string, string> param in parameters)
            {
                request.AddParameter(param.Key, param.Value);
            }

            if (filePath != null)
            {
                request.AddFile("file", filePath);
            }

            // execute the request
			bool		  shouldRetry   = true;
			int			  retryCount    = 0;
			int			  maxRetryCount = 3;
			IRestResponse response      = null;

			while ( shouldRetry && retryCount < maxRetryCount )
			{ 
				try 
				{
                    //Let's track how long API call takes, so we can report back to server team for improvements as needed.
                    VXProfiler prof = new VXProfiler("Method: " + method);

					response = await client.ExecuteTaskAsync(request);	//This may throw exceptions

					prof.Stop(true);
                    double time = prof.ElapsedMilliseconds / 1000.0;

					//Let's format response for some of the methods that have multiple entries.
					String formatedContent = RestfulApiHelper.formatJsonContent( response.Content, method );

					logger.Info( "Method: " + method + ", Time: " + time.ToString() + " seconds.  Content-Type: " + response.ContentType + ",\n Content: " + formatedContent );

					shouldRetry = false;

					//Parse the returned data - xml/json
					try
					{         
						if ( response.ContentType.Contains("html") )
						{
							//This is always an error of us, typically a 404 NotFound error.
							//	This typically indicates our API call is incorrect in some, often small, way.
							if ( response.StatusCode == System.Net.HttpStatusCode.NotFound )
							{
								logger.Warn( "API call returned 404.  Check your API call parameters.  In some instances this is a server error.");
							}
							else
							{
								//TEMPORARY WORKAROUND: sendfax is returning content type of 'text/html'.
								//	Added this so we can continue fax work.
								if ( uri.Contains( "sendfax" ) )
								{
									return (new RestSharp.Deserializers.JsonDeserializer()).Deserialize<T>(response);
								}
							}
						}
						else if (response.ContentType.Contains("xml") )
						{
							return (new RestSharp.Deserializers.XmlDeserializer()).Deserialize<T>(response);
						}
						else  //JSON
						{
							//WORKAROUND - This API method is returning invalid JSON
							if ( method == "updateCallSeenFlag"  ||  
								 method == "updateMessageStatus" ||
								 method == "setCallerId"         ||
								 method == "setBlacklisted"      ||
								 method == "setFavorite"          )
							{
								String save = response.Content;
								String temp = "{\"success\":\"" + response.Content + "\"}";

								response.Content = temp;

								var temp2 = (new RestSharp.Deserializers.JsonDeserializer()).Deserialize<T>(response);
								return temp2;
							}
							else if ( method == "getCompanyDirectory" )
							{
								String save = response.Content;
								String temp = "{\"entries\":" + response.Content + "}";

								response.Content = temp;

								var temp2 = (new RestSharp.Deserializers.JsonDeserializer()).Deserialize<T>(response);
								return temp2;
							}
							else
							{ 
								//TODO: getContacts is causing mscorlib.dll to throw System.FormatException.  Check it out and get server team to fix it.
								//	Additional information: Input string was not in a correct format

								//TODO: checkForInNetworkContacts is causing RestSharp.dll to throw 'System.InvalidCastException'
								//	Additional information: Unable to cast object of type 'RestSharp.JsonArray' to type 'System.Collections.Generic.IDictionary`2[System.String,System.Object]'.
								//	Actual response.Content value is "[]", without the double quotes.

								return (new RestSharp.Deserializers.JsonDeserializer()).Deserialize<T>(response);
							}
						}
					}

					catch(Exception e)	//TODO: Do NOT use this generic exception.  What specific exceptions should we be handling here?
					{
						Console.WriteLine("Exception at Base service : " + e);
					}
				}

				catch ( System.Net.WebException e )
				{
					String msg = "method '" + method + "()'" + " failed with exception:";

					retryCount++;

                    if (retryCount < maxRetryCount)
                    {
                        logger.Warn(msg, e);
                    }
                    else
                    {
                        logger.Error(msg, e);

                        if (e.Status == WebExceptionStatus.ProtocolError)
                        {
                            //TODO: Could be 401, 404 etc. How do we handle those in higher layers?
                        }
                        else if (e.Status == WebExceptionStatus.ConnectFailure ||
                                 e.Status == WebExceptionStatus.NameResolutionFailure ||
                                 e.Status == WebExceptionStatus.ProxyNameResolutionFailure ||
                                 e.Status == WebExceptionStatus.RequestProhibitedByProxy ||
                                 e.Status == WebExceptionStatus.Timeout)
                        {
                            throw new NetworkConnectionException("Network Error.");
                        }
                    }

                    System.Threading.Thread.Sleep( RETRY_DELAY );	//Arbitrary delay.
				}
				catch ( SystemException e )
				{
					//If we determine this is an exception that should be retried, add it specifically above in the catch block
					logger.Error( e );
					shouldRetry = false;
				}
			}

            return default(T);
        }

        /// <summary>
        /// Invokes the RestFul method and return the raw payload back as string.
        /// </summary>        
        /// <param name="parameters">The parameters to send\post</param>        
        /// <param name="serviceUrl"></param>
        /// <param name="uri"></param>
        /// <param name="parameters"></param>
        /// <returns>string payload</returns>
		/// 
		//TODO: This method is only called from UpdateCompanyUserProfile() and appears to be the only non-templated response.
		//		Try to merge this with the templated version
        protected async Task<string> InvokeRestMethodAsync(string serviceUrl, string uri, Dictionary<string, string> parameters)
        {
 			String url = RestfulApiHelper.toUrl( serviceUrl, uri, parameters );
			logger.Debug( "API URL: " + url );

			var client  = new RestClient(serviceUrl);
            var request = new RestRequest(uri, Method.POST);

			addProxyIfNeeded( client, url );

            foreach (KeyValuePair<string, string> param in parameters)
            {
                request.AddParameter(param.Key, param.Value);
            }

            // execute the request
            IRestResponse response = await client.ExecuteTaskAsync(request);

            logger.Debug("Content-Type:" + response.ContentType);
            logger.Debug("Content:"      + response.Content);
            
			return response.Content;
        }

        protected T DeserializeJsonString<T>(string jsonString) where T : new()
        {
            T obj = new T();
            try
            {
                obj = SimpleJson.DeserializeObject<T>(jsonString);
                return obj;

            }
            catch (Exception e)
            {
                Console.WriteLine("Exception at Base service : " + e);
            }

            return default(T);
        }

        //TODO Need to add file upload method(s)
		
		//We pass in URL so we can get the Proxy info from IWebProxy.
		private void addProxyIfNeeded( RestClient client, string url )
		{
			if ( mIProxy != null )
			{
				Uri tempUri = new Uri(url);
				String host = tempUri.Scheme + "://" + tempUri.Host;
				bool isBypassed = mIProxy.IsBypassed( new Uri(host) );

//				logger.Debug( "URI " + host + " bypassed = " + Convert.ToString( isBypassed ) );

				if ( !isBypassed )	//If ByPassed == true, the proxy is NOT used.
				{
					client.Proxy = mIProxy;

					if ( client.Proxy != null )
					{
						Uri    temp  = client.Proxy.GetProxy( new Uri(url) );

						logger.Debug( "Using proxy: " + temp.AbsoluteUri );
					}
				}
			}
		}
    }
}
