﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vxapi23.Base
{
    public class NetworkConnectionException : Exception
    {
        public NetworkConnectionException() { }

        public NetworkConnectionException(string message) : base(message) { }

        public NetworkConnectionException(string message, Exception inner) : base(message, inner) { }

        protected NetworkConnectionException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }

    }
}
