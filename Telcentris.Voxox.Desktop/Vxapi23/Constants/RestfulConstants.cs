﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vxapi23.Constants
{
    public class RestfulConstants
    {
        private RestfulConstants() { } // prevents instantiation

        public static RestfulConstants INSTANCE = new RestfulConstants();

        //URI Query (vxAuth API Methods) :  NOT in vxClient
		public const string METHOD_PING			= "ping";
		public const string METHOD_CALL_CONNECT	= "callConnect";

		//vxAuth API Methods
		public const string METHOD_AUTHENTICATE		= "authenticate";
		public const string METHOD_GET_INIT_OPTIONS	= "getInitOptions";

		//vxClient API methods as of 2015.11.30 from https://voxoxrocks.atlassian.net/wiki/display/DST/vxClient.  
		//	I have listed all, and commented those we are not (yet) using.
		//	Deprecated items are commented and moved to bottom of list so we can compare to server-side docs

		//public const string METHOD_CHECK_VOUCHER_CARD_STATUS			= "checkVoucherCardStatus";		//New - To be implemented?
		//public const string METHOD_CHECK_VOUCHER_CARD_VALUE			= "checkVoucherCardValue";		//New - To be implemented?
		//public const string METHOD_DETECT_LANGUAGE					= "detectLanguage";				//New - To be implemented?
		//public const string METHOD_EXCHANGE_CURRENCY					= "exchangeCurrency";			//New - To be implemented?

		//public const string METHOD_GET_AVAILABLE_LANGUAGES			= "getAvailableLanguages";		//New - To be implemented?
		//public const string METHOD_GET_BADGES							= "getBadges";					//Old - To be implemented? We get same info in getDataSummary, but this call is less load on server
		//public const string METHOD_GET_BALANCE						= "getBalance";					//New - To be implemented? We get balance in getDataSummary, but this call is less load on server
		//public const string METHOD_GET_CALLER_ID_NUMBERS				= "getCallerIdNumbers";			//Old - To be implemented? We get these via other API call (not sure which one right now)

		public const string METHOD_GET_CALL_HISTORY						= "getCallHistory";
		public const string METHOD_GET_COMPANY_DIRECTORY				= "getCompanyDirectory";		//New Contact Syncing
		public const string METHOD_GET_COMPANY_PHONE_BY_USER			= "getCompanyPhoneByUser";
		public const string METHOD_GET_COMPANY_USER_EXTENDED			= "getCompanyUserExtended";
		public const string METHOD_GET_COMPANY_USER_OPTIONS				= "getCompanyUserOptions";
		public const string METHOD_GET_CONTACT_LIST						= "getContactList";				//New Contact Syncing
		public const string METHOD_GET_CONTACT_SOURCES					= "getContactSources";			//New Contact Syncing
		public const string METHOD_GET_DATA_SUMMARY						= "getDataSummary";
		public const string METHOD_GET_FIND_ME_NUMBERS					= "getFindMeNumbers";

		//public const string METHOD_GET_JABBER_ID_META_INFO			= "getJabberIdMetaInfo";		//New - To be implemented? To handle IMs from contacts not in AB?
		//public const string METHOD_GET_LANGUAGES						= "getLanguages";				//New - To be implemented?
		public const string METHOD_GET_MESSAGE_HISTORY					= "getMessageHistory";
		//public const string METHOD_GET_NUMBERS_BY_TAG					= "getNumbersByTag";			//New - To be implemented?	//Retrieve all 'tagged' numbers, e.g. 'favorite', 'blacklisted', etc. from getTags() below.
		public const string METHOD_GET_PHONE_META_INFO					= "getPhoneMetaInfo";			//New Contact Syncing
		public const string METHOD_GET_PLAN_INFO						= "getPlanInfo";
		//public const string METHOD_GET_RATE_BATCH						= "getRateBatch";				//New - To be implemented?
		//public const string METHOD_GET_RATES							= "getRates";					//New - To be implemented?
		//public const string METHOD_GET_STORE_PRODUCT					= "getStoreProduct";			//New - To be implemented?
		//public const string METHOD_GET_STORE_PRODUCTS					= "getStoreProducts";			//New - To be implemented?
		//public const string METHOD_GET_TAGS							= "getTags";					//New - To be implemented? - Values here are used with getNumbersByTag, above.
		//public const string METHOD_GET_TOKENS							= "getTokens";					//Old - get APNS and GCM tokens (nothing for Windows right now)
		//public const string METHOD_GET_VIEW_ATTRIBUTES				= "getViewAttributes";			//New - To be implemented? - at user level.

		//public const string METHOD_INIT_CONTACT_SOURCE				= "initContactSource";			//New - To be implemented when we save contacts from desktop app?
		public const string METHOD_NOTIFY_HTTP_LISTENERS				= "notifyHttpListeners";
		//public const string METHOD_PURCHASE_STORE_PRODUCT				= "purchaseStoreProduct";		//New - To be implemented?
		//public const string METHOD_REGISTER_APNS_TOKEN				= "registerApnsToken";			//Apple only
		//public const string METHOD_REGISTER_TOKEN						= "registerGcmToken";			//Android only
		//public const string METHOD_SAVE_FIND_ME_NUMBERS				= "saveFindMeNumbers";			//Old - To be implemented?	No specs in doc.
		public const string METHOD_SEND_SMS_VERBOSE						= "sendSMSVerbose";
		public const string METHOD_SET_BLACKLISTED						= "setBlacklisted";				//New Contact Syncing
		public const string METHOD_SET_CALLER_ID						= "setCallerId";
		public const string METHOD_SET_FAVORITE							= "setFavorite";				//New Contact Syncing
		//public const string METHOD_STORE_MOBILE_NUMBER				= "storeMobileNumber";			//New - To be implemented?
		//public const string METHOD_TERMINATE_COMPANY					= "terminateCompany";			//New - To be implemented?
		//public const string METHOD_TRANSLATE							= "translate";					//New - To be implemented? Where is this used?  IM/SMS translation?  Other?
		//public const string METHOD_UNREGISTER_TOKEN					= "unregisterToken";			//Old - Apple and Android only, for now.

		public const string METHOD_UPDATE_CALL_SEEN_FLAG				= "updateCallSeenFlag";
		//public const string METHOD_UPDATE_CALL_SEEN_FLAG_BATCH		= "updateCallSeenFlagBatch";	//New - To be implemented?
		public const string METHOD_UPDATE_COMPANY_USER_PROFILE			= "updateCompanyUserProfile";
		//public const string METHOD_UPDATE_CONTACTS					= "updateContacts";				//New - To be implemented when desktop app saves contacts.
		public const string METHOD_UPDATE_MESSAGE_STATUS				= "updateMessageStatus";
        public const string METHOD_UPDATE_MESSAGE_STATUS_BATCH			= "updateMessageStatusBatch";
		//public const string METHOD_UPDATE_PASSWORD					= "updatePassword";				//New - To be implemented?
		//public const string METHOD_UPDATE_VIEW_ATTRIBUTE				= "updateViewAttribute";		//New - To be implemented?

		//public const string METHOD_USE_VOUCHER_CARD					= "useVoucherCard";				//New - To be implemented?
		//public const string METHOD_VALIDATE_CONTACT_SOURCE_HASH		= "validateContactSourceHash";	//New - To be implemented when desktop app saves contacts.


		//DEPRECATED methods
		//public const string METHOD_ADD_BLACKLISTED					= "addBlacklisted";					//DEPRECATED	-- Use setBlacklisted
		//public const string METHOD_ADD_FAVORITE						= "addFavorite";					//DEPRECATED	-- Use setFavorite
		//public const string METHOD_CHECK_COMPANY_USERS				= "checkCompanyUsers";				//DEPRECATED	-- Never implemented, so nothing to replace
		//public const string METHOD_CHECK_FOR_IN_COMPANY_CONTACTS		= "checkForInCompanyContacts"		//DEPRECATED	-- Never implemented, so nothing to replace
		//public const string METHOD_CHECK_NETWORK_CONTACTS				= "checkForInNetworkContacts";		//DEPRECATED	-- Must replace with new getPhoneNumberMeta.
		//public const string METHOD_CHECK_NETWORK_AND_STORE_CONTACTS	= "checkInNetworkAndStoreContacts";	//DEPRECATED	-- Never implemented, so nothing to replace
		//public const string METHOD_GET_CONTACTS						= "getContacts";					//DEPRECATED	-- Use getContactList
		//public const string METHOD_REMOVE_BLACKLISTED					= "removeBlacklisted";				//DEPRECATED	-- Use setBlacklisted
		//public const string METHOD_REMOVE_FAVORITE					= "removeFavorite";					//DEPRECATED	-- Use setFavorite



		//TODO: Move code for SendFax to this class.  Follow VxClient API doc.

		//Hold these for now.  We may need them when we implement Group Messaging.
		//	These are NOT part of VX Client as of 2015.11.30, but Curtis says we will be re-implementing Group Messaging
		//public const string METHOD_CREATE_MESSAGING_GROUP				= "createGroup";
		//public const string METHOD_DELETE_MESSAGING_GROUP				= "deleteGroup";
		//public const string METHOD_GET_MESSAGING_GROUP_AVATAR			= "getGroupAvatar";
		//public const string METHOD_SET_MESSAGING_GROUP_AVATAR			= "setGroupAvatar";
		//public const string METHOD_RENAME_MESSAGING_GROUP				= "renameGroup";
		//public const string METHOD_ADD_MESSAGING_GROUP_MEMBER			= "addMember";
		//public const string METHOD_ADD_MESSAGING_GROUP_MEMBERS		= "addMembers";
		//public const string METHOD_REMOVE_MESSAGING_GROUP_MEMBER		= "removeMember";
		//public const string METHOD_REMOVE_MESSAGING_GROUP_MEMEBERS	= "removeMembers";
		//public const string METHOD_GET_MESSAGING_GROUP_MEMBERS		= "getGroupMembers";
		//public const string METHOD_GET_MESSAGING_GROUPS				= "getGroups";
		//public const string METHOD_SET_MESSAGING_GROUP_MEMBER_NOTIFY	= "setMemberNotify";
		//public const string METHOD_SEND_MESSAGING_GROUP_MESSAGE		= "sendMessage";


        //API Response
        public const int SUCCESS = 0;
        public const int FAILURE = 1;
        public const int EXCEPTION = 2;

		//NOTE TODO JRT: These are all set from Settings during application start-up, as a branding requirement.
		//	I do NOT like this.  ApiServer and ApiKey *may* be branded, but I doubt the xx-ModuleBase properties will be.
		public static string ApiServer					{ get; set; }
//		public static string ApiKey						{ get; set; }

		public static string ClientId					{ get; set; }
		public static string ClientVersion				{ get; set; }

		public static string ApiModuleBase				{ get; set; }
		public static string ApiAuthModuleBase			{ get; set; }
//		public static string CallBackModuleBase			{ get; set; }		//Not used anywhere
		public static string HttpFaxModuleBase			{ get; set; }
		public static string HttpFileDownloadModuleBase { get; set; }
		public static string HttpFileUploadModuleBase	{ get; set; }

		private static bool	mUseHttp = true;	//This may be false for some dev/QA environments.  TODO: implement API server override logic.

        /*
         * A utility method to create full module string based on parameters.
         *
         */
        private static string makeModule(string server, bool useHttps)
        {
            return (useHttps ? "https://" : "http://") + server;		//TODO: We need to override this for some QA test environments
        }

		//private static string makeModuleComplete(string server, string moduleBase, bool useHttps)
		//{
		//	return (useHttps ? "https://" : "http://") + server + moduleBase;		//TODO: We need to override this for some QA test environments
		//}

         /*
         * Standard module URLs are created dynamically using a common public static methods.
         */
        public string getVoxoxModule() { return makeModule(ApiServer, mUseHttp); }
    }
}