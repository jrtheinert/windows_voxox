﻿using Desktop.Utils;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Vxapi23.Base;
using Vxapi23.Constants;
using Vxapi23.Model;
using Vxapi23.Util;

namespace Vxapi23
{
    public class RestfulAPIManager : BaseService
    {
        private VXLogger logger = VXLoggingManager.INSTANCE.GetLogger("RestfulAPIManager");

        public static RestfulAPIManager INSTANCE = new RestfulAPIManager();

//        public string apiKey { get; set; }
        public string Server { get; set; }

        private RestfulAPIManager()
        {
			//if (apiKey != null)
			//	RestfulConstants.ApiKey = apiKey; //initialize the keys
        }

		private string getVoxoxModule()
		{
			return RestfulConstants.INSTANCE.getVoxoxModule();
		}

		private string getApiModuleBase()
		{
			return RestfulConstants.ApiModuleBase;
		}

		private void parseUrl( String url, out String voxoxModule, out String moduleBase )
		{
			//Defaults, in case URL is invalid.
            voxoxModule = getVoxoxModule();
			moduleBase  = RestfulConstants.ApiAuthModuleBase;

			if ( url != null )
			{
				System.Uri temp = new System.Uri( url );

				if ( temp != null )
				{
					voxoxModule = temp.AbsoluteUri.Replace( temp.PathAndQuery, "" );
					moduleBase  = temp.AbsolutePath.Substring( 1 ) + "/rest";	//Drop leading slash.
				}
			}
		}

		#region | NOT part of VX Client API |

		//-----------------------------------------------------------
		// vxAuth APIs - These API calls are NOT part of vxClient API 
		//-----------------------------------------------------------

        public VXResult<GetInitOptionsResponse> GetInitialOptions()
        {
            try
            {
                Dictionary<string, string> parameters = RestfulApiHelper.getGetInitOptionsParameters( true );

                var result = InvokeRestMethodAsync<GetInitOptionsResponse>(getVoxoxModule(), RestfulConstants.ApiAuthModuleBase, parameters);
                logger.Info("Result type : " + result.GetType());

				if (result.Result != null)
				{
					var response = result.Result;
//					logger.Debug("Login: Status=" + response.Status + ", DID=" + response.did);
					return new VXResult<GetInitOptionsResponse> { Success = true, Data = result.Result };
				}
				else
				{
					logger.Warn("Login failed.");
					return new VXResult<GetInitOptionsResponse> { Success = false, ErrorMessage = "Invalid Credentials" };
				}
            }
            catch (NetworkConnectionException e)
            {
                return new VXResult<GetInitOptionsResponse> { Success = false, IsNetworkError = true, ErrorMessage = e.Message };
            }
            catch (Exception e)
            {
                logger.Error("Login failed:", e);
                if (e.InnerException as NetworkConnectionException != null) // Because of aysnc call the NetworkConnectionException thrown in Base Service might be available as InnerException
                {
                    return new VXResult<GetInitOptionsResponse> { Success = false, IsNetworkError = true, ErrorMessage = e.InnerException.Message };
                }
                else
                {
                    return new VXResult<GetInitOptionsResponse> { Success = false };
                }
            }
		}


        public VXResult<AuthenticateResponse> Authenticate( string username, string password, string partnerIdentifier, string url )
        {
            try
            {
                Dictionary<string, string> parameters = RestfulApiHelper.getLoginParameters(username, password, partnerIdentifier, true );

                string voxoxModule;
				string moduleBase;

				parseUrl( url, out voxoxModule, out moduleBase );

                var result = InvokeRestMethodAsync<AuthenticateResponse>(voxoxModule, moduleBase, parameters);
                logger.Info("Result type : " + result.GetType());

                if (result.Result != null)
                {
                    var authResponse = result.Result;
                    logger.Debug("Login: Status=" + authResponse.Status + ", DID=" + authResponse.did);
                    return new VXResult<AuthenticateResponse> { Success = true, Data = result.Result };
                }
                else
                {
                    logger.Warn("Login failed.");
                    return new VXResult<AuthenticateResponse> { Success = false, ErrorMessage = "Invalid Credentials" };
                }
            }
            catch (NetworkConnectionException e)
            {
                return new VXResult<AuthenticateResponse> { Success = false, IsNetworkError = true, ErrorMessage = e.Message };
            }
            catch (Exception e)
            {
                logger.Error("Login failed:", e);
                if (e.InnerException as NetworkConnectionException != null) // Because of aysnc call the NetworkConnectionException thrown in Base Service might be available as InnerException
                {
                    return new VXResult<AuthenticateResponse> { Success = false, IsNetworkError = true, ErrorMessage = e.InnerException.Message };
                }
                else
                {
                    return new VXResult<AuthenticateResponse> { Success = false };
                }
            }
        }

		//Not called anywhere.
		//public PingResponse Ping()
		//{
		//	string messageToSend = "ping";
		//	Dictionary<string, string> parameters = RestfulApiHelper.getPingParameters(messageToSend);
		//	if (logger.IsDebugEnabled())
		//	{
		//		logger.Debug("RestfulConstants.getVoxoxModule(): " + getVoxoxModule());
		//		foreach (KeyValuePair<string, string> param in parameters)
		//		{
		//			logger.Debug("RestfulApiHelper.getPingParameters: " + param.Key + " = " + param.Value);
		//		}
		//	}

		//	try
		//	{
		//		var result = InvokeRestMethodAsync<PingResponse>( getVoxoxModule(), getApiModuleBase(), parameters);
		//		if (result.Result != null)
		//		{
		//			PingResponse pingResp = result.Result;
		//			logger.Debug("Ping: Status=" + pingResp.Status + ", Resp=" + pingResp.Response);
		//			return result.Result;
		//		}
		//		else
		//		{
		//			logger.Warn("Ping failed.");
		//			return null;
		//		}
		//	}
		//	catch (Exception e)
		//	{
		//		logger.Error("Ping failed:", e);
		//		return null;
		//	}
		//}

        public bool isValidUser(AuthenticateResponse authResponse)
        {
            if (authResponse != null && authResponse.did != null)
            {
                return true;
            }
            return false;
        }

        public async Task<T> NotifyHTTPListeners<T>(string companyUserId, string userkey, string httpListenerId, string function, string dtmfDigits, Dictionary<string, string> parametersIn) where T : new()
        {
            T httpListenerList = new T();

            try
            {
                Dictionary<string, string> parameters = RestfulApiHelper.getNotifyHttpListenerParameters(companyUserId, userkey, httpListenerId, function, dtmfDigits, true );

                if(parametersIn != null)
                {
                    foreach(var kv in parametersIn)
                    {
                        if(parameters.ContainsKey(kv.Key) == false)
                            parameters.Add(kv.Key, kv.Value);
                    }
                }

                httpListenerList = await InvokeRestMethodAsync<T>( getVoxoxModule(), RestfulConstants.ApiModuleBase, parameters );

                if (httpListenerList != null)
                {
                    return httpListenerList;
                }
                else
                {
                    logger.Warn("Result of NotifyHttpListener is null");
                    return default(T);
                }
            }
            catch (Exception e)
            {
                logger.Error("failed to get NotifyHttpListener Response:", e);
                return default(T);
            }
        }

		//Not called anywhere
		//public List<CallConnectResponse> Callconnect(string companyUserId, string userKey, string username, string function, string from, string to)
		//{
		//	List<CallConnectResponse> callBackList = new List<CallConnectResponse>();
		//	try
		//	{
		//		Dictionary<string, string> param = RestfulApiHelper.getCallConnectParameters(companyUserId, userKey, username, from, to);

		//		var result = InvokeRestMethodAsync<List<CallConnectResponse>>( getVoxoxModule(), RestfulConstants.CallbackModuleBase, param);
		//		if (result.Result != null)
		//		{
		//			callBackList = result.Result;
		//			return callBackList;
		//		}
		//		else
		//		{
		//			logger.Warn("Result of Call connect response is null");
		//			return null;
		//		}
		//	}
		//	catch (Exception e)
		//	{
		//		logger.Error("failed to get Call connect Response:", e);
		//		return null;
		//	}
		//}

		//This is NOT part of VX Client API, so the parameters are a little different
        public async Task<SendFaxResponse> SendFax(string userKey, string companyUserId, string partnerId, string toNumber, string fromNumber, string filePath)
        {
            try
            {
                Dictionary<string, string> parameters = RestfulApiHelper.getSendFaxParameters(userKey, companyUserId, partnerId, toNumber, fromNumber, false );	//Not VX

				var result = await InvokeRestMethodAsync<SendFaxResponse>( getVoxoxModule(), RestfulConstants.HttpFaxModuleBase, parameters, filePath);

                if (result != null)
                {
                    return result;
                }
                else
                {
                    logger.Warn("Result of SendFax is null");
                    return null;
                }
            }
            catch (Exception ex)
            {
                logger.Error("Failed to get SendFax: ", ex);
                return null;
            }
        }

		//This is NOT part of VX Client API, so the parameters are a little different
        public async Task<SendFileResponse> SendFile(string userKey, string companyUserId, string filePath)
        {
            try
            {
                Dictionary<string, string> parameters = RestfulApiHelper.getSendFileParameters( userKey, companyUserId, filePath, true );

                var result = await InvokeRestMethodAsync<SendFileResponse>( getVoxoxModule(), RestfulConstants.HttpFileUploadModuleBase, parameters, filePath);

                if (result != null)
                {
                    return result;
                }
                else
                {
                    logger.Warn("Result of SendFileResponse is null");
                    return null;
                }
            }
            catch (Exception ex)
            {
                logger.Error("Failed to get SendFileResponse: ", ex);
                return null;
            }
        }

		public string formatDownloadFileUrl( string userKey, string companyUserId, int svrMsgType, string msgId )
		{
            Dictionary<string, string> parameters = RestfulApiHelper.getDownloadFileParameters( userKey, companyUserId, svrMsgType, msgId, false );	//Not VX

			string result = RestfulApiHelper.toUrl( getVoxoxModule(), RestfulConstants.HttpFileDownloadModuleBase, parameters );

			return result;
		}

		#endregion


		#region | VX Client API methods |

		//------------------------------------------------------------
		//These methods are part of the VX Client API 
		//------------------------------------------------------------

		public ContactSourcesResponse getContactSources( string userKey, string companyUserId )
        {
            ContactSourcesResponse sources = new ContactSourcesResponse();

            try
            {
                Dictionary<string, string> parameters = RestfulApiHelper.getContactSourcesParameters(userKey, companyUserId, true );

				var result = InvokeRestMethodAsync<ContactSourcesResponse>( getVoxoxModule(), getApiModuleBase(), parameters);

                if (result.Result != null)
                {
                    sources = result.Result;
                    return sources;
                }
                else
                {
                    logger.Warn("Getting ContactSources failed (result null)");
                    return null;
                }
            }
            catch (Exception e)
            {
                logger.Error("failed to get contactSources:", e);
                return null;
            }
        }

		public ContactListResponse getContactList( string userKey, string companyUserId, string sourceKey )
        {
            ContactListResponse contactListResponse = new ContactListResponse();

            try
            {
                Dictionary<string, string> parameters = RestfulApiHelper.getContactListParameters(userKey, companyUserId, true, sourceKey );

				var result = InvokeRestMethodAsync<ContactListResponse>( getVoxoxModule(), getApiModuleBase(), parameters);

                if (result.Result != null)
                {
                    contactListResponse = result.Result;
                    return contactListResponse;
                }
                else
                {
                    logger.Warn("Getting ContactList failed (result null)");
                    return null;
                }
            }
            catch (Exception e)
            {
                logger.Error("failed to get contactList:", e);
                return null;
            }
        }

		public PhoneMetaInfoResponse getPhoneMetaInfo( string userKey, string companyUserId, string[] phoneNumbers )
        {
            PhoneMetaInfoResponse phoneMetaInfo = new PhoneMetaInfoResponse();

            try
            {
                Dictionary<string, string> parameters = RestfulApiHelper.getPhoneMetaInfoParameters(userKey, companyUserId, true, phoneNumbers );

				var result = InvokeRestMethodAsync<PhoneMetaInfoResponse>( getVoxoxModule(), getApiModuleBase(), parameters);

                if (result.Result != null)
                {
                    phoneMetaInfo = result.Result;
                    return phoneMetaInfo;
                }
                else
                {
                    logger.Warn("Getting PhoneMetaInfo failed (result null)");
                    return null;
                }
            }
            catch (Exception e)
            {
                logger.Error("failed to get PhoneMetaInfo:", e);
                return null;
            }
        }

		public CompanyDirectoryResponse getCompanyDirectory( string userKey, string companyUserId )
        {
            CompanyDirectoryResponse response = new CompanyDirectoryResponse();

            try
            {
                Dictionary<string, string> parameters = RestfulApiHelper.getCompanyDirectoryParameters(userKey, companyUserId, true );

				var result = InvokeRestMethodAsync<CompanyDirectoryResponse>( getVoxoxModule(), getApiModuleBase(), parameters);

                if ( result.Result != null)
                {
                    response = result.Result;
                    return response;
                }
                else
                {
                    logger.Warn("Getting getCompanyDirectory failed (result null)");
                    return null;
                }
            }
            catch (Exception e)
            {
                logger.Error("failed to get getCompanyDirectory:", e);
                return null;
            }
        }

		public SetBlacklistedResponse setBlacklisted( string userKey, string companyUserId, string phoneNumber, bool blacklist )
        {
            SetBlacklistedResponse response = new SetBlacklistedResponse();

            try
            {
                Dictionary<string, string> parameters = RestfulApiHelper.getSetBlacklistedParameters(userKey, companyUserId, true, phoneNumber, blacklist );

				var result = InvokeRestMethodAsync<SetBlacklistedResponse>( getVoxoxModule(), getApiModuleBase(), parameters);

                if (result.Result != null)
                {
                    response = result.Result;
                    return response;
                }
                else
                {
                    logger.Warn("setBlacklisted() failed (result null)");
                    return null;
                }
            }
            catch (Exception e)
            {
                logger.Error("failed to setBlacklisted:", e);
                return null;
            }
        }

		public SetFavoriteResponse setFavorite( string userKey, string companyUserId, string phoneNumber, bool favorite )
        {
            SetFavoriteResponse response = new SetFavoriteResponse();

            try
            {
                Dictionary<string, string> parameters = RestfulApiHelper.getSetFavoriteParameters(userKey, companyUserId, true, phoneNumber, favorite );

				var result = InvokeRestMethodAsync<SetFavoriteResponse>( getVoxoxModule(), getApiModuleBase(), parameters);

                if (result.Result != null)
                {
                    response = result.Result;
                    return response;
                }
                else
                {
                    logger.Warn("setFavorite() failed (result null)");
                    return null;
                }
            }
            catch (Exception e)
            {
                logger.Error("failed to setFavorite:", e);
                return null;
            }
        }

        public CompanyUserExtendedResponse getCompanyUserExtended(string userKey, string companyUserId)
        {
            CompanyUserExtendedResponse companyUserExtendedResponseResponse = null;
            try
            {
                Dictionary<string, string> parameters = RestfulApiHelper.getCompanyUserExtendedParameters(userKey, companyUserId, true );

				var result = InvokeRestMethodAsync<CompanyUserExtendedResponse>( getVoxoxModule(), getApiModuleBase(), parameters);

                if (result.Result != null)
                {
                    companyUserExtendedResponseResponse = result.Result;
                    return companyUserExtendedResponseResponse;
                }
                else
                {
                    logger.Warn("getCompanyUserExtendedParameters failed (result null)");
                    return null;
                }
            }
            catch (Exception e)
            {
                logger.Error("getCompanyUserExtendedParameters failed:", e);
                return null;
            }

        }

        public List<SMSResponse> sendSMS(string userKey, string fromnumber, string to, string body, string companyUserId, string msgId )
        {
            List<SMSResponse> smsResponseList = new List<SMSResponse>();
            try
            {
                Dictionary<string, string> parameters = RestfulApiHelper.getSendSMSParameters(fromnumber, to, body, userKey, companyUserId, msgId, true );

				var result = InvokeRestMethodAsync<List<SMSResponse>>( getVoxoxModule(), getApiModuleBase(), parameters);
                if (result.Result != null)
                {
                    smsResponseList = result.Result;
                    return smsResponseList;
                }
                else
                {
                    logger.Warn("Sending SMS failed (result null)");
                    return null;
                }
            }
            catch (Exception e)
            {
                logger.Error("Sending SMS failed:", e);
                return null;
            }
        }

        public List<DataSummaryResponse> GetDataSummary(string userKey, string companyUserId)
        {
            List<DataSummaryResponse> dataSummaryList = new List<DataSummaryResponse>();
            try
            {
                Dictionary<string, string> parameters = RestfulApiHelper.getDataSummaryParameters(userKey, companyUserId, true );

				var result = InvokeRestMethodAsync<List<DataSummaryResponse>>( getVoxoxModule(), getApiModuleBase(), parameters);

                if (result.Result != null)
                {
                    dataSummaryList = result.Result;
                    return dataSummaryList;
                }
                else
                {
                    logger.Warn("Result of DataSummaryResponse is null");
                    return null;
                }
            }
            catch (Exception e)
            {
                logger.Error("failed to get DataSummary Response:", e);
                return null;
            }

        }

        public List<MessageHistoryResponse> GetMessageHistory(string userKey, string companyUserId, string contactFilter, string contactAddress, string timestamp, int limit, int offset)
        {
            List<MessageHistoryResponse> messageHistoryList = new List<MessageHistoryResponse>();
            try
            {
                Dictionary<string, string> parameters = RestfulApiHelper.getMessageHistoryParameters(userKey, companyUserId, true, timestamp, limit, contactFilter, contactAddress );
                
                var result = InvokeRestMethodAsync<List<MessageHistoryResponse>>( getVoxoxModule(), getApiModuleBase(), parameters);
                if (result.Result != null)
                {
                    messageHistoryList = result.Result;
                    return messageHistoryList;
                }
                else
                {
                    logger.Warn("Result of MessageHistoryResponse is null");
                    return null;
                }
            }
            catch (Exception e)
            {
                logger.Error("failed to get MessageHistoryResponse:", e);
                return null;
            }

        }

        public bool UpdateMessageStatus(string userKey, string companyUserId, int msgType, string msgId, int msgStatus, bool useJson )
        {
           bool result = false;

            try
            {
                Dictionary<string, string> parameters = RestfulApiHelper.updateMessageStatusParameters(userKey, companyUserId, msgType, msgId, msgStatus, useJson );

                var response = InvokeRestMethodAsync<UpdateMessageStatusResponse>( getVoxoxModule(), getApiModuleBase(), parameters);

                if (response.Result != null)
                {
					result = BaseApiResponse.stringToBool( response.Result.Response );
                }
                else
                {
                    logger.Warn("Result of UpdateMessageStatus is null");
                }
            }
            catch (Exception e)
            {
                logger.Error("failed to get UpdateMessageStatus:", e);
            }

			return result;
        }

        public List<UpdatedMessage> updateMessageStatusBatch(string userkey, string companyUserID, List<DeletedMessagesToServer> messages)
        {
            List<UpdatedMessage> responseList = new List<UpdatedMessage>();

            try{
                    Dictionary<string, string> parameters = RestfulApiHelper.getUpdateMessageStatusBatchParameters(userkey, companyUserID, messages, true );

                    var result = InvokeRestMethodAsync<List<UpdatedMessage>>(getVoxoxModule(), getApiModuleBase(), parameters);
                    if (result.Result != null)
                        responseList = result.Result;
                }catch(Exception ex){
                    Console.WriteLine("Result of UpdateMessageStatusBatch is null" + ex);
               }

            return responseList;
        }


        public List<CallHistoryResponse> GetCallHistory(string userKey, string companyUserId, string timestamp, int limit)
        {
            List<CallHistoryResponse> callHistoryList = new List<CallHistoryResponse>();
            try
            {
                Dictionary<string, string> parameters = RestfulApiHelper.getCallHistoryParameters(userKey, companyUserId, true, timestamp, limit );
                

                var result = InvokeRestMethodAsync<List<CallHistoryResponse>>( getVoxoxModule(), getApiModuleBase(), parameters);
                
				if (result.Result != null)
                {
                    callHistoryList = result.Result;
                    return callHistoryList;
                }
                else
                {
                    logger.Warn("Result of CallHistoryResponse is null");
                    return null;
                }
            }
            catch (Exception e)
            {
                logger.Error("failed to get CallHistoryResponse:", e);
                return null;
            }
        }

        public async Task<UpdateCallSeenFlagResponse> UpdateCallSeenFlag( string userKey, string companyUserId, string callUid, int seenFlag, bool useJson )
        {
            UpdateCallSeenFlagResponse response = null;

            try
            {
                Dictionary<string, string> parameters = RestfulApiHelper.updateCallSeenFlagParameters( userKey, companyUserId, callUid, seenFlag, true );

				//TODO: We are getting invalid JSON response in any case.  XML looks OK.
                response = await InvokeRestMethodAsync<UpdateCallSeenFlagResponse>( getVoxoxModule(), getApiModuleBase(), parameters);

                if ( response != null)
                {
					response.Uid = callUid;
                }
                else
                {
                    logger.Warn("Result of UpdateCallSeenFlag is null");
                }
            }
            catch (Exception e)
            {
                logger.Error("failed to get UpdateCallSeenFlag:", e);
            }

			return response;
        }

		//Not called anywhere outside unit test.
		//public List<PlanInfoResponse> GetPlanInfo(string userKey, string companyUserId)
		//{
		//	List<PlanInfoResponse> planInfoList = new List<PlanInfoResponse>();
		//	try
		//	{
		//		Dictionary<string, string> parameters = RestfulApiHelper.getPlanInfoParameters(userKey, companyUserId);

		//		var result = InvokeRestMethodAsync<List<PlanInfoResponse>>( getVoxoxModule(), getApiModuleBase(), parameters);
		//		if (result.Result != null)
		//		{
		//			planInfoList = result.Result;
		//			return planInfoList;
		//		}
		//		else
		//		{
		//			logger.Warn("Result of PlanInfoResponse is null");
		//			return null;
		//		}
		//	}
		//	catch (Exception e)
		//	{
		//		logger.Error("failed to get PlanInfoResponse:", e);
		//		return null;
		//	}
		//}

        public GetFindMeNumbersResponse GetFindMeNumbers(string userKey, string companyUserId)
        {
            GetFindMeNumbersResponse findMeNumberList = new GetFindMeNumbersResponse();
            try
            {
                Dictionary<string, string> parameters = RestfulApiHelper.getFindMeNumbersParameters(userKey, companyUserId, true );

				var result = InvokeRestMethodAsync<GetFindMeNumbersResponse>( getVoxoxModule(), getApiModuleBase(), parameters);

                if (result.Result != null)
                {
                    findMeNumberList = result.Result;
                    return findMeNumberList;
                }
                else
                {
                    logger.Warn("Result of FindMeNumbers is null");
                    return null;
                }
            }
            catch (Exception e)
            {
                logger.Error("failed to get FindMeNumbers Response:", e);
                return null;
            }
        }

		//DEPRECATED
		//public async Task<CheckInNetworkContactResponse> CheckInNetworkContact(string userKey, string companyUserId, string partnerId, string[] phoneNumbers )
		//{
		//	CheckInNetworkContactResponse responseResult = new CheckInNetworkContactResponse();
		//	try
		//	{
		//		Dictionary<string, string> parameters = RestfulApiHelper.getCheckForInNetworkContactsParameters(userKey, companyUserId, partnerId, phoneNumbers, true );

		//		var result = await InvokeRestMethodAsync<Dictionary<string, NetworkContact>>( getVoxoxModule(), getApiModuleBase(), parameters);
		//		if (result != null)
		//		{
		//			responseResult.Contacts = result;
		//			return responseResult;
		//		}
		//		else
		//		{
		//			logger.Warn("Result of checkInNetworkContact is null");
		//			return null;
		//		}
		//	}
		//	catch (Exception e)
		//	{
		//		logger.Error("failed to get checkInNetworkContact Response:", e);
		//		return null;
		//	}
		//}


        /// <summary>
        /// Updates the user profile information
        /// </summary>
        /// <param name="userKey"></param>
        /// <param name="companyUserId"></param>
        /// <param name="firstName">Updated Firstname</param>
        /// <param name="lastName">Updated lastname</param>
        /// <param name="email">Updated email</param>
        /// <returns>True if success else false</returns>
        public async Task<bool> UpdateCompanyUserProfile(string userKey, string companyUserId, string firstName, string lastName, string email)
        {
            try
            {
                Dictionary<string, string> parameters = RestfulApiHelper.getUpdateCompanyUserProfileParameters(userKey, companyUserId, firstName, lastName, email, true );

				var result = await InvokeRestMethodAsync<DataSummaryResponse>(getVoxoxModule(), getApiModuleBase(), parameters);
                
                if (result != null && result.userId == companyUserId)
                {
                    return true;
                }
                else
                {
                    logger.Error("UpdateComapanyUserProfile failed");
                    return false;
                }
            }
            catch (Exception e)
            {
                logger.Error("UpdateCompanyUserProfile failed:", e);
                return false;
            }
		}


        /// <summary>
        /// Retrieve phone number for given CompanyUserId
        /// </summary>
        /// <param name="userKey"></param>
        /// <param name="companyUserId"></param>
        /// <param name="contactUserId">UserId of contact we looking up</param>
        /// <param name="useJson">true == json, false == xml</param>
        /// <returns>phoneNumber on success</returns>
        public string RetrieveCompanyPhoneByUser( string userKey, string companyUserId, string contactUserId, bool useJson )
        {
            try
            {
                Dictionary<string, string> parameters = RestfulApiHelper.getCompanyPhoneByUserParameters( userKey, companyUserId, contactUserId, useJson );

				var result = InvokeRestMethodAsync<CompanyPhoneByUserResponse>( getVoxoxModule(), getApiModuleBase(), parameters);
                
                if (result != null)
                {
                    return result.Result.number;
                }
                else
                {
                    logger.Warn("Result of SendFileResponse is null");
                    return null;
                }
            }
            catch (Exception ex)
            {
                logger.Error("Failed to get SendFileResponse: ", ex);
                return null;
            }

		}

        public async Task<CompanyUserOptionsResponse> GetCompanyUserOptions(string userKey, string companyUserId)
        {
            CompanyUserOptionsResponse responseResult = new CompanyUserOptionsResponse();
            try
            {
                Dictionary<string, string> parameters = RestfulApiHelper.getCompanyUserOptionsParameters(userKey, companyUserId, true);

                var result = await InvokeRestMethodAsync<CompanyUserOptionsResponse>(getVoxoxModule(), getApiModuleBase(), parameters);
                if (result != null)
                {
                    responseResult = result;
                    return responseResult;
                }
                else
                {
                    logger.Warn("Result of getCompanyUserOptions is null");
                    return null;
                }
            }
            catch (Exception e)
            {
                logger.Error("failed to get getCompanyUserOptions Response:", e);
                return null;
            }
        }

        /// <summary>
        /// Sets the Caller ID
        /// </summary>
        /// <param name="userKey"></param>
        /// <param name="companyUserId"></param>
        /// <param name="callerId">callerId to set</param>
        /// <param name="singleUse">If true the callerId will be saved as a single use callerId.</param>
        /// <param name="useJson">true == json, false == xml</param>
        /// <returns></returns>
        public async Task<SetCallerIdResponse> SetCallerId(string userKey, string companyUserId, string callerId, bool singleUse, bool useJson)
        {
            SetCallerIdResponse response = null;

            try
            {
                Dictionary<string, string> parameters = RestfulApiHelper.getSetCallerIdParameters(userKey, companyUserId, callerId, singleUse, true);

                response = await InvokeRestMethodAsync<SetCallerIdResponse>(getVoxoxModule(), getApiModuleBase(), parameters);

                if (response != null)
                {
                    
                }
                else
                {
                    logger.Warn("Result of SetCallerId is null");
                }
            }
            catch (Exception e)
            {
                logger.Error("failed to get SetCallerId:", e);
            }

            return response;
        }

		#endregion


		#region | Old Json Parsing Code |

		//public RichTextMessage GetRichTextMessage(string userKey, string companyUserId)
        //{
        //    RichTextMessage textMessage = new RichTextMessage();
        //    try
        //    {
        //        Dictionary<string, string> parameters = RestfulApiHelper.getRichTextMessageParameters(userKey, companyUserId);

		//        var result = InvokeRestMethodAsync<RichTextMessage>( getVoxoxModule(), getApiModuleBase(), parameters);
        //        if (result.Result != null)
        //        {
        //            textMessage = result.Result;
        //            return textMessage;
        //        }
        //        else
        //        {
        //            logger.Warn("Result of rich text message is null");
        //            return null;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        logger.Error("failed to get rich text message Response:", e);
        //        return null;
        //    }
        //}

        //// This message is Generic for RichText Message takes JsonString as input parameter and the RichTextMessage object contains all the given fields.
        //// Fields which are not related to the call will remain null. for Example Latitude and Longitude fields will remain null for Audio response.
        //public RichTextMessage GetRichTextMessage(string jsonString)
        //{
        //    RichTextMessage textMessage = new RichTextMessage();
        //    try
        //    {
        //        if (textMessage != null)
        //        {
        //            textMessage = DeserializeJsonString<RichTextMessage>(jsonString);
        //            return textMessage;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        logger.Error("failed to get rich text message Response:", e);
        //        return null;
        //    }
        //    return null;
        //}

        //public RichTextAudioResponse GetRichTextAudioMessage(String jsonString)
        //{
        //    RichTextAudioResponse textMessage = new RichTextAudioResponse();
        //    try
        //    {
        //        if (textMessage != null)
        //        {
        //            textMessage = DeserializeJsonString<RichTextAudioResponse>(jsonString);
        //            return textMessage;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        logger.Error("failed to get rich text message Response:", e);
        //        return null;
        //    }
        //    return null;
        //}

        //public RichTextContactResponse GetRichTextContactMessage(String jsonString)
        //{
        //    RichTextContactResponse textMessage = new RichTextContactResponse();
        //    try
        //    {
        //        if (textMessage != null)
        //        {
        //            textMessage = DeserializeJsonString<RichTextContactResponse>(jsonString);
        //            return textMessage;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        logger.Error("failed to get rich text message Response:", e);
        //        return null;
        //    }
        //    return null;
        //}

        //public RichTextLocationResponse GetRichTextLocationMessage(String jsonString)
        //{
        //    RichTextLocationResponse textMessage = new RichTextLocationResponse();
        //    try
        //    {
        //        if (textMessage != null)
        //        {
        //            textMessage = DeserializeJsonString<RichTextLocationResponse>(jsonString);
        //            return textMessage;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        logger.Error("failed to get rich text message Response:", e);
        //        return null;
        //    }
        //    return null;
        //}

        //public RichTextMediaResponse GetRichTextMediaMessage(String jsonString)
        //{
        //    RichTextMediaResponse textMessage = new RichTextMediaResponse();
        //    try
        //    {
        //        if (textMessage != null)
        //        {
        //            textMessage = DeserializeJsonString<RichTextMediaResponse>(jsonString);
        //            return textMessage;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        logger.Error("failed to get rich text message Response:", e);
        //        return null;
        //    }
        //    return null;
        //}

        #endregion
    }
}