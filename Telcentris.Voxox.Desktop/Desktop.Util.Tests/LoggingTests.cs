﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Desktop.Utils;

namespace Desktop.Utils.Tests
{
    [TestClass]
    public class LoggingTests
    {
        [TestMethod]
        public void TestInitLogging() 
        {
            VXLoggingManager.INSTANCE.Setup(@"Tests");
            VXLogger log1 = VXLoggingManager.INSTANCE.GetLogger("classname1");
            log1.Debug("Log1 Debug test");
            log1.Info("Log1 Info test");
            log1.Warn("Log1 Warn test");
            log1.Error("Log1 error test");
            VXLogger log2 = VXLoggingManager.INSTANCE.GetLogger("classname2");
            log1.Debug("Log2 Debug test");
            log1.Info("Log2 Info test");
            log1.Warn("Log2 Warn test");
            log1.Error("Log2 error test");
            VXLogger log3 = VXLoggingManager.INSTANCE.GetLogger("classname3");
            log1.Debug("Log3 Debug test");
            log1.Info("Log3 Info test");
            log1.Warn("Log3 Warn test");
            log1.Error("Log3 error test");
            Assert.IsTrue(VXLoggingManager.INSTANCE.initSuccess());
        }

        //[TestMethod]
		//public void TestRegistryManager()
		//{
		//	VXLoggingManager.INSTANCE.Setup(@"Tests");
		//	VXLogger logger = VXLoggingManager.INSTANCE.GetLogger("UtilTests");
		//	VXRegistryManager regMgr = new VXRegistryManager("SOFTWARE\\Telcentris\\Voxox\\Desktop");
		//	if(!regMgr.SubkeyExists())
		//	{
		//		if (regMgr.CreateSubkey())
		//		{
		//			logger.Debug("Subkey created successfully.");                    
		//		}
		//		else
		//			logger.Error("Unable to create subkey!");
		//	}
		//	regMgr.Write("Version", "1.0");
		//	string strVer = regMgr.Read("Version").ToString();
		//	Assert.IsNotNull(strVer);
		//}
    }
}
