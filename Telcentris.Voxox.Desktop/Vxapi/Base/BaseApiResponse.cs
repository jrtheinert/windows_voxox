﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vxapi.Base
{
    public class BaseApiResponse
    {
        private string status;      //CODE-REVIEW: Does this conflict with the 'Status' property?
//      private string message;     //CODE-REVIEW: Does this conflict with the 'Message' property?  //CODE-REVIEW: never used
        private bool success;

        //CODE-REVIEW: comment fields never used.
//        private string code;        //CODE-REVIEW: Does this conflict with the 'Status' property?
//        private string response;		//This is from XML <response>   //CODE-REVIEW: Does this conflict with the 'Status' property?
//        private string type;        //CODE-REVIEW: Does this conflict with the 'Status' property?

        public BaseApiResponse()
        {
		    success = false;
	    }

        //These are/should be in all API responses.
        public string Message { get; set; }
        public string Status { get; set; }

        //We add the following simple types so we don't need more VoxoxApiResponse-derived classes
        public string Code { get; set; }
        public string Response { get; set; }
        public string Type { get; set; }

        public void setStatus(string val)
        {
            status = val;
            success = status.Equals("success", StringComparison.CurrentCultureIgnoreCase);	//Appropriate use of "success".
        }

        public bool succeeded() { return success; }
    }
}
