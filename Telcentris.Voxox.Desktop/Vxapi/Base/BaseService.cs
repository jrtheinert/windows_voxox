﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using RestSharp;
using Desktop.Utils;

namespace Vxapi.Base
{    
    /// <summary>
    /// The BaseService class provides methods to make RestFul API calls to Voxox
    /// NOC or services. For RestFul calls it uses RestSharp. ( http://restsharp.org/ Apache License 2.0)
    /// </summary>
    public class BaseService
    {
        //Instance of Logger
        private static VXLogger baseSvclogger = VXLoggingManager.INSTANCE.GetLogger("Vxapi.BaseService");

        /// <summary>
        /// Invokes the RestFul method and converts the response to the TypeParam.
        /// </summary>
        /// <typeparam name="T">The return type expected, usually one of the Model objects</typeparam>
        /// <param name="serviceUrl">The base URL for the Restful service</param>
        /// <param name="uri">The URI for the Restful call</param>
        /// <param name="methodName">The method name being called</param>
        /// <param name="parameters">The parameters to send\post</param>
        /// <returns>Task<T></returns>
        protected async Task<T> InvokeRestMethodAsync<T>(string serviceUrl, string uri, Dictionary<string, string> parameters) where T : new()
        {
            var client = new RestClient(serviceUrl);
            var request = new RestRequest(uri, Method.POST);
            foreach(KeyValuePair<string, string> param in parameters)
            {
                request.AddParameter(param.Key, param.Value);
                if (baseSvclogger.IsDebugEnabled())
                    baseSvclogger.Debug("Param: Key=" + param.Key + ", Value=" + param.Value);
            }
            // execute the request
            IRestResponse response = await client.ExecuteTaskAsync(request);
            if (baseSvclogger.IsDebugEnabled())
            {
                baseSvclogger.Debug("Content-Type:" + response.ContentType);
                baseSvclogger.Debug("Content:" + response.Content);
            }
            if (response.ContentType.Contains("xml"))
                return (new RestSharp.Deserializers.XmlDeserializer()).Deserialize<T>(response);
            else
                return (new RestSharp.Deserializers.JsonDeserializer()).Deserialize<T>(response);
        }

        //CODE-REVIEW: the code above and below is essentially the same except for the Execute() vs. ExecuteTaskAsync().  

        /// <summary>
        /// Invokes the RestFul method and return the raw payload back as string.
        /// </summary>        
        /// <param name="parameters">The parameters to send\post</param>        
        /// <param name="serviceUrl"></param>
        /// <param name="uri"></param>
        /// <param name="parameters"></param>
        /// <returns>string payload</returns>
        protected async Task<string> InvokeRestMethodAsync(string serviceUrl, string uri, Dictionary<string, string> parameters)
        {
            var client = new RestClient(serviceUrl);
            var request = new RestRequest(uri, Method.POST);
            foreach (KeyValuePair<string, string> param in parameters)
            {
                request.AddParameter(param.Key, param.Value);
            }
            // execute the request
            IRestResponse response = await client.ExecuteTaskAsync(request);
            baseSvclogger.Debug("Content-Type:" + response.ContentType);
            baseSvclogger.Debug("Content:" + response.Content);
            return response.Content;
        }
        //TODO Need to add file upload method(s)
    }
}
