﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vxapi.Constants
{
    class RestfulConstants
    {
        private RestfulConstants() { } // prevents instantiation

        //URI Host and Port of the XMPP & REST API Servers :        
        private const string VOXOX_XMPP_SERVER = "voxox.com";    //CODE-REVIEW:  VOXOX_XMPP_SERVER and JID_VOXOX_SERVER_PORTION have same value, but no comment on where is used or why we need two.
        private const int VOXOX_XMPP_PORT = 5222;
        public const string JID_VOXOX_SERVER_PORTION = "voxox.com";
        private const string VOXOX_API_REST_SERVER = "api.telcentris.com";
        private const string VOXOX_WEB_SERVER = "my.voxox.com";
        private const string VOXOX_SIP_SERVER = "Sip server (from API)"; // if not overwritten will be used from API.

        /*
           URI Path (Modules) - By convention, they should all have :	 	
            - leading forward-slash ( / )
            - Generally end with question mark.
            - No spaces, leading, trailing or embedded.
            This is what the rest of this class and application will expect.
        */
        public const string VOXOX_MODULE_BASE = "voxox/rest/?";				//TODO JRT - Why /? vs. ? in these values?
        public const string SMS_VERIFY_MODULE_BASE = "/smsverify/rest/?";
        public const string ASSIGN_CREDIT_AND_DID_MODULE_BASE = "/smsverify/rest/?";
        public const string DID_SEARCH_MODULE_BASE = "/didsearch/rest/?";
        public const string EXTENSION_MODULE_BASE = "/extension/rest/?";
        public const string CALLBACK_MODULE_BASE = "/callback/rest/?";
        public const string VOXOX_SMS_MODULE_BASE = "/vsms/rest/?";
        public const string TRANSLATOR_MODULE_BASE = "/translator/rest/?";
        public const string MOBILE_MODULE_BASE = "/mobile/rest?";
        public const string VOXOX_REST_POST_ADDRESS_BASE = "/voxox/rest?";
        public const string MEDIA_FILES_UPLOAD_MODULE_BASE = "/sendfile/file/upload?";
        public const string SEND_FAX_MODULE_BASE = "/sendfax/fax/send/";			//TODO: should this have trailing question mark?
        public const string VOXOX_MESSAGES_MODULE_BASE = "/voxoxmessages/do/open?";	//NOTE: this is using VoxOx web server my.voxox.com
        public const string VOXOX_GROUP_MESSAGING_MODULE_BASE = "/groupmessaging/rest?";


        //URI Query (API Methods) :        
        public const string PING = "ping";
        public const string AUDIT_LOGIN = "auditLogin";
        public const string VALIDATE_CODE = "validateCode";
        public const string SEND_CODE = "sendCode";
        public const string LOGIN = "login";
        public const string SIGNUP = "signUp";
        public const string SIMPLE_SIGNUP = "simpleSignup";
        public const string GET_AVAILABLE_BATCH_IDS = "getAvailableBatchIds";
        public const string GET_STATES = "getStates";
        public const string GET_AREA_CODES_BY_STATE = "getAreaCodesByState";
        public const string GET_FREE_NUMBERS = "getFreeNumbers";
        public const string Assign_DID = "assignCreditAndDID";
        public const string GET_DATA_SUMMARY = "getDataSummary";
        public const string SAVE_PROFILE_SETTINGS = "saveSettings";
        public const string SET_FINDME_STATUS = "setFindmeStatus";
        public const string GET_VOICEMAIL = "getVoicemail";
        public const string UPDATE_VOICEMAIL = "updateVoicemailStatus";
        public const string CALLBACK = "initiateCallback";
        public const string INBOUND_MESSAGES = "getInboundSmsForUser";
        public const string GET_MESSAGES_HISTORY = "getMessageHistory";
        public const string DELETE_MESSAGES = "deleteMessages";
        public const string SET_CALLER_ID = "setCallerid";
        public const string SAVE_FIND_ME = "saveFindme";
        public const string SEND_SMS_VERBOSE = "sendSMSVerbose";
        public const string GET_LANGUAGES = "getLanguages";
        public const string TRANSLATE = "translate";
        public const string CHANGE_PASSWORD = "changePassword";
        public const string CHECK_VOXOX_USERS = "checkVoxoxUsers";
        public const string CHECK_VOXOX_USERS_PHONE_NUMBERS = "getPhonesByUsers";
        public const string GET_BALANCE = "getBalance";
        public const string IN_APP_PURCHASE = "makePurchase";
        public const string REGISTER_TOKEN = "registerGcmToken";
        public const string UNREGISTER_TOKEN = "unregisterToken";
        public const string GET_PRODUCT = "getProductList";

        public const string CREATE_MESSAGING_GROUP = "createGroup";
        public const string DELETE_MESSAGING_GROUP = "deleteGroup";
        public const string GET_MESSAGING_GROUP_AVATAR = "getGroupAvatar";
        public const string SET_MESSAGING_GROUP_AVATAR = "setGroupAvatar";
        public const string RENAME_MESSAGING_GROUP = "renameGroup";
        public const string ADD_MESSAGING_GROUP_MEMBER = "addMember";
        public const string ADD_MESSAGING_GROUP_MEMBERS = "addMembers";
        public const string REMOVE_MESSAGING_GROUP_MEMBER = "removeMember";
        public const string REMOVE_MESSAGING_GROUP_MEMEBERS = "removeMembers";
        public const string GET_MESSAGING_GROUP_MEMBERS = "getGroupMembers";
        public const string GET_MESSAGING_GROUPS = "getGroups";
        public const string SET_MESSAGING_GROUP_MEMBER_NOTIFY = "setMemberNotify";
        public const string SEND_MESSAGING_GROUP_MESSAGE = "sendMessage";




        //API Response
        public const int SUCCESS = 0;
        public const int FAILURE = 1;
        public const int EXCEPTION = 2;

        private static string apiKey;			    //Voxox API calls        

        /// <summary>
        /// We need to make sure we store and retrieve encrypted keys.        
        /// </summary>
        public static void initKeys()
        {
            // TODO Installed should include the encrypted key in registry or a separate resource file
            apiKey = "b5779bf0-3568-102c-8a0d-001ec9b74ff9";
        }

        /// <summary>
        /// Gets the Voxox API key.
        /// </summary>
        /// <returns>String</returns>
        public static string getApiKey()
        {
            //TODO Need to have an override via registry, for developers to work with dev environments.
            return apiKey;
        }
        // environment where the account resides. For VoxOx this will always be 6. (VoxOx Callback and Call Settings documents)
        public static string API_CALL_ENVIRONMENT_VOXOX = "6";
        // message type (VoxOx Messages document)
        // NOTE: These values are from the VoxOx servers, so do NOT change them.  Feel to add new ones as needed.
        //CODE-REVIEW: These are also defined in Persistence API and used only for DeleteMessages API call.  So they may not be needed here.
//        public static string SVR_MSG_TYPE_VOICEMAIL = "1";
//        public static string SVR_MSG_TYPE_FAX = "2";
//        public static string SVR_MSG_TYPE_RECORDED_CALL = "3";
//        public static string SVR_MSG_TYPE_SMS = "4";
//        public static string SVR_MSG_TYPE_XMPP = "5";

        // message status (VoxOx Messages document)
        // NOTE: These values are from the VoxOx servers, so do NOT change them.  Feel to add new ones as needed.
        public static string SVR_VOICEMAIL_STATUS_NEW = "1";
        public static string SVR_VOICEMAIL_STATUS_HEARD = "2";
        public static string SVR_VOICEMAIL_STATUS_DELETED = "4";

        /*
         * WEB Calls (VoxOx and other branded versions)
         *
         * */
        public static string IP_SERVICE = "http://api.ipinfodb.com/v3/ip-city/?key=6e6f9c9284f61c93c231beda1d9cff24673f112aa711327aa0229d2f6575198d&format=json";
        public static string FORGOT_PASSWORD = "https://my.voxox.com/password/forgot/";		//TODO: Should this be modified based on QA Test environment?  I think YES.
        public static string TERMS_N_CONDITIONS = "http://www.voxox.com/index/terms";
        public static string TUTORIAL_LINK = "http://www.youtube.com/watch?v=r6RjjXatxNo";
        public static string VOXOX_HELP_LINK = "http://www.voxox.com/help";
        public static string VOXOX_CALLING_RATES = "http://www.voxox.com/calling-rates";
//        public static string VOXOX_HELP_LINK_TELEFICIENT_BRANDED = "http://www.AwiKonekT.com/help";
//        public static string VOXOX_HELP_LINK_TELERELIANCE_BRANDED = "http://www.MychatMore.com/help";

        /*
         * EMAIL Calls (VoxOx and other branded versions)
         *
         * */
        public static string VOXOX_CONTACT_SUPPORT_BY_EMAIL = "support@voxox.com";
//        public static string VOXOX_CONTACT_SUPPORT_BY_EMAIL_TELEFICIENT_BRANDED = "Android@AwiKonekT.com";
//        public static string VOXOX_CONTACT_SUPPORT_BY_EMAIL_TELERELIANCE_BRANDED = "Android@MychatMore.com ";
//        public static string VOXOX_CONTACT_SUPPORT_BY_EMAIL_ELCA_BRANDED = "Android@elcacomm.com";

        /*
         * Group Messaging
         *
         * */
        public static int NO_USER_ID = -1;


        /*
         * Servers IP / ports
         * if you update the servers 'manually' to a non production environment, the isProductionEnvironment flag should be updated as well.
         * servers can also be updated 'automatically' from the advanced settings dialog that may be displayed in the login screen (if BuildConstants.ENABLE_QA_ADVANCED_SETTINGS is enabled).
         *
         * XMPP :
         * "voxox.com" should be used for production.
         * "pre.im.voxox.com" for PRE-production.
         * "qa0.im.voxox.com" for all dev/QA (Use of qa0 may result in duplicate messages.  They are working on a fix.),
         *
         * REST API :
         * "voxox.com" should be used for production.
         * "pre.api.telcentris.com"
         * "ballastpoint.api.telcentris.com"
         *
         * NOTE:
         * my.voxox.com is a public server (not an API). It is serving audio files just as a web-site might serve an image.
         *
         * */
        private static string mXmppServer = VOXOX_XMPP_SERVER;
        private static string mXmppDevServer = "10.3.9.143";  // NOTE: Requires VPN
        private static int mXmppPort = VOXOX_XMPP_PORT;
        private static string mApiServer = VOXOX_API_REST_SERVER;
        private static string mVoxoxWebServer = VOXOX_WEB_SERVER;
        private static string mSipServer = VOXOX_SIP_SERVER;

        /*
         * A flag to indicate if to use the production API key.
         *
         * NOTE:
         * production API key should be used for production and pre production environments.
         *
         * */
        private static bool mIsProductionApiKey = true;

        /*
         * A flag to indicate if to use the production XMPP server (false = use development server)
         *
         * */
        private static bool mIsProductionXmppServer = true;

        /*
         * A flag to indicate if the server SIP IP is overwritten.
         *
         * */
        private static bool mSipServersIpOverwritten;

        /*
         * Methods to set & get servers IP, port and environment type.
         *
         * */
        public static int getXmppPort() { return mXmppPort; }
        public static String getApiServer() { return mApiServer; }
        public static string getSipServer() { return mSipServer; }
        public static bool isProductionKey() { return mIsProductionApiKey; }
        public static bool isProductionXMPP() { return mIsProductionXmppServer; }
        public static bool isSipServerIpOverwritten() { return mSipServersIpOverwritten; }
        public static string getXmppServer()
        {
            return mIsProductionXmppServer ? mXmppServer : mXmppDevServer;
        }

        public static void setApiServer(string apiServer)
        {
            mApiServer = apiServer;
        }

        public static void setXmppServer(string xmppServer)
        {
            mXmppServer = xmppServer;
        }

        public static void setSipServer(string sipServer)
        {
            if (!VOXOX_SIP_SERVER.Equals(sipServer, StringComparison.CurrentCultureIgnoreCase))
            {
                mSipServersIpOverwritten = true;
                mSipServer = sipServer;
            }
        }

        public static void useProductionKey(bool productionKey)
        {
            mIsProductionApiKey = productionKey;
        }

        public static void resetUserSettings()
        {
            mApiServer = VOXOX_API_REST_SERVER;
            mXmppServer = VOXOX_XMPP_SERVER;
            mSipServer = VOXOX_SIP_SERVER;
            mIsProductionApiKey = true;
            mSipServersIpOverwritten = false;
        }

        /*
         * Standard module URLs are created dynamically using a common method.
         *
         * If, as developer you need to test just a single API change, you just need to:
         * 	- Comment out appropriate method below
         *  - Add new method, replacing mApiServer, with your server.
         *  	- For example, string getSmsVerifyModule() { return makeModule( "myApiServer.telcentris.com", SMS_VERIFY_MODULE_BASE, true );
         *
         * 	NOTE: Do not forget to remove your change before you commit!
         */
        public static string getVoxoxModule()               { return makeModule(mApiServer, true); }
        public static string getSmsVerifyModule()           { return makeModule(mApiServer, true); }
        public static string getAssignCreditAndDidModule()  { return makeModule(mApiServer, true); }
        public static string getDidSearchModule()           { return makeModule(mApiServer, true); }
        public static string getExtensionModule()           { return makeModule(mApiServer, true); }
        public static string getCallbackModule()            { return makeModule(mApiServer, true); }
        public static string getVoxoxSmsModule()            { return makeModule(mApiServer, true); }
        public static string getTranslatorModule()          { return makeModule(mApiServer, true); }
        public static string getMobileModule()              { return makeModule(mApiServer, true); }
        public static string getVoxoxRestPostAddress()      { return makeModule(mApiServer, true); }
        public static string getMediaFilesUploadModule()    { return makeModule(mApiServer, true); }
        public static string getSendFaxModule()             { return makeModule(mApiServer, true); }
        public static string getVoxoxGroupMessagingModule() { return makeModule(mApiServer, true); }
        public static string getVoxoxMessagesModule()       { return makeModule(mVoxoxWebServer, true); } //NOTE: This uses VoxOx web server

        /*
         * A utility method to create full module string based on parameters.
         *
         */
        private static string makeModule(string server, bool useHttps)
        {
            return (useHttps ? "https://" : "http://") + server;		//TODO: We need to override this for some QA test environments
        }

        private static string makeModuleComplete(string server, string moduleBase, bool useHttps)
        {
            return (useHttps ? "https://" : "http://") + server + moduleBase;		//TODO: We need to override this for some QA test environments
        }
    }
}
