﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vxapi.Base;
using Vxapi.Util;
using Vxapi.Constants;
using Desktop.Utils;
using Vxapi.Model;

namespace Vxapi
{
    /// <summary>
    /// This class provides the various methods to make RestFul API calls to Voxox NOC or 
    /// services. For the RestFul calls are provided in the base class which uses 
    /// RestSharp for all RestFul calls.
    /// </summary>
    public class RestfulAPIManager : BaseService
    {
        //Instance of Logger
        private static VXLogger logger = VXLoggingManager.INSTANCE.GetLogger("RestfulAPIManager");
        private LoginResponse loginResponse;

        /// Initializes a new instance of the <see cref="RestfulAPIManager" /> class. 
        /// </summary>
        /// <remarks>
        /// Uses a private access modifier to prevent external instantiation of this class,
        /// as this class uses Singleton Pattern.
        /// </remarks>		
        private RestfulAPIManager(){
            RestfulConstants.initKeys(); //initialize the keys
        }

        /// <summary>
        /// The only instance of RestfulAPIManager
        /// </summary>
        public static RestfulAPIManager INSTANCE = new RestfulAPIManager();


        public bool isInternetConnectionAvailable()
        {
            string messageToSend = "ping";
            Dictionary<string, string> parameters = RestfulApiHelper.getPingParameters(messageToSend);
            parameters.Add("format", "json");

            //CODE-REVIEW: This logging is re-useable code.  Create a method that takes the needed strings and dictionary,
            //      then just call that.  And let logger check if it is enabled or not.
            if (logger.IsDebugEnabled())
            {
                logger.Debug("RestfulConstants.getVoxoxModule(): " + RestfulConstants.getVoxoxModule());
                logger.Debug("RestfulConstants.VOXOX_MODULE_BASE: " + RestfulConstants.VOXOX_MODULE_BASE);                
                foreach (KeyValuePair<string, string> param in parameters)
                {
                    logger.Debug("RestfulApiHelper.getPingParameters: " + param.Key + " = " + param.Value);
                }
            }            
            var result = InvokeRestMethodAsync(RestfulConstants.getVoxoxModule(), RestfulConstants.VOXOX_MODULE_BASE, parameters);            
            if (result.Result != null && result.Result.Length > 0)
            {
                logger.Debug("isInternetConnectionAvailable: " + result.Result);
                return true;
            }
            else
            {
                logger.Warn("isInternetConnectionAvailable failed.");
                return false;
            }
        }

        public PingResponse Ping()
        {
            string messageToSend = "ping";
            Dictionary<string, string> parameters = RestfulApiHelper.getPingParameters(messageToSend);            
            if (logger.IsDebugEnabled())
            {
                logger.Debug("RestfulConstants.getVoxoxModule(): " + RestfulConstants.getVoxoxModule());
                logger.Debug("RestfulConstants.VOXOX_MODULE_BASE: " + RestfulConstants.VOXOX_MODULE_BASE);
                foreach (KeyValuePair<string, string> param in parameters)
                {
                    logger.Debug("RestfulApiHelper.getPingParameters: " + param.Key + " = " + param.Value);
                }
            }

            //CODE-REVIEW: the try/catch should be in the InvokeRestMethodAsync() call.  If an exception is thrown there,
            //  then just set some flag/value in the returned object.  Any exception that is caught is most likely going to
            //  handled in the same way.... Log it and move on, just as we do in all these methods.
            //  Similar case for the 'failed'.  If all we are doing is logging, then let's do that in a common location.
            try
            {
                var result = InvokeRestMethodAsync<PingResponse>(RestfulConstants.getVoxoxModule(), RestfulConstants.VOXOX_MODULE_BASE, parameters);
                if (result.Result != null)
                {
                    PingResponse pingResp = result.Result;
                    logger.Debug("Ping: Status=" + pingResp.Status + ", Resp=" + pingResp.Response);
                    return result.Result;
                }
                else
                {
                    logger.Warn("Ping failed.");
                    return null;
                }
            }catch (Exception e)
            {
                logger.Error("Ping failed:", e);
                return null;
            }
        }


        public bool isValidUser(String username, string password)
        {
            loginResponse = Login(username, password);
            if(loginResponse.did > 0)
            {
                return true;
            }
            return false;
        }

        public LoginResponse getLoginResponse()
        {
            if (loginResponse != null)
                return loginResponse;
            return null;
        }



        public LoginResponse Login(string username, string password)
        {
            try
            {
                Dictionary<string, string> parameters = RestfulApiHelper.getLoginParameters(username, password);
                parameters.Add("format", "json");            
                var result = InvokeRestMethodAsync<LoginResponse>(RestfulConstants.getVoxoxModule(), RestfulConstants.VOXOX_MODULE_BASE, parameters);
                if (result.Result != null)
                {
                    LoginResponse loginResp = result.Result;
                    logger.Debug("Login: Status=" + loginResp.Status + ", DID=" + loginResp.did);
                    return result.Result;
                }
                else
                {
                    logger.Warn("Login failed.");
                    return null;
                }
            }
            catch (Exception e)
            {
                logger.Error("Login failed:", e);
                return null;
            }
        }

        public TranslatorLanguageParametersResponse GetTranslateLanguagesParameters()
        {
            try
            {
                Dictionary<string, string> parameters = RestfulApiHelper.getTranslateLanguagesParameters();
                parameters.Add("format", "json");
                var result = InvokeRestMethodAsync<TranslatorLanguageParametersResponse>(RestfulConstants.getVoxoxModule(), RestfulConstants.VOXOX_MODULE_BASE, parameters);
                if (result.Result != null)
                {
                    TranslatorLanguageParametersResponse loginResp = result.Result;
                    logger.Debug("TranslatorResponse: Status=" + loginResp.Status + ", Response=" + loginResp.Response);
                    return result.Result;
                }
                else
                {
                    logger.Warn("TranslatorResponse failed.");
                    return null;
                }
            }
            catch (Exception e)
            {
                logger.Error("TranslatorResponse failed:", e);
                return null;
            }
        }

        public AssignCreditAndDidResponse GetAssignCreditAndDid(string userId)
        {
            try
            {
                Dictionary<string, string> parameters = RestfulApiHelper.getAssignCreditAndDidParameters(userId);
                var result = InvokeRestMethodAsync<AssignCreditAndDidResponse>(RestfulConstants.getVoxoxModule(), RestfulConstants.VOXOX_MODULE_BASE, parameters);
                if (result.Result != null)
                {
                    AssignCreditAndDidResponse loginResp = result.Result;
                    logger.Debug("TranslatorResponse: Status=" + loginResp.Status + ", Response=" + loginResp.Response);
                    return result.Result;
                }
                else
                {
                    logger.Warn("TranslatorResponse failed.");
                    return null;
                }
            }
            catch (Exception e)
            {
                logger.Error("TranslatorResponse failed:", e);
                return null;
            }
        }

        public MessageHistoryResponse GetMessageHistoryResponse(string userId)
        {
            try
            {
                Dictionary<string, string> parameters = RestfulApiHelper.getMessageHistory(userId, null);
                var result = InvokeRestMethodAsync<MessageHistoryResponse>(RestfulConstants.getVoxoxModule(), RestfulConstants.VOXOX_MODULE_BASE, parameters);
                if (result.Result != null)
                {
                    MessageHistoryResponse loginResp = result.Result;
                    logger.Debug("GetMessageHistoryResponse: Status=" + loginResp.Status + ", Response=" + loginResp.Response);
                    return result.Result;
                }
                else
                {
                    logger.Warn("GetMessageHistoryResponse failed.");
                    return null;
                }
            }
            catch (Exception e)
            {
                logger.Error("GetMessageHistoryResponse failed:", e);
                return null;
            }
        }
    }

}
