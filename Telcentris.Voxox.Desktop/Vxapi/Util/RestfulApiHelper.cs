﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vxapi.Constants;

namespace Vxapi.Util
{
    //CODE_REVIEW:  This is a direct implementation of what is on Android.  Recommended changes (to both platforms)
    //  - since ApiKey, UserKey and UserId are used on almost every API call, we should set them once and then re-use them
    //      so every caller of these methods does not have figure our what they are.
    //      These may also include data elements like useName, DID, etc.
    //      Looser coupling.
    //
    //  - Rather then recoding the key/value pairs for 'key', 'method', 'userkey', etc., we should have a method that returns
    //      dictionary of these common strings to which we just append any additional strings.
    //      This is less error-prone, and makes future common changes (like 'userkey' to 'userKey') much easier.
    //
    //  - Some methods create dictionary in single constructor, others create empty dictionary and call Add().
    //      Let's be consistent.

    class RestfulApiHelper
    {
        private RestfulApiHelper() {}
	
        //Our API calls use '1' and '0' for true/false values.
	    //Let's keep that convenient and easy.
	    private static String booleanToText( bool val ) {
		    return (val ? "1" : "0");
	    }

	    
	    //The method returns parameters for the RESTful API ping()
	    //which echo the message sent.
	    public static Dictionary<string, string> getPingParameters(string echoMessage) 
        {
		    var parameters = new Dictionary<string, string> 
            {
                {"key", RestfulConstants.getApiKey()},
		        {"method", RestfulConstants.PING},
                {"message", echoMessage}
            };
		    return parameters;
	    }

	    //The method returns parameters for RESTful API login()
	    //which validates the credential and return account information
	    public static Dictionary<string, string> getLoginParameters(string userName, string password) {
            var parameters = new Dictionary<string, string>
            {
                {"key", RestfulConstants.getApiKey()},
                {"method", RestfulConstants.LOGIN},
		        {"username", userName},
                {"password", password}
            };
		    return parameters;
	    }

	    //The method returns parameters for RESTful API translate()	    
	    public static Dictionary<string, string> getTranslateParameters(string text, string sourceLocale, string destLocale) {

		    var parameters = new Dictionary<string, string> 
            {
		        {"key", RestfulConstants.getApiKey()},
		        {"method", RestfulConstants.TRANSLATE},
                {"text", text},
                {"source_locale", sourceLocale},
                {"dest_locale", destLocale}
            };
		    return parameters;
	    }

	    //The method returns parameters for RESTful API getLanguages() which
	    //retrieve a list of all languages supported in the API
	    public static Dictionary<string, string> getTranslateLanguagesParameters() {

		    var parameters = new Dictionary<string, string> 
            {
		        {"key", RestfulConstants.getApiKey()},
                {"method", RestfulConstants.GET_LANGUAGES}
            };
		    return parameters;
	    }

	    //The method returns parameters for RESTful API auditLogin() which log
	    //the login event in our reporting data. This should be fired after each login attempt.
	    public static Dictionary<string, string> getAuditLoginParameters() {

		    var parameters = new Dictionary<string, string> 
            {
		        {"key",		RestfulConstants.getApiKey()},
                {"method",		RestfulConstants.AUDIT_LOGIN},
                {"voxox_Id",	""},
                {"username",	""},
                {"success", 	"success"}, // TODO: Verify with API
		        // following fields are NOT a required by REST API, so continue even if we have an exception
                {"platform","Windows7"},
                {"version", ""} //Android :pInfo.versionName + "-" + brandedVersionBuild.toString()
            };
            return parameters;
	    }

	    //The method returns the parameters for RESTful API getInboundSmsForUser()
	    //which retrieves SMSs that were sent to a user.
	    public static Dictionary<string, string> getInboundSMSParameters(string userId, string timestamp) 
        {
		    var parameters = new Dictionary<string, string>
            {
		        {"key", 	RestfulConstants.getApiKey()},
		        {"method", RestfulConstants.INBOUND_MESSAGES},
		        {"userId", userId},//TODO Get Voxox ID
		        {"limit", ""}// Total number of SMSs to return.
            };
        
		    if (timestamp != null) {		// retrieve all message received after timestamp
			    parameters.Add("timestamp", timestamp);
		    }
		    return parameters;
	    }

	    //The method returns the parameters for RESTful API simpleSignup()
	    //This method is what will create voxox users. We use this for the simplified form in the mobile apps.
	    //It will ultimately call the signUp() method described below.	 
        //** NOT NEEDED for DESKTOP as registration is thorugh Web portal
	    public static Dictionary<string, string> getSimpleSignUpParameters(string userName, string password, string email, string mobile) 
        {
		    const string referralCodeBrandedName = "";//TODO get branded version name
		    var parameters = new Dictionary<string, string> 
            {
		        {"key", RestfulConstants.getApiKey()},
		        {"method", RestfulConstants.SIMPLE_SIGNUP},

		        // parameters
		        {"username", userName},
		        {"password", password},
		        {"email", email},
		        {"mobile", mobile},
		        {"country", ""},//TODO Android: context.getResources().getConfiguration().locale.getDisplayCountry())
		        {"version", ""}, //TODO Android: SystemPropertiesUtil.getAppVersion(context)));
                {"referralCode", referralCodeBrandedName} //TODO
            };
		    return parameters;
	    }

	    //The method returns the parameters for RESTful API sendSmsVerbose()
	    //to sends an SMS.
	    public static Dictionary<string, string> getSendSMSParameters(string userName, string body, string to) 
        {
		    var parameters = new Dictionary<string, string> 
            {
                {"key", RestfulConstants.getApiKey()},
                {"method", RestfulConstants.SEND_SMS_VERBOSE},
                {"username", userName}, 
                {"to", to},
                {"body", body.Trim()}
            };
		    return parameters;
	    }


	    //The method returns the parameters for RESTful API getMessageHistory()
	    //which will retrieve voicemails, faxes, recorded calls, sms, and xmpp messages sent to/sent by a user.
	    public static Dictionary<string, string> getMessageHistory(string userId, string timestamp) {

		    var parameters = new Dictionary<string, string> 
            {
		        {"key", RestfulConstants.getApiKey()},
                {"method", RestfulConstants.GET_MESSAGES_HISTORY},
                {"userId", userId}
            };
		    // retrieve all message received after timestamp
		    if (timestamp != null) 
            {
			    parameters.Add("timestamp", timestamp);
		    }
		    return parameters;
	    }

	    //The method returns the parameters for RESTful API deleteMessages call
	    public static Dictionary<string, string> getDeleteMessagesParameters(string userId, List<string> msgDataList) //TODO 
        {
            var parameters = new Dictionary<string, string>();
            if ( msgDataList.Count > 0 ) 
            {
        	    parameters.Add("key",     RestfulConstants.getApiKey());
        	    parameters.Add("method",  RestfulConstants.DELETE_MESSAGES);
        	    parameters.Add("userId",  userId);
                /*
        	    //Handle array of arrays.
        	    int    x = 0;
        	    String msgId;
        	    String svrMsgType;

    		    for ( DeleteMessagesTask.MessageData msgData : msgDataList) {

    			    svrMsgType = msgData.getSvrMsgType();
				    msgId      = msgData.getMsgId();

				    if ( !TextUtils.isEmpty( msgId ) ) {
					    String msgDataTag = "messageData[" + x + "]";										//"messageData[0]"

					    params.add(new BasicNameValuePair( msgDataTag + "[messageType]", svrMsgType ) );	//messageData[0][messageType]=svrMsgType
					    params.add(new BasicNameValuePair( msgDataTag + "[messageId]", 	 msgId      ) );

					    x++;
				    }
			    }
                */
		    }
            return parameters;
	    }
	
	     //The method returns the parameters for RESTful API changePassword()
	 
	    public static Dictionary<string, string> getChangePasswordParameters(string userName, string userKey, string oldPassword, string newPassword) {

		    var parameters = new Dictionary<string, string>();
            parameters.Add("key", RestfulConstants.getApiKey());
		    parameters.Add("method", RestfulConstants.CHANGE_PASSWORD);
		    parameters.Add("username", userName); 
		    parameters.Add("userkey", userKey); 
		    parameters.Add("oldPassword", oldPassword);
		    parameters.Add("newPassword", newPassword);
		    return parameters;
	    }

	
	     // The method returns a string that is used as an HTTP GET parameters to retrieve
	     // a voice-mail file
        //CODE-REVIEW: Why does this return string, while other methods return dictionary?  Let's be consistent.
        public static String getVoicemailFileHttpGetParameters(string userName, string userKey, string voicemail_id)
        {

		    return RestfulConstants.getVoxoxMessagesModule() +
				    "username=" + userName +
                    "&userkey=" + userKey + 
				    "&id=" 		+ voicemail_id;
	    }

	
	     //The method returns the parameters for RESTful API checkVoxoxUsers()
	     //which returns the Voxox user data for each of an array of mobile numbers.
	     //It allows you to send an array of numbers and get back Voxox data for those that are Voxox users.	 
	    public static Dictionary<string, string> getVoxoxUsersParameters(List<string> numbers) {

		    var parameters = new Dictionary<string, string>();
		    parameters.Add("key", 	RestfulConstants.getApiKey());
		    parameters.Add("method", RestfulConstants.CHECK_VOXOX_USERS);
		    foreach(string number in numbers) {
			    if(!number.Equals("")) {
				    parameters.Add("phoneNumbers[]", number);
			    }
		    }
		    parameters.Add("format", "json");
		    return parameters;
	    }

	
	     //The method returns the parameters for RESTful API getPhonesByUsers()
	     //which returns the Voxox phone number (did) for each of an array of Voxox contacts names.
	     //It allows you to send an array of Voxox names and get back Voxox phone numbers.
	 
	    public static Dictionary<string, string> getVoxoxUsersPhoneNumbersParameters(List<string> names, bool useJson) {

		    var parameters = new Dictionary<string, string>();

		    parameters.Add("key", 	RestfulConstants.getApiKey());
		    parameters.Add("method", RestfulConstants.CHECK_VOXOX_USERS_PHONE_NUMBERS);
		    foreach(string name in names) {
			    if(!name.Equals("")) {
				    parameters.Add("users[]", name);
			    }
		    }
		    if ( useJson )
			    parameters.Add("format", "json");
		    return parameters;
	    }

	
	     // This return the parameters of RESTful API getDataSummary()
	     // which return various data related to the users account.
	     // When items change server-side, this information is also pushed to the user
	     // (via XMPP and APNS) and systems should react to the changes by updating displays,
	     // retrieving new voicemail, etc.	 
	    public static Dictionary<string, string> getVoxoxDataSummaryParameters(string userName, string userKey) 
        {
		    var parameters = new Dictionary<string, string>();
		    parameters.Add("key",RestfulConstants.getApiKey());
		    parameters.Add("method",RestfulConstants.GET_DATA_SUMMARY);
		    parameters.Add("username", userName); 
		    parameters.Add("userkey", userKey); 
		    return parameters;
	    }

	
	     //parameters need to be doNotDisturb (1 or 0) and did
	     //NOTE: DND on == FindMe off
	    public static Dictionary<string, string> getUpdateDoNotDisturbParameters(bool isChecked, string did) 
        {
		    var parameters = new Dictionary<string, string>();
		    parameters.Add("key", RestfulConstants.getApiKey());
		    parameters.Add("method",	RestfulConstants.SET_FINDME_STATUS);
		    parameters.Add("i_env", RestfulConstants.API_CALL_ENVIRONMENT_VOXOX);
		    parameters.Add("did", did);
		    parameters.Add("findmeOn", isChecked.ToString());
		    return parameters;
	    }

	    public static Dictionary<string, string> getUpdateCallerIdParameters(string userName, string userKey, string number) 
        {
		    var parameters = new Dictionary<string, string>();
		    parameters.Add("key", RestfulConstants.getApiKey());
		    parameters.Add("method",	RestfulConstants.SET_CALLER_ID);
		    parameters.Add("username", userName);
		    parameters.Add("userKey", userKey);
		    parameters.Add("number", number);
		    return parameters;
	    }

	
	    //The method returns the parameters for RESTful API initiateCallback()	 
	    public static Dictionary<string, string> getInitiateCallbackParameters(string userName, string password, string inboundDid, string userOutboundDidInput) {

		    var parameters = new Dictionary<string, string>();
		    parameters.Add("key",		RestfulConstants.getApiKey());
		    parameters.Add("method",		RestfulConstants.CALLBACK);
		    parameters.Add("user",	userName); 
		    parameters.Add("password",	password);

		    // Phone number to connect to first.
		    parameters.Add("inboundDid", inboundDid); 

		    // Phone number to connect to once in-bound connection completes.
		    parameters.Add("outboundDid",userOutboundDidInput);

		    return parameters;
	    }

	
	    //The method returns the parameters for RESTful API getSmsValidateCode()
	    //which retrieves SMSs that were sent to/by the user.	 
	    public static Dictionary<string, string> getSmsValidateCodeParameters(string userId, string code) {

		    var parameters = new Dictionary<string, string>();
		    parameters.Add("key",    RestfulConstants.getApiKey());
		    parameters.Add("method", RestfulConstants.VALIDATE_CODE );
		    parameters.Add("userId", userId); 
		    parameters.Add("code",	code);
		    return parameters;
	    }

	
	     //The method returns the parameters for RESTful API getSmsSendCode()
	     //which retrieves SMSs that were sent to/by the user.	 
	    public static Dictionary<string, string> getSmsSendCodeParameters(string userId, string smsNumber) 
        {
		    var parameters = new Dictionary<string, string>();
		    parameters.Add("key",    	  RestfulConstants.getApiKey());
		    parameters.Add("method", 	  RestfulConstants.SEND_CODE);
            parameters.Add("userId", userId);
		    parameters.Add("mobileNumber", smsNumber.Trim());
		    return parameters;
	    }

	
	     //The method returns the parameters for RESTful API makePurchase()
	     //which tracks in-app purchases.
	 
	    public static Dictionary<string, string> getMakePurchaseParameters(string userId, string userName, string userKey, string applicationId, int qty, string receipt, string productId, string signature) 
        {
		    var parameters = new Dictionary<string, string>();
		    parameters.Add("key", RestfulConstants.getApiKey());
		    parameters.Add("method", RestfulConstants.IN_APP_PURCHASE);
		    parameters.Add("userId", userId); 
		    parameters.Add("username",  userName); 
		    parameters.Add("userKey", userKey); 
		    parameters.Add("qty", qty.ToString());
		    parameters.Add("receipt", receipt);
		    parameters.Add("envId", "0");
		    parameters.Add("productIdentifier", productId);
		    parameters.Add("signature", signature);
		    parameters.Add("type_id", "2");
		    parameters.Add("applicationId", applicationId); 
		    return parameters;
	    }

	
	     //The method returns the parameters for RESTful API registerGcmToken()
	     //.
	 
	    
        //The method returns the parameters for RESTful API assignCreditAndDID()
	    //this method will assign the chosen DID and apply the promotional credit.	 
	    public static Dictionary<string, string> getAssignCreditAndDidParameters(string userId) 
        {
		    var parameters = new Dictionary<string, string>();
		    parameters.Add("key",	RestfulConstants.getApiKey());
		    parameters.Add("method", RestfulConstants.Assign_DID);
		    parameters.Add("userId", userId);
		    return parameters;
	    }

	    public static Dictionary<string, string> initialiseUpdateCallForwardingParameters(bool hasPhoneNumbers) 
        {
		    Dictionary<string, string> parameters = new Dictionary<string, string>();
		    parameters.Add("key",RestfulConstants.getApiKey());
		    parameters.Add("method",RestfulConstants.SAVE_FIND_ME);
		    parameters.Add("i_env", 	RestfulConstants.API_CALL_ENVIRONMENT_VOXOX);
		    parameters.Add("did", ""); //TODO Android: SessionManager.INSTANCE.getUserDid(context)));
		    parameters.Add("phone_setting_id","0");
		    if (!hasPhoneNumbers)
			    parameters.Add("numbers", "");

		    return parameters;
	    }

	    public static void addCallForwardingPhoneNumber(ref Dictionary<string, string> restParams, string position, string active,
												        string phoneNumber, string callSimultaneous, string name) {
		    restParams.Add("numbers[" + position + "][active]", active);
		    restParams.Add("numbers[" + position + "][phone_number]", phoneNumber);
		    restParams.Add("numbers[" + position + "][name]", name);
		    restParams.Add("numbers[" + position + "][type_id]", callSimultaneous);
		    restParams.Add("numbers[" + position + "][sequence]", position);
		    restParams.Add("numbers[" + position + "][timeout]", "15");
		    restParams.Add("numbers[" + position + "][cid_type]", "1");
	    }

	
	     //The method returns the parameters for RESTful API getStoreCredits()
	     //this method will assign the applicatioId and UserName
	 
	    public static Dictionary<string, string> getStoreCredits() {

		    var parameters = new Dictionary<string, string>();
		    parameters.Add("key", RestfulConstants.getApiKey());
		    parameters.Add("method", RestfulConstants.GET_PRODUCT);
		    parameters.Add("applicationId", ""); //TODO Android: BrandedVersionManager.INSTANCE.getBrandedVersion().getApplicationId()));
		    parameters.Add("username", ""); //TODO Android: SessionManager.INSTANCE.getUserName(context)));
		    return parameters;
	    }
	
	    //GroupMessaging Helpers	 
	    public static Dictionary<string, string> getCreateMessagingGroupParameters(string name, int adminUserId, string adminUserKey, string adminName, int inviteType, string confirmationCode, bool announceMembers) 
        {
            var parameters = new Dictionary<string, string>();
            parameters.Add("key", RestfulConstants.getApiKey());
            parameters.Add("method", RestfulConstants.CREATE_MESSAGING_GROUP);
            parameters.Add("name", name);
            parameters.Add("adminUserId", adminUserId.ToString());
            parameters.Add("adminUserKey", adminUserKey);
            parameters.Add("adminName", adminName);
            parameters.Add("inviteType", inviteType.ToString());
            parameters.Add("confirmationCode", confirmationCode);
            parameters.Add("announceMembers", announceMembers.ToString());
            return parameters;
	    }

	    public static Dictionary<string, string> getDeleteMessagingGroupParameters(int adminUserId, string adminUserKey, string groupId) {
            var parameters = new Dictionary<string, string>();
            parameters.Add("key", RestfulConstants.getApiKey());
            parameters.Add("method", RestfulConstants.DELETE_MESSAGING_GROUP);
            parameters.Add("adminUserId", adminUserId.ToString());
            parameters.Add("adminUserKey", adminUserKey);
            parameters.Add("groupId", groupId);
            return parameters;
        }

	    public static Dictionary<string, string> getSetMessagingGroupAvatarParameters(int adminUserId, string adminUserKey, string groupId, byte[] avatarData) {
            var parameters = new Dictionary<string, string>();
            parameters.Add("key", RestfulConstants.getApiKey());
            parameters.Add("method", RestfulConstants.SET_MESSAGING_GROUP_AVATAR);
            parameters.Add("adminUserId", adminUserId.ToString());
            parameters.Add("adminUserKey", adminUserKey);
            parameters.Add("groupId", groupId);
            // TODO-GM: Add avatar binary data as base64-encoded string
            return parameters;
	    }

	    public static Dictionary<string, string> getGetMessagingGroupAvatarParameters(int adminUserId, string adminUserKey, string groupId) {
            var parameters = new Dictionary<string, string>();
            parameters.Add("method", RestfulConstants.GET_MESSAGING_GROUP_AVATAR);
            parameters.Add("adminUserId", adminUserId.ToString());
            parameters.Add("adminUserKey", adminUserKey);
            parameters.Add("groupId", groupId);
            return parameters;
	    }

	    public static Dictionary<string, string> getRenameMessagingGroupParameters(int adminUserId, string adminUserKey, string groupId, string newGroupName) {
            var parameters = new Dictionary<string, string>();
            parameters.Add("key", RestfulConstants.getApiKey());
            parameters.Add("method", RestfulConstants.RENAME_MESSAGING_GROUP);
            parameters.Add("adminUserId", adminUserId.ToString());
            parameters.Add("adminUserKey", adminUserKey);
            parameters.Add("groupId", groupId);
            parameters.Add("newGroupName", newGroupName);
            return parameters;
	    }

	    public static Dictionary<string, string> getAddMessagingGroupMemberParameters(int adminUserId, string adminUserKey, string groupId, int userId, string nickname, bool isAdmin, bool isInvited, bool isConfirmed, bool isOptout) 
        {
            var parameters = new Dictionary<string, string>();
            parameters.Add("key", RestfulConstants.getApiKey());
            parameters.Add("method", RestfulConstants.ADD_MESSAGING_GROUP_MEMBER);
            parameters.Add("adminUserId", adminUserId.ToString());
            parameters.Add("adminUserKey", adminUserKey);
            parameters.Add("groupId", groupId);
            parameters.Add("userId", userId.ToString());
            parameters.Add("nickname", nickname);
            parameters.Add("admin", isAdmin.ToString());
            parameters.Add("invited", isInvited.ToString());
            parameters.Add("confirmed", isConfirmed.ToString());
            parameters.Add("optout", isOptout.ToString());
            return parameters;
	    }

        //TODO: Not in Phase 1
	    public static Dictionary<string, string> getAddMessagingGroupMembersParameters(int adminUserId, string adminUserKey, string groupId/*, Collection<GroupMessageNewMember> members*/) {
            var parameters = new Dictionary<string, string>();
            parameters.Add("key",          RestfulConstants.getApiKey());
            parameters.Add("method",       RestfulConstants.ADD_MESSAGING_GROUP_MEMBERS);
            parameters.Add("adminUserId",  adminUserId.ToString());
            parameters.Add("adminUserKey", adminUserKey);
            parameters.Add("groupId",      groupId);
            /*
            //Handle array of arrays.
		    int x = 0;

		    for(GroupMessageNewMember newMember : members) {
			    if (newMember.getUserId() > RestfulConstants.NO_USER_ID) {

				    String userId    = String.valueOf(newMember.getUserId());
				    String memberTag = "members[" + x + "]";			//"members[0]"

		            parameters.Add( memberTag + "[userId]", userId ));
		            parameters.Add( memberTag + "[nickname]", 	newMember.getNickname()				   ) );					//member[0][nickname]=nickname
		            parameters.Add( memberTag + "[admin]", 		booleanToText(newMember.isAdmin()      ) ));
		            parameters.Add( memberTag + "[invited]", 	booleanToText(newMember.isInvited()    ) ));
		            parameters.Add( memberTag + "[confirmed]", 	booleanToText(newMember.hasConfirmed() ) ));
		            parameters.Add( memberTag + "[optout]", 		booleanToText(newMember.getOptout()    ) ));

		            x++;
			    }
		    }
            */
            return parameters;
	    }

	    public static Dictionary<string, string> getRemoveMessagingGroupMemberParameters(int userId, string userKey, string groupId, int userIdToRemove) {
            var parameters = new Dictionary<string, string>();
            parameters.Add("key", RestfulConstants.getApiKey());
            parameters.Add("method", RestfulConstants.REMOVE_MESSAGING_GROUP_MEMBER);
            parameters.Add("userId", userId.ToString());
            parameters.Add("userKey", userKey);
            parameters.Add("groupId", groupId);
            parameters.Add("userIdToRemove", userIdToRemove.ToString());
            return parameters;
	    }

	    public static Dictionary<string, string> getRemoveMessagingGroupMembersParameters(int userId, string userKey, string groupId, List<int> userIdsToRemove) 
        {
            var parameters = new Dictionary<string, string>();
            parameters.Add("key", RestfulConstants.getApiKey());
            parameters.Add("method", RestfulConstants.REMOVE_MESSAGING_GROUP_MEMEBERS);
            parameters.Add("userId", userId.ToString());
            parameters.Add("userKey", userKey);
            parameters.Add("groupId", groupId);
		    foreach(int userIdToRemove in userIdsToRemove) 
            {
			    if (userIdToRemove > RestfulConstants.NO_USER_ID) {
				    parameters.Add("members[]", userIdToRemove.ToString());
			    }
		    }
            return parameters;
	    }

	    public static Dictionary<string, string> getGetMessagingGroupMembersParameters(int userId, string userKey, string groupId) {
            var parameters = new Dictionary<string, string>();
            parameters.Add("key", RestfulConstants.getApiKey());
            parameters.Add("method", RestfulConstants.GET_MESSAGING_GROUP_MEMBERS);
            parameters.Add("userId", userId.ToString());
            parameters.Add("userKey", userKey);
            parameters.Add("groupId", groupId);
            return parameters;
	    }

	    public static Dictionary<string, string> getGetMessagingGroupsParameters(int userId, string userKey, int otherUserId) {
            var parameters = new Dictionary<string, string>();
            parameters.Add("key", RestfulConstants.getApiKey());
            parameters.Add("method", RestfulConstants.GET_MESSAGING_GROUPS);
            parameters.Add("userId", userId.ToString());
            parameters.Add("userKey", userKey);
            if (RestfulConstants.NO_USER_ID != otherUserId)
        	    parameters.Add("otheruserId", otherUserId.ToString());
            return parameters;
	    }

	    public static Dictionary<string, string> getSetMessagingGroupMemberNotifyParameters(int adminUserId, string adminUserKey, string groupId, int targetUserId, bool notify) {
		    var parameters = new Dictionary<string, string>();
		    parameters.Add("key", RestfulConstants.getApiKey());
	        parameters.Add("method", RestfulConstants.SET_MESSAGING_GROUP_MEMBER_NOTIFY);
	        parameters.Add("userId", adminUserId.ToString());
	        parameters.Add("userKey", adminUserKey);
	        parameters.Add("groupId", groupId);
	        parameters.Add("targetUserId", targetUserId.ToString());
	        parameters.Add("notify", notify.ToString());
	        return parameters;
	     }

	    public static Dictionary<string, string> getSendMessageToGroupParameters(int userId, string userKey, string groupId, string messageBody, string messageId) {
            var parameters = new Dictionary<string, string>();
            parameters.Add("key", RestfulConstants.getApiKey());
            parameters.Add("method", RestfulConstants.SEND_MESSAGING_GROUP_MESSAGE);
            parameters.Add("userId", userId.ToString());
            parameters.Add("userKey", userKey);
            parameters.Add("groupId", groupId);
            parameters.Add("messageBody", messageBody);
            parameters.Add("messageId", messageId);
            return parameters;
	    }
    }
}
