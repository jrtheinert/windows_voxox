﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vxapi.Base;

namespace Vxapi.Model
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]        
    public class LoginResponse : BaseApiResponse
    {
        //CODE-REVIEW: Standard indent is 4, here we have 8.
            private ulong didField;

            private string signupCountryField;

            private ulong smsNumberField;

            private string smsCodeField;

            private string hasValidatedField;

            private string smsVerificationCancelledField;

            private string hasPaymentValidatedField;

            private byte smsAttemptsField;

            private string signupIpField;

            private string clientVersionField;

            private string emailField;

            private byte activeField;

            private byte bannedField;

            private byte confirmationSentField;

            private uint voxox_idField;

            private uint i_accountField;

            private string countryListField;

            private object billingDateField;

            private ushort textingSubscriptionField;

            private decimal balanceField;

            private ushort productIdField;

            private string productNameField;

            private string sipPasswordField;

            private ulong cidField;

            private string sipServerField;

            private string sipIPField;

            private string turnServerField;

            private string turnIPField;

            private string turnUsernameField;

            private string turnPasswordField;

            private string packetReflectorIpField;

            private ushort packetReflectorPortBaseField;

            private ushort packetReflectorPortCountField;

            private object firstNameField;

            private object lastNameField;

            private string keyField;

            private string statusField;

        //CODE-REVIEW: Why do we have private fields AND public properties for the same data elements?
        //  Confusing and makes the properites harder to code.  We typically have 11 lines of code rather than just 
        //      one line "public ulong did { get; set; }

            /// <remarks/>
            public ulong did
            {
                get
                {
                    return this.didField;
                }
                set
                {
                    this.didField = value;
                }
            }

            /// <remarks/>
            public string signupCountry
            {
                get
                {
                    return this.signupCountryField;
                }
                set
                {
                    this.signupCountryField = value;
                }
            }

            /// <remarks/>
            public ulong smsNumber
            {
                get
                {
                    return this.smsNumberField;
                }
                set
                {
                    this.smsNumberField = value;
                }
            }

            /// <remarks/>
            public string smsCode
            {
                get
                {
                    return this.smsCodeField;
                }
                set
                {
                    this.smsCodeField = value;
                }
            }

            /// <remarks/>
            public string hasValidated
            {
                get
                {
                    return this.hasValidatedField;
                }
                set
                {
                    this.hasValidatedField = value;
                }
            }

            /// <remarks/>
            public string smsVerificationCancelled
            {
                get
                {
                    return this.smsVerificationCancelledField;
                }
                set
                {
                    this.smsVerificationCancelledField = value;
                }
            }

            /// <remarks/>
            public string hasPaymentValidated
            {
                get
                {
                    return this.hasPaymentValidatedField;
                }
                set
                {
                    this.hasPaymentValidatedField = value;
                }
            }

            /// <remarks/>
            public byte smsAttempts
            {
                get
                {
                    return this.smsAttemptsField;
                }
                set
                {
                    this.smsAttemptsField = value;
                }
            }

            /// <remarks/>
            public string signupIp
            {
                get
                {
                    return this.signupIpField;
                }
                set
                {
                    this.signupIpField = value;
                }
            }

            /// <remarks/>
            public string clientVersion
            {
                get
                {
                    return this.clientVersionField;
                }
                set
                {
                    this.clientVersionField = value;
                }
            }

            /// <remarks/>
            public string email
            {
                get
                {
                    return this.emailField;
                }
                set
                {
                    this.emailField = value;
                }
            }

            /// <remarks/>
            public byte active
            {
                get
                {
                    return this.activeField;
                }
                set
                {
                    this.activeField = value;
                }
            }

            /// <remarks/>
            public byte banned
            {
                get
                {
                    return this.bannedField;
                }
                set
                {
                    this.bannedField = value;
                }
            }

            /// <remarks/>
            public byte confirmationSent
            {
                get
                {
                    return this.confirmationSentField;
                }
                set
                {
                    this.confirmationSentField = value;
                }
            }

            /// <remarks/>
            public uint voxox_id
            {
                get
                {
                    return this.voxox_idField;
                }
                set
                {
                    this.voxox_idField = value;
                }
            }

            /// <remarks/>
            public uint i_account
            {
                get
                {
                    return this.i_accountField;
                }
                set
                {
                    this.i_accountField = value;
                }
            }

            /// <remarks/>
            public string countryList
            {
                get
                {
                    return this.countryListField;
                }
                set
                {
                    this.countryListField = value;
                }
            }

            /// <remarks/>
            public object billingDate
            {
                get
                {
                    return this.billingDateField;
                }
                set
                {
                    this.billingDateField = value;
                }
            }

            /// <remarks/>
            public ushort textingSubscription
            {
                get
                {
                    return this.textingSubscriptionField;
                }
                set
                {
                    this.textingSubscriptionField = value;
                }
            }

            /// <remarks/>
            public decimal balance
            {
                get
                {
                    return this.balanceField;
                }
                set
                {
                    this.balanceField = value;
                }
            }

            /// <remarks/>
            public ushort ProductId //CODE-REVIEW: initial uppercase, while others are initial lowercase
            {
                get
                {
                    return this.productIdField;
                }
                set
                {
                    this.productIdField = value;
                }
            }

            /// <remarks/>
            public string ProductName   //CODE-REVIEW: initial uppercase, while others are initial lowercase
            {
                get
                {
                    return this.productNameField;
                }
                set
                {
                    this.productNameField = value;
                }
            }

            /// <remarks/>
            public string sipPassword
            {
                get
                {
                    return this.sipPasswordField;
                }
                set
                {
                    this.sipPasswordField = value;
                }
            }

            /// <remarks/>
            public ulong cid
            {
                get
                {
                    return this.cidField;
                }
                set
                {
                    this.cidField = value;
                }
            }

            /// <remarks/>
            public string sipServer
            {
                get
                {
                    return this.sipServerField;
                }
                set
                {
                    this.sipServerField = value;
                }
            }

            /// <remarks/>
            public string sipIP
            {
                get
                {
                    return this.sipIPField;
                }
                set
                {
                    this.sipIPField = value;
                }
            }

            /// <remarks/>
            public string turnServer
            {
                get
                {
                    return this.turnServerField;
                }
                set
                {
                    this.turnServerField = value;
                }
            }

            /// <remarks/>
            public string turnIP
            {
                get
                {
                    return this.turnIPField;
                }
                set
                {
                    this.turnIPField = value;
                }
            }

            /// <remarks/>
            public string turnUsername
            {
                get
                {
                    return this.turnUsernameField;
                }
                set
                {
                    this.turnUsernameField = value;
                }
            }

            /// <remarks/>
            public string turnPassword
            {
                get
                {
                    return this.turnPasswordField;
                }
                set
                {
                    this.turnPasswordField = value;
                }
            }

            /// <remarks/>
            public string packetReflectorIp
            {
                get
                {
                    return this.packetReflectorIpField;
                }
                set
                {
                    this.packetReflectorIpField = value;
                }
            }

            /// <remarks/>
            public ushort packetReflectorPortBase
            {
                get
                {
                    return this.packetReflectorPortBaseField;
                }
                set
                {
                    this.packetReflectorPortBaseField = value;
                }
            }

            /// <remarks/>
            public ushort packetReflectorPortCount
            {
                get
                {
                    return this.packetReflectorPortCountField;
                }
                set
                {
                    this.packetReflectorPortCountField = value;
                }
            }

            /// <remarks/>
            public object firstName
            {
                get
                {
                    return this.firstNameField;
                }
                set
                {
                    this.firstNameField = value;
                }
            }

            /// <remarks/>
            public object lastName
            {
                get
                {
                    return this.lastNameField;
                }
                set
                {
                    this.lastNameField = value;
                }
            }

            /// <remarks/>
            public string key
            {
                get
                {
                    return this.keyField;
                }
                set
                {
                    this.keyField = value;
                }
            }

            /// <remarks/>
            public string status
            {
                get
                {
                    return this.statusField;
                }
                set
                {
                    this.statusField = value;
                }
            }
    }
}
