﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;					//SessionManager
using Desktop.Model.Calling;			//Sound, PhoneCallMessage
using Desktop.Model.Dialer;				//CountryCode
using Desktop.Util;						//PhoneUtils
using Desktop.Utils;					//VXLogger

using ManagedDataTypes;					//RecentCallList

using GalaSoft.MvvmLight;				//ViewModelBase, MessengerInstance, Set
using GalaSoft.MvvmLight.CommandWpf;	//RelayCommand

using System;							//Uri, Exception, Int32
using System.Collections.Generic;		//List
using System.Linq;						//<query> getMostRecentCall()
using System.ComponentModel;			//INotifyPropertyChanged
using System.Windows;					//DependencyProperty
using System.Windows.Input;				//ICommand
using System.Windows.Media;				//PointCollection

namespace Desktop.ViewModels.Dialer
{
    public class DialPadViewModel : ViewModelBase
    {
        #region | Private Class Variables |

        private VXLogger		logger = VXLoggingManager.INSTANCE.GetLogger("DialPadViewModel");
        private PointCollection pointerPoints;	//Pointer to NavBar

        private string	phoneNumber;
        private bool	allowCountrySelection;
        private bool	isFromCall;
        private Int32	currentCallId = -1;
        private string	balance;
        private string	alphaPhoneNumber;
        private bool	isAlphaPhoneNumber;
		private CountryCode selectedCountryCode;

        #endregion

        #region | Constructors |

        public DialPadViewModel()
        {
            KeyCommand			= new RelayCommand<string>(Keyed, (string value) => { return true; });
            CallCommand			= new RelayCommand(Call, CanCall);
            DialerCloseCommand	= new RelayCommand(OnDialerClose, () => { return true; });

            AllowCountrySelection = true;

            MessengerInstance.Register<DialPadShowMessage>	  ( this, OnShowDialPad		);
            MessengerInstance.Register<PhoneCallMessage>	  ( this, OnPhoneCallClosed	);
            MessengerInstance.Register<ClearDialerTextMessage>( this, (OnClearText) => { PhoneNumber = ""; });
            MessengerInstance.Register<BalanceChangedMessage> ( this, OnBalanceChanged	);
        }

        #endregion

        #region | Public Properties|

		//Bind-ed with VxCountryCodeComboBox
		public CountryCode SelectedCountryCode	
		{
			get { return selectedCountryCode; }
			set { Set(ref selectedCountryCode, value);  }
		}

        public string PhoneNumber
        {
            get { return phoneNumber; }
            set
            {
                Set(ref phoneNumber, value);
                CheckPhoneNumber();
            }
        }

        public bool AllowCountrySelection
        {
            get { return allowCountrySelection; }
            set { Set(ref allowCountrySelection, value); }
        }

        public string Balance
        {
            get { return balance; }
            set { Set(ref balance, value); }
        }

        /// <summary>
        /// Flag to know if the dial pad is open from Nav or Call
        /// </summary>
        public bool IsFromCall
        {
            get { return isFromCall; }
            set { Set(ref isFromCall, value); }
        }

        public string AlphaPhoneNumber
        {
            get { return alphaPhoneNumber; }
            set { Set(ref alphaPhoneNumber, value); }
        }

        public bool IsAlphaPhoneNumber
        {
            get { return isAlphaPhoneNumber; }
            set { Set(ref isAlphaPhoneNumber, value); }
        }

        public PointCollection PointerPoints
        {
            get { return pointerPoints; }
            set { Set(ref pointerPoints, value); }
        }

        public ICommand CallCommand			{ get; set; }
        public ICommand KeyCommand			{ get; set; }
        public ICommand DialerCloseCommand	{ get; set; }

        #endregion

        #region | Private Procedures |

        /// <summary>
        /// Appends dialpad character to PhoneNumber property
        /// </summary>
        /// <param name="value">Character Parameter</param>
        private void Keyed(string value)        //CODE-REVIEW: Where do we validate character?  Not all chars are permissible.
        {
            char key = value.ToCharArray()[0];

			ActionManager.Instance.PlayDtmf( key, IsFromCall );

            PhoneNumber = PhoneNumber + value;
        }

        /// <summary>
        /// Initiate call
        /// </summary>
        private void Call()
        {
            if (string.IsNullOrWhiteSpace(PhoneNumber))
            {
				//TODO: Make this a single DB or CallsManager all - getMostRecentCall();
                var				db	   = SessionManager.Instance.UserDataStore;
                RecentCallList  rclist = db.getRecentCalls(string.Empty);

                foreach(var rc in rclist.OrderByDescending(x => x.StartTime.AsLong() ) )
                {
                    if (rc.isValid() == false)
                        continue;

                    if (!rc.Incoming)
                    {
                        PhoneNumber = rc.Number;
                        break;
                    }
                }
            }
            else
            {
                if (IsFromCall)
                {
                    Model.SessionManager.Instance.CloseCall(currentCallId);
					ActionManager.Instance.CloseDialer();
                }
                else
                {
                    var error = ActionManager.Instance.MakeCall( GetNumberToDial() );

                    if (error == Model.Calling.PhoneLine.CallError.NoError)
                    {
						ActionManager.Instance.CloseDialer();
                    }
                }
            }
        }

        /// <summary>
        /// Checks whether call can be initiated
        /// </summary>
        /// <returns>Returns true if call can be initiated</returns>
        private bool CanCall()
        {
			return ( SelectedCountryCode != null );
        }

        /// <summary>
        /// Parses the phone number for Country Code
        /// </summary>
        private void CheckPhoneNumber()
        {
            //CODE-REVIEW: since all we do is log the caught exception, this should be wrapped in re-useable class to keep this code cleaner.  See comment about PhoneNumbers lib.
            //This logic will likely be use in several places in the app.
            //try/catch should start AFTER the check for IsNullOrWhiteSpace()

            if (string.IsNullOrWhiteSpace(PhoneNumber))
            {
                //CountryCode = defaultCountryKey;
                AllowCountrySelection = true;

                return;
            }

            AllowCountrySelection = !PhoneNumber.StartsWith("+");

            if (PhoneNumber.StartsWith("+"))
            {
				String countryCode = PhoneUtils.GetCountryCodeByPhone(PhoneNumber);
				CountryCode obj = CountryCodeList.Instance.getByCode( countryCode );

				if ( obj != null )
				{
					SelectedCountryCode = obj;
				}
            }
        }

        private void OnDialerClose()
        {
			ActionManager.Instance.CloseDialer();
        }

        private void OnShowDialPad(DialPadShowMessage msg)
        {
            IsFromCall    = msg.IsFromCall;
            currentCallId = msg.IsFromCall ? msg.CallId : -1;

            if ( string.IsNullOrWhiteSpace(msg.PhoneNumber) == false )
            {
               
                var countryCodeFromHyperlink = msg.CountryCode.Trim();

                //Check for the country code. If it exists, update accordingly.
				CountryCode obj = CountryCodeList.Instance.getByCode( countryCodeFromHyperlink );
				if(obj != null) 
				{
					SelectedCountryCode = obj;
				}

                PhoneNumber = msg.PhoneNumber;
            }
        }

        private string GetNumberToDial()
        {
            string ShortNumber;

            if (PhoneUtils.Is911Or411(PhoneNumber, out ShortNumber))
            {
                return ShortNumber;
            }
            else if (PhoneNumber.Contains("+"))
            {
                return PhoneNumber;
            }
            else
            {
                 string code = SelectedCountryCode.Code;

                return string.Format("{0}{1}", code, PhoneNumber);
            }
        }        

        private void OnPhoneCallClosed(PhoneCallMessage msg)
        {
            if (msg.State == Telcentris.Voxox.ManagedSipAgentApi.ManagedSipDataTypes.PhoneCallState.CallState.Closed)
            {
//                LoadBalance();	//We will get response to getDataSummary() from server
            }
        }

		private void OnBalanceChanged( BalanceChangedMessage msg )
		{
			Balance = string.Format("${0}", msg.Balance.ToString("F2"));
		}

        #endregion
    }

    public class ClearDialerTextMessage
    {
    }

    public class DialPadPointerSetMessage 
    {
        public double DialpadPointerVerticalOffset { get; set; }
    }
}
