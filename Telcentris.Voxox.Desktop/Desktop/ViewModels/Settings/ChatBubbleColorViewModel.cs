﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using GalaSoft.MvvmLight;	//ViewModelBase, Set

using System;				//String

namespace Desktop.ViewModels.Settings
{
    public class ChatBubblesColorViewModel : ViewModelBase
    {
        #region | Private Class Variables |

        private String colorName;
        private string hexValue;

        #endregion

        #region | Constructor |

        public ChatBubblesColorViewModel(string colorName, string hexValue)
        {
            this.colorName = colorName;
            this.hexValue = hexValue;
        }

        public ChatBubblesColorViewModel() { }

        #endregion

        #region | Public Properties |

        /// <summary>
        /// Chat bubbles colors names
        /// </summary>
        public string ColorName
        {
            get { return colorName; }
            set { Set(ref this.colorName, value); }
        }

        /// <summary>
        /// Hex value of colors
        /// </summary>
        public string HexValue
        {
            get { return hexValue; }
            set { Set(ref this.hexValue, value); }
        }

        #endregion
    }
}

