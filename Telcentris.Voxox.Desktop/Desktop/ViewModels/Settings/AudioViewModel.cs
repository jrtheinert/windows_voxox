﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;				//AudioDeviceMgr, DeviceChangeMessage
using Desktop.Model.Settings;		//AudioDevice
using Desktop.Utils;				//VXLogger

using GalaSoft.MvvmLight;			//ViewModelBase, MessengerInstance, Set

using System.Collections.Generic;	//List

namespace Desktop.ViewModels.Settings
{
    public class AudioViewModel : ViewModelBase
    {
		//Since audio device selection MUST respond to OS-level device changes, such as USB plug/unplug,
		//	I am moving some of this code to the model level where it belongs anyway.
		//
		//NOTE:DO NOT access SettingsManager directly from this class because AudioDeviceMgr
		//	will handle that AND the device changes.
        #region | Private Class Variables |

		//Ideally these would be in separate class
		//Input
		private List<AudioDevice> inputDevices;
		private int inputDeviceIndex;
		private int inputVolume;

		//Output
		private List<AudioDevice> outputDevices;
		private int outputDeviceIndex;
		private int outputVolume;

        #endregion

        #region | Constructor |

        public AudioViewModel()
        {
            inputDeviceIndex   = -1;
            outputDeviceIndex  = -1;

            LoadProperties();

			MessengerInstance.Register<DeviceChangeMessage>(this, onDeviceChange );		//From AudioDeviceMgr
        }

        #endregion

        #region | Public Properties |

        private static VXLogger logger = VXLoggingManager.INSTANCE.GetLogger("Desktop.AudioVieMmodel");

        public List<AudioDevice> InputDevices
		{
			get { return inputDevices; }
			set { Set(ref this.inputDevices, value); }
		}

        public List<AudioDevice> OutputDevices
		{
			get { return outputDevices; }
            set { Set(ref this.outputDevices, value); }
		}

        public int InputDeviceIndex
        {
            get { return inputDeviceIndex; }
            set
            {
                Set(ref this.inputDeviceIndex, value);

				AudioDeviceMgr.Instance.HandleInputDeviceSelection( inputDeviceIndex );
            }
        }

        public int OutputDeviceIndex
        {
            get { return outputDeviceIndex; }
            set
            {
                Set(ref this.outputDeviceIndex, value);

				AudioDeviceMgr.Instance.HandleOutputDeviceSelection( outputDeviceIndex );
            }
        }

        public int InputVolume
        {
            get { return inputVolume; }
            set
            {
                Set(ref this.inputVolume, value);
				AudioDeviceMgr.Instance.InputVolume = value;
            }
        }

        public int OutputVolume
        {
            get { return outputVolume; }
            set
            {
                Set(ref this.outputVolume, value);
				AudioDeviceMgr.Instance.OutputVolume = value;
           }
        }

        #endregion

        #region | Private methods |

        private void onDeviceChange( DeviceChangeMessage msg )
        {
			//Simply refresh device list.
			LoadProperties();
        }

		/// <summary>
        /// Sets the default values for settings page
        /// Checks if the user specific values exists or not 
        /// if exists loads else default values will be loaded and saves in the AppDB
        /// </summary>
        private void LoadProperties()
        {
			updateDevices( true  );	//Input
			updateDevices( false );	//Output
		}

		private void updateDevices( bool isInput )
		{
			List<AudioDevice> devices;
			
			if ( isInput )
				devices = AudioDeviceMgr.Instance.GetInputDevices();
			else
				devices = AudioDeviceMgr.Instance.GetOutputDevices();

			//If empty, add default text
            if (devices.Count < 1)
                devices.Add(new AudioDevice("", LocalizedString.Settings_NoDeviceText, true));

			//Update property which will trigger UI update of ComboBox
			//NOTE: Because the device count may change due to USB plug/unplug, it is possible that
			//	the 'new' index will be the same even though the device is different.
			//	In this case, the 'index' will NOT trigger an update (because it did not change)
			//	so we for that to change by setting index to -1 before all else.
			if ( isInput )
			{
				InputDeviceIndex = -1;
				InputDevices     = devices;
                InputVolume      = AudioDeviceMgr.Instance.InputVolume;
				InputDeviceIndex = updateComboBoxIndex( devices, AudioDeviceMgr.Instance.InputDeviceGuid );
			}
			else
			{ 
				OutputDeviceIndex = -1;
				OutputDevices     = devices;
                OutputVolume      = AudioDeviceMgr.Instance.OutputVolume;
				OutputDeviceIndex = updateComboBoxIndex( devices, AudioDeviceMgr.Instance.OutputDeviceGuid );
			}
		}

		private int updateComboBoxIndex( List<AudioDevice> devices, string currentDeviceGuid )
		{
			int	result = -1;

			if ( devices.Count > 0 )
			{ 
				AudioDevice device = null;	//new AudioDevice();

				for ( int index = 0; index < devices.Count; index++ )
				{
					device = devices[index];

					if ( device.Guid == currentDeviceGuid )
					{
						result = index;
						break;
					}
					else if ( device.IsDefault )	//Use default, if currently stored device is missing
					{
						result = index;				//We do NOT break here because we would prefer the match on GUID above.
					}
				}

				//Use first device, if none selected
				if (result == -1)	
					result = 0;
			}

			return result;
		}

        #endregion
	}
}
