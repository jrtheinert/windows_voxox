﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telcentris.Voxox.ManagedSipAgentApi;
using Telcentris.Voxox.ManagedSipAgentApi.ManagedSipDataTypes;

namespace Desktop.ViewModels.Settings
{
    class sample
    {
        public static void list()
        {
            AudioDeviceList audioInputDeviceList = SipAgentApi.getInstance().getSystemAudioInputDevices();
            Console.WriteLine("Input count: " + audioInputDeviceList.Count);
            foreach (AudioDevice ad in audioInputDeviceList)
            {
                Console.WriteLine("Input : " + "Guid:" + ad.Guid + ",Index:" + ad.Index + ",IsDefault:" +
                    ad.IsDefault + ",IsUseDefault:" + ad.IsUseDefault + ",Name:" + ad.Name + ",Type:" + ad.Type
                );
            }
            AudioDeviceList audioOutputDeviceList = SipAgentApi.getInstance().getSystemAudioOutputDevices();
            Console.WriteLine("Output count: " + audioOutputDeviceList.Count);
            foreach (AudioDevice ad in audioOutputDeviceList)
            {
                Console.WriteLine("Output : " + "Guid:" + ad.Guid + ",Index:" + ad.Index + ",IsDefault:" +
                    ad.IsDefault + ",IsUseDefault:" + ad.IsUseDefault + ",Name:" + ad.Name + ",Type:" + ad.Type
                );
            }
        }
    }
}
