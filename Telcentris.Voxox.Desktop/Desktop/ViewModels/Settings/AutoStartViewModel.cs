﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;				//AutoStartManager, AutoStartChangeMessage

using GalaSoft.MvvmLight;			//ViewModelBase, MessengerInstance, Set

namespace Desktop.ViewModels.Settings
{
    public class AutoStartViewModel : ViewModelBase
    {
        #region | Private class Variables |

        private bool isAutoStartEnabled;
        private bool isAutoLoginEnabled = false;
		private bool sendMessage		= true;

        #endregion

        #region | Constructors |

        public AutoStartViewModel()
        {
		    LoadProperties();

			//AutoStart - This can be changed external to Settings, from SysTray->AutoStart.
			//AutoLogin - This can be changed external to Settings, from File->Keep me logged in.
			MessengerInstance.Register<SettingsChangeMessage>(this, OnSettingsChange);
        }

        #endregion

        #region | Public Properties & Methods |

        public bool IsAutoStartEnabled
        {
            get { return isAutoStartEnabled; }
            set
            {
                Set(ref isAutoStartEnabled, value);

                if ( isAutoStartEnabled )
                {
                    AutoStartManager.RegisterInStartup();
                }
                else
                {
                    AutoStartManager.RemoveFromStartup();
                }
            }
        }

		public bool IsAutoLoginEnabled
        {
            get { return isAutoLoginEnabled; }
            set
            {
                Set(ref this.isAutoLoginEnabled, value);
                SettingsManager.Instance.App.AutoLogin = value;

				if ( sendMessage )
				{ 
					ActionManager.Instance.HandleAutoLoginChange();
				}
            }
        }

        #endregion

        #region | Private Procedures |

        private void LoadProperties()
        {
            IsAutoStartEnabled = AutoStartManager.IsInStartup();
            IsAutoLoginEnabled = SettingsManager.Instance.App.AutoLogin;
        }

		private void OnSettingsChange( SettingsChangeMessage msg )
		{
			if ( msg.isForAutoStart() )
			{
				LoadProperties();
			}
			else if ( msg.isForAutoLogin() )
			{
				sendMessage = false;
				LoadProperties();
				sendMessage = true;
			}
		}

        #endregion
    }
}
