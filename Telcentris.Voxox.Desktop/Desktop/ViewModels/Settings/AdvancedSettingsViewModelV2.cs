﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Aug 2015
 *		(actually Murali G. of Orchestra, but we want an email address)
 */

using Desktop.Config;					//LoginMode
using Desktop.Model;

using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;		//Messenger
using GalaSoft.MvvmLight.CommandWpf;	//RelayCommand

using System;							//String
using System.Windows.Controls;			//Combobox
using System.Windows.Forms;				//MessageBox, MessageBoxButtons
using System.Windows.Input;				//ICommand
using System.Collections.ObjectModel;	//ObservableCollection

namespace Desktop.ViewModels.Settings
{
    public class AdvancedSettingsViewModelV2 : ViewModelBase
    {
        private string	sipServer;
//        private bool	useProductionApiKey;
        private bool	enableTURN;

		//These display effective values based on user selections.
        private string	effectiveApiServer;
        private string	effectiveXmppServer;
		private string  effectiveExtranetHost;

		//Indices for comboboxes
		private int		brandSelectedIndex;
		private int		apiEnvSelectedIndex;
		private int		apiEnvModSelectedIndex;
		private int		partnerIdSelectedIndex;
		private int		loginModeSelectedIndex;

		//Internals
//		private bool	verifiedUseProductionApiKey;
		private int		prevBrandIndex;			//So we can undo selection.
		private bool	undoingRebrand  = false;
		private bool	loadingDefaults = false;


        public ObservableCollection<String> Brands		{ get; set; }
        public ObservableCollection<String> ApiEnvs		{ get; set; }
        public ObservableCollection<String> ApiEnvMods	{ get; set; }
        public ObservableCollection<String> PartnerIds	{ get; set; }
        public ObservableCollection<String> LoginModes	{ get; set; }


        #region | Constructor |

        public AdvancedSettingsViewModelV2()
        {
//			verifiedUseProductionApiKey = false;

            ApplySipServerCommand	= new RelayCommand( ApplySipServer,  () => { return !string.IsNullOrWhiteSpace(SipServer); });
            ResetCommand			= new RelayCommand( ResetSettings );
  
			PopulateData();
			LoadDefaults();
			UpdateEffectiveValues();

//			Messenger.Default.Register<BrandingChangedMessage>(this, OnBrandingChanged);
        }

        #endregion

        #region | Public Properties |

        public int BrandSelectedIndex
        {
			get { return brandSelectedIndex; }
			set { 
					if ( !undoingRebrand && !loadingDefaults )
					{ 
						prevBrandIndex = brandSelectedIndex;
					}

					Set(ref brandSelectedIndex, value);

					if ( !undoingRebrand && !loadingDefaults )
					{ 
						Rebrand();
					}
			}
        }

        public int ApiEnvSelectedIndex
        {
            get { return apiEnvSelectedIndex; }
            set { 
                Set(ref apiEnvSelectedIndex, value);
				UpdateEffectiveValues();
            }
        }

        public int ApiEnvModSelectedIndex
        {
            get { return apiEnvModSelectedIndex; }
            set { 
                Set(ref apiEnvModSelectedIndex, value);
				UpdateEffectiveValues();
            }
        }

        public int PartnerIdSelectedIndex
        {
            get { return partnerIdSelectedIndex; }
            set { 
                Set(ref partnerIdSelectedIndex, value);
				UpdateEffectiveValues();
            }
        }

        public int LoginModeSelectedIndex
        {
            get { return loginModeSelectedIndex; }
            set { 
                Set(ref loginModeSelectedIndex, value);
				UpdateEffectiveValues();
            }
        }

        public string EffectiveApiServer
        {
            get { return effectiveApiServer; }
            set
            {
                Set(ref effectiveApiServer, value);
            }
        }

        public string EffectiveXmppServer
        {
            get { return effectiveXmppServer; }
            set
            {
                Set(ref effectiveXmppServer, value);
            }
        }

        public string EffectiveExtranetHost
        {
            get { return effectiveExtranetHost; }
            set
            {
                Set(ref effectiveExtranetHost, value);
            }
        }

        public string SipServer
        {
            get { return sipServer; }
            set
            {
                Set(ref sipServer, value);
            }
        }

        public string DefaultSipServer
        {
            get { return "Sip server (from API)"; }
        }

		//public bool UseProductionApiKey
		//{
		//	get { return useProductionApiKey; }
		//	set
		//	{
		//		Set(ref useProductionApiKey, value);
		//		SettingsManager.Instance.Advanced.UseProductionApiKey = value;
		//	}
		//}

        public bool EnableTURN
        {
            get { return enableTURN; }
            set
            {
                Set(ref enableTURN, value);
				SettingsManager.Instance.Advanced.EnableTURN = value;
            }
        }

		public ICommand ApplySipServerCommand	{ get; private set; }
        public ICommand ResetCommand			{ get; private set; }

        #endregion

        #region | Private Procedures |

		//TODO: Get valid values from ApiConfig class
		private void PopulateData()
		{
			Brands     = new ObservableCollection<String>();
			ApiEnvs    = new ObservableCollection<String>();
			ApiEnvMods = new ObservableCollection<String>();
			PartnerIds = new ObservableCollection<String>();
			LoginModes = new ObservableCollection<String>();

			//Brands
			Brands.Add( BrandId.CloudPhone.ToString()	);
			Brands.Add( BrandId.Voxox.ToString()		);
			Brands.Add( BrandId.VoicePass.ToString()	);
//			Brands.Add( BrandId.None.ToString()			);

			//As of 2016.01.04
			ApiEnvs.Add( ""			 );	//Force to Production
			ApiEnvs.Add( "c3po"		 );	//clean, unanchored / 2 TCS servers - Yuri
			ApiEnvs.Add( "chewie"	 );	//vxMaster - MR45
			ApiEnvs.Add( "lando"	 );	//vxMaster - Available
			ApiEnvs.Add( "skywalker" );	//vtc_2.5.1 - vtc_2.5.1 testing
			ApiEnvs.Add( "solo"		 );	//jira/vtc/CP-4971-inbound-call-interaction-malform - VS testing/Shanghai TCS
			ApiEnvs.Add( "vader"	 );	//cpVx_3.5.5 - Cloudphone
			ApiEnvs.Add( "yoda"		 );	//vx/vtc - Staging

			ApiEnvMods.Add( ""    );	//Force to Production
			ApiEnvMods.Add( "hp"  );	//VoicePass - HP
			ApiEnvMods.Add( "vtc" );	//VTC
			ApiEnvMods.Add( "cp"  );	//CloudPhone - Voxox
			ApiEnvMods.Add( "vx"  );	//Voxox		 - Voxox

			//Similar to ApiEnvMods, but used for different purpose
			PartnerIds.Add( ""    );	//Force to Production.
			PartnerIds.Add( "hp"  );	//VoicePass - HP
			PartnerIds.Add( "vtc" );	//VTC
			PartnerIds.Add( "cp"  );	//CloudPhone - Voxox
			PartnerIds.Add( "vx"  );	//Voxox		 - Voxox

			//Login Modes
			LoginModes.Add( LoginMode.Default.ToString() );
			LoginModes.Add( LoginMode.Email.ToString()   );
			LoginModes.Add( LoginMode.Mobile.ToString()  );
		}

        private void LoadDefaults()
        {
			loadingDefaults = true;

			BrandSelectedIndex     = Brands.IndexOf    ( Config.Brand.mBrandId.ToString()						);

			ApiEnvSelectedIndex    = ApiEnvs.IndexOf   ( SettingsManager.Instance.Advanced.ApiEnv				);
			ApiEnvModSelectedIndex = ApiEnvMods.IndexOf( SettingsManager.Instance.Advanced.ApiEnvModifier		);
			PartnerIdSelectedIndex = PartnerIds.IndexOf( SettingsManager.Instance.Advanced.PartnerIdentifier	);
			LoginModeSelectedIndex = LoginModes.IndexOf( SettingsManager.Instance.Advanced.LoginMode.ToString() );

			SipServer			= SettingsManager.Instance.Advanced.UseSipServer  ? SettingsManager.Instance.Advanced.SipServer  : string.Empty;
//            UseProductionApiKey = SettingsManager.Instance.Advanced.UseProductionApiKey;
            EnableTURN			= SettingsManager.Instance.Advanced.EnableTURN;

			loadingDefaults = false;
        }

        private void ApplySipServer()
        {
            SettingsManager.Instance.Advanced.SipServer		= SipServer;
            SettingsManager.Instance.Advanced.UseSipServer	= true;
        }

        private void ResetSettings()
        {
            SettingsManager.Instance.Advanced.resetToDefaultsV2();
            LoadDefaults();
        }

		private void Rebrand()
		{
			if ( verifyRebrand() )
			{
				//Reset Advanced settings

				//Change Config.Brand(ing)
				String brandText = (BrandSelectedIndex >= 0 ?  Brands[BrandSelectedIndex] : String.Empty );

				BrandId	brandId;
				Enum.TryParse( brandText, out brandId );

				//Change Config.Brand(ing) and Re-init app.
				ActionManager.Instance.Rebrand( brandId );	
				ActionManager.Instance.CloseAdvancedSettingsV2Window();
			}
			else 
			{ 
				undoingRebrand = true;
				BrandSelectedIndex = prevBrandIndex;
				ActionManager.Instance.UpdateAdvancedSettingsV2Window( prevBrandIndex );
				undoingRebrand = false;
			}
		}

		//private void OnBrandingChanged( BrandingChangedMessage msg )
		//{
		//	if ( msg.isActionUpdateBrandComboBox() )
		//	{
		//		BrandSelectedIndex = msg.BrandSelectedIndex;
		//	}
		//}

		public void UpdateEffectiveValues()
		{
			//We do NOT handle Brand here because that is handled elsewhere and causes other values to be reset (by design).
			String apiEnv	 = (ApiEnvSelectedIndex    >= 0 ?  ApiEnvs[ApiEnvSelectedIndex]       : String.Empty );
			String apiEnvMod = (ApiEnvModSelectedIndex >= 0 ?  ApiEnvMods[ApiEnvModSelectedIndex] : String.Empty );
			String partnerId = (PartnerIdSelectedIndex >= 0 ?  PartnerIds[PartnerIdSelectedIndex] : String.Empty );
			String loginMode = (LoginModeSelectedIndex >= 0 ?  LoginModes[LoginModeSelectedIndex] : String.Empty );

			EffectiveApiServer    = Config.Brand.makeApiServer  ( apiEnv, apiEnvMod );
			EffectiveXmppServer   = Config.Brand.makeXmppServer ( apiEnv, apiEnvMod );
			EffectiveExtranetHost = AppConfigUtil.makeExtranetUrl( apiEnv, apiEnvMod, true, "" );
		}

        #endregion

		public bool Save()
		{
			bool result = false;

			if ( selectionsAreAcceptable() )
			{ 
				String apiEnv		 = (ApiEnvSelectedIndex    >= 0 ?  ApiEnvs[ApiEnvSelectedIndex]       : String.Empty );
				String apiEnvMod	 = (ApiEnvModSelectedIndex >= 0 ?  ApiEnvMods[ApiEnvModSelectedIndex] : String.Empty );
				String partnerId	 = (PartnerIdSelectedIndex >= 0 ?  PartnerIds[PartnerIdSelectedIndex] : String.Empty );
				String loginModeText = (LoginModeSelectedIndex >= 0 ?  LoginModes[LoginModeSelectedIndex] : String.Empty );

				//Get actual loginMode
				LoginMode	loginMode;
				Enum.TryParse( loginModeText, out loginMode );

				//These are saved so we can re-enter Advance Settings without losing previously selected data.
				SettingsManager.Instance.Advanced.ApiEnv            = apiEnv;
				SettingsManager.Instance.Advanced.ApiEnvModifier    = apiEnvMod;
				SettingsManager.Instance.Advanced.PartnerIdentifier = partnerId;
				SettingsManager.Instance.Advanced.LoginMode			= loginMode;

				//SIP Server
				SettingsManager.Instance.Advanced.SipServer		= SipServer;
				SettingsManager.Instance.Advanced.UseSipServer	= !String.IsNullOrEmpty( SipServer );

				//This does the Rebranding
				SettingsManager.Instance.Advanced.saveV2( EffectiveApiServer, EffectiveXmppServer, EffectiveExtranetHost, partnerId );

				result = true;
			}

			return result;
		}

		private bool verifyRebrand()
		{
			bool result = false;

			//I am having a problem resetting the ComboBox if user declines change.
			//	So for now, just notify user what will happen.
			bool allowOption = false;
			DialogResult dlgResult;

			if ( allowOption )
			{ 
				String msg = "You have chosen to RE-BRAND this app. \n" +
							 "  Doing so will reset all other Advanced Settings.  \n" +
							 "  Is this correct? \n\n" +
								"Click Yes to Re-brand\n"+
								"Click No to return to previous brand\n";

				dlgResult = MessageBox.Show( msg, Config.Branding.AppName, MessageBoxButtons.YesNo, MessageBoxIcon.Warning );
			}
			else
			{ 
				String msg = "You have chosen to RE-BRAND this app. \n" +
							 "  Doing so will reset all other Advanced Settings.  \n";

				dlgResult = MessageBox.Show( msg, Config.Branding.AppName, MessageBoxButtons.OK, MessageBoxIcon.Warning );
			}

			switch ( dlgResult )
			{
			case DialogResult.Yes:
				result = true;
				break;
						
			case DialogResult.No:
				result = false;
				break;
						
			case DialogResult.OK:
				result = true;
				break;
			}

			return result;
		}

		private bool selectionsAreAcceptable()
		{
			bool result = true;

			//If we have apiEnv change, then ensure UseProductionApiKey is unchecked and prompt user if it is.
			String apiEnv	 = (ApiEnvSelectedIndex    >= 0 ?  ApiEnvs[ApiEnvSelectedIndex]       : String.Empty );

			if ( !String.IsNullOrEmpty( apiEnv ) )	//&& UseProductionApiKey )
			{
//				if ( !verifiedUseProductionApiKey )
//				{
//					String msg = "You have a non-production API Environment and have left Use Production Key checked.  \n" +
//								 "Is this correct? \n\n" +
//								 "Click Yes to automatically change to typical setting (recommended),\n"+
//								 "Click No to continue as is,\n" +
//								 "Click Cancel to cancel Save operation.";

//					DialogResult dlgResult = MessageBox.Show( msg, Config.Branding.AppName, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning );

//					switch ( dlgResult )
//					{
//					case DialogResult.Yes:
//						UseProductionApiKey = false;
//						result = true;
//						break;
						
//					case DialogResult.No:
//						result = true;
//						break;

//					case DialogResult.Cancel:
//						result = false;
//						break;
//					}
//				}
			}

			return result;
		}
    }
}
