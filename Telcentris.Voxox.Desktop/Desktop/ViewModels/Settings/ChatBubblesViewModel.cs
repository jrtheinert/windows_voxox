﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;					//AvatarManager, SessionManager, SettingsManager
using Desktop.Utils;					//VXLogger
using Desktop.ViewModels.Messages;		//ChangeChatBubbleCollorMessage

using GalaSoft.MvvmLight;				//ViewModelBase, MessengerInstance, Set

using System;							//String, DateTime, Uri, Exception
using System.Collections.Generic;		//List
using System.Collections.ObjectModel;	//ObservableCollection
using System.Globalization;				//CultureInfo, DateTimeFormatInfo
using System.IO;						//Path - For Chat Bubble color names (TODO: Move to Model layer)
using System.Linq;						//<query>
using System.Windows;					//Application
using System.Windows.Media;				//Brush, BrushConverter
using System.Xml;						//XmlDocument (for colors.xml) TODO: Move to Model layer

namespace Desktop.ViewModels.Settings
{
    public class ChatBubblesViewModel : ViewModelBase
    {
        #region | Private Class Variables |

        private string conversantUri;
        private Brush conversantBubbleColor;
        private String time;
        private string userUri;
        private Brush userBubbleColor;
        private ObservableCollection<ChatBubblesColorViewModel> chatBubbles;
        private int userChatBubble;
        private int conversantChatBubble;
        private ChatBubblesColorViewModel chatBubble;
        private string userChatBubbleColor;
        private string conversantChatBubbleColor;
        private VXLogger logger = VXLoggingManager.INSTANCE.GetLogger("ChatBubblesViewModel");

        #endregion

        #region | Constructor |

        public ChatBubblesViewModel()
        {
            chatBubble = new ChatBubblesColorViewModel();
            LoadDefaults();
        }

        #endregion

        #region | Public Properties and Methods |

        /// <summary>
        /// List of chat bubbles colors and their values
        /// </summary>
        public ObservableCollection<ChatBubblesColorViewModel> ChatBubbles
        {
            get { return chatBubbles; }
            set { Set(ref this.chatBubbles, value); }
        }

        /// <summary>
        /// Sets the coversant avatar
        /// </summary>
        public string ConversantUri
        {
            get { return conversantUri; }
            set { Set(ref this.conversantUri, value); }
        }

        /// <summary>
        /// sets the current time as chat sentand receive time
        /// </summary>
        public string Time
        {
            get { return time; }
            set { Set(ref this.time, value); }
        }

        /// <summary>
        /// sets the conversant chat bubble color
        /// </summary>
        public Brush ConversantBubbleColor
        {
            get { return conversantBubbleColor; }
            set { Set(ref this.conversantBubbleColor, value); }
        }

        /// <summary>
        /// Sets the user chat bubble color
        /// </summary>
        public Brush UserBubbleColor
        {
            get { return userBubbleColor; }
            set { Set(ref this.userBubbleColor, value); }
        }

        /// <summary>
        /// Sets the user Avatar
        /// </summary>
        public string UserUri
        {
            get { return userUri; }
            set { Set(ref this.userUri, value); }
        }

        /// <summary>
        /// Binds with the User Chat bubble color index
        /// </summary>
        public int UserChatBubble
        {
            get { return userChatBubble; }
            set
            {
                Set(ref this.userChatBubble, value);
                ChangeUserChatBubbleColor(value);
            }
        }

        /// <summary>
        /// Binds with the Contacts chat bubble index
        /// </summary>
        public int ConversantChatBubble
        {
            get { return conversantChatBubble; }
            set
            {
                Set(ref this.conversantChatBubble, value);
                ChangeConversantChatBubbleColor(value);
            }
        }
        /// <summary>
        /// Binds with the Contacts chat bubble color
        /// </summary>
        public string ConversantChatBubbleColor
        {
            get { return conversantChatBubbleColor; }
            set
            {
                Set(ref this.conversantChatBubbleColor, value);
            }
        }
        /// <summary>
        /// Binds with the user chat bubble color
        /// </summary>
        public string UserChatBubbleColor
        {
            get { return userChatBubbleColor; }
            set
            {
                Set(ref this.userChatBubbleColor, value);
            }
        }

        #endregion

        #region | Private Methods |

        /// <summary>
        ///  Load the user specific or default values 
        ///  Shows the current time in the 
        /// </summary>
        private void LoadDefaults()
        {
            try
            {
                // Gets the current time and converts ot into am/pm format 
                //Ex: 11:30 am / 11:30 pm
                DateTimeFormatInfo timeFormat = new DateTimeFormatInfo();
                timeFormat.ShortTimePattern = CultureInfo.CurrentCulture.DateTimeFormat.ShortTimePattern;
                timeFormat.AMDesignator = "am";
                timeFormat.PMDesignator = "pm";
                time = String.Format(timeFormat, "{0:t}", DateTime.Now);

                //TODO - NEED to Change according to the current user
                conversantUri = AvatarManager.Instance.GetUriForMessaging( string.Empty );
                userUri = SessionManager.Instance.User.AvatarUri;

                // Gets the user specific chat bubbles colors from AppDb
                //TODO : Need to get it from DB
                chatBubbles				  = new ObservableCollection<ChatBubblesColorViewModel>();
                UserChatBubbleColor		  = SettingsManager.Instance.User.MyChatBubbleColor;
                ConversantChatBubbleColor = SettingsManager.Instance.User.MyConversantChatBubbleColor;

                //List of available colors Reading from Colors.xml file
                var uri = new Uri(Path.Combine("pack://application:,,,/", LocalizedString.ChatBubblesColorsName));
                var colorsViewModelStream = Application.GetResourceStream(uri);
                var colors = new List<ChatBubblesColorViewModel>();
                XmlDocument doc = new XmlDocument();
                using (var stream = colorsViewModelStream.Stream)
                {
                    doc.Load(stream);

                    using (XmlNodeList nodeList = doc.SelectNodes("Colors/Color"))
                    {
                        foreach (XmlNode node in nodeList)
                        {
                            colors.Add(new ChatBubblesColorViewModel()
                            {
                                ColorName = node.ChildNodes[0].InnerText,
                                HexValue = node.ChildNodes[1].InnerText
                            });
                        }
                    }
                }
                foreach (var item in (colors.ToList()))
                {
                    chatBubbles.Add(item);
                    if (userChatBubbleColor.ToLower().Trim().Equals(item.ColorName.ToLower().Trim()))
                        userBubbleColor = (Brush)(new BrushConverter().ConvertFrom(item.HexValue));
                    if (conversantChatBubbleColor.ToLower().Trim().Equals(item.ColorName.ToLower().Trim()))
                        conversantBubbleColor = (Brush)(new BrushConverter().ConvertFrom(item.HexValue));
                }
            }
            catch (Exception ex)
            {
                logger.Error("ChatbubbleViewModel", ex);
            }
        }

        #endregion

        #region | OnSelectevents |
        /// <summary>
        /// Saves the selected chat bubble  for user
        /// </summary>
        /// <param name="value"></param>
        private void ChangeUserChatBubbleColor(int value)
        {
            chatBubble	    = chatBubbles.ElementAt(userChatBubble);
            UserBubbleColor = (Brush)(new BrushConverter().ConvertFrom(chatBubble.HexValue));

            SettingsManager.Instance.User.MyChatBubbleColor = chatBubble.ColorName;

            MessengerInstance.Send<ChangeChatBubbleColorMessage>(new ChangeChatBubbleColorMessage());
        }

        /// <summary>
        /// Saves the Chatbuvbble color for conversant
        /// </summary>
        /// <param name="value"></param>
        private void ChangeConversantChatBubbleColor(int value)
        {
            chatBubble			  = chatBubbles.ElementAt(conversantChatBubble);
            ConversantBubbleColor = (Brush)(new BrushConverter().ConvertFrom(chatBubble.HexValue));

            SettingsManager.Instance.User.MyConversantChatBubbleColor = chatBubble.ColorName;

            MessengerInstance.Send<ChangeChatBubbleColorMessage>(new ChangeChatBubbleColorMessage());
        }

        #endregion
    }
}
