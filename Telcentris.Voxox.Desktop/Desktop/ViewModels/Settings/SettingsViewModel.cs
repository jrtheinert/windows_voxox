﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;				//ActionManager

using GalaSoft.MvvmLight;			//ViewModelBase, MessengerInstance, Set
using GalaSoft.MvvmLight.CommandWpf;	//RelayCommand

using System.Windows.Input;			//ICommand

namespace Desktop.ViewModels.Settings
{
    /// <summary>
    /// Active tab Enumeration
    /// </summary>
    public enum ActiveTabEnum : int
    {
        Audio,
        ChatSettings,
        ChatBubbles,
        Sounds,
        KeypadDefault,
        AutoStart,
        Support,
        AutoLogin
    }

    public class SettingsViewModel : ViewModelBase
    {
        #region | Private Variables |

        // To Check which tab isactive
        private ActiveTabEnum activeTab;
        // Right panel navigation control
        private ViewModelBase rightPanel;

        private static SettingsViewModel instance;

        #endregion

        #region | Constructors |

        private SettingsViewModel()
        {
            AudioCommand		= new RelayCommand<Navigation>( (Navigation) => OnNavCommand( Navigation.Audio			) );
            AutoStartCommand	= new RelayCommand<Navigation>( (Navigation) => OnNavCommand( Navigation.AutoStart		) );
            CallerIdCommand		= new RelayCommand<Navigation>( (Navigation) => OnNavCommand( Navigation.CallerID		) );
            ChatBubblesCommand	= new RelayCommand<Navigation>( (Navigation) => OnNavCommand( Navigation.ChatBubbles	) );
            ChatSettingsCommand = new RelayCommand<Navigation>( (Navigation) => OnNavCommand( Navigation.ChatSettings	) );
            KeypadCommand		= new RelayCommand<Navigation>( (Navigation) => OnNavCommand( Navigation.KeypadDefault	) );
            ReachMeCommand		= new RelayCommand<Navigation>( (Navigation) => OnNavCommand( Navigation.ReachMe		) );
            SoundsCommand		= new RelayCommand<Navigation>( (Navigation) => OnNavCommand( Navigation.Sounds			) );
            SupportCommand		= new RelayCommand<Navigation>( (Navigation) => OnNavCommand( Navigation.Support		) );

            MessengerInstance.Register<SettingsNavigateMessage>(this, NavigateTo);

            // Default screen control
            ActiveTab  = ActiveTabEnum.Support;
            RightPanel = new SupportViewModel();
        }

        public static SettingsViewModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new SettingsViewModel();
                }
                return instance;
            }
        }

        #endregion

        #region | Public properties & Methods |
        /// <summary>
        /// Commands to change right panel navigation
        /// </summary>
        public ICommand AudioCommand		{ get; set; }
        public ICommand AutoStartCommand	{ get; set; }
        public ICommand CallerIdCommand		{ get; set; }
        public ICommand ChatBubblesCommand	{ get; set; }
        public ICommand ChatSettingsCommand { get; set; }
        public ICommand KeypadCommand		{ get; set; }
        public ICommand ReachMeCommand		{ get; set; }
        public ICommand SoundsCommand		{ get; set; }
        public ICommand SupportCommand		{ get; set; }

        public ActiveTabEnum ActiveTab
        {
            get { return activeTab; }
            set { Set(ref this.activeTab, value); }
        }

        public ViewModelBase RightPanel
        {
            get { return rightPanel; }
            set { Set(ref this.rightPanel, value); }
        }

        #endregion

        #region | Private Methods |

		private void OnNavCommand( Navigation navTo )
		{
			switch( navTo )
			{
			case Navigation.Audio:
				handleNavTab( ActiveTabEnum.Audio, Navigation.Audio );
				break;

			case Navigation.AutoStart:
				handleNavTab( ActiveTabEnum.AutoStart, Navigation.AutoStart );
				break;

			case Navigation.CallerID:
	            ActionManager.Instance.ShowCallerIdUrl();		//Goes to portal
				break;

			case Navigation.ChatBubbles:
				handleNavTab( ActiveTabEnum.ChatBubbles, Navigation.ChatBubbles );
				break;

			case Navigation.ChatSettings:
				handleNavTab( ActiveTabEnum.ChatSettings, Navigation.ChatSettings );
				break;

			case Navigation.KeypadDefault:
				handleNavTab( ActiveTabEnum.KeypadDefault, Navigation.KeypadDefault );
				break;

			case Navigation.ReachMe:
	            ActionManager.Instance.ShowReachMeUrl();		//Goes to portal
				break;

			case Navigation.Sounds:
				handleNavTab( ActiveTabEnum.Sounds, Navigation.Sounds );
				break;

			case Navigation.Support:
				handleNavTab( ActiveTabEnum.Support, Navigation.Support );
				break;

			default:
				//TODO: log warning message?
				break;
			}
		}

		private void handleNavTab( ActiveTabEnum newTab, Navigation navTo )
		{
            if ( ActiveTab != newTab )
            {
                ActiveTab = newTab;
//                MessengerInstance.Send<SettingsNavigateMessage>(new SettingsNavigateMessage { NavigateTo = navTo });
				//No need to Send message since we are the only ones listening.
				NavigateTo( navTo );
            }
		}

		//Keep this for now.  We may need it for menu-based navigation.
        private void NavigateTo( SettingsNavigateMessage msg )
        {
			NavigateTo( msg.NavigateTo );
		}

        private void NavigateTo( Navigation navTo )
        {
            switch ( navTo )
            {
            case Navigation.Audio:
                RightPanel = new AudioViewModel();
                break;
            case Navigation.AutoStart:
                RightPanel = new AutoStartViewModel();
                break;
            case Navigation.ChatBubbles:
                RightPanel = new ChatBubblesViewModel();
                break;
            case Navigation.ChatSettings:
                RightPanel = new ChatSettingsViewModel();
                break;
            case Navigation.KeypadDefault:
                RightPanel = new CountryListViewModel();
                break;
            case Navigation.Support:
                RightPanel = new SupportViewModel();
                break;
            case Navigation.Sounds:
                RightPanel = new SoundsViewModel();
                break;

			//These go to portal, so no panel to display.
			case Navigation.CallerID:
			case Navigation.ReachMe:
			default:
				break;
            }
        }

        #endregion
    }

    public enum Navigation
    {
        Audio,
        AutoStart,
        CallerID,
        ChatBubbles,
        ChatSettings,
        KeypadDefault,
        ReachMe,
        Sounds,
        Support,
    }

    /// <summary>
    /// Navigation broadcast message
    /// </summary>
    public class SettingsNavigateMessage
    {
        public Navigation NavigateTo { get; set; }
    }
}
