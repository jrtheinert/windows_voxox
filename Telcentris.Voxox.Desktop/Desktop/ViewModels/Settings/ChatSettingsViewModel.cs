﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;				//SettingsManager
using Desktop.ViewModels.Messages;	//ShowOrHideVoiceMailsMessage

using GalaSoft.MvvmLight;			//ViewModelBase, MessengerInstance, Set

namespace Desktop.ViewModels.Settings
{
    public class ChatSettingsViewModel : ViewModelBase
    {
        #region | Private Class Variables |

        private bool isVoicemailsEnabled;
        private bool isFaxesEnabled;
        private bool isRecordedcallsEnabled;

        #endregion

        #region | Constructor |

        public ChatSettingsViewModel()
        {
            LoadProperties();
        }

        #endregion

        #region | Public Properties |

        public bool IsVoicemailsEnabled
        {
            get { return isVoicemailsEnabled; }
            set
            {
                Set(ref this.isVoicemailsEnabled, value);
                SettingsManager.Instance.User.ShowVoicemailsInChatThread = value;
                MessengerInstance.Send<ShowOrHideVoiceMailsMessage>(new ShowOrHideVoiceMailsMessage());
            }
        }

        public bool IsFaxesEnabled
        {
            get { return isFaxesEnabled; }
            set
            {
                Set(ref this.isFaxesEnabled, value);
                SettingsManager.Instance.User.ShowFaxesInChatThread = value;
                MessengerInstance.Send<ShowOrHideVoiceMailsMessage>(new ShowOrHideVoiceMailsMessage());
            }
        }

        public bool IsRecordedcallsEnabled
        {
            get { return isRecordedcallsEnabled; }
            set
            {
                Set(ref this.isRecordedcallsEnabled, value);
                SettingsManager.Instance.User.ShowRecordedCallsInChatThread = value;
                MessengerInstance.Send<ShowOrHideVoiceMailsMessage>(new ShowOrHideVoiceMailsMessage());
            }
        }

        #endregion

        #region | Load Default Properties |

        public void LoadProperties()
        {
            isFaxesEnabled			= SettingsManager.Instance.User.ShowFaxesInChatThread;
            isRecordedcallsEnabled	= SettingsManager.Instance.User.ShowRecordedCallsInChatThread;
            isVoicemailsEnabled		= SettingsManager.Instance.User.ShowVoicemailsInChatThread;
        }

        #endregion
    }
}
