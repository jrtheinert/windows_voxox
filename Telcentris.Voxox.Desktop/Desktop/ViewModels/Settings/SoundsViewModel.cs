﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;		//SettingsManager

using GalaSoft.MvvmLight;	//ViewModelBase, Set

namespace Desktop.ViewModels.Settings
{
    public class SoundsViewModel : ViewModelBase
    {
        #region | Private class Variables |

		//NOTE: JRT - 2015.09.30
		//	There is no requirment for Outgoing CALL.  This was conflated with an earlier requirement
		//	for Outgoing CHAT.  Howeve, PM is not sure we need Outgoing CHAT either, so we
		//	are commenting Outgoing CALL code until this is resolved.
        private bool isIncomingChatEnabled;
        private bool isIncomingCallEnabled;
//        private bool isOutgoingCallEnabled;

        #endregion

        #region | Constructors |

        public SoundsViewModel()
        {
            LoadProperties();
        }

        #endregion

        #region | Public Properties & Methods |

        public bool IsIncomingChatEnabled
        {
            get { return isIncomingChatEnabled; }
            set
            {
                Set(ref this.isIncomingChatEnabled, value);
                SettingsManager.Instance.User.EnableSoundForIncomingChat = value;
            }
        }

        public bool IsIncomingCallEnabled
        {
            get { return isIncomingCallEnabled; }
            set
            {
                Set(ref this.isIncomingCallEnabled, value);
                SettingsManager.Instance.User.EnableSoundForIncomingCall = value;
            }
        }

		//public bool IsOutgoingCallEnabled
		//{
		//	get { return isOutgoingCallEnabled; }
		//	set
		//	{
		//		Set(ref this.isOutgoingCallEnabled, value);
		//		SettingsManager.Instance.User.EnableSoundForOutgoingCall = value;
		//	}
		//}

        public void LoadProperties()
        {
            isIncomingChatEnabled = SettingsManager.Instance.User.EnableSoundForIncomingChat;
            isIncomingCallEnabled = SettingsManager.Instance.User.EnableSoundForIncomingCall;
//            isOutgoingCallEnabled = SettingsManager.Instance.User.EnableSoundForOutgoingCall;
        }

        #endregion
    }
}
