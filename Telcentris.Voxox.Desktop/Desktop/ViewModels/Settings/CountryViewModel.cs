﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using GalaSoft.MvvmLight;	//ViewModelBase, Set

namespace Desktop.ViewModels.Settings
{
    public class CountryViewModel : ViewModelBase
    {
        #region | Private Class Variables |

        private string countryName;
        private string countryCode;

        #endregion

        #region | Public Properties |

        /// <summary>
        /// Country name
        /// </summary>
        public string CountryName
        {
            get
            {
                return countryName;
            }
            set { Set(ref countryName, value); }
        }

        /// <summary>
        /// Country telephone code
        /// </summary>
        public string CountryCode
        {
            get { return countryCode; }
            set { Set(ref countryCode, value); }
        }

        /// <summary>
        /// To get the Alphabet in list to seperate names startingletter
        /// </summary>
        public string AlphabetHeader
        {
            get
            {
                return string.IsNullOrWhiteSpace(countryName) ? "" : countryName.Trim().ToUpper().Substring(0, 1);
            }
        }

        /// <summary>
        /// Country code
        /// </summary>
        public string Abbreviation { get; set; }

        /// <summary>
        /// ImageSource  source for the country flag
        /// </summary>
        public string FlagFileName
        {
            get { return string.Format("/Branding/Flags/{0}.png", Abbreviation); }
        }

        #endregion
    }
}
