﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;					//SettingsManager
using Desktop.Utils;					//VXLogger
using Desktop.ViewModels.Dialer;		//DefaultCountryCodeChanged

using GalaSoft.MvvmLight;				//ViewModelBase, MessengerInstance, Set

using System;							//Uri, Exception
using System.Collections.Generic;		//List
using System.Collections.ObjectModel;	//ObservableCollection
using System.IO;						//Path (for CountryCode.xml.  TODO: Move to Model Layer)
using System.Linq;						//<query>
using System.Windows;					//Application
using System.Xml;						//XmlDocument (for CountryCode.xml.  TODO: Move to Model Layer)

namespace Desktop.ViewModels.Settings
{
    public class CountryListViewModel : ViewModelBase
    {
        #region | Private Class Variables |

        private VXLogger logger = VXLoggingManager.INSTANCE.GetLogger("CountryListViewModel");
        private List<CountryViewModel> allCountryCodes;
        private ObservableCollection<CountryViewModel> countryList;
        private CountryViewModel selectedCountry;
        private int selectedCountryIndex;
        private string searchText;

        #endregion

        #region|Constructors|

        public CountryListViewModel()
        {
            allCountryCodes = new List<CountryViewModel>();
            countryList = new ObservableCollection<CountryViewModel>();
            selectedCountry = new CountryViewModel();

            LoadCountryList();
            LoadDefaults();
        }

        #endregion

        #region |Public Properties|

        /// <summary>
        /// Country codes name and flags list
        /// </summary>
        public ObservableCollection<CountryViewModel> CountryList
        {
            get { return countryList; }
            set { Set(ref countryList, value); }
        }

        /// <summary>
        /// Selected country by the user
        /// </summary>
        public CountryViewModel SelectedCountry
        {
            get { return selectedCountry; }
            set { Set(ref selectedCountry, value); }
        }

        /// <summary>
        /// Selected country index
        /// </summary>
        public int SelectedCountryIndex
        {
            get { return selectedCountryIndex; }
            set
            {
                Set(ref selectedCountryIndex, value);
                OnSelectionChanged(value);
            }
        }

        /// <summary>
        /// Search text 
        /// </summary>
        public string SearchText
        {
            get { return searchText; }
            set
            {
                Set(ref searchText, value);
                LoadCountryList(value);
            }
        }

        #endregion

        #region|Private Procedures|

        /// <summary>
        /// Loads the countries list from the xml file
        /// </summary>
        private void LoadCountryList()
        {
            try
            {
                var uri = new Uri(Path.Combine("pack://application:,,,/", LocalizedString.CountryCodesFileName));
                var CountryViewModelsStream = Application.GetResourceStream(uri);
                var countries = new List<CountryViewModel>();

                XmlDocument doc = new XmlDocument();

                using (var stream = CountryViewModelsStream.Stream)
                {
                    doc.Load(stream);

                    using (XmlNodeList nodesList = doc.SelectNodes("CountryCodes/CountryCode"))
                    {
                        foreach (XmlNode node in nodesList)
                        {
                            countries.Add(new CountryViewModel()
                            {
                                CountryCode = node.ChildNodes[0].InnerText,
                                CountryName = node.ChildNodes[1].InnerText,
                                Abbreviation = node.ChildNodes[2].InnerText
                            });
                        }
                    }
                }

                allCountryCodes.Clear();

                foreach (var item in (from c in countries orderby c.CountryName select c).ToList())
                {
                    allCountryCodes.Add(item);
                }
            }
            catch (Exception ex)
            {
                logger.Error("LoadCountryViewModels", ex);
            }
        }

        /// <summary>
        /// Loads the default values in the list
        /// </summary>
        private void LoadDefaults()
        {
            if (countryList.Count < allCountryCodes.Count )
            {
                countryList.Clear();
                foreach (var item in allCountryCodes)
                {
                    countryList.Add(item);
                }
            }

            for (int ii = 0; ii < countryList.Count; ii++)
            {
                if (countryList[ii].Abbreviation == SettingsManager.Instance.User.CountryAbbreviation)
                {
                    selectedCountryIndex = ii;
                    SelectedCountry = countryList[ii];
                    break;
                }
            }
        }

        /// <summary>
        /// Sets the selected value
        /// </summary>
        /// <param name="value">Selected value</param>
        private void OnSelectionChanged(int value)
        {
            if (value != -1)
            {
                SelectedCountry = countryList.ElementAt(value);

                SettingsManager.Instance.User.CountryCode         = SelectedCountry.CountryCode;
                SettingsManager.Instance.User.CountryAbbreviation = SelectedCountry.Abbreviation;

				ActionManager.Instance.HandleDefaultCountryCodeChange();
            }
        }

        /// <summary>
        /// Loads the list according the search text
        /// </summary>
        /// <param name="text">search text</param>
        private void LoadCountryList(string text)
        {
            if (!string.IsNullOrWhiteSpace(text))
            {
                CountryList.Clear();

                var filteredList = (from c in allCountryCodes where (text == "" || c.CountryName.ToLower().Contains(text.ToLower()) == true || 
                                        c.CountryCode.ToLower().Contains(text.ToLower()) == true) select c).ToList();

                foreach (var country in filteredList)
                {
                    CountryList.Add(country);
                }
            }
            else
            {
                LoadDefaults();
            }
        }

        #endregion
    }
}
