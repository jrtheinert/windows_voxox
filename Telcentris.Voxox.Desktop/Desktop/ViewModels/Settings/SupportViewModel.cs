﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;				//ActionManager
using Desktop.Util;					//PhoneUtils
using Desktop.Utils;				//VXLogger

using GalaSoft.MvvmLight;			 //ViewModelBase, Set
using GalaSoft.MvvmLight.CommandWpf; //RelayCommand

using System;						//Exception
using System.Reflection;			//Assembly
using System.Windows.Input;			//ICommand

namespace Desktop.ViewModels.Settings
{
    public class SupportViewModel : ViewModelBase
    {
        #region | Private Class Variables |

        private string phoneNo;
        private string email;
        private string version;
        private string displayEmail;
        private static VXLogger logger = VXLoggingManager.INSTANCE.GetLogger("SupportViewModel");

        #endregion

        #region | Constructors |

        public SupportViewModel()
        {
            CallCommand = new RelayCommand( OnCall );
            LoadDefaults();
        }

        #endregion

        #region | Public Properties and Methods |

        /// <summary>
        /// Supooet phone number
        /// </summary>
        public string PhoneNo
        {
            get { return phoneNo; }
            set { Set(ref this.phoneNo, value); }
        }

        /// <summary>
        /// Support email 
        /// </summary>
        public string Email
        {
            get { return email; }
            set { Set(ref this.email, value); }
        }

        /// <summary>
        /// Current version
        /// </summary>
        public string Version
        {
            get { return version; }
            set { Set(ref this.version, value); }
        }

        public string DisplayEmail
        {
            get { return displayEmail; }
            set { Set(ref this.displayEmail, value); }
        }

        public ICommand CallCommand { get; set; }

        #endregion

        #region | Private Methods |

        private void LoadDefaults()
        {
            var appVer = Assembly.GetExecutingAssembly().GetName().Version;
            version = appVer.Major + "." + appVer.Minor + "." + appVer.Build + "(" + appVer.Revision + ")";

            Email   = Desktop.Config.Support.Settings_Email;
            PhoneNo = Desktop.Config.Support.Settings_PhoneNo;

            // mailto: is syntax for wpf to open user's default email client
            //So we are adding mailto: user@domain.com
            DisplayEmail = "mailto:" + Email;
        }

        private void OnCall()
        {
            try
            {
                string number = Config.UI.SupportPhone_CountryCode + phoneNo;

                if (PhoneUtils.IsValidNumber(number))
                {
					String	faxNumber = PhoneUtils.GetPhoneNumberForFax( PhoneNo );
                    var		error	  = ActionManager.Instance.MakeCall( faxNumber );
                }
            }
			catch(Exception ex)
            {
                logger.Warn( "Failed to place call to " + phoneNo + ex);
            }
        }

        #endregion
    }
}
