﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;				//ActionManager, SessionManager
using Desktop.Utils;				//VXLogger

using Vxapi23;						//RestfulAPIManager

using GalaSoft.MvvmLight;				//ViewModelBase, MessengerInstance, Set
using GalaSoft.MvvmLight.CommandWpf;	//RelayCommand
using GalaSoft.MvvmLight.Threading;		//DispatcherHelper

using System;							//String, Exception
using System.Collections.ObjectModel;	//ObservableCollection
using System.Threading.Tasks;			//Task
using System.Windows.Input;				//ICommand

namespace Desktop.ViewModels.Profile
{
    public class ProfileViewModel : ViewModelBase
    {
		public class NumberInfo
		{
			public NumberInfo( String number, String label )
			{
				Number = number;
				Label  = label;
			}

			public String Number	{ get; set;	}
			public String Label		{ get; set; }
		}

        #region | Private Class Variables |

        private VXLogger logger = VXLoggingManager.INSTANCE.GetLogger("ProfileViewModel");

        private bool	isFirstNameValidated = true; 
        private bool	isLastNameValidated	 = true; 
        private bool	isEmailValidated	 = true;

		public ObservableCollection<NumberInfo> OtherNumbers { get; private set; }
        
        #endregion

        #region | Constructors |

        public ProfileViewModel()
        {
            ChooseAvatarCommand		= new RelayCommand( OnChooseAvatarCommand );
            SaveCommand				= new RelayCommand( OnSaveProfile		  );
            ProfileCloseCommand		= new RelayCommand( OnProfileClose		  );
            ChangePasswordCommand	= new RelayCommand( OnChangePassword	  );
			CallCommand				= new RelayCommand<string>( OnCall, (string value) => { return true; });


			MessengerInstance.Register<AvatarChooseMessage>(this, (AvatarChooseMessage msg) => { AvatarUri = msg.AvatarUri; });

			OtherNumbers = new ObservableCollection<NumberInfo>();

			LoadData();
        }

        #region | Private Procedures |

		private void LoadData()
		{
            AvatarUri	 = SessionManager.Instance.User.AvatarUri;
			FirstName	 = SessionManager.Instance.User.FirstName;
			LastName	 = SessionManager.Instance.User.LastName;
			Email		 = SessionManager.Instance.User.Email;
			CompanyName	 = SessionManager.Instance.User.CompanyName;
			Extension    = SessionManager.Instance.User.Extension;

			Name		 = string.Format("{0} {1}", FirstName, LastName).Trim();
			PhoneNumber  = Util.PhoneUtils.FormatPhoneNumber( SessionManager.Instance.User.Did );
			IsCloudPhone = !String.IsNullOrEmpty( Extension );

			//Main number (PhoneNumber) *may* also be in OtherNumbers, so let's avoid duplicating it.
			OtherNumbers.Clear();

			var cids = SessionManager.Instance.User.CallerIDs;

			if ( cids != null )
			{ 
				String mainNumber = Util.PhoneUtils.FormatPhoneNumber( PhoneNumber );

				foreach( Desktop.Model.User.CallerID cid in cids )
				{
					String formattedNumber	= Util.PhoneUtils.FormatPhoneNumber( cid.Number );
					bool   add				= true;

					if ( !IsCloudPhone )
					{
						add = (formattedNumber != mainNumber);
					}

					if ( add )
					{
						OtherNumbers.Add( new NumberInfo ( formattedNumber, cid.Name ) );
					}
				}
			}
		}

		private bool isDataValid()
		{
			bool result = false;

			IsFirstNameValidated = !String.IsNullOrEmpty( FirstName );
			IsLastNameValidated  = !String.IsNullOrEmpty( LastName  );
			IsEmailValidated     = !String.IsNullOrEmpty( Email     );

            result = IsFirstNameValidated && IsLastNameValidated && IsEmailValidated;

			return result;
		}

        #endregion

        private void OnChangePassword()
        {
            ActionManager.Instance.ShowManageAccountUrl();
        }

        private void OnChooseAvatarCommand()
        {
            ActionManager.Instance.ChooseAvatar();
        }

        private void OnProfileClose()
        {
			ActionManager.Instance.CloseProfile();
        }

		private void OnCall( String number )
		{
			ActionManager.Instance.MakeCall( number );
		}

        private void OnSaveProfile()
        {
			if ( isDataValid() )
            {
                try
                {
                    Task.Run( async() =>
                    {
                        bool isUpdated = await RestfulAPIManager.INSTANCE.UpdateCompanyUserProfile( SessionManager.Instance.User.UserKey,
																									SessionManager.Instance.User.CompanyUserId,
																									FirstName, LastName, Email);

                        if (isUpdated)
                        {
                            // Update Current session
                            SessionManager.Instance.User.FirstName = FirstName;
                            SessionManager.Instance.User.LastName  = LastName;
                            SessionManager.Instance.User.Email     = Email;
                        }
                        else
                        {
							logger.Warn( "UpdateCompanyUserProfile failed." );
                        }

						//Update local properties/data no matter what. 
						//Must be on UI thread, since we created Observeable collection on that thread.
						DispatcherHelper.CheckBeginInvokeOnUI( () => { LoadData(); } );	
                    });
                }
				catch(Exception e)
                {
                    logger.Error("Failed to update profile " + e);
                }
            }
        }
       
        #endregion

        #region | Public Properties |

        public ICommand ChangePasswordCommand	{ get; set; }
        public ICommand ChooseAvatarCommand		{ get; set; }
        public ICommand ProfileCloseCommand		{ get; set; }
        public ICommand SaveCommand				{ get; set; }
        public ICommand CallCommand				{ get; set; }

        public string Name			{ get; set; }
        public string FirstName		{ get; set; }
        public string LastName		{ get; set; }
        public string CompanyName	{ get; set; }
        public string Extension		{ get; set; }
        public string Email			{ get; set; }
        public string AvatarUri		{ get; set; }
		public string PhoneNumber	{ get; set; }
        public bool   IsCloudPhone	{ get; set; }

        public bool   HasOtherNumbers	
		{ 
			get { return (OtherNumbers.Count > 0 ); }
		}

		//These need the Set Ref code because we change color in XAML if edit is invalid.
        public bool IsFirstNameValidated
		{
			get { return isFirstNameValidated; }
			set { Set(ref isFirstNameValidated, value); }
		}

        public bool IsLastNameValidated
		{
			get { return isLastNameValidated; }
			set { Set(ref isLastNameValidated, value); }
		}

        public bool IsEmailValidated
		{
			get { return isEmailValidated; }
			set { Set(ref isEmailValidated, value); }
		}

		//These are 'extended' properties that depend on value of IsCloudPhone.
		//	Within .xaml file Binding set Mode=OneWay
		public string MainNumber
		{
			get { return (IsCloudPhone ? Extension : PhoneNumber); }
		}

		public string MainNumberDescription
		{
			get { return (IsCloudPhone ? LocalizedString.GenericYourExtension : LocalizedString.GenericBrandedNumber); }
		}

		public string OtherNumbersDescription
		{
			get { return (IsCloudPhone ? LocalizedString.GenericCompanyNumbers : LocalizedString.GenericOtherNumbers); }
		}

        #endregion
    }
}
