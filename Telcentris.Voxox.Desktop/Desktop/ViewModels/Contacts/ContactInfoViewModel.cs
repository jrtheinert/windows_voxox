﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Aug 2015
 */

using Desktop.Model.Contact;			//ContactManager
using Desktop.Util;						//PhoneUtils

using ManagedDataTypes;					//PhoneNumberList

using GalaSoft.MvvmLight;				//ViewModelBase, MessengerInstance, Set
using GalaSoft.MvvmLight.CommandWpf;	//RelayCommand

using System.Collections.ObjectModel;	//ObservableCollection
using System.Windows.Input;				//ICommand

namespace Desktop.ViewModels.Contacts
{
    public class ContactInfoViewModel : ViewModelBase
    {
		private	int		mCmGroup			  = Contact.INVALID_CMGROUP;
		private string	mCurrentPhoneNumber   = "";
		private int		mSelectedIndex		  = -1;
		private bool	mInitialized		  = false;
        private string  mAnimationReferenceID = System.Guid.NewGuid().ToString();

		public ObservableCollection<ContactInfoDetailViewModel> ContactNumbers { get; set; }

		public ICommand ContactInfoCloseCommand { get; set; }

		//For XAML
        public ContactInfoViewModel()
        {
		}

        public ContactInfoViewModel( int cmGroupIn, string currentPhoneNumber )
        {
			mCmGroup			= cmGroupIn;
			mCurrentPhoneNumber = currentPhoneNumber;

            ContactInfoCloseCommand = new RelayCommand(OnClose);

			LoadData();

			mInitialized = true;
        }

        // For Animation
        public string AnimationReferenceID
        {
            get { return mAnimationReferenceID; }
        }

		public int SelectedIndex
		{
			get { return mSelectedIndex; }
            set 
			{ 
				if ( mSelectedIndex != value )
				{ 
					Set(ref mSelectedIndex, value); 

					if ( ContactNumbers != null && mSelectedIndex > -1 && ContactNumbers.Count > 0 && mSelectedIndex < ContactNumbers.Count)
					{
	                    var item = ContactNumbers[mSelectedIndex];
						MessengerInstance.Send<ChangeContactMessageNumberMessage>(new ChangeContactMessageNumberMessage { PhoneNumber = item.DisplayNumber });
					}
				}

				//For some reason, framework is setting SelectedIndex to -1 when widget closes, triggering another message.
				//	Let's avoid that.
				if ( mInitialized && mSelectedIndex >= 0 )	//Avoid sending messeage on startup.  It complicates debugging.
				{ 
					OnClose();								//Close widget immediately after making change.
				}
			}
		}

		private void OnClose()
        {
            MessengerInstance.Send<ShowContactInfoMessage>(new ShowContactInfoMessage { Show = false } );
        }

		private void LoadData()
		{
			SelectedIndex = -1;

			var				contactNumbers	= new System.Collections.ObjectModel.ObservableCollection<ContactInfoDetailViewModel>();
			PhoneNumberList numbers			= ContactManager.getInstance().getContactDetailsPhoneNumbersByCmGroup( mCmGroup );

			if ( numbers.Count == 0 )
			{
				var newNum = new ContactInfoDetailViewModel();

				newNum.DisplayName	   = mCurrentPhoneNumber;
				newNum.DisplayNumber   = PhoneUtils.FormatPhoneNumber(mCurrentPhoneNumber);
				newNum.Number		   = mCurrentPhoneNumber;
				newNum.Label		   = PhoneUtils.BestLabel( null, PhoneNumber.PnType.NOT_VOXOX_RELATED );
				newNum.IsCurrentNumber = true;

				contactNumbers.Add(newNum);

				SelectedIndex = 0;
			}
			else 
			{
				int x = 0;
				string cleanCurrentNumber = PhoneUtils.CleanPhoneNumberFormatting( mCurrentPhoneNumber );

				foreach (PhoneNumber number in numbers)
				{
					var newNum = new ContactInfoDetailViewModel();


					newNum.DisplayName	   = number.DisplayName;
					newNum.DisplayNumber   = PhoneUtils.FormatPhoneNumber(number.DisplayNumber);
					newNum.Number		   = number.Number;
					newNum.Label		   = PhoneUtils.BestLabel( number );
					newNum.IsCurrentNumber = (cleanCurrentNumber == number.DisplayNumber) || (cleanCurrentNumber == number.Number);

					contactNumbers.Add(newNum);

					if ( newNum.IsCurrentNumber )
						SelectedIndex = x;

					x++;
				}
			}
		
			ContactNumbers = contactNumbers;
		}
	}

    public class ContactInfoDetailViewModel
    {
        #region Public Properties

		public string DisplayNumber		{ get; set; }
		public string DisplayName		{ get; set; }
		public string Number			{ get; set; }
		public string Label				{ get; set; }
		public bool   IsCurrentNumber	{ get; set; }

		#endregion
    }
}
