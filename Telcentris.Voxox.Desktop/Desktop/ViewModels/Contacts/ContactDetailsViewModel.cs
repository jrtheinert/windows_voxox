﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;					//AvatarManager
using Desktop.Model.Contact;			//ContactManager
using Desktop.Util;						//PhoneUtils
using Desktop.ViewModels.Calls;			//CallHistoryMessage, CallHistoryViewModel

using ManagedDataTypes;					//PhoneNumberList
using ManagedDbManager;					//DbChangedEventHandler, DbChangedEventArgs

using GalaSoft.MvvmLight;				//ViewModelBase, MessengerInstance, Set
using GalaSoft.MvvmLight.CommandWpf;	//RelayCommand
using GalaSoft.MvvmLight.Threading;		//DispatcherHelper

using System.Collections.ObjectModel;	//ObservableCollection
using System.Windows.Input;				//ICommand

namespace Desktop.ViewModels.Contacts
{
    public class ContactDetailsViewModel : ViewModelBase
    {
        #region | Private Class Variables |

		private string mContactKey;
		private string mPhoneNumber;
		private bool   mShowCallHistory;

        private ViewModelBase		  mCallHistoryPane;
        private DbChangedEventHandler dbChangeHandler = null;

        #endregion

        #region | Constructors |

		//JRT - 2015.10.19 - We NEED contactKey here, not CmGroup.
		//		CmGroup is a close approximation, but does NOT return proper 
		//		phone number list in event a single number is shared by two or more contacts.
        public ContactDetailsViewModel( string contactKeyIn, int cmGroupIn )
        {
			mContactKey		= contactKeyIn;
            CmGroup			= cmGroupIn;
            ShowCallHistory	= false;

            MessengerInstance.Register<CallHistoryMessage>(this, CloseCallHistory);
     
			dbChangeHandler = new DbChangedEventHandler(OnDbChange);
            getUserDbm().addDbChangeListener(dbChangeHandler);

            ContactNumbers		= new ObservableCollection<ContactNumberViewModel>();
            CallHistoryCommand	= new RelayCommand(OnCallHistory, () => { return true; });

            LoadData();
        }

        public override void Cleanup()
        {
            base.Cleanup();

            if ( getUserDbm() != null && dbChangeHandler != null)
            {
                getUserDbm().removeDbChangeListener( dbChangeHandler );
                dbChangeHandler = null;
            }
        }

		private ManagedDbManager.ManagedDbManager getUserDbm()
		{
			return SessionManager.Instance.UserDataStore;
		}

        private void OnDbChange(object sender, DbChangedEventArgs e)
        {
            if ( e.Data.isContactTable() )
            {
                DispatcherHelper.CheckBeginInvokeOnUI( () => { LoadData(); } );
            }
        }

        #endregion

        #region | Public Properties |

        public int    CmGroup				{ get; set; }		//Needed by ContactsMainViewModel
        public string Name					{ get; set; }
        public string LastSeen				{ get; set; }
        public string AvatarUri				{ get; set; }
        public string InNetworkImageUri		{ get; set; }
        public bool   IsRegisteredUser		{ get; set; }

        public bool ShowCallHistory
        {
            get { return mShowCallHistory; }
            set { Set(ref mShowCallHistory, value); }
        }

		public ViewModelBase CallHistoryPane
		{
			get { return mCallHistoryPane; }
			set { Set(ref mCallHistoryPane, value); }
		}

        public ObservableCollection<ContactNumberViewModel> ContactNumbers { get; set; }

        public ICommand CallHistoryCommand { get; set; }

        #endregion

        #region | Private Procedures |

        private void CloseCallHistory(CallHistoryMessage msg)
        {
            if (msg != null)
            {
                ShowCallHistory = false;
            }
        }

        private void OnCallHistory()
        {
            ShowCallHistory = true;
            CallHistoryPane = new CallHistoryViewModel( mPhoneNumber, CmGroup );
        }

        private void LoadData()
        {
            PhoneNumberList numbers		   = ContactManager.getInstance().getContactDetailsPhoneNumbersByKey( mContactKey );
			var				contactNumbers = ContactManager.PhoneNumberListToContactNumberViewModel( numbers );

            //NOTE: name, lastseen, etc. are the same for all these numbers, so get that data from the first entry.
            int count	   = 0;
            int voxoxCount = numbers.getVoxoxCount();

            foreach (PhoneNumber number in numbers)
            {
                if (count == 0)
                {
					bool nameIsPhoneNumber = false;
						
					Name			= number.bestDisplayName( out nameIsPhoneNumber );
//					LastSeen		= "Online Yesterday";                    //TODO: Need to get status
                    AvatarUri		= AvatarManager.Instance.GetUriForContact        ( number.ContactKey );
                    mPhoneNumber	= PhoneUtils.FormatPhoneNumber( number.Number );

                    count++;
                }

				break;
            }

			InNetworkImageUri = Config.UI.InNetworkContact;
            IsRegisteredUser  = (voxoxCount > 0);
            ContactNumbers	  = contactNumbers;
        }

        #endregion
    }
}
