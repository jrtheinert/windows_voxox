﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Dec 2015
 */

using Desktop.Config;					//LoginMode
using Desktop.Model;

using Vxapi23.Model;					//ContactSourceResponse

using GalaSoft.MvvmLight;				//ViewModelBase

using System;							//String
using System.Collections.ObjectModel;	//ObservableCollection

namespace Desktop.ViewModels.Settings
{
    public class ContactSourceSelectionViewModel : ViewModelBase
    {
        public ObservableCollection<ContactSourcesResponse.Source> Sources	{ get; set; }

        public int	  SelectedIndex		{ get; set; }
        public String DefaultSourceKey	{ get; set; }

        public ContactSourceSelectionViewModel( ContactSourcesResponse response, String defaultSourceKey )
        {
			DefaultSourceKey = defaultSourceKey;
			Sources = new ObservableCollection<ContactSourcesResponse.Source>();
			LoadData( response );
        }

		private void LoadData( ContactSourcesResponse response )
		{
			Sources.Clear();

			foreach ( ContactSourcesResponse.Source src in response.sources )
			{
				Sources.Add( src );
			}
		}

		public Boolean Save( Boolean useDefault )
		{
			Boolean result = false;

			if ( useDefault )
			{
				result = SaveWithSourceKey( DefaultSourceKey );
			}
			else 
			{ 
				if ( SelectedIndex >= 0 && SelectedIndex < Sources.Count )
				{
					result = SaveWithSourceKey( Sources[SelectedIndex].SourceKey );
				}
			}

			return result;
		}

		private Boolean SaveWithSourceKey( String sourceKey )
		{
			ActionManager.Instance.SelectContactSource( sourceKey );

			return true;
		}
    }
}
