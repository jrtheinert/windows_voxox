﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;					//AvatarManager, SessionManager
using Desktop.Model.Contact;			//ContactManager
using Desktop.Util;						//PhoneUtils, VxObservableCollection
using Desktop.Utils;					//VXProfiler

using ManagedDataTypes;					//PhoneNumber, Contact, ContactList
using ManagedDbManager;					//DbChangedEventHandler, DbChangedEventArgs

using GalaSoft.MvvmLight;				//ViewModelBase, MessengerInstance, Set
using GalaSoft.MvvmLight.CommandWpf;	//RelayCommand
using GalaSoft.MvvmLight.Threading;		//DispatcherHelper

using System.Collections.Generic;		//List
using System.Linq;						//<query>

using System.Windows.Threading;			//Time rendering

//TODO: Most of this code is duplicated following classes and should be refactored.:
//	- ContactListViewModel
//	- ComposeFaxViewModel
//	- ComposeMessageViewModel

namespace Desktop.ViewModels.Contacts
{
    /// <summary>
    /// The class is the backing model for Contacts pane.
    /// This will call/consume the PAPI methods & objects (like Contact, ContactList etc)
    /// </summary>
    public class ContactListViewModel : ViewModelBase
    {
        #region | Private Class Variables |

        private ContactFilter	filterContactsBy;
        private string			searchContacts;
        private bool			noSearchResultsAvailable;
        private int				selectedIndex = -1;
		private bool			mLoading = false;

        private DbChangedEventHandler dbChangeHandler = null;
       
		private VXProfiler renderingProfiler;

        #endregion

        #region | Constructors |

        public ContactListViewModel()
        {
            Contacts = new VxObservableCollection<ContactViewModel>();

            LoadContacts();

            if (Contacts.Count > 0)
            {
                SelectedIndex = 0;
            }

            dbChangeHandler = new DbChangedEventHandler(OnDbChange);
            getUserDbm().addDbChangeListener(dbChangeHandler);
        }

        public override void Cleanup()
        {
            base.Cleanup();

            if ( getUserDbm() != null && dbChangeHandler != null)
            {
                getUserDbm().removeDbChangeListener( dbChangeHandler );
                dbChangeHandler = null;
            }
        }

		private ManagedDbManager.ManagedDbManager getUserDbm()
		{
			return SessionManager.Instance.UserDataStore;
		}

        #endregion

        #region | Public Properties |

        public VxObservableCollection<ContactViewModel> Contacts { get; set; }

        /// <summary>
        /// Filter contacts based on ContactFilterType setting
        /// </summary>
        public ContactFilter FilterContactsBy
        {
            get { return filterContactsBy; }
            set
            {
                Set(ref filterContactsBy, value);

                LoadContacts();
            }
        }

        /// <summary>
        /// Search contacts
        /// </summary>
        public string SearchContacts
        {
            get { return searchContacts; }
            set
            {
                Set(ref searchContacts, value);

                LoadContacts();
            }
        }

        public bool NoSearchResultsAvailable
        {
            get { return noSearchResultsAvailable; }
            set { Set(ref noSearchResultsAvailable, value); }
        }

        public int SelectedIndex
        {
            get { return selectedIndex; }
            set
            {
				if ( mLoading )
					return;

                Set(ref selectedIndex, value);

				int    cmGroup	  = Contact.INVALID_CMGROUP;
				string contactKey = "";

				if (Contacts.Count > 0 && selectedIndex >= 0 && selectedIndex < Contacts.Count)
                {
                    cmGroup    = Contacts[selectedIndex].CmGroup;
                    contactKey = Contacts[selectedIndex].ContactKey;
                }
                    
				MessengerInstance.Send<ShowContactDetailMessage>(new ShowContactDetailMessage { ContactKey = contactKey, CmGroup = cmGroup });
            }
        }

        #endregion

        #region | Private Procedures |

        /// <summary>
        /// Load Contacts
        /// </summary>
        private void LoadContacts()
        {
			//Below, we do Contacts.Clear().  This forces SelectedIndex to be -1 which sends a message
			//	to display the top-most contact.
			//	To avoid that, we use 'mLoading', so SelectedIndex.set, can skip the message being sent
			//	We also must set 'selectedIndex' (the memvar, not the property) to -1, so that when
			//	we reload Contacts, we can set the proper SelectedIndex value and have it sense
			//	the 'change' to display the proper selected item in ListBox.
			mLoading = true;
			int saveIndex = SelectedIndex;
			selectedIndex = -1;

            VXProfiler prof = new VXProfiler("ContactList::LoadContacts");

            ContactViewModel cvm = null;
            List<ContactViewModel> list = new List<ContactViewModel>();

            ContactFilter filter = filterContactsBy;

            Contacts.Clear();	//This causes SelectedIndex to be set to -1.

            ContactList cl = ContactManager.getInstance().getContacts( searchContacts, filter);

			prof.LogElapsedTime( "after CM.getContacts()");

            foreach (Contact contact in cl)
            {
                cvm = fromContactModel(contact);

                list.Add(cvm);
            }

			prof.LogElapsedTime( "after create 'list'");

            Contacts.AddRange(list);            
	
			mLoading = false;

			updateResults( Contacts.Count, saveIndex );

			prof.LogElapsedTime( "after AddRange()");
            prof.Stop();

			renderingProfiler = new VXProfiler( "ContactList::LoadContacts - Rendering" );

			System.Windows.Application.Current.Dispatcher.BeginInvoke( new System.Action(RenderingDone), System.Windows.Threading.DispatcherPriority.ContextIdle, null );
        }

		private void RenderingDone()
		{
			renderingProfiler.Stop();
		}
 		
		private void updateResults( int count, int saveIndex )
		{
           // check contacts exist or not
            // If exists then set Selected index as 0 otherwise set it as -1
            if ( count == 0)
            {
                NoSearchResultsAvailable = true;
                SelectedIndex = -1;
            }
            else
            {
				//Validate 'saveIndex'.
				//	This should happen *only* if user deletes contacts on another device, and we get a verfy different ContactList
				//	Otherwise, this preserves user-selected Contact index for actions such as 'Favorite/Unfavorite' or 'Block/Unblock'.
				//NOTE: For some unknown reason, this next line causes ContactList to flash.  It does NOT flash without this line.
				//	True in Debug and Release builds.  ODD!  And now it does not!
//				System.Diagnostics.Debug.WriteLine( "BEFORE - saveIndex: " + System.Convert.ToString( saveIndex ) + ", count: " + System.Convert.ToString( count ) );
				saveIndex = (saveIndex >= count ? 0 : saveIndex);
//				System.Diagnostics.Debug.WriteLine( "AFTER  - saveIndex: " + System.Convert.ToString( saveIndex ) + ", count: " + System.Convert.ToString( count ) );

                NoSearchResultsAvailable = false;
                SelectedIndex = saveIndex;
            }
		}

        //Convert Contact Model to ContactViewModel
        private ContactViewModel fromContactModel(Contact contact)
        {
			bool			 nameIsPhoneNumber = false;
            ContactViewModel cvm			   = new ContactViewModel();
            
            cvm.CmGroup				= contact.CmGroup;
            cvm.NameOrNumber		= contact.bestDisplayName( out nameIsPhoneNumber );
            cvm.IsRegisteredUser	= (contact.VoxoxCount > 0);
			cvm.InNetworkImageUri	= Config.UI.InNetworkContact;
            cvm.PhoneNumber			= PhoneUtils.FormatPhoneNumber(contact.PhoneNumber);
			cvm.ContactKey			= contact.ContactKey;
			cvm.Extension			= contact.Extension;

            cvm.AvatarUri			= AvatarManager.Instance.GetUriForContact( contact.ContactKey );
            cvm.LastSeen			= (contact.VoxoxCount > 0) ? "Last online on May 15" : "";            //TODO: We do not get this data from DB yet.

			//This formating call is expensive on non-numbers, so let's not waste time where not needed.
			if ( nameIsPhoneNumber )
			{ 
				cvm.NameOrNumber = PhoneUtils.FormatPhoneNumber( cvm.NameOrNumber );
			}

            return cvm;
        }

        private void OnDbChange(object sender, DbChangedEventArgs e)
        {
            if (e.Data.isContactTable())
            {
                DispatcherHelper.CheckBeginInvokeOnUI(() => { LoadContacts(); });
            }
        }

        #endregion
	}

    /// <summary>
    /// This message is raised when a contact is selected to show contact detail
    /// </summary>
    public class ShowContactDetailMessage
    {
		public string ContactKey	{ get; set; }
        public int	  CmGroup		{ get; set; }
    }
}
