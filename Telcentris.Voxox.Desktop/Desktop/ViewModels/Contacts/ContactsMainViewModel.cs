﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using ManagedDataTypes;				//Contact

using GalaSoft.MvvmLight;			//ViewModelBase, MessengerInstance, Set

namespace Desktop.ViewModels.Contacts
{
    /// <summary>
    /// Contacts Main View Model that wraps contact listing & details pane
    /// </summary>
    public class ContactsMainViewModel : ViewModelBase
    {
        private ContactListViewModel contactListViewModel;
        private ContactDetailsViewModel contactDetailViewModel;

        public ContactsMainViewModel()
        {
            MessengerInstance.Register<ShowContactDetailMessage>(this, ShowContactDetail);

            contactListViewModel = new ContactListViewModel();
        }

        public override void Cleanup()
        {
            base.Cleanup();

            MessengerInstance.Unregister<ShowContactDetailMessage>(this, ShowContactDetail);

            if(ContactList != null)
            {
                ContactList.Cleanup();
                ContactList = null;
            }

            if (ContactDetail != null)
            {
                ContactDetail.Cleanup();
                ContactDetail = null;
            }
        }

        public ContactListViewModel ContactList
        {
            get { return contactListViewModel; }
            set { Set(ref contactListViewModel, value); }
        }

        public ContactDetailsViewModel ContactDetail
        {
            get { return contactDetailViewModel; }
            set { Set(ref contactDetailViewModel, value); }
        }

        public void ShowContactDetail( ShowContactDetailMessage msg )
        {
            if ( msg.CmGroup == Contact.INVALID_CMGROUP )
            {
                ContactDetail = null;
            }
            else if (ContactDetail != null && ContactDetail.CmGroup != msg.CmGroup)
            {
                var existing = contactDetailViewModel;
                ContactDetail = new ContactDetailsViewModel( msg.ContactKey, msg.CmGroup );

                existing.Cleanup();
                existing = null;
            }
            else if (ContactDetail == null)
            {
                ContactDetail = new ContactDetailsViewModel( msg.ContactKey, msg.CmGroup );
            }
        }
    }
}
