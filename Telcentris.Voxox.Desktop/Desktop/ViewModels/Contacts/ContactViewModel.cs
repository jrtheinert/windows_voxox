﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using GalaSoft.MvvmLight;		//ViewModelBase, Set

using System;

//NOTE: This is base class for:
//	- Calling/AddToCallContactViewModel which is where we use PhoneNumber
//	- 
namespace Desktop.ViewModels.Contacts
{
    /// <summary>
    /// This class provides display properties & actions for each "Contact" item in the Contacts list.
    /// Setting of the properties is taken care from the listing model
    /// </summary>
    public class ContactViewModel : ViewModelBase
    {
        #region | Private Class Variables |

        private int cmGroup;
        private string phoneNumber;
		private string extension;		//For CloudPhone
		private string contactKey;
        private string nameOrNumber;
        private string lastSeen;
        private string avatarUri;
		private string inNetworkImageUri;
        private bool isRegisteredUser;
		private bool isInCompany;

        #endregion

        #region | Public Properties |

        /// <summary>
        /// CmGroup
        /// </summary>
        public int CmGroup
        {
            get { return cmGroup; }
            set { Set(ref cmGroup, value); }
        }

        /// <summary>
        /// Contact's Display Name or Phone Number
        /// </summary>
        public string NameOrNumber
        {
            get { return nameOrNumber; }
            set { Set(ref nameOrNumber, value); }
        }

        /// <summary>
        /// Text that indicates the last time the contact used voxox
        /// e.g., "Online Today", "Online Yesterday", "Online May 26 2013"
        /// </summary>
        public string LastSeen
        {
            get { return lastSeen; }
            set { Set(ref lastSeen, value); }
        }

        /// <summary>
        /// Avatar Uri of the Contact
        /// </summary>
        public string AvatarUri
        {
            get { return avatarUri; }
            set { Set(ref avatarUri, value); }
        }

        /// <summary>
        /// Is registered User, to show the appropriate badge
        /// </summary>
        public bool IsRegisteredUser
        {
            get { return isRegisteredUser; }
            set { Set(ref isRegisteredUser, value); }
        }

        /// <summary>
        /// In Network image
        /// </summary>
        public string InNetworkImageUri
        {
            get { return inNetworkImageUri; }
            set { Set(ref this.inNetworkImageUri, value); }
        }

        /// <summary>
        /// Primary Phone Number - TODO: PhoneNumber is not at Contact-level, so I am suspicious of its use here.  Sadly, removing it is not so simple.
		/// See NOTE at top of this file.
        /// </summary>
		public string PhoneNumber
		{
			get { return phoneNumber; }
			set { Set(ref phoneNumber, value); }
		}

        /// <summary>
        /// ContactKey
        /// </summary>
		public string ContactKey
		{
			get { return contactKey; }
			set { Set(ref contactKey, value); }
		}

		public string Extension
		{
			get { return extension; }
			set { 
					Set(ref extension, value); 
					IsInCompany = !String.IsNullOrEmpty( extension );
				}
		}

		public string ExtensionEx
		{
			get { return LocalizedString.GenericExtensionAbbreviation + "  " + extension; }	
//			set { Set(ref contactKey, value); }
		}

		public bool IsInCompany
		{
			get { return isInCompany; }
			set { Set(ref isInCompany, value); }
		}

        /// <summary>
        /// Group under which this contact falls when shown in contacts list with alpha grouping.
        /// This is a UI helper property.
        /// The group name is the first character of the NameOrNumber property.
        /// If the first character is not an alphabet then the contact is grouped with "#"
        /// TODO: Can "#" be used as group name for unknown contacts or contacts without names?
        /// </summary>
        public string DisplayGroup
        {
            get
            {
                char startChar = string.IsNullOrWhiteSpace(NameOrNumber) ? '#' : NameOrNumber.Trim().ToUpper()[0];

                if (char.IsLetter(startChar))
                {
                    return startChar.ToString();
                }

                return "#";
            }
        }

        #endregion
    }
}
