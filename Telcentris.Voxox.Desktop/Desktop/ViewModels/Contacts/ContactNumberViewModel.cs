﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;					//ActionManager
using Desktop.Model.Contact;			//ContactManager
using Desktop.ViewModels.Main;			//MainWindowViewModel
using Desktop.ViewModels.Messages;		//ShowContactMessagesMessage, ComposeMessageViewModel

using GalaSoft.MvvmLight;				//ViewModelBase, MessengerInstance, Set
using GalaSoft.MvvmLight.CommandWpf;	//RelayCommand

using System.Windows.Input;				//ICommand

namespace Desktop.ViewModels.Contacts
{
    public class ContactNumberViewModel : ViewModelBase
    {
        #region | Private Class Variables |

        private string	contactKey;
        private string	number;
        private string	displayNumber;
        private string	numberType;
        private bool	isFavorite;
        private bool	isOnNet;
        private int		recordId;
        private int		cmGroup;

        #endregion

        #region | Constructors |

        public ContactNumberViewModel()
        {
            ToggleFavoriteCommand	= new RelayCommand<string>( OnToggleFavorite, CanToggleFavorite );
            CallCommand				= new RelayCommand<string>( OnCall,			  CanCall			);
            MessageCommand			= new RelayCommand<string>( OnMessage,		  CanMessage		);
        }

        #endregion

        #region | Public Properties |

		public int CmGroup
        {
            get { return cmGroup; }
            set { Set(ref cmGroup, value); }
        }

        public int RecordId
        {
            get { return recordId; }
            set { Set(ref recordId, value); }
        }

        public string Number
        {
            get { return number; }
            set { Set(ref number, value); }
        }

        public string ContactKey
        {
            get { return contactKey; }
            set { Set(ref contactKey, value); }
        }

        public string DisplayNumber
        {
            get { return displayNumber; }
            set { Set(ref displayNumber, value); }
        }

        public string NumberType
        {
            get { return numberType; }
            set { Set(ref numberType, value); }
        }

        public bool IsFavorite
        {
            get { return isFavorite; }
            set { Set(ref isFavorite, value); }
        }

		public bool IsOnNet
		{
			get { return isOnNet; }
			set { Set(ref isOnNet, value); }
		}

        public bool EnableMessaging
        {
            get 
			{ 
				bool result = ( IsOnNet ? true : Config.Settings.EnableSMS );
				return result; 
			}
        }

        public ICommand ToggleFavoriteCommand	{ get; set; }
        public ICommand CallCommand				{ get; set; }
        public ICommand MessageCommand			{ get; set; }

        #endregion

        #region | Private Procedures |

        /// <summary>
        /// Initiate call
        /// </summary>
        private void OnCall(string phoneNumber)
        {
			var error = ActionManager.Instance.MakeCall( phoneNumber );
        }

        /// <summary>
        /// Checks whether call can be initiated
        /// </summary>
        /// <returns>Returns true if call can be initiated</returns>
        private bool CanCall(string phoneNumber)
        {
            return true;
        }

        private void OnMessage(string value)
        {
			//'value' should equal 'number' property
			ActionManager.Instance.ShowContactMessages( cmGroup, number, contactKey, false );
        }

        /// <summary>
        /// Checks whether messaging can be initiated
        /// </summary>
        /// <returns>Returns true if messaging can be initiated</returns>
        private bool CanMessage(string phoneNumber)
        {
            return true;
        }

        private void OnToggleFavorite( string phoneNumber )
        {
            IsFavorite = !IsFavorite;
			ActionManager.Instance.FavoriteNumber( phoneNumber, isFavorite );
        }

        /// <summary>
        /// Checks whether messaging can be initiated
        /// </summary>
        /// <returns>Returns true if messaging can be initiated</returns>
        private bool CanToggleFavorite( string phoneNumber )
        {
            return true;
        }

        #endregion
    }
}
