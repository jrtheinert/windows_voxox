﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;		//ShowContactMessagesMessage

using ManagedDataTypes;		//Contact

using GalaSoft.MvvmLight;	//ViewModelBase, MessengerInstance, Set

namespace Desktop.ViewModels.Messages
{
    public class MessagesMainViewModel : ViewModelBase
    {
        private MessageListViewModel messageListViewModel;
        private MessagesViewModel messagesViewModel;
        private ComposeMessageViewModel composeMessageViewModel;
        private bool isComposing;

        public MessagesMainViewModel()
        {
            MessengerInstance.Register<ShowContactMessagesMessage>(this, ShowContactMessages);
            MessengerInstance.Register<MessageComposeMessage>(this, OnMessageCompose);
            MessengerInstance.Register<MessageSentMessage>(this, OnMessageSent);

            MessageList = new MessageListViewModel();
        }

        public override void Cleanup()
        {
            base.Cleanup();

            MessengerInstance.Unregister<ShowContactMessagesMessage>(this, ShowContactMessages);
            MessengerInstance.Unregister<MessageComposeMessage>(this, OnMessageCompose);
            MessengerInstance.Unregister<MessageSentMessage>(this, OnMessageSent);

            if (MessageList != null)
            {
                MessageList.Cleanup();
                MessageList = null;
            }

            if (Messages != null)
            {
                Messages.Cleanup();
                Messages = null;
            }

            IsComposing = false;
            if (ComposeMessage != null)
            {
                ComposeMessage.Cleanup();
                ComposeMessage = null;
            }
        }

        public MessageListViewModel MessageList
        {
            get { return messageListViewModel; }
            set { Set(ref messageListViewModel, value); }
        }

        public MessagesViewModel Messages
        {
            get { return messagesViewModel; }
            set { Set(ref messagesViewModel, value); }
        }

        public ComposeMessageViewModel ComposeMessage
        {
            get { return composeMessageViewModel; }
            set { Set(ref composeMessageViewModel, value); }
        }

        public bool IsComposing
        {
            get { return isComposing; }
            set { Set(ref isComposing, value); }
        }

        public void ShowContactMessages( ShowContactMessagesMessage msg )
        {
            if (Messages != null && (Messages.CmGroup != msg.CmGroup || messagesViewModel.PhoneNumber != msg.PhoneNumber))
            {
                var existing = Messages;
                existing.Cleanup();				//Remove DbChange handler.

                if (msg.CmGroup == Contact.INVALID_CMGROUP && string.IsNullOrWhiteSpace(msg.PhoneNumber))
                {
                    Messages = null;
                }
                else
                {
                    Messages = new MessagesViewModel( msg.ContactKey, msg.CmGroup, msg.PhoneNumber );
                }

                existing = null;
            }
            else if (Messages == null)
            {
                Messages = new MessagesViewModel( msg.ContactKey, msg.CmGroup, msg.PhoneNumber);
            }
        }

        private void OnMessageCompose(MessageComposeMessage msg)
        {
            if (msg.Show)
            {
                ComposeMessage = new ComposeMessageViewModel();
                IsComposing = true;
            }
            else
            {
                IsComposing = false;
                ComposeMessage = null;
            }
        }

        private void OnMessageSent(MessageSentMessage msg)
        {
            if (IsComposing)
            {
                IsComposing = false;
                ComposeMessage = null;
            }
        }
    }
}
