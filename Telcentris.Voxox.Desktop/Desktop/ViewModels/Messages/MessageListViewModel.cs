﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;					//AvatarManager, SessionManager, SettingsManager
using Desktop.Model.Contact;			//ContactManager
using Desktop.Util;						//VxObservableCollection, PhoneUtils, TimeUtils
using Desktop.Utils;					//VXLogger, VXProfiler

using ManagedDataTypes;					//Message, RichData, ContactMessageSummaryList
using ManagedDbManager;					//DbChangedEventHandler, DbChangedEventArgs

using GalaSoft.MvvmLight;				//ViewModelBase, MessengerInstance, Set
using GalaSoft.MvvmLight.CommandWpf;	//RelayCommand
using GalaSoft.MvvmLight.Threading;		//DispatcherHelper

using System;							//Exception
using System.Collections.Generic;		//List
using System.Linq;						//<query>
using System.Windows.Input;				//ICommand

namespace Desktop.ViewModels.Messages
{
    /// <summary>
    /// The class is the backing model for Messages (Middle) pane.
    /// This will call/consume the PAPI methods & objects
    /// </summary>
    public class MessageListViewModel : ViewModelBase
    {
        #region | Private Class Variables |

        private static VXLogger logger = VXLoggingManager.INSTANCE.GetLogger("MessageListViewModel");
        private string searchMessages;
        private bool noSearchResultsAvailable;
        private int selectedIndex = -1;
        private bool isInitPending = true;
        private bool selfSelectedIndexUpdate = false;
        private string formattedSearchMessages;

        private DbChangedEventHandler dbChangeHandler = null;

        #endregion

        #region | Constructors |

        public MessageListViewModel()
        {
            Messages = new VxObservableCollection<MessageItemViewModel>();
            ChooseContactCommand = new RelayCommand(OnChooseContact);

            LoadMessages();

            dbChangeHandler = new DbChangedEventHandler(OnDbChange);
            getUserDbm().addDbChangeListener(dbChangeHandler);

            // Catch the Compose Message event for the Contact/Number choosen from the Compose Message UI
            MessengerInstance.Register<ShowContactMessagesMessage>(this, ActionManager.COMPOSE_MESSAGE_TOKEN, OnShowContactMessages);

            // Reload on settings change
            MessengerInstance.Register<ShowOrHideVoiceMailsMessage>(this, OnShowOrHideVoiceMailsMessage);

            isInitPending = false;
        }

		private ManagedDbManager.ManagedDbManager getUserDbm()
		{
			return SessionManager.Instance.UserDataStore;
		}

        public override void Cleanup()
        {
            base.Cleanup();

            if ( getUserDbm() != null && dbChangeHandler != null)
            {
                getUserDbm().removeDbChangeListener(dbChangeHandler);
                dbChangeHandler = null;
            }

            MessengerInstance.Unregister<ShowContactMessagesMessage> ( this, ActionManager.COMPOSE_MESSAGE_TOKEN, OnShowContactMessages );
            MessengerInstance.Unregister<ShowOrHideVoiceMailsMessage>( this, OnShowOrHideVoiceMailsMessage );
        }

        #endregion

        #region | Public Properties |

        public VxObservableCollection<MessageItemViewModel> Messages { get; set; }

        /// <summary>
        /// Search messages
        /// </summary>
        public string SearchMessages
        {
            get { return searchMessages; }
            set
            {
                Set(ref searchMessages, value);

                LoadMessages();
            }
        }

        /// <summary>
        /// This is set to true when no messages are available
        /// </summary>
        public bool NoSearchResultsAvailable
        {
            get { return noSearchResultsAvailable; }
            set { Set(ref noSearchResultsAvailable, value); }
        }

        public int SelectedIndex
        {
            get { return selectedIndex; }
            set
            {
                Set(ref selectedIndex, value);

                if (selfSelectedIndexUpdate == false && selectedIndex > -1 && Messages.Count > 0 && selectedIndex < Messages.Count)
                {
                    var item = Messages[selectedIndex];
                    MessengerInstance.Send<ShowContactMessagesMessage>(new ShowContactMessagesMessage { CmGroup = item.CmGroup, PhoneNumber = item.PhoneNumber, ContactKey = item.ContactKey });
                }
            }
        }

        public ICommand ChooseContactCommand { get; set; }

        #endregion

        #region | Private Procedures |

        /// <summary>
        /// Load Messages
        /// </summary>
        private void LoadMessages()
        {
            VXProfiler prof = new VXProfiler("MessageList::LoadMessages");

            // Store the currently selected item so that the selection can be remembered after the list is re-loaded
            string backupSelectedItemKey = "";
            if (SelectedIndex > -1)
            {
                backupSelectedItemKey = Messages[SelectedIndex].UniqueId;
            }

            MessageItemViewModel cvm = null;
            List<MessageItemViewModel> list = new List<MessageItemViewModel>();

			//NOTE use of Ex properties from SessionManager.
            ContactMessageSummaryList summary = ContactManager.getInstance().getContactsMessageSummary( SettingsManager.Instance.User.ShowVoicemailsInChatThreadEx, 
																										SettingsManager.Instance.User.ShowRecordedCallsInChatThreadEx, 
																										SettingsManager.Instance.User.ShowFaxesInChatThreadEx );

            foreach (var msgSum in summary)
            {
				bool nameIsPhoneNumber = false;
				String name = msgSum.bestDisplayNameForMessageSummary( out nameIsPhoneNumber );

				name = ( nameIsPhoneNumber ? PhoneUtils.FormatPhoneNumber( name ) : name );

                cvm = new MessageItemViewModel();

                cvm.CmGroup			= msgSum.CmGroup;
                cvm.NameOrNumber	= name;
                cvm.LastMessage		= GetLastMessage(msgSum.MsgBody, msgSum.MsgType);
                cvm.DateText		= TimeUtils.GetDateWithFormatting( msgSum.MsgServerTimestamp.AsLocalTime() );
                cvm.PhoneNumber		= PhoneUtils.FormatPhoneNumber(msgSum.bestPhoneNumber());
                cvm.ContactKey		= msgSum.ContactKey;

				//For PhoneNumbers not in contact list, such as incoming SMS from unknown phone number,
				//	msgSum will NOT have valid phone number, so use the cvm.PhoneNumber above.
				//	It will be normalized by DBM.
				//
				//NOTE use of Ex properties from SessionManager.
                int unreadMsgCount = ContactManager.getInstance().getNewInboundMessageCount( cvm.PhoneNumber, msgSum.XmppJid, 
																							 SettingsManager.Instance.User.ShowVoicemailsInChatThreadEx, 
																							 SettingsManager.Instance.User.ShowRecordedCallsInChatThreadEx, 
																							 SettingsManager.Instance.User.ShowFaxesInChatThreadEx );

                cvm.HasUnreadMessage = unreadMsgCount > 0;	//TODO: do we need to use the actual number in a badge?
                cvm.AvatarUri = AvatarManager.Instance.GetUriForMessaging( msgSum.ContactKey );

                list.Add(cvm);
            }

            if (!string.IsNullOrWhiteSpace(searchMessages))
            {
                formattedSearchMessages = searchMessages.Replace("-", "");
                formattedSearchMessages = formattedSearchMessages.Replace(" ", "");


                list = list.Where(x => (x.NameOrNumber.ToLower().Contains(searchMessages.ToLower()) == true) ||
                                        (PhoneUtils.GetPhoneNumberForFax(x.PhoneNumber).Contains(formattedSearchMessages) == true)).ToList();
            }

            Messages.Clear();
            Messages.AddRange(list);

            NoSearchResultsAvailable = (Messages.Count == 0);

            // Restore the selection
            if (backupSelectedItemKey != "")
            {
                var msg = (from m in Messages
                           where m.UniqueId == backupSelectedItemKey
                           select m).FirstOrDefault();

                if (msg != null)
                {
                    var index = Messages.IndexOf(msg);

                    SelectedIndex = index;
                }
                else
                {
                    if (Messages.Count > 0)
                    {
                        SelectedIndex = -1;
                        SelectedIndex = 0;
                    }
                    else
                    {
                        MessengerInstance.Send<ShowContactMessagesMessage>(new ShowContactMessagesMessage { CmGroup = Contact.INVALID_CMGROUP, PhoneNumber = string.Empty, ContactKey = string.Empty });
                    }
                }
            }

            if (isInitPending) // If the UI is loading for the first time, then choose the first Item
            {
                if (SelectedIndex == -1)
                {
                    if (Messages.Count > 0)
                    {
                        SelectedIndex = 0;
                    }
                }
            }

            prof.Stop();
        }

        private void OnChooseContact()
        {
            MessengerInstance.Send<MessageComposeMessage>(new MessageComposeMessage { Show = true });
        }

        /// <summary>
        /// This method gets called when a Contact is choosen from Compose Message listing.
        /// So if contact is already in the list, select that item, else remove the selection from the list.
        /// </summary>
        /// <param name="msg"></param>
        private void OnShowContactMessages(ShowContactMessagesMessage msg)
        {
            var index = -1;

            var item = (from m in Messages
                        where (msg.CmGroup != Contact.INVALID_CMGROUP && m.CmGroup == msg.CmGroup) || m.PhoneNumber == msg.PhoneNumber
                        select m).FirstOrDefault();

            if (item != null)
            {
                index = Messages.IndexOf(item);
            }

            selfSelectedIndexUpdate = true;
            SelectedIndex			= index;
            selfSelectedIndexUpdate = false;
        }

        private void OnShowOrHideVoiceMailsMessage(ShowOrHideVoiceMailsMessage msg)
        {
            LoadMessages();
        }

        private void OnDbChange(object sender, DbChangedEventArgs e)
        {
            if (e.Data.isMsgTable() || e.Data.isContactTable() )	//Contact table for avatar initials
            {
                DispatcherHelper.CheckBeginInvokeOnUI(() => { LoadMessages(); });
            }
        }

        private string GetLastMessage(string msgBody, ManagedDataTypes.Message.MsgType msgType)
        {
            if (!string.IsNullOrWhiteSpace(msgBody) && msgBody.ToLower().Contains("voxoxrichmessage"))
            {
                try
                {
                    RichData rtm = RichData.fromJsonText(msgBody);

                    if (rtm != null)
                    {
                        if (rtm.isTypeImage())
                        {
                            msgBody = "Picture";
                        }
                        else if (rtm.isTypeVideo())
                        {
                            msgBody = "Video";
                        }
                        else if (rtm.isTypeLocation())
                        {
                            msgBody = "Location";
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.Error("GetLastMessage", ex);
                    msgBody = "";
                }
            }
            else
            {
                switch (msgType)
                {
                    case Message.MsgType.VOICEMAIL:
                        msgBody = "Voicemail";
                        break;
                    case Message.MsgType.FAX:
                        msgBody = "Fax";
                        break;
                    case Message.MsgType.RECORDED_CALL:
                        msgBody = "Recorded Call";
                        break;
                }
            }

            return msgBody;
        }

        #endregion
    }

    public class MessageComposeMessage
    {
        public bool Show { get; set; }
    }

    public class HideDrawerMessage
    {
    }
}
