﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;				//SessionManager
using Desktop.Model.Contact;		//ContactManager
using Desktop.Model.Messaging;		//MessageManager
using Desktop.Util;					//PhoneUtils
using Desktop.Utils;				//VXLogger

using ManagedDataTypes;				//Message, PhoneNumber

using Vxapi23;						//RestfulApiManager

using GalaSoft.MvvmLight;			 //ViewModelBase, MessengerInstance, Set
using GalaSoft.MvvmLight.CommandWpf; //RelayCommand

using System;						//DateTime, Exception
using System.Windows.Input;			//ICommand

namespace Desktop.ViewModels.Messages
{
    public class VoicemailsViewModel : ViewModelBase
    {
        private static VXLogger logger = VXLoggingManager.INSTANCE.GetLogger("VoicemailsViewModel");
        private string header;
        private string heading;
        private string name;
        private string date;
        private string audioSource;
        private string phoneNumber;
        private string subline;
        private string messageText;
		private string msgTimestamp;

        public VoicemailsViewModel(VoicemailMessage msg)
        {
            LoadData(msg);
            VoiceMailsCloseCommand = new RelayCommand(OnCloseCommand, () => { return true; });
        }

        public string Header
        {
            get { return header; }
            set { Set(ref header, value); }
        }

        public string Heading
        {
            get { return heading; }
            set { Set(ref heading, value); }
        }

        public string Name
        {
            get { return name; }
            set { Set(ref name, value); }
        }

        public string Date
        {
            get { return date; }
            set { Set(ref date, value); }
        }

        public string AudioSource
        {
            get { return audioSource; }
            set { Set(ref audioSource, value); }
        }

        public string MsgTimestamp		//TODO: Should be 'long'
        {
            get { return msgTimestamp; }
            set { Set(ref msgTimestamp, value); }
        }

        public string PhoneNumber
        {
            get { return phoneNumber; }
            set { Set(ref phoneNumber, value); }
        }

        public string Subline
        {
            get { return subline; }
            set { Set(ref subline, value); }
        }

        public string MessageText
        {
            get { return messageText; }
            set { Set(ref messageText, value); }
        }

        public ICommand VoiceMailsCloseCommand { get; set; }

        private void LoadData(VoicemailMessage msg)
        {
            try
            {
				msgTimestamp = msg.TimeStamp.ToString();

                Message message = MessageManager.getInstance().getMessage(msg.TimeStamp);
                int cmGroup = ContactManager.getInstance().getCmGroup(message.FromDid);
                PhoneNumber contact = ContactManager.getInstance().getPhoneNumber(message.FromDid, cmGroup);

                if (message.isVoicemail())
                {
                    header = "Voicemail Review";
                    heading = "Voicemail from";
                }
                else
                {
                    header = "Recorded Call Review";
                    heading = "Recorded Call";
                }

                name = PhoneUtils.FormatPhoneNumber(contact.Name != "" ? contact.Name : contact.Number);
                date = TimeUtils.GetDateWithLongFormatting( message.ServerTimestamp.AsLocalTime() );

				Message.ServerMsgType msgType = (message.isVoicemail() ? Message.ServerMsgType.VOICEMAIL : Message.ServerMsgType.RECORDED_CALL);
				audioSource = RestfulAPIManager.INSTANCE.formatDownloadFileUrl( SessionManager.Instance.User.UserKey, SessionManager.Instance.User.CompanyUserId, (int)msgType, message.MsgId );

                subline = contact.isVoxox() ? Config.UI.Contact_Label : "";
                phoneNumber = PhoneUtils.FormatPhoneNumber(message.FromDid);

                if (message.isVoicemail())
                    messageText = message.Body != "" ? message.Body : "VoiceMail";
                else if (message.isRecordedCall())
                    messageText = message.Body != "" ? message.Body : LocalizedString.RecordedCall;
            }
            catch (Exception e)
            {
                logger.Error("Failed to get number properties: " + e);
            }
        }

        private void OnCloseCommand()
        {
            MessengerInstance.Send<VoicemailDoneMessage>(new VoicemailDoneMessage());
        }
    }
}
