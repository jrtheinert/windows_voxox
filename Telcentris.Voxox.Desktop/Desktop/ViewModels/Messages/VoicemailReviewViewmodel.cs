﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using GalaSoft.MvvmLight;	//ViewModelBase, Set

namespace Desktop.ViewModels.Messages
{
    public class VoicemailReviewViewmodel : ViewModelBase
    {
        private ViewModelBase content;
        public VoicemailsViewModel voicemailVm;

        public VoicemailReviewViewmodel(VoicemailMessage msg)
        {
            ShowVoicemailsorRecordedCalls(msg);
        }

        public ViewModelBase Content
        {
            get { return content; }
            set { Set(ref content, value); }
        }

        public void ShowVoicemailsorRecordedCalls(VoicemailMessage msg)
        {
            voicemailVm = new VoicemailsViewModel(msg);
            Content = voicemailVm;
        }
    }
}
