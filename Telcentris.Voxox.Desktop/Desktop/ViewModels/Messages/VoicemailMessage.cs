﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

namespace Desktop.ViewModels.Messages
{
    public enum AudioType
    {
        Voicemail,
        RecordedCall
    }

    public class VoicemailMessage
    {
        public long TimeStamp { get; set; }
    }

    public class VoicemailDoneMessage
    {
        // to close the window
    }
}
