﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;					//AvatarManager, SessionManager
using Desktop.Model.Contact;			//ContactManager
using Desktop.ViewModels.Contacts;		//ContactViewModel
using Desktop.Util;						//PhoneUtils

using ManagedDataTypes;					//Contact, ContactList, PhoneNumber

using GalaSoft.MvvmLight;				//ViewModelBase, MessengerInstance, Set
using GalaSoft.MvvmLight.CommandWpf;	//RelayCommand

using System.Collections.Generic;		//List
using System.Collections.ObjectModel;	//ObservableCollection
using System.Windows.Input;				//ICommand

//TODO: Most of this code is duplicated following classes and should be refactored.:
//	- ContactListViewModel
//	- ComposeFaxViewModel
//	- ComposeMessageViewModel
namespace Desktop.ViewModels.Messages
{
    public class ComposeMessageViewModel : ViewModelBase
    {
        #region | Private Class Variables |

        private string searchContacts;
        private bool noSearchResultsAvailable;
        private int selectedIndex = -1;

        #endregion

        #region | Constructors |

        public ComposeMessageViewModel()
        {
            Contacts = new ObservableCollection<ContactViewModel>();

            LoadContacts();

            CloseComposeMessageCommand = new RelayCommand(OnCloseCompose);
        }

        #endregion

        #region | Public Properties |

        public ObservableCollection<ContactViewModel> Contacts { get; set; }

        public string SearchContacts
        {
            get { return searchContacts; }
            set
            {
                Set(ref searchContacts, value);

                LoadContacts();
            }
        }

        public bool NoSearchResultsAvailable
        {
            get { return noSearchResultsAvailable; }
            set { Set(ref noSearchResultsAvailable, value); }
        }

        public int SelectedIndex
        {
            get { return selectedIndex; }
            set
            {
                Set(ref selectedIndex, value);

                if (selectedIndex > -1 && Contacts.Count > 0 && selectedIndex < Contacts.Count)
                {
                    CallContactsMessagesMessage();
                }
            }
        }

        public ICommand CloseComposeMessageCommand { get; set; }

        #endregion

        #region | Private Procedures |

        private void LoadContacts()
        {
            ContactViewModel cvm = null;
            List<ContactViewModel> list = new List<ContactViewModel>();
            Contacts.Clear();

            ContactList cl = ContactManager.getInstance().getContacts( searchContacts, ContactFilter.All );

            foreach (Contact contact in cl)
            {
                cvm = FromContactModel(contact, false);
                if (cvm.IsRegisteredUser || Config.Settings.EnableSMS)
                {
                    list.Add(cvm);
                }
            }
            if (list.Count > 0)
            {
                foreach (ContactViewModel contact in list)
                {
                    Contacts.Add(contact);
                }
            }
            else
            {
                if (PhoneUtils.IsValidNumber(SearchContacts))
                {
                    cvm = FromContactModel(new Contact { Name = SearchContacts, PhoneNumber = SearchContacts }, true);

                    if (Config.Settings.EnableSMS)
                    {
                        Contacts.Add(cvm);
                    }
                }
            }

            NoSearchResultsAvailable = (Contacts.Count == 0);
        }

        private ContactViewModel FromContactModel(Contact contact, bool isNewContact)
        {
            ContactViewModel cvm = new ContactViewModel();

            cvm.CmGroup				= contact.CmGroup;
            cvm.NameOrNumber		= isNewContact ? contact.Name : PhoneUtils.FormatPhoneNumber(contact.Name);
            cvm.IsRegisteredUser	= (contact.VoxoxCount > 0);
			cvm.InNetworkImageUri	= Config.UI.InNetworkContact;
			cvm.PhoneNumber			= isNewContact ? contact.PhoneNumber : PhoneUtils.FormatPhoneNumber(contact.PhoneNumber);	//PhoneNumber, in case this is a message to number NOT in AB.

            cvm.AvatarUri			= AvatarManager.Instance.GetUriForContact( contact.ContactKey );
            cvm.LastSeen			= (contact.VoxoxCount > 0) ? "Last online on May 15" : "";            //TODO: WE do not get this data from DB yet.

            return cvm;
        }

        private void OnCloseCompose()
        {
            MessengerInstance.Send<MessageComposeMessage>(new MessageComposeMessage { Show = false });
        }

        /// <summary>
        /// Check if any contact has more than one number 
        /// If yes, loads the PhoneNumbersShowMessage else ShowContactMessagesMessage.
        /// </summary>
        private void CallContactsMessagesMessage()
        {
            var item = Contacts[selectedIndex];
            var db   = SessionManager.Instance.UserDataStore;

            PhoneNumber number	= db.getPhoneNumber( item.PhoneNumber, item.CmGroup );
            var			list	= db.getContactPhoneNumbersListForCmGroup( number.CmGroup, number.isVoxox() );

            if (list.Count > 1)
            {
				MessengerInstance.Send<PhoneNumbersShowMessage>(new PhoneNumbersShowMessage { CmGroup = item.CmGroup, PhoneNumber = item.PhoneNumber });
            }
            else
            {
				ActionManager.Instance.ShowContactMessages( item.CmGroup, item.PhoneNumber, item.ContactKey, true );
                // OnCloseCompose(); // Keeping the Window open
            }
        }

        #endregion
    }
}
