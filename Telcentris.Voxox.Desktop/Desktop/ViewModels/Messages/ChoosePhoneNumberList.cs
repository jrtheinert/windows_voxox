﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;					//SessionManager

using ManagedDataTypes;					//PhoneNumber

using GalaSoft.MvvmLight;				//ViewModelBase, MessengerInstance, Set
using GalaSoft.MvvmLight.CommandWpf;	//RelayCommand

using System.Collections.Generic;		//List
using System.Collections.ObjectModel;	//ObservableCollection
using System.Linq;						//<query>
using System.Windows.Input;				//ICommand

namespace Desktop.ViewModels.Messages
{
    class ChoosePhoneNumberList : ViewModelBase
    {
        #region private variables

        private int selectedIndex = -1;

        private ChooseANumberViewModel selectedContact = new ChooseANumberViewModel();

        #endregion

        #region Public properties and methods

        public ChoosePhoneNumberList()
        {
            PhoneNumbers = new ObservableCollection<ChooseANumberViewModel>();

            CancelCommand = new RelayCommand(OnCancel);
            NewMessageCommand = new RelayCommand(OnNewMessage);
            SelectContactCommand = new RelayCommand<string>(OnContactSelect);
            MessengerInstance.Register<PhoneNumbersShowMessage>(this, OnPhoneNumbersMessage);
        }

        public ICommand CancelCommand{get; set;}

        public ICommand NewMessageCommand { get; set; }

        public ICommand SelectContactCommand { get; set; }

        public ObservableCollection<ChooseANumberViewModel> PhoneNumbers { get; set; }

        public int SelectedIndex
        {
            get { return selectedIndex; }
            set 
            { 
                Set(ref selectedIndex, value);
                var item = PhoneNumbers[SelectedIndex];
                OnContactSelect(item.PhoneNumber);
            }
        }

        #endregion


        private void OnCancel()
        {
            MessengerInstance.Send<PhoneNumbersCloseMessage>(new PhoneNumbersCloseMessage());
        }
        
        private void OnNewMessage()
        {
            if(selectedContact != null)
            {
                MessengerInstance.Send<ShowContactMessagesMessage>(new ShowContactMessagesMessage{ CmGroup = selectedContact.CmGroup, PhoneNumber = selectedContact.PhoneNumber, ContactKey = string.Empty });
                MessengerInstance.Send<PhoneNumbersCloseMessage>(new PhoneNumbersCloseMessage());
            }
        }

        private void OnPhoneNumbersMessage(PhoneNumbersShowMessage msg)
        {

            PhoneNumbers.Clear();
            var db = SessionManager.Instance.UserDataStore;

            List<ChooseANumberViewModel> numbersList = new List<ChooseANumberViewModel>();

            PhoneNumber number = db.getPhoneNumber(msg.PhoneNumber, msg.CmGroup);

            var list = db.getContactPhoneNumbersListForCmGroup(number.CmGroup, number.isVoxox());

            if (list.Count() > 1)
            {
                foreach (PhoneNumber phoneNum in list)
                {
                    numbersList.Add(new ChooseANumberViewModel()
                    {
                       CmGroup = phoneNum.CmGroup,
                       PhoneNumber = phoneNum.Number,
                       Label = phoneNum.Label
                    });
                }
            }
            foreach (var cm in numbersList)
            {
                PhoneNumbers.Add(cm);
            }

            SelectedIndex = 0;
        }

        private void OnContactSelect(string phoneNumber)
        {
            foreach(var phoneNum in PhoneNumbers)
            {
                if (phoneNum.PhoneNumber.Equals(phoneNumber))
                {
                    selectedContact = phoneNum;
                    phoneNum.IsSelected = true;
                }
                else
                    phoneNum.IsSelected = false;
            }

        }

  
    }

    public class PhoneNumbersCloseMessage
    {

    }

    public class PhoneNumbersShowMessage{
        public int CmGroup { get; set; }
        public string PhoneNumber { get; set; }
    }

}
