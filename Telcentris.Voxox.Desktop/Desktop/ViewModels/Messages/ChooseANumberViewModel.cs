﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using GalaSoft.MvvmLight;		//ViewModelBase, Set

namespace Desktop.ViewModels.Messages
{
    public class ChooseANumberViewModel : ViewModelBase
    {
        #region PrivateProperties

        private int cmGroup;
        private string phoneNumber;
        private string label;
        private bool isSelected = false;

        #endregion

        #region Public properties

        public int CmGroup 
        {
            get { return cmGroup; }
            set { Set(ref cmGroup, value); }
        }
        public string PhoneNumber
        {
            get { return phoneNumber; }
            set { Set(ref phoneNumber, value); }
        }
        public string Label
        {
            get { return label; }
            set { Set(ref label, value); }
        }
        public bool IsSelected 
        {
            get { return isSelected; }
            set { Set(ref isSelected, value); }
        }

        #endregion
    }
}
