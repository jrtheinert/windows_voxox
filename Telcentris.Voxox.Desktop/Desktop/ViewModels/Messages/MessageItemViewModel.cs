﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using GalaSoft.MvvmLight;	//ViewModelBase, Set

namespace Desktop.ViewModels.Messages
{
    /// <summary>
    /// This class provides display properties & actions for each "Message" item in the Message list.
    /// Setting of the properties is taken care from the listing model
    /// </summary>
    public class MessageItemViewModel : ViewModelBase
    {
        #region | Private Class Variables |

        private int cmGroup;
		private string contactKey;
        private string phoneNumber;
        private string nameOrNumber;
        private string lastMessage;
        private string dateText;
        private string avatarUri;
        private bool hasUnreadMessage;

        #endregion

        #region | Public Properties |

        /// <summary>
        /// CmGroup
        /// </summary>
        public int CmGroup
        {
            get { return cmGroup; }
            set { Set(ref cmGroup, value); }
        }

        /// <summary>
        /// Contact's Display Name or Phone Number
        /// </summary>
        public string NameOrNumber
        {
            get { return nameOrNumber; }
            set { Set(ref nameOrNumber, value); }
        }

        /// <summary>
        /// Text that indicates the last message sent/received
        /// e.g., "What? Really?", "Sent you a photo", "Online May 26 2013"
        /// </summary>
        public string LastMessage
        {
            get { return lastMessage; }
            set { Set(ref lastMessage, value); }
        }

        /// <summary>
        /// Text that indicates the call date
        /// e.g., "Today", "M/DD/YY"
        /// </summary>
        public string DateText
        {
            get { return dateText; }
            set { Set(ref dateText, value); }
        }

        /// <summary>
        /// Avatar Uri of the Contact
        /// </summary>
        public string AvatarUri
        {
            get { return avatarUri; }
            set { Set(ref avatarUri, value); }
        }

        /// <summary>
        /// Has any unread message
        /// </summary>
        public bool HasUnreadMessage
        {
            get { return hasUnreadMessage; }
            set { Set(ref hasUnreadMessage, value); }
        }

        /// <summary>
        /// Primary Phone Number
        /// </summary>
        public string PhoneNumber
        {
            get { return phoneNumber; }
            set { Set(ref phoneNumber, value); }
        }

        /// <summary>
        /// Contact Key
        /// </summary>
        public string ContactKey
        {
            get { return contactKey; }
            set { Set(ref contactKey, value); }
        }

        /// <summary>
        /// A unique key to identify in message list
        /// </summary>
        public string UniqueId
        {
            get
            {
                return string.Format("{0}{1}{2}", cmGroup, nameOrNumber, phoneNumber).Trim().ToUpper();
            }
        }

        #endregion
    }
}
