﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;				//ActionManager
using Desktop.Utils;				//VXLogger

using GalaSoft.MvvmLight;			 //ViewModelBase, MessengerInstance, Set
using GalaSoft.MvvmLight.CommandWpf; //RelayCommand

using System;						//DateTime, Int64
using System.Windows.Input;			//ICommand
using System.Windows.Media;			//Brush

namespace Desktop.ViewModels.Messages
{
    public enum MessageDirection	//TODO: Ensure these match model layer values.
    {
        Incoming,
        Outgoing
    }

    public enum MessageStatus	//TODO: Why do these NOT match model layer?  Makes it difficult to debug vs. database.
    {
        New,
        Sent,
        Delivered,
        Read,
        Failed,
        Retry
    }

    public enum MessageBodyType
    {
        Plain,			//Chat, SMS, GM?

        Fax,			//Same as model layer msgType?
        Voicemail,		//Same as model layer msgType?
        RecordedCall,	//Same as model layer msgType?
        
		Location,		//RichData - NOT USED
        Picture,		//RichData
        Video,			//RichData - NOT USED
        Contact,		//RichData - NOT USED

//      Phone,			//NOT USED.  What is this?
        Link			//Used only for Video
    }

    /// <summary>
    /// Base Class for different types of Message items
    /// </summary>
    public abstract class MessageThreadItem : ViewModelBase
    {
        private DateTime date;
        private MessageDirection messageDirection;
        private MessageStatus messageStatus;
        private bool isTyping;

        public DateTime Date
        {
            get { return date; }
            set { Set(ref date, value); }
        }

        public MessageDirection MessageDirection
        {
            get { return messageDirection; }
            set { Set(ref messageDirection, value); }
        }

        public MessageStatus MessageStatus
        {
            get { return messageStatus; }
            set { Set(ref messageStatus, value); }
        }

        public bool IsTyping
        {
            get { return isTyping; }
            set { Set(ref isTyping, value); }
        }

		public Int64 LocalTimestamp { get; set;	}

        public bool ToRemove { get; set; }
    }

    /// <summary>
    /// Chat Message Item
    /// </summary>
    public class ChatMessageThreadItem : MessageThreadItem
    {
        private static VXLogger logger = VXLoggingManager.INSTANCE.GetLogger("ChatMessageThreadItem");
        private string avatarUri;
        private string body;
        private MessageBodyType bodyType;
        private bool isSMS;
        private string fileUrl;
        private string thumbnailUrl;
        private Brush senderChatBubbleColor;
        private Brush receipentChatBubbleColor;
        private bool isLocation;

        public ChatMessageThreadItem()
        {
            LinkCommand					= new RelayCommand(OnLinkLaunch);	//TODO: This appears to handle IMAGE RichData and NOT URL links.
            ShowOriginalPictureCommand	= new RelayCommand(OnShowOriginalPicture);
            ViewFaxCommand				= new RelayCommand(OnViewFax);
			ResendChatCommand			= new RelayCommand(OnResendChat);
        }

        public string AvatarUri
        {
            get { return avatarUri; }
            set { Set(ref avatarUri, value); }
        }

        public string Body
        {
            get { return body; }
            set { Set(ref body, value); }
        }

        public MessageBodyType BodyType
        {
            get { return bodyType; }
            set { Set(ref bodyType, value); }
        }

        public bool IsSMS
        {
            get { return isSMS; }
            set { Set(ref isSMS, value); }
        }

        public string FileUrl
        {
            get { return fileUrl; }
            set { Set(ref fileUrl, value); }
        }

        public string ThumbnailUrl
        {
            get { return thumbnailUrl; }
            set { Set(ref thumbnailUrl, value); }
        }

        public Brush SenderChatBubbleColor
        {
            get { return senderChatBubbleColor; }
            set { Set(ref this.senderChatBubbleColor, value); }
        }

        public Brush ReceipentChatBubbleColor
        {
            get { return receipentChatBubbleColor; }
            set { Set(ref this.receipentChatBubbleColor, value); }
        }

        public bool IsLocation
        {
            get { return isLocation; }
            set { Set(ref isLocation, value); }
        }

        public ICommand LinkCommand					{ get; private set; }
        public ICommand ShowOriginalPictureCommand  { get; private set; }
        public ICommand ViewFaxCommand				{ get; set; }
        public ICommand ResendChatCommand			{ get; set; }

        private void OnLinkLaunch()		//TODO: This appears to handle IMAGE RichData and NOT URL links.
        {
            if (IsLocation)
            {
                ActionManager.Instance.HandleUrl( FileUrl );
            }
            else
            {
                ActionManager.Instance.ViewVideo( LocalTimestamp );
            }
        }

        private void OnShowOriginalPicture()
        {
            ActionManager.Instance.ViewImage( LocalTimestamp );
        }

        private void OnViewFax()
        {
            ActionManager.Instance.ViewFax( LocalTimestamp );
        }

        private void OnViewVideo()
        {
            ActionManager.Instance.ViewVideo( LocalTimestamp );
        }

		private void OnResendChat()
		{
            ActionManager.Instance.ResendMessage( LocalTimestamp );
		}
    }

    /// <summary>
    /// Call Well Item
    /// </summary>
    public class CallMessageThreadItem : MessageThreadItem
    {

    }

    /// <summary>
    /// Fax Item
    /// </summary>
    public class FaxMessageThreadItem : MessageThreadItem
    {

    }

    /// <summary>
    /// Voicemail Item
    /// </summary>
    public class VoicemailMessageThreadItem : MessageThreadItem
    {
        private string body;
        private long time;
        private MessageBodyType bodyType;
        private string avatarUri;
        private Brush senderChatBubbleColor;
        private Brush receipentChatBubbleColor;
        private bool isVoicemailPlayed;
        private bool isBodyTruncated;

        public VoicemailMessageThreadItem()
        {
            PlayVoicemailCommand = new RelayCommand<long>((time => OnPlayVoicemail(time)));
        }

        public ICommand PlayVoicemailCommand { get; set; }

        public string Body
        {
            get { return body; }
            set { Set(ref body, value); }
        }

        public long Time
        {
            get { return time; }
            set { Set(ref time, value); }
        }

        public MessageBodyType BodyType
        {
            get { return bodyType; }
            set { Set(ref bodyType, value); }
        }

        public string AvatarUri
        {
            get { return avatarUri; }
            set { Set(ref avatarUri, value); }
        }

        public Brush SenderChatBubbleColor
        {
            get { return senderChatBubbleColor; }
            set { Set(ref this.senderChatBubbleColor, value); }
        }

        public Brush ReceipentChatBubbleColor
        {
            get { return receipentChatBubbleColor; }
            set { Set(ref this.receipentChatBubbleColor, value); }
        }

        public bool IsVoicemailPlayed
        {
            get { return isVoicemailPlayed; }
            set { Set(ref this.isVoicemailPlayed, value); }
        }

        public bool IsBodyTruncated
        {
            get { return isBodyTruncated; }
            set { Set(ref this.isBodyTruncated, value); }
        }

        private void OnPlayVoicemail(long time)
        {
            MessengerInstance.Send<VoicemailMessage>(new VoicemailMessage { TimeStamp = time });
        }
    }
}
