﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;					//AvatarManager, SessionManager, SettingsManager
using Desktop.Model.Contact;			//ContactManager
using Desktop.Model.Messaging;			//MessageManager, XmppManager, XmppTypingNotificationMessage
using Desktop.Util;						//PhoneUtils, TimeUtils
using Desktop.Utils;					//VXLogger
using Desktop.ViewModels.Contacts;		//ContactNumberViewModel
using Desktop.ViewModels.Settings;		//ChatBubblesColorViewModel

using ManagedDataTypes;					//Message, PhoneNumber, UmwMessage, RichData, VxTimestamp
using ManagedDbManager;					//DbChangedEventHandler, DbChangedEventArgs

using GalaSoft.MvvmLight;				//ViewModelBase, MessengerInstance, Set
using GalaSoft.MvvmLight.CommandWpf;	//RelayCommand
using GalaSoft.MvvmLight.Threading;		//DispatcherHelper

using System;							//Action, Exception, Int64, String, DateTime, Guid, Uri
using System.Collections.Generic;		//List
using System.Collections.ObjectModel;	//ObservableCollection
using System.IO;						//Path - TODO: Appears related logic should be in Model layer
using System.Linq;						//<query>
using System.Threading.Tasks;			//Task
using System.Windows;					//Application
using System.Windows.Input;				//ICommand
using System.Windows.Media;				//Brush, BrushConverter
using System.Xml;						//XmlDocument - to read Colors.xml (TODO: Do we have this?)

namespace Desktop.ViewModels.Messages
{
    /// <summary>
    /// The class is the backing model for Messages Center pane.
    /// This will call/consume the PAPI methods & objects
    /// </summary>
    public class MessagesViewModel : ViewModelBase
    {
        #region | Private Class Variables |

        private static VXLogger logger = VXLoggingManager.INSTANCE.GetLogger("MessagesViewModel");
        private int cmGroup;
        private string name;
        private string avatarUri;
        private string onlineStatus;
        private string messageText;
        private string smsChargesText;
        private string phoneNumber;
		private string contactKey;
        private string selectedCallPhone;

        private bool hasAdditionalNumbers;
        private bool showFaxOption;
        private bool isSMSMode;				//TODO: rework this.  Depends on: 
        private bool isSelfTyping = false;

        private string otherJid;
        private ChatMessageThreadItem typingMsg;

        private DbChangedEventHandler dbChangeHandler = null;

        string selfAvatarUri = SessionManager.Instance.User.AvatarUri;

        public Brush senderBubbleColor;
        public Brush receipentBubbleColor;

        private List<ChatBubblesColorViewModel>				 colorsList     = new List<ChatBubblesColorViewModel>();
		private ObservableCollection<ContactNumberViewModel> contactNumbers = new ObservableCollection<ContactNumberViewModel>();
	
        #endregion

        #region | Constructors |

        public MessagesViewModel( string contactKey, int cmGroupIn, string phoneNumberIn)
        {
            Messages = new VxObservableCollection<MessageThreadItem>();

            SendCommand			= new RelayCommand( new Action(OnSend), () => { return !string.IsNullOrWhiteSpace(messageText); });
            CallCommand			= new RelayCommand( OnCall);
            ContactInfoCommand	= new RelayCommand( OnContactInfo );
            GalleryCommand		= new RelayCommand( OnGallery	  );
            FaxCommand			= new RelayCommand( OnFax		  );
            TranslatorCommand	= new RelayCommand( OnTranslator  );
            LocationCommand		= new RelayCommand( OnLocation	  );

			ContactKey  = contactKey;
            CmGroup		= cmGroupIn;
            PhoneNumber = phoneNumberIn;

			//Since LoadData() may change DB, due to mark READ or incoming message, the handler needs to come before LoadData().
            dbChangeHandler = new DbChangedEventHandler(OnDbChange);
            getUserDbm().addDbChangeListener(dbChangeHandler);

            ContactNumbers = new ObservableCollection<ContactNumberViewModel>();

			handlePhoneNumberChange();

            LoadColors();
            OnBubbleColorChange(new ChangeChatBubbleColorMessage());	//Calls LoadData()

            MessengerInstance.Register<XmppTypingNotificationMessage>	 ( this, OnTyping	);
            MessengerInstance.Register<SendFileMessage>					 ( this, OnSendFile	);
            MessengerInstance.Register<SendFaxMessage>					 ( this, OnSendFax	);
            MessengerInstance.Register<ChangeChatBubbleColorMessage>	 ( this, OnBubbleColorChange);
            MessengerInstance.Register<ShowOrHideVoiceMailsMessage>		 ( this, (OnChange) => { LoadData(); });
            MessengerInstance.Register<ChangeContactMessageNumberMessage>( this, OnChangeContactMessageNumber );	//User selected a different number to IM/SMS on.
            MessengerInstance.Register<ReloadDataMessage>				 ( this, OnReloadData );					//Force reload of data under specific conditions

            ShowFaxOption = SessionManager.Instance.User.Options.IsFaxAvailable;

			//User may have chosen a new message thread while the ContactInfo right panel is open.
			//In that case, we want to close the old one and open a new one, complete with normal UX animations.
			MessengerInstance.Send<ShowContactInfoMessage>(new ShowContactInfoMessage { Show = true, Refresh = true, CmGroup = cmGroup, PhoneNumber = phoneNumber } );
        }

		private ManagedDbManager.ManagedDbManager getUserDbm()
		{
			return SessionManager.Instance.UserDataStore;
		}

        public override void Cleanup()
        {
            base.Cleanup();

            if ( getUserDbm() != null && dbChangeHandler != null)
            {
                getUserDbm().removeDbChangeListener(dbChangeHandler);
                dbChangeHandler = null;
            }
        }

        #endregion

        #region | Public Properties |

        public VxObservableCollection<MessageThreadItem> Messages { get; set; }

        public int CmGroup
        {
            get { return cmGroup; }
            set { Set(ref cmGroup, value); }
        }

        public string Name
        {
            get { return name; }
            set { Set(ref name, value); }
        }

        public string ContactKey
        {
            get { return contactKey; }
            set { Set(ref contactKey, value); }
        }

        public string AvatarUri
        {
            get { return avatarUri; }
            set { Set(ref avatarUri, value); }
        }

        public string OnlineStatus
        {
            get { return onlineStatus; }
            set { Set(ref onlineStatus, value); }
        }

        public string PhoneNumber
        {
            get { return phoneNumber; }
            set { Set(ref phoneNumber, value); }
        }

        public Brush SenderBubbleColor
        {
            get { return senderBubbleColor; }
            set { Set(ref this.senderBubbleColor, value); }
        }

        public Brush ReceipentBubbleColor
        {
            get { return receipentBubbleColor; }
            set { Set(ref this.receipentBubbleColor, value); }
        }

        public string MessageText
        {
            get { return messageText; }
            set
            {
                Set(ref messageText, value);

                if (isSMSMode == false)
                {
                    //TODO: This appears to be bad form.  Since this method
                    //	makes an XMPP API call, in the event of an exception,
                    //	the exception is handled somewhere within the .NET
                    //	code which defeats the purpose catching the exception.
                    handleSelfTyping(!string.IsNullOrEmpty(messageText));
                }
            }
        }

        private void handleSelfTyping(bool isTyping)
        {
            //Do nothing if typing state did not change.
            if (isTyping != isSelfTyping)
            {
                //We do NOT display anything when we are typing.  That is only when our contact is typing.
                XmppManager.getInstance().sendChatState(otherJid, isTyping);
                isSelfTyping = isTyping;
            }
        }

        //This is for INCOMING typing indicator
        private void AddTypingIndicator()
        {
            if (typingMsg == null)
            {
                typingMsg = new ChatMessageThreadItem();

                typingMsg.Date				= DateTime.Now;
                typingMsg.MessageDirection	= MessageDirection.Incoming;
                typingMsg.AvatarUri			= avatarUri;
                typingMsg.BodyType			= MessageBodyType.Plain;
                typingMsg.IsTyping			= true;

                typingMsg.SenderChatBubbleColor = SenderBubbleColor;
                typingMsg.ReceipentChatBubbleColor = ReceipentBubbleColor;

                Messages.Add(typingMsg);
            }
        }

        private void RemoveTypingIndicator()
        {
            if (typingMsg != null)
            {
                if (Messages.Contains(typingMsg))
                {
                    Messages.Remove(typingMsg);
                }

                typingMsg.Cleanup();
                typingMsg = null;
            }
        }

        public bool IsSMSMode
        {
            get { return isSMSMode; }
            set { Set(ref isSMSMode, value); }
        }

        public string SMSChargesText
        {
            get { return smsChargesText; }
            set { Set(ref smsChargesText, value); }
        }

        public string SelectedCallPhone
        {
            get { return selectedCallPhone; }
            set {
					Set(ref selectedCallPhone, value);
					var error = ActionManager.Instance.MakeCall( selectedCallPhone );
                }
        }

        public bool HasAdditionalNumbers
        {
            get { return hasAdditionalNumbers; }
            set { Set(ref hasAdditionalNumbers, value); }
        }

        public bool ShowFaxOption
        {
            get { return showFaxOption; }
            set { Set(ref showFaxOption, value); }
        }

        public ObservableCollection<ContactNumberViewModel> ContactNumbers 
		{
			get { return contactNumbers;			}
            set { Set(ref contactNumbers, value);	}
		}

        public ICommand SendCommand			{ get; set; }
        public ICommand CallCommand			{ get; set; }
        public ICommand TranslatorCommand	{ get; set; }
        public ICommand ContactInfoCommand	{ get; set; }
        public ICommand GalleryCommand		{ get; set; }
        public ICommand FaxCommand			{ get; set; }
        public ICommand LocationCommand		{ get; set; }


        #endregion

        #region | Private Procedures |

        /// <summary>
        /// Send the message
        /// </summary>
        private void OnSend()
        {
            Int64 localTs  = VxTimestamp.GetNowTimestamp();
            Int64 serverTs = VxTimestamp.GetNowTimestamp();	//All times are UTC
            Message msg = new Message();

            msg.Status    = Message.MsgStatus.NEW;
            msg.Direction = Message.MsgDirection.OUTBOUND;

            msg.MsgId = XmppManager.generateMsgId();
            msg.Body  = messageText;

            msg.FromDid = SessionManager.Instance.User.Did;
            msg.ToDid   = PhoneNumber;

            if (isSMSMode)
            {
                msg.Type = Message.MsgType.SMS;
            }
            else
            {
                msg.FromJid = SessionManager.Instance.User.XmppJid;
                msg.ToJid   = otherJid;
                msg.Type    = Message.MsgType.CHAT;
            }

            msg.LocalTimestamp  = localTs;
            msg.ServerTimestamp = new VxTimestamp( serverTs );

            MessageManager.getInstance().sendMessage(msg);

            MessageText = "";

            MessengerInstance.Send<MessageSentMessage>(new MessageSentMessage { });
        }

        private void OnCall()
        {
            var error = ActionManager.Instance.MakeCall( PhoneNumber );

            if (error == Model.Calling.PhoneLine.CallError.NoError)
            {
				//TODO: Notify user?
            }
        }

        private void OnContactInfo()
        {
			MessengerInstance.Send<ShowContactInfoMessage>(new ShowContactInfoMessage { Show = true, Refresh = false, CmGroup = cmGroup, PhoneNumber = phoneNumber } );
		}

        private void OnTranslator()
        {
			MessengerInstance.Send<NotImplementedMessage>(new NotImplementedMessage());
		}

        private void OnLocation()
        {
			MessengerInstance.Send<NotImplementedMessage>(new NotImplementedMessage());
		}

        private async void getPhoneNumberInfo()
        {
            //Defaults which may be overridden later
            Name      = PhoneNumber;
            IsSMSMode = true;

            PhoneNumber temp = getUserDbm().getPhoneNumber(PhoneNumber, -1);	//This will NEVER be null

			if ( temp.isFromDb() )	//PhoneNumber is NOT in DB and we have a placeholder PhoneNumber object
            {
                if (temp.isValid())
                {
					bool nameIsPhoneNumber = false;
                    Name = temp.bestDisplayName( out nameIsPhoneNumber );

					if ( nameIsPhoneNumber )
						Name = PhoneUtils.FormatPhoneNumber( Name );

                    otherJid = temp.XmppJid;
                }
                else
                {
                    Name = LocalizedString.GenericUnknown;
                }

                if (temp.isVoxox())
                {
                    IsSMSMode = false;
                    //OnlineStatus = "Last Online Today"; // TODO: Need to get the Status
                }
            }
            else
            {
				bool inNetwork = await ContactManager.getInstance().isInNetwork( phoneNumber );//This will make API call, if needed, and update DB as appropriate.

				if ( inNetwork )
				{
					PhoneNumber temp1 = ContactManager.getInstance().getPhoneNumber(PhoneNumber, -1);

					if ( temp1.isVoxox() )
					{ 
						IsSMSMode = false;
						otherJid = temp1.XmppJid;
 //						OnlineStatus = "Last Online Today"; // TODO: Need to get the Status
					}
				}
            }

            // TODO: Find the rate            
            // if (IsSMSMode)
            // {
            //    SMSChargesText = "SMS: $0.09 / Message";
            // }

            AvatarUri = AvatarManager.Instance.GetUriForMessaging( ContactKey );
        }

        private void LoadData()
        {

            HasAdditionalNumbers = false;

            typingMsg = null;

            UmwMessageList messageList = MessageManager.getInstance().getMessagesForUmw( CmGroup, PhoneNumber );

			//Mark messages READ, as needed.  If any are changed, restart LoadData()
			//NOTE: We do this via SessionManager, since that is where we can monitor active NavTab 
			//	and active Message list entry.
			int changeCount = SessionManager.Instance.SetMessagesReadIfNeeded( CmGroup, PhoneNumber, messageList );

			if ( changeCount > 0 )
			{
				LoadData();		//Should only recurse one time, at most.
				return;
			}

			ReloadMessages( messageList );

			MessengerInstance.Send<MessagesModifiedMessage>(new MessagesModifiedMessage { }); // Work around for UI scroll to bottom

            PhoneNumber number = getUserDbm().getPhoneNumber(phoneNumber, cmGroup);
            var			list   = getUserDbm().getContactPhoneNumbersListForCmGroup(cmGroup, number.isVoxox());

            if (list.Count() > 1)
            {
                HasAdditionalNumbers = true;
                ContactNumbers.Clear();
	
				ContactNumbers = ContactManager.PhoneNumberListToContactNumberViewModel( list );
            }
            else
            {
                HasAdditionalNumbers = false;
            }
        }
		

		private void ReloadMessages( UmwMessageList messageList )
		{
            var showVoicemailsInChatThread    = SettingsManager.Instance.User.ShowVoicemailsInChatThread;
            var showFaxesInChatThread         = SettingsManager.Instance.User.ShowFaxesInChatThread;
            var showRecordedCallsInChatThread = SettingsManager.Instance.User.ShowRecordedCallsInChatThread;

            // All the messages in the list are no longer removed, but initially are only marked to be removed (except typing indicator) & if the messageList does not have the message it is removed
            Messages.All(c => { c.ToRemove = (c.IsTyping == false); return true; });

            var   lastReadMessage	= messageList.getLastReadMessage();
			Int64 lastReadTimestamp = lastReadMessage.ServerTimestamp.AsLong();	//We use ServerTimestamp because getMessageHistory() does not provide other timestamps.

			foreach (var msg in messageList)
			{
                bool canAdd = false;

				if (msg.isChat() || msg.isSms())
				{
                    // Check if message is already in the list, create a new if not exists
                    ChatMessageThreadItem chat = Messages.Where(x => x.LocalTimestamp == msg.LocalTimestamp).FirstOrDefault() as ChatMessageThreadItem;
                
                    if (chat == null)
                    {
                        chat = new ChatMessageThreadItem();
                        canAdd = true;
                    }
                    else
                    {
                        chat.ToRemove = false;
                    }

					chat.LocalTimestamp   = msg.LocalTimestamp;
                    chat.Date			  = msg.ServerTimestamp.AsLocalTime();
					chat.MessageDirection = msg.Direction == Message.MsgDirection.INBOUND ? MessageDirection.Incoming : MessageDirection.Outgoing;
					chat.AvatarUri		  = chat.MessageDirection == MessageDirection.Outgoing ? selfAvatarUri : AvatarUri;
					chat.BodyType		  = MessageBodyType.Plain; // TODO: More body types

					if (msg.Direction == Message.MsgDirection.OUTBOUND)
                    {
                        // Showing green check mark for last read XMPP message.
                        // After that for delivered messages grey check mark will be shown.
                        if (msg.Status == Message.MsgStatus.READ           && msg.ServerTimestamp.AsLong() == lastReadTimestamp)
                            chat.MessageStatus = GetMessageStatus(msg.Status);
                        else if (msg.Status == Message.MsgStatus.DELIVERED && msg.ServerTimestamp.AsLong() > lastReadTimestamp)	//Because we mark all messages READ once we open widget, we will NEVER see this condition.
                            chat.MessageStatus = GetMessageStatus(msg.Status);
                        else if (msg.Status == Message.MsgStatus.SENT)
                            chat.MessageStatus = GetMessageStatus(msg.Status);
                        else if (msg.Status == Message.MsgStatus.FAILED || msg.Status == Message.MsgStatus.PENDING )
                            chat.MessageStatus = GetMessageStatus(msg.Status);
						//else
						//	chat.MessageStatus = GetMessageStatus(msg.Status);	//TODO: What is proper thing to do here for other Statuses?

                    }

					chat.IsSMS					  = (msg.Type == Message.MsgType.SMS);	//To show "SMS" label below the bubble
					chat.SenderChatBubbleColor	  = SenderBubbleColor;
					chat.ReceipentChatBubbleColor = ReceipentBubbleColor;

					if (string.IsNullOrWhiteSpace(msg.Body) == false && msg.Body.ToLower().Contains("voxoxrichmessage")) // TODO: Needs better identification //TODO-JRT: This should be in Message class, not here.
					{
						try
						{
							RichData rtm = RichData.fromJsonText(msg.Body);

							if (rtm != null)
							{
								if (rtm.isTypeMedia())
								{
									chat.FileUrl = rtm.MediaUrl;
									chat.ThumbnailUrl = rtm.ThumbnailUrl;

									if (string.IsNullOrWhiteSpace(chat.ThumbnailUrl))
									{
										chat.ThumbnailUrl = chat.FileUrl;
									}

									if (rtm.isTypeImage())
									{
										chat.BodyType = MessageBodyType.Picture;

										// Loading of Thumbnail from remote source, so show loading image & start downloading in background
										chat.ThumbnailUrl = SessionManager.Instance.PackUri + Config.UI.RichMediaLoading;

										Task.Run(() => DownloadRichMedia(msg, chat, rtm));
									}
									else if (rtm.isTypeVideo())
									{
										chat.BodyType = MessageBodyType.Link;
										if (string.IsNullOrWhiteSpace(chat.Body))
											chat.Body = "Video";
									}
								}
								else if (rtm.isTypeLocation())
								{
									chat.BodyType = MessageBodyType.Link;
									chat.FileUrl = string.Format(Desktop.Config.Urls.MapProviderTemplateURL, rtm.Body);
									chat.Body = rtm.Body;
                                    chat.IsLocation = true;
								}
								else if (rtm.isTypeContact())
								{
									chat.Body = "Contact";
								}
							}
						}
						catch (Exception ex)
						{
							logger.Error("LoadData - GetRichTextMessage - " + msg.MsgId, ex);
						}
					}
					else
					{
						chat.Body = msg.Body;
					}

                    if (canAdd) // Only add new messages
                    {
                        Messages.Add(chat);
                    }
				}
				else
				{
					if ((msg.isVoicemail() && showVoicemailsInChatThread) || (msg.isRecordedCall() && showRecordedCallsInChatThread))
					{
                        // Check if message is already in the list, create a new if not exists
						VoicemailMessageThreadItem voicemail = Messages.Where(x => x.LocalTimestamp == msg.LocalTimestamp).FirstOrDefault() as VoicemailMessageThreadItem;

                        if(voicemail == null)
                        {
                            voicemail = new VoicemailMessageThreadItem();
                            canAdd = true;
                        }
                        else
                        {
                            voicemail.ToRemove = false;
                        }

					    voicemail.LocalTimestamp			= msg.LocalTimestamp;
                        voicemail.Date						= msg.ServerTimestamp.AsLocalTime();
						voicemail.MessageDirection			= msg.Direction == Message.MsgDirection.INBOUND ? MessageDirection.Incoming : MessageDirection.Outgoing;
						voicemail.AvatarUri					= msg.Direction == Message.MsgDirection.OUTBOUND ? selfAvatarUri : AvatarUri;
						voicemail.SenderChatBubbleColor		= SenderBubbleColor;
						voicemail.ReceipentChatBubbleColor	= ReceipentBubbleColor;

						if (msg.isVoicemail())
						{
							voicemail.Body				= msg.Body != "" ? (msg.Body.Length > 160 ? msg.Body.Substring(0,160) + "..." : msg.Body) : "Voicemail";
							voicemail.BodyType			= MessageBodyType.Voicemail;
							voicemail.IsVoicemailPlayed = msg.Body == "" ? msg.isStatusRead() : true;
                            voicemail.IsBodyTruncated   = msg.Body != "" && msg.Body.Length > 160;
						}
						else
						{
                            voicemail.Body		= msg.Body != "" ? msg.Body : LocalizedString.RecordedCall;
							voicemail.BodyType	= MessageBodyType.RecordedCall;
						}

						voicemail.Time		= msg.LocalTimestamp;
						voicemail.BodyType	= MessageBodyType.Voicemail;

                        if(canAdd) // Only add new messages
                        {
						    Messages.Add(voicemail);
                        }

						continue;
					}
					else if ( msg.isFax() && showFaxesInChatThread )
					{
					    //THIS IS ALL WRONG, BUT NECESSARY FOR DEMO, SINCE OTHER MESSAGETHREADITEM TYPES ARE INCOMPLETE
                        // Check if message is already in the list, create a new if not exists
					    ChatMessageThreadItem chat = Messages.Where(x => x.LocalTimestamp == msg.LocalTimestamp).FirstOrDefault() as ChatMessageThreadItem;

                        if (chat == null)
                        {
                            chat = new ChatMessageThreadItem();
                            canAdd = true;
                        }
                        else
                        {
                            chat.ToRemove = false;
                        }

					    chat.LocalTimestamp				= msg.LocalTimestamp;
                        chat.Date						= msg.ServerTimestamp.AsLocalTime();
					    chat.MessageDirection			= msg.Direction == Message.MsgDirection.INBOUND ? MessageDirection.Incoming : MessageDirection.Outgoing;
					    chat.AvatarUri					= chat.MessageDirection == MessageDirection.Outgoing ? selfAvatarUri : AvatarUri;
					    chat.BodyType					= MessageBodyType.Plain; // TODO: More body types
					    chat.AvatarUri					= chat.MessageDirection == MessageDirection.Outgoing ? selfAvatarUri : AvatarUri;
					    chat.IsSMS						= (msg.Type == Message.MsgType.SMS);
					    chat.SenderChatBubbleColor		= SenderBubbleColor;
					    chat.ReceipentChatBubbleColor	= ReceipentBubbleColor;
						chat.BodyType	                = MessageBodyType.Fax;
						chat.Body		                = "Fax";

                        if (canAdd) // only add new messages
                        {
                            Messages.Add(chat);
                        }
					}
				}	//if (msg.isChat() || msg.isSms())
			} // foreach

            Messages.RemoveAll(x => x.ToRemove == true);
            
            // Move the typing indicator to the bottom if it exists.
            // This handles the case where the typing indicator was being removed on list reload (because of db update) though the user at the other end was still typing
            if(Messages.Count(x => x.IsTyping) > 0)
            {
                typingMsg = (ChatMessageThreadItem)Messages.Where(x => x.IsTyping).FirstOrDefault();
                int oldIndex = Messages.IndexOf(typingMsg);
                Messages.Move(oldIndex, Messages.Count - 1);                
            }
		}

        private void DownloadRichMedia(UmwMessage msg, ChatMessageThreadItem chat, RichData rtm)
        {
            var FileUrl = rtm.MediaUrl;
            var ThumbnailUrl = rtm.ThumbnailUrl;

            if (string.IsNullOrWhiteSpace(ThumbnailUrl))
            {
                ThumbnailUrl = FileUrl;
            }
            
            if (rtm.isTypeImage())
            {
                string localThumbnailUrl = RichMediaManager.Download(msg.RecordId.ToString() + "-THUMB", ThumbnailUrl);
                DispatcherHelper.CheckBeginInvokeOnUI(() =>
                {
                    chat.ThumbnailUrl = localThumbnailUrl;
                });
            }
        }

        private void OnGallery()
        {
            MessengerInstance.Send<ChooseFileMessage>(new ChooseFileMessage { });
        }

        private void OnFax()
        {
            MessengerInstance.Send<SendFaxMessage>(new SendFaxMessage { ChooseFile = true });
        }

        private void OnSendFile(SendFileMessage msg)
        {
            if (msg.SendFile == true)
            {
                handleSelfTyping(true);

                try
                {
                    Int64 localTs  = VxTimestamp.GetNowTimestamp();
                    Int64 serverTs = VxTimestamp.GetNowTimestamp();	//All times are UTC
                    Message message = new Message();

                    message.Status = Message.MsgStatus.NEW;
                    message.Direction = Message.MsgDirection.OUTBOUND;

                    message.MsgId = "UUID-" + localTs.ToString();		//TODO: XMPP Manager will assign this, as will SMS API call.  Not quite there yet.
                    message.Body = "";

                    message.FromDid = SessionManager.Instance.User.Did;
                    message.ToDid = PhoneNumber;

                    if (isSMSMode)
                    {
                        message.Type = Message.MsgType.SMS;
                    }
                    else
                    {
                        message.FromJid = SessionManager.Instance.User.XmppJid;
                        message.ToJid = otherJid;
                        message.Type = Message.MsgType.CHAT;
                    }

                    message.LocalTimestamp = localTs;
                    message.ServerTimestamp = new VxTimestamp( serverTs );

                    message.RichData.MediaFile = msg.File;
                    string ext = Path.GetExtension(msg.File).ToLower();
                    switch (ext)
                    {
                        case ".jpg":
                        case ".jpeg":
                            message.RichData.Type = RichData.RichDataType.IMAGE;
                            break;
                        case ".mpg":
                        case ".mpeg":
                            message.RichData.Type = RichData.RichDataType.VIDEO;
                            break;
                    }

                    string thumbnailFile = Path.Combine(Path.GetTempPath(), string.Format("{0}{1}", Guid.NewGuid(), Path.GetExtension(msg.File)));

                    if (ThumbnailCreator.ResizeImage(msg.File, thumbnailFile))
                    {
                        message.RichData.ThumbnailFile = thumbnailFile;
                    }

                    MessageManager.getInstance().sendMessage(message);
                }
                catch (Exception ex)
                {
                    logger.Error("OnSendFile", ex);
                }

                isSelfTyping = false;
            }
        }

        private void OnSendFax(SendFaxMessage msg)
        {
            if (msg.SendFile == true)
            {
                handleSelfTyping(true);

                try
                {
                    Int64 localTs  = VxTimestamp.GetNowTimestamp();
                    Int64 serverTs = VxTimestamp.GetNowTimestamp();	//All times are UTC
                    Message message = new Message();

                    message.Status = Message.MsgStatus.NEW;
                    message.Direction = Message.MsgDirection.OUTBOUND;

                    message.MsgId = "UUID-" + localTs.ToString();		//TODO: XMPP Manager will assign this, as will SMS API call.  Not quite there yet.
                    message.Body = "";

                    message.FromDid = SessionManager.Instance.User.Did;
                    message.ToDid = PhoneNumber;

                    message.FromJid = SessionManager.Instance.User.XmppJid;
                    message.ToJid = otherJid;
                    message.Type = Message.MsgType.FAX;

                    message.LocalTimestamp  = localTs;
                    message.ServerTimestamp = new VxTimestamp( serverTs );

                    message.RichData.MediaFile = msg.File;
                    message.RichData.Type = RichData.RichDataType.PDF;

                    MessageManager.getInstance().sendMessage(message);
                }
                catch (Exception ex)
                {
                    logger.Error("OnSendFax", ex);
                }

                isSelfTyping = false;
            }
        }

        private MessageStatus GetMessageStatus(Message.MsgStatus msgStatus)
        {
		//x NEW				=  0, 	// construct a new message (in-bound or out-bound)
		//x READ			=  1, 	// in-bound message already read or outbound XMPP message has been read by receiver (person)
		//x PENDING			=  2, 	// message is waiting to be sent
		//  PENDING_WIFI	=  3, 	// message is waiting be sent only on a WIFI
		//  INTRANSITION	=  4, 	// sending ..., message is in a transition phase
		//x SENT			=  5, 	// message sent successfully
		//x FAILED			=  6, 	// failed to send message (network failure?)
		//  RETRY			=  7, 	// user asks to re-send message that was failed to deliver
		//  UPLOADING		=  8,	// for RichData media
		//  DELETED			=  9,	// deleted locally, but not yet deleted on server
		//x DELIVERED		= 10, 	// message successfully received at remote end
		//  TRANSLATING		= 11,	// async API call should be active to retrieve translated value
		//  SCHEDULED		= 12,
		//  ALL				= 13,	//Had to change from ALL since we get compile 'redefinition' errors		
		//  UNKNOWN			= 14	//Catch-all in case we have invalid data in DB.	//Had to change from UNKNOWN since we get compile 'redefinition' errors		

			//TODO: set proper values for items not marked with 'x' above
			MessageStatus result = MessageStatus.New;

            switch (msgStatus)
            {
            case Message.MsgStatus.NEW:
				result = MessageStatus.New;
				break;
            
			case Message.MsgStatus.SENT:
                result =  MessageStatus.Sent;
				break;

            case Message.MsgStatus.DELIVERED:
                result = MessageStatus.Delivered;
				break;

            case Message.MsgStatus.READ:
                result = MessageStatus.Read;
				break;

            case Message.MsgStatus.PENDING:	//TODO: this may not be correct from model layer
            case Message.MsgStatus.FAILED:
                result = MessageStatus.Failed;
				break;

            case Message.MsgStatus.RETRY:
                result = MessageStatus.Retry;
				break;
            }

            return result;
        }

        private void OnDbChange(object sender, DbChangedEventArgs e)
        {
			if (e.Data.isMsgTable())
			{
				DispatcherHelper.CheckBeginInvokeOnUI(() => { LoadData(); });
			}
        }

        private void OnTyping(XmppTypingNotificationMessage msg)
        {
            String cleanJid = Util.Jid.getCleanJid(msg.Jid);
            if (cleanJid == otherJid)
            {
                if (msg.IsComposing)
                    DispatcherHelper.CheckBeginInvokeOnUI(() => { AddTypingIndicator(); });
                else
                    DispatcherHelper.CheckBeginInvokeOnUI(() => { RemoveTypingIndicator(); });
            }
        }

        private void OnBubbleColorChange(ChangeChatBubbleColorMessage obj)
        {
            var SenderChatBubbleColor	 = SettingsManager.Instance.User.MyChatBubbleColor;
            var ReceipentChatBubbleColor = SettingsManager.Instance.User.MyConversantChatBubbleColor;

            foreach (var item in colorsList)
            {
                if (SenderChatBubbleColor.ToLower().Trim().Equals(item.ColorName.ToLower().Trim()))
                    SenderBubbleColor = (Brush)(new BrushConverter().ConvertFrom(item.HexValue));

                if (ReceipentChatBubbleColor.ToLower().Trim().Equals(item.ColorName.ToLower().Trim()))
                    ReceipentBubbleColor = (Brush)(new BrushConverter().ConvertFrom(item.HexValue));
            }

            LoadData();
        }

		private void OnChangeContactMessageNumber( ChangeContactMessageNumberMessage msg )
		{
			PhoneNumber = msg.PhoneNumber;
			handlePhoneNumberChange();
		}

		private void OnReloadData( ReloadDataMessage msg )
		{
            LoadData();
		}
		
		//Called from ctor and OnChangeContactMessageNumber()
		private void handlePhoneNumberChange()
		{
			SessionManager.Instance.SetActiveChatInfo( CmGroup, PhoneNumber );

            if ( CmGroup == Contact.INVALID_CMGROUP && String.IsNullOrEmpty(PhoneNumber) )
            {
//                int xxx = 1;	//We get here with incoming chat from someone not in our AB.  Need to handle this in model layer.
            }

            getPhoneNumberInfo();
		}

        private void LoadColors()
        {
            //List of available colors Reading from Colors.xml file
			//TODO: This should be in Model layer util class.
            var uri = new Uri(Path.Combine("pack://application:,,,/", LocalizedString.ChatBubblesColorsName));
            var colorsViewModelStream = Application.GetResourceStream(uri);
            var colors = new List<ChatBubblesColorViewModel>();
            XmlDocument doc = new XmlDocument();
            using (var stream = colorsViewModelStream.Stream)
            {
                doc.Load(stream);

                using (XmlNodeList nodeList = doc.SelectNodes("Colors/Color"))
                {
                    foreach (XmlNode node in nodeList)
                    {
                        colors.Add(new ChatBubblesColorViewModel()
                        {
                            ColorName = node.ChildNodes[0].InnerText,
                            HexValue = node.ChildNodes[1].InnerText
                        });
                    }
                }
            }
            colorsList = colors.ToList();
        }

        #endregion
    }	//public class MessagesViewModel


    public class ChangeChatBubbleColorMessage	{}
    public class ShowOrHideVoiceMailsMessage	{}
    public class MessagesModifiedMessage		{}
    public class MessageSentMessage				{}
    public class ChooseFileMessage              {}

    public class SendFileMessage
    {
        public bool   ChooseFile { get; set; }
        public bool   SendFile	 { get; set; }
        public string File		 { get; set; }
    }

    public class SendFaxMessage
    {
        public bool   ChooseFile { get; set; }
        public bool   SendFile   { get; set; }
        public string File       { get; set; }
    }

	public class VoicemailPlayClickedMessage
	{
        public string Timestamp { get; set; }
	}
}
