﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using GalaSoft.MvvmLight;			//ViewModelBase, Set

using System;						//String
using System.Reflection;			//Assembly

namespace Desktop.ViewModels.Help
{
    class AboutWindowViewModel : ViewModelBase
    {
        #region|private variables|

        private String currentVersion;
      
        #endregion

        #region|Constructor|

        public AboutWindowViewModel()
        {
           var  appVer = Assembly.GetExecutingAssembly().GetName().Version;
           currentVersion = appVer.Major + "." + appVer.Minor + "." + appVer.Build + " (" + appVer.Revision + ")";
        }

        #endregion

        #region|Public Properties|

        public String CurrentVersion
        {
            get { return currentVersion; }
            set { Set(ref this.currentVersion, value); }
        }

        #endregion
    }
}
