﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using GalaSoft.MvvmLight;	//ViewModelBase, MessengerInstance, Set

namespace Desktop.ViewModels.Calls
{
    public class CallHistoryItemViewModel : ViewModelBase
    {
        #region Private Class Variables

        private string	mName;
        private string	mHistoryType;
        private string	mDuration;
        private string	mHistoryDate;
        private string	mNumber;
        private string	mNumberType;
        private string	mTranscription;
        private string	mAudioSource;
        private string	mHistoryMonth;
        private string	mHistoryDay;
		private string	mMsgTimestamp;

        private CallDirection	mCallDirection;
        private CallType		mCallType;

        #endregion

        #region | Constructors |

        public CallHistoryItemViewModel()
        {
        }

        #endregion

        #region | Public Properties |

        public string Name
        {
            get { return mName; }
            set { Set(ref mName, value); }
        }

        public string HistoryType
        {
            get { return mHistoryType; }
            set { Set(ref mHistoryType, value); }
        }

        public string Duration
        {
            get { return mDuration; }
            set { Set(ref mDuration, value); }
        }

        public string HistoryDate
        {
            get { return mHistoryDate; }
            set { Set(ref mHistoryDate, value); }
        }

        public string Number
        {
            get { return mNumber; }
            set { Set(ref mNumber, value); }
        }

        public string NumberType
        {
            get { return mNumberType; }
            set { Set(ref mNumberType, value); }
        }

        public string Transcription
        {
            get { return mTranscription; }
            set { Set(ref mTranscription, value); }
        }

        public string AudioSource
        {
            get { return mAudioSource; }
            set { Set(ref mAudioSource, value); }
        }

        public string MsgTimestamp		//TODO: Should be 'long'
        {
            get { return mMsgTimestamp; }
            set { Set(ref mMsgTimestamp, value); }
        }

        public string HistoryMonth
        {
            get { return mHistoryMonth; }
            set { Set(ref mHistoryMonth, value); }
        }

        public string HistoryDay
        {
            get { return mHistoryDay; }
            set { Set(ref mHistoryDay, value); }
        }

        public CallDirection CallDirection
        {
            get { return mCallDirection; }
            set { Set(ref mCallDirection, value); }
        }

        public CallType CallType
        {
            get { return mCallType; }
            set { Set(ref mCallType, value); }
        }

        public bool ShowAudioPlayer
        {
            get { return !string.IsNullOrEmpty( mAudioSource ); }
        }

        public bool ShowDateBubble
        {
            get { return !string.IsNullOrEmpty( mHistoryDay ); }
        }

        #endregion
    }
}
