﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;				//SessionManager
using Desktop.Model.Contact;		//ContactManager
using Desktop.Model.Messaging;		//MessageManager
using Desktop.Util;					//PhoneUtils, TimeUtils

using ManagedDataTypes;				//Contact, Message, RecentCall

using Vxapi23;						//RestfulApiManager

using GalaSoft.MvvmLight;			 //ViewModelBase, MessengerInstance, Set
using GalaSoft.MvvmLight.CommandWpf; //RelayCommand

using System;						//DateTime, Convert
using System.Windows.Input;			//ICommand

namespace Desktop.ViewModels.Calls
{
    public class CallDetailViewModel : ViewModelBase
    {
        #region Private Class Variables

		//Used in Properties
        private CallHistoryItemViewModel mCallHistoryItemVM;
        private ViewModelBase			 mCallHistoryPane;
 
		private	int		mRecordId;
        private string	mName;
        private bool	mShowCallHistory;
        private bool	mIsVMRC;

		//Not used in properties
        private int		mCmGroup;			//ctor
        private string	mPhoneNumber;		//LoadData()
		private long	mVcmrcTimestamp;	//LoadData()

        #endregion

        #region Constructors

        public CallDetailViewModel( int recordId, CallType callType, long timeStamp, string nameOrNumber, string pn, int cmGroup )
        {
            MessengerInstance.Register<CallHistoryMessage>( this, CloseCallHistory );

            CallHistoryCommand	= new RelayCommand( OnCallHistory, () => { return true; } );
            CallCommand			= new RelayCommand( onCallCommand, () => { return true; } );
            DeleteCommand		= new RelayCommand( OnDelete,      () => { return true; } );

            ShowCallHistory = false;
            mCmGroup		= cmGroup;

            LoadData( recordId, callType, timeStamp, nameOrNumber, pn);
        }

        public override void Cleanup()
        {
            base.Cleanup();

            mShowCallHistory	= false;
            CallHistoryItemVM	= null;
            CallHistoryPane		= null;
        }

        #endregion

        #region Public Properties

        public ICommand CallHistoryCommand	{ get; set; }
        public ICommand CallCommand			{ get; set; }
        public ICommand DeleteCommand		{ get; set; }

        public CallHistoryItemViewModel CallHistoryItemVM
        {
            get { return mCallHistoryItemVM; }
            set { Set(ref mCallHistoryItemVM, value); }
        }

        public ViewModelBase CallHistoryPane
        {
            get { return mCallHistoryPane; }
            set { Set(ref mCallHistoryPane, value); }
        }

        public int RecordId
        {
            get { return mRecordId; }
            set { Set(ref mRecordId, value); }
        }

        public string Name
        {
            get { return mName; }
            set { Set(ref mName, value); }
        }

        public bool ShowCallHistory
        {
            get { return mShowCallHistory; }
            set { Set(ref mShowCallHistory, value); }
        }

        public bool IsVMRC
        {
            get { return mIsVMRC; }
            set { Set(ref mIsVMRC, value); }
        }

        #endregion

        #region Private Procedures

        private void CloseCallHistory(CallHistoryMessage msg)
        {
            if (msg != null)
            {
                ShowCallHistory = false;
            }
        }

        private void OnCallHistory()
        {
            ShowCallHistory = true;
            CallHistoryPane = new CallHistoryViewModel( mPhoneNumber, mCmGroup );
        }

        private void LoadData( int recordId, CallType callType, long timeStamp, string nameOrNumber, string pn )
        {
            RecordId		= recordId;
            mVcmrcTimestamp = timeStamp;
       
			var		db		= SessionManager.Instance.UserDataStore;

            if ( callType == CallType.Voicemail || callType == CallType.Recorded )
            {
				String[] matchArray;
                Message  msg		= db.getMessage(timeStamp);

				if ( callType == CallType.Recorded )
				{
					mPhoneNumber = PhoneUtils.FormatPhoneNumber( msg.isInbound() ? msg.FromDid : msg.ToDid );
					matchArray = new string[] { mPhoneNumber };
				}
				else
				{ 
					mPhoneNumber = PhoneUtils.FormatPhoneNumber(pn);
					matchArray = new string[] { pn, nameOrNumber };	
				}

                Contact contact = ContactManager.getInstance().getMatchingContact( matchArray );

                Name = ContactManager.getBestDisplayName( contact, null, nameOrNumber );

                Message.ServerMsgType msgType = (msg.isVoicemail() ? Message.ServerMsgType.VOICEMAIL : Message.ServerMsgType.RECORDED_CALL);
                string audioSource = RestfulAPIManager.INSTANCE.formatDownloadFileUrl(SessionManager.Instance.User.UserKey, SessionManager.Instance.User.CompanyUserId, (int)msgType, msg.MsgId);

                DateTime vmTime = msg.ServerTimestamp.AsLocalTime();

                mCallHistoryItemVM = new CallHistoryItemViewModel();

                mCallHistoryItemVM.MsgTimestamp	 = timeStamp.ToString();
                mCallHistoryItemVM.Name			 = Name;
                mCallHistoryItemVM.CallDirection = msg.isInbound()   ? CallDirection.Incoming : CallDirection.Outgoing;
                mCallHistoryItemVM.CallType		 = msg.isVoicemail() ? CallType.Voicemail : CallType.Recorded;
                mCallHistoryItemVM.HistoryType	 = msg.isVoicemail() ? LocalizedString.MiddlePane_Calls_Text_Voicemail : LocalizedString.MiddlePane_Calls_Text_Recorded;
                mCallHistoryItemVM.Duration		 = TimeUtils.FormatDuration(msg.Duration);
                mCallHistoryItemVM.HistoryDate	 = TimeUtils.GetDateWithLongFormatting(vmTime);
                mCallHistoryItemVM.HistoryMonth	 = vmTime.ToString("MMM");
                mCallHistoryItemVM.HistoryDay	 = vmTime.ToString("dd");
                mCallHistoryItemVM.Number		 = mPhoneNumber;
				mCallHistoryItemVM.NumberType	 = ContactManager.getBestNumberLabel( contact, null );
                mCallHistoryItemVM.AudioSource	 = audioSource;
                mCallHistoryItemVM.Transcription = msg.Body;

                IsVMRC = true;
            }
            else
            {
                RecentCall	call	  = db.getRecentCall( RecordId );
                Contact		contact   = ContactManager.getInstance().getMatchingContact(new string[] { call.OrigNumber, call.Number, pn, nameOrNumber });
                DateTime	startTime = call.StartTime.AsLocalTime();

                mPhoneNumber = PhoneUtils.FormatPhoneNumber(call.OrigNumber);
				Name = ContactManager.getBestDisplayName( contact, call, null );
           
				mCallHistoryItemVM = new CallHistoryItemViewModel();

                mCallHistoryItemVM.Name			 = formatNameDisplayString( call );
                mCallHistoryItemVM.CallDirection = call.Incoming ? CallDirection.Incoming : CallDirection.Outgoing;
                mCallHistoryItemVM.CallType		 = call.isMissedEx() ? CallType.Missed : CallType.Normal;
                mCallHistoryItemVM.HistoryType	 = call.isMissedEx() ? LocalizedString.MiddlePane_Calls_Text_MissedCall : LocalizedString.MiddlePane_Calls_Text_BrandName;
                mCallHistoryItemVM.HistoryDate	 = TimeUtils.GetDateWithLongFormatting(startTime);
                mCallHistoryItemVM.HistoryMonth	 = startTime.ToString("MMM");
                mCallHistoryItemVM.HistoryDay	 = startTime.ToString("dd");
                mCallHistoryItemVM.Number		 = mPhoneNumber;
				mCallHistoryItemVM.NumberType	 = ContactManager.getBestNumberLabel( contact, call );
                mCallHistoryItemVM.AudioSource	 = "";

                if ( !call.isMissedEx() )
                    mCallHistoryItemVM.Duration = TimeUtils.FormatDuration(call.Duration);
            }
        }

		private String formatNameDisplayString( RecentCall rc )
		{
			String typeText = rc.isMissedEx() ? LocalizedString.MiddlePane_Calls_Text_MissedCall : LocalizedString.GenericCall;
			String toFrom   = rc.Incoming     ? LocalizedString.GenericFrom : LocalizedString.GenericTo;

			String result = String.Format("{0} {1} {2}", typeText, toFrom, Name);

			return result;
		}

        private void onCallCommand()
        {
			var error = ActionManager.Instance.MakeCall( mPhoneNumber );
        }

        private void OnDelete()
        {
            if ( mVcmrcTimestamp > 0 )
			{
				var		db  = SessionManager.Instance.UserDataStore;
				Message msg = db.getMessage( mVcmrcTimestamp );

				if ( msg != null && string.IsNullOrWhiteSpace(msg.MsgId) == false )
				{
					MessageList list = new MessageList();
					list.Add(msg);

					MessageManager.getInstance().deleteMessages(list);
				}
			}
        }

        #endregion
    }
}
