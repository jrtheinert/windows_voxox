﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using GalaSoft.MvvmLight;		//ViewModelBase, MessengerInstance, Set

namespace Desktop.ViewModels.Calls
{
    public class CallsMainViewModel : ViewModelBase
    {
        private CallListViewModel callListViewModel;
        private CallDetailViewModel callDetailViewModel;

        public CallsMainViewModel()
        {
            MessengerInstance.Register<ShowCallDetailMessage>(this, OnShowCallDetail);

            CallList = new CallListViewModel();
        }

        public override void Cleanup()
        {
            base.Cleanup();

            MessengerInstance.Unregister<ShowCallDetailMessage>(this, OnShowCallDetail);

            if(CallList != null)
            {
                CallList.Cleanup();
                CallList = null;
            }

            if (CallDetail != null)
            {
                CallDetail.Cleanup();
                CallDetail = null;
            }
        }

        public CallListViewModel CallList
        {
            get { return callListViewModel; }
            set { Set(ref callListViewModel, value); }
        }

        public CallDetailViewModel CallDetail
        {
            get { return callDetailViewModel; }
            set { Set(ref callDetailViewModel, value); }
        }

		public void markItemsRead()
		{
			if ( callListViewModel != null )
			{
				callListViewModel.markItemsRead();
			}
		}

        public void OnShowCallDetail(ShowCallDetailMessage msg)
        {
            if (CallDetail != null)
            {
                bool loadNewData = false;

                if (msg.CallType == CallType.Voicemail || msg.CallType == CallType.Recorded) // TODO: Temp for demo
                {
                    loadNewData = true;
                }
                else if (CallDetail.RecordId != msg.RecordId )
                {
                    loadNewData = true;
                }
				else if( msg.RecordId == -1)
                {
                    loadNewData = false;
                    CallDetail = null;
                }

                if (loadNewData)
                {
                    var existing = CallDetail;
                    CallDetail = new CallDetailViewModel( msg.RecordId, msg.CallType, msg.TimeStamp, msg.NameOrNumber, msg.PhoneNumber, msg.CmGroup );

                    existing.Cleanup();
                    existing = null;
                }
            }
            else if (CallDetail == null)
            {
                if ( msg.RecordId != -1 )
					CallDetail = new CallDetailViewModel( msg.RecordId, msg.CallType, msg.TimeStamp, msg.NameOrNumber, msg.PhoneNumber, msg.CmGroup );
            }
        }
    }
}
