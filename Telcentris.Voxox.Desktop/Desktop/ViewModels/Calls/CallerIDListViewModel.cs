﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;					//SessionManager
using Desktop.Util;						//PhoneUtils
using Desktop.ViewModels.Messages;		//ChooseANumberViewModel

using ManagedDataTypes;					//PhoneNumber

using GalaSoft.MvvmLight;				//ViewModelBase, MessengerInstance, Set
using GalaSoft.MvvmLight.Threading;     //DispatcherHelper

using System.Collections.Generic;		//List
using System.Collections.ObjectModel;	//ObservableCollection
using System.Linq;						//<query>

//NOTE: I changed how this is handled.  
//	Previously, EVERY entry in Call History list had theCallIdListView created and populated.
//	This was true even though MOST of those widgets were not needed because contact has a single phone number.
//	This was a huge waste of resources, slowed app response when populating/scrolling Call History and
//	made handling of CallerID control overly complex.
//This implementation creates a single CallerId control, destroying it when no longer needed and
//	simplifying the message handling for opening and closing the control.
//Associate CallListControl.xaml file has also been updated to REMOVE use of this class.

namespace Desktop.ViewModels.Calls
{
    class CallerIDListViewModel : ViewModelBase
    {

        #region Private variables

        private int									selectedIndex;
		private Desktop.Views.Calls.CallerIDControl mControl;
        private string                              phoneNumberToCall;

        #endregion

        #region Constructor

        public CallerIDListViewModel()
        {
            PhoneNumbers	= new ObservableCollection<ChooseANumberViewModel>();
            selectedIndex	= -1;
        }

        #endregion

        #region Public methods and properties

        public ObservableCollection<ChooseANumberViewModel> PhoneNumbers { get; set; }

		public Desktop.Views.Calls.CallerIDControl ViewControl 
		{ 
			get { return mControl; }
			set { mControl = value; }
		}

        public int SelectedIndex
        {
            get { return selectedIndex; }
            set { 
                Set(ref selectedIndex, value);
                if (selectedIndex != -1 && selectedIndex < PhoneNumbers.Count)
                {
                    MakeCall();
                }
                MessengerInstance.Send<CloseCallerIDMessage>(new CloseCallerIDMessage());
            }
        }

        #endregion

        #region Public and Private methods

        public void Populate( string phoneNumber, int cmGroup )
        {
            phoneNumberToCall = phoneNumber;

            PhoneNumbers.Clear();           

            // Show the User's CallerIDs
            foreach(var rmm in SessionManager.Instance.User.CallerIDs)
            {
                PhoneNumbers.Add(new ChooseANumberViewModel()
                {
                    PhoneNumber = PhoneUtils.FormatPhoneNumber(rmm.Number),
                    Label = rmm.Name
                });
            }
        }

        private void MakeCall()
        {
            // Get the selected CallerID
            var selectedNumber = PhoneNumbers[selectedIndex];

            if(selectedNumber != null && string.IsNullOrWhiteSpace(phoneNumberToCall) == false)
            {
                System.Threading.Tasks.Task.Run(async () =>
                {
                    // Set the Selected CallerID through VX API (only for this time)
                    await Model.Calling.CallManager.getInstance().setCallerId( selectedNumber.PhoneNumber, true);

                    DispatcherHelper.CheckBeginInvokeOnUI(() =>
                    {
                        // Now make the actual call
						ActionManager.Instance.MakeCall( phoneNumberToCall );
                    });
                });
            }
        }

        #endregion
    }

    public class CloseCallerIDMessage { }
}
