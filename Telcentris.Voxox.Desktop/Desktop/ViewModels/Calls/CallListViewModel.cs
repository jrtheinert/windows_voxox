﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;				//AvatarManager, SessionManager
using Desktop.Model.Calling;		//CallManager
using Desktop.Model.Calls;			//CallFilterType
using Desktop.Model.Contact;		//ContactManager
using Desktop.Model.Messaging;		//MessageManager - To mark VMs as READ.
using Desktop.Util;					//PhoneUtils, TimeUtils
using Desktop.Utils;				//VXProfiler

using ManagedDataTypes;				//RecentCallList, UmwMessageList
using ManagedDbManager;				//DbChangedEventHandler, DbChangeEventArgs

using GalaSoft.MvvmLight;			//ViewModelBase, MessengerInstance, Set
using GalaSoft.MvvmLight.Threading;	//DispatcherHelper

using System.Collections.Generic;	//List
using System.Linq;					//<query>

namespace Desktop.ViewModels.Calls
{
    /// <summary>
    /// The class is the backing model for Calls pane.
    /// TODO: This will call/consume the PAPI methods & objects
    /// </summary>
    public class CallListViewModel : ViewModelBase
    {
        #region | Private Class Variables |

        private CallFilterType filterCallsBy;
        private string searchCalls = "";
        private bool noSearchResultsAvailable;
        private string noSearchResultsAvailableText;
        private int allUnseenOrUnheardCount;
        private int unseenMissedCallCount;
        private int unheardVoicemailCount;
        private int selectedIndex = -1;

        private DbChangedEventHandler dbChangeHandler = null;

        #endregion

        #region | Constructors |

        public CallListViewModel()
        {
            Calls = new VxObservableCollection<CallItemViewModel>();

            LoadCalls( true, false );

            if (Calls.Count > 0)
            {
                SelectedIndex = 0;
            }

            dbChangeHandler = new DbChangedEventHandler(OnDbChange);
            getUserDbm().addDbChangeListener(dbChangeHandler);
        }

        public override void Cleanup()
        {
            if ( getUserDbm() != null && dbChangeHandler != null)
            {
                getUserDbm().removeDbChangeListener(dbChangeHandler);
                dbChangeHandler = null;
            }
        }

        ~CallListViewModel()
        {
            Cleanup();
        }

        #endregion

        #region | Public Properties |

        public VxObservableCollection<CallItemViewModel> Calls { get; set; }

        /// <summary>
        /// Filter calls based on CallFilterType
        /// </summary>
        public CallFilterType FilterCallsBy
        {
            get { return filterCallsBy; }
            set
            {
				bool changed = (filterCallsBy != value);	//Let's try to minimize reloading of data.

				if ( changed )
				{ 
					Set(ref filterCallsBy, value);

					LoadCalls( true, true );
				}
            }
        }

        /// <summary>
        /// Search calls
        /// </summary>
        public string SearchCalls
        {
            get { return searchCalls; }
            set
            {
                Set(ref searchCalls, value);

                LoadCalls( true, true );
            }
        }

        /// <summary>
        /// This is set to true when no calls are available
        /// </summary>
        public bool NoSearchResultsAvailable
        {
            get { return noSearchResultsAvailable; }
            set { Set(ref noSearchResultsAvailable, value); }
        }

        public string NoSearchResultsAvailableText
        {
            get { return noSearchResultsAvailableText; }
            set { Set(ref noSearchResultsAvailableText, value); }
        }

        /// <summary>
        /// All not seen notifications
        /// </summary>
        public int AllUnseenOrUnheardCount
        {
            get { return allUnseenOrUnheardCount; }
            set { Set(ref allUnseenOrUnheardCount, value); }
        }

        /// <summary>
        /// Missed Calls not seen count
        /// </summary>
        public int UnseenMissedCallCount
        {
            get { return unseenMissedCallCount; }
            set { Set(ref unseenMissedCallCount, value); }
        }

        /// <summary>
        /// Voice mail not seen count
        /// </summary>
        public int UnheardVoicemailCount
        {
            get { return unheardVoicemailCount; }
            set { Set(ref unheardVoicemailCount, value); }
        }

        public int SelectedIndex
        {
            get { return selectedIndex; }
            set
            {
                Set(ref selectedIndex, value);
                if (selectedIndex == -1)
				{
                    MessengerInstance.Send<ShowCallDetailMessage>(new ShowCallDetailMessage { RecordId = -1 });
				}
                else if (Calls.Count > 0 && selectedIndex < Calls.Count)
                {
                    var item = Calls[selectedIndex];
                    MessengerInstance.Send<ShowCallDetailMessage>(new ShowCallDetailMessage { RecordId = item.RecordId, CallType = item.CallType, TimeStamp = item.TimeStamp, NameOrNumber = item.NameOrNumber, PhoneNumber = item.PhoneNumber , CmGroup = item.CmGroup});
                }
               
            }
        }

        #endregion

		public void markItemsRead()
		{
			LoadCalls( true, true );
		}

        #region | Private Procedures |

		private ManagedDbManager.ManagedDbManager getUserDbm()
		{
			return SessionManager.Instance.UserDataStore;
		}

        /// <summary>
        /// Load Calls
        /// </summary>
        private void LoadCalls( bool recurseOk, bool shouldMarkItemsRead )
        {
            VXProfiler prof = new VXProfiler("CallList::LoadCalls");

            CallItemViewModel cim = null;
            List<CallItemViewModel> list = new List<CallItemViewModel>();

			//This is not efficient, but since we must mark as Read/Seen, both Calls AND Voicemails
			//	when this thread is entered, we need to get both lists no matter what the filter condition is.
			int            changeCount = 0;
            RecentCallList rclist      = getUserDbm().getRecentCalls(string.Empty);
            UmwMessageList umwMsgList  = getUserDbm().getVmAndRecordedCallMessages();

			//We want/need a positive action from user before we mark items as read.
			//	This covers condition where user has window open and gets a missed call.
			if ( shouldMarkItemsRead )
			{ 
				//Mark Calls SEEN, as needed.  If any are changed, restart LoadCalls()
				changeCount += CallManager.getInstance().markCallsSeenWhenViewed( rclist );

				//Mark Voicemails as Read
				changeCount += MessageManager.getInstance().markMessagesReadWhenViewed( umwMsgList );
			}

			if ( recurseOk && changeCount > 0 )
			{
				LoadCalls( false, false );		//Should only recurse one time, at most, but update may fail, so force no recursion.
				return;
			}

			//Get Unread/Unseen counts - regardless of filter
 			int tempAll		= 0;
			int tempMissed	= 0;
			int tempUnheard	= 0;

           foreach (var rc in rclist)
            {
                if ( rc.isValid() )
				{
					if ( rc.isMissedEx() )
					{
						if ( !rc.hasBeenSeen() )
						{
							tempMissed++;
						}
					}
				}
			}

			foreach( var msg in umwMsgList )
			{
				if ( msg.isUnreadVoicemail() )
				{
					tempUnheard++;
				}
			}

			tempAll = tempMissed + tempUnheard; 
			
			//Change property so UI is updated
			AllUnseenOrUnheardCount = tempAll;
			UnseenMissedCallCount	= tempMissed;
			UnheardVoicemailCount	= tempUnheard;
			//End Unseen/Unheard logic


            // All & Missed Call tabs show from Recent Calls
            if (filterCallsBy == CallFilterType.All || filterCallsBy == CallFilterType.Missed)
            {
                // Loop through the Calls history ordered by latest at top
                foreach (var rc in rclist.OrderByDescending(x => x.StartTime.AsLong() ) )		//TODO: this is handled by the Model layer.  Otherwise, every ViewModel entry needs to know this.
                {
                    if (rc.isValid() == false) // Ignore just added / running call
                        continue;

                    Contact contact = ContactManager.getInstance().getMatchingContact(new string[] { rc.Number, rc.OrigNumber, rc.DisplayNumber });

                    // increase the call count
                    cim = new CallItemViewModel();

                    cim.RecordId		= rc.RecordId;
					cim.NameOrNumber	= ContactManager.getBestDisplayName( contact, rc, null );
                    cim.CallDirection	= rc.Incoming ? CallDirection.Incoming : CallDirection.Outgoing;
                    cim.CallType		= rc.isMissedEx() ? CallType.Missed : CallType.Normal;
                    cim.PhoneNumber		= rc.OrigNumber;
                    cim.ContactKey		= rc.ContactKey;
                    cim.CmGroup			= rc.CmGroup;
                    cim.IsBlocked		= contact != null ? contact.BlockedCount > 0 : false;

                    if (rc.isMissedEx())
                    {
                        cim.StatusText = LocalizedString.MiddlePane_Calls_Text_MissedCall;
                    }
                    else
                    {
						string label = ContactManager.getBestNumberLabel( contact, rc );

						cim.StatusText = string.Format("{0} - {1}", label, TimeUtils.FormatDuration(rc.Duration));
                    }

                    cim.DateText = TimeUtils.GetDateWithFormatting( rc.StartTime.AsLocalTime() );
                    cim.AvatarUri = AvatarManager.Instance.GetUriForCalling( contact != null ? contact.ContactKey : string.Empty );

                    list.Add(cim);
                }
            }
            else // Voicemail & Recorded calls show from 
            {
                var orderedMsgList = umwMsgList.OrderByDescending(x => x.LocalTimestamp).ToList();
                
                foreach (var msg in orderedMsgList.OrderByDescending(x => x.ServerTimestamp))
                {
                    cim = new CallItemViewModel();
                    Contact contact = ContactManager.getInstance().getMatchingContact(new string[] { msg.Number, msg.bestPhoneNumber(), msg.Name });

                    cim.RecordId	  = -1;	//msg.CmGroup;		//VMs etc. are NOT RecentCall messages, so we just pass -1 for RecordId.  TODO: Test impact.
                    cim.NameOrNumber  = contact != null ? contact.Name : PhoneUtils.FormatPhoneNumber(msg.bestDisplayName());
                    cim.CallDirection = msg.Direction == Message.MsgDirection.INBOUND ? CallDirection.Incoming : CallDirection.Outgoing;
                    cim.CallType	  = msg.Type == Message.MsgType.VOICEMAIL ? CallType.Voicemail : CallType.Recorded;
                    cim.StatusText	  = string.Format("{0} - {1}",
                                                cim.CallType == CallType.Voicemail ? LocalizedString.MiddlePane_Calls_Text_Voicemail : LocalizedString.MiddlePane_Calls_Text_Recorded,
                                                TimeUtils.FormatDuration(msg.Duration));

                    cim.DateText	= TimeUtils.GetDateWithFormatting( msg.ServerTimestamp.AsLocalTime() );
                    cim.TimeStamp	= msg.LocalTimestamp;
                    cim.AvatarUri	= AvatarManager.Instance.GetUriForCalling( contact != null ? contact.ContactKey : msg.CmGroup.ToString() );
                    cim.PhoneNumber = msg.FromDid;

                    cim.IsVoicemailNotPlayed = msg.isUnreadVoicemail();		//Negative logic is BAD, but works better with XAML Visibility Binding

                    list.Add(cim);
                }
            }

            if (!string.IsNullOrWhiteSpace(searchCalls))
            {
                list = list.Where(x => (x.NameOrNumber.ToLower().Contains(searchCalls.ToLower()) == true) ||
                                        (PhoneUtils.GetPhoneNumberForFax(x.PhoneNumber).Contains(searchCalls) == true)).ToList();
            }

            // These filter calls can be merged in the above retrival code, but left it for now
            if (filterCallsBy == CallFilterType.All)
            {
                Calls.Clear();
                Calls.AddRange(list);
                SetSelectedIndex();
            }
            else if (filterCallsBy == CallFilterType.Missed)
            {
                var filteredList = list.Where(x => x.CallType == CallType.Missed).AsEnumerable();

                Calls.Clear();
                Calls.AddRange(filteredList);                
                SetSelectedIndex();
            }
            else if (filterCallsBy == CallFilterType.Voicemail)
            {
                var filteredList = list.Where(x => x.CallType == CallType.Voicemail).AsEnumerable();

                Calls.Clear();
                Calls.AddRange(filteredList);                
                SetSelectedIndex();
            }
            else if (filterCallsBy == CallFilterType.Recorded)
            {
                var filteredList = list.Where(x => x.CallType == CallType.Recorded).AsEnumerable();

                Calls.Clear();
                Calls.AddRange(filteredList);                
                SetSelectedIndex();
            }

            NoSearchResultsAvailable = (Calls.Count == 0);

            if (NoSearchResultsAvailable)
            {
                switch(FilterCallsBy)
                {
                    case CallFilterType.All :
                    case CallFilterType.Missed:
                        NoSearchResultsAvailableText = LocalizedString.MiddlePane_Calls_NoCalls;
                        break;
                    case CallFilterType.Voicemail:
                        NoSearchResultsAvailableText = LocalizedString.MiddlePane_Calls_NoVoicemails;
                        break;
                    case CallFilterType.Recorded:
                        NoSearchResultsAvailableText = LocalizedString.MiddlePane_Calls_NoRecordedCalls;
                        break;
                }
            }

            prof.Stop();
        }

        private void OnDbChange(object sender, DbChangedEventArgs e)
        {
            if (e.Data.isRecentCallTable() || e.Data.isMsgTable() || e.Data.isContactTable())
            {
                DispatcherHelper.CheckBeginInvokeOnUI(() =>
                {
                    LoadCalls( true, false );
                });
            }
        }

        private void SetSelectedIndex()
        {
            if (Calls.Count > 0)
                SelectedIndex = 0;
            else
                SelectedIndex = -1;
        }

        #endregion
    }

    /// <summary>
    /// This message is raised when a call is selected to show call details
    /// </summary>
    public class ShowCallDetailMessage
    {
		public int		RecordId		{ get; set; }
        public CallType CallType		{ get; set; }
        public long		TimeStamp		{ get; set; }
        public string	NameOrNumber	{ get; set; }
        public string	PhoneNumber		{ get; set; }
        public int		CmGroup			{ get; set; }
    }
}
