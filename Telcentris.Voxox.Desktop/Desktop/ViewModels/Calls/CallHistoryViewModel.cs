﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;					//AvatarManager, SessionManager
using Desktop.Model.Contact;			//ContactManager
using Desktop.Util;						//PhoneUtils, TimeUtils
using Desktop.ViewModels.Contacts;		//ContactNumberViewModel

using ManagedDataTypes;					//Contact, PhoneNumber, RecentCall, RecentCallList

using GalaSoft.MvvmLight;				//ViewModelBase, MessengerInstance, Set
using GalaSoft.MvvmLight.CommandWpf;	//RelayCommand

using System;							//DateTime
using System.Collections.ObjectModel;	//ObservableCollection
using System.Linq;						//<query>
using System.Windows.Input;				//ICommand

namespace Desktop.ViewModels.Calls
{
    public class CallHistoryViewModel : ViewModelBase
    {
        #region | Private Class Variables |

        private string mPhoneNumber;
        private string mSelectedCallPhone;

        #endregion

        #region | Constructors |

        public CallHistoryViewModel( string phoneNumber, int cmGroup )
        {
            CallHistoryCloseCommand = new RelayCommand(OnCallHistoryClose, () => { return true; });
            CallCommand				= new RelayCommand(onCallCommand);

            CallHistoryItems		= new ObservableCollection<CallHistoryItemViewModel>();

            LoadData( phoneNumber,cmGroup );
        }

        #endregion

        #region | Public Properties |

        public string Name					{ get; set; }
        public string AvatarUri				{ get; set; }
        public bool   HasAdditionalNumbers	{ get; set; }

        public string SelectedCallPhone
        {
            get { return mSelectedCallPhone; }
            set
            {
                Set(ref mSelectedCallPhone, value);

                var error = ActionManager.Instance.MakeCall( mSelectedCallPhone );
            }
        }

        public ObservableCollection<CallHistoryItemViewModel>	CallHistoryItems	{ get; set; }
        public ObservableCollection<ContactNumberViewModel>		ContactNumbers		{ get; set; }

        public ICommand CallHistoryCloseCommand		{ get; set; }
        public ICommand CallCommand					{ get; set; }

        #endregion

        #region | Private Procedures |

        private void OnCallHistoryClose()
        {
            MessengerInstance.Send<CallHistoryMessage>(new CallHistoryMessage());
        }

        private void LoadData( string phoneNumber, int cmGroup )
        {
            var db = SessionManager.Instance.UserDataStore;
            RecentCallList calls = db.getRecentCalls(cmGroup);

            if ( calls.Count == 0 )
                calls = db.getRecentCalls(phoneNumber);

            if (calls != null && calls.Count > 0)
            {
                RecentCall recentCall = calls[0];
                Contact    contact    = ContactManager.getInstance().getMatchingContact(new string[] { recentCall.Number, recentCall.OrigNumber, recentCall.DisplayNumber });

                Name		 = ContactManager.getBestDisplayName( contact, recentCall, null );
                AvatarUri	 = contact != null ? AvatarManager.Instance.GetUriForContact( contact.ContactKey ) : AvatarManager.Instance.GetUriForCalling( string.Empty );
                mPhoneNumber = PhoneUtils.FormatPhoneNumber(phoneNumber);

                bool   showDate     = true;
                string lastDateTime = "";

                foreach (var call in calls.OrderByDescending(x => x.StartTime.AsLong() ) )		//TODO: We should get proper ordering from DB API call.
                {
                    contact = ContactManager.getInstance().getMatchingContact(new string[] { call.Number, call.OrigNumber, call.DisplayNumber });
                    DateTime callDate = call.StartTime.AsLocalTime();

                    if (lastDateTime != TimeUtils.GetOnlyFormattedDate(callDate))
                    {
                        lastDateTime = TimeUtils.GetOnlyFormattedDate(callDate);
                        showDate = true;
                    }
                    else
                    {
                        showDate = false;
                    }

                    var callHistoryItemVM = new CallHistoryItemViewModel();

                    callHistoryItemVM.Name			= string.Format("{0} {1} {2}", call.isMissedEx() ? LocalizedString.MiddlePane_Calls_Text_MissedCall : "Call", call.Incoming ? "from" : "to", Name);
                    callHistoryItemVM.CallDirection = call.Incoming ? CallDirection.Incoming : CallDirection.Outgoing;
                    callHistoryItemVM.CallType		= call.isMissedEx() ? CallType.Missed : CallType.Normal;
                    callHistoryItemVM.HistoryType	= call.isMissedEx() ? LocalizedString.MiddlePane_Calls_Text_MissedCall : LocalizedString.MiddlePane_Calls_Text_BrandName;
                    callHistoryItemVM.Duration		= TimeUtils.FormatDuration(call.Duration);

                    callHistoryItemVM.HistoryDate	= TimeUtils.GetDateWithLongFormatting(callDate);
                    callHistoryItemVM.HistoryMonth	= callDate.ToString("MMM");
                    callHistoryItemVM.HistoryDay	= showDate ? callDate.ToString("dd") : string.Empty;

                    callHistoryItemVM.Number		= PhoneUtils.FormatPhoneNumber(call.OrigNumber);
					callHistoryItemVM.NumberType	= ContactManager.getBestNumberLabel( contact, call );
                    callHistoryItemVM.AudioSource	= string.Empty;

                    CallHistoryItems.Add(callHistoryItemVM);
                }

                //TODO: Load phone numbers from API
                var contactNumbers = new System.Collections.ObjectModel.ObservableCollection<ContactNumberViewModel>();

                ContactNumbers       = contactNumbers;
                HasAdditionalNumbers = false;

                PhoneNumber number = db.getPhoneNumber(phoneNumber, cmGroup);

                var list = db.getContactPhoneNumbersListForCmGroup(cmGroup, number.isVoxox());

                if (list.Count() > 1)
                {
                    HasAdditionalNumbers = true;
					ContactNumbers		 = ContactManager.PhoneNumberListToContactNumberViewModel( list );
                }
                else
                {
                    HasAdditionalNumbers = false;
                }
            }
            else
            {
                Contact contact = ContactManager.getInstance().getMatchingContact(new string[] { phoneNumber });

                Name		 = ContactManager.getBestDisplayName( contact, null, phoneNumber );
                AvatarUri	 = contact != null ? AvatarManager.Instance.GetUriForContact( contact.ContactKey ) : AvatarManager.Instance.GetUriForCalling( string.Empty );
                mPhoneNumber = PhoneUtils.FormatPhoneNumber(phoneNumber);
            }
        }

        private void onCallCommand()
        {
            var error = ActionManager.Instance.MakeCall( mPhoneNumber );
        }

        #endregion
    }

    public class CallHistoryMessage
    {
    }
}
