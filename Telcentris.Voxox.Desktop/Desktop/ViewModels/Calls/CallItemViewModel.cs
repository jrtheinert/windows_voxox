﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;					//SessionManager
using Desktop.Util;						//PhoneUtils
using Desktop.Controls;					//NonTopmostPopup

using GalaSoft.MvvmLight;				//ViewModelBase, MessengerInstance, Set
using GalaSoft.MvvmLight.CommandWpf;	//RelayCommand

using System.Windows.Input;				//ICommand

namespace Desktop.ViewModels.Calls
{
    /// <summary>
    /// Call Direction Enumeration
    /// </summary>
    public enum CallDirection
    {
        Incoming,
        Outgoing
    }

    /// <summary>
    /// Call type enumeration
    /// </summary>
    public enum CallType
    {
        Normal,
        Missed,
        Voicemail,
        Recorded
    }

    /// <summary>
    /// This class provides display properties & actions for each "Call" item in the Calls list.
    /// Setting of the properties is taken care from the listing model
    /// </summary>
    public class CallItemViewModel : ViewModelBase
    {
        #region | Private Class Variables |

		private	int				recordId;
        private string			nameOrNumber;
        private CallDirection	callDirection;
        private CallType		callType;
        private string			statusText;
        private string			dateText;
        private string			avatarUri;
        private bool			isVoicemailNotPlayed;
		private bool			shouldShowCallButton;
        private bool            isBlocked;

		private NonTopmostPopup	mPopup;
		private bool			mIsMouseOver;

        #endregion

        public CallItemViewModel()
        {
            MakeCallCommand		 = new RelayCommand(OnMakeCall);
            ShowCallerIDsCommand = new RelayCommand(OnShowCallerIds);

            MessengerInstance.Register<CloseCallerIDMessage>(this, CloseCallerIds);
        }

        #region | Public Properties |

        public int RecordId
        {
            get { return recordId; }
            set { Set(ref recordId, value); }
        }

        /// <summary>
        /// Contact's Display Name or Phone Number
        /// </summary>
        public string NameOrNumber
        {
            get { return nameOrNumber; }
            set { Set(ref nameOrNumber, value); }
        }

        /// <summary>
        /// Call direction of this item
        /// </summary>
        public CallDirection CallDirection
        {
            get { return callDirection; }
            set { Set(ref callDirection, value); }
        }

        /// <summary>
        /// Call type of this item
        /// </summary>
        public CallType CallType
        {
            get { return callType; }
            set { Set(ref callType, value); }
        }

        /// <summary>
        /// Text that indicates the call status
        /// e.g., "Missed Call", "Voicemail M:SS"
        /// </summary>
        public string StatusText
        {
            get { return statusText; }
            set { Set(ref statusText, value); }
        }

        /// <summary>
        /// Text that indicates the call date
        /// e.g., "Today", "M/DD/YY"
        /// </summary>
        public string DateText
        {
            get { return dateText; }
            set { Set(ref dateText, value); }
        }

        /// <summary>
        /// Avatar Uri of the Contact
        /// </summary>
        public string AvatarUri
        {
            get { return avatarUri; }
            set { Set(ref avatarUri, value); }
        }

		/// <summary>
        /// This is for type Voicemail.
        /// Sets or gets the Voicemails Unread Symbol.
		/// Negative logic is BAD, but works better with XAML Visibility Binding
        /// </summary>
        public bool IsVoicemailNotPlayed
        {
            get { return isVoicemailNotPlayed; }
            set { Set(ref isVoicemailNotPlayed, value); }
        }

        public bool ShouldShowCallButton
        {
            get { return shouldShowCallButton; }
            set { Set(ref shouldShowCallButton, value); }
        }

		public void updateIsMouseOver( bool isMouseOver )
		{
			mIsMouseOver = isMouseOver;
			updateShowCallButton();
		}

        public bool IsBlocked
        {
            get { return isBlocked; }
            set { Set(ref isBlocked, value); }
        }

        public int		CmGroup			{ get; set; }
	    public long		TimeStamp			{ get; set; }
        public string	PhoneNumber			{ get; set; }
        public string	ContactKey			{ get; set; }

        public ICommand MakeCallCommand		 { get; set; }
        public ICommand ShowCallerIDsCommand { get; set; }

        #endregion


		#region Public Procedures

		public void displayContextMenu()
		{
//			int cmGroup = ( CmGroup == 0 ? Contact.INVALID_CMGROUP : CmGroup );	This should be fixed now.
			MenuManager.Instance.showHistoryContextMenu( ContactKey, PhoneNumber, CmGroup );
		}

		#endregion

		#region | Private Procedures |

		/// <summary>
        /// Directly dials the number 
        /// </summary>
        private void OnMakeCall()
        {
			ActionManager.Instance.MakeCall( PhoneNumber );
        }

        /// <summary>
        /// Displays the list of Caller IDs
        /// </summary>
        private void OnShowCallerIds()
        {
            // If the User has more than 1 CallerIDs then display the popup
            if(SessionManager.Instance.User.CallerIDs.Count > 1)
            {
                displayPopup();
            }
		}

        /// <summary>
        /// Hides the List of caller Ids
        /// </summary>
        /// <param name="msg"></param>
        private void CloseCallerIds(CloseCallerIDMessage msg)
        {
			closePopup();
        }

		private void updateShowCallButton()
		{
			ShouldShowCallButton = mIsMouseOver || (mPopup != null);
		}

		private void callIdControl_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
		{
			closePopup();
		}
	
		private void closePopup()
		{
			if ( mPopup != null )
			{ 
				mPopup.IsOpen = false;
				mPopup		  = null;
			}

			updateShowCallButton();
		}

		private void displayPopup()
		{
			//This will become child/content of the popup.
			Desktop.Views.Calls.CallerIDControl callerIdControl = new Desktop.Views.Calls.CallerIDControl();
			callerIdControl.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
			callerIdControl.VerticalAlignment   = System.Windows.VerticalAlignment.Stretch;

			//So we can close control when mouse leaves.
            callerIdControl.MouseLeave += new System.Windows.Input.MouseEventHandler( callIdControl_MouseLeave );

			//Populate the ViewModel
			CallerIDListViewModel tempVm = (CallerIDListViewModel)callerIdControl.DataContext;
			tempVm.Populate( PhoneNumber, CmGroup );

			mPopup					= new Desktop.Controls.NonTopmostPopup();
			mPopup.Placement		= System.Windows.Controls.Primitives.PlacementMode.MousePoint;
			mPopup.HorizontalOffset = -2;	//Force popup under cursor position, so we get proper handling with MouseLeave event.
			mPopup.VerticalOffset   = -2;

			mPopup.Child			  = callerIdControl;
			mPopup.Cursor			  = System.Windows.Input.Cursors.Arrow;
			mPopup.AllowsTransparency = true;
			mPopup.StaysOpen		  = false;
			mPopup.IsOpen			  = true;

			updateShowCallButton();
        }

        #endregion
    }
}
