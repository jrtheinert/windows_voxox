﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;			//SessionManager
using Desktop.Model.Calling;	//PhoneCall

using GalaSoft.MvvmLight;		//MessengerInstance, Set

using System;					//Int32

namespace Desktop.ViewModels.Calling
{
    public enum CallItemMode
    {
        Active,
        SubActive,
        Hold,
        Ended,
        Done
    }

    public abstract class CallItemViewModel : ViewModelBase
    {
        #region | Private Class Variables |

        private Int32 callId;
        private string name;
        private string avatarUri;
        private string status;
        private CallItemMode mode;
        private string mAnimationReferenceID = System.Guid.NewGuid().ToString();

        #endregion

        #region | Constructors |

        public CallItemViewModel()
        {
        }

        public override void Cleanup()
        {
            base.Cleanup();
        }

        #endregion

        #region | Public Properties |

        public Int32 CallId
        {
            get { return callId; }
            set { Set(ref callId, value); }
        }

        public string Name
        {
            get { return name; }
            set { Set(ref name, value); }
        }

        public string AvatarUri
        {
            get { return avatarUri; }
            set { Set(ref avatarUri, value); }
        }

        public string Status
        {
            get { return status; }
            set { Set(ref status, value); }
        }

        public CallItemMode Mode
        {
            get { return mode; }
            set
            {
                Set(ref mode, value);
                if (mode == CallItemMode.Ended)
                {
                    OnCallEnded();
                }
            }
        }

		public PhoneCall CallRef
        {
            get { return getPhoneCallRef( CallId ); }
            private set { }
        }

		public PhoneCall getPhoneCallRef( int callId )
		{
			PhoneCall result = null;

			result = SessionManager.Instance.GetPhoneCall( callId );

			return result;
		}

        public int SortOrder
        {
            get
            {
                switch (mode)
                {
                    case CallItemMode.Active:
                        return 2;
                    case CallItemMode.SubActive:
                        return 1;
                    case CallItemMode.Hold:
                        return 0;
                    case CallItemMode.Ended:
                        return 3;
                    case CallItemMode.Done:
                        return 4;
                    default:
                        return 5;
                }
            }
        }

        // For Animation
        public string AnimationReferenceID
        {
            get { return mAnimationReferenceID; }
        }

        #endregion

        #region | Protected Methods |

        protected virtual void OnCallEnded()
        {

        }

        #endregion
    }

    public class CallDoneMessage
    {
        public Int32 CallId { get; set; }
    }

}
