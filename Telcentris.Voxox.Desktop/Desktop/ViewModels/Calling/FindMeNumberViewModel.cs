﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using GalaSoft.MvvmLight;		//ViewModelBase, Set

namespace Desktop.ViewModels.Calling
{
    public class FindMeNumberViewModel : ViewModelBase
    {
        #region | Private Class Variables |

        private string name;
        private string phoneNumber;

        #endregion

        #region | Constructors |

        public FindMeNumberViewModel()
        {

        }

        #endregion

        #region | Public Properties |

        public string Name
        {
            get { return name; }
            set { Set(ref name, value); }
        }

        public string PhoneNumber
        {
            get { return phoneNumber; }
            set { Set(ref phoneNumber, value); }
        }

        #endregion
    }
}
