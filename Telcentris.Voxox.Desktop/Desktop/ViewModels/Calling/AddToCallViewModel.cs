﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;					//AvatarManager, ActionManager
using Desktop.Model.Contact;			//ContactManager
using Desktop.Util;						//PhoneUtils
using Desktop.Utils;					//VxProfiler
using Desktop.ViewModels.Contacts;		//ContactFilterType, ContactViewmodel, 

using ManagedDataTypes;					//PhoneNumber, Contact, ContactList, 

using GalaSoft.MvvmLight;				//ViewModelBase, Set, MessengerInstance
using GalaSoft.MvvmLight.CommandWpf;	//RelayCommand

using System;
using System.Collections.Generic;		//List
using System.Collections.ObjectModel;	//ObservableCollection
using System.Linq;						//Sort DB query results
using System.Windows.Input;				//ICommand

namespace Desktop.ViewModels.Calling
{
    public class AddToCallViewModel : ViewModelBase
    {
        #region | Private Class Variables |

		private ContactFilter	filterContactsBy;
        private String			searchContacts;
        private bool			noSearchResultsAvailable;
        private string			mAnimationReferenceID = System.Guid.NewGuid().ToString();
		private VXProfiler		renderingProfiler;

        #endregion

        #region | Constructors |

        public AddToCallViewModel()
        {
            Contacts = new VxObservableCollection<ContactViewModel>();

            LoadContacts();

            SelectContactCommand = new RelayCommand<String>(OnSelectContact);
            AddContactCommand    = new RelayCommand(OnAddContact);
            ContactsCloseCommand = new RelayCommand(OnClose);
        }

        #endregion

        #region | Public Properties |

        public VxObservableCollection<ContactViewModel> Contacts { get; set; }

        /// <summary>
        /// Filter contacts based on ContactFilterType setting
        /// </summary>
        public ManagedDataTypes.ContactFilter FilterContactsBy
        {
            get { return filterContactsBy; }
            set
            {
                Set(ref filterContactsBy, value);

                LoadContacts();
            }
        }

        /// <summary>
        /// Search contacts
        /// </summary>
        public String SearchContacts
        {
            get { return searchContacts; }
            set
            {
                Set(ref searchContacts, value);

                LoadContacts();
            }
        }

        public bool NoSearchResultsAvailable
        {
            get { return noSearchResultsAvailable; }
            set { Set(ref noSearchResultsAvailable, value); }
        }

		public String SelectedPhoneNumber { get; set; }

		public ICommand ContactsCloseCommand { get; set; }
        public ICommand SelectContactCommand { get; set; }
        public ICommand AddContactCommand	 { get; set; }

        // For Animation
        public string AnimationReferenceID
        {
            get { return mAnimationReferenceID; }
        }

        #endregion

        #region | Private Procedures |

        private void LoadContacts()
        {
            VXProfiler prof = new VXProfiler("AddToCallViewModel::LoadContacts");

            AddToCallContactViewModel		cvm   = null;
            List<AddToCallContactViewModel> list  = new List<AddToCallContactViewModel>();
            List<ContactViewModel>			list2 = new List<ContactViewModel>();	//We create a temporary 'final' list so we can use AddRange() for better UI performance.

            ContactFilter filter = filterContactsBy;

            Contacts.Clear();

            int prevCmGroup    = Contact.INVALID_CMGROUP;
            bool isSameContact = false;

			//We do NOT apply search filter here.  If we did, then we would have to requery DB for each keystroke.
            PhoneNumberList cl = ContactManager.getInstance().getContactPhoneNumbers( "", filter );

			prof.LogElapsedTime( "after CM.getContactPhoneNumbers()" );

			// Order all by Name & CmGroup with Voxox/InNetwork DID at top
            foreach ( PhoneNumber pn in cl.OrderBy(x => x.Name).ThenBy(x => x.CmGroup).ThenByDescending(x => x.isInNetwork() ).ThenBy(x => x.Label).ThenBy(x => x.Number)) 
            {
                isSameContact	= (prevCmGroup == pn.CmGroup);
                prevCmGroup		= pn.CmGroup;
                cvm				= fromContactModel( pn, isSameContact );

                list.Add(cvm);
            }

			prof.LogElapsedTime( "after create 'list'" );

			//Filter based on 'search criteria', by matching on Name or Number
			if ( !String.IsNullOrWhiteSpace( searchContacts ) )
			{
				list = list.Where(x => (x.NameOrNumber.ToLower().Contains(searchContacts.ToLower()) == true) ||
										(PhoneUtils.GetPhoneNumberForCall(x.PhoneNumber, String.IsNullOrEmpty(x.Extension) ).Contains(searchContacts) == true)).ToList();
			}

			prof.LogElapsedTime( "after filter 'list'" );

            //Apply 'list'
			if (list.Count > 0)
			{
				foreach (ContactViewModel item in list)
				{
					list2.Add( item );
				}
			}
			else
			{
				if (PhoneUtils.IsValidNumber(searchContacts))
				{
					cvm = fromContactModel( new PhoneNumber { Name = searchContacts, Number = searchContacts }, false);

					list2.Add( cvm );
				}
			}

			Contacts.AddRange( list2 );
			prof.LogElapsedTime( "after Contacts.Add()" );

			NoSearchResultsAvailable = (Contacts.Count == 0);

			prof.Stop();

			renderingProfiler = new VXProfiler( "AddToCallViewModel::LoadContacts - Rendering" );

			System.Windows.Application.Current.Dispatcher.BeginInvoke( new System.Action(RenderingDone), System.Windows.Threading.DispatcherPriority.ContextIdle, null );
        }

		private void RenderingDone()
		{
			renderingProfiler.Stop();
		}

        //Convert Contact Model to AddToCallContactViewModel
        private AddToCallContactViewModel fromContactModel( PhoneNumber pn, bool isSameContact )
        {
            AddToCallContactViewModel cvm = new AddToCallContactViewModel();

			bool   nameIsNumber = false;
			String name			= pn.bestDisplayName( out nameIsNumber );

			cvm.CmGroup				= pn.CmGroup;
            cvm.NameOrNumber		= (nameIsNumber ? PhoneUtils.FormatPhoneNumber( name ) : name);
            cvm.Label               = pn.isInNetwork() ? LocalizedString.AddToCall_Registered_Label : (string.IsNullOrWhiteSpace( pn.Label) ? LocalizedString.AddToCall_Empty_Label : pn.Label);
            cvm.IsRegisteredUser	= pn.isInNetwork();
			cvm.InNetworkImageUri	= Config.UI.InNetworkContact;
            cvm.PhoneNumber			= pn.Number;
            
            cvm.AvatarUri			= AvatarManager.Instance.GetUriForContact( pn.ContactKey );
            cvm.IsAdditionalNumber  = isSameContact;
            cvm.LastSeen			= pn.isInNetwork() ? "Last online on May 15" : "";			//TODO: WE do not get this data from DB yet.
            
            return cvm;
        }

        private void OnClose()
        {
            MessengerInstance.Send<AddToCallMessage>(new AddToCallMessage { Show = false });
        }

        private void OnSelectContact(String PhoneNumber)
        {
			SelectedPhoneNumber = PhoneNumber;
		}

        private void OnAddContact()
        {
			if ( ! String.IsNullOrEmpty( SelectedPhoneNumber ) )
			{ 
				var error = ActionManager.Instance.MakeCall( SelectedPhoneNumber );

				if (error == Model.Calling.PhoneLine.CallError.NoError)
				{
					OnClose();
				}
            }            
        }

        #endregion
    }
}
