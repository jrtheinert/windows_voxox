﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;					//ActionManager, SessionManager, NotifyHttpListenersHelper
using Desktop.Utils;					//Logger
using Desktop.Model.Calling;			//CallRecordingMessage
using Desktop.Model.Contact;			//ContactManager

using Telcentris.Voxox.ManagedSipAgentApi;						//SipAgentApi
using Telcentris.Voxox.ManagedSipAgentApi.ManagedSipDataTypes;	//PhoneCallState

using GalaSoft.MvvmLight.CommandWpf;	//RelayCommand
using GalaSoft.MvvmLight.Threading;		//DispatcherHelper

using System;							//TimeSpan, EventHandler, Exception, EventArgs
using System.Collections.ObjectModel;	//ObservableCollection
using System.Threading.Tasks;			//Task
using System.Windows.Input;				//ICommand
using System.Windows.Threading;			//DispatcherTimer

namespace Desktop.ViewModels.Calling
{
    public class CallInProgressViewModel : CallItemViewModel
    {
        #region | Private Class Variables |

        private static VXLogger logger = VXLoggingManager.INSTANCE.GetLogger("CallInProgressViewModel");

        private PhoneCallState.CallState callState;

        private DisplayMode		displayMode;
        private TimeSpan		callTime;
        private DispatcherTimer dispatcherTimer;			//To display call duration.
        private DispatcherTimer dispatcherCallEndTimer;		//We want a short delay before the window is closed.

        private bool	isRecording;
        private bool	isRecordEnabled = true;
        private bool	isMute;
        private bool	isTalking;
        private bool	isOutOfNetwork;
        private bool	isOutOfCredits;
        private bool	isFlipAvailable = false;
        private bool	isDialerOpened  = false;
        private bool	showMerge;
        private bool	enableAddingToCall;
        private bool	showFlip = false;
        private bool	isInConference = false;
//        private bool isTransferable;

		private string	callCost;
        private string	originalName   = "";
        private string	originalAvatar = "";

        #endregion

        #region | Constructors |

        public CallInProgressViewModel()
        {
            AddToCallCommand			= new RelayCommand( OnAddToCall);
            EndCallDoneCommand			= new RelayCommand( OnEndCallDone);
            HangUpCommand				= new RelayCommand( OnHangUp, () => { return true; });
            InviteCommand				= new RelayCommand( OnInvite);
            MakeActiveCallCommand		= new RelayCommand( OnMakeActiveCall, () => { return true; });
            MergeCallCommand			= new RelayCommand( OnMergeCall);
            MuteCommand					= new RelayCommand( OnMute, () => { return true; });
            RecordCommand				= new RelayCommand( OnRecord);
            TransferCallCommand			= new RelayCommand( OnTransferCall, () => { return true; });
            TransferCallToCommand		= new RelayCommand( OnTransferCallTo);
            TransferCallCancelCommand	= new RelayCommand( OnTransferCallCancel, () => { return true; });

            AudioCommand				= new RelayCommand( OnAudio, () => { return true; });
            DialPadCommand				= new RelayCommand( OnDialPad, () => { return true; });
            DisplayAudioCommand			= new RelayCommand( OnDisplayAudio );
            AudioOuputCancelCommand		= new RelayCommand( OnAudioOuputCancel, () => { return true; });
            BuyCreditsCommand			= new RelayCommand( OnBuyCredits);

            BluetoothDeviceCommand		= new RelayCommand( () => { MessengerInstance.Send<NotImplementedMessage>(new NotImplementedMessage()); });
            BuiltInOutputCommand		= new RelayCommand( () => { MessengerInstance.Send<NotImplementedMessage>(new NotImplementedMessage()); });

            dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += new EventHandler(OnDispatcherTimerTick);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 1);

            MessengerInstance.Register<CallsInQueueMessage>(this, onCallsInQueue);
            MessengerInstance.Register<CallRecordingMessage>(this, onRecordingMessage);
            MessengerInstance.Register<CallRecordingControlMessage>(this, onRecordingControlMessage);

            FindMeNumbers = new ObservableCollection<FindMeNumberViewModel>();

//            LoadFindMeNumbers();		//This was used to set IsTransferable, but user can Flip Call even without FindMeNumbers.

            EnableAddingToCall = true;

			//CompanyUserOptions
            IsRecordEnabled = SessionManager.Instance.User.Options.IsCallRecordingAvailable;
            IsFlipAvailable = SessionManager.Instance.User.Options.IsFlipAvailable;

            Participants    = new ObservableCollection<ConferenceParticipant>();
        }

        public override void Cleanup()
        {
            base.Cleanup();

            if (dispatcherTimer != null && dispatcherTimer.IsEnabled)
            {
                dispatcherTimer.Stop();
            }

            dispatcherTimer = null;

			//Call End timer
            if (dispatcherCallEndTimer != null )
            {
                dispatcherCallEndTimer.Stop();
            }

            dispatcherCallEndTimer = null;
        }

        #endregion

        #region | Public Properties |

        public PhoneCallState.CallState CallState
        {
            get { return callState; }
            set
            {
                Set(ref callState, value);

                DispatcherHelper.CheckBeginInvokeOnUI(() =>
                {
                    if (callState == PhoneCallState.CallState.Talking)
                    {
                        dispatcherTimer.Start();
                        IsTalking = true;

                        CheckNetworkContact(CallRef.PeerSipAddress.getUserName());
                    }

                    if (callState == PhoneCallState.CallState.Closed || callState == PhoneCallState.CallState.Missed || callState == PhoneCallState.CallState.Error)
                    {
                        if (dispatcherTimer != null)
                        {
                            dispatcherTimer.Stop();
                        }
                    }

                    if (callState == PhoneCallState.CallState.Error)
                    {
                        OnEndCallDone();
                    }
                });
            }
        }

        public TimeSpan CallTime
        {
            get { return callTime; }
            set { Set(ref callTime, value); }
        }

        public bool IsMute
        {
            get { return isMute; }
            set { Set(ref isMute, value); }
        }

        public bool IsRecordEnabled
        {
            get { return isRecordEnabled; }
            set { Set(ref isRecordEnabled, value); }
        }

        public bool IsRecording
        {
            get { return isRecording; }
            set { Set(ref isRecording, value); }
        }

        public DisplayMode DisplayMode
        {
            get { return displayMode; }
            set { Set(ref displayMode, value); }
        }

        public bool ShowMerge
        {
            get { return showMerge; }
            set { Set(ref showMerge, value); }
        }

        /// <summary>
        /// Needed because the PhoneCallState.CallState is a inner enum which is not supported in WPF
        /// </summary>
        public bool IsTalking
        {
            get { return isTalking; }
            set { Set(ref isTalking, value); }
        }

		//public bool IsTransferable
		//{
		//	get { return isTransferable; }
		//	set { Set(ref isTransferable, value); }
		//}

        public bool IsOutOfNetwork
        {
            get { return isOutOfNetwork; }
            set { Set(ref isOutOfNetwork, value); }
        }

        public bool IsOutOfCredits
        {
            get { return isOutOfCredits; }
            set { Set(ref isOutOfCredits, value); }
        }

        public string CallCost
        {
            get { return callCost; }
            set { Set(ref callCost, value); }
        }

        public bool ShowFlip
        {
            get { return showFlip; }
            set { Set(ref showFlip, value); }
        }

        public bool IsInConference
        {
            get { return isInConference; }
            set { Set(ref isInConference, value); }
        }

        public bool EnableAddingToCall
        {
            get { return enableAddingToCall; }
            set { Set(ref enableAddingToCall, value); }
        } 

        public bool IsIncomingCall { get; set; }

        public bool IsFlipAvailable
        {
            get { return isFlipAvailable; }
            set { Set(ref isFlipAvailable, value); }
        }
        
        public ObservableCollection<FindMeNumberViewModel> FindMeNumbers	{ get; set; }
        public ObservableCollection<ConferenceParticipant> Participants		{ get; set; }

        public ICommand HangUpCommand				{ get; set; }
        public ICommand MuteCommand					{ get; set; }
        public ICommand DialPadCommand				{ get; set; }
        public ICommand RecordCommand				{ get; set; }
        public ICommand AudioCommand				{ get; set; }
        public ICommand TransferCallCommand			{ get; set; }
        public ICommand AddToCallCommand			{ get; set; }
        public ICommand BuiltInOutputCommand		{ get; set; }
        public ICommand DisplayAudioCommand			{ get; set; }
        public ICommand BluetoothDeviceCommand		{ get; set; }
        public ICommand AudioOuputCancelCommand		{ get; set; }
        public ICommand TransferCallToCommand		{ get; set; }
        public ICommand TransferCallCancelCommand	{ get; set; }
        public ICommand MakeActiveCallCommand		{ get; set; }
        public ICommand MergeCallCommand			{ get; set; }
        public ICommand InviteCommand				{ get; set; }
        public ICommand BuyCreditsCommand			{ get; set; }
        public ICommand EndCallDoneCommand			{ get; set; }


        #endregion

        public void AddToConference(string name, string avatarUri)
        {
            if (Participants.Count == 0)
            {
                // Keep original call names & avatar
                originalName = Name;
                originalAvatar = AvatarUri;

                Participants.Add(new ConferenceParticipant { Name = Name, AvatarUri = AvatarUri }); // Add Existing 
            }

            Participants.Add(new ConferenceParticipant { Name = name, AvatarUri = avatarUri });
            UpdateCallDisplayName();
            IsInConference = true;
        }

        #region | Protected Procedures |

        protected override void OnCallEnded()
        {
            dispatcherTimer.Stop();

            if (IsRecording) // If the call is currently recording, then stop it. 
            {
                //We only raise the UI event to hide the UI as IsRecording is true in this function only when the remote end hangs up 
                // leading to SIPCallId being not being valid anymore and the VXAPI call in OnRecord() will not succeed
                MessengerInstance.Send<CallRecordingMessage>(new CallRecordingMessage { IsRecording = false });
            }

            if (IsIncomingCall == true)
            {
                MessengerInstance.Send<CallDoneMessage>(new CallDoneMessage { CallId = CallId });
            }
            else
            {
                if (IsOutOfNetwork)
                {
                    //TODO: Update properties with API
                    IsOutOfCredits = false;
                    CallCost = "";	//"$xx.xx";
                }
                else
                {
                    CallCost = "Free";
                }

				//Start timer to close window after 5 seconds.
				if ( dispatcherCallEndTimer == null )	//We do NOT want more than one of these.
				{ 
					dispatcherCallEndTimer = new DispatcherTimer();

					dispatcherCallEndTimer.Tick += new EventHandler( (object s, EventArgs e) => { OnEndCallDone(); } );
				
					dispatcherCallEndTimer.Interval = new TimeSpan(0, 0, 5);
					dispatcherCallEndTimer.Start();
				}
            }
        }

        #endregion

        #region | Private Procedures |

        private void OnHangUp()
        {
            if (IsRecording) // If the call is currently recording, then stop it
            {
                // Raise the UI event to hide the recording UI first because the VX API call might not succeed after hang up as both SIP call and VXAPI call are async.
                MessengerInstance.Send<CallRecordingMessage>(new CallRecordingMessage { IsRecording = false });
                
                // Toggle recording (i.e., stop recording)
                OnRecord();
            }

            SessionManager.Instance.CloseCall(CallId);
        }

        private async void OnMute()
        {
            IsMute = !isMute;

			SipAgentApi.getInstance().muteCall( CallId, isMute );
        }

        private void OnDialPad()
        {
            if ( isDialerOpened )	//TODO: Why do we need to retain this info here?  Isn't it (or shouldn't it) be known elsewhere?
            {
				ActionManager.Instance.CloseDialer();
            }
            else
            {
				ActionManager.Instance.ShowDialer( true, CallId, null,null );
            }

			isDialerOpened = ! isDialerOpened;
        }

        private async void OnRecord()
        {
			//OnHold();	//Test code.  Uncomment to test Hold functionality using Record button.

			IsRecordEnabled = false;
			bool success = false;

			try
			{
				success = await NotifyHttpListenersHelper.ToggleRecording(CallRef.HttpListenerId);
			}
			catch (Exception ex)
			{
				logger.Error("OnRecord", ex);
			}
        }

		//Test Code
		//private async void OnHold()
		//{
		//	bool newValue = (CallRef.getState() != PhoneCallState.CallState.Hold );

		//	if ( newValue )	//Put call on hold
		//		CallRef.hold();
		//	else
		//		CallRef.resume();	//We will never get here.

		//}

        private void OnAudio()
        {
            DisplayMode = DisplayMode.Audio;
        }

        private void OnTransferCall()
        {
            DisplayMode = DisplayMode.Transfer;
			//User can always FlipCall to another logged in device.
//			if (IsTransferable)
//			{
//				ShowFlip = true;
//				await NotifyHttpListenersHelper.BlastTransfer(CallRef.HttpListenerId);
//			}
//			else
//			{ 
//				ActionManager.ShowFindMeNumberNotSetupControl( true );
//			}
        }

        private void OnBuiltInOutput()
        {
        }

		private void OnDisplayAudio()
		{
			ActionManager.Instance.NavigateToSettings();
		}

        private void OnBluetoothDevice()
        {
        }

        private void OnAudioOuputCancel()
        {
            DisplayMode = DisplayMode.Normal;
        }

        private async void OnTransferCallTo()
        {
            await NotifyHttpListenersHelper.BlastTransfer(CallRef.HttpListenerId);
        }

        private void OnTransferCallCancel()
        {
            DisplayMode = DisplayMode.Normal;
        }

        private void OnMakeActiveCall()
        {
            MessengerInstance.Send<MakeActiveCallMessage>(new MakeActiveCallMessage { CallId = CallId, SipCallId = CallRef.SIPCallId, HttpListenerId = CallRef.HttpListenerId });
        }

        public void onCallsInQueue(CallsInQueueMessage msg)
        {
			//Logic for ShowMerge:
			//	- If call is already in a conference, it cannot be merged.
			//	- If conference call (topHoldCall in CallsViewModel) has reached its limit, it cannot merge.
			//		- NOTE: This limit is monitored server-side because client may not be aware of all
			//				contacts on call.  So do NOT bother with this limit check.  
			//				Server will issue an error in API call.
			//
			//Logic for EnableAddingToCall:
			//	- If conference call (topHoldCall in CallsViewModel) has reached its limit, it cannot Add to call.
			//
			//Notice that EnableAddingToCall is always true.  This controls the enabling of the Add/Merge icon in .xaml.
			//	TODO: Under what conditions will this be false?  Options FEATURE_DISABLED? which we do not get yet.

            if (msg.Count > 1)
            {
				//We have a limit on how many participants there can be.
				if ( IsInConference )
				{
					//Cannot be in two conferences at once.
					ShowMerge = false;
					EnableAddingToCall = true;
				}
                else
                {
					ShowMerge = true;
					EnableAddingToCall = true;
                }
            }
            else
            {
                ShowMerge = false;
                EnableAddingToCall = true;
            }
        }

        private void OnMergeCall()
        {
            MessengerInstance.Send<MergeCallMessage>(new MergeCallMessage { CallId = CallId, SIPCallId = CallRef.SIPCallId, HttpListenerId = CallRef.HttpListenerId });
        }

        private void onRecordingMessage(CallRecordingMessage msg)
        {
            IsRecording = msg.IsRecording;
            IsRecordEnabled = SessionManager.Instance.User.Options.IsCallRecordingAvailable;
        }

        private void onRecordingControlMessage(CallRecordingControlMessage msg)
        {
            OnRecord();
        }
        
        private void OnDispatcherTimerTick(object sender, EventArgs e)
        {
			long duration = SessionManager.Instance.GetCallDuration( CallId );
            CallTime = new TimeSpan( 0, 0, (int)duration );
        }

		//This was used to set IsTransferable, but user can Flip Call even without FindMeNumbers.
		//private void LoadFindMeNumbers()
		//{
		//	Task.Run(() =>
		//	{
		//		var numbers = Vxapi23.RestfulAPIManager.INSTANCE.GetFindMeNumbers(SessionManager.Instance.User.UserKey, SessionManager.Instance.User.CompanyUserId);

		//		DispatcherHelper.CheckBeginInvokeOnUI(() =>
		//		{
		//			if (numbers != null)
		//			{
		//				foreach ( var item in numbers.reachMeNumbers )
		//				{
		//					if ( item.isEnabled() )
		//					{ 
		//						FindMeNumbers.Add(new FindMeNumberViewModel() { Name = item.label, PhoneNumber = item.offnetNumber });
		//					}
		//				}
		//			}

		//			IsTransferable = FindMeNumbers.Count > 0;
		//		});
		//	});
		//}

        private void OnInvite()
        {
            MessengerInstance.Send<NotImplementedMessage>(new NotImplementedMessage());
        }

        private void OnBuyCredits()
        {
            MessengerInstance.Send<NotImplementedMessage>(new NotImplementedMessage());
        }

        private void OnEndCallDone()
        {
            if (dispatcherCallEndTimer != null && dispatcherCallEndTimer.IsEnabled)
            {
                dispatcherCallEndTimer.Stop();
            }

            MessengerInstance.Send<CallDoneMessage>(new CallDoneMessage { CallId = CallId });
        }

        private async void CheckNetworkContact(string PhoneNumber)
        {
			IsOutOfNetwork = await ContactManager.getInstance().isInNetwork( PhoneNumber );
        }

        private void OnAddToCall()
        {
            MessengerInstance.Send<AddToCallMessage>(new AddToCallMessage { Show = true });
        }

        private void UpdateCallDisplayName()
        {
            if (Participants.Count == 0)
            {
                Name = originalName;
            }
            else if (Participants.Count == 1)
            {
                Name = string.Format("{0} & {1}", Participants[0].Name, Participants[1].Name);
            }
            else
            {
                Name = LocalizedString.GenericConference;
            }
        }

        #endregion
    }

    public class ConferenceParticipant
    {
        public string Name { get; set; }

        public string AvatarUri { get; set; }
    }

    public enum DisplayMode
    {
        Normal,
        Audio,
        Transfer
    }
}
