﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;						//SessionManager, AvatarManager, NotifyHttpListenersHelper
using Desktop.Model.Contact;				//ContactManager
using Desktop.Model.Calling;				//PhoneCallMessage, CallRecordingMessage, PhoneCall
using Desktop.Utils;						//Logger
using Desktop.Util;
using Desktop.ViewModels.Dialer;			//ClearDialerTextMessage

using Vxapi23.Model;						//ConferenceCallHttpListenerResponse

using Telcentris.Voxox.ManagedSipAgentApi;	//SipAgentApi
using Telcentris.Voxox.ManagedSipAgentApi.ManagedSipDataTypes;	//PhoneCallState

using GalaSoft.MvvmLight;					//MessengerInstance, ViewModelBase, Set
using GalaSoft.MvvmLight.CommandWpf;		//RelayCommand
using GalaSoft.MvvmLight.Threading;			//DispatcherHelper

using System;
using System.Collections.Generic;			//List
using System.Collections.ObjectModel;		//ObservableCollection
using System.Linq;							//Query
using System.Windows.Input;					//ICommand
using System.Windows.Threading;				//DispatcherTimer
using System.Windows.Forms;					//MessageBox, MessageBoxButtons, MessageBoxIcon

namespace Desktop.ViewModels.Calling
{
	#region Calling Messages

	public class MakeActiveCallMessage
    {
        public Int32  CallId		 { get; set; }
		public String SipCallId		 { get; set; }
		public String HttpListenerId { get; set; }
    }

    public class MergeCallMessage
    {
        public Int32  CallId		 { get; set; }
        public string SIPCallId		 { get; set; }
		public String HttpListenerId { get; set; }
    }

    public class CallsInQueueMessage
    {
        public int Count { get; set; }
    }

    public class CallsDoneMessage
    {
    }

    public class EndAndAcceptCallMessage
    {
        public Int32 CallId { get; set; }
    }

    public class CallRecordingControlMessage
    {
        public bool Stop { get; set; }
    }

    public class AddToCallMessage
    {
        public bool Show { get; set; }
    }

	#endregion


	public class CallsViewModel : ViewModelBase
    {
        #region | Private Class Variables |

        private static VXLogger logger = VXLoggingManager.INSTANCE.GetLogger("CallsViewModel");

        private ViewModelBase	currentCall;
        private ViewModelBase	currentIncomingCall;
        private ViewModelBase	addToCallViewModel;

        private DateTime		recordingStartTime;
        private TimeSpan		recordingDuration;
        private DispatcherTimer recordingTimer;
        private bool			showRecording;
        private bool			endCallRecordingStarted;
        private string          mAnimationReferenceID = System.Guid.NewGuid().ToString();

		//When we close widget with animation, how we do that depends on how it started.
		//	Here are some key strings we use with the StartAnimationMessage.DataKey to handle that
		//	in a single handler for AnimationDoneMessage.
		//	We define them here because I (JRT) dislike 'magic' values in the main code.
		private const string	animationDataKey_AddToCall			= "AddToCallViewModel";	//This one does NOT need the trailing pipe |
		private const string	animationDataKey_CallDone			= "CallDone|";
		private const string	animationDataKey_JoinCall			= "JoinCall|";
		private const string	animationDataKey_AddOrReplaceCall	= "AddOrReplaceCall|";

        #endregion

        #region | Constructors |

        public CallsViewModel(PhoneCallMessage msg)
        {
            CallVMs = new ObservableCollection<CallItemViewModel>();

            OnPhoneCallMessage(msg);

            MessengerInstance.Register<CallDoneMessage>			(this, OnCallDoneMessage);
            MessengerInstance.Register<PhoneCallMessage>		(this, OnPhoneCallMessage);
            MessengerInstance.Register<MakeActiveCallMessage>	(this, OnMakeActiveCallMessage);
            MessengerInstance.Register<MergeCallMessage>		(this, OnMergeCallMessage);
            MessengerInstance.Register<EndAndAcceptCallMessage>	(this, OnEndAndAcceptCallMessage);
            MessengerInstance.Register<CallRecordingMessage>	(this, OnCallRecordingMessage);
            MessengerInstance.Register<JoinCallMessage>			(this, OnJoinCall);
            MessengerInstance.Register<AddToCallMessage>		(this, onAddToCallMessage);
            MessengerInstance.Register<AnimationDoneMessage>    (this, onAnimationDoneMessage);

            EndCallRecordingCommand = new RelayCommand(OnEndCallRecording, () => { return !endCallRecordingStarted; });

			//Otherwise event handler does not fire on incoming calls.
			DispatcherHelper.CheckBeginInvokeOnUI(() =>
			{
				recordingTimer			 = new DispatcherTimer();
				recordingTimer.Tick		+= new EventHandler(OnDispatcherTimerTick);
				recordingTimer.Interval  = new TimeSpan(0, 0, 1);
			} );

        }

        #endregion

        #region | Public Properties |
 
		public ObservableCollection<CallItemViewModel> CallVMs { get; private set; }

        public ICommand EndCallRecordingCommand { get; set; }

        public string SipCallId { get; set; }

        public ViewModelBase CurrentCall
        {
            get { return currentCall; }
            set { Set(ref currentCall, value); }
        }

        public ViewModelBase CurrentIncomingCall
        {
            get { return currentIncomingCall; }
            set { Set(ref currentIncomingCall, value); }
        }

        public bool ShowRecording
        {
            get { return showRecording; }
            set { Set(ref showRecording, value); }
        }

        public TimeSpan RecordingDuration
        {
            get { return recordingDuration; }
            set { Set(ref recordingDuration, value); }
        }

        public bool EndCallRecordingStarted
        {
            get { return endCallRecordingStarted; }
            set { Set(ref endCallRecordingStarted, value); }
        }

        public ViewModelBase AddToCallViewModel
        {
            get { return addToCallViewModel; }
            set { Set(ref addToCallViewModel, value); }
        }

        // For Animation
        public string AnimationReferenceID
        {
            get { return mAnimationReferenceID; }
        }

        #endregion

        #region | Private Procedures |

        SipAgentApi getSipApi()
        {
            return SipAgentApi.getInstance();
        }

        private void OnPhoneCallMessage(PhoneCallMessage msg)
        {
            // As the Messages can come from SIP API which run on different thread,
            // the local objects can only be modified on the thread that created them (the UI thread)
            DispatcherHelper.CheckBeginInvokeOnUI(() =>
            {
				handleCallStateChange( msg.Call, msg.CallId, msg.State );
			} );
        }

        private void OnCallDoneMessage(CallDoneMessage msg)
        {
            // As the Messages can come from SIP API which run on different thread,
            // the local objects can only be modified on the thread that created them (the UI thread)
            DispatcherHelper.CheckBeginInvokeOnUI(() =>
            {
				var call = getCallByCallId( msg.CallId );

				if ( call != null )
                {
					updateIsInConference();

                    var tmp = call as CallInProgressViewModel;
                    var isIncoming = false;
                    if (tmp != null && tmp.IsIncomingCall)
                    {
                        isIncoming = true;
                    }

                    MessengerInstance.Send<StartAnimationMessage>(new StartAnimationMessage { AnimationReferenceID = call.AnimationReferenceID, DataKey = animationDataKey_CallDone + call.CallId.ToString(), SkipAnimation = isIncoming });

                    /// Moved to Animation Done handler
                    //CallVMs.Remove(call);
                    //call.Cleanup();
					//call = null;

                    //MakeTopCallActive();
                }

                //if (CallVMs.Count == 0)
                //{
                //    MessengerInstance.Send<CallsDoneMessage>(new CallsDoneMessage { });
                //}

                //SortCalls();
                //CheckCallsCount();
            });
        }

        private async void OnMakeActiveCallMessage(MakeActiveCallMessage msg)
        {
            // Make this call active
            SessionManager.Instance.ResumeCall(msg.CallId);

            await NotifyHttpListenersHelper.HoldCall( msg.HttpListenerId, false);
        }

        private async void OnMergeCallMessage(MergeCallMessage msg)
        {
            // Get Current Call
            var currentCall = (from c in CallVMs
                               where c.CallId == msg.CallId
                               select c).FirstOrDefault();

            // Get Top Hold Call - We will merge TO this call.
            var topHoldCall = (from c in CallVMs
                           where c.CallId != msg.CallId
                           && c.Mode == CallItemMode.Hold
                           select c).FirstOrDefault();

            // Do Merge
            if (topHoldCall != null)
            {
                // Store the values                
                var name		= currentCall.Name;
                var avatarUri	= currentCall.AvatarUri;

                // Start/AddTo Conference - This logic merges NEW call into EXISTING, which is what we want.
				ConferenceCallHttpListenerResponse response = await NotifyHttpListenersHelper.Conference( topHoldCall.CallRef.ConferenceId, topHoldCall.CallRef.HttpListenerId, msg.HttpListenerId );

				if ( response != null )
				{
					if ( response.succeeded() )
					{
						if ( ! response.hasNoError() )
						{
							logger.Warn( "NotifyHttpListener returned 'success' and code OTHER than 200.");
						}

						string conferenceId = response.ConferenceId;
						logger.Debug("ConferenceId: " + conferenceId);

						//We only get conferenceId on START, not on ADD.
						if ( !String.IsNullOrEmpty( conferenceId ) )
						{ 
							topHoldCall.CallRef.ConferenceId = conferenceId;
//							currentCall.CallRef.ConferenceId = conferenceId;	//Only in topHoldCall, for now.
						}

						//When call hangs up, we must clear the conferenceId.

						// Add the current call to top hold call.
						var mergedCall = getCallByCallId(topHoldCall.CallId);
						SessionManager.Instance.MarkCallAsMerging( currentCall.CallRef.CallId, true );	//So we can control the hangup sound
						((CallInProgressViewModel)topHoldCall).AddToConference(currentCall.Name, currentCall.AvatarUri);

						//Server should be sending BYE for current call, so we should not have to do anything for that.  It should hangup on its own.

						//Make top hold call active.  This will happen any way after active call window times out, but we want it ASAP.
						SessionManager.Instance.ResumeCall( topHoldCall.CallRef.CallId );

						CheckCallsCount();	//TODO: Not sure we need this, based on other logic changes.
					}
					else
					{
						logger.Warn( "NotifyHttpListener returned code: " + response.Code );

						//Since we have a failure, switch the Merge button to its unpressed state.

						//TODO: Notify user based on specific code.
						if		( response.hasConferenceMaxContactsError() )
						{
							//Display message in Response
							MessageBox.Show( response.Message, Config.Branding.AppName, MessageBoxButtons.OK, MessageBoxIcon.Warning );
						}
						else if ( response.hasConferenceNotEnabledOnPlanError() )
						{
							//Display message in Response
							MessageBox.Show( response.Message, Config.Branding.AppName, MessageBoxButtons.OK, MessageBoxIcon.Warning );
						}
						else if ( response.hasConnectingToServerError() )
						{
							//TODO: Display generic message
						}
					}	//if (response.succeeded())
				}
				else
				{
					logger.Warn( "NotifyHttpListener returned null." );
					//TODO: Display generic message
				}// if ( response != null
			}
        }

		CallItemViewModel getCallByCallId( int callId )
		{
			CallItemViewModel result = null;

			foreach ( CallItemViewModel c in CallVMs )
			{
				if ( c.CallId == callId )
				{ 
					result = c;
					break;
				}
			}

			return result;
		}

		private void updateIsInConference()
		{
			foreach ( CallItemViewModel call in CallVMs )
			{
                if ( call.GetType() == typeof(CallInProgressViewModel))
				{
					bool	  inConference = false;
					PhoneCall phoneCall    = SessionManager.Instance.GetPhoneCall( call.CallId );

					if ( phoneCall != null )
					{ 
						inConference = !string.IsNullOrWhiteSpace( phoneCall.ConferenceId );
					}

					((CallInProgressViewModel)call).IsInConference = inConference;
				}
			}
		}

        private void MinimizeExistingCalls()
        {
            foreach (var call in CallVMs)
            {
                if (call.GetType() == typeof(CallInProgressViewModel))
                {
                    call.Mode = CallItemMode.SubActive;
                }
            }
        }

        private void MaximizeAnySubActiveCall()
        {
            if (CallVMs.Count > 0)
            {
                var activeCount = (from c in CallVMs
                                      where c.Mode == CallItemMode.Active
                                      select c).Count();

                var subActiveCount = (from c in CallVMs
                                      where c.Mode == CallItemMode.SubActive
                                      select c).Count();

                if(subActiveCount > 0 && activeCount == 0)
                {
                    var subActiveCalls = (from c in CallVMs
                                          where c.Mode == CallItemMode.SubActive
                                          select c).ToList();

                    foreach(var call in subActiveCalls)
                    {
                        if (call.GetType() == typeof(CallInProgressViewModel))
                        {
                            call.Mode = CallItemMode.Active;
                        }
                    }
                }
            }
        }

        private void MakeTopCallActive()
        {
            if (CallVMs.Count > 0)
            {
				if ( CallVMs[0].Mode != CallItemMode.Active )
				{ 
					SessionManager.Instance.ResumeCall(CallVMs[0].CallId);
				}
            }
        }

        private void SortCalls()
        {
            var sortedCalls = (from c in CallVMs
                               orderby c.SortOrder
                               select c).ToList();

            for (int i = 0; i < sortedCalls.Count(); i++)
                CallVMs.Move(CallVMs.IndexOf(sortedCalls[i]), i);

            sortedCalls = null;
        }

        private void CheckCallsCount()
        {
            MessengerInstance.Send<CallsInQueueMessage>(new CallsInQueueMessage { Count = CallVMs.Count });
        }

        private void OnEndAndAcceptCallMessage(EndAndAcceptCallMessage msg)
        {
            //Close all active calls & accept incoming call
            DispatcherHelper.CheckBeginInvokeOnUI(() =>
            {
                foreach (var item in CallVMs)
                {
                    if (msg.CallId != item.CallId)
                    {
                        SessionManager.Instance.CloseCall(item.CallId);
                    }
                }
            });
        }

        private void OnCallRecordingMessage(CallRecordingMessage msg)
        {
            if (msg.IsRecording)
            {
                RecordingDuration  = new TimeSpan(0, 0, 0);
                recordingStartTime = DateTime.Now;
                recordingTimer.Start();
            }
            else
            {
                recordingTimer.Stop();
            }

            ShowRecording			= msg.IsRecording;
            EndCallRecordingStarted = false;
        }

        private void OnJoinCall(JoinCallMessage msg)
        {
            DispatcherHelper.CheckBeginInvokeOnUI(() =>
            {
                if (msg.Sender != null )
                {
					var call = getCallByCallId( msg.Sender.CallId );

					if ( call != null)
					{
                        MessengerInstance.Send<StartAnimationMessage>(new StartAnimationMessage { AnimationReferenceID = call.AnimationReferenceID, DataKey = animationDataKey_JoinCall + call.CallId.ToString(), Data = msg });

                        /* // Moved to Animation Done handler
						var newCall = new CallInProgressViewModel();

						newCall.CallId			= msg.Sender.CallId;
						newCall.Name			= msg.Sender.Name;
						newCall.AvatarUri		= msg.Sender.AvatarUri;
						newCall.CallState		= msg.Sender.CallRef.getState();
						newCall.IsIncomingCall	= true;

						// Update the collection
						CallVMs.Remove(call);
						CallVMs.Add(newCall);

	                    call.Cleanup();
						call = null;
                        */
					}
				}
            });
        }

        private void OnDispatcherTimerTick(object sender, EventArgs e)
        {
            RecordingDuration = DateTime.Now - recordingStartTime;
        }

        private void OnEndCallRecording()
        {
            EndCallRecordingStarted = true;
            MessengerInstance.Send<CallRecordingControlMessage>(new CallRecordingControlMessage { Stop = true });
        }

        private void onAddToCallMessage(AddToCallMessage msg)
        {
            if(msg.Show)
            {
                AddToCallViewModel = (new AddToCallViewModel());
            }
            else
            {
                if (AddToCallViewModel == null)
                    return;

                MessengerInstance.Send<StartAnimationMessage>(new StartAnimationMessage { AnimationReferenceID = ((AddToCallViewModel)AddToCallViewModel).AnimationReferenceID, DataKey = animationDataKey_AddToCall });
            }
        }

        private void onAnimationDoneMessage(AnimationDoneMessage msg)
        {
            if (msg.DataKey == null)
                return;

            DispatcherHelper.CheckBeginInvokeOnUI(() =>
            {
                string whatToHandle = msg.DataKey.ToString();

                if (whatToHandle == animationDataKey_AddToCall)
                {
                    if (AddToCallViewModel == null)
                        return;

                    if (((AddToCallViewModel)AddToCallViewModel).AnimationReferenceID != msg.AnimationReferenceID)
                        return;

                    AddToCallViewModel = null;
                }

                if (whatToHandle.StartsWith( animationDataKey_CallDone )) // Call is Done
                {
                    int CallId = Convert.ToInt32(whatToHandle.Substring(animationDataKey_CallDone.Length));
                    var call = getCallByCallId(CallId);

                    if (call != null)
                    {
                        CallVMs.Remove(call);
                        call.Cleanup();
                        call = null;

                        MakeTopCallActive();
                    }

                    if (CallVMs.Count == 0)
                    {
                        MessengerInstance.Send<CallsDoneMessage>(new CallsDoneMessage { });
                    }

                    SortCalls();
                    CheckCallsCount();
                }

                if (whatToHandle.StartsWith( animationDataKey_JoinCall )) // Join Call
                {
                    int CallId = Convert.ToInt32(whatToHandle.Substring( animationDataKey_JoinCall.Length ) );
                    var call = getCallByCallId(CallId);

                    if (call != null)
                    {
                        var newCall = new CallInProgressViewModel();
                        var data = msg.Data as JoinCallMessage;

                        newCall.CallId = data.Sender.CallId;
                        newCall.Name = data.Sender.Name;
                        newCall.AvatarUri = data.Sender.AvatarUri;
                        newCall.CallState = data.Sender.CallRef.getState();
                        newCall.IsIncomingCall = true;

                        // Update the collection
                        CallVMs.Remove(call);
                        CallVMs.Add(newCall);

                        call.Cleanup();
                        call = null;
                    }
                }

                if (whatToHandle.StartsWith( animationDataKey_AddOrReplaceCall ) ) // Add or Replace Call
                {
                    int CallId = Convert.ToInt32(whatToHandle.Substring( animationDataKey_AddOrReplaceCall.Length ) );
                    var call = getCallByCallId(CallId);

                    if (call != null)
                    {
                        CallVMs.Remove(call);
                        call.Cleanup();
                        call = null;

                        var newCall = msg.Data as CallItemViewModel;
                        CallVMs.Add(newCall);
                    }
                }
            });
        }

        private int GetActiveCallsCount()
        {
            return (from c in CallVMs
                    where c.Mode == CallItemMode.Active ||
                    c.Mode == CallItemMode.SubActive ||
                    c.Mode == CallItemMode.Hold
                    select c).ToList().Count;
        }

		#endregion

		#region CallState handlers - The REAL work
				
		private void addOrReplaceCall( CallItemViewModel newCall )
		{
            var call = getCallByCallId( newCall.CallId );

            if (call != null)
            {
                MessengerInstance.Send<StartAnimationMessage>(new StartAnimationMessage { AnimationReferenceID = call.AnimationReferenceID, DataKey = animationDataKey_AddOrReplaceCall + call.CallId.ToString(), Data = newCall });

                /// Moved to Animation Done handler
                //CallVMs.Remove(call);
                //call.Cleanup();
                //call = null;
            }
            else
            {
                CallVMs.Add(newCall);
            }
		}

		private void handleCallStateChange( PhoneCall phoneCall, int callId, PhoneCallState.CallState state )
		{
			switch( state )
			{
			case PhoneCallState.CallState.Dialing:	// Outgoing Start
				handleDialingState( phoneCall, callId, state );
				break;

			case PhoneCallState.CallState.Incoming:	// Incoming Start
				handleIncomingState( phoneCall, callId, state );
				break;

            case PhoneCallState.CallState.Talking:	// Existing call processing
				handleTalkingState( phoneCall, callId, state );
				break;

            case PhoneCallState.CallState.Hold:		// Existing call processing: Hold
				handleHoldState( phoneCall, callId, state );
				break;

            case PhoneCallState.CallState.Resumed:	// Existing call processing: Resumed
				handleResumedState( phoneCall, callId, state );
				break;

            case PhoneCallState.CallState.Closed:
			case PhoneCallState.CallState.Missed:	// Existing call processing: Complete
				handleClosedOrMissedState( phoneCall, callId, state );
				break;

			case PhoneCallState.CallState.Error:	// Existing call processing: Error
				handleErrorState( phoneCall, callId, state );
				break;

			default:
				logger.Warn( "Unknown PhoneCallState.CallState: " + Convert.ToString( state ) );
				break;
            }

            SortCalls();
            CheckCallsCount();
            MaximizeAnySubActiveCall();
        }

		private void handleDialingState( PhoneCall phoneCall, int callId, PhoneCallState.CallState state )
		{
			var call	= new CallInProgressViewModel();
            var contact = ContactManager.getInstance().getMatchingContact( phoneCall.PeerSipAddress );

			String name;
			
			if ( contact != null )
				name = contact.Name;
			else
				name = PhoneUtils.FormatPhoneNumber( phoneCall.PeerSipAddress.getUserName() );

            call.CallId		= callId;
            call.Name		= name;
            call.AvatarUri	= AvatarManager.Instance.GetUriForCalling( contact != null ? contact.ContactKey : string.Empty );
            call.CallState	= state;

            call.CallRef.retrieveSipCallId(null);

            MinimizeExistingCalls();

			addOrReplaceCall( call );
		}

		private void handleIncomingState( PhoneCall phoneCall, int callId, PhoneCallState.CallState state )
		{
            var isBlastTransferCallback = phoneCall.PeerSipAddress.getUserName() == SessionManager.Instance.User.Did;

            List<CallItemViewModel> callList = new List<CallItemViewModel>();
            foreach (CallItemViewModel callIn in CallVMs)
            {
                callList.Add(callIn);
            }

            callList = callList.Where(x => (x.Name == phoneCall.PeerSipAddress.getUserName()) == true).ToList();
            var isAlreadyAnswered = callList.Count > 0 ? true : false;

            if ((isBlastTransferCallback && CallVMs.Count > 0) || isAlreadyAnswered)
            {
                SessionManager.Instance.CloseCall( callId );
                return;
            }

            var  call		= new IncomingCallViewModel();
            var  contact	= ContactManager.getInstance().getMatchingContact( phoneCall.PeerSipAddress);
			bool canMessage = false;

			if ( contact != null )
			{
				canMessage = contact.canBeMessaged( Config.Settings.EnableSMS );
			}

            call.CallId				= callId;
            call.Name				= contact != null ? contact.Name : phoneCall.PeerSipAddress.getUserName();
            call.AvatarUri			= AvatarManager.Instance.GetUriForCalling( contact != null ? contact.ContactKey : string.Empty );
            call.AlreadyInCall		= GetActiveCallsCount() > 0;
			call.IsMessageAvailable = canMessage;

            MinimizeExistingCalls();

            addOrReplaceCall( call );
           
			MessengerInstance.Send<IncomingEventMessage>( new IncomingEventMessage{ Type = IncomingEventMessage.EventType.IncomingCall } );	//So UI can move mainwindow to foreground and other handling
		}
		
		private void handleTalkingState( PhoneCall phoneCall, int callId, PhoneCallState.CallState state )
		{
			var call = getCallByCallId( callId );

			if ( call != null )
            {
                if (call.GetType() == typeof(IncomingCallViewModel)) // Incoming call accepted
                {
                    if (((IncomingCallViewModel)call).IncomingMode != IncomingMode.Eavesdrop)
                    {
                        var newCall = new CallInProgressViewModel();

                        newCall.CallId			= call.CallId;
                        newCall.Name			= call.Name;
                        newCall.AvatarUri		= call.AvatarUri;
                        newCall.CallState		= state;
                        newCall.IsIncomingCall	= true;

						addOrReplaceCall( newCall );
                    }

                }
                else if (call.GetType() == typeof(CallInProgressViewModel)) // Outgoing call connected
                {
                    ((CallInProgressViewModel)call).CallState = state;
                }
            }
		}

		private void handleHoldState( PhoneCall phoneCall, int callId, PhoneCallState.CallState state )
		{
			var call = getCallByCallId( callId );
			if ( call != null )
            {
                call.Mode = CallItemMode.Hold;
                ((CallInProgressViewModel)call).CallState = state;
            }
		}

		private void handleResumedState( PhoneCall phoneCall, int callId,  PhoneCallState.CallState state )
		{
			var call = getCallByCallId( callId );

			if ( call != null )
            {
                call.Mode = CallItemMode.Active;
                ((CallInProgressViewModel)call).CallState = state;

//				SipAgentApi.getInstance().muteCall( callId, isMute );
            }
		}
		
		private void handleClosedOrMissedState( PhoneCall phoneCall, int callId, PhoneCallState.CallState state )
		{
			var call = getCallByCallId( callId );

			if ( call != null )
            {
                call.Mode = CallItemMode.Ended;

                if (call.GetType() == typeof(CallInProgressViewModel)) // Outgoing call
                {
                    ((CallInProgressViewModel)call).CallState = state;
                }
            }

            MessengerInstance.Send<ClearDialerTextMessage>(new ClearDialerTextMessage());
		}

		private void handleErrorState( PhoneCall phoneCall, int callId, PhoneCallState.CallState state )
		{
			var call = getCallByCallId( callId );

			if ( call != null )
            {
                if (call.GetType() == typeof(CallInProgressViewModel)) // Outgoing call
                {
                    ((CallInProgressViewModel)call).CallState = state;
                }

                call.Mode = CallItemMode.Done;
            }
		}

        #endregion
    }	//class CallsViewModel
}
