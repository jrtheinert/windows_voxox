﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;				//SessionManager, NotifyHttpListenersHelper
using Desktop.Model.Messaging;		//MessageManager, XmppManager
using Desktop.Util;					//TimeUtils
using Desktop.Utils;				//Logger

using ManagedDataTypes;				//Contact, Message, VxTimestamp

using GalaSoft.MvvmLight.CommandWpf; //RelayCommand

using System;						//TimeSpan, EventArgs, EventHandler, Int64
using System.Windows.Input;			//ICommand
using System.Windows.Threading;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Threading;		//DispatcherTimer
using System.Threading;

namespace Desktop.ViewModels.Calling
{
    public class IncomingCallViewModel : CallItemViewModel
    {
        #region | Private Class Variables |

        private static VXLogger logger = VXLoggingManager.INSTANCE.GetLogger("IncomingCallViewModel");

        private IncomingMode	incomingMode;
        private bool			alreadyInCall;
        private DispatcherTimer voicemailTimer;
        private TimeSpan		voicemailTime;
        private bool			isEavesdropAvailable;
        private bool isBusyMessage;
        private bool isMeetingMessage;
        private bool isCantTalkMessage;
        private int spinnerMilliSeconds = 400;

        #endregion

        #region | Constructors |

        public IncomingCallViewModel()
        {
            MessageCommand			= new RelayCommand( OnMessage,   () => { return true; } );
            EavesdropCommand		= new RelayCommand( OnEavesdrop, () => { return true; } );
            AcceptCommand			= new RelayCommand( OnAccept		);
            DeclineCommand			= new RelayCommand( OnDecline		);
            BusyCommand				= new RelayCommand( OnBusy			);
            MeetingCommand			= new RelayCommand( OnMeeting		);
            CallBackCommand			= new RelayCommand( OnCallBack		);
            MessageDeclineCommand	= new RelayCommand( OnDecline		);
            JoinCallCommand			= new RelayCommand( OnJoin			);
            DoneCommand				= new RelayCommand( OnDone			);
            IgnoreCommand			= new RelayCommand( OnDecline		);
            HoldAndAcceptCommand	= new RelayCommand( OnAccept		);
            EndAndAcceptCommand		= new RelayCommand( OnEndAndAccept	);

            IsEavesdropAvailable = SessionManager.Instance.User.Options.IsEavesdropAvailable;
			IsMessageAvailable   = true;
            IsBusyMessage		 = false;
            IsMeetingMessage	 = false;
            IsCantTalkMessage	 = false;

            MessengerInstance.Register<DeclinedMessage>(this, (DeclinedMessage msg) => { OnDecline();  });
        }

        #endregion

        #region | Public Properties |

        public IncomingMode IncomingMode
        {
            get { return incomingMode; }
            set { Set(ref incomingMode, value); }
        }

        public bool AlreadyInCall
        {
            get { return alreadyInCall; }
            set { Set(ref alreadyInCall, value); }
        }

        public TimeSpan VoicemailTime
        {
            get { return voicemailTime; }
            set { Set(ref voicemailTime, value); }
        }

        public bool IsEavesdropAvailable
        {
            get { return isEavesdropAvailable; }
            set { Set(ref isEavesdropAvailable, value); }
        }

        public bool IsMessageAvailable	{ get; set; }

        public bool IsBusyMessage
        {
            get { return isBusyMessage; }
            set { Set(ref isBusyMessage, value); }
        }

        public bool IsMeetingMessage
        {
            get { return isMeetingMessage; }
            set { Set(ref isMeetingMessage, value); }
        }

        public bool IsCantTalkMessage
        {
            get { return isCantTalkMessage; }
            set { Set(ref isCantTalkMessage, value); }
        }

        public ICommand MessageCommand			{ get; set; }
        public ICommand EavesdropCommand		{ get; set; }
        public ICommand AcceptCommand			{ get; set; }
        public ICommand DeclineCommand			{ get; set; }
        public ICommand BusyCommand				{ get; set; }
        public ICommand MeetingCommand			{ get; set; }
        public ICommand CallBackCommand			{ get; set; }
        public ICommand MessageDeclineCommand	{ get; set; }
        public ICommand JoinCallCommand			{ get; set; }
        public ICommand DoneCommand				{ get; set; }
        public ICommand IgnoreCommand			{ get; set; }
        public ICommand HoldAndAcceptCommand	{ get; set; }
        public ICommand EndAndAcceptCommand		{ get; set; }

        #endregion

        #region | Protected Procedures |

        protected override void OnCallEnded()
        {
            MessengerInstance.Send<CallDoneMessage>(new CallDoneMessage { CallId = CallId });
        }

        #endregion

        #region | Private Procedures |

        private void OnAccept()
        {
            logger.Debug("User clicked Accept button.");		//To time how long it takes for app to respond.

            SessionManager.Instance.AcceptCall(CallId);

            //TODO: Per WD-236, we should be waiting until we get a SIP INFO packet with 'Listener started' for this call.
            //	This appears to take about 0.65 seconds and is in addition to the SipCallId, which we get much quicker.

			//We NEED the httpListenerID for proper functionality.  For incoming calls, this may arrive upto 1 second
			//	after we accept the call (which we did above via SessionManager).
            CallRef.retrieveHttpListenerId(async (string httpListenerId) =>
            {
                await NotifyHttpListenersHelper.AcceptCall(httpListenerId);
            });
        }

        private void OnDecline()
        {
            StopVoicemailTimer();

            CallRef.retrieveHttpListenerId(async (string httpListenerId) =>
            {
                await NotifyHttpListenersHelper.HangupCall(httpListenerId);
            });

			SessionManager.Instance.RejectCall(CallId);			//This causes call to be deleted, so do this last.
        }

        private void OnMessage()
        {
            IncomingMode = IncomingMode.Messages;
        }

        private void OnEavesdrop()
        {
            IncomingMode = IncomingMode.Eavesdrop;

            DispatcherHelper.CheckBeginInvokeOnUI(() =>
            {
                SessionManager.Instance.AcceptCall(CallId);

                CallRef.retrieveHttpListenerId(async (string httpListenerId) =>
                {
                    await NotifyHttpListenersHelper.SendCallToVoiceMessage(httpListenerId);
                });

                voicemailTimer			 = new DispatcherTimer();
                voicemailTimer.Tick		+= new EventHandler(OnVoicemailTimer);
                voicemailTimer.Interval  = new TimeSpan(0, 0, 1);
                
				VoicemailTime			 = new TimeSpan(0, 0, 0);

                voicemailTimer.Start();
                
                Status = string.Format("Voicemail: {0}", Util.TimeUtils.FormatTimeSpan(VoicemailTime));
            });
        }

        private void OnVoicemailTimer(object sender, EventArgs e)
        {
			if ( CallRef != null )
			{ 
				VoicemailTime = new TimeSpan(0, 0, (int)CallRef.getRunningDurationAsSeconds());
				Status		  = string.Format("Voicemail: {0}", Util.TimeUtils.FormatTimeSpan(VoicemailTime));
			}
        }

        private async void OnJoin()
        {
            await NotifyHttpListenersHelper.BargeIn( CallRef.HttpListenerId );
            IncomingMode = IncomingMode.BargeIn;

            MessengerInstance.Send<JoinCallMessage>(new JoinCallMessage { Sender = this });

            StopVoicemailTimer();
        }

        private void OnBusy()
        {
            DispatcherHelper.CheckBeginInvokeOnUI(() =>
            {
                IsBusyMessage = true;
            });
            
            Thread tempThread = new Thread(() =>
            {
                Thread.Sleep(spinnerMilliSeconds * 3);
                
                DispatcherHelper.CheckBeginInvokeOnUI(() =>
                {
                    IsBusyMessage = false;
                });
                
                Thread.Sleep(spinnerMilliSeconds / 2);

                DispatcherHelper.CheckBeginInvokeOnUI(() =>
                {
                    SendMessage(LocalizedString.Messages_Busy);
                    MessengerInstance.Send<DeclinedMessage>(new DeclinedMessage());
                });
            });

            tempThread.Start();
        }

        private void OnMeeting()
        {
            DispatcherHelper.CheckBeginInvokeOnUI(() =>
            {
                IsMeetingMessage = true;
            });

            Thread tempThread = new Thread(() =>
            {
                Thread.Sleep(spinnerMilliSeconds * 3);

                DispatcherHelper.CheckBeginInvokeOnUI(() =>
                {
                    IsMeetingMessage = false;
                });

                Thread.Sleep(spinnerMilliSeconds / 2);

                DispatcherHelper.CheckBeginInvokeOnUI(() =>
                {
                    SendMessage(LocalizedString.Messages_Meeting);
                    MessengerInstance.Send<DeclinedMessage>(new DeclinedMessage());
                });
            });

            tempThread.Start();
        }

        private void OnCallBack()
        {
            DispatcherHelper.CheckBeginInvokeOnUI(() =>
            {
                IsCantTalkMessage = true;
            });

            Thread tempThread = new Thread(() =>
            {
                Thread.Sleep(spinnerMilliSeconds * 3);

                DispatcherHelper.CheckBeginInvokeOnUI(() =>
                {
                    IsCantTalkMessage = false;
                });

                Thread.Sleep(spinnerMilliSeconds / 2);

                DispatcherHelper.CheckBeginInvokeOnUI(() =>
                {
                    SendMessage(LocalizedString.Messages_CantTalk);
                    MessengerInstance.Send<DeclinedMessage>(new DeclinedMessage());
                });
            });

            tempThread.Start();
        }

        private void SendMessage(string messageText)
        {
            var sipAddress = CallRef.PeerSipAddress;
            PhoneNumber pn = Model.Contact.ContactManager.getInstance().getMatchingPhoneNumber(new string[] { sipAddress.getUserName() });

            Message msg    = new Message();
            Int64 localTs  = VxTimestamp.GetNowTimestamp();		//All times are UTC
            Int64 serverTs = VxTimestamp.GetNowTimestamp();	

            msg.Status    = Message.MsgStatus.NEW;
            msg.Direction = Message.MsgDirection.OUTBOUND;

            msg.MsgId = XmppManager.generateMsgId();
            msg.Body  = messageText;

            msg.FromDid = SessionManager.Instance.User.Did;
            msg.ToDid   = pn != null ? pn.DisplayNumber : sipAddress.getUserName();

            msg.FromJid = SessionManager.Instance.User.XmppJid;
            msg.ToJid   = pn != null ? pn.XmppJid : string.Empty;

			msg.Type = (pn != null ? Message.MsgType.CHAT : Message.MsgType.SMS);

            msg.LocalTimestamp  = localTs;
            msg.ServerTimestamp = new VxTimestamp( serverTs );

            logger.Debug("Sending Message - " + messageText + " - TO: " + msg.ToDid);
            MessageManager.getInstance().sendMessage(msg);
        }

        private void OnDone()
        {
            StopVoicemailTimer();
            SessionManager.Instance.CloseCall(CallId);
        }

        private void OnEndAndAccept()
        {
            MessengerInstance.Send<EndAndAcceptCallMessage>(new EndAndAcceptCallMessage() { CallId = CallId });

            SessionManager.Instance.AcceptCall(CallId);

            CallRef.retrieveHttpListenerId( async (string httpListenerId ) =>
            {
                await NotifyHttpListenersHelper.AcceptCall(httpListenerId);	
            });
        }

        private void StopVoicemailTimer()
        {
            if (voicemailTimer != null && voicemailTimer.IsEnabled)
            {
                voicemailTimer.Stop();
                voicemailTimer = null;
            }
        }

        #endregion
    }

    public class JoinCallMessage
    {
        public IncomingCallViewModel Sender { get; set; }
    }

    public enum IncomingMode
    {
        Normal,
        Messages,
        Eavesdrop,
        BargeIn
    }

    public class DeclinedMessage
    {

    }
}
