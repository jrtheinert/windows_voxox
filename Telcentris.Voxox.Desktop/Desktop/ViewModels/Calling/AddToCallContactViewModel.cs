﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Desktop.ViewModels.Calling
{
    /// <summary>
    /// This class provides display properties & actions for each "Contact" item (Main Contact & Additional Number) in the Contacts list for Add to Call.
    /// Setting of the properties is taken care from the listing model
    /// </summary>
    public class AddToCallContactViewModel : Desktop.ViewModels.Contacts.ContactViewModel
    {
        #region | Private Class Variables |

        private bool isAdditionalNumber;
        private string label;

        #endregion


        public AddToCallContactViewModel()
            : base()
        {

        }

        #region | Public Properties |

        /// <summary>
        /// Phone Label
        /// </summary>
        public string Label
        {
            get { return label; }
            set { Set(ref label, value); }
        }

        /// <summary>
        /// Is this an additional number to contact
        /// </summary>
        public bool IsAdditionalNumber
        {
            get { return isAdditionalNumber; }
            set { Set(ref isAdditionalNumber, value); }
        }

        #endregion
    }
}
