﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */
using Desktop.Config;					//Branding info for authenticate()
using Desktop.Model;					//SettingsManager
using Desktop.Util;						//EncryptDecrypt
using Desktop.Utils;					//VXLogger
using Desktop.Model.Dialer;             //CountryCode

using Vxapi23;							//RestfulApiManager
using Vxapi23.Model;					//VXResult, AuthenticateResponse

using GalaSoft.MvvmLight;				//ViewModelBase, MessengerInstance, Set
using GalaSoft.MvvmLight.CommandWpf;	//RelayCommand
using GalaSoft.MvvmLight.Threading;		//DispatcherHelper

using System;							//String, Exception
using System.Collections.Generic;		//List
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;             //ICommand
using System.Collections.ObjectModel;	//ObservableCollection				

namespace Desktop.ViewModels.Login
{
    public class LoginViewModel : ViewModelBase
    {
        #region | Private Member Variables |

        private static VXLogger logger = VXLoggingManager.INSTANCE.GetLogger("LoginViewModel");

        private string		username;
        private string		phoneNumber;
        private string		password;
        private string		errorMessageHeading;
        private string		errorMessage;
        private bool		isAuthenticating;
        private bool?		isAuthenticated;
        private bool		autoLogin;
        private bool		rememberPassword;
        private bool		enableAutoLogin;
		private string		logoImageUri;
        private LoginMode	loginMode;

		private CountryCode selectedCountryCode;

        #endregion

        #region | Constructor |

        public LoginViewModel()
        {
            LoginCommand = new RelayCommand(DoLogin, CanLogin);
            LoginMode    = Config.Branding.LoginMode;
            
            LoadDefaults();
        }

        #endregion

        #region | Public Properties |

        public string Username
        {
            get { return username; }
            set
            {
                Set(ref username, value);
                ClearErrors();
            }
        }

        public CountryCode SelectedCountryCode
        {
            get { return selectedCountryCode; }
            set
            {
                Set(ref selectedCountryCode, value);
                ClearErrors();
            }
        }

        public string PhoneNumber
        {
            get { return phoneNumber; }
            set
            {
                Set(ref phoneNumber, value);
                ClearErrors();
            }
        }

        public string Password
        {
            get { return password; }
            set
            {
                Set(ref password, value);
                ClearErrors();
            }
        }

        public bool RememberPassword
        {
            get { return rememberPassword; }
            set { Set(ref rememberPassword, value); }
        }

        public string ErrorMessageHeading
        {
            get { return errorMessageHeading; }
            set { Set(ref errorMessageHeading, value); }
        }

        public string ErrorMessage
        {
            get { return errorMessage; }
            set { Set(ref errorMessage, value); }
        }

        public bool IsAuthenticating
        {
            get { return isAuthenticating; }
            set { Set(ref isAuthenticating, value); }
        }

        public bool? IsAuthenticated
        {
            get { return isAuthenticated; }
            set { Set(ref isAuthenticated, value); }
        }
		
        public String LogoImageUri
        {
            get { return logoImageUri; }
            set { Set(ref this.logoImageUri, value); }
        }

        public LoginMode LoginMode
        {
            get { return loginMode; }
            set { Set(ref this.loginMode, value); }
        }

        public ICommand LoginCommand { get; private set; }

        #endregion

        #region | Private Properties |

        private void LoadDefaults()
        {
            SelectedCountryCode = CountryCodeList.Instance.getByKey( SettingsManager.Instance.App.LastLoginCountryCode );

			username		 = SettingsManager.Instance.App.LastLoginUserName;
            phoneNumber		 = SettingsManager.Instance.App.LastLoginPhoneNumber;
            autoLogin		 = SettingsManager.Instance.App.AutoLogin;
            rememberPassword = SettingsManager.Instance.App.RememberPassword;
            enableAutoLogin  = rememberPassword;

            string decryptedPassword = "";

            if ( EncryptDecrypt.Decrypt(SettingsManager.Instance.App.LastLoginPassword, out decryptedPassword) )
            {
                password = decryptedPassword;
            }
            else
            {
                password = "";
            }

            if (RememberPassword && autoLogin && CanLogin())
            {   
                DoLogin();
            }
        }

        /// <summary>
        /// Connects to Vxapi to authenticate user
        /// </summary>
        private void DoLogin()
        {
            IsAuthenticating = true;
            ClearErrors();
            SaveAutoLoginSettings();

            Task.Run(() =>
                {
                    try
                    {
						Username	= username.Trim();
                        PhoneNumber = PhoneNumber.Trim();
						Password	= password.Trim();

                        string apiSendUsername = ""; // Username to send to API. Save THIS in SessionManager.

                        if(LoginMode == LoginMode.Email)
                        {
                            apiSendUsername = Username;
                        }
                        else if (LoginMode == LoginMode.Mobile)
                        {
                            apiSendUsername = string.Format("{0}{1}", SelectedCountryCode.Code, PhoneNumber);
                        }

						Username = apiSendUsername;

                        VXResult<AuthenticateResponse> result = RestfulAPIManager.INSTANCE.Authenticate( apiSendUsername, Password, Config.Branding.PartnerIdentifier, SessionManager.Instance.NativeLoginUrl );
                        
                        DispatcherHelper.CheckBeginInvokeOnUI(async () =>
                        {
                            if(result.Success)
                            {
                                if (RestfulAPIManager.INSTANCE.isValidUser(result.Data))
                                {
                                    logger.Info(String.Format("User <{0}> successfully authenticated.", Username));

                                    AuthenticateResponse response = result.Data;
                                    logger.Info("Login response : " + response);
                                    try
                                    {
                                        var initialized = await SessionManager.Instance.InitializeUserSession(Username, Password, response);	//We need password for XMPP login.

                                        if (initialized)
                                        {
                                            logger.Info("user session initialized");
                                            if (RememberPassword == false)
                                            {
                                                Password = "";
                                            }

											ActionManager.Instance.HandleLoginSuccess();

                                            IsAuthenticated = true;
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        logger.Error(String.Format("Exception while initializing {0} user.", Username), ex);

                                        ErrorMessageHeading = LocalizedString.LoginFailed;
                                        IsAuthenticated = false;
                                    }

                                    IsAuthenticating = false;
                                }
                                else
                                {
                                    logger.Info(String.Format("Failed to login using {0} username.", Username));
                                    ErrorMessage		= LocalizedString.LoginFailMsg;
                                    ErrorMessageHeading = LocalizedString.LoginFailed;

                                    IsAuthenticated  = false;
                                    IsAuthenticating = false;
                                }
                            }
                            else
                            {
                                ErrorMessageHeading = LocalizedString.LoginFailed;

                                if(result.IsNetworkError)
                                {
                                    ErrorMessage = LocalizedString.NetworkErrorMsg;    
                                }
                                else
                                {
                                    ErrorMessage = LocalizedString.GenericErrorMsg;
                                }                               

                                IsAuthenticated  = false;
                                IsAuthenticating = false;
                            }
                        });
                    }
                    catch (Exception ex)
                    {
                        logger.Error(String.Format("Exception while authenticating {0} user.", Username), ex);

                        DispatcherHelper.CheckBeginInvokeOnUI(() =>
                        {
                            ErrorMessageHeading = LocalizedString.LoginFailed;
                            ErrorMessage		= LocalizedString.GenericErrorMsg;

                            IsAuthenticated  = false;
                            IsAuthenticating = false;
                        });
                    }
                });
        }

        private bool CanLogin()
        {
            bool nameOrNumberCheck = false;

            if (LoginMode == LoginMode.Email)
            {
                nameOrNumberCheck = !String.IsNullOrWhiteSpace(Username);
            }
            else if(LoginMode == LoginMode.Mobile)
            {
                nameOrNumberCheck = SelectedCountryCode != null && !String.IsNullOrWhiteSpace(PhoneNumber);
            }

            return nameOrNumberCheck
                && !String.IsNullOrWhiteSpace(Password) 
                && IsAuthenticating == false;
        }

        private void ClearErrors()
        {
            IsAuthenticated = null;
            ErrorMessageHeading = null;
            ErrorMessage = null;
        }

        private void SaveAutoLoginSettings()
        {
            SettingsManager.Instance.App.LastLoginUserName    = Username;
            SettingsManager.Instance.App.LastLoginCountryCode = SelectedCountryCode.Key;
            SettingsManager.Instance.App.LastLoginPhoneNumber = PhoneNumber;
            SettingsManager.Instance.App.RememberPassword     = RememberPassword;

            string passwordToStore = "";

            if(RememberPassword)
            {
                string encryptedPassword = "";
                if (EncryptDecrypt.Encrypt(Password, out encryptedPassword) == true)
                {
                    passwordToStore = encryptedPassword;
                }
            }

			SettingsManager.Instance.App.LastLoginPassword = passwordToStore;
        }

        #endregion
    }
}