﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Aug 2015
 *		(actually Murali G. of Orchestra, but we want an email address)
 */

using Desktop.Model;

using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;	//RelayCommand

using System.Windows.Input;				// ICommand

namespace Desktop.ViewModels.Login
{
    public class AdvancedSettingsViewModel : ViewModelBase
    {
        private string	apiServer;
        private string	xmppServer;
        private string	sipServer;
//        private bool	useProductionApiKey;
        private bool	enableTURN;

        #region | Constructor |

        public AdvancedSettingsViewModel()
        {
            ApplyApiServerCommand	= new RelayCommand( ApplyApiServer,  () => { return !string.IsNullOrWhiteSpace(ApiServer); });
            ApplyXmppServerCommand	= new RelayCommand( ApplyXmppServer, () => { return !string.IsNullOrWhiteSpace(XmppServer); });
            ApplySipServerCommand	= new RelayCommand( ApplySipServer,  () => { return !string.IsNullOrWhiteSpace(SipServer); });
            ResetCommand			= new RelayCommand( ResetSettings );

            LoadDefaults();
        }

        #endregion

        #region | Public Properties |

        public string ApiServer
        {
            get { return apiServer; }
            set
            {
                Set(ref apiServer, value);
            }
        }

        public string DefaultApiServer
        {
            get { return Desktop.Config.Brand.getOriginalApiServer(); }
        }

        public string XmppServer
        {
            get { return xmppServer; }
            set
            {
                Set(ref xmppServer, value);
            }
        }

        public string DefaultXmppServer
        {
            get { return Config.Xmpp.Server; }
        }

        public string SipServer
        {
            get { return sipServer; }
            set
            {
                Set(ref sipServer, value);
            }
        }

        public string DefaultSipServer
        {
            get { return "Sip server (from API)"; }
        }

		//public bool UseProductionApiKey
		//{
		//	get { return useProductionApiKey; }
		//	set
		//	{
		//		Set(ref useProductionApiKey, value);
		//		SettingsManager.Instance.Advanced.UseProductionApiKey = value;
		//	}
		//}

        public bool EnableTURN
        {
            get { return enableTURN; }
            set
            {
                Set(ref enableTURN, value);
				SettingsManager.Instance.Advanced.EnableTURN = value;
            }
        }

        public ICommand ApplyApiServerCommand	{ get; private set; }
        public ICommand ApplyXmppServerCommand	{ get; private set; }
        public ICommand ApplySipServerCommand	{ get; private set; }
        public ICommand ResetCommand			{ get; private set; }

        //public ICommand OKCommand				{ get; private set; }	

        #endregion

        #region | Private Procedures |

        private void LoadDefaults()
        {
			//Moved this logic to Settings class?
            ApiServer			= SettingsManager.Instance.Advanced.UseApiServer  ? SettingsManager.Instance.Advanced.ApiServer  : string.Empty;
            XmppServer			= SettingsManager.Instance.Advanced.UseXmppServer ? SettingsManager.Instance.Advanced.XmppServer : string.Empty;
            SipServer			= SettingsManager.Instance.Advanced.UseSipServer  ? SettingsManager.Instance.Advanced.SipServer  : string.Empty;
//            UseProductionApiKey = SettingsManager.Instance.Advanced.UseProductionApiKey;
            EnableTURN			= SettingsManager.Instance.Advanced.EnableTURN;
        }

        private void ApplyApiServer()
        {
            SettingsManager.Instance.Advanced.ApiServer	   = ApiServer;
            SettingsManager.Instance.Advanced.UseApiServer = true;
        }

        private void ApplyXmppServer()
        {
            SettingsManager.Instance.Advanced.XmppServer	= XmppServer;
            SettingsManager.Instance.Advanced.UseXmppServer = true;
        }

        private void ApplySipServer()
        {
            SettingsManager.Instance.Advanced.SipServer		= SipServer;
            SettingsManager.Instance.Advanced.UseSipServer	= true;
        }

        private void ResetSettings()
        {
            SettingsManager.Instance.Advanced.resetToDefaultsV1();
            LoadDefaults();
        }

        #endregion
    }
}
