﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using GalaSoft.MvvmLight;	//ViewModelBase, Set

namespace Desktop.ViewModels.Faxes
{
    public class FaxDetailsItemViewModel : ViewModelBase
    {
        #region | Private Class Variables |

        private string name;
        private string faxDate;
        private string number;
        private string numberType;
        private string transcription;
        private string faxMonth;
        private string faxDay;
        private string faxSource;
        private FaxDirection faxDirection;

        #endregion

        #region | Constructors |

        public FaxDetailsItemViewModel()
        {

        }

        #endregion

        #region | Public Properties |

        public string Name
        {
            get { return name; }
            set { Set(ref name, value); }
        }

        public string FaxDate
        {
            get { return faxDate; }
            set { Set(ref faxDate, value); }
        }

        public string Number
        {
            get { return number; }
            set { Set(ref number, value); }
        }

        public string NumberType
        {
            get { return numberType; }
            set { Set(ref numberType, value); }
        }

        public string Transcription
        {
            get { return transcription; }
            set { Set(ref transcription, value); }
        }

        public string FaxSource
        {
            get { return faxSource; }
            set { Set(ref faxSource, value); }
        }

        public string FaxMonth
        {
            get { return faxMonth; }
            set { Set(ref faxMonth, value); }
        }

        public string FaxDay
        {
            get { return faxDay; }
            set { Set(ref faxDay, value); }
        }

        public FaxDirection FaxDirection
        {
            get { return faxDirection; }
            set { Set(ref faxDirection, value); }
        }

        public bool ShowDateBubble
        {
            get { return !string.IsNullOrEmpty(faxDay); }
        }

        #endregion
    }
}
