﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using GalaSoft.MvvmLight;	//ViewModelBase, MessengerInstance, Set

namespace Desktop.ViewModels.Faxes
{
    public class FaxesMainViewModel : ViewModelBase
    {
        #region | Private Class Variables |

        private FaxListViewModel faxListViewModel;
        private ComposeFaxViewModel composeFaxViewModel;
        private FaxesViewModel faxesViewModel;
        private bool isComposing;

        #endregion

        #region | Constructors |

        public FaxesMainViewModel()
        {
            MessengerInstance.Register<ShowContactFaxesMessage>(this, ShowContactFaxes);
            MessengerInstance.Register<FaxComposeMessage>(this, OnFaxCompose);

            FaxList = new FaxListViewModel();
        }

        public override void Cleanup()
        {
            base.Cleanup();

            MessengerInstance.Unregister<ShowContactFaxesMessage>(this, ShowContactFaxes);
            MessengerInstance.Unregister<FaxComposeMessage>(this, OnFaxCompose);

            if(FaxList != null)
            {
                FaxList.Cleanup();
                FaxList = null;
            }

            if(Faxes != null)
            {
                Faxes.Cleanup();
                Faxes = null;
            }

            IsComposing = false;
            if (ComposeFax != null)
            {
                ComposeFax.Cleanup();
                ComposeFax = null;
            }
        }

        #endregion

        #region | Public Properties |

        public FaxListViewModel FaxList
        {
            get { return faxListViewModel; }
            set { Set(ref faxListViewModel, value); }
        }

        public FaxesViewModel Faxes
        {
            get { return faxesViewModel; }
            set { Set(ref faxesViewModel, value); }
        }

        public ComposeFaxViewModel ComposeFax
        {
            get { return composeFaxViewModel; }
            set { Set(ref composeFaxViewModel, value); }
        }

        public bool IsComposing
        {
            get { return isComposing; }
            set { Set(ref isComposing, value); }
        }

        #endregion

        #region | Private Procedures |

        private void ShowContactFaxes(ShowContactFaxesMessage msg)
        {
            if (Faxes != null && (Faxes.CmGroup != msg.CmGroup || Faxes.PhoneNumber != msg.PhoneNumber))
            {
                var existing = Faxes;
                Faxes = new FaxesViewModel( msg.ContactKey, msg.CmGroup, msg.PhoneNumber, msg.LocalTimestamp, msg.IsComposing);

                existing.Cleanup();
                existing = null;
            }
            else if (Faxes == null)
            {
                Faxes = new FaxesViewModel( msg.ContactKey, msg.CmGroup, msg.PhoneNumber, msg.LocalTimestamp, msg.IsComposing);
            }
        }

        private void OnFaxCompose(FaxComposeMessage msg)
        {
            if (msg.Show)
            {
                ComposeFax = new ComposeFaxViewModel();
                IsComposing = true;
            }
            else
            {
                IsComposing = false;
                ComposeFax = null;
            }
        }

        #endregion
    }
}
