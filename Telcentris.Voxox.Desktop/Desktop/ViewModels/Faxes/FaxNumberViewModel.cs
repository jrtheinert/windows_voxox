﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using GalaSoft.MvvmLight;	//ViewModelBase, Set

namespace Desktop.ViewModels.Faxes
{
    public class FaxNumberViewModel : ViewModelBase
    {
        #region | Private Class Variables |

        private string number;
        private string displayNumber;
        private string numberType;

        #endregion

        #region | Public Properties |

        public string Number
        {
            get { return number; }
            set { Set(ref number, value); }
        }

        public string DisplayNumber
        {
            get { return displayNumber; }
            set { Set(ref displayNumber, value); }
        }

        public string NumberType
        {
            get { return numberType; }
            set { Set(ref numberType, value); }
        }

        #endregion
    }
}
