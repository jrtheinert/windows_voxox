﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using GalaSoft.MvvmLight;	//ViewModelBase, Set

using System;				//Int64

namespace Desktop.ViewModels.Faxes
{
    public class FaxListItemViewModel : ViewModelBase
    {
        #region | Private Class Variables |

        private int    cmGroup;
        private string contactKey;
        private string phoneNumber;
        private string nameOrNumber;
        private string lastMessage;
        private string dateText;
        private string avatarUri;
        private FaxDirection faxDirection;

        #endregion

        #region | Public Properties |

        public int CmGroup
        {
            get { return cmGroup; }
            set { Set(ref cmGroup, value); }
        }

        public string ContactKey
        {
            get { return contactKey; }
            set { Set(ref contactKey, value); }
        }

        public string NameOrNumber
        {
            get { return nameOrNumber; }
            set { Set(ref nameOrNumber, value); }
        }

        public string LastMessage
        {
            get { return lastMessage; }
            set { Set(ref lastMessage, value); }
        }

        public string DateText
        {
            get { return dateText; }
            set { Set(ref dateText, value); }
        }

        public string AvatarUri
        {
            get { return avatarUri; }
            set { Set(ref avatarUri, value); }
        }

        public string PhoneNumber
        {
            get { return phoneNumber; }
            set { Set(ref phoneNumber, value); }
        }

        public FaxDirection FaxDirection
        {
            get { return faxDirection; }
            set { Set(ref faxDirection, value); }
        }

        public Int64 LocalTimestamp { get; set; }

        #endregion
    }
}

