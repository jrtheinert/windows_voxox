﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;					//AvatarManager, SessionManager
using Desktop.Model.Messaging;			//MessageManager
using Desktop.Util;						//PhoneUtils, TimeUtils
using Desktop.Utils;					//VXLogger

using ManagedDataTypes;					//Message, RichData, PhoneNumber, VxTimestamp
using ManagedDbManager;					//DbChangedEventHandler, DbChangedEventArgs

using Vxapi23;							//RestfulApiManager

using GalaSoft.MvvmLight;				//ViewModelBase, MessengerInstance, Set
using GalaSoft.MvvmLight.CommandWpf;	//RelayCommand
using GalaSoft.MvvmLight.Threading;		//DispatcherHelper

using System;
using System.Collections.ObjectModel;	//ObservableCollection
using System.Windows.Input;				//ICommand

namespace Desktop.ViewModels.Faxes
{
    public class FaxesViewModel : ViewModelBase
    {
        #region | Private Class Variables |

        private static VXLogger logger = VXLoggingManager.INSTANCE.GetLogger("FaxesViewModel");
        private Int64 localTimestamp;
        private int cmGroup;
        private string phoneNumber;
		private string contactKey;
        private string name;
        private string avatarUri;
        private string selectedFaxNumber;
        private string newFaxNumber;
        private bool isComposing;
        private bool showAddFaxNumber;
        private bool canDownload;
        private string faxSource;
        private DbChangedEventHandler dbChangeHandler = null;

        #endregion

        #region | Constructors |

        public FaxesViewModel( String contactKeyIn, int cmGroupIn, string phoneNumberIn, Int64 localTimestampIn, bool isComposingIn )
        {
			ContactKey		= contactKeyIn;
            CmGroup			= cmGroupIn;
            PhoneNumber		= phoneNumberIn;
            LocalTimestamp	= localTimestampIn;
            IsComposing		= isComposingIn;

            DownloadCommand			= new RelayCommand<string>(OnDownload, CanDownloadFax);
            AttachPagesCommand		= new RelayCommand(OnAttachPages);
            SendFaxCommand			= new RelayCommand(OnSendFax, () => { return !(string.IsNullOrEmpty(FaxSource) || string.IsNullOrEmpty(FaxSource)); });
            AddFaxNumberCommand		= new RelayCommand(OnAddFaxNumber);
            CancelFaxNumberCommand	= new RelayCommand(OnCancelFaxNumber);

            Faxes		= new ObservableCollection<FaxDetailsItemViewModel>();
            FaxNumbers	= new ObservableCollection<FaxNumberViewModel>();

            LoadData();

            SelectedFaxNumber = PhoneNumber;

            dbChangeHandler = new DbChangedEventHandler(OnDbChange);
            getUserDbm().addDbChangeListener(dbChangeHandler);
        }

		private ManagedDbManager.ManagedDbManager getUserDbm()
		{
			return SessionManager.Instance.UserDataStore;
		}

        public override void Cleanup()
        {
            base.Cleanup();

            if ( getUserDbm() != null && dbChangeHandler != null)
            {
                getUserDbm().removeDbChangeListener(dbChangeHandler);
                dbChangeHandler = null;
            }
        }

        #endregion

        #region | Public Properties |

        public Int64 LocalTimestamp
        {
            get { return localTimestamp; }
            set { Set(ref localTimestamp, value); }
        }

        public int CmGroup
        {
            get { return cmGroup; }
            set { Set(ref cmGroup, value); }
        }

        public string ContactKey
        {
            get { return contactKey; }
            set { Set(ref contactKey, value); }
        }

        public string PhoneNumber
        {
            get { return phoneNumber; }
            set { Set(ref phoneNumber, value); }
        }

        public string Name
        {
            get { return name; }
            set { Set(ref name, value); }
        }

        public string AvatarUri
        {
            get { return avatarUri; }
            set { Set(ref avatarUri, value); }
        }

        public string SelectedFaxNumber
        {
            get { return selectedFaxNumber; }
            set
            {
                if (value == "")
                {
                    ShowAddFaxNumber = true;
                }
                else
                {
                    Set(ref selectedFaxNumber, value);
                }
            }
        }

        public string NewFaxNumber
        {
            get { return newFaxNumber; }
            set { Set(ref newFaxNumber, value); }
        }

        public bool IsComposing
        {
            get { return isComposing; }
            set { Set(ref isComposing, value); }
        }

        public bool ShowAddFaxNumber
        {
            get { return showAddFaxNumber; }
            set { Set(ref showAddFaxNumber, value); }
        }

        public string FaxSource
        {
            get { return faxSource; }
            set { Set(ref faxSource, value); }
        }

        public ObservableCollection<FaxDetailsItemViewModel> Faxes { get; set; }

        public ObservableCollection<FaxNumberViewModel> FaxNumbers { get; set; }

        public ICommand DownloadCommand { get; set; }

        public ICommand AttachPagesCommand { get; set; }

        public ICommand SendFaxCommand { get; set; }

        public ICommand AddFaxNumberCommand { get; set; }

        public ICommand CancelFaxNumberCommand { get; set; }

        #endregion

        #region | Private Procedures |

        private void OnDownload(string url)
        {
            ActionManager.Instance.HandleUrl(url);
        }

        private bool CanDownloadFax(string url)
        {
            return canDownload;
        }

        private void OnAttachPages()
        {
            try
            {
                Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
                dlg.DefaultExt = ".pdf";
                dlg.Filter = "PDF Files (*.pdf)|*.pdf";		//TODO: These info should come from model layer since it defines supported file types.

                Nullable<bool> result = dlg.ShowDialog();

                if (result != null && result == true)
                {
                    FaxSource = dlg.FileName;
                }
            }
            catch (Exception ex)
            {
                logger.Error("OnAttachPages", ex);
            }
        }

        private void OnSendFax()
        {
            try
            {
                Int64 localTs  = VxTimestamp.GetNowTimestamp();
                Int64 serverTs = VxTimestamp.GetNowTimestamp();		//All times are UTC
                Message msg = new Message();

                msg.Status = Message.MsgStatus.NEW;
                msg.Direction = Message.MsgDirection.OUTBOUND;

                msg.MsgId = "UUID-" + localTs.ToString();		//TODO: XMPP Manager will assign this, as will SMS API call.  Not quite there yet.
                msg.Body = "";

                msg.FromDid = SessionManager.Instance.User.Did;
                msg.ToDid = PhoneUtils.GetPhoneNumberForFax(SelectedFaxNumber);

                msg.FromJid = SessionManager.Instance.User.XmppJid;
                msg.Type = Message.MsgType.FAX;

                msg.LocalTimestamp  = localTs;
                msg.ServerTimestamp = new VxTimestamp( serverTs );

                msg.RichData.MediaFile = FaxSource;
                msg.RichData.Type = RichData.RichDataType.PDF;

                MessageManager.getInstance().sendMessage(msg);

                FaxSource = "";
                IsComposing = false;

                MessengerInstance.Send<FaxComposeMessage>(new FaxComposeMessage { Show = false });
            }
            catch (Exception ex)
            {
                logger.Error("OnSendFax", ex);
            }
        }

        private void OnAddFaxNumber()
        {
            if (PhoneUtils.IsValidNumber(NewFaxNumber))
            {
                FaxNumbers.Insert(0, new FaxNumberViewModel { Number = NewFaxNumber, DisplayNumber = NewFaxNumber, NumberType = "Fax" });
                SelectedFaxNumber = NewFaxNumber;
                ShowAddFaxNumber = false;
                NewFaxNumber = "";
            }
        }

        private void OnCancelFaxNumber()
        {
            ShowAddFaxNumber = false;
        }

        private void LoadData()
        {
            PhoneNumber number = getUserDbm().getPhoneNumber(phoneNumber, cmGroup);
            AvatarUri = AvatarManager.Instance.GetUriForMessaging( ContactKey );
            Name = string.IsNullOrEmpty(number.Name) ? phoneNumber : number.Name;
            string url = "";

            if (IsComposing)
            {
                FaxNumbers.Add(new FaxNumberViewModel { Number = PhoneNumber, DisplayNumber = PhoneUtils.FormatPhoneNumber(PhoneNumber), NumberType = "Fax" });
                FaxNumbers.Add(new FaxNumberViewModel { Number = "", DisplayNumber = "New Number", NumberType = "Add a new fax number" });
            }
            else
            {
                Message temp = MessageManager.getInstance().getMessage(LocalTimestamp);	//TODO-FAX: get UmwMessage

                if (temp != null)
                {

                    if (String.IsNullOrEmpty(Name))
                    {
                        Name = LocalizedString.GenericUnknown;
                    }

                    if (temp.isFax())		//It should be
                    {
						url = RestfulAPIManager.INSTANCE.formatDownloadFileUrl( SessionManager.Instance.User.UserKey, SessionManager.Instance.User.CompanyUserId, (int)Message.ServerMsgType.FAX, temp.MsgId );
                    }

                    DateTime faxDateTime = temp.ServerTimestamp.AsLocalTime();

                    canDownload = temp.isStatusDelivered() || temp.isStatusRead() || temp.isStatusSent();

                    Faxes.Clear();

                    Faxes.Add(new FaxDetailsItemViewModel
                    {
                        Name = PhoneUtils.FormatPhoneNumber(temp.CName == "" ? Name : temp.CName),
                        FaxDate = string.Format("{0} at {1}", temp.Direction == Message.MsgDirection.OUTBOUND ? "Sent" : "Received", faxDateTime.ToString("hh:mm tt on MMMM dd")),
                        Number = PhoneUtils.FormatPhoneNumber(phoneNumber),
                        NumberType = "Home", //TODO: Set fax number type
                        FaxDirection = temp.Direction == Message.MsgDirection.OUTBOUND ? FaxDirection.Outgoing : FaxDirection.Incoming,
                        FaxDay = faxDateTime.ToString("dd"),
                        FaxMonth = faxDateTime.ToString("MMM"),
                        Transcription = temp.Body,
                        FaxSource = url
                    });
                }
            }
        }

        private void OnDbChange(object sender, DbChangedEventArgs e)
        {
            if (e.Data.isMsgTable())
            {
                DispatcherHelper.CheckBeginInvokeOnUI(() => { LoadData(); });
            }
        }

        #endregion
    }
}
