﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;					//AvatarManager, SessionManager
using Desktop.Model.Messaging;			//MessageManager
using Desktop.Util;						//PhoneUtils, TimeUtils

using GalaSoft.MvvmLight;				//ViewModelBase, MessengerInstance, Set
using GalaSoft.MvvmLight.CommandWpf;	//RelayCommand
using GalaSoft.MvvmLight.Threading;		//DispatcherHelper

using ManagedDataTypes;					//Message, UmwMessage, UmwMessageList
using ManagedDbManager;					//DbChangedEventHandler, DbChangedEventArgs

using System.Collections.Generic;		//List
using System.Collections.ObjectModel;	//ObservableCollection
using System.Linq;						//<query>
using System.Windows.Input;				//ICommand

namespace Desktop.ViewModels.Faxes
{
    public class FaxListViewModel : ViewModelBase
    {
        #region | Private Class Variables |

        private FaxFilterType filterFaxesBy;
        private string searchFaxes;
        private bool noSearchResultsAvailable;
        private int selectedIndex = -1;
        private DbChangedEventHandler dbChangeHandler = null;

        #endregion

        #region | Constructors |

        public FaxListViewModel()
        {
            Faxes = new ObservableCollection<FaxListItemViewModel>();
            ChooseContactCommand = new RelayCommand(OnChooseContact);

            LoadFaxes();

            dbChangeHandler = new DbChangedEventHandler(OnDbChange);
            getUserDbm().addDbChangeListener(dbChangeHandler);
        }

		private ManagedDbManager.ManagedDbManager getUserDbm()
		{
			return SessionManager.Instance.UserDataStore;
		}

        public override void Cleanup()
        {
            base.Cleanup();

            if ( getUserDbm() != null && dbChangeHandler != null)
            {
                getUserDbm().removeDbChangeListener(dbChangeHandler);
                dbChangeHandler = null;
            }
        }

        #endregion

        #region | Public Properties |

        public ObservableCollection<FaxListItemViewModel> Faxes { get; set; }

        public FaxFilterType FilterFaxesBy
        {
            get { return filterFaxesBy; }
            set
            {
                Set(ref filterFaxesBy, value);

                LoadFaxes();
            }
        }

        public string SearchFaxes
        {
            get { return searchFaxes; }
            set
            {
                Set(ref searchFaxes, value);

                LoadFaxes();
            }
        }

        public bool NoSearchResultsAvailable
        {
            get { return noSearchResultsAvailable; }
            set { Set(ref noSearchResultsAvailable, value); }
        }

        public int SelectedIndex
        {
            get { return selectedIndex; }
            set
            {
                Set(ref selectedIndex, value);

                if (selectedIndex > -1 && Faxes.Count > 0 && selectedIndex < Faxes.Count)
                {
                    var item = Faxes[selectedIndex];
                    MessengerInstance.Send<ShowContactFaxesMessage>(new ShowContactFaxesMessage { ContactKey = item.ContactKey, CmGroup = item.CmGroup, PhoneNumber = item.PhoneNumber, LocalTimestamp = item.LocalTimestamp, IsComposing = false });
                }
            }
        }

        public ICommand ChooseContactCommand { get; set; }

        #endregion

        #region | Private Procedures |

        private void LoadFaxes()
        {
            FaxListItemViewModel cvm = null;
            List<FaxListItemViewModel> list = new List<FaxListItemViewModel>();
            UmwMessageList faxList = MessageManager.getInstance().getFaxMessages();

            foreach (var fax in faxList.OrderByDescending(x => x.ServerTimestamp))
            {
                int         cmGroup = getUserDbm().getCmGroup(fax.bestPhoneNumber());
                PhoneNumber number  = getUserDbm().getPhoneNumber(fax.bestPhoneNumber(), cmGroup);

                //TODO: Why not just pass the entire UmwMessage object to CVM?  Why are we creating new/copied data when it is already defined at model layer?
                cvm = new FaxListItemViewModel();

                cvm.CmGroup			= cmGroup;
                cvm.NameOrNumber	= string.IsNullOrEmpty(number.Name) ? fax.bestPhoneNumber() : number.Name;
				cvm.ContactKey		= fax.ContactKey;
                cvm.FaxDirection	= fax.Direction == Message.MsgDirection.INBOUND ? FaxDirection.Incoming : FaxDirection.Outgoing;
                cvm.LastMessage		= GetLastMessageTextForFax(fax);
                cvm.DateText		= TimeUtils.GetDateWithFormatting( fax.ServerTimestamp.AsLocalTime() );
                cvm.PhoneNumber		= PhoneUtils.FormatPhoneNumber(fax.bestPhoneNumber());
                cvm.AvatarUri		= AvatarManager.Instance.GetUriForMessaging( fax.ContactKey );
                cvm.LocalTimestamp	= fax.LocalTimestamp;

                list.Add(cvm);
            }

            if (!string.IsNullOrWhiteSpace(searchFaxes))
            {
                list = list.Where(x => (x.NameOrNumber.ToLower().Contains(searchFaxes.ToLower()) == true) ||
                                        (PhoneUtils.GetPhoneNumberForFax(x.PhoneNumber).Contains(searchFaxes) == true)).ToList();
            }

            if (filterFaxesBy == FaxFilterType.All)
            {
                Faxes.Clear();

                foreach (var cm in list)
                {
                    Faxes.Add(cm);
                }
            }
            else if (filterFaxesBy == FaxFilterType.Incoming)
            {
                var filteredList = list.Where(x => x.FaxDirection == FaxDirection.Incoming).AsEnumerable();

                Faxes.Clear();

                foreach (var cm in filteredList)
                {
                    Faxes.Add(cm);
                }
            }
            else if (filterFaxesBy == FaxFilterType.Outgoing)
            {
                var filteredList = list.Where(x => x.FaxDirection == FaxDirection.Outgoing).AsEnumerable();

                Faxes.Clear();

                foreach (var cm in filteredList)
                {
                    Faxes.Add(cm);
                }
            }

            NoSearchResultsAvailable = (Faxes.Count == 0);

            //If no previous selection, select first.
            //	If previous selection, reselect it on reload, which may be due to DB change.
            if (SelectedIndex == -1)
            {
                if (Faxes.Count > 0)
                {
                    SelectedIndex = 0;
                }
            }
            else
            {
                SelectedIndex = SelectedIndex;	//Force reselection, likely to due DB change.  VERIFY THIS.
            }
        }

        private string GetLastMessageTextForFax(UmwMessage fax)
        {
            if(fax.Direction == Message.MsgDirection.OUTBOUND)
            {
                if (fax.isStatusDelivered() || fax.isStatusSent())
                {
                    return "Delivered";
                }
                else if (fax.isStatusPending() || fax.isStatusScheduled() || fax.isStatusUploading())
                {
                    return "Sending";
                }
                else
                {
                    return "Failed";
                }
            }
            else
            {
                return "Received";
            }
        }

        private void OnChooseContact()
        {
            MessengerInstance.Send<FaxComposeMessage>(new FaxComposeMessage { Show = true });
        }

        private void OnDbChange(object sender, DbChangedEventArgs e)
        {
            if (e.Data.isMsgTable())
            {
                DispatcherHelper.CheckBeginInvokeOnUI(() => { LoadFaxes(); });
            }
        }

        #endregion
    }

    public class ShowContactFaxesMessage
    {
        public int	  CmGroup			{ get; set; }
        public string PhoneNumber		{ get; set; }
        public string ContactKey		{ get; set; }
		public long   LocalTimestamp	{ get; set; }
		public bool   IsComposing		{ get; set; }
    }

    public class FaxComposeMessage
    {
        public bool Show { get; set; }
    }

    public enum FaxFilterType
    {
        All,
        Incoming,
        Outgoing
    }

    public enum FaxDirection
    {
        Incoming,
        Outgoing
    }
}
