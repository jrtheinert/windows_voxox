﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;					//AvatarManager
using Desktop.Model.Contact;			//ContactManager
using Desktop.Util;						//PhoneUtils
using Desktop.ViewModels.Contacts;		//ContactViewModel

using ManagedDataTypes;					//Contact, ContactList, PhoneNumber

using GalaSoft.MvvmLight;				//ViewModelBase, MessengerInstance, Set
using GalaSoft.MvvmLight.CommandWpf;	//RelayCommand

using System.Collections.Generic;		//List
using System.Collections.ObjectModel;	//ObservableCollection
using System.Windows.Input;				//ICommand

//TODO: Most of this code is duplicated following classes and should be refactored.:
//	- ContactListViewModel
//	- ComposeFaxViewModel
//	- ComposeMessageViewModel

namespace Desktop.ViewModels.Faxes
{
    public class ComposeFaxViewModel : ViewModelBase
    {
        #region | Private Class Variables |

        private string searchContacts;
        private bool noSearchResultsAvailable;
        private int selectedIndex = -1;

        #endregion

        #region | Constructors |

        public ComposeFaxViewModel()
        {
            Contacts = new ObservableCollection<ContactViewModel>();

            LoadContacts();

            CloseComposeFaxCommand = new RelayCommand(OnCloseCompose);
        }

        #endregion

        #region | Public Properties |

        public ObservableCollection<ContactViewModel> Contacts { get; set; }

        public string SearchContacts
        {
            get { return searchContacts; }
            set
            {
                Set(ref searchContacts, value);

                LoadContacts();
            }
        }

        public bool NoSearchResultsAvailable
        {
            get { return noSearchResultsAvailable; }
            set { Set(ref noSearchResultsAvailable, value); }
        }

        public int SelectedIndex
        {
            get { return selectedIndex; }
            set
            {
                Set(ref selectedIndex, value);

                if (selectedIndex > -1 && Contacts.Count > 0 && selectedIndex < Contacts.Count)
                {
                    var item = Contacts[selectedIndex];
					//2015.10.13 - NOTE: With recent corrections, item.PhoneNumber will ALWAYS be empty.
                    MessengerInstance.Send<ShowContactFaxesMessage>(new ShowContactFaxesMessage { ContactKey = item.ContactKey, CmGroup = item.CmGroup, PhoneNumber = item.PhoneNumber, LocalTimestamp = 0, IsComposing = true });
                }
            }
        }

        public ICommand CloseComposeFaxCommand { get; set; }

        #endregion

        #region | Private Procedures |

        private void LoadContacts()
        {
            ContactViewModel cvm = null;
            List<ContactViewModel> list = new List<ContactViewModel>();

            Contacts.Clear();

            ContactList cl = ContactManager.getInstance().getContacts( searchContacts, ContactFilter.All );

            foreach(Contact contact in cl)
            {
                cvm = FromContactModel(contact, false);
                list.Add(cvm);
            }

            if (list.Count > 0)
            {
                foreach (ContactViewModel contact in list)
                {
                    Contacts.Add(contact);
                }
            }
            else
            {
                if (PhoneUtils.IsValidNumber(SearchContacts))
                {
                    cvm = FromContactModel(new Contact { Name = SearchContacts, PhoneNumber = SearchContacts }, true);

                    Contacts.Add(cvm);
                }
            }

            NoSearchResultsAvailable = (Contacts.Count == 0);
        }

        private ContactViewModel FromContactModel( Contact contact, bool isNewContact )
        {
            ContactViewModel cvm = new ContactViewModel();

			cvm.CmGroup				= contact.CmGroup;
            cvm.NameOrNumber		= isNewContact ? contact.Name : Desktop.Util.PhoneUtils.FormatPhoneNumber(contact.Name);	//TODO: use Contact.bestDisplayName()?
            cvm.IsRegisteredUser	= (contact.VoxoxCount > 0);
			cvm.InNetworkImageUri	= Config.UI.InNetworkContact;
//          cvm.PhoneNumber			= contact.PhoneNumber;		//This is invalid at Contact-level

            cvm.AvatarUri			= AvatarManager.Instance.GetUriForContact( contact.ContactKey );
            cvm.LastSeen			= (contact.VoxoxCount > 0) ? "Last online on May 15" : "";            //TODO: We do not get this data from DB yet.

            return cvm;
        }

        private void OnCloseCompose()
        {
            MessengerInstance.Send<FaxComposeMessage>(new FaxComposeMessage { Show = false });
        }

        #endregion
    }
}
