﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;				//SessionManager, ConnectionMessage
using Desktop.Model.Calls;			//CallFilterType
using Desktop.ViewModels.Profile;	

using GalaSoft.MvvmLight;			 //ViewModelBase, MessengerInstance, Set
using GalaSoft.MvvmLight.CommandWpf; //RelayCommand

using System;						//Boolean, lamba
using System.Windows.Input;			//ICommand
using System.Windows.Media;			//Colors, SolidColorBrush

//=============================================================================
// NOTES on Navigation logic:
//	- Navigation requires that we keep the NavBar and main window in sync.
//	- Navigation can occur from clicking NavBar, clicking View menu, or elsewhere in app.
//	- Due to above, we have moved all navigation messaging to ActionManager, which is 
//	  not the ONLY place where a navigation related message should be Sent.
//	- This will:
//		- keep the logic in both MainTabNavViewModel (this class) and 
//		  MainWindow (the center view) much simpler
//		- Keep the navigation logic itself simpler
//		- Allow common code to be used for all navigation
//		- Provide better encapsulation for the navigation requirements.
//
//	- Since our MvvmLight Messenger does not actually use an ascync messaging
//	  system, at least not as we have implemented it, any overhead incurred
//	  by this is about 5 function/method calls.
//=============================================================================
namespace Desktop.ViewModels.Main
{
    /// <summary>
    /// Main Tabbed Navigation View Model
    /// </summary>
    public class MainTabNavViewModel : ViewModelBase
    {
        #region | Private Class Variables |

		//Navigation related
        private ActiveTabEnum	activeTab;
        private ActiveTabEnum	focusTab;
        private Boolean			isDialer		= false;
        private bool			isProfileActive = false;

		private string			avatarUri;
        private bool			isFaxAvailable;
        private bool			isStoreAvailable;

		//TEMP for displaying connectivity
		private bool	mInternetConnected	= false;
		private bool	mSipConnected		= false;	//Actually 'Registered'
		private bool	mXmppConnected		= false;

		private SolidColorBrush mForegroundInternet;
		private SolidColorBrush mForegroundSip;
		private SolidColorBrush mForegroundXmpp;

		private SolidColorBrush connectedBrush      = new SolidColorBrush( Colors.Green  );
		private SolidColorBrush mixedConnectedBrush = new SolidColorBrush( Colors.Yellow );
		private SolidColorBrush disconnectedBrush   = new SolidColorBrush( Colors.Red    );
		//End TEMP

        #endregion

        #region | Constructors |

        public MainTabNavViewModel()
        {
            MessagesCommand = new RelayCommand<Navigation>( (Navigation) => OnNavCommand(Navigation.Messages) );
            CallsCommand	= new RelayCommand<Navigation>( (Navigation) => OnNavCommand(Navigation.Calls	) );
            FaxCommand		= new RelayCommand<Navigation>( (Navigation) => OnNavCommand(Navigation.Fax		) );
            ContactsCommand = new RelayCommand<Navigation>( (Navigation) => OnNavCommand(Navigation.Contacts) );
            CartCommand		= new RelayCommand<Navigation>( (Navigation) => OnNavCommand(Navigation.Store	) );
            SettingsCommand = new RelayCommand<Navigation>( (Navigation) => OnNavCommand(Navigation.Settings) );
			DialerCommand	= new RelayCommand<Navigation>( (Navigation) => OnNavCommand(Navigation.Dialer  ) );
            ProfileCommand	= new RelayCommand<Navigation>( (Navigation) => OnNavCommand(Navigation.Profile	) );

            AvatarUri			= SessionManager.Instance.User.AvatarUri;
            IsFaxAvailable		= SessionManager.Instance.User.Options.IsFaxAvailable;
            IsStoreAvailable    = SessionManager.Instance.User.Options.IsStoreAvailable;

			InternetConnected	= SessionManager.Instance.InternetConnected;
			SipConnected		= SessionManager.Instance.SipConnected;
			XmppConnected		= SessionManager.Instance.XmppConnected;

            MessengerInstance.Register<MainTabNavigateMessage>	( this, OnMainTabNavigate );
            MessengerInstance.Register<ProfileCloseMessage>		( this, OnProfileClose );
            MessengerInstance.Register<KeypadRegionClickMessage>( this, (KeypadRegionClickMessage msg) => { isDialer = false; });
			MessengerInstance.Register<ConnectionMessage>		( this, OnConnectionChange );
        }

        public override void Cleanup()
        {
            base.Cleanup();
        }

        #endregion

        #region | Public Properties & Actions|

        public ICommand MessagesCommand { get; set; }
        public ICommand CallsCommand	{ get; set; }
        public ICommand FaxCommand		{ get; set; }
        public ICommand ContactsCommand { get; set; }
        public ICommand DialerCommand	{ get; set; }
        public ICommand CartCommand		{ get; set; }
        public ICommand SettingsCommand { get; set; }
        public ICommand ProfileCommand	{ get; set; }

        public ActiveTabEnum ActiveTab
        {
            get { return activeTab; }
            set { 
					bool changed = (activeTab != value);
					Set(ref activeTab, value); 
					SessionManager.Instance.ActiveNavTab = activeTab;

					if ( changed )
						handleActiveTabChange( activeTab );
				}
        }

        public string AvatarUri
        {
            get { return avatarUri; }
            set { Set(ref avatarUri, value); }
        }

        public bool IsFaxAvailable
        {
            get { return isFaxAvailable; }
            set { Set(ref isFaxAvailable, value); }
        }

        public bool IsStoreAvailable
        {
            get { return isStoreAvailable; }
            set { Set(ref isStoreAvailable, value); }
        }

        public bool IsProfileActive
        {
            get { return isProfileActive; }
            set { Set(ref isProfileActive, value); }
        }

        public ActiveTabEnum FocusTab
        {
            get { return focusTab; }
            set { Set(ref focusTab, value); }
        }

		public bool InternetConnected
		{
			get { return mInternetConnected; }
			set {  
					Set( ref mInternetConnected, value );
					ForegroundInternet = getBrush( value );

					//Since Internet affects others, update them as well
					ForegroundSip		= getBrush( SipConnected  );
					ForegroundXmpp		= getBrush( XmppConnected );
				}
		}

		public bool SipConnected
		{
			get { return mSipConnected; }
			set {  
					Set( ref mSipConnected, value ); 
					ForegroundSip = getBrush( value );
				}
		}

		public bool XmppConnected
		{
			get { return mXmppConnected; }
			set {  
					Set( ref mXmppConnected, value ); 
					ForegroundXmpp = getBrush( value );
				}
		}

		private SolidColorBrush getBrush( bool value )
		{
			SolidColorBrush result = ( value ? connectedBrush : disconnectedBrush );

			//Test code - In some cases, we see Internet disconnected, but no positive disconnect from SIP,
			//	or a delayed disconnect from XMPP.  Allow for this.
			if ( ! InternetConnected && value )
				result = mixedConnectedBrush;

			return result;
		}

		//Foreground properties for connectivity widgets.
		public SolidColorBrush ForegroundInternet
		{
			get {  return mForegroundInternet; }
			set { Set( ref mForegroundInternet, value );  }
		}

		public SolidColorBrush ForegroundSip			
		{
			get {  return mForegroundSip; }
			set { Set( ref mForegroundSip, value );  }
		}

		public SolidColorBrush ForegroundXmpp			
		{ 
			get { return mForegroundXmpp;}
			set { Set( ref mForegroundXmpp, value );  }
		}

        #endregion

        #region | Private Procedures |

		private void OnNavCommand( Navigation navTo )
		{
			ActionManager.Instance.NavigateTo( navTo );
		}

		private void OnMainTabNavigate( MainTabNavigateMessage msg )
		{
			handleNav( msg.NavigateTo );
		}

		private void OnProfileClose( ProfileCloseMessage msg )
		{
			IsProfileActive = false;
		}

		private void handleActiveTabChange( ActiveTabEnum activeTab )
		{
			Navigation navTo = Navigation.Messages;

			//Convert ActiveTabEnum to Navigation and call OnNavCommand
			switch ( activeTab )
			{
			case ActiveTabEnum.Calls:
				navTo = Navigation.Calls;
				break;
			case ActiveTabEnum.Contacts:
				navTo = Navigation.Contacts;
				break;
			case ActiveTabEnum.Dialer:
				navTo = Navigation.Dialer;
				break;
			case ActiveTabEnum.Fax:
				navTo = Navigation.Fax;
				break;
			case ActiveTabEnum.Messages:
				navTo = Navigation.Messages;
				break;

			default:
				break;

			}

			OnNavCommand( navTo );
		}

		private void handleNav( Navigation navigateTo )
		{
			switch ( navigateTo )
			{
			//These navigate to new tab AND set focus to that tab.
			case Navigation.Calls:
				handleNavTabClick( ActiveTabEnum.Calls, Navigation.Calls, true );
				break;
			case Navigation.Contacts:
				handleNavTabClick( ActiveTabEnum.Contacts, Navigation.Contacts, true );
				break;
			case Navigation.Messages:
				handleNavTabClick( ActiveTabEnum.Messages, Navigation.Messages, true );
				break;
			case Navigation.Fax:
				handleNavTabClick( ActiveTabEnum.Fax, Navigation.Fax, true );
				break;

			//These do take some action, but do not change focus.
			case Navigation.Store:
				handleNavTabClick( ActiveTab, Navigation.Store, false );
				break;
			case Navigation.Settings:
				handleNavTabClick( ActiveTab, Navigation.Settings, false );
				break;

			//These navigate to new tab, and temporarily set focus to that tab.
			case Navigation.Dialer:
				handleNavDialer();
				break;
			case Navigation.Profile:
				handleNavProfile();
				break;

			default:
				//TODO: log warning 
				break;
			}
		}

		private void handleNavTabClick( ActiveTabEnum newTab, Navigation navigateTo, bool updateActiveTab )
		{
            FocusTab = newTab;

			//Some tab clicks simply perform an action, but do not become the ActiveTab
			if ( updateActiveTab )
			{ 
				if ( ActiveTab != newTab )
				{
					ActiveTab = newTab;
				}
			}
		}

		private void handleNavDialer()
        {
            if (isDialer)
            {
                isDialer = false;
				ActionManager.Instance.CloseDialer();
            }
            else
            {
                isDialer = true;
				ActionManager.Instance.NavigateToDialer();
           }
        }

        private void handleNavProfile()
        {
            IsProfileActive = !IsProfileActive;
        }
		
		private void OnConnectionChange( ConnectionMessage msg )
		{
			if		( msg.isInternet() )
			{
				InternetConnected = msg.Connected;
			}
			else if ( msg.isXmpp() )
			{
				XmppConnected = msg.Connected;
			}
			else if ( msg.isSip() )
			{
				SipConnected = msg.Connected;
			}
		}

        #endregion
    }

    /// <summary>
    /// Application Main Navigation Enumeration
    /// </summary>
    public enum Navigation
    {
        Messages,
        Calls,
        Fax,
        Contacts,
        Dialer,
        Store,
        Settings,
        Profile
    }

    /// <summary>
    /// Active Tab Enumeration
    /// </summary>
    public enum ActiveTabEnum : int
    {
        Messages,
        Calls,
        Fax,
        Contacts,
        Dialer
    }

    /// <summary>
    /// Navigation broadcast message
    /// </summary>
    public class MainTabNavigateMessage
    {
		public MainTabNavigateMessage()
		{ 
			CallFilter = CallFilterType.All;
		}

        public Navigation		NavigateTo	{ get; set; }
		public CallFilterType	CallFilter	{ get; set; }	//So we can navigate to filtered calls.
    }

	public class KeypadRegionClickMessage
	{
	}
}
