﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;				//SessionManager

using GalaSoft.MvvmLight;			 //ViewModelBase, MessengerInstance, Set
using GalaSoft.MvvmLight.CommandWpf; //RelayCommand

using System.Windows.Input;			//ICommand

namespace Desktop.ViewModels.Main
{
    public class ConfirmBlockContactViewModel : ViewModelBase
    {
        #region | Private Class Variables |

		private string	contactKey;
        private int		cmGroup;
        private string	phoneNumber;
        private string	name;
        private bool	block;

        private string smallTextBlock;
        private string smallTextUnblock;

        #endregion

        #region | Constructors |

		//If ContactKey is invalid, then we use phoneNumber to block a single number.
        public ConfirmBlockContactViewModel( string contactKeyIn, int cmGroupIn, string phoneNumberIn, string nameIn, bool blockIn )
        {
            contactKey  = contactKeyIn;
            cmGroup		= cmGroupIn;
            phoneNumber = phoneNumberIn;
            name		= nameIn;
            block		= blockIn;

            smallTextBlock   = string.Format( LocalizedString.BlockContactConfirm_BlockSmallText,   nameEx() );
            smallTextUnblock = string.Format( LocalizedString.BlockContactConfirm_UnblockSmallText, nameEx() );

            CancelCommand  = new RelayCommand( OnCancel  );
            BlockCommand   = new RelayCommand( OnBlock   );
            UnblockCommand = new RelayCommand( OnUnblock );
        }

        #endregion

        #region | Public Properties |

        public ICommand CancelCommand  { get; set; }
        public ICommand BlockCommand   { get; set; }
        public ICommand UnblockCommand { get; set; }

        public string SmallTextBlock
        {
            get { return smallTextBlock; }
            set { Set(ref smallTextBlock, value); }
        }

        public string SmallTextUnblock
        {
            get { return smallTextUnblock; }
            set { Set(ref smallTextUnblock, value); }
        }

        public bool IsBlock
        {
            get { return block; }
            set { Set(ref block, value); }
        }

        #endregion

        #region | Private Procedures |

        private void OnCancel()
        {
			ActionManager.Instance.CloseConfirmBlockContactPopup();
        }

        private void OnBlock()
        {
			handleBlock( true );
        }

        private void OnUnblock()
        {
			handleBlock( false );
        }

		private void handleBlock( bool isBlock )
		{
			if ( string.IsNullOrEmpty( contactKey ) )
			{
				ActionManager.Instance.BlockNumber( phoneNumber, isBlock);
			}
			else 
			{ 
				ActionManager.Instance.BlockContact( contactKey, isBlock);
			}

            OnCancel();
		}

		private string nameEx()
		{
			return ( string.IsNullOrEmpty( name ) ? phoneNumber : name );
		}

        #endregion
    }
}
