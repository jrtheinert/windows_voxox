﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;				//ActionManager, NoAudioDevicesMessage

using GalaSoft.MvvmLight;			 //ViewModelBase, MessengerInstance, Set
using GalaSoft.MvvmLight.CommandWpf; //RelayCommand

using System;						//Boolean
using System.Windows.Input;			//ICommand

namespace Desktop.ViewModels.Main
{
    public class NoAudioDevicesViewModel : ViewModelBase
    {
        #region | Private Class Variables |

        private bool isAfterLogin;

        #endregion

        #region | Constructors |

        public NoAudioDevicesViewModel()
        {
            SettingsCommand = new RelayCommand<Boolean>( (Boolean) => handleCommand( true  ) );
            OkCommand       = new RelayCommand<Boolean>( (Boolean) => handleCommand( true  ) );
            CancelCommand   = new RelayCommand<Boolean>( (Boolean) => handleCommand( false ) );

            MessengerInstance.Register<NoAudioDevicesMessage>(this, (NoAudioDevicesMessage msg) => { IsAfterLogin = msg.IsAfterLogin; });
        }

        #endregion

        #region | Public Properties |

        public ICommand SettingsCommand { get; set; }
        public ICommand CancelCommand	{ get; set; }
        public ICommand OkCommand		{ get; set; }

        public bool IsAfterLogin
        {
            get { return isAfterLogin; }
            set { Set(ref isAfterLogin, value); }
        }

        #endregion

        #region | Private Procedures |

		private void handleCommand( Boolean showSettings )
		{
			ActionManager.Instance.ShowNoAudioDevicePopup( false, false ); //Show, isAfterLogin

			if ( showSettings )
			{
				ActionManager.Instance.NavigateToSettings();
			}
		}

        #endregion
    }
}
