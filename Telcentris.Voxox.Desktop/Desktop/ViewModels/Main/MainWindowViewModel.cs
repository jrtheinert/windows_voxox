﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;						//SessionManager, internal messages.
using Desktop.Model.Calling;				//PhoneCallMessage
using Desktop.Model.Calls;					//CallFilterType
using Desktop.Model.Messaging;				//MessageManager - To mark VM as 'READ'

using Desktop.ViewModels.Calling;			//CallsDoneMessage, CallsViewModel
using Desktop.ViewModels.Calls;				//CallsMainViewModel, ShowCallDetailMessage, CallFilterType
using Desktop.ViewModels.Contacts;			//ContactsMainViewModel, ShowContactDetailMessage
using Desktop.ViewModels.Faxes;				//FaxesMainViewModel, ShowContactFaxesMessage
using Desktop.ViewModels.Dialer;			//DialPadViewModel, DialPadPointerSetMessage, DialPadShowMessage, DialpadCloseMessage
using Desktop.ViewModels.Messages;			//MessagesMainViewModel, VoicemailReviewViewModel, ShowContactMessagesMessage, VoicemailMessage, VoicemailDoneMessage, VoicemailPlayClickedMessage, PhoneNumbersCloseMessage, PhoneNumbersShowMessage
using Desktop.ViewModels.Profile;			//ProfileViewModel
using Desktop.ViewModels.Settings;			//SettingsViewModel

using ManagedDataTypes;						//Contact
using Telcentris.Voxox.ManagedSipAgentApi.ManagedSipDataTypes;	//For PhoneCallState.  

using GalaSoft.MvvmLight;					//ViewModelBase, MessengerInstance, Set
using GalaSoft.MvvmLight.CommandWpf;		//RelayCommand
using GalaSoft.MvvmLight.Threading;			//DispatcherHelper

using System;								//Convert, Boolean, Int64
using System.Windows;						//Point
using System.Windows.Input;					//ICommand
using System.Windows.Media;					//PointCollection

namespace Desktop.ViewModels.Main
{
    public class MainWindowViewModel : ViewModelBase
    {
        #region | MVVM Register Tokens |

        #endregion

        #region | Private Class Variables |

        MainTabNavViewModel		tabNavVM;					//Left navbar

        ViewModelBase			mainPane;					//Left panel that loads other 'left' panels below.
        ViewModelBase			callPane;					//Right panel - incoming call or call in-progress
        ViewModelBase			voicemailReviewPane;		//Right panel - Play VM and Recorded call
        ViewModelBase			contactInfoPane;			//Right panel - Review Contact Detail from message thread.

		SettingsViewModel		SettingsVM;					//Non-modal dialog
        DialPadViewModel		dialPadViewModel;			//Popup within main window
        CloseAppInCallViewModel closeAppInCallViewModel;	//Popup within main window
        NoAudioDevicesViewModel noAudioDevicesViewModel;	//Popup within main window

        ConfirmMessageDeleteViewModel    confirmMessageDeleteViewModel; // popup within main window
        ConfirmBlockContactViewModel     confirmBlockContactViewModel; // popup within main window

        private MessagesMainViewModel	 messagesMainViewModel;		//Left panel
        private CallsMainViewModel		 callsMainViewModel;		//Left panel
        private ContactsMainViewModel	 contactsMainViewModel;		//Left panel
        private FaxesMainViewModel		 faxesMainViewModel;		//Left panel
        private VoicemailReviewViewmodel voicemailsVM;				//Left panel
        private ProfileViewModel		 profileViewModel;			//Left panel

        private bool isFaxAvailable;
        private bool showDialPad;		//Not displayed as other popups are.

		//Control display of various popups
		private bool showMainOverlay				= false;
        private bool showCloseAppInCall				= false;
		private bool showFindMeNumbersNotSetupPopup = false;
        private bool showNoAudioDevices				= false;
        private bool showPhoneNumbers				= false;
        private bool showUpgradePlanPopup			= false;
        private bool showConfirmMessageDelete       = false;
        private bool showConfirmBlockContact        = false;

        // Variable used for ignoring recursive "Show" notifications from MVVM
        private bool isBuildingMessages = false;
        private bool isBuildingCalls	= false;
        private bool isBuildingContacts = false;
        private bool isBuildingFaxes	= false;

        private bool autoLogin;

		//To refresh ContactInfo panel, if needed.
		private bool	refreshContactInfoPanel = false;
		private int		refreshCmGroup          = Contact.INVALID_CMGROUP;
		private string	refreshPhoneNumber;

        #endregion

        #region Constructors & clean-up

        public MainWindowViewModel()
        {
            tabNavVM   = new MainTabNavViewModel();
            SettingsVM = SettingsViewModel.Instance;

            SetupNavigation();

            dialPadViewModel = new DialPadViewModel();
            showDialPad		 = false;

            // Active state, defaults to Messages
            tabNavVM.ActiveTab = ActiveTabEnum.Messages;
            OnShowContactMessages(null);

            closeAppInCallViewModel = new CloseAppInCallViewModel();
            noAudioDevicesViewModel = new NoAudioDevicesViewModel();
            profileViewModel		= new ProfileViewModel();

            IsFaxAvailable	= SessionManager.Instance.User.Options.IsFaxAvailable;
            AutoLogin		= SettingsManager.Instance.App.AutoLogin;

	        MessengerInstance.Register<AnimationDoneMessage>    ( this, OnAnimationDone     );
            MessengerInstance.Register<DialPadPointerSetMessage>( this, OnDialPadPointerSet );


            MessengerInstance.Register<MessageDeleteMessage>(this, OnMessageDelete);
            MessengerInstance.Register<ContactBlockMessage>(this, OnBlockContact);
        }

        public override void Cleanup()
        {
            base.Cleanup();

            if (TabNavVM != null)
            {
                TabNavVM.Cleanup();
                TabNavVM = null;
            }

            if (MainPane != null)
            {
                MainPane.Cleanup();
                MainPane = null;
            }

            if (CallPane != null)
            {
                CallPane.Cleanup();
                CallPane = null;
            }

            ShowDialPad = false;
            if (DialPad != null)
            {
                DialPad.Cleanup();
            }
        }

        #endregion

        #region | Public Properties & Commands|

        /// <summary>
        /// Navigation View Model
        /// </summary>
        public MainTabNavViewModel TabNavVM
        {
            get { return tabNavVM; }
            set { Set(ref tabNavVM, value); }
        }

        /// <summary>
        /// Main Pane that loads Contacts, Messages or Calls screens
        /// </summary>
        public ViewModelBase MainPane
        {
            get { return mainPane; }
            set { Set(ref mainPane, value); }
        }

        public ViewModelBase CallPane
        {
            get { return callPane; }
            set { Set(ref callPane, value); }
        }

        public DialPadViewModel DialPad
        {
            get { return dialPadViewModel; }
            set { Set(ref dialPadViewModel, value); }
        }

        public ViewModelBase VoicemailReviewPane
        {
            get { return voicemailReviewPane; }
            set {Set(ref voicemailReviewPane, value);}
        }

        public ViewModelBase ContactInfoPane
        {
            get { return contactInfoPane; }
            set {Set(ref contactInfoPane, value);}
        }

        public CloseAppInCallViewModel CloseAppInCall
        {
            get { return closeAppInCallViewModel; }
            set { Set(ref closeAppInCallViewModel, value); }
        }

        public NoAudioDevicesViewModel NoAudioDevices
        {
            get { return noAudioDevicesViewModel; }
            set { Set(ref noAudioDevicesViewModel, value); }
        }

        public ProfileViewModel Profile
        {
            get { return profileViewModel; }
            set { Set(ref profileViewModel, value); }
        }

        public bool IsFaxAvailable
        {
            get { return isFaxAvailable; }
            set { Set(ref isFaxAvailable, value); }
        }

		//Dialpad does NOT use the ShowMainOverlay property
        public bool ShowDialPad
        {
            get { return showDialPad; }
            set { Set(ref showDialPad, value); }
        }

		//For these properties, that show/hide various popups, we will also set 'ShowMainOverlay' so
		//	we get a consistent modal background in main window when the popup is visible.
        public bool ShowMainOverlay
        {
            get { return showMainOverlay; }
            set { Set(ref showMainOverlay, value); }
        }

		public bool ShowPhoneNumbers
		{
            get { return showPhoneNumbers; }
            set 
			{ 
				Set(ref showPhoneNumbers, value); 
				ShowMainOverlay = value;
			}
        }

        public bool ShowCloseAppInCall
        {
            get { return showCloseAppInCall; }
            set 
			{ 
				Set(ref showCloseAppInCall, value); 
				ShowMainOverlay = value;
			}
        }

        public bool ShowUpgradePlanPopup
        {
            get { return showUpgradePlanPopup; }
            set 
			{ 
				Set(ref showUpgradePlanPopup, value); 
				ShowMainOverlay = value;
			}
        }

		public bool ShowFindMeNumbersNotSetupPopup
		{
            get { return showFindMeNumbersNotSetupPopup; }
            set 
			{ 
				Set(ref showFindMeNumbersNotSetupPopup, value); 
				ShowMainOverlay = value;
			}
		}

        public bool ShowNoAudioDevices
        {
            get { return showNoAudioDevices; }
            set 
			{ 
				Set(ref showNoAudioDevices, value); 
				ShowMainOverlay = value;
			}
        }
		//End special handling for pop-ups

		public bool AutoLogin
		{
			get { return autoLogin; }
			set { Set(ref autoLogin, value); }
		}

        public ConfirmMessageDeleteViewModel ConfirmMessageDelete
        {
            get { return confirmMessageDeleteViewModel; }
            set { Set(ref confirmMessageDeleteViewModel, value); }
        }

        public bool ShowConfirmMessageDelete
        {
            get { return showConfirmMessageDelete; }
            set
            {
                Set(ref showConfirmMessageDelete, value);
                ShowMainOverlay = value;
            }
        }

        public ConfirmBlockContactViewModel ConfirmBlockContact
        {
            get { return confirmBlockContactViewModel; }
            set { Set(ref confirmBlockContactViewModel, value); }
        }

        public bool ShowConfirmBlockContact
        {
            get { return showConfirmBlockContact; }
            set
            {
                Set(ref showConfirmBlockContact, value);
                ShowMainOverlay = value;
            }
        }

        #endregion

        #region private navigation methods

        private void SetupNavigation()
        {
            MessengerInstance.Register<MainTabNavigateMessage>			(this, OnNavigateTo				);
            MessengerInstance.Register<ShowContactMessagesMessage>		(this, ActionManager.MESSAGES_TOKEN, OnShowContactMessages);
            MessengerInstance.Register<DialPadCloseMessage>				(this, OnDialerClose			);
            MessengerInstance.Register<PhoneCallMessage>				(this, OnShowCallPane			);
            MessengerInstance.Register<CallsDoneMessage>				(this, OnHideCallPane			);
            MessengerInstance.Register<VoicemailMessage>				(this, OnPlayVoicemail			);
            MessengerInstance.Register<VoicemailDoneMessage>			(this, OnHideVoicemailReview	);
            MessengerInstance.Register<VoicemailPlayClickedMessage>		(this, OnVoicemailPlayed		);
            MessengerInstance.Register<ShowContactInfoMessage>			(this, OnShowContactInfo		);
            MessengerInstance.Register<SettingsChangeMessage>           (this, OnSettingsChange			);

			//These show/hide various pop-ups.
            MessengerInstance.Register<DialPadShowMessage>				(this, (DialPadShowMessage				msg) => { ShowDialPad = true; tabNavVM.FocusTab = ActiveTabEnum.Dialer; });
            MessengerInstance.Register<PhoneNumbersCloseMessage>		(this, (PhoneNumbersCloseMessage		msg) => { ShowPhoneNumbers					= false; });
            MessengerInstance.Register<PhoneNumbersShowMessage>			(this, (PhoneNumbersShowMessage			msg) => { ShowPhoneNumbers					= true; });
            MessengerInstance.Register<ShowUpgradePlanMessage>			(this, (ShowUpgradePlanMessage			msg) => { ShowUpgradePlanPopup				= msg.ShowPopUp; });
            MessengerInstance.Register<ShowFindMeNumberNotSetupMessage>	(this, (ShowFindMeNumberNotSetupMessage msg) => { ShowFindMeNumbersNotSetupPopup	= msg.ShowPopUp; });
            MessengerInstance.Register<CloseAppInCallMessage>			(this, (CloseAppInCallMessage			msg) => { ShowCloseAppInCall				= msg.ShowPopup; });
            MessengerInstance.Register<NoAudioDevicesMessage>			(this, (NoAudioDevicesMessage			msg) => { ShowNoAudioDevices				= msg.ShowPopup; });
        }

        /// <summary>
        /// Navigate app to selected tab
        /// </summary>
        /// <param name="msg">Message containing the tab to navigate to</param>
        private void OnNavigateTo( MainTabNavigateMessage msg )
        {
            switch (msg.NavigateTo)
            {
                case Navigation.Messages:
                    OnShowContactMessages(null);
                    break;
                case Navigation.Calls:
                    ShowCallDetails(null);
					callsMainViewModel.CallList.FilterCallsBy = msg.CallFilter;
                    break;
                case Navigation.Fax:
                    ShowFaxDetails(null);
                    break;
                case Navigation.Contacts:
                    ShowContactDetails(null);
                    break;
                case Navigation.Dialer:
					ActionManager.Instance.ShowDialer( false, -1, null,null );
                    break;
                case Navigation.Store:
					ActionManager.Instance.ShowStore();
                    break;
                case Navigation.Settings:
//					SettingsVM.AudioCommand.Execute( null );	//ShowSettings();	//Handled in ActionManager.
                    break;
                case Navigation.Profile:
                    ShowProfile();
                    break;
            }
        }

		#endregion

		#region Middle panel show/hide methods

        private void OnShowContactMessages( ShowContactMessagesMessage msg )
        {
            if (isBuildingMessages == false)
            {
                if (messagesMainViewModel == null)
                {
                    isBuildingMessages = true;

                    messagesMainViewModel = new MessagesMainViewModel();

                    if (msg != null)
                    {
                        messagesMainViewModel.ShowContactMessages(msg);
                    }

                    isBuildingMessages = false; // Might not be needed
                }
            }

            tabNavVM.ActiveTab = ActiveTabEnum.Messages;
            MainPane = messagesMainViewModel;
        }

        private void ShowCallDetails(ShowCallDetailMessage msg)
        {
            if (isBuildingCalls == false)
            {
                if (callsMainViewModel == null)
                {
                    isBuildingCalls = true;

                    callsMainViewModel = new CallsMainViewModel();

                    if (msg != null)
                    {
                        callsMainViewModel.OnShowCallDetail(msg);
                    }

                    isBuildingCalls = false; // Might not be needed
                }
				else
				{
					callsMainViewModel.markItemsRead();
				}
            }

            tabNavVM.ActiveTab = ActiveTabEnum.Calls;
            MainPane = callsMainViewModel;
        }

        private void ShowContactDetails(ShowContactDetailMessage msg)
        {
            if (isBuildingContacts == false)
            {
                if (contactsMainViewModel == null)
                {
                    isBuildingContacts = true;

                    contactsMainViewModel = new ContactsMainViewModel();

                    if (msg != null)
                    {
                        contactsMainViewModel.ShowContactDetail(msg);
                    }

                    isBuildingContacts = false; // Might not be needed
                }                
            }

            tabNavVM.ActiveTab = ActiveTabEnum.Contacts;
            MainPane = contactsMainViewModel;
        }

        private void ShowProfile()
        {
            Profile = new ProfileViewModel();
        }

		#endregion


		#region Right panel show/hide methods

		private void ShowFaxDetails(ShowContactFaxesMessage msg)
        {
            if (isBuildingFaxes == false)
            {
                if (faxesMainViewModel == null)
                {
                    isBuildingFaxes = true;

                    faxesMainViewModel = new FaxesMainViewModel();

                    if (msg != null)
                    {
                        
                    }

                    isBuildingFaxes = false; // Might not be needed
                }
            }

            tabNavVM.ActiveTab = ActiveTabEnum.Fax;
            MainPane = faxesMainViewModel;
        }

        private void OnShowCallPane(PhoneCallMessage msg)
        {
            if (VoicemailReviewPane != null)
                VoicemailReviewPane = null;

            if (CallPane == null)
            {
                if (msg.State != PhoneCallState.CallState.Closed &&
                    msg.State != PhoneCallState.CallState.Missed &&
                    msg.State != PhoneCallState.CallState.Error  &&
                    msg.State != PhoneCallState.CallState.Unknown)
                {
                    var callsVM = new CallsViewModel(msg);
                    CallPane = callsVM;
                }
            }
        }

        private void OnHideCallPane(CallsDoneMessage msg)
        {
            CallPane = null;
            if (ShowUpgradePlanPopup)
                ShowUpgradePlanPopup = false;
        }

		//Contact phone numbers from Message Pane.
		private void OnShowContactInfo( ShowContactInfoMessage msg )
		{
			//If we have Show == true and Refresh == true, then we close and re-open the VM.
			//Otherwise, we do normal handling.
			if ( msg.Show && msg.Refresh )
			{
				//Do this only IF ContactInfoPane is not null, indicating the window is already open.
				if ( ContactInfoPane != null )
				{ 
					refreshContactInfoPanel = true;
					refreshCmGroup          = msg.CmGroup;
					refreshPhoneNumber		= msg.PhoneNumber;
					MessengerInstance.Send<StartAnimationMessage>(new StartAnimationMessage { AnimationReferenceID = ((ContactInfoViewModel)ContactInfoPane).AnimationReferenceID });
				}
			}
			else if ( msg.Show )
			{
				var contactInfoVM = new ContactInfoViewModel( msg.CmGroup, msg.PhoneNumber );
				ContactInfoPane = contactInfoVM;
			}
			else
			{
                MessengerInstance.Send<StartAnimationMessage>(new StartAnimationMessage { AnimationReferenceID = ((ContactInfoViewModel)ContactInfoPane).AnimationReferenceID });
			}
		}
       
		private void OnDialPadPointerSet ( DialPadPointerSetMessage msg )
		{
            dialPadViewModel.PointerPoints = new PointCollection { 
																	new Point { X = 0, Y = msg.DialpadPointerVerticalOffset		 }, 
																	new Point { X = 8, Y = msg.DialpadPointerVerticalOffset - 10 }, 
																	new Point { X = 8, Y = msg.DialpadPointerVerticalOffset + 10 } 
																 };
		}

		private void OnAnimationDone( AnimationDoneMessage msg )
		{
            if ( ContactInfoPane != null && msg.AnimationReferenceID == ((ContactInfoViewModel)ContactInfoPane).AnimationReferenceID )
			{ 
			    ContactInfoPane = null;

				//Handle the refresh.
				if ( refreshContactInfoPanel )
				{
					int		tempCmGroup		= refreshCmGroup;
					string	tempPhoneNumber = refreshPhoneNumber;

					refreshContactInfoPanel = false;
					refreshCmGroup			= Contact.INVALID_CMGROUP;
					refreshPhoneNumber		= null;

					//Without this, the new object does not execute opening animation and remains out of sight.
					DispatcherHelper.RunAsync( () =>
					{
						OnShowContactInfo( new ShowContactInfoMessage { Show = true, Refresh = false, CmGroup = tempCmGroup, PhoneNumber = tempPhoneNumber } );	
					} );
				}
			}
		}

		//This is for Voicemail and recorded calls
        private void OnPlayVoicemail(VoicemailMessage msg)
        {
            if (CallPane == null)
            {
				voicemailsVM = new VoicemailReviewViewmodel(msg);
				VoicemailReviewPane = voicemailsVM;
            }
        }

		//This is for Voicemail and recorded calls
        public void OnHideVoicemailReview(VoicemailDoneMessage msg)
        {
            VoicemailReviewPane = null;
        }

        private void OnMessageDelete(MessageDeleteMessage msg)
        {
            if(msg.ShowConfirm)
            {
                ConfirmMessageDelete = new ConfirmMessageDeleteViewModel(msg.CmGroup, msg.PhoneNumber, msg.Name);
                ShowConfirmMessageDelete = true;
            }
            else
            {
                ShowConfirmMessageDelete = false;
                ConfirmMessageDelete = null;                
            }
        }

        private void OnBlockContact( ContactBlockMessage msg )
        {
            if (msg.ShowConfirm)
            {
                ConfirmBlockContact     = new ConfirmBlockContactViewModel( msg.ContactKey, msg.CmGroup, msg.PhoneNumber, msg.Name, msg.Block );
                ShowConfirmBlockContact = true;
            }
            else
            {
                ShowConfirmBlockContact = false;
                ConfirmBlockContact     = null;
            }
        }

		private void OnSettingsChange( SettingsChangeMessage msg )
		{
			if ( msg.isForAutoLogin() )
			{
				AutoLogin = SettingsManager.Instance.App.AutoLogin;
			}
		}

		#endregion


		#region private command handlers

        private void OnDialerClose(DialPadCloseMessage msg)
        {
            ShowDialPad = false; 
            tabNavVM.FocusTab = tabNavVM.ActiveTab;
            MessengerInstance.Send<KeypadRegionClickMessage>(new KeypadRegionClickMessage());
        }

		#endregion

		#region Data handling methods

		private void OnVoicemailPlayed( VoicemailPlayClickedMessage msg )
		{
			if ( msg.Timestamp != null )
			{ 
				long timestamp = Convert.ToInt64( msg.Timestamp );

				MessageManager.getInstance().markMessageRead( timestamp, true );
			}
		}

		#endregion
	}
}

namespace Desktop.ViewModels
{
    public class NotImplementedMessage
    {

    }
	
    public class IncomingEventMessage
    {
        public enum EventType
        {
            IncomingCall	= 0,
			MissedCall		= 1,
			Voicemail		= 2,
			Fax				= 3,
			Message			= 4,	//Chat, SMS, or Group Message (when we get to GM)
        }

		public EventType Type { get; set; }

//		public void setTypeIncomingCall()	{  Type = EventType.IncomingCall; }
//		public void setTypeMissedCall()		{  Type = EventType.MissedCall; }
//		public void setTypeVoicemail()		{  Type = EventType.Voicemail; }
//		public void setTypeFax()			{  Type = EventType.Fax; }
//		public void setTypeMessage()		{  Type = EventType.Message; }

		public Boolean isIncomingCall()		{ return Type == EventType.IncomingCall;}
		public Boolean isMissedCall()		{ return Type == EventType.MissedCall;}
		public Boolean isMessage()			{ return !isIncomingCall() && !isMissedCall(); }			//This may be refined later.
    }

    public class UpdateUnreadItemCountsMessage
    {
		public int Messages			{ get; set; }
		public int MissedCalls		{ get; set; }	
		public int Faxes			{ get; set; }
		public int Voicemails		{ get; set; }
    }

	public class BalanceChangedMessage
	{
		public double Balance { get; set; }
	}

	public class MessageListenedToMessage
	{
		public Int64 Timestamp { get; set; }
	}

    public class ShowUpgradePlanMessage
    {
      public  bool ShowPopUp { get; set; }
    }

    public class ShowFindMeNumberNotSetupMessage
    {
      public  bool ShowPopUp { get; set; }
    }

	public class ShowContactInfoMessage
	{
		public bool   Show			{ get; set; }	//Show/Hide
		public bool   Refresh		{ get; set; }	//Addresses refresh handling
		public int	  CmGroup		{ get; set; }	//For DB query
		public string PhoneNumber	{ get; set; }	//Current phonenumber
	}

	public class ChangeContactMessageNumberMessage
	{
		public string PhoneNumber	{ get; set; }	//Newly phonenumber
	}
	
	public class StartAnimationMessage
	{
        public string AnimationReferenceID { get; set; }
        public object DataKey              { get; set; } // Additional data key
        public object Data                 { get; set; } // Additional data
        public bool   SkipAnimation        { get; set; }
	}
	
	public class AnimationDoneMessage
	{
        public string AnimationReferenceID { get; set; }
        public object DataKey              { get; set; }
        public object Data                 { get; set; }
		public object Object               { get; set; }
	}

    public class MessageDeleteMessage
    {
        public int CmGroup			{ get; set; }	//For DB query
        public string PhoneNumber	{ get; set; }	//Current phonenumber
        public string Name			{ get; set; }
        public bool ShowConfirm		{ get; set; }
    }

    public class ContactBlockMessage
    {
        public string ContactKey	{ get; set; }	//For DB query
        public int	  CmGroup		{ get; set; }
        public string PhoneNumber	{ get; set; }	//Current phonenumber
        public string Name			{ get; set; }
        public bool   Block			{ get; set; }
        public bool   ShowConfirm	{ get; set; }
    }
}
