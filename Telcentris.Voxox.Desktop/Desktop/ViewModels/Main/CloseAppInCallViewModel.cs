﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;				//SessionManager

using GalaSoft.MvvmLight;			 //ViewModelBase, MessengerInstance, Set
using GalaSoft.MvvmLight.CommandWpf; //RelayCommand

using System.Windows.Input;			//ICommand

namespace Desktop.ViewModels.Main
{
    public class CloseAppInCallViewModel : ViewModelBase
    {
        #region | Private Class Variables |

        private bool isQuit;

        #endregion

        #region | Constructors |

        public CloseAppInCallViewModel()
        {
            CancelCommand = new RelayCommand(OnCancel);
            CloseCommand  = new RelayCommand(OnClose);

            MessengerInstance.Register<CloseAppInCallMessage>(this, (CloseAppInCallMessage msg) => { isQuit = msg.Quit; });
        }

        #endregion

        #region | Public Properties |

        public ICommand CancelCommand { get; set; }
        public ICommand CloseCommand  { get; set; }

		//public bool IsQuit
		//{
		//	get { return isQuit; }
		//	set { Set(ref isQuit, value); }
		//}

        #endregion

        #region | Private Procedures |

        private void OnCancel()
        {
			ActionManager.Instance.CloseAppInCallPopup();
        }

        private void OnClose()
        {
            SessionManager.Instance.CloseAllPhoneCalls();

			ActionManager.Instance.LogoutOrQuit( isQuit );
        }

        #endregion
    }
}
