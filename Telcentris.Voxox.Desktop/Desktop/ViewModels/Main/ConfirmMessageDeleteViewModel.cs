﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;				//SessionManager

using GalaSoft.MvvmLight;			 //ViewModelBase, MessengerInstance, Set
using GalaSoft.MvvmLight.CommandWpf; //RelayCommand

using System.Windows.Input;			//ICommand

namespace Desktop.ViewModels.Main
{
    public class ConfirmMessageDeleteViewModel : ViewModelBase
    {
        #region | Private Class Variables |

        private int cmGroup;
        private string phoneNumber;
        private string name;

        private string smallText;

        #endregion

        #region | Constructors |

        public ConfirmMessageDeleteViewModel(int cmGroup, string phoneNumber, string name)
        {
            this.cmGroup = cmGroup;
            this.phoneNumber = phoneNumber;
            this.name = name;

            SmallText = string.Format(LocalizedString.MessageDeleteConfirm_SmallText, name);

            CancelCommand = new RelayCommand(OnCancel);
            DeleteCommand  = new RelayCommand(OnDelete);
        }

        #endregion

        #region | Public Properties |

        public ICommand CancelCommand { get; set; }
        public ICommand DeleteCommand  { get; set; }

        public string SmallText
        {
            get { return smallText; }
            set { Set(ref smallText, value); }
        }

        #endregion

        #region | Private Procedures |

        private void OnCancel()
        {
            ActionManager.Instance.CloseConfirmMessageDeletePopup();
        }

        private void OnDelete()
        {
            ActionManager.Instance.MessageDelete(cmGroup, phoneNumber);
            OnCancel();
        }

        #endregion
    }
}
