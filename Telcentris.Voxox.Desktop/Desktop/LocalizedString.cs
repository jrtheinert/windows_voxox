﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using ResourceMgr = Desktop.Properties.Resources;

//=============================================================================
//This class handles all settings, branding and special build requirements.
//
//Theory of operation is:
//	- All strings are defined in Resources.resx, as they normally are.
//	- This class will have public static accessors for EVERY entry in Resourles.resx 
//		via the ResourceMgr defined above.
//	- All consumers of defined strings (.xaml files mostly) should use this class
//		rather than directly accesses Resources class.  This allows us to use
//		the ReplaceMacros() method to replace 'APPNAME' with branded application name.
//		e.g. %APPNAME% becomes 'Voice Pass'
//
//	NOTE: Due to Universal Build, the strings that use ReplaceMacros()
//			are NOT readonly, so they can be re-calculated dynamically upon re-branding.
//==============================================================================

namespace Desktop
{
    public static class LocalizedString
    {
        public readonly static string AboutWindow_Copyright					= ResourceMgr.AboutWindow_Copyright;
        public readonly static string AboutWindow_Title						= ResourceMgr.AboutWindow_Title;
        public readonly static string AboutWindow_Trademark					= ResourceMgr.AboutWindow_Trademark;
        public			static string AboutWindow_Version					= ReplaceMacros(ResourceMgr.AboutWindow_Version);

        public readonly static string AddToCall_Filter_Contacts				= ResourceMgr.AddToCall_Filter_Contacts;
        public readonly static string AddToCall_Filter_Favorites			= ResourceMgr.AddToCall_Filter_Favorites;
        public			static string AddToCall_Filter_Registered			= ReplaceMacros(ResourceMgr.AddToCall_Filter_Registered);
        public readonly static string AddToCall_NoResults					= ResourceMgr.AddToCall_NoResults;
        public readonly static string AddToCall_SearchContacts				= ResourceMgr.AddToCall_SearchContacts;
        public readonly static string AddToCall_Title						= ResourceMgr.AddToCall_Title;
        public			static string AddToCall_Registered_Label            = ReplaceMacros(ResourceMgr.AddToCall_Registered_Label);
        public readonly static string AddToCall_Empty_Label                 = ResourceMgr.AddToCall_Empty_Label;
        
        public readonly static string AudioPlayer_InitialTime               = ResourceMgr.AudioPlayer_InitialTime;

        public readonly static string AudioSettings_Audio					= ResourceMgr.AudioSettings_Audio;
        public readonly static string AudioSettings_AudioInput				= ResourceMgr.AudioSettings_AudioInput;
        public readonly static string AudioSettings_AudioOutput				= ResourceMgr.AudioSettings_AudioOutput;
        public readonly static string AudioSettings_InputVolume				= ResourceMgr.AudioSettings_InputVolume;
        public readonly static string AudioSettings_OutputVolume			= ResourceMgr.AudioSettings_OutputVolume;

        public readonly static string CallingPane_BuyCredits				= ResourceMgr.CallingPane_BuyCredits;
        public			static string CallingPane_InviteToApp				= ReplaceMacros(ResourceMgr.CallingPane_InviteToApp);
        public			static string CallingPane_InviteToApp_Button		= ReplaceMacros(ResourceMgr.CallingPane_InviteToApp_Button);
        public readonly static string CallingPane_OutofCredits				= ResourceMgr.CallingPane_OutofCredits;
        public readonly static string CallingPane_AudioOutput               = ResourceMgr.CallingPane_AudioOutput;
        public readonly static string CallingPane_TransferCallTo            = ResourceMgr.CallingPane_TransferCallTo;
        public readonly static string CallingPane_CallEnded                 = ResourceMgr.CallingPane_CallEnded;
        public readonly static string CallingPane_OnHold                    = ResourceMgr.CallingPane_OnHold;
        public readonly static string CallingPane_Seperator                 = ResourceMgr.CallingPane_Seperator;
        public readonly static string CallingPane_Recording                 = ResourceMgr.CallingPane_Recording;
        public readonly static string CallingPane_FlippingYourCall          = ResourceMgr.CallingPane_FlippingYourCall;
        public readonly static string CallingPane_FlippingYourCall_Message  = ResourceMgr.CallingPane_FlippingYourCall_Message;
        public readonly static string CallingPane_FlippingYourCall_Okay     = ResourceMgr.CallingPane_FlippingYourCall_Okay;
        public readonly static string CallingPane_FlippingYourCall_Cancel   = ResourceMgr.CallingPane_FlippingYourCall_Cancel;

        public readonly static string IncomingCall_Accept					= ResourceMgr.IncomingCall_Accept;
        public readonly static string IncomingCall_AcceptAndEnd				= ResourceMgr.IncomingCall_AcceptAndEnd;
        public readonly static string IncomingCall_AcceptAndHold			= ResourceMgr.IncomingCall_AcceptAndHold;
        public readonly static string IncomingCall_Decline					= ResourceMgr.IncomingCall_Decline;

        public readonly static string CenterPane_Messages_Failed			= ResourceMgr.CenterPane_Messages_Failed;
        public readonly static string CenterPane_Messages_Fax				= ResourceMgr.CenterPane_Messages_Fax;
        public readonly static string CenterPane_Messages_FreeChat			= ResourceMgr.CenterPane_Messages_FreeChat;
        public readonly static string CenterPane_Messages_Gallery			= ResourceMgr.CenterPane_Messages_Gallery;
        public			static string CenterPane_Messages_Invite			= ReplaceMacros(ResourceMgr.CenterPane_Messages_Invite);
        public readonly static string CenterPane_Messages_Location			= ResourceMgr.CenterPane_Messages_Location;
        public readonly static string CenterPane_Messages_Send				= ResourceMgr.CenterPane_Messages_Send;
        public readonly static string CenterPane_Messages_SMS				= ResourceMgr.CenterPane_Messages_SMS;
        public readonly static string CenterPane_Messages_ChooseANumber     = ResourceMgr.CenterPane_Messages_ChooseANumber;
        public readonly static string CenterPane_Messages_OutgoingCall      = ResourceMgr.CenterPane_Messages_OutgoingCall;
        public readonly static string CenterPane_Messages_IncomingCall      = ResourceMgr.CenterPane_Messages_IncomingCall;

        public readonly static string ChatBubblesColorsName					= ResourceMgr.ChatBubblesColorsName;

        public readonly static string CloseAppInCall_Cancel					= ResourceMgr.CloseAppInCall_Cancel;
        public readonly static string CloseAppInCall_Close					= ResourceMgr.CloseAppInCall_Close;
        public			static string CloseAppInCall_LargeText				= ReplaceMacros(ResourceMgr.CloseAppInCall_LargeText);
        public			static string CloseAppInCall_SmallText				= ReplaceMacros(ResourceMgr.CloseAppInCall_SmallText);
        public readonly static string CountryCodesFileName					= ResourceMgr.CountryCodesFileName;

        public readonly static string ContextMenu_Message					= ResourceMgr.ContextMenu_Message;
        public readonly static string ContextMenu_Call_SelectCallerId		= ResourceMgr.ContextMenu_Call_SelectCallerId;
        public readonly static string ContextMenu_Call_SelectNumber			= ResourceMgr.ContextMenu_Call_SelectNumber;
		

        public			static string CrashReporter_Message					= ReplaceMacros( ResourceMgr.CrashReporter_Message );

        public readonly static string Dialer_Credits                        = ResourceMgr.Dialer_Credits;

        public readonly static string GenericErrorMsg						= ResourceMgr.GenericErrorMsg;
        public readonly static string GenericOr								= ResourceMgr.GenericOr;
        public readonly static string Generic_Setup							= ResourceMgr.Generic_Setup;
        public readonly static string GenericMessage						= ResourceMgr.GenericMessage;
        public readonly static string GenericEavesdrop						= ResourceMgr.GenericEavesdrop;
        public readonly static string GenericIgnore							= ResourceMgr.GenericIgnore;
        public readonly static string GenericContactInfo_Title				= ResourceMgr.GenericContactInfo_Title;
        public readonly static string GenericDefaultPhoneLabel				= ResourceMgr.GenericDefaultPhoneLabel;
        public readonly static string GenericExtension						= ResourceMgr.GenericExtension;
        public readonly static string GenericExtensionAbbreviation			= ResourceMgr.GenericExtensionAbbreviation;
        public readonly static string GenericConference						= ResourceMgr.GenericConference;
        public readonly static string GenericYourExtension					= ResourceMgr.GenericYourExtension;
        public readonly static string GenericCompanyNumbers					= ResourceMgr.GenericCompanyNumbers;
        public readonly static string GenericOtherNumbers					= ResourceMgr.GenericOtherNumbers;
        public			static string GenericBrandedNumber					= ReplaceMacros( ResourceMgr.GenericBrandedNumber );
        public readonly static string GenericTo								= ResourceMgr.GenericTo;
        public readonly static string GenericFrom							= ResourceMgr.GenericFrom;
        public readonly static string GenericCall							= ResourceMgr.GenericCall;
        public readonly static string GenericBlock							= ResourceMgr.GenericBlock;
        public readonly static string GenericUnblock						= ResourceMgr.GenericUnblock;
        public readonly static string GenericDelete							= ResourceMgr.GenericDelete;
        public readonly static string GenericUnknown						= ResourceMgr.GenericUnknown;
        public readonly static string GenericCancel							= ResourceMgr.GenericCancel;
        public readonly static string GenericUpgrade						= ResourceMgr.GenericUpgrade;
        public readonly static string GenericOK								= ResourceMgr.GenericOK;
        public readonly static string GenericNewMessage						= ResourceMgr.GenericNewMessage;

        public readonly static string ContactSourceHasChanged				= ResourceMgr.ContactSourceHasChanged;
		
        public readonly static string Login									= ResourceMgr.Login;
        public readonly static string Login_ForgotPassword					= ResourceMgr.Login_ForgotPassword;
        public readonly static string Login_Password						= ResourceMgr.Login_Password;
        public readonly static string Login_RememberPassword				= ResourceMgr.Login_RememberPassword;
        public readonly static string Login_Signup							= ResourceMgr.Login_Signup;
        public readonly static string Login_UserName						= ResourceMgr.Login_UserName;
        public readonly static string Login_MobileNumber                    = ResourceMgr.Login_MobileNumber;
        public readonly static string LoginFailed							= ResourceMgr.LoginFailed;
        public readonly static string LoginFailMsg							= ResourceMgr.LoginFailMsg;
        public readonly static string Login_CapsLockOnText					= ResourceMgr.Login_CapsLockOnText;

        public readonly static string MainMenu_File							= ResourceMgr.MainMenu_File;
        public readonly static string MainMenu_File_ForgotPassword			= ResourceMgr.MainMenu_File_ForgotPassword;
        public readonly static string MainMenu_File_ManageAccount			= ResourceMgr.MainMenu_File_ManageAccount;
        public readonly static string MainMenu_File_Options					= ResourceMgr.MainMenu_File_Options;
        public readonly static string MainMenu_File_Quit					= ResourceMgr.MainMenu_File_Quit;
        public readonly static string MainMenu_File_Settings_Audio			= ResourceMgr.MainMenu_File_Settings_Audio;
        public readonly static string MainMenu_File_Settings_AutoLaunch		= ResourceMgr.MainMenu_File_Settings_AutoLaunch;
        public readonly static string MainMenu_File_Settings_Notifications	= ResourceMgr.MainMenu_File_Settings_Notifications;
        public readonly static string MainMenu_File_Settings_Personalize	= ResourceMgr.MainMenu_File_Settings_Personalize;
        public readonly static string MainMenu_File_KeepMeSignedIn			= ResourceMgr.MainMenu_File_KeepMeSignedIn;
        public readonly static string MainMenu_File_Signout					= ResourceMgr.MainMenu_File_Signout;

        public readonly static string MainMenu_Help							= ResourceMgr.MainMenu_Help;
        public readonly static string MainMenu_Help_About					= ResourceMgr.MainMenu_Help_About;
        public readonly static string MainMenu_Help_CheckForUpdates			= ResourceMgr.MainMenu_Help_CheckForUpdates;
        public readonly static string MainMenu_Help_ContactSupport			= ResourceMgr.MainMenu_Help_ContactSupport;
        public readonly static string MainMenu_Help_Terms					= ResourceMgr.MainMenu_Help_Terms;
        public readonly static string MainMenu_Help_ViewInAppTour			= ResourceMgr.MainMenu_Help_ViewInAppTour;

        public readonly static string MainMenu_View							= ResourceMgr.MainMenu_View;
        public readonly static string MainMenu_View_Calls					= ResourceMgr.MainMenu_View_Calls;
        public readonly static string MainMenu_View_Contacts				= ResourceMgr.MainMenu_View_Contacts;
        public readonly static string MainMenu_View_Conversations			= ResourceMgr.MainMenu_View_Conversations;
        public readonly static string MainMenu_View_Dialer					= ResourceMgr.MainMenu_View_Dialer;
        public readonly static string MainMenu_View_Faxes					= ResourceMgr.MainMenu_View_Faxes;
        public readonly static string MainMenu_View_NewConversation			= ResourceMgr.MainMenu_View_NewConversation;
        public readonly static string MainMenu_View_Profile					= ResourceMgr.MainMenu_View_Profile;
        public readonly static string MainMenu_View_Recordings				= ResourceMgr.MainMenu_View_Recordings;
        public readonly static string MainMenu_View_Store					= ResourceMgr.MainMenu_View_Store;
        public readonly static string MainMenu_View_Voicemails				= ResourceMgr.MainMenu_View_Voicemails;

        public readonly static string MainMenu_Debug						= ResourceMgr.MainMenu_Debug;

        public			static string MainWindowTitle						= ReplaceMacros(ResourceMgr.MainWindowTitle);

        public readonly static string Messages_Busy							= ResourceMgr.Messages_Busy;
        public readonly static string Messages_CantTalk						= ResourceMgr.Messages_CantTalk;
        public readonly static string Messages_Meeting						= ResourceMgr.Messages_Meeting;

        public readonly static string MiddlePane_Calls_Filter_All		= ResourceMgr.MiddlePane_Calls_Filter_All;
        public			static string MiddlePane_Calls_Filter_BrandName = ReplaceMacros(ResourceMgr.MiddlePane_Calls_Filter_BrandName);
        public readonly static string MiddlePane_Calls_Filter_Missed	= ResourceMgr.MiddlePane_Calls_Filter_Missed;
        public readonly static string MiddlePane_Calls_Filter_Recorded	= ResourceMgr.MiddlePane_Calls_Filter_Recorded;
        public readonly static string MiddlePane_Calls_Filter_Voicemail = ResourceMgr.MiddlePane_Calls_Filter_Voicemail;
        public readonly static string MiddlePane_Calls_NoCalls			= ResourceMgr.MiddlePane_Calls_NoCalls;
        public readonly static string MiddlePane_Calls_NoRecordedCalls	= ResourceMgr.MiddlePane_Calls_NoRecordedCalls;
        public readonly static string MiddlePane_Calls_NoVoicemails		= ResourceMgr.MiddlePane_Calls_NoVoicemails;
        public readonly static string MiddlePane_Calls_SearchCalls		= ResourceMgr.MiddlePane_Calls_SearchCalls;
        public			static string MiddlePane_Calls_Text_BrandName	= ReplaceMacros(ResourceMgr.MiddlePane_Calls_Text_BrandName);
        public readonly static string MiddlePane_Calls_Text_MissedCall	= ResourceMgr.MiddlePane_Calls_Text_MissedCall;
        public readonly static string MiddlePane_Calls_Text_Recorded	= ResourceMgr.MiddlePane_Calls_Text_Recorded;
        public readonly static string MiddlePane_Calls_Text_Voicemail	= ResourceMgr.MiddlePane_Calls_Text_Voicemail;
        public readonly static string MiddlePane_Calls_Title			= ResourceMgr.MiddlePane_Calls_Title;
        public readonly static string MiddlePane_Calls_Delete           = ResourceMgr.MiddlePane_Calls_Delete;
        public readonly static string MiddlePane_Calls_CallBack         = ResourceMgr.MiddlePane_Calls_CallBack;
        public readonly static string MiddlePane_Calls_CallsWith        = ResourceMgr.MiddlePane_Calls_CallsWith;
        public readonly static string MiddlePane_Calls_OutOfCredits     = ResourceMgr.MiddlePane_Calls_OutOfCredits;
        public readonly static string MiddlePane_Calls_PurchaseLinkText = ResourceMgr.MiddlePane_Calls_PurchaseLinkText;
        public readonly static string MiddlePane_Calls_CreditsOff       = ResourceMgr.MiddlePane_Calls_CreditsOff;

		public readonly static string MiddlePane_Contacts_Filter_All		= ResourceMgr.MiddlePane_Contacts_Filter_All;
        public readonly static string MiddlePane_Contacts_Filter_Favorites	= ResourceMgr.MiddlePane_Contacts_Filter_Favorites;
        public			static string MiddlePane_Contacts_Filter_Registered = ReplaceMacros(ResourceMgr.MiddlePane_Contacts_Filter_Registered);
        public readonly static string MiddlePane_Contacts_NoContacts		= ResourceMgr.MiddlePane_Contacts_NoContacts;
        public readonly static string MiddlePane_Contacts_NoResults			= ResourceMgr.MiddlePane_Contacts_NoResults;
        public readonly static string MiddlePane_Contacts_SearchContacts	= ResourceMgr.MiddlePane_Contacts_SearchContacts;
        public readonly static string MiddlePane_Contacts_Title				= ResourceMgr.MiddlePane_Contacts_Title;

        public readonly static string MiddlePane_Faxes_Add					= ResourceMgr.MiddlePane_Faxes_Add;
        public readonly static string MiddlePane_Faxes_AttachPages			= ResourceMgr.MiddlePane_Faxes_AttachPages;
        public readonly static string MiddlePane_Faxes_Cancel				= ResourceMgr.MiddlePane_Faxes_Cancel;
        public readonly static string MiddlePane_Faxes_DownloadPages		= ResourceMgr.MiddlePane_Faxes_DownloadPages;
        public readonly static string MiddlePane_Faxes_EnterNumber			= ResourceMgr.MiddlePane_Faxes_EnterNumber;
        public readonly static string MiddlePane_Faxes_Filter_All			= ResourceMgr.MiddlePane_Faxes_Filter_All;
        public readonly static string MiddlePane_Faxes_Filter_Incoming		= ResourceMgr.MiddlePane_Faxes_Filter_Incoming;
        public readonly static string MiddlePane_Faxes_Filter_Outgoing		= ResourceMgr.MiddlePane_Faxes_Filter_Outgoing;
        public readonly static string MiddlePane_Faxes_NewFax				= ResourceMgr.MiddlePane_Faxes_NewFax;
        public readonly static string MiddlePane_Faxes_NoFaxes				= ResourceMgr.MiddlePane_Faxes_NoFaxes;
        public readonly static string MiddlePane_Faxes_SearchFaxes			= ResourceMgr.MiddlePane_Faxes_SearchFaxes;
        public readonly static string MiddlePane_Faxes_SendFax				= ResourceMgr.MiddlePane_Faxes_SendFax;
        public readonly static string MiddlePane_Faxes_SendToThisNumber		= ResourceMgr.MiddlePane_Faxes_SendToThisNumber;
        public readonly static string MiddlePane_Faxes_Title				= ResourceMgr.MiddlePane_Faxes_Title;

        public readonly static string MiddlePane_Messages_NewMessage		= ResourceMgr.MiddlePane_Messages_NewMessage;
        public readonly static string MiddlePane_Messages_NoMessages		= ResourceMgr.MiddlePane_Messages_NoMessages;
        public readonly static string MiddlePane_Messages_SearchMessages	= ResourceMgr.MiddlePane_Messages_SearchMessages;
        public readonly static string MiddlePane_Messages_Title				= ResourceMgr.MiddlePane_Messages_Title;

        public readonly static string NetworkErrorMsg						= ResourceMgr.NetworkErrorMsg;
        public readonly static string NetworkErrorWithCallsMsg				= ResourceMgr.NetworkErrorWithCallsMsg;

        public readonly static string NoAudioDevices_Cancel					= ResourceMgr.NoAudioDevices_Cancel;
        public readonly static string NoAudioDevices_LargeText				= ResourceMgr.NoAudioDevices_LargeText;
        public readonly static string NoAudioDevices_Ok						= ResourceMgr.NoAudioDevices_Ok;
        public readonly static string NoAudioDevices_Settings				= ResourceMgr.NoAudioDevices_Settings;

        public readonly static string NotImplemented						= ResourceMgr.NotImplemented;

        public readonly static string OutOfCredits_BuyCredits				= ResourceMgr.OutOfCredits_BuyCredits;
        public readonly static string OutOfCredits_Cancel					= ResourceMgr.OutOfCredits_Cancel;

        public readonly static string Profile_ChangePassword				= ResourceMgr.Profile_ChangePassword;
        public readonly static string Profile_ChooseAvatar					= ResourceMgr.Profile_ChooseAvatar;
        public readonly static string Profile_Email							= ResourceMgr.Profile_Email;
        public readonly static string Profile_FirstName						= ResourceMgr.Profile_FirstName;
        public readonly static string Profile_LastName						= ResourceMgr.Profile_LastName;
        public readonly static string Profile_Save							= ResourceMgr.Profile_Save;

        public readonly static string RecordedCall							= ResourceMgr.RecordedCall;

        public readonly static string Settings_Audio						= ResourceMgr.Settings_Audio;
        public readonly static string Settings_AutoStart					= ResourceMgr.Settings_AutoStart;
        public readonly static string Settings_AutoLogin                    = ResourceMgr.Settings_AutoLogin;
        public readonly static string Settings_AutoLoginOption              = ResourceMgr.Settings_AutoLoginOption;
        public readonly static string Settings_AutoStartHeader				= ResourceMgr.Settings_AutoStartHeader;
        public readonly static string Settings_AutoStartOption				= ResourceMgr.Settings_AutoStartOption;
        public readonly static string Settings_AutoLoginHeader              = ResourceMgr.Settings_AutoLoginHeader;
        public readonly static string Settings_CallerID						= ResourceMgr.Settings_CallerID;
        public readonly static string Settings_CallForwardingSettings		= ResourceMgr.Settings_CallForwardingSettings;
        public readonly static string Settings_CallSettings					= ResourceMgr.Settings_CallSettings;
        public readonly static string Settings_ChangeAudioConfigurations	= ResourceMgr.Settings_ChangeAudioConfigurations;
        public readonly static string Settings_Chat_ReceivedMessage			= ResourceMgr.Settings_Chat_ReceivedMessage;
        public readonly static string Settings_Chat_SentMessage				= ResourceMgr.Settings_Chat_SentMessage;
        public readonly static string Settings_ChatBubbles					= ResourceMgr.Settings_ChatBubbles;
        public readonly static string Settings_ChatSettings					= ResourceMgr.Settings_ChatSettings;
        public readonly static string Settings_CheckForUpdates				= ResourceMgr.Settings_CheckForUpdates;
        public readonly static string Settings_ContactsChats				= ResourceMgr.Settings_ContactsChats;
        public readonly static string Settings_ContactSupport				= ResourceMgr.Settings_ContactSupport;
        public readonly static string Settings_countryCode					= ResourceMgr.Settings_countryCode;
        public readonly static string Settings_Email						= ResourceMgr.Settings_Email;
        public readonly static string Settings_Faxes						= ResourceMgr.Settings_Faxes;
        public readonly static string Settings_Help							= ResourceMgr.Settings_Help;
        public readonly static string Settings_Incomingcall					= ResourceMgr.Settings_Incomingcall;
        public readonly static string Settings_Incomingchat					= ResourceMgr.Settings_Incomingchat;
        public readonly static string Settings_Keypad						= ResourceMgr.Settings_Keypad;
        public readonly static string Settings_More							= ResourceMgr.Settings_More;
        public readonly static string Settings_MyChats						= ResourceMgr.Settings_MyChats;
        public readonly static string Settings_NoDeviceText					= ResourceMgr.Settings_NoDeviceText;
        public readonly static string Settings_Outgoingcall					= ResourceMgr.Settings_Outgoingcall;
        public readonly static string Settings_personalize					= ResourceMgr.Settings_personalize;
        public readonly static string Settings_Phone						= ResourceMgr.Settings_Phone;
        public readonly static string Settings_Reachme						= ResourceMgr.Settings_Reachme;
        public readonly static string Settings_RecordedCalls				= ResourceMgr.Settings_RecordedCalls;
        public readonly static string Settings_SelectBubbleColor			= ResourceMgr.Settings_SelectBubbleColor;
        public readonly static string Settings_SetYourOutgoingCallNumber	= ResourceMgr.Settings_SetYourOutgoingCallNumber;
        public readonly static string Settings_ShowFax						= ResourceMgr.Settings_ShowFax;
        public readonly static string Settings_ShowinChatthread				= ResourceMgr.Settings_ShowinChatthread;
        public readonly static string Settings_Sounds						= ResourceMgr.Settings_Sounds;
        public readonly static string Settings_SoundsHeader					= ResourceMgr.Settings_SoundsHeader;
        public readonly static string Settings_support						= ResourceMgr.Settings_support;
        public readonly static string Settings_Version						= ResourceMgr.Settings_Version;
        public readonly static string Settings_VersionHeader				= ResourceMgr.Settings_VersionHeader;
        public readonly static string Settings_VoiceMails					= ResourceMgr.Settings_VoiceMails;
        public			static string Settings_WindowTitle					= ReplaceMacros(ResourceMgr.Settings_WindowTitle);

		public			static string SystemTrayBalloonTip					= ReplaceMacros(ResourceMgr.SystemTrayBalloonTip);
        public readonly static string SystemTrayMenu_AutoStart				= ResourceMgr.SystemTrayMenu_AutoStart;
        public			static string SystemTrayMenu_OpenAppName			= ReplaceMacros(ResourceMgr.SystemTrayMenu_OpenAppName);
        public readonly static string SystemTrayMenu_Quit					= ResourceMgr.SystemTrayMenu_Quit;
        public readonly static string SystemTrayMenu_Settings				= ResourceMgr.SystemTrayMenu_Settings;
        public readonly static string SystemTrayMenu_SignOut				= ResourceMgr.SystemTrayMenu_SignOut;

        public readonly static string FeatureNotAvailable_Header			= ResourceMgr.FeatureNotAvailable_Header;
        public readonly static string FeatureNotAvailable_Data				= ResourceMgr.FeatureNotAVailable_Data;

        public readonly static string FindMeNumbersNotSetup_Header			= ResourceMgr.FindMeNumbersNotSetup_Header;
        public readonly static string FindMeNumbersNotSetup_SubText			= ResourceMgr.FindMeNumbersNotSetup_SubText;

        public readonly static string MessageDeleteConfirm_LargeText        = ResourceMgr.MessageDeleteConfirm_LargeText;
        public readonly static string MessageDeleteConfirm_SmallText        = ResourceMgr.MessageDeleteConfirm_SmallText;
        public readonly static string MessageDeleteConfirm_Cancel           = ResourceMgr.MessageDeleteConfirm_Cancel;
        public readonly static string MessageDeleteConfirm_Delete           = ResourceMgr.MessageDeleteConfirm_Delete;

        public readonly static string BlockContactConfirm_Block             = ResourceMgr.BlockContactConfirm_Block;
        public readonly static string BlockContactConfirm_BlockLargeText    = ResourceMgr.BlockContactConfirm_BlockLargeText;
        public readonly static string BlockContactConfirm_BlockSmallText    = ResourceMgr.BlockContactConfirm_BlockSmallText;
        public readonly static string BlockContactConfirm_Cancel            = ResourceMgr.BlockContactConfirm_Cancel;
        public readonly static string BlockContactConfirm_Unblock           = ResourceMgr.BlockContactConfirm_Unblock;
        public readonly static string BlockContactConfirm_UnblockLargeText  = ResourceMgr.BlockContactConfirm_UnblockLargeText;
        public readonly static string BlockContactConfirm_UnblockSmallText  = ResourceMgr.BlockContactConfirm_UnblockSmallText;

        #region | Replace Functions |

        private const string APPNAME_PLACEHOLDER = "%APPNAME%";

        private static string ReplaceMacros(string data)
        {
            if (string.IsNullOrWhiteSpace(data))
                return data;

            return data.Replace(APPNAME_PLACEHOLDER, Desktop.Config.Branding.AppName);
        }

		//For Universal Build, we need to re-brand items with Macros
		//	It *may* be better to just do all the strings, but for now, let's just get the once that use ReplaceMacros()
		public static void Rebrand()
		{
			AboutWindow_Version						= ReplaceMacros(ResourceMgr.AboutWindow_Version);
			AddToCall_Filter_Registered				= ReplaceMacros(ResourceMgr.AddToCall_Filter_Registered);
			AddToCall_Registered_Label				= ReplaceMacros(ResourceMgr.AddToCall_Registered_Label);
			CallingPane_InviteToApp					= ReplaceMacros(ResourceMgr.CallingPane_InviteToApp);
			CallingPane_InviteToApp_Button			= ReplaceMacros(ResourceMgr.CallingPane_InviteToApp_Button);
			CenterPane_Messages_Invite				= ReplaceMacros(ResourceMgr.CenterPane_Messages_Invite);
		
			CloseAppInCall_LargeText				= ReplaceMacros(ResourceMgr.CloseAppInCall_LargeText);
			CloseAppInCall_SmallText				= ReplaceMacros(ResourceMgr.CloseAppInCall_SmallText);
			CrashReporter_Message					= ReplaceMacros( ResourceMgr.CrashReporter_Message );

			GenericBrandedNumber					= ReplaceMacros( ResourceMgr.GenericBrandedNumber );

			MainWindowTitle							= ReplaceMacros(ResourceMgr.MainWindowTitle);
			MiddlePane_Calls_Filter_BrandName		= ReplaceMacros(ResourceMgr.MiddlePane_Calls_Filter_BrandName);
			MiddlePane_Calls_Text_BrandName			= ReplaceMacros(ResourceMgr.MiddlePane_Calls_Text_BrandName);
			MiddlePane_Contacts_Filter_Registered	= ReplaceMacros(ResourceMgr.MiddlePane_Contacts_Filter_Registered);

			Settings_WindowTitle					= ReplaceMacros(ResourceMgr.Settings_WindowTitle);
			SystemTrayBalloonTip					= ReplaceMacros(ResourceMgr.SystemTrayBalloonTip);
			SystemTrayMenu_OpenAppName				= ReplaceMacros(ResourceMgr.SystemTrayMenu_OpenAppName);
		}

        #endregion
    }
}
