﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Utils;				//Logger

using Vxapi23;						//RestfulApiManager
using Vxapi23.Model;				//NotifyHttpListenerResponse, ConferenceCallHttpListenerResponse

using System;
using System.Collections.Generic;	//Dictionary
using System.Threading.Tasks;

namespace Desktop.Model
{
    public class NotifyHttpListenersHelper
    {
        private static VXLogger logger = VXLoggingManager.INSTANCE.GetLogger("NotifyHttpListenersHelper");

		private static String s_code_OK				  = "200";	//No error
		private static String s_code_NoNumbersToBlast = "412";	//Display message in UI.  NOTE: this error should NEVER happen.
		private static String s_code_ErrorConnecting  = "500";	//Client should retry

		//Menu options
		private static String s_funcOnCallMenu			 = "oncallmenu";
		private static String s_funcVmLeavesDropMenu	 = "voicemaileavesdropmenu";
		private static String s_funcOnHoldMenuCollect	 = "onholdmenucollect";
		private static String s_funcPickupOptionsCollect = "pickupoptionscollect";

		//DTMF tones
		private static String s_dtmfAcceptCall			 = "ACCEPT_CALL";		//With s_funcPickupOptionsCollect
		private static String s_dtmfBlacklist			 = "BLACKLIST";			//With s_funcPickupOptionsCollect	//TODO: Coded for, but not called anywhere.
		private static String s_dtmfSendToVm			 = "SEND_TO_VM";		//With s_funcPickupOptionsCollect

		private static String s_dtmfBlastTransfer		 = "BLAST_TRANSFER";	//With s_funcOnCallMenu
		private static String s_dtmfConfJoinOther		 = "CONF_JOIN_OTHER";	//With s_funcOnCallMenu
		private static String s_dtmfConfStart			 = "CONF_START";		//With s_funcOnCallMenu
		private static String s_dtmfHangup				 = "HANGUP";			//With s_funcOnCallMenu
		private static String s_dtmfMuteOff				 = "MUTE_OFF";			//With s_funcOnCallMenu
		private static String s_dtmfMuteOn				 = "MUTE_ON";			//With s_funcOnCallMenu
		private static string s_dtmfHoldOn				 = "ON_HOLD";			//With s_funcOnCallMenu
		private static String s_dtmfToggleRecord		 = "TOGGLE_RECORD";		//With s_funcOnCallMenu

		private static String s_dtmfHoldOff				 = "OFF_HOLD";			//With s_funcOnHoldMenuCollect

		private static String s_dtmfBargeIn				 = "BARGE_IN";			//With s_funcVmLeavesDropMenu

		private static async Task<bool> doWork( string callId, string funcName, string dtmfDigits, Dictionary<string, string> parameters )
		{
			bool result     = false;

			//Variables for retry handling
			int  maxRetries = 5;
			int  retries    = 0;
			bool isDone     = false;
			int  retryDelay = 500;	//in ms.

			try
			{
				var user = SessionManager.Instance.User;

				while ( !isDone && retries < maxRetries )
				{
					retries++;

					var response = await RestfulAPIManager.INSTANCE.NotifyHTTPListeners<List<NotifyHttpListenerResponse>>(user.CompanyUserId, user.UserKey, callId, funcName, dtmfDigits, parameters );

					if ( response != null && response.Count > 0 )
					{
						foreach ( NotifyHttpListenerResponse resp in response )
						{
							if ( resp.succeeded() )
							{
								if (resp.Code == s_code_OK )
								{
									result = true;
									isDone = true;
								}
								else if ( resp.Code == s_code_ErrorConnecting )
								{
									//Do nothing.  Allow retry to happen.
									System.Threading.Thread.Sleep( retryDelay );
								}
								else if ( resp.Code == s_code_NoNumbersToBlast )
								{
									//TODO: pass code back?
								}
							}
						}	//foreach
					}	//if
				}	//while
			}	//try

			catch (Exception ex)
			{
				logger.Error( funcName + ": " + callId, ex);
			}


			if (!result )
			{
				logger.Error( funcName + ": " + callId + " failed after max retries." );
			}

			return result;
		}

        public static async Task<bool> AcceptCall(string callId)
        {
			bool result = await doWork( callId, s_funcPickupOptionsCollect, s_dtmfAcceptCall, null);
			return result;
		}

        public static async Task<bool> HangupCall(string callId)
        {
			bool result = await doWork( callId,  s_funcOnCallMenu, s_dtmfHangup, null);		//TODO: This always returns error 500.
			return result;
		}

        public static async Task<bool> SendCallToVoiceMessage(string callId)
        {
			bool result = await doWork( callId, s_funcPickupOptionsCollect, s_dtmfSendToVm, null);
			return result;
		}

        public static async Task<bool> Blacklist(string callId)	//Not called anywhere
        {
			bool result = await doWork( callId, s_funcPickupOptionsCollect, s_dtmfBlacklist, null);
			return result;
		}

        public static async Task<bool> ToggleRecording(string callId)
        {
			bool result = await doWork( callId, s_funcOnCallMenu, s_dtmfToggleRecord, null);
			return result;
		}

        public static async Task<bool> MuteCall(string callId, bool mute )
        {
			string dtmf = mute ? s_dtmfMuteOn : s_dtmfMuteOff;
			bool result = await doWork( callId, s_funcOnCallMenu, dtmf, null);
			return result;
		}

        public static async Task<bool> HoldCall(string callId, bool hold )
        {
			//NOTE: the funcName differs depending on 'hold' value.  This is unique.
			string func = hold ? s_funcOnCallMenu : s_funcOnHoldMenuCollect;
			string dtmf = hold ? s_dtmfHoldOn     : s_dtmfHoldOff;

			bool result = await doWork( callId,  func, dtmf, null);
			return result;
		}

        public static async Task<bool> BlastTransfer(string callId)
        {
			bool result = await doWork( callId, s_funcOnCallMenu, s_dtmfBlastTransfer, null);
			return result;
		}

        public static async Task<bool> BargeIn(string callId)
        {
			bool result = await doWork( callId, s_funcVmLeavesDropMenu, s_dtmfBargeIn, null);
			return result;
		}

		//NOTE: This returns a value other than 'Boolean'
		//TODO: Add retry logic here?
        public static async Task<ConferenceCallHttpListenerResponse> Conference( string conferenceId, string callId, string otherCallId)
        {
            try
            {
                Dictionary<string, string> param = new Dictionary<string, string>();

				//Validity of conferenceID will determine whether we are STARTING a conference call or ADDING TO a conference call.
				bool startConf = String.IsNullOrEmpty( conferenceId );
				string dtmfValue;

				if ( startConf )
				{ 
					dtmfValue = s_dtmfConfStart;
					param.Add("params[joinHttpListenerIds][]", otherCallId);
				}
				else
				{
					dtmfValue = s_dtmfConfJoinOther;
					param.Add("params[joinHttpListenerIds][]", otherCallId);
					param.Add("params[conferenceid]",          conferenceId);
				}

                var user = SessionManager.Instance.User;
                var response = await RestfulAPIManager.INSTANCE.NotifyHTTPListeners<ConferenceCallHttpListenerResponse>(user.CompanyUserId, user.UserKey, callId, s_funcOnCallMenu, dtmfValue, param);

				return response;
            }
            catch (Exception ex)
            {
                logger.Error("Conference: " + callId, ex);
            }

            return null;
        }
    }
}
