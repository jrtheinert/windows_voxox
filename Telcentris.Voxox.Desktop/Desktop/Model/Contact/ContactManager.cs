﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model.Calling;		//SipAddress
using Desktop.Util;					//PhoneUtils
using Desktop.Utils;				//Logger
using Desktop.ViewModels.Contacts;	//ContactNumberViewModel

using ManagedDataTypes;				//Various data types

using Vxapi23;						//RestfulApiManager
using Vxapi23.Model;				//CheckInNetworkContactResponse, NetworkContact, ContactResponse

using GalaSoft.MvvmLight.Messaging;	//Messenger

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Desktop.Model.Contact
{
    public class ContactManager
    {
        #region | Private Member Variables |

        private VXLogger logger = VXLoggingManager.INSTANCE.GetLogger("ContactManager");
        private static ContactManager mInstance = null;

		private Boolean mContactSyncDone = false;

        #endregion

        public static ContactManager getInstance()
        {
            if (mInstance == null)
            {
                mInstance = new ContactManager();
            }

            return mInstance;
        }

        private ContactManager()
        {
			Messenger.Default.Register<SelectContactSourceMessage>( this, OnSelectContactSourceMessage );
		}

        private ManagedDbManager.ManagedDbManager getUserDbm()
        {
            return SessionManager.Instance.UserDataStore;
        }

        public ContactMessageSummaryList getContactsMessageSummary(bool includeVoicemail = true, bool includeRecordedCalls = true, bool includeFaxes = true)
        {
            if (getUserDbm() == null)
                return new ContactMessageSummaryList();

            return getUserDbm().getContactsMessageSummary(includeVoicemail, includeRecordedCalls, includeFaxes);
        }

        public ContactList getContacts( String search, ContactFilter filter)
        {
            using (VXProfiler prof = new VXProfiler("getContacts"))
            {
                return getUserDbm().getContacts(search, filter);
            }
        }

		//This is a better alternative to getContacts() above.  At least in some cases.		
        public PhoneNumberList getContactPhoneNumbers(String search, ContactFilter filter)
        {
            using (VXProfiler prof = new VXProfiler("getContacts"))
            {
	            return getUserDbm().getContactPhoneNumbers( search, filter);
			}
        }

        public PhoneNumberList getContactDetailsPhoneNumbersByKey( string contactKey )
        {
            return getUserDbm().getContactDetailsPhoneNumbersByKey( contactKey );
        }

		public PhoneNumberList getContactDetailsPhoneNumbersByCmGroup( int cmGroup )
		{
			return getUserDbm().getContactDetailsPhoneNumbersByCmGroup( cmGroup );
		}

        public int getNewInboundMessageCount(String phoneNumber, String xmppAddress, bool includeVoicemail = true, bool includeRecordedCalls = true, bool includeFaxes = true)
        {
            return getUserDbm().getNewInboundMessageCount(phoneNumber, xmppAddress, includeVoicemail, includeRecordedCalls, includeFaxes);
        }

        public RecentCall GetRecentCall(int callID)
        {
            return getUserDbm().getRecentCall(callID);
        }

        public int getCmGroup(String phoneNumber)
        {
            return getUserDbm().getCmGroup(phoneNumber);
        }

        public PhoneNumber getPhoneNumber(string phoneNumber, int cmGroup)
        {
            return getUserDbm().getPhoneNumber(phoneNumber, cmGroup);
        }

		public bool phoneNumberExists( string phoneNumber )
		{
			return getUserDbm().phoneNumberExists( phoneNumber );
		}

		public async Task<bool> isInNetwork( string phoneNumber )
		{
			bool result = false;

			if ( phoneNumberExists( phoneNumber ) )
			{
				PhoneNumber temp = getPhoneNumber( phoneNumber, -1 );	//Debug
				result = true;
			}
			else
			{
				String[] phoneNumbers = new String[] { phoneNumber };
				result = await isInNetwork( phoneNumbers );
			}

			return result;
		}
		
		private async Task<bool> isInNetwork( String[] phoneNumbers )
		{
            Task<bool> syncTask = Task.Run( () => isInNetworkPrivate( phoneNumbers ) );
            await Task.WhenAll( syncTask );

			return syncTask.Result;
		}

		private bool isInNetworkPrivate( String[] phoneNumbers )
		{
			bool result = false;

			VoxoxContactList	  nakedNumbersToAdd	= new VoxoxContactList();				//Add these to DB so we do not keep making API calls for them.
			PhoneMetaInfoResponse response			= getPhoneMetaInfo( phoneNumbers );		//Error reporting is done in method call.

			if ( response != null && response.numbers != null && response.numbers.Count > 0 )
			{
				int			     existCount = 0;
				VoxoxContactList vcs = new VoxoxContactList();

				foreach ( PhoneMetaInfoResponse.Number meta in response.numbers )
				{
					if ( meta.inNetwork != null )
					{
						if ( phoneNumberExists( meta.inNetwork.did ) )
						{
							existCount++;
						}
						else
						{ 
							VoxoxContact vc = voxoxContactFromPhoneMeta( meta,  ManagedDataTypes.Contact.DbSource_InNetwork );

							if ( vc != null )
							{
								vcs.Add( vc );
							}
						}
					}
					else
					{
						if ( phoneNumberExists( meta.phoneNumber ) )
						{
							existCount++;		//Should not get here.  If we do, then the query for the phoneNumbers is wrong.
						}
						else 
						{ 
							String contactKey = Guid.NewGuid().ToString();
							VoxoxContact vc = new VoxoxContact( contactKey, meta.phoneNumber, meta.isBlocked(), meta.isFavorite(), ManagedDataTypes.Contact.DbSource_NakedNumber );
							nakedNumbersToAdd.Add( vc );
						}
					}
				}
					
				addOrUpdateInNetworkContacts( vcs );
			
				result = ( phoneNumbers.Length == (vcs.Count + existCount) );	//Mostly for when we have a single phoneNumber to add.
			}

			//Add these numbers so we do not keep making API calls.
			if ( nakedNumbersToAdd.Count > 0 )
			{
				addNakedNumbers( nakedNumbersToAdd );
			}

			return result;
		}

        /// <summary>
        /// Returns the first Contact found based on the first matching phone number.
        /// </summary>
        /// <param name="contactKey">Conact Key</param>
        /// <returns></returns>
        public ManagedDataTypes.Contact getContactByKey( string contactKey )
        {
			ManagedDataTypes.Contact result = getUserDbm().getContactByKey( contactKey );
			return result;
		}

        /// <summary>
        /// Returns the first Contact found based on the first matching phone number.
        /// </summary>
        /// <param name="phoneNumbers">List of phone numbers that are similar but with different formatting</param>
        /// <returns></returns>
        public ManagedDataTypes.Contact getMatchingContact( string[] phoneNumbers )
        {
            foreach (var phoneNumber in phoneNumbers)
            {
                if (string.IsNullOrWhiteSpace(phoneNumber) == false)
                {
					var cleanedPhone = PhoneUtils.GetPhoneNumberNoCountry( phoneNumber, false );

                    ContactList contactList = getUserDbm().getContacts( cleanedPhone, ContactFilter.All );

                    // The PAPI call returns the passed Phone Number as a new Contact if none is found, so not using it
                    ManagedDataTypes.Contact contact = (contactList != null && contactList.Count > 0 && contactList[0].isFromAddressBook() ) ? contactList[0] : null;

                    if (contact != null)
                        return contact;
                }
            }

            return null;
        }

        public ManagedDataTypes.Contact getMatchingContact(SipAddress sipAddress)
        {
            return getMatchingContact(new string[] { sipAddress.getUserName(), sipAddress.getDisplayName() });
        }

        /// <summary>
        /// Returns the first PhoneNumber found based on the first matching phone number.
        /// </summary>
        /// <param name="phoneNumbers">List of phone numbers that are similar but with different formatting</param>
        /// <returns></returns>
        public ManagedDataTypes.PhoneNumber getMatchingPhoneNumber( string[] phoneNumbers )
        {
            foreach (var phoneNumber in phoneNumbers)
            {
                if (string.IsNullOrWhiteSpace(phoneNumber) == false)
                {
                    var cleanedPhone = PhoneUtils.CleanPhoneNumberFormatting(phoneNumber).Replace("+", ""); // Remove "+" sign also

					PhoneNumber pn = getUserDbm().getPhoneNumber( cleanedPhone, -1 );

                    if ( pn != null )
                        return pn;
                }
            }

            return null;
        }

        public Boolean isNumberAnExtension( string phoneNumber )
        {
			return getUserDbm().isNumberAnExtension( phoneNumber );
        }


        public ManagedDataTypes.PhoneNumber getMatchingPhoneNumber( SipAddress sipAddress)
        {
            return getMatchingPhoneNumber(new string[] { sipAddress.getUserName(), sipAddress.getDisplayName() });
        }

		public void setBlacklisted( String phoneNumber, bool blacklist )
		{
            SetBlacklistedResponse response = RestfulAPIManager.INSTANCE.setBlacklisted( SessionManager.Instance.User.UserKey, SessionManager.Instance.User.CompanyUserId, phoneNumber, blacklist );

            if (response == null )
			{
				logger.Warn( "setBlacklisted returned null." );
			}
			else
            {
				if ( ! response.succeeded() )
				{ 
					logger.Warn( "setBlacklisted failed." );
				}
			}
		}

		public void setFavorite( String phoneNumber, bool favorite )
		{
			//Make API call
            SetFavoriteResponse response = RestfulAPIManager.INSTANCE.setFavorite( SessionManager.Instance.User.UserKey, SessionManager.Instance.User.CompanyUserId, phoneNumber, favorite );

            if (response == null )
			{
				logger.Warn( "setFavorite returned null." );
			}
			else
            {
				if ( response.succeeded() )
				{
					//Update local DB upon success.
		            getUserDbm().updatePhoneNumberFavorite( phoneNumber, favorite);
				}
				else
				{ 
					logger.Warn( "setFavorite failed." );
				}
			}
		}

		//Since UX has no means to select sourceKey, if more than one exists, for now we just take the first entry and log if more than one exists.
		//This method will encapsulate:
		//	- getContactSources();
		//	- getContactList()
		//	- getPhoneMetaInfo()
        public void SynchronizeContacts2()
        {
            logger.Debug("Synchronizing contacts from server with new Contact Syncing APIs");

            try
            {
				//Get Contact Sources, and chose one to use.
                ContactSourcesResponse sourcesResponse = RestfulAPIManager.INSTANCE.getContactSources( SessionManager.Instance.User.UserKey, SessionManager.Instance.User.CompanyUserId );

                if (sourcesResponse != null && sourcesResponse.sources.Count > 0 )
                {
					//For Dev/Debug
//					sourcesResponse.addTestData( "Test source 1", "srcId 1", "key 1", 10, 10 );
//					sourcesResponse.addTestData( "Test source 2", "srcId 2", "key 2", 10, 11 );

					mContactSyncDone = false;

					Task.Run(() => { chooseContactSourceKey( sourcesResponse); });	//This *may* have user-interaction so it must be async.

					//Since SyncManager is waiting for this task to complete,
					//	AND it is not complete until we perform rest of Contact Sycning,
					//	we loop here until it is.
//					int maxWait   = 30000;		//Typical time is about xxx ms.
					int incWait   =   100;
					int totalWait =     0;

					while ( !mContactSyncDone )	//&& totalWait < maxWait )
					{
						System.Threading.Thread.Sleep( incWait );
						totalWait += incWait;
					}
				}
				else 
				{ 
		           logger.Warn( "No Contact Sources found.  Have you logged into mobile device with this account?");
				}
			}
            catch (Exception ex)
            {
                logger.Error(ex);
            }
		}

		//Continue contact syncing after Contact Source is selected.
		private void OnSelectContactSourceMessage( SelectContactSourceMessage msg )
		{
			if ( msg.SourceKey == null )	//Wrong message info.
				return;

			String sourceKey = msg.SourceKey;

			handleContactSourceIfNeeded( sourceKey );

			//These are independent of CompanyDirectoryReponse and will be populated later in this block.
			PhoneMetaInfoResponse phoneMetaInfoResponse = null;
			ContactListResponse   contactListResponse   = null;

            try
            {
				//Get company directory.  This is independent of the contactSources and ContactList, but will be merged with that data.
				CompanyDirectoryResponse directoryResponse = RestfulAPIManager.INSTANCE.getCompanyDirectory( SessionManager.Instance.User.UserKey, SessionManager.Instance.User.CompanyUserId );

				if ( !String.IsNullOrEmpty( sourceKey ) )
				{
					//Get Contact List for chose 'sourceKey'
					contactListResponse = RestfulAPIManager.INSTANCE.getContactList( SessionManager.Instance.User.UserKey, SessionManager.Instance.User.CompanyUserId, sourceKey );

					if (contactListResponse != null )
					{
						if ( contactListResponse.contacts.Count == 0 )
						{
							logger.Warn( "ContactList has no entries.  No contacts have been retrieved.");
						}
						else
						{
							//Get Phone MetaInfo by using ALL the phone numbers in ContactListReponse.
							String[] phoneNumbers = contactListResponse.getPhoneNumbersAsArray();

							if ( phoneNumbers.Length == 0 )
							{
 								logger.Warn( "No phoneNumbers found to use with PhoneMetaInfo.");
							}
							else
							{
								phoneMetaInfoResponse = getPhoneMetaInfo( phoneNumbers );
							}
						}
					}
					else
					{
						logger.Warn( "ContactList Reponse is null.  No contacts have been retrieved.");
					}
				}
				else 
				{ 
					logger.Warn( "SourceKey is empty. No contacts can be retrieved.");
				}

				//This is where real work is done.
				mergeContactAndPhoneResponses( contactListResponse, phoneMetaInfoResponse, directoryResponse );
			}
            catch (Exception ex)
            {
                logger.Error(ex);
            }

			mContactSyncDone = true;
		}

		private void handleContactSourceIfNeeded( String sourceKey )
		{
			//Check DB to see if sourceKey changed.  If so:
			//	- Update DB with new sourceKey
			//	- Notify user
			if ( SettingsManager.Instance.User.LastContactSourceKey != sourceKey )
			{
				bool notifyUser = !String.IsNullOrEmpty( SettingsManager.Instance.User.LastContactSourceKey);

				SettingsManager.Instance.User.LastContactSourceKey = sourceKey;

				if ( notifyUser )
					ActionManager.Instance.HandleContactSourceKeyChange();
			}
		}

		private PhoneMetaInfoResponse getPhoneMetaInfo( String[] phoneNumbers )
		{
			PhoneMetaInfoResponse result = null;

			for ( int x = 0; x < phoneNumbers.Length; x++ )
			{
				phoneNumbers[x] = PhoneUtils.GetPhoneNumberForCall( phoneNumbers[x], false );
			}

			result = RestfulAPIManager.INSTANCE.getPhoneMetaInfo( SessionManager.Instance.User.UserKey, SessionManager.Instance.User.CompanyUserId, phoneNumbers );

			if (result != null && result.numbers != null )
			{
				if ( result.numbers.Count == 0 )
				{
					logger.Warn( "PhoneMetaInfo has no entries.  Some contact/number info may be missing.");
				}
			}
			else
			{
				logger.Warn( "PhoneMetaInfo response is NULL, or response.numbers is NULL.");
			}

			return result;
		}

		private void mergeContactAndPhoneResponses( ContactListResponse contactListResponse, PhoneMetaInfoResponse phoneMetaInfoResponse, CompanyDirectoryResponse directoryResponse )
		{
			//contactListResponse and phoneMetaInfoResponse work together.
			//directoryResponse is independent.
			VXProfiler prof0 = new VXProfiler( "mergeContactAndPhoneResponses()");

            ContactImportList ciList = new ContactImportList();

			//Extract main Contact info.
			//	ContactKey is used to group phonenumbers to Contacts.

			VXProfiler prof1 = new VXProfiler( "merge Contact & Phone data");
			if ( contactListResponse != null )
			{ 
				foreach ( ContactListResponse.Contact contact in contactListResponse.contacts )
				{
					String fullName		= "";
					String company		= contact.Company;
					String number		= "";
					String numberLabel	= "";

					//Create Name
					fullName = constructFullName( contact.FirstName, contact.LastName );

					//Let's NOT use phone number as name during import.  Just leave it emtpy.
					//	We have logic in Contact class to use phone Number as name where needed.
					//	Using PN here results in incorrect Avatar being generated.
					//if ( fullName == null )
					//{
					//	fullName = "no name or number";
					//
					//	if ( contact.numbers.Count > 0 )	//TODO: We should never have this condition.
					//	{
					//		var iter = contact.numbers.GetEnumerator();
					//		iter.MoveNext();
					//	
					//		fullName = iter.Current.Number;
					//	}
					//}

					if ( contact.numbers.Count == 0 )
					{
						logger.Warn( "No phone numbers found for contactKey " + contact.ContactKey + ".  No entries will be added." );
					}

					//Now loop for each phone number to complete the data.
					foreach ( ContactListResponse.PhoneNumber pn in contact.numbers )
					{ 
						number		= pn.Number;
						numberLabel = pn.Label;

						//Find entry in PhoneMetaInfo to complete data.
						String temp = PhoneUtils.GetPhoneNumberForCall( number, false );
						temp = temp.Replace( "+", "");
						PhoneMetaInfoResponse.Number meta = phoneMetaInfoResponse.numbers.Find( PhoneMetaInfoResponse.ByNumber( temp ) );

						//Get most data from 'meta'.
						ContactImport ci = contactImportFromPhoneMeta( meta, ManagedDataTypes.Contact.DbSource_AddressBook );

						//If 'meta' has valid 'did' then we have two items to add:
						//	1) The contact info, which is likely a mobile number.
						//	2) The DID, which is the Voxox number.
						//Both will be added to the same contact.

						//2) The DID, which is the Voxox number.
						bool addBoth = ( ci.PhoneNumber != null && ci.PhoneNumber != pn.Number );

						if ( addBoth )
						{
							ContactImport ci2 = ci.clone();

							//Add data we have from Contact and/or PhoneNumber.
							ci2.ContactKey	= contact.ContactKey;
							ci2.FirstName	= contact.FirstName;
							ci2.LastName	= contact.LastName;
							ci2.Name		= fullName;
							ci2.Company		= company;

							if ( String.IsNullOrEmpty( ci2.PhoneNumber ) )
								ci2.PhoneNumber = pn.Number;

							if ( String.IsNullOrEmpty( ci2.PhoneLabel ) )
								ci2.PhoneLabel  = pn.Label;

							ciList.Add( ci2 );
						}

						//	1) The contact info, which is likely a mobile number.  Always add this one.
						//Add data we have from Contact and/or PhoneNumber.
						ci.ContactKey = contact.ContactKey;
						ci.FirstName  = contact.FirstName;
						ci.LastName	  = contact.LastName;
						ci.Name		  = fullName;
						ci.Company	  = company;

						ci.PhoneNumber = pn.Number;
						ci.PhoneLabel  = pn.Label;

						//Clear any On-net info, if we already added it.
						if ( addBoth )
						{ 
							ci.UserId	= 0;
							ci.UserName	= "";
							ci.Jid		= "";
						}

						ciList.Add( ci );
					}
				}
			}

			prof1.Stop();

			//Now add CompanyDirectory entries, if any.
			VXProfiler prof2 = new VXProfiler( "merge Company Directory");

			if ( directoryResponse != null )
			{
				foreach( CompanyDirectoryResponse.DirectoryEntry entry in directoryResponse.entries )
				{
					string  contactKey  = Guid.NewGuid().ToString();	//Must be unique.  This is likely what iOS will send for actual contacts.
					string	fullName    = constructFullName( entry.firstName, entry.lastName );
					string  number	    = (entry.extension == null ? entry.dialedNumber : entry.extension);
					string  numberLabel = LocalizedString.GenericExtension;
					bool	isExtension = true;		//Always 'true' for company directory
					bool	isFavorite  = false;	//We hope/expect this to be added to Entry
					bool	isBlocked   = false;	//We do NOT expect this to be added to Entry
					Int32	userId      = 0;		//We do NOT get this, but we capture it for Contacts, so let's be consistent.
					String	userName    = "";		//We do NOT get this, but we capture it for Contacts, so let's be consistent.
					String	company		= entry.companyName;
					String  jabberId	= (entry.jabberId == null ? number + "@voxox.com" : entry.jabberId);	//TODO: Fix when we get jabberID in response.

					if ( fullName == null )		// I suspect this should never happen, but...
					{
						fullName = entry.extension;	
					}

					ciList.Add(new ContactImport( ManagedDataTypes.Contact.DbSource_CompanyDirectory, contactKey, fullName, entry.firstName, entry.lastName, 
												  number, numberLabel, jabberId, isFavorite, isBlocked, isExtension, userId, userName, company ) );
				}
			}

			prof2.Stop();

			addContactsToDb( ciList );

			prof0.Stop();		//Overall time
		}

		//The logic here will get a bit complicated if there are 2 or more 'ContactSources' because:
		//	- For users:
		//	  - UX and PM teams want us to auto-select the more recent
		//	  - Notify user if there is a change from previous use (no user selection permitted at this time)
		//	- For dev/QA (who will very likely have multiple ContactSources due to use same account on different mobile devices)
		//	  - Implementation here is designed by JRT.  Hopefully it will closely match what UX will won't when we do same for user.
		//
		// See <source>/docs/MultipleContactSourcesDesign.txt
		private String chooseContactSourceKey( ContactSourcesResponse sourcesResponse )
		{
			String result = "";

			Boolean showWindow = false;

			if ( sourcesResponse.sources.Count == 1 )
			{ 
				result= sourcesResponse.sources[0].SourceKey;
			}
			else if ( sourcesResponse.sources.Count > 1 )
			{
				if ( SessionManager.Instance.IsContactSourceSelectionEnabled() )
				{
					//Lab/QA logic
					logger.Warn( "Multiple Contact Sources found and Contact Source Seletion is enabled.  Prompt user." );

					result = sourcesResponse.getMostRecent().SourceKey;	//This will be default, if user cancels window.
					showWindow = true;
				}
				else 
				{ 
					//Normal user logic
					logger.Warn( "Multiple Contact Sources found.  Using most recent one based on Modified Date." );

					result = sourcesResponse.getMostRecent().SourceKey;
				}
			}

			if ( showWindow )
			{
				ActionManager.Instance.ShowContactSourceSelection( result, sourcesResponse );
			}
			else 
			{ 
				ActionManager.Instance.SelectContactSource( result );
			}

			return result;
		}
		
		private ContactImport contactImportFromPhoneMeta( PhoneMetaInfoResponse.Number meta, String contactSource )
		{
			ContactImport result = null;

			String contactKey   = "";
			String number		= "";
			String numberLabel	= "";
			String jid			= "";
			Int32  userId		=  0;
			String userName		= "";
			String fullName		= "";
			String firstName	= "";
			String lastName		= "";
			String company		= "";
			bool   isFavorite	= false;
			bool   isBlocked	= false;
			bool   isExtension  = false;	//Always 'false' for contact phone numbers

			if ( meta != null )
			{
				if ( meta.inNetwork != null )
				{
					//We have an in-network phone number/contact, that is not in our AB.  No Name.
					//	Add to DB.
								
					//meta.inNetwork.mobile		- Appears to be always null.
					//meta.inNetwork.userId		- Is the Voxox userId.  Add to DB?
					//meta.inNetwork.username	- Is used by that contact to login (email or mobile number).  Add to DB?

					contactKey  = Guid.NewGuid().ToString();					//Must be unique.  This is likely what iOS will send for actual contacts.
					number		= meta.inNetwork.did;
					userId		= Convert.ToInt32( meta.inNetwork.userId );		//No use for this right now, but since we get from server, let's save it.
					userName	= meta.inNetwork.username;						//No use for this right now, but since we get from server, let's save it.
					jid			= meta.inNetwork.xmppjid;
//					numberLabel = getBestNumberLabel( null, null );
					numberLabel = String.IsNullOrEmpty( jid ) ? getBestNumberLabel( null, null ) : LocalizedString.MiddlePane_Calls_Filter_BrandName;

					isBlocked  = meta.isBlocked();
					isFavorite = meta.isFavorite();

					result = new ContactImport( contactSource, contactKey, fullName, firstName, lastName, number, numberLabel, 
												jid, isFavorite, isBlocked, isExtension, userId, userName, company );
				}
			}

			if ( result == null )
			{
				result		  = new ContactImport();
				result.Source = contactSource;

				if ( meta != null )
				{
					result.IsBlocked   = meta.isBlocked();
					result.IsFavorite  = meta.isFavorite();
					result.PhoneNumber = meta.phoneNumber;
				}
			}

			return result;
		}
		
		private VoxoxContact voxoxContactFromPhoneMeta( PhoneMetaInfoResponse.Number meta, String contactSource )
		{
			VoxoxContact result = null;

			String contactKey   = "";
			String regNumber	= "";
			String did			= "";
			String numberLabel	= "";
			String jid			= "";
			String userName		= "";
			Int32  userId		=  0;
			bool   isFavorite	= false;
			bool   isBlocked	= false;

			if ( meta != null )
			{
				if ( meta.inNetwork != null )
				{

					//We have an in-network phone number/contact, that is not in our AB.  No Name.
					//	Add to DB or update matching entry.
								
					//meta.inNetwork.mobile		- Appears to be always null.
					//meta.inNetwork.userId		- Is the Voxox userId.  Add to DB?
					//meta.inNetwork.username	- Is used by that contact to login (email or mobile number).  Add to DB?

					contactKey  = Guid.NewGuid().ToString();					//Must be unique.  We will not use this in DBM, if did already exists in DB.
					did			= meta.inNetwork.did;
					regNumber	= meta.inNetwork.mobile;
					userId		= Convert.ToInt32( meta.inNetwork.userId );		//No use for this right now, but since we get from server, let's save it.
					userName	= meta.inNetwork.username;						//No use for this right now, but since we get from server, let's save it.
					jid			= meta.inNetwork.xmppjid;
					numberLabel = getBestNumberLabel( null, null );

					isBlocked  = meta.isBlocked();
					isFavorite = meta.isFavorite();

					result = new VoxoxContact( contactKey, regNumber, userName, did, userId, numberLabel, isBlocked, isFavorite, contactSource );
				}
			}

			return result;
		}

		private void addContactsToDb( ContactImportList ciList )
		{
			//Now that we have the data, save it to DB.
			VXProfiler prof3 = new VXProfiler( "Add contacts to DB" );

            var db = SessionManager.Instance.UserDataStore;
            db.addAddressBookContacts(ciList);

			prof3.Stop();

			//Generate Avatars
			VXProfiler prof4 = new VXProfiler( "Generate contact avatars" );

			ContactImportList ciList2 = ciList.getUniqueContactKeys();

			foreach ( ContactImport ci in ciList2 )
			{
				AvatarManager.Instance.Generate( ci.Name, ci.ContactKey );
			}

			prof4.Stop();
		}

		private void addOrUpdateInNetworkContacts( VoxoxContactList vcs )
		{
			getUserDbm().addOrUpdateVoxoxContacts( vcs );
		}

		private void addNakedNumbers( VoxoxContactList vcs )
		{
			getUserDbm().addNakedNumbers( vcs );
		}
		
		public String constructFullName( String firstName, String lastName )
		{
			String fullName = null;

            if (String.IsNullOrEmpty( firstName ) && String.IsNullOrEmpty( lastName ) )
            {
				//Do nothing here.  Caller will handle this condition
            }
            else if (!String.IsNullOrEmpty( firstName) && !String.IsNullOrEmpty( lastName ) )
            {
                fullName = firstName + " " + lastName;
            }
            else if (!String.IsNullOrEmpty( firstName ) )
            {
                fullName = firstName;
            }
            else
            {
                fullName = lastName;
            }

			return fullName;
		}

		#region Contact utilities
		//These *could* be in a separate ContactUtils class.

		//TODO-Contact-Sync: With corrected DB queries, we need PhoneNumber here, not contact.
		public static string getBestNumberLabel( ManagedDataTypes.Contact contact, RecentCall call )
        {
			string result = LocalizedString.GenericDefaultPhoneLabel;

			if (contact != null)
            {
                if (contact.PhoneLabel.Length > 0)
                {
                    result = contact.PhoneLabel;
                }
				//else if (contact.VoxoxUserId > 0)
				//{
				//	result = LocalizedString.MiddlePane_Calls_Text_BrandName;
				//}
            }
            else if ( call != null )
            {
                if (call.DisplayNumber.Length > 0)
                {
                    result = LocalizedString.MiddlePane_Calls_Text_BrandName;
                }
            }
			else
			{
				//This was coded at one of 4-5 locations from which I refactored this code.  
				//It does not seem correct, so I am commenting it.  
				//Specifically, it was on Call History Detail for Recorded Call.
//				result = LocalizedString.MiddlePane_Calls_Text_BrandName;
			}

			return result;
		}

		public static String getBestNumberLabel( PhoneNumber pn )
		{
			String result = String.IsNullOrEmpty( pn.Label) ? LocalizedString.GenericDefaultPhoneLabel : pn.Label;

			return result;
		}
	
		public static string getBestDisplayName( ManagedDataTypes.Contact contact, RecentCall rc, String nameOrNumber )
		{
			string result;
			bool   nameIsPhoneNumber = false;
			
			if ( contact != null )
			{
				result = contact.bestDisplayName( out nameIsPhoneNumber );
			}
			else if ( rc != null )
			{ 
				result = PhoneUtils.FormatPhoneNumber( rc.bestDisplayNumber() );
			}
			else if ( nameOrNumber != null )
			{ 
				result = PhoneUtils.FormatPhoneNumber( nameOrNumber );
			}
			else
			{
				result = "<Unknown>";	//Should never get here.  TODO: translate
			}

			return result;
		}

	#endregion

		//Check recent calls for numbers that are NOT in AB and make API to retrieve inNetwork info, if any.
		public void checkCallsForInNetworkContacts()
		{
			ManagedDataTypes.StringList phoneNumbers = getUserDbm().getPhoneNumbersWithInvalidCmGroup();

			if ( phoneNumbers.Count > 0 )
			{
				String[] temp = phoneNumbers.ToArray();

				isInNetwork( temp );
			}
		}

		public static System.Collections.ObjectModel.ObservableCollection<ContactNumberViewModel> PhoneNumberListToContactNumberViewModel( PhoneNumberList list )
		{
			var	result = new System.Collections.ObjectModel.ObservableCollection<ContactNumberViewModel>();

            foreach (PhoneNumber pn in list)
            {
				ContactNumberViewModel cnv = new ContactNumberViewModel();

                cnv.CmGroup		  = pn.CmGroup;
                cnv.DisplayNumber = PhoneUtils.FormatPhoneNumber( pn.DisplayNumber );
                cnv.NumberType	  = pn.Label;
                cnv.Number		  = pn.Number;
                cnv.RecordId	  = pn.RecordId;
				cnv.ContactKey	  = pn.ContactKey;
				cnv.IsOnNet       = pn.isVoxox();
					
				result.Add( cnv );
            }

			return result;
		}

	}
}
