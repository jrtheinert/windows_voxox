﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Config;				//LoginMode
using Desktop.Model.Calls;			//CallFilterType
using Desktop.Model.Calling;		//PhoneLine.CallError
using Desktop.Model.Contact;        //ContactManager
using Desktop.Model.Messaging;		//MessageManager
using Desktop.Util;					//PhoneUtil

using Desktop.Utils;				//Logger
using Desktop.Views.Contacts;		//ContactSourceSelection window
using Desktop.Views.Help;			//AboutWindow
using Desktop.Views.Login;			//AdvancedSettings window
using Desktop.Views.Settings;		//Settings window, Advanced Settings, ContactSourceSelection window
using Desktop.ViewModels;			//ShowFindMeNumberNotSetupMessage
using Desktop.ViewModels.Main;		//NavigateMessage

using ManagedDataTypes;				//Message

using Telcentris.Voxox.ManagedSipAgentApi.ManagedSipDataTypes;	//PhoneCallState

using Vxapi23;						//RestfulApiManager
using Vxapi23.Model;				//ContactSourcesResponse

using GalaSoft.MvvmLight.Messaging;	//Messenger
using GalaSoft.MvvmLight.Threading;	//DispatcherHelper

using System;
using System.Collections.Generic;	//List
using System.IO;					//Path
using System.Windows;				//MessageBox


//-----------------------------------------------------------------------------
// A small class that handles all the user actions in one place.
//	This keeps the code cleaner and provides consistent handling.
//-----------------------------------------------------------------------------

//=============================================================================
// NOTES on Navigation logic - See NOTES in MainTabNavViewModel
//=============================================================================

namespace Desktop.Model
{

	class ActionManager
	{
        private VXLogger logger = VXLoggingManager.INSTANCE.GetLogger("ActionManager");

		private AboutWindow					mAbout						  = null;
        private Views.Settings.Settings		mSettings					  = null;	//We need 'Views.Settings' due to conflict with 'Model'
		private AdvancedSettingsWindow		mAdvancedSettingsWindow		  = null;
		private AdvancedSettingsWindowV2	mAdvancedSettingsWindowV2	  = null;
		private ContactSourceSelection		mContactSourceSelectionWindow = null;


		//TODO: Not sure why we need these
		public static readonly String MESSAGES_TOKEN		= "MESSAGES_TOKEN";
        public static readonly String COMPOSE_MESSAGE_TOKEN = "COMPOSE_MESSAGE_TOKEN";

		private ActionManager()
		{
		}

        public static ActionManager Instance
        {
            get { return Nested.instance; }
        }

        class Nested
        {
            static Nested()		{}

            internal static readonly ActionManager instance = new ActionManager();
        }

		#region Public URL related methods

		//---------------------------------------------------------------------------
		//Specific URLs - typically branded.
		//---------------------------------------------------------------------------

		public void ShowContactSupportUrl()
		{
			HandleUrl( Desktop.Config.Support.HelpContactSupportLink );
		}

		public void ShowManageAccountUrl()
		{
			HandleUrl( Desktop.Config.Urls.ManageAccountURL );
		}
		
		public void ShowTermsAndConditionsUrl()
		{
			HandleUrl( Desktop.Config.Urls.TermsandConditionsURL );
		}

		public void ShowForgotPasswordUrl()
		{
			HandleUrl(Desktop.Config.Urls.ForgotPasswordURL);
		}

		public void ShowBuyCreditsUrl()
		{
			HandleUrl( Desktop.Config.Urls.BuyCredits_URL);
		}
	
		public void ShowReachMeUrl()
        {
            HandleUrl( Desktop.Config.Urls.ReachmeNavigationLink );
        }

		public void ShowCallerIdUrl()
		{ 
            HandleUrl( Desktop.Config.Urls.CallerIdNavigationLink );
		}

		//Generic method to handle URL and handle exceptions.
		public void HandleUrl( string url )
		{
			//TODO: Add proxy handling?  Should not be an issue for IE (WPAD), but possibly for manual proxy.
            try
            {
				System.Diagnostics.Process.Start( url );
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("Launch failure: {0}", url), ex);
            }
		}

		#endregion

		#region Fax, Image, Video, Chat

		public void ViewImage( Int64 msgTimeStampIn )
		{
			String localFile = MessageManager.getInstance().downloadImage( msgTimeStampIn );

			HandleUrl( localFile );
		}

		public void ViewVideo( Int64 msgTimeStampIn )
		{
			String localFile = MessageManager.getInstance().downloadVideo( msgTimeStampIn );

			HandleUrl( localFile );
		}

		public void ViewFax( Int64 msgTimeStampIn )
		{
			String localFile = MessageManager.getInstance().downloadFax( msgTimeStampIn );

			HandleUrl( localFile );
		}

		public void ResendMessage( Int64 msgTimeStampIn )
		{
			MessageManager.getInstance().resendMessage( msgTimeStampIn );
		}

		#endregion

		
		#region Public Navigation related methods

		public void NavigateTo( Navigation navTo )
		{
			Messenger.Default.Send<MainTabNavigateMessage>(new MainTabNavigateMessage { NavigateTo = navTo });	//Update UI, as needed.

			handlePostNav( navTo );
		}

		//Same as above, but we have the CallFilterType.
		public void NavigateToSubmenu( Navigation navTo, CallFilterType filter )
		{
			Messenger.Default.Send<MainTabNavigateMessage>(new MainTabNavigateMessage { NavigateTo = navTo, CallFilter = filter });

			handlePostNav( navTo );
		}

		private void handlePostNav( Navigation navTo )
		{
			//Handle any 'navTo' specific handling outside UI update, such as displaying Settings window.
			if ( navTo == Navigation.Settings )
			{ 
				ShowSettings();
			}
			else if ( navTo == Navigation.Messages )
			{
				//Under certain conditions we need to force reload of data to clear 'unread' badge
				ReloadMessages();
			}
		}

		public void NavigateToMessages()
		{
			NavigateTo( Navigation.Messages );
		}

		public void NavigateToCalls()
		{
			NavigateTo( Navigation.Calls );
		}

		public void NavigateToVoicemails()
		{
			NavigateToSubmenu( Navigation.Calls, CallFilterType.Voicemail );
		}

		public void NavigateToRecordings()
		{
			NavigateToSubmenu( Navigation.Calls, CallFilterType.Recorded );
		}

		public void NavigateToContacts()
		{
			NavigateTo( Navigation.Contacts );
		}

		public void NavigateToFaxes()
		{
			NavigateTo( Navigation.Fax );
		}

		public void NavigateToDialer()
		{
			NavigateTo( Navigation.Dialer );
		}

		public void NavigateToSettings()
		{
			NavigateTo( Navigation.Settings );
		}

		public void NavigateToStore()
		{
			NavigateTo( Navigation.Store );
		}

		public void NavigateToProfile()
		{
			NavigateTo( Navigation.Profile );
		}

		public void ShowContactMessages( int cmGroup, String number, String contactKey, bool composing )
		{
			if ( composing )
			{
                Messenger.Default.Send<ShowContactMessagesMessage>(new ShowContactMessagesMessage { CmGroup = cmGroup, PhoneNumber = number, ContactKey = contactKey }							);
                Messenger.Default.Send<ShowContactMessagesMessage>(new ShowContactMessagesMessage { CmGroup = cmGroup, PhoneNumber = number, ContactKey = contactKey }, COMPOSE_MESSAGE_TOKEN	);
			}
			else
			{ 
				Messenger.Default.Send<ShowContactMessagesMessage>( new ShowContactMessagesMessage { CmGroup = cmGroup, PhoneNumber = number, ContactKey = contactKey } );
				Messenger.Default.Send<ShowContactMessagesMessage>( new ShowContactMessagesMessage { CmGroup = cmGroup, PhoneNumber = number, ContactKey = contactKey }, MESSAGES_TOKEN        );
				Messenger.Default.Send<ShowContactMessagesMessage>( new ShowContactMessagesMessage { CmGroup = cmGroup, PhoneNumber = number, ContactKey = contactKey }, COMPOSE_MESSAGE_TOKEN );
			}
		}

		public void CloseDialer()
		{ 
			Messenger.Default.Send<DialPadCloseMessage>( new DialPadCloseMessage() );
		}

		public void ShowDialer( bool isFromCall, Int32 callId, string phoneNumber, string countryCode )
		{
            Messenger.Default.Send<DialPadShowMessage>(new DialPadShowMessage { IsFromCall = isFromCall, CallId = callId, PhoneNumber = phoneNumber, CountryCode =countryCode } );
		}

		public void ShowFindMeNumberNotSetupControl( bool value )
		{
			Messenger.Default.Send<ShowFindMeNumberNotSetupMessage>( new ShowFindMeNumberNotSetupMessage{ ShowPopUp = value } );
		}

		public void ShowStore()
		{
			NotImplementedMessage();
		}

        // Store the reference to about window & use it to being it to focus if already created
        public void ShowAbout()
        {
            if ( mAbout == null)
            {
                mAbout = new AboutWindow();
                mAbout.Closed += About_Closed;
            }

            mAbout.Show();
            mAbout.Activate();
        }

        // Make the reference null so that on next show request the object is re-initialized
		public void About_Closed(object sender, EventArgs e)
        {
            mAbout = null;
        }

		//Settings related methods
		public void ShowSettings()
        {
			if ( mSettings == null )
			{
		        mSettings = new Views.Settings.Settings();
			}

            mSettings.Show();
            mSettings.Activate();
        }

		//AdvanceSettings window
		public void ShowAdvancedSettingsV1()
		{
			if ( mAdvancedSettingsWindow == null )
			{ 
				mAdvancedSettingsWindow = new Desktop.Views.Login.AdvancedSettingsWindow();
			}

			mAdvancedSettingsWindow.ShowDialog();
            mAdvancedSettingsWindow.Activate();
		}

		//Advanced Settings V2
		public void ShowAdvancedSettingsV2()
		{
//			if ( mAdvancedSettingsWindowV2 == null )
			{ 
				mAdvancedSettingsWindowV2 = new Desktop.Views.Settings.AdvancedSettingsWindowV2();
			}

			mAdvancedSettingsWindowV2.ShowDialog();
            mAdvancedSettingsWindowV2.Activate();
		}

		public void ShowNoAudioDevicePopup( bool show, bool isAfterLogin )
		{
			Messenger.Default.Send<NoAudioDevicesMessage>(new NoAudioDevicesMessage { IsAfterLogin = isAfterLogin, ShowPopup = show });
		}

		public void ReloadMessages()
		{
			Messenger.Default.Send<ReloadDataMessage>(new ReloadDataMessage() );
		}

		public void Rebrand( BrandId brandId )
		{
			Config.Brand.reinit( brandId );
			Messenger.Default.Send<BrandingChangedMessage>( BrandingChangedMessage.createAsRebrand() );
		}

		public void Rebrand( String apiServer, String xmppServer, String extranetHost, String partnerId, LoginMode loginMode )
		{
			Config.Brand.Rebrand( apiServer, xmppServer, extranetHost, partnerId, loginMode );
			Messenger.Default.Send<BrandingChangedMessage>( BrandingChangedMessage.createAsRebrand() );
		}

		public void UnRebrand( bool updateUI )
		{
			Config.Brand.UnRebrand();

			if ( updateUI )	//TODO: Looks like may never send this message.
			{ 
				Messenger.Default.Send<BrandingChangedMessage>( BrandingChangedMessage.createAsRebrand() );
			}
		}

		public void CloseAdvancedSettingsV2Window()
		{
			Messenger.Default.Send<BrandingChangedMessage>( BrandingChangedMessage.createAsCloseWindow() );
//			mAdvancedSettingsWindowV2 = null;
		}

		public void UpdateAdvancedSettingsV2Window( int brandIndex )
		{
			Messenger.Default.Send<BrandingChangedMessage>( BrandingChangedMessage.createAsUpdateBrandComboBox( brandIndex ) );
		}

		#endregion


		#region Misc action methods

		//File Menu
		public void ToggleKeepMeSignedIn()
        {
			//Toggle it and update MenuItem
			bool temp = !SettingsManager.Instance.App.AutoLogin;
            SettingsManager.Instance.App.AutoLogin = temp;

			HandleAutoLoginChange();
        }

		#endregion

		#region Public and Private Logout/Quit methods

		//Add any additional windows here that may be open but need to be closed upon logout/quit.
		private void CloseWindows()
		{
			if ( mSettings != null )
			{ 
				mSettings.Close();
			}
		}

		public void Logout()
        {
			LogoutOrQuitePrivate( false );
        }

		//No pending call check here, so we keep it private.
		private void LogoutPrivate()
		{
			CloseWindows();

			Messenger.Default.Send<SignoutMessage>(new SignoutMessage());
		}

		public void Quit()
		{
			LogoutOrQuitePrivate( true );
		}

		private void LogoutOrQuitePrivate( bool doQuit )
		{
            if (SessionManager.Instance.HasPendingCalls())
            {
                Messenger.Default.Send<CloseAppInCallMessage>(new CloseAppInCallMessage { Quit = false, ShowPopup = true });	//Should 'false' be 'doQuit'.  Did not seem to work.
            }
            else
            {
				if ( doQuit )
					QuitPrivate();
				else
					LogoutPrivate();
            }
		}

		//No pending call check here, so we keep it private.
		private void QuitPrivate()
		{
			Messenger.Default.Send<SignoutMessage>(new SignoutMessage { Quit = true });
		}

		public void LogoutOrQuit( bool doQuit )
		{
			if ( doQuit )
				Quit();
			else
				LogoutPrivate();
        }

		public void CloseAppInCallPopup()
		{
            Messenger.Default.Send<CloseAppInCallMessage>(new CloseAppInCallMessage { ShowPopup = false });
		}

		public void HandleLoginSuccess()
		{
			Messenger.Default.Send<LoginSuccessMessage>( new LoginSuccessMessage { } );
		}

        public void CloseConfirmMessageDeletePopup()
        {
            Messenger.Default.Send<MessageDeleteMessage>(new MessageDeleteMessage { ShowConfirm = false });
        }

        public void CloseConfirmBlockContactPopup()
        {
            Messenger.Default.Send<ContactBlockMessage>(new ContactBlockMessage { ShowConfirm = false });
        }

		#endregion

		public void ChooseAvatar()
		{
			Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

			//Set filter for file extension and default file extension
			dlg.DefaultExt = ".png";
			dlg.Filter     = "PNG Files (*.png)|*.png|JPG Files (*.jpg)|*.jpg";

			Nullable<bool> result = dlg.ShowDialog();

			if (result == true)
			{
				Messenger.Default.Send<AvatarChooseMessage>(new AvatarChooseMessage() { AvatarUri = dlg.FileName });
			}
		}

		public void CloseProfile()
		{
            Messenger.Default.Send<ProfileCloseMessage>(new ProfileCloseMessage());
		}

		public void NotImplementedMessage()
        {
            MessageBox.Show( LocalizedString.NotImplemented );
        }

		public PhoneLine.CallError MakeCall( String phoneNumberToCallIn )
		{
			Boolean				isExtension			= ContactManager.getInstance().isNumberAnExtension( phoneNumberToCallIn );
			String				phoneNumberToCall	= PhoneUtils.GetPhoneNumberForCall( phoneNumberToCallIn, isExtension );
			PhoneLine.CallError error				= Model.SessionManager.Instance.MakeCall( phoneNumberToCall, isExtension );
			return error;
		}

		public void SetCallerId( String callerId )
		{
			//TODO: Handle 'await'
			CallManager.getInstance().setCallerId ( callerId, true );	//true = singleUse
		}

		public void MessageContact( ManagedDataTypes.Contact contact )
		{
			ShowContactMessages( contact.CmGroup, contact.PreferredNumber, contact.ContactKey, false );
		}

		public void BlockContactConfirm( ManagedDataTypes.Contact contact, bool block )
		{
            Messenger.Default.Send<ContactBlockMessage>( new ContactBlockMessage { ContactKey = contact.ContactKey, CmGroup = contact.CmGroup, PhoneNumber = contact.PhoneNumber, Name = contact.Name, Block = block, ShowConfirm = true } );
		}

		public void BlockPhoneNumberConfirm( ManagedDataTypes.PhoneNumber pn, bool block )
		{
            Messenger.Default.Send<ContactBlockMessage>( new ContactBlockMessage { PhoneNumber = pn.DisplayNumber, Block = block, ShowConfirm = true } );
		}

        public void MessageDeleteConfirm( ManagedDataTypes.Contact contact )
        {
            //Confirm Delete message thread for the contact.
            Messenger.Default.Send<MessageDeleteMessage>( new MessageDeleteMessage { CmGroup = contact.CmGroup, PhoneNumber = contact.PreferredNumber, Name = contact.Name, ShowConfirm = true } );
        }

        public void MessageDelete( int cmGroup, string phoneNumber )
        {   
            UmwMessageList umwMessagelist = MessageManager.getInstance().getMessagesForUmw(cmGroup, phoneNumber);
            MessageList    deleteMessages = new MessageList();

            // Convert UmwMessageList to MessageList
            for(int ii = 0; ii < umwMessagelist.Count;ii++)
            {
                var msg = MessageManager.getInstance().getMessage(umwMessagelist[ii].LocalTimestamp);
                deleteMessages.Add(msg);
            }

            MessageManager.getInstance().deleteMessages(deleteMessages);
        }

        public void BlockContact( String contactKey, bool block )
        {
            var phoneNumberList = ContactManager.getInstance().getContactDetailsPhoneNumbersByKey( contactKey );

            foreach (var number in phoneNumberList)
            {
				BlockNumber( number.Number, block );
            }
        }

        public void BlockNumber( String numberIn, bool block )
        {
            System.Threading.Tasks.Task.Run(() =>
            {
                try
                {
					String number = Util.PhoneUtils.CleanPhoneNumberFormatting( numberIn );
					ContactManager.getInstance().setBlacklisted( number, block );
                }
                catch( Exception ex)
                {
                    logger.Error( "BlockNumber", ex );
                }
            });
        }

        public void FavoriteNumber( String phoneNumber, bool favorite )
        {
            System.Threading.Tasks.Task.Run( () =>
            {
				ContactManager.getInstance().setFavorite( phoneNumber, favorite );
            } );
        }

		public void HandleDebugMenuToggle()
		{
			Messenger.Default.Send<RebuildMenuBarMessage>(new RebuildMenuBarMessage() );
		}

		public void OpenAppDataDir()
		{
			System.Diagnostics.Process.Start( SessionManager.Instance.AppDataFolder );
		}

		public void ChangeGetInitOptionsHandling( int value )
		{
			SettingsManager.Instance.App.GetInitOptionsHandling = value;
		}

		//This is for testing Incoming call UI when incoming calls do not work.
		public void FakeIncomingCall()
		{
			SipAddress sipAddress = SipAddress.fromString( "7605559999", "voxox.com" );	//Off-net
//			SipAddress sipAddress = SipAddress.fromString( "3202728633", "voxox.com" );	//On-net
			int		   statusCode = 0;

			int						 callId		= 0;
			PhoneCallState.CallState callState  = PhoneCallState.CallState.Incoming;

			SessionManager.Instance.setPhoneCallState( callId, callState, sipAddress, statusCode );
		}

		public void ShowContactSourceSelection( String defaultSourceKey, ContactSourcesResponse response )
		{
			//Must do on UI thread
            DispatcherHelper.CheckBeginInvokeOnUI(() =>
            {
				mContactSourceSelectionWindow = new Desktop.Views.Contacts.ContactSourceSelection( response, defaultSourceKey );

				mContactSourceSelectionWindow.ShowDialog();
				mContactSourceSelectionWindow.Activate();
            });
		}

		public void SelectContactSource( String sourceKey )
		{
            System.Threading.Tasks.Task.Run( () =>
            {
				Messenger.Default.Send<SelectContactSourceMessage>(new SelectContactSourceMessage { SourceKey = sourceKey } );
            } );
		}

		public void HandleContactSourceKeyChange()
		{
			//Must do on UI thread
            DispatcherHelper.CheckBeginInvokeOnUI(() =>
            {
	//			Messenger.Default.Send<SelectContactSourceMessage>(new SelectContactSourceMessage { NotifyUser = true } );
				MessageBox.Show( LocalizedString.ContactSourceHasChanged );
			} );
		}

		public void HandleAutoLoginChange()
		{
			Messenger.Default.Send<SettingsChangeMessage>( SettingsChangeMessage.makeForAutoLogin() );
		}

		public void HandleAutoStartChange()
		{
			Messenger.Default.Send<SettingsChangeMessage>( SettingsChangeMessage.makeForAutoStart() );
		}

		public void HandleDefaultCountryCodeChange()
		{
			Messenger.Default.Send<SettingsChangeMessage>( SettingsChangeMessage.makeForDefaultCountryCode() );
		}

		public void PlayDtmf( Char charKey, bool isFromCall )
		{
            if ( !Util.PhoneUtils.KeyPlaysTone( charKey ) )
                return;

            if ( isFromCall )
            {
                Messenger.Default.Send<PlayDtmfOnCallMessage>(new PlayDtmfOnCallMessage { DtmfChar = charKey });
                return;
            }

            string soundFilePath = "";

            switch ( charKey )
            {
                case '0':
                    soundFilePath = Desktop.Config.DialpadSounds.Key0SoundFile; 
                    break;

                case '1':
                    soundFilePath = Desktop.Config.DialpadSounds.Key1SoundFile; 
                    break;

                case '2':
                    soundFilePath = Desktop.Config.DialpadSounds.Key2SoundFile; 
                    break;

                case '3':
                    soundFilePath = Desktop.Config.DialpadSounds.Key3SoundFile; 
                    break;

                case '4':
                    soundFilePath = Desktop.Config.DialpadSounds.Key4SoundFile; 
                    break;

                case '5':
                    soundFilePath = Desktop.Config.DialpadSounds.Key5SoundFile; 
                    break;

                case '6':
                    soundFilePath = Desktop.Config.DialpadSounds.Key6SoundFile; 
                    break;

                case '7':
                    soundFilePath = Desktop.Config.DialpadSounds.Key7SoundFile; 
                    break;

                case '8':
                    soundFilePath = Desktop.Config.DialpadSounds.Key8SoundFile; 
                    break;

                case '9':
                    soundFilePath = Desktop.Config.DialpadSounds.Key9SoundFile; 
                    break;

                case '*':
                    soundFilePath = Desktop.Config.DialpadSounds.KeyStarSoundFile; 
                    break;

                case '#':
                    soundFilePath = Desktop.Config.DialpadSounds.KeyPoundSoundFile; 
                    break;
            }

            if (soundFilePath.Length > 0)
            {
				try
				{
                    soundFilePath = Path.Combine(SessionManager.Instance.AppStartupPath, soundFilePath);
					var keySound = new Sound(soundFilePath);
					keySound.play();
				}

				catch ( System.IO.FileNotFoundException /*e*/ )
				{
					logger.Warn( "Sound file not found: " + soundFilePath );
				}
            }
		}

		public void HandleInitOptionsFail()
		{
			MessageBox.Show( "GetOptions Failed", Config.Branding.AppName, MessageBoxButton.OK, MessageBoxImage.Error );
		}
	}
}
