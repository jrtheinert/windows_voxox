﻿using Desktop.Model.Settings;
using Desktop.Model.Sip;
using Desktop.Util;
using Desktop.Util.Settings;
using Desktop.Utils;
using ManagedDataTypes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Telcentris.Voxox.ManagedSipAgentApi;
using Telcentris.Voxox.ManagedSipAgentApi.ManagedSipDataTypes;

namespace Desktop.Model.Threads
{

    public static class InitUserDetails
    {
        private static int inputVolume;
        private static int outputVolume;
        private static int inputIndex;
        private static int outputIndex;
        private static ArrayList inputDevices = new ArrayList();
        private static ArrayList outputDevices = new ArrayList();
        private static SettingsRegistry registryObj;
        private static SoundDevices soundDevices = new SoundDevices();

        //Variables related to contatcts

        private static ContactList contactList = new ContactList();
        private static VoxoxContactList voxoxContacts = new VoxoxContactList();

        ////Database relsted variables
        private static String mDbName = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Voxox\\DB\\" + Session.getUserName();
        private static ManagedDbManager.ManagedDbManager mDbm = null;

        private static VXLogger logger = VXLoggingManager.INSTANCE.GetLogger("Init");

        #region Local database
        /// <summary>
         /// Initialises thread related to Database tasks
         /// </summary>
        public static void initDB()
        {
            logger.Info("Initialize db by user : " + Session.getUserName());
            if (mDbm == null)
            {
                mDbm = new ManagedDbManager.ManagedDbManager(mDbName);
                mDbm.create();
            }
        }

        private static ManagedDbManager.ManagedDbManager getDbm()
        {
                return mDbm;
        }

        public static void setAllContacts()
        {
            contactList = getDbm().getContacts("", PhoneNumber.ContactFilter.CONTACT_FILTER_ALL);
            //contactList = getDbm().getContactDetailsPhoneNumbers(1627, 1627, true);
        }

        public static ContactList getAllContacts()
        {
            if (contactList != null)
                return contactList;
            return null;
        }

        //TODO Need to update this method once vxapi gets contact list from server.
        private static void setDefaultContacts()
        {
            openDb();
            //clearPhoneNumberTable();

            ContactImportList ciList = new ContactImportList();

            ciList.Add(new ContactImport(1, "Contact 1", "17605551111", "Mobile"));
            ciList.Add(new ContactImport(2, "Contact 2", "17605552222", "Mobile")); 
            ciList.Add(new ContactImport(3, "Contact 3", "17605553333", "Mobile"));
            ciList.Add(new ContactImport(4, "Contact 4", "17605554444", "Mobile"));

            ciList.Add(new ContactImport(5, "Contact 5", "17605555555", "Mobile"));
            ciList.Add(new ContactImport(6, "Contact 6", "17605556666", "Mobile"));
            ciList.Add(new ContactImport(7, "Contact 7", "17605557777", "Mobile"));
            ciList.Add(new ContactImport(8, "Contact 8", "17605558888", "Mobile"));

            getDbm().addAddressBookContacts(ciList);

            VoxoxContactList vcs = new VoxoxContactList();
            vcs.Add(new VoxoxContact("17605552222", "Contact 1", "17605551111", 11111));
            vcs.Add(new VoxoxContact("17605554444", "Contact 3", "17605553333", 22222));

            /**
            * Add or updated DB based on VoxoxContact info.
            */
            getDbm().addOrUpdateVoxoxContacts(vcs);
        }

        private static void openDb()
        {
            logger.Info("Open db by user : " + Session.getUserName());
            if (mDbm == null)
            {
                mDbm = new ManagedDbManager.ManagedDbManager(mDbName);
                mDbm.create();
            }
        }

        // Have to call this method while user logs out. (TODO)
        private static void closeDB()
        {
            logger.Info("close db by user : " + Session.getUserName());
            if(mDbm != null)
            {
                mDbm.close();
            }
        }
        #endregion


        #region Initialize
        public static void initialize()
        {
            logger.Info("Initialize by user : " + Session.getUserName());
            createDirectory();
            setDefaults();
            saveRegistryDetails();

            initDB();
            setDefaultContacts(); // TODO this method is only for testing need to intigrate with Vxapi to get contacts from Voxox server
            setAllContacts();
        }
        #endregion

        #region Default values and directory creation
        /// <summary>
        /// This method will create local directory in 
        /// </summary>
        public static void createDirectory()
        {
            String homedir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Voxox\\DB\\";
            logger.Info("Create homedir " + homedir + " by : " + Session.getUserName());
            if (!System.IO.Directory.Exists(homedir))
            {
                System.IO.Directory.CreateDirectory(homedir);
            }
        }

        /// <summary>
        /// Create a registry key and stores Audio related information on user basis.
        /// </summary>
        private static void saveRegistryDetails()
        {
            registryObj = new SettingsRegistry();
            registryObj.CurrentUser = Session.getUserName();
            if (registryObj.Load(Constants.AUDIO_INPUT_VOLUME) != null)
                registryObj.Save("AudioInputVolume", Convert.ToString(inputVolume));
            if (registryObj.Load(Constants.AUDIO_OUTPUT_VOLUME) != null)
                registryObj.Save("AudioOutputVolume", outputVolume);
            if (registryObj.Load(Constants.AUDIO_INPUT_INDEX) != null)
                registryObj.Save("AudioInputIndex", inputIndex);
            if (registryObj.Load(Constants.AUDIO_OUTPUT_INDEX) != null)
            registryObj.Save("AudioOutputIndex", outputIndex);
        }

        /// <summary>
        /// Sets the default values for settings page
        /// </summary>
        private static void setDefaults()
        {
            inputDevices = soundDevices.GetInputDevices();
            outputDevices = soundDevices.GetOutputDevices();
            if (inputDevices.Count < 0)
                inputDevices.Add(Defaults.defaultText);
            if (outputDevices.Count < 0)
                outputDevices.Add(Defaults.defaultText);
            inputVolume = Defaults.inputVolume;
            outputVolume = Defaults.outputVolume;
            inputIndex = Defaults.defaultIndex;
            outputIndex = Defaults.defaultIndex;
        }
        #endregion

    };

    public static class Init
    {
        
        public static void initThreads()
        {
            InitUserDetails.initialize();
            SipManager.INSTANCE.basicConfiguration();

            // Steps to initiate a call
            //CallManager call = new CallManager();
            //call.makeCall("19148008445");
        }
    }
}