﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

//Currently, our Push Notifications come to us via XMPP device known as JPush at Voxox.
//	Rather than have the Push Notification code confuse the MessageManager, I am creating this class.

using Desktop.Model.Calling;		//CallManager
using Desktop.Model.Messaging;
using Desktop.ViewModels;			//For IncomingEventMessage
using Desktop.Utils;				//Logger

using ManagedDataTypes;				//Message, VxTimestamp

using System;
using System.Collections.Generic;	//List
using System.Threading.Tasks;

namespace Desktop.Model
{
	class PushNotificationManager
	{
		private static readonly String PUSH_NOTIFICATION_SOURCE = "push.voxox.com";		//TODO: Should really get this in SSO info.

		private static PushNotificationManager mInstance = null;

		private VXLogger					   mLogger   = VXLoggingManager.INSTANCE.GetLogger("PushNotificationManager");
		private DuplicateMsgHandler			   mDupeMsgHandler = new DuplicateMsgHandler();

		static public PushNotificationManager getInstance()
		{
			if ( mInstance == null )
			{
				mInstance = new PushNotificationManager();
			}

			return mInstance;
		}

		private PushNotificationManager()
		{
		}
		
		private ManagedDbManager.ManagedDbManager getUserDbm()
		{
			return SessionManager.Instance.UserDataStore;
		}

		public Boolean handledNotification( Message message )
		{
			bool result = false;

			if ( message.FromJid.Contains( PUSH_NOTIFICATION_SOURCE ) )
			{ 
				PushedInfo pushInfo = PushedInfo.fromXml( message.Body );

				//Due to Carbons, we may get the same push notification more than once.
				//	Detect this and return if we have seen it.
				if ( mDupeMsgHandler.hasSeen( pushInfo ) )
				{
					result = true;
					return result;
				}

				mLogger.Debug( "PUSH NOTIFICATION: <" + pushInfo.type + ">" );	//For clarity in log.

				if (pushInfo.isAccountUpdate())
				{
					retrieveDatasummary();
					retrieveCompanyUserOptions();
				}
				else if (pushInfo.isSettingsUpdate())
				{
					retrieveDatasummary();
					retrieveCompanyUserOptions();
				}
				else if (pushInfo.isPasswordChange())
				{
					retrieveDatasummary();
				}
				else if (pushInfo.isContactUpdate())
				{
					retrieveContacts();
				}
				else if (pushInfo.isMessageHistory())
				{
					retrieveMessageHistory();
//					retrieveBadges();				//This may be part of getMessageHistory();
				}
				else if (pushInfo.isMessage())
				{
					retrieveMessageHistory();

					IncomingEventMessage mvvmMsg = new IncomingEventMessage();

					if ( pushInfo.isFax() )
						mvvmMsg.Type = IncomingEventMessage.EventType.Fax;
					else if (pushInfo.isVoiceMail() )
						mvvmMsg.Type = IncomingEventMessage.EventType.Voicemail;
					else
						mvvmMsg.Type = IncomingEventMessage.EventType.Message;

					GalaSoft.MvvmLight.Messaging.Messenger.Default.Send<IncomingEventMessage>( mvvmMsg );
				}
				else if (pushInfo.isCall())
				{
                    if (getUserDbm() != null)
                    {
                        //We get jpush from both legs of call, so check if we already have it.
                        if (!getUserDbm().callExists(pushInfo.id))
                        {
                            retrieveCallHistory();
                        }
                    }
				}
				else if ( pushInfo.isStopRing() )
				{
					//Do nothing.  This is used for 'push calling' on mobile platforms.
					//	It may be pushed to desktop, but have no need for it.
				}
				else
				{
					mLogger.Warn("Unknown PushedInfo type: <" + pushInfo.type + ">  Possibly due to invalid XML." );
				}

				result = true;
			}
			else 
			{ 
				//Chat msg.
				GalaSoft.MvvmLight.Messaging.Messenger.Default.Send<IncomingEventMessage>(new IncomingEventMessage { Type = IncomingEventMessage.EventType.Message } );
			}

			return result;
		}

		//After XMPP reconnect, we need to call ALL pertinent methods to retrieve data.
		public void HandleXmppReconnect()
		{
			retrieveContacts();

			retrieveMessageHistory();
            retrieveCallHistory();

			retrieveDatasummary();
			retrieveCompanyUserOptions();
		}

		/**
		 * Description:
		 * RESTful Server API getDataSummary(): The method will return various data related to the user's account.
		 * When items change server-side, this information is also pushed to the user (via XMPP and APNS).
		 * */
		
		private void retrieveContacts()
		{
			//Reload contacts async'ly
			Task.Run(() => { Desktop.Model.Contact.ContactManager.getInstance().SynchronizeContacts2(); });
		}

		private void retrieveDatasummary() 
		{
			SessionManager.Instance.updateDataSummary();
		}

		private void retrieveCompanyUserOptions() 
		{
			SessionManager.Instance.updateCompanyUserOptions();	//Runs async'ly
		}

		private void retrieveCallHistory()
		{
			CallManager.getInstance().retrieveCallHistory();
		}

		private void retrieveMessageHistory()
		{
			MessageManager.getInstance().retrieveMessageHistory();
		}

		private void retrieveBadges()
		{
//			MessageManager.getInstance().retrieveBadges();		//TODO: this maybe included in results of getMessageHistory().
		}

		#region Duplicate message handler

		//Here will we retain a small queue of previous messages.
		//	We will match them as uniquely as we can.
		//	- Currently we do NOT have an 'id' element.  If we did, that would be a perfect match.
		//	- So, we will match on the <body> and allow for a small, reasonable time difference for the Carbons
		//	- This is not perfect, but should rarely fail, and ONLY if the user is sending the same SMS text message in quick succession.
		private class DuplicateMsgHandler
		{
			const Int64 MAX_TIME_DIFF_FOR_LIST  = 2 * 10000;		//In seconds, based on time intervals of LocalTimestamp.
			const Int64 MAX_TIME_DIFF_FOR_MATCH = 2 * 10000;		//In seconds, based on time intervals of LocalTimestamp.

			private class MsgInfo
			{
				public Int64	Timestamp	{ get; set; }		//This is time we received the message, not the actual message timestamp, if any.
				public String	MsgId		{ get; set;	}		//If this is not empty, this is best match.
				public String	MsgBody		{ get; set; }		//Body of message is best match without 'id'

				public MsgInfo( String msgId, String msgBody )
				{
					Timestamp = VxTimestamp.GetNowTimestamp();
					MsgId     = msgId;
					MsgBody   = msgBody;
				}
			}

			private System.Collections.Generic.List<MsgInfo> MsgInfoList = new System.Collections.Generic.List<MsgInfo>();

			//Logic is:
			//	- Clear out 'old' entries to keep list clean.
			//	- Loop to find match.
			//	- If match found, update the timestamp.
			//	- If match not found, at entry to list.
			public bool hasSeen( PushedInfo pushedInfo )
			{
				bool seenIt = false;

				//Make new MsgInfo to capture timestamp.
				MsgInfo newMsg = new MsgInfo( pushedInfo.id, pushedInfo.fullBody );

				//If both are emtpy we cannot do anything.
				if ( String.IsNullOrEmpty( newMsg.MsgBody ) && String.IsNullOrEmpty( newMsg.MsgId ) )
				{
					return false;
				}

				//This will contain timestamp of 'now', so use it to clear out old entries.
				bool changed = false;

				do 
				{
					changed = false;

					foreach ( MsgInfo msg in MsgInfoList )
					{
						Int64 timeDiff = newMsg.Timestamp - msg.Timestamp;

						if ( timeDiff > MAX_TIME_DIFF_FOR_LIST )
						{
							MsgInfoList.Remove( msg );	//TODO: does this affect following entries?
							changed = true;
							break;
						}
					}
				} while ( changed );

				//Loop for best match.
				bool matched     = false;
				bool matchOnBody = String.IsNullOrEmpty( newMsg.MsgId );

				foreach ( MsgInfo msg in MsgInfoList )
				{
					if ( matchOnBody )
					{
						matched = ( msg.MsgBody == newMsg.MsgBody );
					}
					else
					{
						matched = ( msg.MsgId == newMsg.MsgId );
					}

					if ( matched )
					{
						//Check time difference, and set 'result'.
						Int64 timeDiff = newMsg.Timestamp - msg.Timestamp;

						seenIt = timeDiff < MAX_TIME_DIFF_FOR_MATCH;

						//Update
						msg.Timestamp = newMsg.Timestamp;
						msg.MsgId     = newMsg.MsgId;
						msg.MsgBody	  = newMsg.MsgBody;

						break;
					}
				}

				//If no match, then add to list
				if ( !matched )
				{
					MsgInfoList.Add( newMsg );
				}

				return seenIt;
			}
			
		}

		#endregion

	}	//class PushNotificationManager
}	// namespace Desktop.Model
