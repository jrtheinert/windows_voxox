﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

namespace Desktop.Model.Calls
{
    /// <summary>
    /// Enumeration used to filter calls in the Calls/History pane
    /// </summary>
    public enum CallFilterType
    {
        All,
        Missed,
        Voicemail,
        Recorded
    }
}
