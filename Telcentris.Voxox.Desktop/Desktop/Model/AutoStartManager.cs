﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using GalaSoft.MvvmLight.Messaging;	//Messenger

using System;
using System.Windows.Forms;		//Application

namespace Desktop.Model
{
    public class AutoStartManager
    {
        static String appName = Desktop.Config.Branding.AppName;
		static String exePath = Application.ExecutablePath;

        public static void RegisterInStartup()
        {
 			if ( RegistryManager.AutoStartRegisterFromStartup( appName, exePath ) )
			{
				ActionManager.Instance.HandleAutoStartChange();
			}
        }

        public static void RemoveFromStartup()
        {
 			if ( RegistryManager.AutoStartRemoveFromStartup( appName ) )
			{
				ActionManager.Instance.HandleAutoStartChange();
			}
        }

        public static bool IsInStartup()
        {
			return RegistryManager.AutoStartIsInStartup( appName );
        }
    }
}
