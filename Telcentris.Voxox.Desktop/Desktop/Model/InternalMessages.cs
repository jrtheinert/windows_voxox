﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

//TODO: Move all internal messages to this class

using Desktop.Model.Calling;									//PhoneCall

using Telcentris.Voxox.ManagedSipAgentApi.ManagedSipDataTypes;	//PhoneCallState
using Vxapi23.Model;											//ContactSourcesResponse

using System;

namespace Desktop.Model
{
    public class ConnectionMessage
    {
		private enum Type
		{
			Internet = 0,
			Sip		 = 1,
			Xmpp	 = 2
		};

		private ConnectionMessage()
		{
		}

		private ConnectionMessage( Type type, bool connected )
		{
			ConnType  = type;
			Connected = connected;
		}

		public static ConnectionMessage makeInternet( bool connected )
		{
			return new ConnectionMessage( Type.Internet, connected );
		}

		public static ConnectionMessage makeSip( bool connected )
		{
			return new ConnectionMessage( Type.Sip, connected );
		}

		public static ConnectionMessage makeXmpp( bool connected )
		{
			return new ConnectionMessage( Type.Xmpp, connected );
		}

		private Type	ConnType		{ get; set; }
		public Boolean  Connected		{ get; set; }
	
		public bool isInternet()		{ return ConnType == Type.Internet; }
		public bool isSip()				{ return ConnType == Type.Sip;		}
		public bool isXmpp()			{ return ConnType == Type.Xmpp;		}
    }

	//From ViewModels/main/NoAudioDevicesViewModel.cs
    public class NoAudioDevicesMessage
    {
        public bool IsAfterLogin { get; set; }
        public bool ShowPopup	 { get; set; }
    }

	//From ViewModels/Main/CloseAppInCallViewModel
    public class CloseAppInCallMessage
    {
        public bool Quit		{ get; set; }
        public bool ShowPopup	{ get; set; }
    }

	//From ViewModels/Main/DialpadViewModel
    public class DialPadCloseMessage
    {
    }

	//From ViewModels/Main/DialpadViewModel
    public class DialPadShowMessage
    {
        public bool		IsFromCall	{ get; set; }
        public Int32	CallId		{ get; set; }
        public string	PhoneNumber	{ get; set; }
        public string   CountryCode { get; set; }
    }

    public class AppStateChangeMessage
    {
        public string AppState { get; set; }
    }

	//From ViewModels/Profile/ProfileViewModel
    public class ProfileCloseMessage
    {
    }

	//From ViewModels/Main/MainWindowViewModel
    public class SignoutMessage
    {
        public virtual bool MainWindowClosed { get; set; }
        public virtual bool Quit			 { get; set; }
    }

    public class LoginSuccessMessage
    {
    }

	//From ViewModels/Profile/ProfileViewModel	
    public class AvatarChooseMessage
    {
        public string AvatarUri { get; set; }
    }

	public class RebuildMenuBarMessage
	{
	}

	public class BrandingChangedMessage
	{
		public enum ActionType
		{
			Rebrand				= 0,
			CloseWindow			= 1,
			UpdateBrandComboBox = 2
		}

        private ActionType Action				{ get; set; }
        public int		   BrandSelectedIndex	
		{ 
			get; 
			private set; 
		}

		private BrandingChangedMessage() 		//To force use of static methods
		{
		}

		static public BrandingChangedMessage createAsRebrand()
		{ 
			BrandingChangedMessage result = new BrandingChangedMessage();
			result.Action = ActionType.Rebrand; 
			return result;
		}

		static public BrandingChangedMessage createAsCloseWindow()
		{ 
			BrandingChangedMessage result = new BrandingChangedMessage();
			result.Action = ActionType.CloseWindow; 
			return result;
		}

		static public BrandingChangedMessage createAsUpdateBrandComboBox( int newIndex )
		{ 
			BrandingChangedMessage result = new BrandingChangedMessage();
			result.Action = ActionType.UpdateBrandComboBox; 
			result.BrandSelectedIndex = newIndex;
			return result;
		}

		public Boolean isActionRebrand()				{  return Action == ActionType.Rebrand;				}
		public Boolean isActionCloseWindow()			{  return Action == ActionType.CloseWindow;			}
		public Boolean isActionUpdateBrandComboBox()	{  return Action == ActionType.UpdateBrandComboBox; }
	}

    /// <summary>
    /// This message is raised when a contact is selected to show contact messages
    /// </summary>
    public class ShowContactMessagesMessage
    {
        public int	  CmGroup			{ get; set; }
        public string PhoneNumber		{ get; set; }        // Phone Number of the Contact to start conversation with
        public string ContactKey		{ get; set; }
    }

    public class ReloadDataMessage
    {
    }

    /// <summary>
    /// This is the messages that is published wherever there is a state change in the Call identified by CallId
    /// </summary>
    public class PhoneCallMessage
    {
        public int						CallId		{ get; set; }
        public PhoneCallState.CallState State		{ get; set; }
        public PhoneCall				Call		{ get; set; }
    }

	//Used to allow user (currently dev/QA desired ContactSource if more than one exists)
	public class SelectContactSourceMessage
	{
        public string	SourceKey		{ get; set; }
		public bool		ShowWindow		{ get; set; }
		public bool		NotifyUser		{ get; set; }

		public ContactSourcesResponse Response { get; set; }
	}

	public class SettingsChangeMessage
	{
		private enum SettingType
		{
			Unknown				= 0,
			AutoStart			= 1,
			AutoLogin			= 2,
			DefaultCountryCode	= 3
		};

		private SettingType mType = SettingType.Unknown;

		private SettingsChangeMessage()
		{
		}

		//Static 'make' methods hide implementatation details
		public static SettingsChangeMessage makeForAutoStart()
		{
			return makeForType( SettingType.AutoStart );
		}

		public static SettingsChangeMessage makeForAutoLogin()
		{
			return makeForType( SettingType.AutoLogin );
		}

		public static SettingsChangeMessage makeForDefaultCountryCode()
		{
			return makeForType( SettingType.DefaultCountryCode );
		}

		private static SettingsChangeMessage makeForType( SettingType type )
		{
			SettingsChangeMessage msg = new SettingsChangeMessage{ mType = type };
			return msg;
		}

		//'isType' methods to hide implementation details
		public Boolean isForAutoLogin()
		{
			return isForType( SettingType.AutoLogin );
		}

		public Boolean isForAutoStart()
		{
			return isForType( SettingType.AutoStart );
		}

		public Boolean isForDefaultCountryCode()
		{
			return isForType( SettingType.DefaultCountryCode );
		}

		private Boolean isForType(  SettingType type )
		{
			return ( type == mType );
		}
	}

    public class PlayDtmfOnCallMessage
    {
        public char DtmfChar { get; set; }
    }

}
