﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using System;
using System.Collections.Generic;

namespace Desktop.Model.Settings
{
    public class AudioDevice
    {
        public string	Guid		{ get; set; }
        public string	Name		{ get; set; }
        public bool		IsDefault	{ get; set; }

        public AudioDevice(string guid, string name, bool isDefault)
        {
            this.Guid = guid;
            this.Name = name;
            this.IsDefault = isDefault;
        }

        public AudioDevice()
        {
        }

		public String toString()
		{
			String result = "Guid: "			+ Guid 
						  + ", IsDefault: "		+ IsDefault 
						  + ", Name: "			+ Name 
						  ;

			return result;
		}
	}


	public class AudioDeviceList : List<Desktop.Model.Settings.AudioDevice>
	{

	}
}
