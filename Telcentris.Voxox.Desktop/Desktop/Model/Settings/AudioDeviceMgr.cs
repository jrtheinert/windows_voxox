﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */


using Desktop.Utils;		//Logger

//We intentially do NOT use the following 'using' statements in order to clearly disambiguate SIP dataTypes from Model dataTypes.
//using Desktop.Model.Settings;
//using Telcentris.Voxox.ManagedSipAgentApi.ManagedSipDataTypes;

using Telcentris.Voxox.ManagedSipAgentApi;	//SipAgentApi

using GalaSoft.MvvmLight.Messaging;			//Messenger

using System;
using System.Collections.Generic;

namespace Desktop.Model
{
 	public class DeviceChangeMessage
	{
	}

    class AudioDeviceMgr
    {
		#region | Singleton |

        public static AudioDeviceMgr Instance
        {
            get { return Nested.instance; }
        }

        class Nested
        {
            static Nested()
            {
            }

            internal static readonly AudioDeviceMgr instance = new AudioDeviceMgr();
        }

        #endregion

       #region|private variables|
       
        private static VXLogger logger		  = VXLoggingManager.INSTANCE.GetLogger("AudioDeviceMgr");
		private const String	mNoDeviceGuid = "0";

        #endregion

        #region|Constructor|

        private AudioDeviceMgr()
        {
        }

        #endregion

        #region|public methods|

		//Handle OS USB device change message
		public void HandleDeviceChange()
		{
			//The logic here is tricky.  Typically, on device change, we want to use the device marked 'default' so change is automatic,
			//	such as when user plugs in USB headphones.
			//However, there can be cases where we may not want to do this, and we just use currently selected device, if it is available.
			
			String currentGuid = "";

			//Input
			currentGuid = InputDeviceGuid;

			Desktop.Model.Settings.AudioDeviceList inputDevices = getSystemAudioInputDevices();

			//Compare GUID and Default to change device, if appropriate.
			foreach ( Desktop.Model.Settings.AudioDevice device in inputDevices )
			{
				if ( device.IsDefault )
				{
					if ( device.Guid != currentGuid )
					{
						updateSipInputAudioDevice( device );
						InputDeviceGuid = device.Guid;
						break;
					}
				}
			}


			//Output
			currentGuid = OutputDeviceGuid;

			List<Desktop.Model.Settings.AudioDevice> outputDevices = getSystemAudioOutputDevices();

			//Compare GUID and Default to change device, if appropriate.
			foreach ( Desktop.Model.Settings.AudioDevice device in outputDevices )
			{
				if ( device.IsDefault )
				{
					if ( device.Guid != currentGuid )
					{
						updateSipOutputAudioDevice( device );
						OutputDeviceGuid = device.Guid;
						break;
					}
				}
			}

			//Notify UI
			Messenger.Default.Send<DeviceChangeMessage>(new DeviceChangeMessage() );	//Settings.Audio window should be listening.
		}

		private void ChangeSipAudioDevice( string deviceGuid, bool isInput )
		{
			Desktop.Model.Settings.AudioDevice     deviceToUse = null;
			Desktop.Model.Settings.AudioDeviceList devices;

			if ( isInput )
				devices = getSystemAudioInputDevices();
			else
				devices = getSystemAudioOutputDevices();

			//Compare GUID to find selected device.  If no match, use Default device
			foreach ( Desktop.Model.Settings.AudioDevice device in devices )
			{
				if ( device.IsDefault )
				{
					if ( deviceToUse == null )
						deviceToUse = device;
				}

				if ( device.Guid == deviceGuid )
				{
					deviceToUse = device;
					break;
				}
			}

			if ( isInput )
				updateSipInputAudioDevice( deviceToUse );
			else
				updateSipOutputAudioDevice( deviceToUse );
		}

		private void updateSipInputAudioDevice( Desktop.Model.Settings.AudioDevice deviceIn )
		{
			SipAgentApi.getInstance().setCallInputAudioDevice( toSipDevice( deviceIn ) );
		}

		private void updateSipOutputAudioDevice( Desktop.Model.Settings.AudioDevice deviceIn )
		{
			SipAgentApi.getInstance().setCallOutputAudioDevice( toSipDevice( deviceIn ) );
		}

		private void ChangeSipAudioVolume( int volume, bool isInput )
		{
			if ( isInput )
				SipAgentApi.getInstance().setMicVolume    ( (uint)volume );
			else
				SipAgentApi.getInstance().setSpeakerVolume( (uint)volume );
		}

		private Telcentris.Voxox.ManagedSipAgentApi.ManagedSipDataTypes.AudioDevice toSipDevice( Desktop.Model.Settings.AudioDevice deviceIn )
		{
			//TODO: May need to expand Settings.AudioDevice class.
			Telcentris.Voxox.ManagedSipAgentApi.ManagedSipDataTypes.AudioDevice result = new Telcentris.Voxox.ManagedSipAgentApi.ManagedSipDataTypes.AudioDevice();

			if ( deviceIn != null )
			{ 
				result.Guid			= deviceIn.Guid;
	//			result.Index		= deviceIn.Index;
				result.IsDefault	= deviceIn.IsDefault;
	//			result.IsUseDefault = deviceIn.IsUseDefault;
				result.Name			= deviceIn.Name;
	//			result.Type			= deviceIn.GetType;
			}
			
			return result;
		}

		public void HandleInputDeviceSelection( int deviceIndex )
		{
            String guid = mNoDeviceGuid;		//Assume index is invalid.  Note that deviceIndex may be too high, if a device has been removed.
			
			if ( deviceIndex >= 0 && deviceIndex < GetInputDevices().Count )
			{
				guid = GetInputDevices()[deviceIndex].Guid;
			}

			InputDeviceGuid = guid;
		}

		public void HandleOutputDeviceSelection( int deviceIndex )
		{
            String guid = mNoDeviceGuid;		//Assume index is invalid.  Note that deviceIndex may be too high, if a device has been removed.
			
			if ( deviceIndex >= 0 && deviceIndex < GetOutputDevices().Count )
			{
				guid = GetOutputDevices()[deviceIndex].Guid;
			}

			OutputDeviceGuid = guid;
		}

		//Typically called on startup.
		public void SetAudioInputDeviceFromSettings()
		{
			Desktop.Model.Settings.AudioDeviceList inputDevices = getSystemAudioInputDevices();

			if ( inputDevices.Count > 0 )
			{
				String currentGuid = InputDeviceGuid;
				Desktop.Model.Settings.AudioDevice deviceToUse = inputDevices[0];	//Default if no other match.

				//Compare GUID and Default to change device, if appropriate.
				foreach ( Desktop.Model.Settings.AudioDevice device in inputDevices )
				{
					if ( device.Guid == currentGuid )
					{
						deviceToUse = device;
						break;
					}
					else if ( device.IsDefault )
					{
						deviceToUse = device;	//We do NOT break here, because we want the GUID if we can get it
					}
				}
				
				updateSipInputAudioDevice( deviceToUse );	//Change SIP device
				InputDeviceGuid = deviceToUse.Guid;			//Update settings in case of change
			}
		}

		//Typically called on startup.
		public void SetAudioOutputDeviceFromSettings()
		{
			Desktop.Model.Settings.AudioDeviceList outputDevices = getSystemAudioOutputDevices();

			if ( outputDevices.Count > 0 )
			{
				String currentGuid = OutputDeviceGuid;
				Desktop.Model.Settings.AudioDevice deviceToUse = outputDevices[0];	//Default if no other match.

				//Compare GUID and Default to change device, if appropriate.
				foreach ( Desktop.Model.Settings.AudioDevice device in outputDevices )
				{
					if ( device.Guid == currentGuid )
					{
						deviceToUse = device;
						break;
					}
					else if ( device.IsDefault )
					{
						deviceToUse = device;	//We do NOT break here, because we want the GUID if we can get it
					}
				}
				
				this.updateSipOutputAudioDevice( deviceToUse );		//Change SIP device
				OutputDeviceGuid = deviceToUse.Guid;				//Update settings in case of change
			}
		}

		public String InputDeviceGuid  
		{ 
			get 
			{ 
				return (SettingsManager.Instance.User == null ? "" : SettingsManager.Instance.User.AudioInputDevice);  
			}
			
			set 
			{ 
				if ( SettingsManager.Instance.User != null )
				{
					SettingsManager.Instance.User.AudioInputDevice = value; 
				}

				ChangeSipAudioDevice( value, true );
			}
		}

		public String OutputDeviceGuid
		{ 
			get 
			{ 
				return (SettingsManager.Instance.User == null ? "" : SettingsManager.Instance.User.AudioOutputDevice);  
			}
			
			set 
			{ 
				if ( SettingsManager.Instance.User != null )
				{
					SettingsManager.Instance.User.AudioOutputDevice = value; 
				}

				ChangeSipAudioDevice( value, false );
			}
		}

		public int InputVolume
		{
			get 
			{ 
				return (SettingsManager.Instance.User == null ? 100 : SettingsManager.Instance.User.AudioInputVolume);  
			}

			set 
			{ 
				if ( SettingsManager.Instance.User != null )
				{
					SettingsManager.Instance.User.AudioInputVolume = value; 
				}

				ChangeSipAudioVolume( value, true );
			}
		}

		public int OutputVolume
		{
			get 
			{ 
				return (SettingsManager.Instance.User == null ? 100 : SettingsManager.Instance.User.AudioOutputVolume);  
			}
			set 
			{ 
				if ( SettingsManager.Instance.User != null )
				{
					SettingsManager.Instance.User.AudioOutputVolume = value; 
				}

				ChangeSipAudioVolume( value, false );
			}
		}


        /// <summary>
        /// Calls the SIP Api to get the list of audio input devices
        /// </summary>
        /// <returns>List of available audio input devices</returns>
        public List<Desktop.Model.Settings.AudioDevice> GetInputDevices()
        {
			return getDevices( true );
		}

		/// <summary>
        /// Calls the API to get the list of available devices
        /// </summary>
        /// <returns>List of available output devices</returns>
        public List<Desktop.Model.Settings.AudioDevice> GetOutputDevices()
        {
			return getDevices( false );
		}

		//Generic method to handle input/output devices consistently.
		private List<Desktop.Model.Settings.AudioDevice> getDevices( Boolean isInput )
		{
			//Use fully-qualified type name for SIP data types to disambiguate.
            Desktop.Model.Settings.AudioDeviceList audioDeviceList = null;
			String			audioDesc       = "";

			//Get specifics for input/output
			if ( isInput )
			{
	            audioDeviceList = getSystemAudioInputDevices();
				audioDesc       = "Input ";
			}
			else
			{
				audioDeviceList = getSystemAudioOutputDevices();
				audioDesc       = "Output";
			}

			logger.Debug( audioDesc + " count: " + audioDeviceList.Count );

			foreach ( Desktop.Model.Settings.AudioDevice ad in audioDeviceList)
			{
				logger.Debug( audioDesc + ": " + ad.toString() );
			}

			return audioDeviceList;
        }

		//We need BOTH input and output device available
        public bool DevicesAvailable
        {
            get
            {
                var inputDevices = GetInputDevices();
                if (inputDevices == null || inputDevices.Count == 0)
                {
                    return false;
                }

                var outputDevices = GetOutputDevices();
                if (outputDevices == null || outputDevices.Count == 0)
                {
                    return false;
                }

                return true;
            }
        }

        #endregion

		#region Private methods using NAudio lib to get devices

		private Desktop.Model.Settings.AudioDeviceList getSystemAudioInputDevices()
		{ 
			return getSystemAudioDevices( true );
		}

		private Desktop.Model.Settings.AudioDeviceList getSystemAudioOutputDevices()
		{ 
			return getSystemAudioDevices( false );
		}

		private	Desktop.Model.Settings.AudioDeviceList getSystemAudioDevices( bool input )
		{
			Desktop.Model.Settings.AudioDeviceList result = new Desktop.Model.Settings.AudioDeviceList();

			NAudio.CoreAudioApi.Role				role1			 = NAudio.CoreAudioApi.Role.Communications;	//TODO: Multimedia as well?
			NAudio.CoreAudioApi.Role				role2			 = NAudio.CoreAudioApi.Role.Multimedia;

			NAudio.CoreAudioApi.DataFlow			dataflow		 = ( input ? NAudio.CoreAudioApi.DataFlow.Capture : NAudio.CoreAudioApi.DataFlow.Render );
			NAudio.CoreAudioApi.DeviceState			deviceStateMask  = NAudio.CoreAudioApi.DeviceState.Active;	//All;
			NAudio.CoreAudioApi.MMDeviceEnumerator	deviceEnumerator = new NAudio.CoreAudioApi.MMDeviceEnumerator();	//TODO: Make memvar?
			NAudio.CoreAudioApi.MMDeviceCollection	devices			 = deviceEnumerator.EnumerateAudioEndPoints( dataflow, deviceStateMask );

			NAudio.CoreAudioApi.MMDevice defaultDevice1 = null;
			NAudio.CoreAudioApi.MMDevice defaultDevice2 = null;

			if ( deviceEnumerator.HasDefaultAudioEndpoint( dataflow, role1 ) )
			{
				defaultDevice1 = deviceEnumerator.GetDefaultAudioEndpoint( dataflow, role1 );
			}

			if ( deviceEnumerator.HasDefaultAudioEndpoint( dataflow, role2 ) )
			{
				defaultDevice2 = deviceEnumerator.GetDefaultAudioEndpoint( dataflow, role2 );
			}

			foreach ( NAudio.CoreAudioApi.MMDevice device in devices )
			{
//				bool   supports   = device.SupportsJackDetection;
				bool   isDefault1 = ( defaultDevice1 == null ? false : device.ID == defaultDevice1.ID );
				bool   isDefault2 = ( defaultDevice2 == null ? false : device.ID == defaultDevice2.ID );
				String name       = device.FriendlyName;

				result.Add( new Desktop.Model.Settings.AudioDevice( device.ID, name, isDefault1 ) );
			}

			return result;
		}

		#endregion
	}
}

