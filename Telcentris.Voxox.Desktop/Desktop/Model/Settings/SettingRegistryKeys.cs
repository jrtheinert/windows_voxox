﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Desktop.Model.Settings
{
    class SettingRegistryKeys
    {

        #region| private class variables |

        //CODE-REVIEW: Why use of fields AND properties?
        //              Inconsistent use of 'this'
        //              Inconsistent property names: 'InputVolume' vs. 'AudioOutputVolume'
        private int audioInputVolume;
        private int audioOutputVolume;
        private int audioInput;
        private int audioOutput;

        #endregion

        #region|public properties |

        public int InputVolume{
            get{ return audioInputVolume;}
            set{ this.audioInputVolume = value; }
        }

        public int AudioOutputVolume{

            get { return audioOutputVolume; }
            set { this.audioOutputVolume = value; }
        }

        public int AudioInput
        {
            get { return audioInput; }
            set { this.audioInput = value; }
        }

        public int AudioOutput
        {
            get { return audioOutput; }
            set { this.audioOutput = value; }
        }
        #endregion
    }
}
