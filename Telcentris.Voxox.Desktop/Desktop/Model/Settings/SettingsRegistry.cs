﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Desktop.Utils;

namespace Desktop.Model.Settings
{
    /// <summary>
    /// Sets the user settings to registry
    /// </summary>
    class SettingsRegistry
    {
        #region| private class variables| 
        /// <summary>
        /// Will update the username from session
        /// </summary>
       
        private string currentUser = null;

        private string subKey = "SOFTWARE\\Telcentris\\Voxox\\Desktop\\";

        private VXLogger logger = VXLoggingManager.INSTANCE.GetLogger("SettingsRegistry");

        #endregion



        #region| Methods to load and save properties|

        public VXRegistryManager registryManager;

        public String CurrentUser
        {
            get { return currentUser; }
            set
                {
                   this.currentUser = value;
                   subKey = string.Format("SOFTWARE\\Telcentris\\Voxox\\Desktop\\{0}\\Settings", currentUser);
                   registryManager = new VXRegistryManager(subKey);
                }
        }

        /// <summary>
        /// Check whether subkey exists or not. If not creates and saves
        /// Saves the key , value pair in registry under Currentuser\Settings
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Save<T>(String key, T value)
        {
          if (!registryManager.SubkeyExists())
             {
               if (registryManager.CreateSubkey())
                  {
                     logger.Debug("Subkey created successfully.");
                  }
               else
                     logger.Error("Unable to create subkey!");
                }
                registryManager.Write(key, value);
         }

        /// <summary>
        /// Calls the Vxapi to write the subkey
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public String Load(String key)
        {
           String value = registryManager.Read(key);
           return value;
        }

       #endregion
    }
}
