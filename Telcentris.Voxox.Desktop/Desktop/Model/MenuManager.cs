﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model.Contact;				//ContactManager
using ManagedDataTypes;						//Contact

using System;
using System.Windows;						//Visibility
using System.Windows.Controls;				//Menu, MenuItem
using System.Windows.Controls.Primitives;	//Placement
using System.Windows.Media;					//Brush (for foreground)

using AM = Desktop.Model.ActionManager;

//-----------------------------------------------------------------------------
//This class will handle all MenuItems and Menu building.
//	And should make connections to ActionManager to execute many/most/all menu clicks.
//-----------------------------------------------------------------------------

namespace Desktop.Model
{
	class VxMenuItem : MenuItem
	{
		private Boolean mNeedsUserLogin   = false;
		private Boolean mHideWhenDisabled = false;

		public VxMenuItem( String text, bool needsUserLogin, bool enabled, bool hideWhenDisabled, bool applyPadding )
		{
			base.Header    = text;
			base.IsEnabled = enabled;

			//These should be brandable, but have previously been hard-coded in XAML files.
			//	We use these values for MenuBar items, but they do not (yet) apply to context menus
			if ( applyPadding )
			{ 
				base.Padding = new Thickness( 12, 8, 12, 0 );
				base.Height	 = 32;
			}

			base.Foreground = (Brush) Application.Current.FindResource( "Brush_Dock_Panel_Font" );

			mNeedsUserLogin = needsUserLogin;	
	
			//TODO: Register for message that reports user logged in status change.
			//	Call updateMenuAccordingToUserStatus() accordingly.
		}

		private void updateMenuAccordingToUserStatus( bool userIsLogged )
		{
			if ( mNeedsUserLogin )
			{
				IsEnabled = userIsLogged;
			}
		}

		//This does not seem to work on MenuBar items.
		public void SetEnabled( bool enabled )
		{
			IsEnabled  = enabled;

			if ( enabled )
			{
				Visibility = Visibility.Visible;
			}
			else
			{
				Visibility = mHideWhenDisabled ? Visibility.Collapsed : Visibility.Visible;
			}
		}
	}

	//A small class to make it easy to set commonly used MenuItem properties
	class VxActionMenuItem : MenuItem
	{
		bool mNeedsUserLogin   = false;
		bool mHideWhenDisabled = false;

		public VxActionMenuItem( String text, bool needsUserLogin, bool enabled, bool isCheckable, bool hideWhenDisabled /*Shortcut shortcutKey*/)
		{
			base.Header		 = text;
			base.IsEnabled   = enabled;
			base.IsCheckable = isCheckable;
//			base.Shortcut	 = shortcutKey;

			mNeedsUserLogin   = needsUserLogin;
			mHideWhenDisabled = hideWhenDisabled;
		}

		public void SetEnabled( bool enabled )
		{
			IsEnabled  = enabled;

			if ( enabled )
			{
				Visibility = Visibility.Visible;
			}
			else
			{
				Visibility = mHideWhenDisabled ? Visibility.Collapsed : Visibility.Visible;
			}
		}
	}


	class MenuManager
	{
		private Menu		mMenuBar	= null;

		private VxMenuItem mFileMenu	= new VxMenuItem( LocalizedString.MainMenu_File,  false, true, false, true );
		private VxMenuItem mViewMenu	= new VxMenuItem( LocalizedString.MainMenu_View,  true,  true, true,  true );
		private VxMenuItem mHelpMenu	= new VxMenuItem( LocalizedString.MainMenu_Help,  false, true, false, true );

		//TODO: Add this
		private VxMenuItem mDebugMenu				= new VxMenuItem( LocalizedString.MainMenu_Debug, false, true, false, true  );
		private VxMenuItem mDebugGetInitOptionsMenu	= new VxMenuItem( "getInitOptions() handling",    false, true, false, false );

		//For testing. We eventually want Systray object to use this class, but it needs more work.
		private VxMenuItem mSystrayMenu	= new VxMenuItem( "Systray",					  false, true, false, true );	//OK not localized

		//Actual menu items to be used to populate menus
		//	#1 - Name/Text
		//	#2 - Needs User Login
		//	#3 - Enabled
		//	#4 - IsCheckable
		//	#5 - HideWhenDisabled
		//																				#1												#2     #3    #4	    #5
		//File Menu items
		private VxActionMenuItem	mActionFileManageAccount	= new VxActionMenuItem( LocalizedString.MainMenu_File_ManageAccount,    false, true, false, false );
		private VxActionMenuItem	mActionFileForgotPassword	= new VxActionMenuItem( LocalizedString.MainMenu_File_ForgotPassword,   false, true, false, false );
		private VxActionMenuItem	mActionFileSettings			= new VxActionMenuItem( LocalizedString.MainMenu_File_Options,		    true,  true, false, true  );
		private VxActionMenuItem	mActionFileKeepMeLoggedIn	= new VxActionMenuItem( LocalizedString.MainMenu_File_KeepMeSignedIn,	true,  true, true,  true  );
		private VxActionMenuItem	mActionFileLogout			= new VxActionMenuItem( LocalizedString.MainMenu_File_Signout,			true,  true, false, true  );
		private VxActionMenuItem	mActionFileQuit				= new VxActionMenuItem( LocalizedString.MainMenu_File_Quit,				false, true, false, false );

		//View Menu items
		private VxActionMenuItem	mActionViewMessages			= new VxActionMenuItem( LocalizedString.MainMenu_View_Conversations,	true,  true, false, false );
		private VxActionMenuItem	mActionViewCalls			= new VxActionMenuItem( LocalizedString.MainMenu_View_Calls,			true,  true, false, false );
		private VxActionMenuItem	mActionViewVoicemails		= new VxActionMenuItem( LocalizedString.MainMenu_View_Voicemails,		true,  true, false, false );
		private VxActionMenuItem	mActionViewRecordings		= new VxActionMenuItem( LocalizedString.MainMenu_View_Recordings,		true,  true, false, false );
		private VxActionMenuItem	mActionViewContacts			= new VxActionMenuItem( LocalizedString.MainMenu_View_Contacts,			true,  true, false, false );
		private VxActionMenuItem	mActionViewDialer			= new VxActionMenuItem( LocalizedString.MainMenu_View_Dialer,			true,  true, false, false );
		private VxActionMenuItem	mActionViewStore			= new VxActionMenuItem( LocalizedString.MainMenu_View_Store,			true,  true, false, false );
		private VxActionMenuItem	mActionViewProfile			= new VxActionMenuItem( LocalizedString.MainMenu_View_Profile,			true,  true, false, false );

		//Help Menu items
		private VxActionMenuItem	mActionContactSupport		= new VxActionMenuItem( LocalizedString.MainMenu_Help_ContactSupport,	true,  true, false, false );
		private VxActionMenuItem	mActionShowVersion			= new VxActionMenuItem( LocalizedString.MainMenu_Help_About,			true,  true, false, false );
		private VxActionMenuItem	mActionTermsAndConditions	= new VxActionMenuItem( LocalizedString.MainMenu_Help_Terms,			true,  true, false, false );

		//Debug Menu items - Hard-coding strings here is OK.
		private VxActionMenuItem	mActionDebugAdvancedSettingsV1		= new VxActionMenuItem( "Show Advanced Settings V1",			false, true, false, false );
		private VxActionMenuItem	mActionDebugAdvancedSettingsV2		= new VxActionMenuItem( "Show Advanced Settings V2",			false, true, false, false );
		private VxActionMenuItem	mActionDebugOpenAppDataDir			= new VxActionMenuItem( "Open AppData Dir",						false, true, false, false );
		private VxActionMenuItem	mActionDebugMaxSipLogging			= new VxActionMenuItem( "Max SIP Logging",						false, true, true,  false );
		private VxActionMenuItem	mActionDebugEnableSms				= new VxActionMenuItem( "Enable SMS",							false, true, true,  false );
		private VxActionMenuItem	mActionDebugUseSipTls				= new VxActionMenuItem( "Use SIP TLS",							false, true, true,  false );
		private VxActionMenuItem	mActionDebugDoSipProxyCheck			= new VxActionMenuItem( "Do SIP Proxy Check",					false, true, true,  false );
		private VxActionMenuItem	mActionDebugFakeIncomingCall		= new VxActionMenuItem( "Fake incoming call",					true,  true, false, false );
		private VxActionMenuItem	mActionDebugContactSourceSelection	= new VxActionMenuItem( "Contact Source Selection",				false, true, true,  false );
//		private VxActionMenuItem	mActionDebugForceCrash		   = new VxActionMenuItem( "Force Crash",								false, true, false, false );
//		private VxActionMenuItem	mActionDebugTestUpdater		   = new VxActionMenuItem( "Test Auto Updater",							false, true, false, false );
//		private VxActionMenuItem	mActionDebugTestUi			   = new VxActionMenuItem( "Test UI element",							false, true, false, false );

		//Debug Menu -> GetInitOptions submenu items - Hard-coding strings here is OK.
		private VxActionMenuItem	mActionDebugGetInitOptionsNormal			= new VxActionMenuItem( "Normal",						false, true, true,  false );
		private VxActionMenuItem	mActionDebugGetInitOptionsIgnore			= new VxActionMenuItem( "Ignore",						false, true, true,  false );
		private VxActionMenuItem	mActionDebugGetInitOptionsSimulateFailure	= new VxActionMenuItem( "Simulate Failure",				false, true, true,  false );


		private VxActionMenuItem	mActionSystrayAutoStart		 = new VxActionMenuItem( LocalizedString.SystemTrayMenu_AutoStart,		false, true, false, false );
		private VxActionMenuItem	mActionSystrayShow			 = new VxActionMenuItem( LocalizedString.SystemTrayMenu_OpenAppName,	false, true, false, false );
		private VxActionMenuItem	mActionSystraySettings		 = new VxActionMenuItem( LocalizedString.SystemTrayMenu_Settings,	    true,  true, false, true  );	//MUST be same as MainMenu Settings
		private VxActionMenuItem	mActionSystrayLogout		 = new VxActionMenuItem( LocalizedString.SystemTrayMenu_SignOut,		true,  true, false, false );	//MUST be same as MainMenu_File_Signout
		private VxActionMenuItem	mActionSystrayQuit			 = new VxActionMenuItem( LocalizedString.SystemTrayMenu_Quit,			false, true, false, false );	//MUST be same as MainMenu_File_Quit

		private MenuManager()
		{
			//Add handlers to update menus before they show.
			mFileMenu.SubmenuOpened		+= OnOpenFileMenu;
			mDebugMenu.SubmenuOpened	+= OnOpenDebugMenu;
			mSystrayMenu.SubmenuOpened	+= OnOpenSystrayMenu;

			build();
		}

        public static MenuManager Instance
        {
            get { return Nested.instance; }
        }

        class Nested
        {
            static Nested()		{}

            internal static readonly MenuManager instance = new MenuManager();
        }

		private void build()
		{
//			buildMenuBar();

			buildFileMenu();
			buildViewMenu();
			buildHelpMenu();
			buildDebugMenu();
			buildSystrayMenu();
		}

		#region Methods to build various menus

		public void buildMenuBar( Menu menuBar, bool forceRebuild )
		{
			if ( menuBar.HasItems && !forceRebuild )
				return;

			if ( mMenuBar != null )
			{
				mMenuBar.Items.Clear();
			}

			mMenuBar = menuBar;

			//Menu bar
			if (  isLoggedIn() )
			{ 
				menuBar.Items.Add( mFileMenu );
				menuBar.Items.Add( mViewMenu );
				menuBar.Items.Add( mHelpMenu );

                mActionViewStore.Visibility = SessionManager.Instance.User.Options.IsStoreAvailable? Visibility.Visible : Visibility.Collapsed;
			}
			else
			{
				menuBar.Items.Add( mFileMenu );
//				menuBar.Items.Add( mViewMenu );
				menuBar.Items.Add( mHelpMenu );
			}

//			menuBar.Items.Add( mSystrayMenu );	//Testing Only

			if ( showDebugMenu() )
			{
				menuBar.Items.Add( mDebugMenu);
			}
		}		

		private void buildFileMenu()
		{
			mFileMenu.Items.Add( mActionFileManageAccount	);
			mFileMenu.Items.Add( mActionFileForgotPassword	);
			mFileMenu.Items.Add( mActionFileSettings		);
			mFileMenu.Items.Add( mActionFileKeepMeLoggedIn	);
			mFileMenu.Items.Add( mActionFileLogout			);
			mFileMenu.Items.Add( mActionFileQuit			);

			//AssignHandlers
			mActionFileManageAccount.Click	+= (object sender, System.Windows.RoutedEventArgs e) => { AM.Instance.ShowManageAccountUrl();	};
			mActionFileForgotPassword.Click	+= (object sender, System.Windows.RoutedEventArgs e) => { AM.Instance.ShowForgotPasswordUrl();	};
			mActionFileSettings.Click		+= (object sender, System.Windows.RoutedEventArgs e) => { AM.Instance.NavigateToSettings();		};
			mActionFileKeepMeLoggedIn.Click	+= (object sender, System.Windows.RoutedEventArgs e) => { AM.Instance.ToggleKeepMeSignedIn();	};
			mActionFileLogout.Click			+= (object sender, System.Windows.RoutedEventArgs e) => { AM.Instance.Logout();					};
			mActionFileQuit.Click			+= (object sender, System.Windows.RoutedEventArgs e) => { AM.Instance.Quit();					};
		}

		private void buildViewMenu()
		{
			mViewMenu.Items.Add( mActionViewMessages	);
			mViewMenu.Items.Add( mActionViewCalls		);
			mViewMenu.Items.Add( mActionViewVoicemails	);
			mViewMenu.Items.Add( mActionViewRecordings	);
			mViewMenu.Items.Add( mActionViewContacts	);
			mViewMenu.Items.Add( mActionViewDialer		);
			mViewMenu.Items.Add( mActionViewStore		);
			mViewMenu.Items.Add( mActionViewProfile		);

			//AssignHandlers
			mActionViewMessages.Click	+= (object sender, System.Windows.RoutedEventArgs e) => { AM.Instance.NavigateToMessages();		};
			mActionViewCalls.Click		+= (object sender, System.Windows.RoutedEventArgs e) => { AM.Instance.NavigateToCalls();		};
			mActionViewVoicemails.Click	+= (object sender, System.Windows.RoutedEventArgs e) => { AM.Instance.NavigateToVoicemails();	};
			mActionViewRecordings.Click	+= (object sender, System.Windows.RoutedEventArgs e) => { AM.Instance.NavigateToRecordings();	};
			mActionViewContacts.Click	+= (object sender, System.Windows.RoutedEventArgs e) => { AM.Instance.NavigateToContacts();		};
			mActionViewDialer.Click		+= (object sender, System.Windows.RoutedEventArgs e) => { AM.Instance.NavigateToDialer();		};
			mActionViewStore.Click		+= (object sender, System.Windows.RoutedEventArgs e) => { AM.Instance.NavigateToStore();		};
			mActionViewProfile.Click	+= (object sender, System.Windows.RoutedEventArgs e) => { AM.Instance.NavigateToProfile();		};
		}

		private void buildHelpMenu()
		{
			mHelpMenu.Items.Add( mActionContactSupport		);
			mHelpMenu.Items.Add( mActionShowVersion			);
			mHelpMenu.Items.Add( mActionTermsAndConditions	);

			//AssignHandlers
			mActionContactSupport.Click		+= (object sender, System.Windows.RoutedEventArgs e) => { AM.Instance.ShowContactSupportUrl();		};
			mActionShowVersion.Click		+= (object sender, System.Windows.RoutedEventArgs e) => { AM.Instance.ShowAbout();					};
			mActionTermsAndConditions.Click += (object sender, System.Windows.RoutedEventArgs e) => { AM.Instance.ShowTermsAndConditionsUrl();	};
		}

		private void buildDebugMenu()
		{
			buildDebugGetInitOptionsMenu();

			mDebugMenu.Items.Add( mActionDebugAdvancedSettingsV1	 );
			mDebugMenu.Items.Add( mActionDebugAdvancedSettingsV2	 );
			mDebugMenu.Items.Add( mActionDebugOpenAppDataDir		 );
			mDebugMenu.Items.Add( mActionDebugMaxSipLogging			 );
			mDebugMenu.Items.Add( mActionDebugEnableSms				 );
			mDebugMenu.Items.Add( mActionDebugUseSipTls				 );
			mDebugMenu.Items.Add( mActionDebugDoSipProxyCheck		 );
			mDebugMenu.Items.Add( mActionDebugFakeIncomingCall		 );
			mDebugMenu.Items.Add( mActionDebugContactSourceSelection );
			mDebugMenu.Items.Add( mDebugGetInitOptionsMenu			 );

			
			
//			mDebugMenu.Items.Add( mActionDebugForceCrash		 );
//			mDebugMenu.Items.Add( mActionDebugTestUpdater		 );
//			mDebugMenu.Items.Add( mActionDebugTestUi			 );	//This is a DEBUG option for testing UI changes and/or model layer changes.

			//AssignHandlers
			mActionDebugAdvancedSettingsV1.Click		+= (object sender, System.Windows.RoutedEventArgs e) => { AM.Instance.ShowAdvancedSettingsV1();				};
			mActionDebugAdvancedSettingsV2.Click		+= (object sender, System.Windows.RoutedEventArgs e) => { AM.Instance.ShowAdvancedSettingsV2();				};
			mActionDebugOpenAppDataDir.Click			+= (object sender, System.Windows.RoutedEventArgs e) => { AM.Instance.OpenAppDataDir();						};
			mActionDebugMaxSipLogging.Click				+= (object sender, System.Windows.RoutedEventArgs e) => { handleMaxSipLoggingClick		   ( sender, e );	};
			mActionDebugEnableSms.Click					+= (object sender, System.Windows.RoutedEventArgs e) => { handleEnableSmsClick			   ( sender, e );	};
			mActionDebugUseSipTls.Click					+= (object sender, System.Windows.RoutedEventArgs e) => { handleUseSipTlsClick			   ( sender, e );	};
			mActionDebugDoSipProxyCheck.Click			+= (object sender, System.Windows.RoutedEventArgs e) => { handleDoSipProxyCheck			   ( sender, e );	};
			mActionDebugFakeIncomingCall.Click			+= (object sender, System.Windows.RoutedEventArgs e) => { handleFakeIncomingCall		   ( sender, e );	};
			mActionDebugContactSourceSelection.Click	+= (object sender, System.Windows.RoutedEventArgs e) => { handleContactSourceSelectionClick( sender, e );	};
			
//			mActionDebugForceCrash.Click			+= (object sender, System.Windows.RoutedEventArgs e) => { AM.Instance.NavigateToSettings();			};
//			mActionDebugTestUpdater.Click			+= (object sender, System.Windows.RoutedEventArgs e) => { AM.Instance.ToggleKeepMeSignedIn();		};
//			mActionDebugTestUi.Click				+= (object sender, System.Windows.RoutedEventArgs e) => { AM.Instance.Logout();						};
		}

		private void buildDebugGetInitOptionsMenu()
		{
			mDebugGetInitOptionsMenu.Items.Add( mActionDebugGetInitOptionsNormal		  );
			mDebugGetInitOptionsMenu.Items.Add( mActionDebugGetInitOptionsIgnore		  );
			mDebugGetInitOptionsMenu.Items.Add( mActionDebugGetInitOptionsSimulateFailure );
		
			mActionDebugGetInitOptionsNormal.Click			+= (object sender, System.Windows.RoutedEventArgs e) => { AM.Instance.ChangeGetInitOptionsHandling( 0 );	};
			mActionDebugGetInitOptionsIgnore.Click			+= (object sender, System.Windows.RoutedEventArgs e) => { AM.Instance.ChangeGetInitOptionsHandling( 1 );	};
			mActionDebugGetInitOptionsSimulateFailure.Click	+= (object sender, System.Windows.RoutedEventArgs e) => { AM.Instance.ChangeGetInitOptionsHandling( 2 );	};
		}

		private void buildSystrayMenu()
		{
			mSystrayMenu.Items.Add( mActionSystrayAutoStart );
			mSystrayMenu.Items.Add( mActionSystrayShow		);
			mSystrayMenu.Items.Add( mActionSystraySettings	);
			mSystrayMenu.Items.Add( getSeparator()			);
			mSystrayMenu.Items.Add( mActionSystrayLogout	);
			mSystrayMenu.Items.Add( mActionSystrayQuit		);
		}

		public void showContactContextMenu( String contactKey, String phoneNumber, int cmGroup )
		{
			ContextMenu ctxMenu = buildContactContextMenu( contactKey, phoneNumber, cmGroup, String.Empty );

			if ( ctxMenu != null )
			{
				ctxMenu.Placement = PlacementMode.MousePoint;
				ctxMenu.IsOpen = true;
			}
		}

		public void showHistoryContextMenu( String contactKey, String phoneNumber, int cmGroup )
		{
			ContextMenu ctxMenu = buildHistoryContextMenu( contactKey, phoneNumber, cmGroup, String.Empty );

			if ( ctxMenu != null )
			{
				ctxMenu.Placement = PlacementMode.MousePoint;
				ctxMenu.IsOpen = true;
			}
		}

		public void showMessageContextMenu( String contactKey, String phoneNumber, int cmGroup )
		{
			ContextMenu ctxMenu = buildMessageContextMenu( contactKey, phoneNumber, cmGroup, String.Empty );

			if ( ctxMenu != null )
			{
				ctxMenu.Placement = PlacementMode.MousePoint;
				ctxMenu.IsOpen = true;
			}
		}

		private ContextMenu buildContactContextMenu( String contactKey, String phoneNumber, Int32 cmGroup, String lastPhoneNumberUsed )
		{ 
		   //- Call
		   //- Call - Select Number ->
		   //- Call - Select Caller ID ->

		   //- Message
		   //- Block/Unblock (add confirmation)
		   //- Delete (add confirmation)

			ContextMenu ctxMenu = null;

			//Get contact
			ManagedDataTypes.Contact contact = getContact( contactKey, phoneNumber );

			//Create menu
			ctxMenu = new ContextMenu();

			addCommonCallRelatedMenuItems    ( ctxMenu, contact, phoneNumber, lastPhoneNumberUsed );
			addCommonMessageAndBlockMenuItems( ctxMenu, contact, phoneNumber );

			//TODO: Add DeleteContact();

			return ctxMenu;
		}

		private ContextMenu buildHistoryContextMenu( String contactKey, String phoneNumber, Int32 cmGroup, String lastPhoneNumberUsed )
		{ 
		   //-- Call
		   //-- Call - Select Number ->
		   //-- Call - Select Caller ID ->

		   //-- Message
		   //-- Block/Unblock (add confirmation)
		   //-- Delete (add confirmation)

			ContextMenu ctxMenu = null;

			//Get contact
			ManagedDataTypes.Contact contact = getContact( contactKey, phoneNumber );

			//Create menu
			ctxMenu = new ContextMenu();

			addCommonCallRelatedMenuItems    ( ctxMenu, contact, phoneNumber, lastPhoneNumberUsed );
			addCommonMessageAndBlockMenuItems( ctxMenu, contact, phoneNumber );

			//TODO: Add DeleteCall();

			return ctxMenu;
		}

		private ContextMenu buildMessageContextMenu( String contactKey, String phoneNumber, Int32 cmGroup, String lastPhoneNumberUsed )
		{ 
		   //-- Call
		   //-- Call - Select Number ->
		   //-- Call - Select Caller ID ->
		   //-- Delete

			ContextMenu ctxMenu = null;

			//Get contact
			ManagedDataTypes.Contact contact = getContact( contactKey, phoneNumber );

			//Create menu
			ctxMenu = new ContextMenu();

			addCommonCallRelatedMenuItems    ( ctxMenu, contact, phoneNumber, lastPhoneNumberUsed );
			addCommonMessageAndBlockMenuItems( ctxMenu, contact, phoneNumber );
			addDeleteMessageThread			 ( ctxMenu, contact, phoneNumber, cmGroup );

			return ctxMenu;
		}

		private ManagedDataTypes.Contact getContact( string contactKey, string phoneNumberIn )
		{
			ManagedDataTypes.Contact contact = null;

			if ( !String.IsNullOrEmpty( contactKey ) )
			{
				contact = ContactManager.getInstance().getContactByKey( contactKey );
			}
			else 
			{ 
				String phoneNumber = getUserDbm().normalizePhoneNumber( phoneNumberIn );
				contact = ContactManager.getInstance().getMatchingContact( new string[] {phoneNumber} );
			}

			return contact;
		}

		private void addCommonCallRelatedMenuItems( ContextMenu ctxMenu, ManagedDataTypes.Contact contact, String phoneNumber, String lastNumberUsed )
		{
			//This portion is common to context menus for: Message List, Contact List, Call History List.
		   //- Call
		   //- Call - Select Number ->
		   //- Call - Select Caller ID ->

			//Gather needed data.
			PhoneNumberList phoneNumbers = null;
			String			numberToCall = String.Empty;

			if ( contact != null )
			{ 
				phoneNumbers = ContactManager.getInstance().getContactDetailsPhoneNumbersByKey( contact.ContactKey );
			}
			else 
			{ 
				phoneNumbers = new PhoneNumberList();
				PhoneNumber pn = ContactManager.getInstance().getPhoneNumber( phoneNumber, -1 );

				if ( pn.isValid() )
					phoneNumbers.Add( pn );
			}
			
			//We *may* not have a valid number, so check.
			if ( phoneNumbers.Count > 0 )
			{ 
				numberToCall = String.IsNullOrEmpty( lastNumberUsed ) ? phoneNumbers[0].DisplayNumber : lastNumberUsed;

				if ( !String.IsNullOrEmpty( numberToCall ) )
				{ 
					VxActionMenuItem actionCall = new VxActionMenuItem( LocalizedString.GenericCall, false, true, false, false );

					actionCall.DataContext	 = numberToCall;
					actionCall.Click		+= handleCallClick;
					ctxMenu.Items.Add( actionCall );

					//Include the 'Call -> Select Number' option only if we have more than one phone number
					if ( phoneNumbers.Count > 1 )
					{
						VxMenuItem phoneNumberSubmenu = new VxMenuItem( LocalizedString.ContextMenu_Call_SelectNumber, true, true, false, false );

						ctxMenu.Items.Add( phoneNumberSubmenu );

						foreach ( PhoneNumber pn in phoneNumbers )
						{
							String			 text				= pn.Label + " " + pn.DisplayNumber;
							VxActionMenuItem actionCallNumber	= new VxActionMenuItem( text, true, true, false, false );
					
							actionCallNumber.DataContext = pn;
							actionCallNumber.Click		+= handleSelectNumberToCallClick;

							phoneNumberSubmenu.Items.Add( actionCallNumber );
						}
					}
				}
			}

			//Include the 'Call -> Select Caller ID' option only if USER has more than one caller ID
			if ( SessionManager.Instance.User.CallerIDs.Count > 1 )
			{
				VxMenuItem callerIdSubmenu = new VxMenuItem( LocalizedString.ContextMenu_Call_SelectCallerId, true, true, false, false );

				ctxMenu.Items.Add( callerIdSubmenu );

				foreach ( User.CallerID callerId in SessionManager.Instance.User.CallerIDs )
				{
					String			 text			 = callerId.Name + " " + callerId.Number;
					VxActionMenuItem actionCallerID	 = new VxActionMenuItem( text, true, true, false, false );
					
					actionCallerID.DataContext  = callerId;
					actionCallerID.Click	   += handleSelectCallerIDClick;

					callerIdSubmenu.Items.Add( actionCallerID );
				}
			}
		}
	
		private void addCommonMessageAndBlockMenuItems( ContextMenu ctxMenu, ManagedDataTypes.Contact contact, String phoneNumber )
		{
			//-- Message

			if ( contact != null )
			{ 
				if ( contact.canBeMessaged( Config.Settings.EnableSMS ) )
				{
					VxActionMenuItem actionMessage = new VxActionMenuItem( LocalizedString.ContextMenu_Message, true, true, false, false );

					actionMessage.DataContext	 = contact;
					actionMessage.Click			+= handleMessageClick;

					ctxMenu.Items.Add( actionMessage );
				}

				//-- Block/Unblock (add confirmation)
				//Some special cases...
				//We do not block Extensions, so if contact has just extensions, do not add menu item(s).
				if ( contact.ExtensionCount == contact.PhoneNumberCount && contact.ExtensionCount == 1 )
				{
					//Do nothing.
				}
				else
				{ 
					bool isBlocked = contact.BlockedCount > 0;	//For temp testing
		//			if ( contact.IsBlocked() )	//TODO: this will come from new Contact API calls
					{
						String text = getBlockingText( isBlocked );

						VxActionMenuItem actionBlock = new VxActionMenuItem( text, true, true, false, false );

						actionBlock.DataContext	 = contact;
						if ( isBlocked )
						{
							actionBlock.Click += handleUnBlockClick;
						}
						else
						{
							actionBlock.Click += handleBlockClick;
						}

						ctxMenu.Items.Add( actionBlock );
					}
				}
			}
			else 
			{ 
				//Message
				if ( Config.Settings.EnableSMS )
				{
					VxActionMenuItem actionMessage = new VxActionMenuItem( LocalizedString.ContextMenu_Message, true, true, false, false );

					actionMessage.DataContext	 = phoneNumber;
					actionMessage.Click			+= handleMessageClick;

					ctxMenu.Items.Add( actionMessage );
				}

				//Block/Unblock
				PhoneNumber pn		  = ContactManager.getInstance().getPhoneNumber( phoneNumber, -1 );

				if ( pn.isValid() )
				{ 
					bool   isBlocked = pn.IsBlocked;
					String text      = getBlockingText( isBlocked );

					VxActionMenuItem actionBlock = new VxActionMenuItem( text, true, true, false, false );

					actionBlock.DataContext	 = pn;	//NOTE: this differs from Contact != null
					if ( isBlocked )
					{
						actionBlock.Click += handleUnBlockClick;
					}
					else
					{
						actionBlock.Click += handleBlockClick;
					}

					ctxMenu.Items.Add( actionBlock );
				}
			}
		}

		private void addDeleteMessageThread( ContextMenu ctxMenu, ManagedDataTypes.Contact contact, String phoneNumber, int cmGroup )
		{
            VxActionMenuItem actionMessage = new VxActionMenuItem( LocalizedString.GenericDelete, true, true, false, false);

			//If contact is null, let's create one for consistency
			if ( contact == null )
			{
				contact = new ManagedDataTypes.Contact();
				contact.Name			= phoneNumber;
				contact.PreferredNumber = phoneNumber;
				contact.CmGroup			= cmGroup;
			}

            actionMessage.DataContext = contact;
            actionMessage.Click		 += handleMessageDeleteClick;

            ctxMenu.Items.Add(actionMessage);
		}

		private String getBlockingText( bool isBlocked )
		{
			return (isBlocked ? LocalizedString.GenericUnblock : LocalizedString.GenericBlock );	//Note the text is opposite of current setting.
		}

		#endregion

		#region Methods to update existing menu items based on conditions

		private void OnOpenSystrayMenu( object sender, System.EventArgs e )
		{
			//Enable/Disable 'logout'
			mActionSystrayLogout.SetEnabled( isLoggedIn() );
		}

		private void OnOpenFileMenu( object sender, System.EventArgs e )
		{
			//Show/Hide some items based on logged in status
			mActionFileSettings.SetEnabled		( isLoggedIn() );
			mActionFileKeepMeLoggedIn.SetEnabled( isLoggedIn() );
			mActionFileLogout.SetEnabled		( isLoggedIn() );

			//Misc
			mActionFileKeepMeLoggedIn.IsChecked = SettingsManager.Instance.App.AutoLogin;
		}

		private void OnOpenDebugMenu( object sender, System.EventArgs e )
		{
			//Enabled/Disabled
			mActionDebugAdvancedSettingsV1.SetEnabled	 ( !isLoggedIn() );
			mActionDebugAdvancedSettingsV2.SetEnabled	 ( !isLoggedIn() );
			mActionDebugMaxSipLogging.SetEnabled		 ( !isLoggedIn() );
			mActionDebugEnableSms.SetEnabled			 ( !isLoggedIn() );
			mActionDebugUseSipTls.SetEnabled			 ( !isLoggedIn() );
			mActionDebugFakeIncomingCall.SetEnabled		 ( isLoggedIn()  );	//Only AFTER login
			mActionDebugContactSourceSelection.SetEnabled( !isLoggedIn() );

			//Checked/Unchecked
			mActionDebugMaxSipLogging.IsChecked			 = SettingsManager.Instance.App.MaxSipLogging;
			mActionDebugUseSipTls.IsChecked				 = SettingsManager.Instance.Sip.UseTls;
			mActionDebugDoSipProxyCheck.IsChecked		 = SettingsManager.Instance.Sip.DoProxyCheck;
			mActionDebugEnableSms.IsChecked				 = Config.Settings.EnableSMS;
			mActionDebugContactSourceSelection.IsChecked = SessionManager.Instance.EnableContactSourceSelection;

			mActionDebugGetInitOptionsNormal.IsChecked			= SettingsManager.Instance.App.GetInitOptionsHandlingIsNormal();
			mActionDebugGetInitOptionsIgnore.IsChecked			= SettingsManager.Instance.App.GetInitOptionsHandlingIsIgnore();
			mActionDebugGetInitOptionsSimulateFailure.IsChecked	= SettingsManager.Instance.App.GetInitOptionsHandlingIsSimulateFailure();
		}

		#endregion

		private void handleCallClick( object sender, System.Windows.RoutedEventArgs e )
		{
			VxActionMenuItem item = (VxActionMenuItem)sender;
											
			if ( item != null )
			{ 
				String phoneNumber = (String)item.DataContext;
				AM.Instance.MakeCall( phoneNumber );
			}
		}

		private void handleSelectNumberToCallClick( object sender, System.Windows.RoutedEventArgs e )
		{
			VxActionMenuItem item = (VxActionMenuItem)sender;
											
			if ( item != null )
			{ 
				PhoneNumber pn = (PhoneNumber)item.DataContext;

				if ( pn != null )
				{ 
					AM.Instance.MakeCall( pn.DisplayNumber );
				}
			}
		}

		private void handleSelectCallerIDClick( object sender, System.Windows.RoutedEventArgs e )
		{
			VxActionMenuItem item = (VxActionMenuItem)sender;
											
			if ( item != null )
			{ 
				User.CallerID callerId = (User.CallerID)item.DataContext;

				if ( callerId != null )
				{ 
					AM.Instance.SetCallerId( callerId.Number );
				}
			}
		}

		private void handleMessageClick( object sender, System.Windows.RoutedEventArgs e )
		{
			VxActionMenuItem item = (VxActionMenuItem)sender;
											
			if ( item != null )
			{ 
				if ( item.DataContext.GetType() == typeof( ManagedDataTypes.Contact ) )
				{ 
					ManagedDataTypes.Contact contact = (ManagedDataTypes.Contact)item.DataContext;

					if ( contact != null )
					{ 
						AM.Instance.MessageContact( contact );
					}
				}
//				else if ( item.DataContext.GetType() == typeof( ManagedDataTypes.PhoneNumber ) )
				else if ( item.DataContext.GetType() == typeof( String ) )
				{ 
//	                ManagedDataTypes.PhoneNumber pn = (ManagedDataTypes.PhoneNumber)item.DataContext;
	                String phoneNumber = (String)item.DataContext;

		            if (phoneNumber != null)
			        {
//				        AM.Instance.MessageContact( contact );
						int xxx = 1;
					}
				}
			}
		}

		private void handleBlockClick( object sender, System.Windows.RoutedEventArgs e )
		{
			VxActionMenuItem item = (VxActionMenuItem)sender;

			handleBlockAction( item, true );
		}

		private void handleUnBlockClick( object sender, System.Windows.RoutedEventArgs e )
		{
			VxActionMenuItem item = (VxActionMenuItem)sender;

			handleBlockAction( item, false );
		}
		
		private void handleBlockAction( VxActionMenuItem item, bool block )
		{
			if ( item != null )
			{ 
				if ( item.DataContext.GetType() == typeof( ManagedDataTypes.Contact ) )
				{ 
					ManagedDataTypes.Contact contact = (ManagedDataTypes.Contact)item.DataContext;

					if ( contact != null )
					{ 
						AM.Instance.BlockContactConfirm( contact, block );
					}
				}
				else if ( item.DataContext.GetType() == typeof( ManagedDataTypes.PhoneNumber ) )
				{ 
	                ManagedDataTypes.PhoneNumber pn = ( ManagedDataTypes.PhoneNumber)item.DataContext;

		            if (pn != null)
			        {
				        AM.Instance.BlockPhoneNumberConfirm( pn, block );
					}
				}
			}
		}

        private void handleMessageDeleteClick( object sender, System.Windows.RoutedEventArgs e )
        {
            VxActionMenuItem item = (VxActionMenuItem)sender;

            if ( item != null )
            {
				if ( item.DataContext.GetType() == typeof( ManagedDataTypes.Contact ) )
				{ 
					ManagedDataTypes.Contact contact = (ManagedDataTypes.Contact)item.DataContext;

					if ( contact != null )
					{
						AM.Instance.MessageDeleteConfirm( contact );
					}
				}
            }
        }

		private void handleMaxSipLoggingClick( object sender, System.Windows.RoutedEventArgs e ) 
		{
			//Just toggle value.
			SettingsManager.Instance.App.MaxSipLogging = !SettingsManager.Instance.App.MaxSipLogging;
		}

		private void handleEnableSmsClick( object sender, System.Windows.RoutedEventArgs e ) 
		{
			//Just toggle value.
			Config.Settings.EnableSMS = !Config.Settings.EnableSMS;
		}

		private void handleUseSipTlsClick( object sender, System.Windows.RoutedEventArgs e ) 
		{
			//Just toggle value.
			SettingsManager.Instance.Sip.UseTls= !SettingsManager.Instance.Sip.UseTls;
		}

		private void handleDoSipProxyCheck( object sender, System.Windows.RoutedEventArgs e ) 
		{
			//Just toggle value.
			SettingsManager.Instance.Sip.DoProxyCheck= !SettingsManager.Instance.Sip.DoProxyCheck;
		}

		private void handleFakeIncomingCall( object sender, System.Windows.RoutedEventArgs e ) 
		{
			AM.Instance.FakeIncomingCall();
		}

		private void handleContactSourceSelectionClick( object sender, System.Windows.RoutedEventArgs e ) 
		{
			//Just toggle value.
			SessionManager.Instance.EnableContactSourceSelection = !SessionManager.Instance.EnableContactSourceSelection;
		}


		#region Helper methods

		private object getSeparator()
		{
			return new System.Windows.Controls.Separator();
		}

		private bool isLoggedIn()
		{
			return SessionManager.Instance.IsUserLoggedIn;
		}

		private bool showDebugMenu()
		{
			return SessionManager.Instance.IsDebugMenuEnabled;
		}

		private ManagedDbManager.ManagedDbManager getUserDbm()
		{
			return SessionManager.Instance.UserDataStore;
		}

		#endregion
	}
}
