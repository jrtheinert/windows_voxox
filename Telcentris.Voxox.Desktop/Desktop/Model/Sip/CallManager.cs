﻿using Desktop.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Desktop.Model.Sip
{
    class CallManager
    {
        private static VXLogger callManagerLog = VXLoggingManager.INSTANCE.GetLogger("CallManager");

        public void makeCall(String number)
        {
            //String number = "19148008445";
            Boolean useVideo = false;	//Not supported

            Int32 callId = SipManager.INSTANCE.getSIPApi().makeCall(SipManager.INSTANCE.getLineId(), number, useVideo);
            System.Threading.Thread.Sleep(50000);		//Testing.  Wait for remote end to answer.  Looking for hits in AppCallback object.
            SipManager.INSTANCE.terminate();	//Move this when testing items below.
        }

        public void sendRingingNotification(Int32 callId, Boolean enableVideo)
        {
            callManagerLog.Debug("send ringing notification initiated");
            SipManager.INSTANCE.getSIPApi().sendRingingNotification(callId, enableVideo);
        }

        public void acceptCall(Int32 callId, Boolean enableVideo)
        {
            callManagerLog.Debug("accept a call from call id " + callId);
            SipManager.INSTANCE.getSIPApi().acceptCall(callId, enableVideo);
        }

        public void rejectCall(Int32 callId)
        {
            SipManager.INSTANCE.getSIPApi().rejectCall(callId);
        }

        public void closeCall(Int32 callId)
        {
            SipManager.INSTANCE.getSIPApi().closeCall(callId);
        }

        public void holdCall(Int32 callId)
        {
            SipManager.INSTANCE.getSIPApi().holdCall(callId);
        }

        public void resumeCall(Int32 callId)
        {
            SipManager.INSTANCE.getSIPApi().resumeCall(callId);
        }

        public void blindTransfer(Int32 callId, String sipAddress)
        {
            SipManager.INSTANCE.getSIPApi().blindTransfer(callId, sipAddress);
        }

        public void playDtmf(Int32 callId, char dtmf)
        {
            SipManager.INSTANCE.getSIPApi().playDtmf(callId, dtmf);
        }

        public void playSoundFile(Int32 callId, String soundFile)
        {
            SipManager.INSTANCE.getSIPApi().playSoundFile(callId, soundFile);
        }

        public String getAudioCodecUsed(Int32 callId)
        {
            return SipManager.INSTANCE.getSIPApi().getAudioCodecUsed(callId);
        }

        public Boolean isCallEncrypted(Int32 callId)
        {
            return SipManager.INSTANCE.getSIPApi().isCallEncrypted(callId);
        }
    }
}
