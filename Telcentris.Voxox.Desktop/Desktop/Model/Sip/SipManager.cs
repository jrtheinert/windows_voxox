﻿using Desktop.Util;
using Desktop.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Telcentris.Voxox.ManagedSipAgentApi;
using Telcentris.Voxox.ManagedSipAgentApi.ManagedSipDataTypes;

namespace Desktop.Model.Sip
{
    class SipManager
    {
        private static VXLogger sipVMLogger = VXLoggingManager.INSTANCE.GetLogger("SipManager");

        public static SipManager INSTANCE = new SipManager();

        SipAgentApi mSipAgentApi = null;
        static ServerInfoList s_servers = new ServerInfoList();

        String voxoxDid = Convert.ToString(Session.getUser().did);
        String sipUserId = Convert.ToString(Session.getUser().did);
        String sipPassword = Session.getUser().sipPassword;
        String userName = Convert.ToString(Session.getUser().did);
        String displayName = Session.getUserName();
        String password = Session.getUser().sipPassword;
        String realm = "voxox.com";

        //App-level values
        String sipUserUuid;
        Boolean debugLogging = true;

        //These SIP configuration values came from old desktop app SSO 
        SipAgent.Topology topology = SipAgent.Topology.TOPOLOGY_ICE;
        Boolean enableIceMedia = false;
        Boolean enableIceSecurity = true;
        Boolean topologyEncryption = true;
        Boolean topologyTurn = true;

        #if USE_TLS
	        Boolean sipUdp = false;
	        Boolean sipTcp = false;
	        Boolean sipTls = true;
        #else
            Boolean sipUdp = true;
            Boolean sipTcp = true;
            Boolean sipTls = false;
        #endif

        Boolean sipKeepAlive = true;
        Boolean sipUseRport = true;
        Int32 registrationRefreshInterval = 0;
        Int32 failureInterval = 0;
        Int32 optionKeepAlive = 30;
        Int32 minimumSessionTime = 1800;
        Boolean supportSessionTimer = true;
        Boolean initiateSessionTimer = false;
        String tagProlog = "voxox";
        UInt32 registerTimeout = 2940;		//In seconds
        UInt32 publishTimeout = 300;		//In seconds
        Boolean useOptionsRequest = true;
        Boolean optionsP2PPresence = false;
        Boolean optionsUseTypingState = true;
        Boolean chatWithoutPresence = false;

        Boolean aec = true;
        Boolean agc = true;
        Boolean noiseSuppression = true;
        SipAgent.CallEncryption mCallEncryption = SipAgent.CallEncryption.ENCRYPTION_NONE;

        Int32 lineId = -1;				//LineId from most recent registration.
        Int32 callId;

        public SipAgentApi getSIPApi()
        {
            if (mSipAgentApi == null)
            {
                sipVMLogger.Debug("mSipAgentApi is null");
                mSipAgentApi = SipAgentApi.getInstance();
                addSipEventHandlers();
            }

            return mSipAgentApi;
        }

        public void initSIPApi()
        {
            getSIPApi();

            if (!mSipAgentApi.isInitialized())
            {
                sipVMLogger.Debug("SipAgentApi initialized");
                mSipAgentApi.init();
            }
        }

        public void terminateSIPApi() // To be used while user logs out.
        {
            getSIPApi().terminate();
            mSipAgentApi = null;
            sipVMLogger.Debug("SipAgentApi terminated");
        }

        ServerInfoList getServers()
        {
            if (s_servers.Count == 0)
            {
                //These are values for old desktop app SSO.
                //Not using HTTP Tunnel for now
                #if USE_TLS
                            s_servers.addSipRegistrar( "sips.voxox.com", 5061, true );	//TODO: Some discussion of using port 443 here.  Check with Roman if that does not work cleanly.
                            s_servers.addSipProxy    ( "sips.voxox.com", 5061, true );
                #else
                    s_servers.addSipRegistrar("sips.voxox.com", 5060, false);
                    s_servers.addSipProxy("sips.voxox.com", 5060, false);
                #endif
                s_servers.addStun("voxox.com", 5050);
                s_servers.addTurnUdp("voxox.com", 3478, sipUserId, sipPassword);
                s_servers.addTurnTcp("voxox.com", 80, sipUserId, sipPassword);
                s_servers.addTurnTls("voxox.com", 443, sipUserId, sipPassword);
            }

            return s_servers;
        }


        TlsInfo getTlsInfo()
        {
            TlsInfo tlsInfo = new TlsInfo();

            #if USE_TLS
                tlsInfo.Method( SipAgent.TLSMethod.TLS_SSLv23 );
            #else
                tlsInfo.Method = SipAgent.TLSMethod.TLS_SSLv2;
            #endif

            tlsInfo.VerifyCertificate = false;		//Change to true after we have CA file
            tlsInfo.RequireCertificate = false;		//Change to true after we have CA file
            tlsInfo.VerifyDepth = 9;
            //Remaining values are empty string by default.

            return tlsInfo;
        }

        ServerInfo getSipProxyServer()
        {
            return getServers().getFirstSipProxy();
        }

        ServerInfo getSipRegistrarServer()
        {
            return getServers().getFirstSipRegistrar();
        }

        void doAppConfiguration()
        {
            //Called from app initialization.  Uses stored UUID or creates a new one if one does not exist.
            sipVMLogger.Debug("Start App configuration");
            getSIPApi().setSipOptionUuid(sipUserUuid);
            getSIPApi().debugLogging(debugLogging);
        }

        void doSsoConfiguration()
        {
            sipVMLogger.Debug("Start sso configuration");
            ServerInfoList serversIn = getServers();

            getSIPApi().addServers(serversIn);			//Bulk add servers, typically from SSO.

            TlsInfo tlsInfoIn = getTlsInfo();

            getSIPApi().setTlsInfo(tlsInfoIn);

            //Discovery Topology
            getSIPApi().setDiscoveryTopology(topology);
            getSIPApi().enableIceMedia(enableIceMedia);
            getSIPApi().enableIceSecurity(enableIceSecurity);
            getSIPApi().setTopologyEncryption(topologyEncryption);
            getSIPApi().setTopologyTurn(topologyTurn);
            getSIPApi().setSipUdp(sipUdp);
            getSIPApi().setSipTcp(sipTcp);
            getSIPApi().setSipTls(sipTls);
            getSIPApi().setKeepAlive(sipKeepAlive);
            getSIPApi().setUseRport(sipUseRport);
            getSIPApi().setRegistrationRefreshInterval(registrationRefreshInterval);
            getSIPApi().setFailureInterval(failureInterval);
            getSIPApi().setOptionsKeepAliveInterval(optionKeepAlive);
            getSIPApi().setMinSessionTime(minimumSessionTime);
            getSIPApi().setSupportSessionTimer(supportSessionTimer);
            getSIPApi().setInitiateSessionTimer(initiateSessionTimer);
            getSIPApi().setTagProlog(tagProlog);
            getSIPApi().setSipOptionRegisterTimeout(registerTimeout);
            getSIPApi().setSipOptionPublishTimeout(publishTimeout);
            getSIPApi().setSipOptionUseOptionsRequest(useOptionsRequest);
            getSIPApi().setSipOptionP2pPresence(optionsP2PPresence);
            getSIPApi().setSipOptionUseTypingState(optionsUseTypingState);
            getSIPApi().setSipOptionChatWithoutPresence(chatWithoutPresence);
        }

        public void doRegistration()
        {
            sipVMLogger.Debug("Registring to server");
            ServerInfo proxyServer = getSipProxyServer();
            proxyServer.Address = "69.26.183.234";
            ServerInfo registrarServer = getSipRegistrarServer();
            registrarServer.Address = "69.26.183.234";
            //This registers with SIP server.
            lineId = getSIPApi().addVirtualLine(displayName, userName, voxoxDid, password, realm, proxyServer, registrarServer);
            sipVMLogger.Debug("make a call initiated with lineid : " + lineId);
            System.Threading.Thread.Sleep(2000);	//Wait for SIP stack events, if any.

            if (lineId >= 0)
            {
                Int32 result = getSIPApi().registerVirtualLine(lineId);
            }
        }

        public void initialization()
        {
            sipVMLogger.Debug("Initializing SipAgentAPI");
            SipAgentApi api = getSIPApi();
            api.init();
            Boolean isInitialized = api.isInitialized();
        }
        public void terminate()
        {
            if(getSIPApi().isInitialized())
            {
                getSIPApi().terminate();
                sipVMLogger.Debug("SipAgentAPI terminated");
            }
        }

        public void basicConfiguration()
        {
            sipVMLogger.Debug("Basic setup for SipAgentApi");
		    initSIPApi();
		    doAppConfiguration();
		    doSsoConfiguration();
		    doPhoneLineConfiguration();
            doRegistration();

            

            //EventHandler test1 = new EventHandler("Ganesh");
            //SipEventHandler sipDel1 = (SipEventHandler)System.Delegate.CreateDelegate(typeof(SipEventHandler), test1, "HandleSipEvent");

            //String number = "19148008445";
            //Boolean useVideo = false;	//Not supported
            //Int32 callId = getApi().makeCall(mLineId, number, useVideo);
            //System.Threading.Thread.Sleep(50000);		//Testing.  Wait for remote end to answer.  Looking for hits in AppCallback object.
            //terminateApi();	//Move this when testing items below.
        }

        void addSipEventHandlers()
        {
            EventHandler test1 = new EventHandler("Ganesh");
            EventHandler test2 = new EventHandler("KVSH");

            SipEventHandler sipDel1 = (SipEventHandler)System.Delegate.CreateDelegate(typeof(SipEventHandler), test1, "HandleSipEvent");
            SipEventHandler sipDel2 = (SipEventHandler)System.Delegate.CreateDelegate(typeof(SipEventHandler), test2, "HandleSipEvent");

            getSIPApi().addSipEventListener(sipDel1);     //Test with multiple listeners to demonstrate it works.
            getSIPApi().addSipEventListener(sipDel2);
        }

        
        void doPhoneLineConfiguration()
        {
            sipVMLogger.Debug("Phone line configuration");
            getSIPApi().enableAEC(aec);
            getSIPApi().enableAGC(agc);
            getSIPApi().enableNoiseSuppression(noiseSuppression);
            getSIPApi().setCallsEncryption(mCallEncryption);
        }



        public Int32 makeACall(String number, bool useVideo)
        {
            
            callId = getSIPApi().makeCall(lineId, number, useVideo);
            sipVMLogger.Debug("make a call initiated with callid "+ callId);
            return callId;
        }

        public void sendRingingNotification(Int32 callId, Boolean enableVideo)
        {
            sipVMLogger.Debug("send ringing notification initiated");
            getSIPApi().sendRingingNotification(callId, enableVideo);
        }
        
        public void acceptCall(Int32 callId, Boolean enableVideo)
        {
            MessageBox.Show("acceptCall");
            sipVMLogger.Debug("accept a call from call id "+ callId);
            getSIPApi().acceptCall(callId,enableVideo);
        }

        public Int32 getLineId()
        {
            return lineId;
        }

        public void rejectCall(Int32 callId)
        {
            getSIPApi().rejectCall(callId);
        }

        public void closeCall(Int32 callId)
        {
            getSIPApi().closeCall(callId);
        }

        public void holdCall(Int32 callId)
        {
            getSIPApi().holdCall(callId);
        }

        public void resumeCall(Int32 callId)
        {
            getSIPApi().resumeCall(callId);
        }

        public void blindTransfer(Int32 callId, String sipAddress)
        {
            getSIPApi().blindTransfer(callId, sipAddress);
        }

        public void playDtmf(Int32 callId, char dtmf)
        {
            getSIPApi().playDtmf(callId, dtmf);
        }

        public void playSoundFile(Int32 callId, String soundFile)
        {
            getSIPApi().playSoundFile(callId, soundFile);
        }

        public String getAudioCodecUsed(Int32 callId)
        {
            return getSIPApi().getAudioCodecUsed(callId);
        }
        
        public Boolean isCallEncrypted(Int32 callId)
        {
            return getSIPApi().isCallEncrypted(callId);
        }
        
    }   
}
