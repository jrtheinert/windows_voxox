﻿using Desktop.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telcentris.Voxox.ManagedSipAgentApi;

namespace Desktop.Model.Sip
{
    class EventHandler
    {
        private static VXLogger sipeventLogger = VXLoggingManager.INSTANCE.GetLogger("EventHandler");
        public EventHandler(String name)
        {
            mName = name;
        }

        public void HandleSipEvent(Object sender, SipEventArgs e)		//Same signature as SipEventHandler defined in ManagedSipAgentApi.h
        {
            Console.WriteLine("In HandleSipEvent() for instance name: " + mName);
            Console.WriteLine("Event : " + e.Data.GetType());
            if (e.Data.isConnectEvent())
            {
                // TODO need to handle the front end based on the response.
            }
            else if (e.Data.isDisconnectEvent())
            {
                // TODO need to handle the disconnect event 
            }
            else if (e.Data.isLineStateChangeEvent())
            {
                
            }
            else if (e.Data.isCallStateChangeEvent())
            {
                // TODO need to handle the front end based on the line state change 
                // this event will be fired once user initiates a call.
                //Console.WriteLine("  SipEvent: Call State Change");
            }
            else if (e.Data.isLogEvent())
            {
                // TODO log the call details 
            }
            else
            {
                // TODO To handle the events that are not listed.                 
            }
        }

        public String getName()
        {
            return mName;
        }
        private String mName;
    }
}
