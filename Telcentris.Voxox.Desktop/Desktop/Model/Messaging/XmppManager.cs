﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Utils;										//Logger

using ManagedDataTypes;										//Message, VxTimestamp

using Telcentris.Voxox.ManagedXmppApi;						//XmppApi
using Telcentris.Voxox.ManagedXmppApi.ManagedXmppDataTypes;	//ChatMsg, PresenceUpdate, XmppParameters

using System;
using System.Diagnostics;									//Debug()

//-----------------------------------------------------------------------------
//NOTE: We have been getting crash reports (CRs) indicating that mApi is null, which is very strange.
//	This is a singleton, and getInstance() *should* be initializing the mApi memvar.
//	However, since it *appears* it is not, I have reworked the code to call initApi() before
//	each API method is called.
//-----------------------------------------------------------------------------
namespace Desktop.Model.Messaging
{

	public class XmppManager
	{
        static private VXLogger    s_Logger   = VXLoggingManager.INSTANCE.GetLogger( "XmppManager" );
		static private XmppManager s_Instance = null;

		private XmppApi			   mApi			 = null;
		private XmppEventHandler   mEventHandler = null;
		private String			   mJid			 = null;
		private Boolean			   mLoggingOff	 = false;

		static public XmppManager getInstance()
		{
			if ( s_Instance == null )
			{
				s_Instance = new XmppManager();
			}

			return s_Instance;
		}

		static private void deleteInstance()
		{
			if ( s_Instance != null )
			{
				s_Instance.release();
				s_Instance = null;
			}
		}

		private XmppManager()
		{
			s_Logger.Debug( "In XmppManager ctor." );	//Indications this is NOT getting called.  Odd.

			initApi();
		}

		~XmppManager()
		{
			release();
		}

		public void release()
		{
			if ( mApi != null )
				mApi.removeEventListener( mEventHandler );

			mEventHandler = null;
			mApi          = null;
		}
		
		private void initApi()
		{ 
			if ( mApi == null )
			{ 
				try
				{ 
					mEventHandler = new XmppEventHandler( OnXmppEvent );

					mApi		  = XmppApi.getInstance();
					mApi.addEventListener( mEventHandler );

					mLoggingOff = false;
				}
				catch( Exception ex )
				{
					s_Logger.Error( "Error in XmppManager ctor:", ex );
				}
			}
		}

		private XmppApi getApi()
		{
			initApi();

			return mApi;
		}

		public void login( String userId, String password )
		{
			XmppParameters xmppParameters = SettingsManager.Instance.getDefaultXmppParameters();
			
			String tempDomain = Util.Jid.getDomain( userId );

			//Use domain, if it is provided on userId.
			if ( tempDomain != null )
			{
				xmppParameters.Domain = tempDomain;
			}

			xmppParameters.UserId		= Util.Jid.getUserId( userId );
			xmppParameters.Password		= password;
			xmppParameters.InitialShow	= PresenceUpdate.ShowType.Online;	//TODO: Always use this default?  If not, we need to get from Settings.
			xmppParameters.Status		= "";								//TODO: Set initial status msg?  If so, should be persisted between sessions.

			mJid = Util.Jid.makeFullJid( xmppParameters.UserId, xmppParameters.Domain, "" );

			getApi().login( xmppParameters );
		}

		public void logoff()
		{
			mJid = null;

			if ( mApi != null )						//Do NOT use getApi() here.
			{ 
				mLoggingOff = true;
				mApi.logoff();
			}
		}

		public Boolean isConnected()
		{
			bool result = false;

			if ( mApi != null )						//Do NOT user getApi() here.
				result = mApi.isConnected();

			return result;
		}

		public Boolean isShutdown()
		{
			bool result = false;

			if ( !isConnected() )				//For easier debugging
			{
				result = (mApi == null);		//Do NOT use getApi() here.
			}

			return result;
		}

        private void OnXmppEvent(object sender, XmppEventArgs e)
        {
			//TODO: Reconnect event (network)?
            if (e.Data.isSignOnEvent())
            {
				//TODO: Maybe notify UI layer so they can somehow inform user CHAT is available?
				bool temp = isConnected();
                s_Logger.Debug("XmppEvent: SignOn" );
				Debug.WriteLine( "XmppEvent: SignOn" );
 				handleConnectionChange( true );
           }
            else if (e.Data.isSignOffEvent())
            {
				mJid = null;
				bool temp = isConnected();
//				release();			//TODO: This removes the delegate, so hold for now.
                s_Logger.Debug("XmppEvent: SignOff" );
				Debug.WriteLine( "XmppEvent: SignOff" );
				handleConnectionChange( false );
            }
            else if (e.Data.isDisconnectEvent())
            {
				bool temp = isConnected();
                s_Logger.Debug("XmppEvent: Disconnect" );
				Debug.WriteLine( "XmppEvent: Disconnect" );
				handleConnectionChange( false );
            }
            else if (e.Data.isPresenceChangeEvent())
            {
				//Do nothing since we do not care about presence
				//TODO: Verify if we get Avatar change info here.  
				//	If so, we need update Avatar, but I think we will do that via separate event for clarity.
            }
            else if (e.Data.isIncomingMsgEvent())
            {
                Debug.WriteLine( e.Data.ChatMsg.toString() );
				handleIncomingMsg( e.Data.ChatMsg );
            }
            else if (e.Data.isLogEvent())
            {
                s_Logger.Debug("XmppEvent: Log - " + e.Data.LogEntry.Message);
				Debug.WriteLine( "XmppEvent: Log - " + e.Data.LogEntry.Message );
            }
            else
            {
                s_Logger.Error("Unknown XMPP Event " + e.Data.LogEntry.Message);
                Debug.WriteLine("Unknown XMPP Event " + e.Data.LogEntry.Message);
            }
        }
		
		private void handleConnectionChange( bool connected )
		{
			if ( ! connected )
			{ 
				if ( mLoggingOff )
				{
					release();
				}
			}

			GalaSoft.MvvmLight.Messaging.Messenger.Default.Send<ConnectionMessage>( ConnectionMessage.makeXmpp( connected ) );
		}

		//Handles typing indicator and actual message, so...
		//	Send message to MessageManager
		//	Send typing indicator to who ever is listening (via a MVVMLight message?)
		//  Properly handle Carbons messages
		//	Handle read/delivery receipts.
		private void handleIncomingMsg( ChatMsg chatMsg )
		{
            try
            {
                Boolean handleChatState = false;
                Boolean handleMsg = false;

                //Determine what needs to be done.
                if (chatMsg.isMsg())
                {
                    handleMsg = true;
                    handleChatState = true;		//Message implies a chatState.
                }
                else if (chatMsg.isChatState())
                {
                    handleChatState = true;
                }
                else if (chatMsg.isDeliveredReceipt())
                {
                    MessageManager.getInstance().markMessageDelivered(chatMsg.ReceiptMsgId, true);
                }
                else if (chatMsg.isReadReceipt())
                {
                    MessageManager.getInstance().markMessageRead(chatMsg.ReceiptMsgId, true);
                }
                else if (chatMsg.isCarbon())
                {
                    handleMsg = !String.IsNullOrEmpty(chatMsg.Body);
                    handleChatState = true;
                }

                //Now do what needs to be done.
                if (handleMsg)
                {
                    //Carbons may send us a message from either direction.
                    String fromJid = Util.Jid.getCleanJid(chatMsg.FromJid);
                    Message.MsgDirection dir = (fromJid == mJid) ? Message.MsgDirection.OUTBOUND : Message.MsgDirection.INBOUND;
                    Message.MsgStatus status = (fromJid == mJid) ? Message.MsgStatus.SENT : Message.MsgStatus.NEW;

                    Message message = Message.makeChatMessage(dir, status, chatMsg.FromJid, chatMsg.ToJid, chatMsg.FromResource, chatMsg.MyResource,
                                                               chatMsg.MsgId, chatMsg.Body, chatMsg.ReceiptMsgId, (int)chatMsg.Type, VxTimestamp.GetNowTimestamp() );

                    message.ServerTimestamp = new VxTimestamp( VxTimestamp.GetNowTimestamp() );		//All times are UTC.

                    if (MessageManager.getInstance() != null)
                        MessageManager.getInstance().chatMessageReceived(message);
                }

                if (handleChatState)
                {
                    //We always have ChatState (typing indicator)
                    Boolean composing = isComposing(chatMsg.ChatState);
                    GalaSoft.MvvmLight.Messaging.Messenger.Default.Send<XmppTypingNotificationMessage>(new XmppTypingNotificationMessage { Jid = chatMsg.FromJid, IsComposing = composing });
                }
            }
			catch(Exception ex)
            {
                s_Logger.Error("Error in handlePushNotification", ex);
            }
		}

		private Boolean isComposing( String chatState )
		{
			Boolean isComposing = false;
			
			if		( chatState == "composing" )
				isComposing = true;
			else if ( chatState == "paused" )
				isComposing = true;
			else if ( chatState == "active" )
				isComposing = false;
			else 
				isComposing = false;	//Not sure about this.

			return isComposing;
		}

        public void sendChatState(string toJid, bool composing)
        {
            getApi().sendChatState(toJid, composing);
        }

		public bool sendChat( string toJid, string msg, string msgId )
		{
			return getApi().sendChat( toJid, msg, msgId );
		}

		public void sendReadReceipt( string toJid, string msgId )
		{
			getApi().sendReadReceipt( toJid, msgId );
		}

		//This will be a UUID
		static public String generateMsgId()
		{
			String result = "Voxox:" + Guid.NewGuid().ToString();	//TODO: Configure 'Voxox'?

			return result;
		}

	}	// class XmppManager

}	//namespace Desktop.Model.Messaging 
