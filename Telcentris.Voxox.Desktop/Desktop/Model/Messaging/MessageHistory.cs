﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Util;			//TimeUtils
using Desktop.Utils;		//Logger

using ManagedDataTypes;		//Message, MessageList, VxTimestamp

using Vxapi23;				//RestfulApiManager
using Vxapi23.Model;		//MessageHistoryResponse, MessageHistoryData

using System;
using System.Collections.Generic;

namespace Desktop.Model.Messaging
{
	class MessageHistory
	{
		static public MessageList getMessages( Int64 timestamp, String userKey, String userCompanyUserId, int numberOfMessagesToRetrieve )
		{
			MessageList msgList = new MessageList();
            MessageList deletedMsgsList = new MessageList();

            String serverFormatTimestamp = VxTimestamp.GetFormattedTimestampForREST(timestamp);
		    VXLogger logger				 = VXLoggingManager.INSTANCE.GetLogger("MessageHistory");

			try
			{
				//Some default values.
				String contactFilter  = null;
				String contactAddress = null;
				int	   offset         = 0;

				List<MessageHistoryResponse> apiMsgList = 
						RestfulAPIManager.INSTANCE.GetMessageHistory( userKey, userCompanyUserId, contactFilter, contactAddress, serverFormatTimestamp, numberOfMessagesToRetrieve, offset );

                var db = SessionManager.Instance.UserDataStore;

                if (db == null)
                    return msgList;

				if ( apiMsgList != null )
				{
					foreach ( MessageHistoryResponse msgResponse in apiMsgList )
					{
						List<MessageHistoryData> msgDataList = msgResponse.Response;

						if(msgDataList != null)
						{
							String userDid = SessionManager.Instance.User.Did;

							int deletedChatCount  = 0;
							int deletedOtherCount = 0;

							foreach ( MessageHistoryData msgData in msgDataList )
							{
								//Some data conversions:
								//	- Server message types do NOT match client message types
								//	- Server Inbound/Outbound values do NOT match client values
								//	- Server message status do NOT match client values
								//	- Server is currently returning Status == 0, for RC and SMS, at least for new messages.  Need to implement updateMessageStatus()

								Message.MsgType		 msgType		 = Message.typeFromServerType( msgData.messageType );
								Message.MsgDirection msgDir			 = (msgData.inbound == "1" ? Message.MsgDirection.INBOUND : Message.MsgDirection.OUTBOUND );
								Message.MsgDirection msgDirOrig		 = msgDir;

								//We do this here, because messageStatus is also affected by bad server data.
                                // Server seems to return null for inbound property for Faxes even if the fax is an inbound fax.
								// Server seems to return null for inbound property for Voicemail.
								// Server seems to return Outbound/Inbound for RecordedCalls.
                                if (msgData.inbound == null && msgData.to == userDid)
                                {
                                    msgDir = Message.MsgDirection.INBOUND;
                                }
								else if ( msgType == Message.MsgType.VOICEMAIL )
								{
									//By definition, all VMs are inbound messages.
									msgDir = Message.MsgDirection.INBOUND;	
								}
								else if ( msgType == Message.MsgType.RECORDED_CALL )
								{
									//By definition, all RCs are inbound messages.
									//	But we actually need the real direction for proper UI because for SOME 
									//	reason, UX team cares about displaying recording as on an inbound/outbound call.
//									msgDir = Message.MsgDirection.INBOUND;	
								}

								Message.MsgStatus    msgStatus		 = Message.statusFromServerStatusEx( msgData.status, msgDir );

								//We should NOT be getting deleted/archived messages from getMessageHistory()
								//	Just count them, drop them and log the count later.
								if ( msgStatus == Message.MsgStatus.DELETED )
								{ 
									if ( msgType == Message.MsgType.CHAT )
										deletedChatCount++;
									else
										deletedOtherCount++;

                                    // Add deleted messages to list to delete from db
                                    Message message = new Message();

                                    message.Type			= msgType;
                                    message.Direction		= msgDir;
                                    message.Status			= msgStatus;
                                    message.LocalTimestamp	= 0;	// If we need LocalTimestamp, we must look it up.  Not needed for now.;
                                    message.ServerTimestamp = new VxTimestamp( msgData.created );
                                    message.MsgId			= msgData.messageId;
                                    message.Duration		= Convert.ToInt32(msgData.duration);

                                    deletedMsgsList.Add(message);

									continue;		//This SKIPS adding to DB.
								}

								Message msg = new Message();

								msg.Type			= msgType;
								msg.Direction		= msgDir;
								msg.Status			= msgStatus;
								msg.LocalTimestamp	= VxTimestamp.GetNowTimestamp();
								msg.ServerTimestamp = new VxTimestamp( msgData.created );
								msg.MsgId			= msgData.messageId;
								msg.Duration		= Convert.ToInt32( msgData.duration );
								msg.ClientUid		= msgData.clientUid;

								if ( msg.isSms() )
								{
									msg.FromDid = msgData.from;
									msg.ToDid	= msgData.to;
									msg.Body	= msgData.body;
								}
								else if ( msg.isChat() )	//Could be IM or GroupMessage
								{
									// CHAT message. if the message id is empty, it is a chat message. But if it is not, it is either a chat message or a group message. in the latter case
									//	We need to verify that it is not a group message by checking the message body.
									//	Sometimes a chat message is being received with a proper message id and sometimes it is empty. why?
	//								if ( String.IsNullOrEmpty( msg.MsgId ) /*|| getMessage.isGroupMesageBody( msgData.body)*/ )	//TODO-GM

									//For now, let's just address Chat since we are not doing GM
									msg.FromJid = msgData.from;
									msg.ToJid	= msgData.to;
									msg.Body	= msgData.body;

									if ( msg.isInbound() )
									{
										String did = db.getDidByJid( msgData.from );

										msg.ToDid	= userDid;
										msg.FromDid = did;
									}
									else 
									{ 
										String did = db.getDidByJid( msgData.to );

										msg.ToDid	= did;
										msg.FromDid = userDid;
									}
								}
								else if ( msg.isFax() )
								{
									msg.Body	  = msgData.body;
									msg.FromDid   = msgData.from;
									msg.ToDid     = msgData.to;
									msg.FileLocal = msgData.filename;
								}
								else if ( msg.isVoicemail() )
								{
									msg.FromDid   = msgData.from;
									msg.ToDid	  = msgData.to;
									msg.Body	  = msgData.body;
									msg.FileLocal = msgData.filename;
								}
                                else if (msg.isRecordedCall())
                                {
									msg.FromDid	  = msgData.from;
									msg.ToDid	  = msgData.to;
                                    msg.Body	  = msgData.body;
                                    msg.FileLocal = msgData.filename;
                                }
								//We currently have some (one) invalid condition we need to check for.
								//TODO: Monitor this.
								Boolean isValid = true;

								if ( msg.isSms() )
								{
									isValid = !String.IsNullOrEmpty( msg.ToDid );

									if (!isValid)
										logger.Warn( "Discarding invalid SMS message.");
								}

								if ( isValid )
								{ 
									msgList.Add(msg);
								}

								System.Threading.Thread.Sleep( 1 );	//So we can get unique local timestamps.	//TODO: Drop use of LocalTimestamp as unique ID?
							}	//Foreach

							//Log deleted message count.
							if ( deletedChatCount + deletedOtherCount > 0 )
							{
                                // Deleting messages from local DB
                                if (deletedMsgsList != null)
                                    MessageManager.getInstance().deleteMessagesBasedOnHistory(deletedMsgsList);

								logger.Warn( String.Format( "Received {0} messages from getMessageHistory().", deletedChatCount + deletedOtherCount ) );
							}
						}
					}
				}
			}

			catch (Exception ex)
			{
				logger.Error("Error while synchronizing", ex);
			}

			return msgList;
		}
	}
}
