﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Utils;		//Logger

using System;

namespace Desktop.Model.Messaging
{
	public class PushedInfo
	{
		// XMPP (JPush) push notification type
		static private readonly String PUSHED_INFO_KEY		= "pushedInfo";

		//Message related
		static private readonly String TYPE_MESSAGE_HISTORY	= "messagehistory";
		static private readonly String TYPE_SMS				= "sms";
		static private readonly String TYPE_FAX				= "fax";
		static private readonly String TYPE_VOICEMAIL		= "voicemail";
		static private readonly String TYPE_RECORDED_CALL	= "callrecord";
//		static private readonly String TYPE_GROUP_MESSAGE	= "voxoxgroupmessage";		//FUTURE

		//Call related
		static private readonly String TYPE_CALL			= "call";

		//Contact related
		static private readonly String TYPE_CONTACT_UPDATE	= "contactupdate";

		//User related
		static private readonly String TYPE_ACCOUNT_UPDATE	= "updateuser";			//Simliar/Same as old dataSummary
		static private readonly String TYPE_SETTINGS_UPDATE = "settingsupdate";
		static private readonly String TYPE_DATA_SUMMARY	= "datasummary";		//Still seeing this, so adding back in.
		static private readonly String TYPE_PASSWORD_CHANGE	= "passwordchange";		//NOT IMPLEMENTED, as far as I can tell.

		//Misc.
		static private readonly String TYPE_STOP_RING		= "stopring";			//Used for 'push calling' on Mobile.  We do not need this, but want to properly handle if pushed to desktop.


		public PushedInfo()
		{
		}

		public string fullBody					{ get; set; }		//We save this in order to detect and ignore duplicate PNs due to Carbons

		public string type						{ get; set; }		//sms, fax, vm, call, datasummary, voxoxgroupmessage
		public string dataType					{ get; set; }		//ASCII
		public string enc						{ get; set; }		//utf8

		//We are ignoring most/all content for now.  We may use smaller content payloads going forward, but none for now.

		////Contents - SMS/Fax/VM (Since we now use JPush for notification only, these are likely not needed)
		public string id				{ get; set; }		//854196
		//public string from				{ get; set; }		//+17602775395	//SMS
		//public string body				{ get; set; }		//<text>

		//Message related
		public Boolean isMessageHistory()					{  return compareType( TYPE_MESSAGE_HISTORY );	}
		public Boolean isSms()								{  return compareType( TYPE_SMS				);	}
		public Boolean isFax()								{  return compareType( TYPE_FAX				);	}
		public Boolean isVoiceMail()						{  return compareType( TYPE_VOICEMAIL		);	}
		public Boolean isRecordedCall()						{  return compareType( TYPE_RECORDED_CALL	);	}
//		public Boolean isGroupMessage()						{  return compareType( TYPE_GROUP_MESSAGE	);	}		//FUTURE
		public Boolean isMessage()							{  return isSms() || isFax() || isVoiceMail() || isRecordedCall(); }	//TODO-GM:	Add GM?

		//Call related
		public Boolean isCall()								{  return compareType( TYPE_CALL			);	}

		//Contact related
		public Boolean isContactUpdate()					{  return compareType( TYPE_CONTACT_UPDATE	);	}

		//User related
		public Boolean isAccountUpdate()					{  return compareType( TYPE_ACCOUNT_UPDATE	) || compareType( TYPE_DATA_SUMMARY );	}
		public Boolean isSettingsUpdate()					{  return compareType( TYPE_SETTINGS_UPDATE );	}
		public Boolean isPasswordChange()					{  return compareType( TYPE_PASSWORD_CHANGE );	}

		//Misc.
		public Boolean isStopRing()							{  return compareType( TYPE_STOP_RING		);	}

		//We have had issues where the case of the 'type' value is incorrectly cased.  Make the comparison case-insensitive.
		//	Note that all our 'private readonly' strings are completely lowercase.
		private Boolean compareType( String definedType )
		{
			return type.ToLower() == definedType;
		}
	 

		static public PushedInfo fromXml( String xmlIn )
		{
			VXLogger mLogger   = VXLoggingManager.INSTANCE.GetLogger("PushedInfo");

			//2015.05.28: We are getting invalid XML from server for <datasummary>, so let's remove it.	//TODO: At some time remove this.
			//	This is in <content> which we really do not use, so if this continues to be an issue, just parse out the <content></content> substring.
			String xml = xmlIn.Replace( "<0>Array</0>", "" );

			PushedInfo result = new PushedInfo();
		
			if ( xml.Contains( PUSHED_INFO_KEY ) )
			{
				try 
				{ 
					RestSharp.RestResponse response = new RestSharp.RestResponse();
					response.Content = xml;

					var deserializer = new RestSharp.Deserializers.XmlDeserializer();

					result = deserializer.Deserialize<PushedInfo>(response);

					result.fullBody = xml;
				}

				catch ( Exception e )
				{
					mLogger.Error( e.ToString() );
				}
			}

			return result;
		}
	}
}
