﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

//TODO: Move to a common internal message class.

using System;

namespace Desktop.Model.Messaging
{
    public class XmppTypingNotificationMessage
    {
        public string  Jid		   { get; set; }
		public Boolean IsComposing { get; set; }
    }
}
