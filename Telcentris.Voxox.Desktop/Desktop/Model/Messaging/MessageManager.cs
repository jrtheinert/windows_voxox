﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Util;						//TimeUtils
using Desktop.Utils;					//Logger
using Desktop.ViewModels;				//MessageListenedToMessage

using ManagedDataTypes;					//Contact, Message, MessageList, VxTimestamp

using GalaSoft.MvvmLight.Messaging;		//MessageListenedToMessage

using Vxapi23;							//RestfulApiManager
using Vxapi23.Model;					//SMSResponse, DeletedMessagesToServer, UpdateMessage

using System;
using System.IO;						//File
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;


namespace Desktop.Model.Messaging
{

//TODO List as of 2014.10.24
//	- TODO				 3
//	- TODO-RICHDATA		10
//	- TODO-TRANSLATE	 7
//	- TODO-API			 5
//	- TODO-XMPP			 3
//	- TODO-GM			 4
public class MessageManager
{
	private static MessageManager mInstance = null;
    private static VXLogger		  mLogger    = VXLoggingManager.INSTANCE.GetLogger("MessageManager");

	static public MessageManager getInstance()
	{
		if ( mInstance == null )
		{
			mInstance = new MessageManager();
		}

		return mInstance;
	}

	private MessageManager()
	{
		Messenger.Default.Register<MessageListenedToMessage>(this, OnMessageListenedTo);
	}

	private ManagedDbManager.ManagedDbManager getUserDbm()
	{
		return SessionManager.Instance.UserDataStore;
	}

	//To handle Voicemail messages that have been listened to and mark them as READ.
	private void OnMessageListenedTo( MessageListenedToMessage msgIn )
	{
		//This SHOULD be Voicemail, but let's keep it open.
		Message msg = getMessage( msgIn.Timestamp );

		if ( msg != null )
		{
			markMessageRead( msg.LocalTimestamp, true );
		}
	}


	/**
	 * Description: sending XMPP chat or SMS message.
	 * SMS: 
	 * 	- Save the message in the database. 
	 *	- Make API call to send SMS which will return the server Timestamp, msgId, etc.
	 *	- Update message with data returned from API call and mark it SENT (on success)
	 * Chat:
	 *	- Add chat to DB
	 *	- Use XmppManager to send message
	 *	- Update message as SENT upon completion
	 * */
	public void sendMessage( Message message ) 
	{
        if (getUserDbm() == null)
            return;

		// a new message -MUST- be saved in the local database before taking any other action
		if ( message.isStatusNew() ) 
		{
			getUserDbm().addMessage( message, true );
		}

		// all messages other than CHAT are being sent via HTTP. On low network connections the XMPP may not be connected or takes more time
		// to connect, but HTTP might work.
		// if we have the network available (WiFi or Mobile) lets try and send the message. In the worse case we will have a failed message status and the user will
		// have the option to re-send it.

		Boolean isNetworkAvailable = NetworkStatus.Instance.IsAvailable;

		// do we have network available?
		if ( isNetworkAvailable ) 
		{
			// verify that we have the proper state to send a message
			if ( message.isOkToSend() ) 
			{
				// check for rich data
				RichData richData = message.RichData;
				if ( null!=richData && (richData.isTypeMedia()) )
				{
					getUserDbm().updateMessage(message.LocalTimestamp, Message.MsgStatus.UPLOADING);
					uploadMediaFile(message, richData);
				}
                else if (null != richData && richData.isTypePdf())
                {
                    getUserDbm().updateMessage(message.LocalTimestamp, Message.MsgStatus.UPLOADING);
                    uploadFaxFile(message, richData);
                    // do we need to translate the message, if no, just send it as a simple text message
                } 
				else // else if ( ! translateMessageIfRequired(context, message) )		//TODO-TRANSLATE
				{		
					doSendMessage( message );
				}
			}
		} 
		else 
		{
			// Network is not available, update the message state to pending. once we gain connection we will try to re-send the message.
			getUserDbm().updateMessage( message.LocalTimestamp, Message.MsgStatus.PENDING );
		}
	}

	/**
	 * 	 Description: The message was already saved in the database and
	 * 	 now we get the second request to send it with the rich data ready (media uploaded to the REST server and we have the URL)
	 * */
	public void sendMessageRichDataReady(Message message) 
	{
		if ( message.isGroupMessage() ) 
		{
			String body = message.Body;
//			getUserDbm().updateGroupMessageRichData( message.LocalTimestamp, message.RichData, true, body );	//TODO-RICHDATA
		} 
		else 
		{
//			getUserDbm().updateMessageRichData( message.LocalTimestamp, message.RichData, true);	//TODO-RICHDATA
		}

		doSendMessage( message );
	}

	/**
	 * 	 Description: The message was already saved in the database and
	 * 	 now we get the second request to send it with text translation
	 * */
	public void sendMessageTranslatedReady( Message message ) 
	{
        if (getUserDbm() == null)
            return;

		getUserDbm().updateTranslatedMessage( message.LocalTimestamp, message);

		doSendMessage( message );
	}

	/**
	 * 	 Description: Single location to determine HOW to send a message and then send the message.
	 *
	 * */
	private void doSendMessage( Message message ) 
	{

//		VoxoxMixpanel.instance().trackMessageSent( message );

		//JRT - We are getting CRs here.  I do NOT see how 'message' can be null, but add a check and log it.
		//	I will still let the crash happen because I want to understand the cause.
		if ( message == null )
		{
			mLogger.Debug( "message is NULL." );
		}

		if (message.isSms()) 
		{
			sendSMSRequest( message );
		} 
		else if (message.isGroupMessage()) 
		{
			sendGroupMessage( message.LocalTimestamp );
		} 
		else if (message.isChat()) 
		{
			sendChatRequest( message.LocalTimestamp, message.ToJid, message.Body, message.MsgId );
		} 
		else 
		{
//			if (BuildConstants.ENABLE_LOGGING) Log.w(TAG, "sendMessage() : Wrong message type, cannot send message!");
		}
	}

	/**
	 * Description: Resend message that failed, most likely due to network failure
	 * */
	public void resendMessage( Int64 timestamp )
	{
		Message message = getMessage( timestamp );

		if ( message != null && message.canBeResent() )
		{
			Int64 newTimestamp = VxTimestamp.GetNowTimestamp();

			updateMessage( timestamp, newTimestamp, message.Status );

			message = getMessage( timestamp );

			doSendMessage( message );
		}
	}

	/**
	 * Description: Replace the addressed Message in the database,
	 * */
    public void updateMessageRichData(Int64 timeStamp, RichData richData, Boolean broadcast)	//TODO-RICHDATA
    {
        if (getUserDbm() == null)
            return;

        getUserDbm().updateMessageRichData(timeStamp, richData, broadcast);
    }

//	public void updateGroupMessageRichData( Int64 timeStamp, RichData richData, Boolean broadcast, String origBody )	//TODO-RICHDATA
//	{
//		getUserDbm().updateGroupMessageRichData( timeStamp, richData, broadcast, origBody );
//	}

	public void updateMessage( Int64 timeStamp, Message.MsgStatus status ) 
	{
        if (getUserDbm() == null)
            return;

		getUserDbm().updateMessage( timeStamp, status );
	}

	public void updateMessage( Int64 timeStamp, Message.MsgStatus status, String messageId ) 
	{
        if (getUserDbm() == null)
            return;

		getUserDbm().updateMessage( timeStamp, status, messageId );
	}

	public void updateMessage( Int64 timeStamp, Int64 serverTimeStamp, Message.MsgStatus status) 
	{
        if (getUserDbm() == null)
            return;

		getUserDbm().updateMessage( timeStamp, serverTimeStamp, status );
	}

	public void updateMessage( Int64 timeStamp, Int64 serverTimeStamp, Message.MsgStatus status, String messageId ) 
	{
        if (getUserDbm() == null)
            return;

		getUserDbm().updateMessage( timeStamp, serverTimeStamp, status, messageId );
	}

	public void updateTranslatedMessage( Int64 timeStamp, Message message ) 
	{
        if (getUserDbm() == null)
            return;

		getUserDbm().updateTranslatedMessage( timeStamp, message );
	}

	//Should be for chat msg only
	public void markMessageDelivered( String msgId, Boolean broadcast ) 
	{
        if (getUserDbm() == null)
            return;

		Int64 timestamp = getUserDbm().getLocalTimestampForMsgId( msgId );

		markMessageDelivered( timestamp, broadcast );
	}

	public void markMessageDelivered( Int64 timeStamp, Boolean broadcast ) 
	{
        if (getUserDbm() == null)
            return;

		Message message = getMessage( timeStamp );

		if ( (message != null) && ( !message.isStatusRead() && !message.isStatusDeleted() ) )
		{
			getUserDbm().markMessageDelivered( timeStamp, broadcast );
		}
	}

	public int markMessagesReadWhenViewed( UmwMessageList msgList ) 
	{
        if (getUserDbm() == null)
            return 0;

		int markedCount = 0;

        foreach (var msg in msgList)
        {
			bool markMsgRead = false;

            if (msg.isChat() || msg.isSms())
            {
				//Always mark these a READ once Message Thread is displayed
				markMsgRead = true;
			}
			else if ( msg.isFax() )
			{
				//No action
			}
			else if ( msg.isVoicemail() )
			{
				//If VM has transcription, then mark as READ.  Otherwise, user must actually listen to VM
				if ( !String.IsNullOrEmpty( msg.Body ) )
				{
					markMsgRead = true;
				}
			}
			else if ( msg.isRecordedCall() )
			{
				//No action
			}

			if ( markMsgRead )
			{
				if ( msg.canBeMarkedRead() )
				{ 
					//TODO: implement BATCH method for better server efficiency
					markMessageRead( msg.LocalTimestamp, false );

					markedCount++;
				}
			}

		}	//foreach


		if ( markedCount > 0 )
		{
			getUserDbm().broadcastMessagesDatasetChanged();
		}

		return markedCount;
	}

	//This is local only, when we get 'read' receipt.
	public void markMessageRead( String msgId, Boolean broadcast ) 
	{
        if (getUserDbm() == null)
            return;

		Int64 timestamp = getUserDbm().getLocalTimestampForMsgId( msgId );

		markMessageRead( timestamp, broadcast );
	}

	public void markMessageRead( Int64 timeStamp, bool broadcast )  
	{
        if (getUserDbm() == null)
            return;

		Message message = getMessage( timeStamp );

		if ( (message != null) && message.canBeMarkedRead() ) 
		{
			//Local
			getUserDbm().markMessageRead( timeStamp, broadcast );

		   //Server
		   //TODO: Should we update server first and then update client based on the result?
		   //TODO: We should wrap this in canBeMarkedRead() above, but client and server are currently out of sync.
		   markMessageReadOnServer(message);
		}
	}

	//TODO: implement BATCH method for better server efficiency
	private void markMessageReadOnServer( Message message ) 
	{
		Message.ServerMsgStatus svrMsgStatus = Message.statusToServerStatus( Message.MsgStatus.READ );
		Message.ServerMsgType   svrMsgType   = Message.typeToServerType    ( message.Type   );
		bool result = false;
       
        if ( message.isChat() )
		{
			//For XMPP messages we fire 'read receipt'
			XmppManager.getInstance().sendReadReceipt( message.FromJid, message.MsgId );
		}
		else
		{ 
			//For other message types, we fire API call
			Task.Run(() =>
			{
                if (SessionManager.Instance.User == null)
                    return;

				  result =  RestfulAPIManager.INSTANCE.UpdateMessageStatus( SessionManager.Instance.User.UserKey, SessionManager.Instance.User.CompanyUserId,
																			(int)svrMsgType, message.MsgId, (int)svrMsgStatus, true );
			 });
		}
	}

	//Determine if we already have message with given MessageId so we do not display it again, as in GroupMessaging.
	public Boolean hasMessageId( String messageId ) 
	{
		return 	getUserDbm().hasMessageId( messageId );
	}

	/** Description:
	 * The method handles received chat messages.
	 * When receiving a chat message we:
	 *	- Determine if it from JPush (ask.voxox.com) and handle it if it is.
	 *	- Verify that we have the sender DID and we add it to the message.
	 * The sender did may be retrieved from the local database (from a Voxox contact) or from the REST server.
	 * As for now, we display only messages with either a valid name (retrieved from the native address book) or a phone number.
	 * A contact JID is -NOT- considered a valid name to display to the user.
	 **/
	public void chatMessageReceived( Message message) 
	{
        if (getUserDbm() == null)
            return;

        if (PushNotificationManager.getInstance() != null)
        {
            if (PushNotificationManager.getInstance().handledNotification(message))
                return;
        }

		//NOTE: Trying to implement something better.  Will remove this code if/when that works.
		//The MsgId from XMPP manager is only unique within that chat conversation.
		//	Since we need a MORE unique MsgId, we append the current timestamp separated by a colon ':".
		//	This will allow us to work with Voxox API calls and strip the appended timestamp as needed for XMPP work.
		//	TODO: Add this logic to UTIL.

//		String tempMsgId = message.MsgId;
//		if ( !tempMsgId.Contains( ":") )	//Not perfect but should work
//		{
//			tempMsgId += ":" + TimeUtils.getLocalTimestamp().ToString();
//			message.MsgId = tempMsgId;
//		}

		String jidTemp = message.getOriginatingJid();						//OK for now, was using ContactManager which Desktop does not have (and only usage of Contactmanager)
		String jid     = Desktop.Util.Jid.getCleanJid( jidTemp );

		// do we have the JID's did?  it may be retrieved as part of populating the local address book with Voxox users or
		// from a previously chat message that its JID was used to retrieve the did from the REST server.
//		String did = ContactsManager.INSTANCE.getContactDidByJid( jid );
		String did = getUserDbm().getDidByJid( jid );		//TODO: ideally we would use ContactManager, when we get one implemented.

		if ( String.IsNullOrEmpty( did ) )
		{
			// retrieve the contact did from the REST server, add the did to the message and call messageReceived().
			Task.Run(() => { handleChatMessageDidIntegrity( message ); });
		} 
		else 
		{
			message.setOriginatingDid( did );
			message.setDestinationDid( SessionManager.Instance.User.Did );	//TODO: normalize.  And this may not be best location for this.
			messageReceived( message );
		}
	}


	public void messageReceived( Message message) 
	{
        if (getUserDbm() == null)
            return;

//		MessageTranslationProperties mtp = getTranslateMessageProperties(message);		//TODO-TRANSLATE

//		VoxoxMixpanel.instance().trackMessageReceived( message );

		// sanity test, do we need to translate the message?
//		if (mtp != null && mtp.getEnabled() && mtp.isDirectionActive(MessageDirection.INBOUND) && message.getBody() != null && isTranslationAvailable(message) ) 
//		{
//			// add to database, do not broadcast data-set change
//			getUserDbm().addMessage(message, false);

//			// get the translation (will broadcast data-set change when done)
//			new MessageTranslatorTask(null, message, mtp.getContactLanguage(), mtp.getUserLanguage()).execute();
//		} 
//		else 
		{
			// add to database and broadcast data-set change
			getUserDbm().addMessage( message, true );

			// check if the message is a rich data and includes a media file (as we may need to download a thumb-nail)
//			downloadMediaThumbnailIfRequired( message, true );			//TODO-RICHDATA

			//If we have a GM announcement, we need to update Group and Member info.
//			if ( message.isGroupMessage() )		//TODO-GM
//			{	
//				GroupMessagingManager.INSTANCE.handleIncomingMessage( message );
//			}
		}
	}

	//TODO: Why doesn't this just call messageReceived() for each message?  Why the different logic? Notably the translation logic.
	public void messagesReceived( MessageList messages ) 
	{
        if (getUserDbm() == null)
            return;

		getUserDbm().addMessages( messages );

		int count      = 0;
		int msgCount   = messages.Count();
		bool broadcast = false;

		foreach ( Message msg in messages )
		{
			count++;
			broadcast = (count == msgCount );	//Broadcast only on last message.

			// check if we need to translate the message
//			translateMessageIfRequired( null, msg );				//TODO-TRANSLATE
			
			// do we have a media message with thumb-nail?
//			downloadMediaThumbnailIfRequired( msg, broadcast );		//TODO-RICHDATA
		}
	}

	public Message getMessage( Int64 timeStamp) 
	{
        if (getUserDbm() == null)
            return new Message();

		return getUserDbm().getMessage( timeStamp );
	}

	public MessageList getMessages( Message.MsgStatus status, Message.MsgDirection dir )
	{
        if (getUserDbm() == null)
            return new MessageList();

		return getUserDbm().getMessages( status, dir );
	}

	public UmwMessageList getMessagesForPhoneNumber( String phoneNumber )
	{
        if (getUserDbm() == null)
            return new UmwMessageList();

		return getUserDbm().getMessagesForPhoneNumber( phoneNumber );
	}

	public UmwMessageList getMessagesForCmGroup( int cmGroup )
	{
        if (getUserDbm() == null)
            return new UmwMessageList();

		return getUserDbm().getMessagesForCmGroup( cmGroup );
	}

	public UmwMessageList getMessagesForUmw( int cmGroup, String phoneNumber )
	{
        if (getUserDbm() == null)
            return new UmwMessageList();

		//CmGroup has priority over phoneNumber, since it handles contacts with more than one phone number.
        if (cmGroup > ManagedDataTypes.Contact.INVALID_CMGROUP )
            return getMessagesForCmGroup(cmGroup);
        else if (phoneNumber != null)
            return getMessagesForPhoneNumber(phoneNumber);
        else
            return new UmwMessageList();
	}

	public UmwMessageList getFaxMessages()
	{
		return getUserDbm().getFaxMessages();
	}

//	public Cursor getMessagesForMessageGroup(final String groupId)	//TODO-GM
//	{	
//		return getUserDbm().getMessagesForGroupId(groupId);
//	}

	private void sendSMSRequest( Message message ) 
	{
		List<SMSResponse> responseList = null;

        Task.Run(() =>
        {
            if (SessionManager.Instance.User == null)
                return;

              responseList =  RestfulAPIManager.INSTANCE.sendSMS
                    (SessionManager.Instance.User.UserKey,
                     SessionManager.Instance.User.Cid,
                     message.ToDid, 
                     message.Body,
                     SessionManager.Instance.User.CompanyUserId,
					 Convert.ToString( message.LocalTimestamp ) );

			  if ( responseList != null )
			  {
				  foreach ( SMSResponse smsResponse in responseList )
				  {
					  if ( smsResponse.pushDataSummary == true )
					  {
						  //NOTE: Currently, SMS does NOT have a DELIVERED status, just SENT.
						  //	This is because our underlying carriers, for the most part, can not provide this info.
						  message.Status = Message.MsgStatus.SENT;
						  message.MsgId  = smsResponse.messageId;
						  updateMessage( message.LocalTimestamp, VxTimestamp.GetTimestamp( smsResponse.timestamp ), Message.MsgStatus.SENT, smsResponse.messageId );
					  }
					  else
					  {
						  // TODO: Call to PAPI to change the status to retry
					  }
				  }
			  }
         });
	}

	private void sendChatRequest( Int64 timestamp, String toJid, String body, String msgId ) 
	{
		//TODO-XMPP: Why no broadcast as there is in SMS and GM?
		if ( XmppManager.getInstance().sendChat( toJid, body, msgId ) )		//TODO: Coupling too tight?
		{ 
			updateMessage( timestamp, Message.MsgStatus.SENT );
		}
		else
		{
			updateMessage( timestamp, Message.MsgStatus.FAILED );
		}
	}

	private void sendGroupMessage( Int64 timestamp ) 
	{
//		Intent broadcastIntent = new Intent(ACTION_SEND_SMS_MESSAGE);
//		broadcastIntent.putExtra(Message.INTENT_MESSAGE_KEY_EXTRA, key);
//		LocalBroadcastManager.getInstance(context).sendBroadcast(broadcastIntent);

		//TODO-GM: Call API to send and them 'broadcast' (who is listening)
	}

    private async void uploadMediaFile(Message message, RichData richData)
    {
        try
        {
            // upload files
            string thumbnailFile = richData.ThumbnailFile;

            if (thumbnailFile == null)
            {
                var user = SessionManager.Instance.User;
                var response = await RestfulAPIManager.INSTANCE.SendFile(user.UserKey, user.CompanyUserId, richData.MediaFile);
                if (response != null && string.IsNullOrWhiteSpace(response.file) == false)
                {
                    richData.MediaUrl = response.file;
                    updateMessageRichData(message.LocalTimestamp, richData, true);
                    getUserDbm().updateMessage(message.LocalTimestamp, Message.MsgStatus.PENDING);
                    message = getMessage(message.LocalTimestamp);
                    doSendMessage(message);
                }
            }
            else
            {
                bool canSend = false;
                var user = SessionManager.Instance.User;
                var mediaResponse = await RestfulAPIManager.INSTANCE.SendFile(user.UserKey, user.CompanyUserId, richData.MediaFile);
                if (mediaResponse != null && string.IsNullOrWhiteSpace(mediaResponse.file) == false)
                {
                    richData.MediaUrl = mediaResponse.file;
                    updateMessageRichData(message.LocalTimestamp, richData, false);
                    canSend = true;
                }

                var thumbResponse = await RestfulAPIManager.INSTANCE.SendFile(user.UserKey, user.CompanyUserId, richData.ThumbnailFile);
                if (thumbResponse != null && string.IsNullOrWhiteSpace(thumbResponse.file) == false)
                {
                    richData.ThumbnailUrl = thumbResponse.file;
                    updateMessageRichData(message.LocalTimestamp, richData, false);
                }

                if (canSend)
                {
                    getUserDbm().updateMessage(message.LocalTimestamp, Message.MsgStatus.PENDING);
                    message = getMessage(message.LocalTimestamp);
                    doSendMessage(message);
                }
            }
        }
        catch( Exception ex)
        {
            mLogger.Error("uploadMediaFile", ex);
        }
    }

    private async void uploadFaxFile(Message message, RichData richData)
    {
        if (getUserDbm() == null)
            return;

        try
        {
            // upload fax            
            var user = SessionManager.Instance.User;
            var response = await RestfulAPIManager.INSTANCE.SendFax(user.UserKey, user.CompanyUserId, user.PartnerId, message.ToDid, message.FromDid, richData.MediaFile);

            if (getUserDbm() == null)
                return;

            if (response != null && response.succeeded() )
            {
                getUserDbm().updateMessage(message.LocalTimestamp, Message.MsgStatus.SENT);
                message = getMessage(message.LocalTimestamp);
            }
            else // Failed either because of Network Failure or the Remote server did not handle the request properly
            {
                getUserDbm().updateMessage(message.LocalTimestamp, Message.MsgStatus.FAILED);
                message = getMessage(message.LocalTimestamp);
            }
        }
        catch (Exception ex)
        {
            mLogger.Error("uploadFaxFile", ex);

            // Message sending failed
            getUserDbm().updateMessage(message.LocalTimestamp, Message.MsgStatus.FAILED);
            message = getMessage(message.LocalTimestamp);
        }
    }

//	private void retrieveGroupMessage(Context context, String body) {	//TODO-GM
//		GroupMessageMessage message = GroupMessageMessage.fromJson( body );
//
//		if ( message != null ) {
//			//NOTE: If we sent the message, just throw this away so we don't have sent and received copies of same message.
//			boolean handle = true;
//
//			//NOTE: If we sent the message, based on messageId, just throw this away so we don't have sent and received copies of same message.
//			//	Handle all Well Messages.  We check MessageId.isEmpty because of inconsistent data.
//			if ( !message.isTypeWellMessage() ) {
//				if ( !TextUtils.isEmpty( message.getMessageId() )) {
//					handle = !MessagesManager.INSTANCE.hasMessageId( message.getMessageId() );
//				}
//			}
//
//			if ( handle )
//				messageReceived(message);
//		} else {
//			// either an invalid GM message, or, if we sent the message we just throw this away so we don't have sent and received copies of same message.
//			if (BuildConstants.ENABLE_LOGGING) Log.d(TAG, "retrieveGroupMessage(): invalid GM Message - " + body );
//		}
//	}

	private long getTimeStampToRetrieveMessages( Message.MsgType type, Message.MsgDirection direction ) 
	{
		Int64 timestamp = 0;

        if (getUserDbm() != null)
		{ 
			//NOTE: We cannot just use LastMessageTimestamp without considering the state of the DB table.
			//	This is because:
			//		- During dev/testing, we may manually delete the DB without uninstalling the app.
			//		- End-user may manually delete the DB which would likely trigger a support call
			//			when messages are not properly populated.
			//	So, to avoid issues, we will ask the DB if it has any Message table entries.
			//		If, not then we NOT use the LastMessageTimestamp.
			//		It is unclear if we have to manually override that value or if it will
			//		be handled normally.  I think normally.

			Int64 timestamp1 = getUserDbm().getLatestMessageServerTimestamp( Message.MsgType.ALL, Message.MsgDirection.ALL, Message.MsgStatus.ALL );
			Int64 timestamp2 = SessionManager.Instance.LastMessageTimestamp;

			if ( timestamp1 > 0 )
			{
				//Indicates we have Messages, but may not be the *proper* value, since messages may have been deleted.
				//	Check SessionManager and use greater value.
				timestamp = (timestamp2 > 0 ? timestamp2 : timestamp1 );
			}
			else
			{
				//Since we have no DB value, we must assume DB/Table has been deleted which means we make maximum query
				timestamp = 0;
			}

			if (timestamp > 0) 
			{
				//Indicates we have Messages, but may not be the *proper* value, since messages may have been deleted.
				//	Check SessionManager

				// WORKAROUND add a whole second to the time stamp as we use getMessageHistory() API which uses FULL seconds in the message time stamp. 
				//	If we will use the actual message time stamp it will retrieve it again.
				return (timestamp + 1000);
			} 
		}

		if ( timestamp == 0 )
            timestamp = VxTimestamp.GetServerTimestampSomeDaysBack(30);		//TODO: move literal to SettingsManager

		return timestamp;
	}

	/**
	 * Description:
	 * RESTful Server API getMessageHistory() : used to retrieve all message types.
	 * if time-stamp is NOT null, will retrieve all messages newer than time-stamp
	 * */
	public void retrieveMessageHistory()
	{
        if (SessionManager.Instance.User == null)
            return;

		retrieveMessageHistory( SessionManager.Instance.User.UserKey,  
								SessionManager.Instance.User.CompanyUserId, 
								Desktop.Config.API.DefaultNumberOfMessagesFromServer );
	}

	public void retrieveMessageHistory( String userKey, String userCompanyUserId, Int32 numberOfMessagesToRetrieve ) 
	{
        if (getUserDbm() == null)
            return;

		Int64 timestamp = getTimeStampToRetrieveMessages( Message.MsgType.ALL, Message.MsgDirection.ALL );

		MessageList msgList   = MessageHistory.getMessages( timestamp, userKey, userCompanyUserId, numberOfMessagesToRetrieve );

		//We may have invalid/incomplete messages for at least the following reason:
		//	- We have a chat message from someone not in our addressbook.
		//	  -- We have existing code to handle this when it is an incoming message, but need to address it here as well.

        if (getUserDbm() == null) // If the session is logged out before the request returns then DB will be null
            return;

		MessageList badChatMsgs = new MessageList();
		MessageList goodMsgs    = new MessageList();

		foreach ( Message msg in msgList )
		{
			bool isValid = true;

			if ( msg.isChat() )
			{
				if ( String.IsNullOrEmpty( msg.getOriginatingDid() ) )
				{
					isValid = false;
				}
			}

			if ( isValid )
			{
				//If we have a clientUid, we should try to update the msgId to resolve issue relate to sendSMSVerbose() and getMessageHistory() sequencing.
				if ( !String.IsNullOrWhiteSpace( msg.ClientUid ) )
				{
					try 
					{ 
						Int64 tempTimestamp = Convert.ToInt64( msg.ClientUid );
						getUserDbm().updateMessage( tempTimestamp, msg.Status, msg.MsgId );
					}
					catch ( System.FormatException /*ex*/ )
					{
						//Do nothing.  This is caused by old messages before we chose to use LocalTimestamp as ClientUid.
					}
				}

				goodMsgs.Add( msg );
			}
			else 
			{ 
				badChatMsgs.Add( msg );
			}
		}

		//I am seeing duplicate msgIds.  Let's check it out.
        var goodMsgs1 = (from c in goodMsgs
                               orderby c.MsgId, c.Type
                               select c).ToList();

		String		    tempMsgId = "";
		Message.MsgType tempType   = Message.MsgType.UNKNOWN;
		Int64			tempLocalTs = 0;
		Int64			tempServerTs = 0;
		Message.MsgDirection tempDir = Message.MsgDirection.ALL;
		Message			tempMsg;
		int    item = 0;
		int	   dupeCnt = 0;

		foreach ( Message msg1 in goodMsgs1 )
		{
			if ( msg1.MsgId == tempMsgId )
			{
				if ( msg1.Type == tempType )
				{
					dupeCnt++;
				}
			}

			tempMsgId    = msg1.MsgId;
			tempType     = msg1.Type;
			tempLocalTs  = msg1.LocalTimestamp;
			tempServerTs = msg1.ServerTimestamp.AsLong();
			tempDir		 = msg1.Direction;

			tempMsg = msg1;

			item++;
		}

		//Add good/valid messages to DB
        mLogger.Info("Adding message list to database");
        getUserDbm().addMessages(goodMsgs);

		//Handle invalid msgs
		foreach ( Message msg in badChatMsgs )
		{
			handleChatMessageDidIntegrity( msg );
		}

		//Update 'latest' msg timestamp in SessionManager
        if (msgList != null && msgList.Count > 0)
        {
            var lastTimestamp = (from a in msgList
                               orderby a.ServerTimestamp.AsLong() descending
                               select a.ServerTimestamp.AsLong() ).First();

            if (lastTimestamp > SessionManager.Instance.LastMessageTimestamp)
                SessionManager.Instance.LastMessageTimestamp = lastTimestamp;
        }
	}

	//Make server API call to retrieve phone number(s) for given Voxox userId
	//	Use updated info to update Messages table
	private void handleChatMessageDidIntegrity( Message message )
	{
        if (getUserDbm() == null)
            return;

		String jid           = Desktop.Util.Jid.getCleanJid( message.getOriginatingJid() );
		String contactUserId = Desktop.Util.Jid.getUserId( jid );

		if ( !String.IsNullOrEmpty( contactUserId ) )
		{
			string userKey       = SessionManager.Instance.User.UserKey;
			string companyUserId = SessionManager.Instance.User.CompanyUserId;

            String response = RestfulAPIManager.INSTANCE.RetrieveCompanyPhoneByUser( userKey, companyUserId, contactUserId, true );

            if ( !String.IsNullOrEmpty( response ) )
            {
				//Add new phone number to DB
				String contactKey = Guid.NewGuid().ToString();
				String source	  = "IN";
				getUserDbm().addOrUpdateXmppContact( jid, response, contactKey, source );

				//Normal handling of message.
				message.setOriginatingDid( response );
				messageReceived( message );
            }
		}
	}

	//private void downloadMediaThumbnailIfRequired( Message message, Boolean broadcastMode )	//TODO-RICHDATA
	//{
	//	final RichData richData = RichData.getRichData(message.getBodyForRichData());

	//	if (richData.getType() == RichData.RichDataType.IMAGE || richData.getType() == RichData.RichDataType.VIDEO) {

	//		if (MediaFilesHelper.isExternalStorageWriteable()) {
	//			String url = richData.getThumbnailUrl();

	//			// set local storage
	//			final String outputMediaFileName = MediaFilesHelper.convertURLToOutputMediaFile(url);
	//			if (url != null && outputMediaFileName != null) {

	//				// verify HTTPS URL (REST returns HTTP instead of HTTTPS)
	//				if ( ! url.startsWith("https")) {
	//					url = url.replace("http", "https");
	//				}
	//				final File dir = MediaFilesHelper.getVoxoxExternalStorageDirectory(richData.getType());
	//				new ThumbnailFetcherTask(dir.getAbsolutePath() + File.separator + outputMediaFileName, message.LocalTimestamp, broadcastMode).execute(url);
	//			}
	//		}
	//	}
	//}

	public String downloadFax( Int64 msgTimeStamp )
	{
        Message	message		  = MessageManager.getInstance().getMessage( msgTimeStamp );
		String  fileExt		  = Path.GetExtension( message.FileLocal );
		String	url           = Config.Urls.ExtranetBase_URL + "/" + message.FileLocal;		//TODO: This data comes form server but is stored in wrong location.
		String  localFileName = "FAX-" + message.RecordId.ToString() + fileExt;

		return RichMediaManager.Download( localFileName, url );
	}

	public String downloadImage( Int64 msgTimeStamp )
	{
		return downloadMediaFile( msgTimeStamp, "IMAGE" );
	}

	public String downloadVideo(  Int64 msgTimeStamp )
	{
		return downloadMediaFile( msgTimeStamp, "VIDEO" );
	}

	private String downloadMediaFile( Int64 msgTimeStamp, String filePrefix )
	{
        Message	 message		= MessageManager.getInstance().getMessage( msgTimeStamp );
		RichData richData		= RichData.fromJsonText(message.Body);

		String   remoteUrl		= richData.MediaUrl;
		String   fileExt		= Path.GetExtension( remoteUrl );
		String   localFileName	= filePrefix + "-" + message.RecordId.ToString() + fileExt;

		return RichMediaManager.Download( localFileName, remoteUrl );
	}

	public void deleteMessages( MessageList messages )			//Simplify call by providing a list of message.LocalTimestamps?
	{
        if (getUserDbm() == null)
            return;

		getUserDbm().deleteMessages( messages );		//Flags local DB messages as DELETED
		deleteMessageHistory();						//Handles deleting messages on server.
	}

    /// <summary>
    /// Makes a call to server to delete the messages
    /// </summary>
    /// <param name="messages"></param>
    public void deleteMessageHistory() 
	{
        if (getUserDbm() == null)
            return;

        try
        {
            if (getUserDbm().hasDeletedMessages())
            {
                DeleteMessageDataList messages = getUserDbm().getMessageDataForDeletedMessages();
                List<DeletedMessagesToServer> deletedMessages = new List<DeletedMessagesToServer>();

                foreach(var message in messages)
                {
                    deletedMessages.Add(new DeletedMessagesToServer(
                       message.MsgId,
                       (int)Message.statusToServerStatus(message.MsgStatus),
                       (int)Message.typeToServerType(message.MsgType)
                    ));
                }
                
                var updatedList = new List<UpdatedMessage>();
                Task.Run(() =>
                {
                    updatedList = RestfulAPIManager.INSTANCE.updateMessageStatusBatch(SessionManager.Instance.User.UserKey, SessionManager.Instance.User.CompanyUserId, deletedMessages);
                    if (updatedList != null)
                    {
                        // TODO: Need to delete messages from DB
                    }
                });
            }
        }
        catch(Exception e)
        {
            mLogger.Error("Deleting messages on server failed" + e);
        }
	}

    /// <summary>
    /// Deleting the messages from local DB . Which are deleted from other mobile clients.
    /// </summary>
    /// <param name="messages"></param>
    public void deleteMessagesBasedOnHistory(MessageList messages)
    {
        if (getUserDbm() == null)
            return;

		getUserDbm().removeDeletedMessages( messages );
    }

	private Boolean translateMessageIfRequired( Message message ) 
	{
		Boolean messageTranslated = false;

		//MessageTranslationProperties mtp = getTranslateMessageProperties(message);	//TODO-TRANSLATE

		//if (mtp != null && mtp.getEnabled() && mtp.isDirectionActive(message.getDirection()) && isTranslationAvailable(message)) {

		//	final String userLanguage    = mtp.getUserLanguage();
		//	final String contactLanguage = mtp.getContactLanguage();

		//	if (message.isOutbound() ) {
		//		new MessageTranslatorTask(context, message, userLanguage, contactLanguage).execute();
		//	} else {
		//		new MessageTranslatorTask(context, message, contactLanguage, userLanguage).execute();
		//	}

		//	messageTranslated = true;
		//}

		return messageTranslated;
	}

	//private MessageTranslationProperties getTranslateMessageProperties( Message message )		//TODO-TRANSLATE
	//{
	//	String  phoneNumber       = null;
	//	String  jid		          = null;

	//	if ( message.isChat() ) 
	//	{
	//		if (((ChatMessage) message).isCarbon()) 
	//		{
	//			// Don't translate received outgoing carbon messages
	//			return null;
	//		}

	//		jid =  message.isInbound() ? ((ChatMessage) message).getFromJID() : ((ChatMessage) message).getToJID();
	//	} 
	//	else 
	//	{
	//		phoneNumber = message.getOriginatingDid();
	//	}

	//	return getUserDbm().getTranslationModeProps(phoneNumber, jid);
	//}

	private Boolean isTranslationAvailable( Message message ) 
	{
        if (getUserDbm() == null)
            return false;

		RichData richData = getUserDbm().richDataFromJsonText( message.Body );
		return message.isTranslatable( richData );
	}

	//public Boolean addContactMessageTranslation( MessageTranslationProperties mt)		//TODO-TRANSLATE
	//{
	//	return getUserDbm().addContactMessageTranslation(mt);
	//}

	//public MessageTranslationProperties getTranslationModeProps( String phoneNumber, String jid)	//TODO-TRANSLATE
	//{
	//	return getUserDbm().getTranslationModeProps(phoneNumber, jid);
	//}

}	//	class MessageManager


}	//namespace Desktop.Model.Messaging
