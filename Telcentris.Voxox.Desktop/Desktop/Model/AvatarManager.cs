﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Utils;		//Logger

using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;				//PrivateFontCollection
using System.IO;						//Stream, File
using System.Reflection;				//Assembly
using System.Runtime.InteropServices;	//Marshal
using System.Windows;					//Application

using System.Collections.Generic;		//Dictionary

//---------------------------------------------------------------------------------------
// JRT - 2015.11.12 - This class has been optimized for fastest creation of Avatars.
//		Current timing is about 1 ms per avatar with about 80% of that in the Bitmap.Save() method.
//---------------------------------------------------------------------------------------

namespace Desktop.Model
{
    public class AvatarManager
    {
        private VXLogger logger = VXLoggingManager.INSTANCE.GetLogger("AvatarManager");

        private PrivateFontCollection mPfc;

		private int				mBmpWidth  = 96;
		private int				mBmpHeight = 96;
        private string			mPackUri;
        private Graphics		mGfx;
		private Bitmap			mBmp;
		private StringFormat	mSf;
		private Rectangle		mRectangle;
		private Color			mColorBackground = Color.FromArgb( 214, 214, 214 );
		private Color			mColorForeground = Color.FromArgb( 164, 164, 164 );
		private SolidBrush		mBrushForeground;
		private Font			mFont;

		private ImageCodecInfo	  mImgCodec;
		private EncoderParameters mCodecParams;

        private enum AvatarType
        {
            Calling,
            Contact,
            Messaging
        }

		private AvatarManager()
		{
			Initialize();
		}

        public static AvatarManager Instance
        {
            get { return Nested.instance; }
        }

        class Nested
        {
            static Nested()		{}

            internal static readonly AvatarManager instance = new AvatarManager();
        }

        /// <summary>
        /// Initialize fonts
        /// </summary>
        private void Initialize()
        {
			mBrushForeground = new SolidBrush( mColorForeground );

			initFont();
			initGraphics();
		}

		private void initFont()
		{
            if ( mPfc == null)
            {
                mPfc = new PrivateFontCollection();

                using (Stream fontStream = Application.GetResourceStream(new Uri("/Branding/Fonts/segoeui.ttf", UriKind.RelativeOrAbsolute)).Stream)
                {
                    if (null == fontStream)
                    {
                        return;
                    }

                    int fontStreamLength = (int)fontStream.Length;

                    IntPtr data = Marshal.AllocCoTaskMem(fontStreamLength);

                    byte[] fontData = new byte[fontStreamLength];
                    fontStream.Read(fontData, 0, fontStreamLength);

                    Marshal.Copy(fontData, 0, data, fontStreamLength);

                    mPfc.AddMemoryFont(data, fontStreamLength);

                    Marshal.FreeCoTaskMem(data);
                }

                Assembly assembly = Assembly.GetExecutingAssembly();
                mPackUri = string.Format("/{0};component", assembly.GetName().Name);
            }
		}

		private void initGraphics()
		{
			mImgCodec			  = Array.Find<ImageCodecInfo>(ImageCodecInfo.GetImageEncoders(), delegate(ImageCodecInfo c) { return c.FormatID == ImageFormat.Jpeg.Guid; });
			mCodecParams		  = new EncoderParameters(1);
			mCodecParams.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 80L);

			mFont = new Font( mPfc.Families[0], 32 );

			mSf				  = new StringFormat();
			mSf.LineAlignment = StringAlignment.Center;
			mSf.Alignment	  = StringAlignment.Center;

			mBmp = new Bitmap ( mBmpWidth, mBmpHeight );
            mBmp.SetResolution( mBmpWidth, mBmpHeight );

			mRectangle = new Rectangle(0, 0, mBmp.Width, mBmp.Height);

			if ( mGfx == null )
			{
				makeGraphics();
			}
        }

		private void makeGraphics()
		{
			Font		 font = new Font( mPfc.Families[0], 32 );
			StringFormat sf   = new StringFormat();
			sf.LineAlignment  = StringAlignment.Center;
			sf.Alignment	  = StringAlignment.Center;

            mGfx = Graphics.FromImage( mBmp );

			mGfx.InterpolationMode	= InterpolationMode.HighQualityBicubic;
			mGfx.SmoothingMode		= SmoothingMode.HighQuality;
			mGfx.CompositingQuality	= CompositingQuality.HighQuality;
			mGfx.PixelOffsetMode	= PixelOffsetMode.HighQuality;
			mGfx.TextRenderingHint	= TextRenderingHint.ClearTypeGridFit;
		}

        /// <summary>
        /// Dispose of resources
        /// </summary>
        public void Dispose()
        {
            if ( mPfc != null)
            {
                mPfc.Dispose();
                mPfc = null;
            }
        }

        /// <summary>
        /// Generates Avatar with initials derived from Name
        /// </summary>
        /// <param name="name">If Name is more than one word, character from first two words are used, else just the first character</param>
        /// <param name="contactKey">ContactKey of the contact in format "contactKey.jpg"</param>
        public void Generate( string name, string contactKey )
        {
			string avatarFile = getFullPath( contactKey );

			try
			{
				if (File.Exists(avatarFile))
					File.Delete(avatarFile);
			}
			catch (Exception ex)
			{
				logger.Error("Generate:" + contactKey, ex);
			}

            try
            {
				String inits = GetInitials(name);

				mGfx.Clear( mColorBackground );			//Faster than FillRectangle()
				mGfx.DrawString( inits, mFont, mBrushForeground, mRectangle, mSf);

				mBmp.Save( avatarFile, mImgCodec, mCodecParams );
            }

            catch (Exception ex)
            {
                logger.Error( "Generate: " + contactKey, ex );
            }
        }

		//Get URIs
		public string GetUriForCalling( string contactKey )
		{
			return GetUri( contactKey, AvatarType.Calling );
		}

		public string GetUriForContact( string contactKey )
		{
			return GetUri( contactKey, AvatarType.Contact );
		}

		public string GetUriForMessaging( string contactKey )
		{
			return GetUri( contactKey, AvatarType.Messaging );
		}

        /// <summary>
        /// Get the Avatar for ContactKey
        /// </summary>
        /// <param name="contactKey">ContactKey of the Contact</param>
        /// <returns>File Path to the Contact Avatar stored in local cache</returns>
        private string GetUri( string contactKey, AvatarType type )
        {
			if ( !String.IsNullOrEmpty( contactKey ) )
			{ 
				string avatarFilePath = getFullPath( contactKey );

				if ( File.Exists( avatarFilePath ) )
					return avatarFilePath;
			}

            switch (type)
            {
            case AvatarType.Contact:
                return mPackUri + Config.UI.ContactNoAvatar;

            case AvatarType.Messaging:
                return mPackUri + Config.UI.MessagingNoAvatar;

            case AvatarType.Calling:
                return mPackUri + Config.UI.CallingNoAvatar;
            }

            return string.Empty;
        }

        /// <summary>
        /// Initials parsing logic
        /// </summary>
        /// <param name="Name"></param>
        /// <returns></returns>
        private string GetInitials(string Name)
        {
            string initials = "#";

            if (string.IsNullOrWhiteSpace(Name))
                return initials;

            Name = Name.Trim().ToUpper();

            if (Name.Contains(" "))
            {
                string[] parts = Name.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                if (parts != null && parts.Length >= 2)
                {
                    initials = parts[0][0].ToString() + parts[1][0].ToString();
                }
            }
            else
            {
                initials = Name.Length > 0 ? Name[0].ToString() : initials;
            }

            return initials;
        }

        // Avatar Cache store
        private string CheckAndGetAvatarDir()
        {
            string avatarDir = Path.Combine(SessionManager.Instance.UserAppDataFolder, "Avatar");

            try
            {
                if (Directory.Exists(avatarDir) == false)
                    Directory.CreateDirectory(avatarDir);
            }
            catch (Exception ex)
            {
                logger.Error("CheckAndGetAvatarDir", ex);
            }

            return avatarDir;
        }

		private string getFullPath( string contactKey )
		{
			string avatarFilePath = "";

            if ( !string.IsNullOrWhiteSpace( contactKey ) )
            {
				string avatarDir = CheckAndGetAvatarDir();

				if ( !contactKey.ToLower().EndsWith(".jpg") )
					contactKey += ".jpg";

				avatarFilePath = Path.Combine( avatarDir, contactKey );
			}
			
			return avatarFilePath;
		}
    }
}
