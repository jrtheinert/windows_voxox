﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Oct 2014
 */

using Telcentris.Voxox.ManagedSipAgentApi.ManagedSipDataTypes;	//AudioDevice

using GalaSoft.MvvmLight.Threading;		//DispatcherHelper

using System;
using System.Threading;					//Thread.Sleep
using System.Windows.Media;				//MediaPlayer

//Simple class to handle playing of sounds files:
//	- on specific devices 
//	- asynchronously
//	- with option to loop from 1 - n times

namespace Desktop.Model.Calling
{
    class Sound
    {
        #region Properties and memvars

        public AudioDevice	Device		 { get; set; }
        public Int32		LoopCount	 { get; set; }
        private Boolean		Abort		 { get; set; }

        private MediaPlayer player = null;
		private String		fileToPlay;
        private Int32		loopCounter;

        #endregion

        public Sound(string fileToPlayIn)
        {
            fileToPlay	 = fileToPlayIn;
            Abort		 = false;

            // Use the UI thread to create the Media player to receive the MediaEnded event
            DispatcherHelper.CheckBeginInvokeOnUI(() =>
            {
                player = new MediaPlayer();
                player.MediaEnded += player_MediaEnded;
            });
        }

        public void play()
        {
            Abort = false;

            // If there isn't file available to play ignore the call for now
            // TODO: Does this need to throw an Exception?
            if (string.IsNullOrWhiteSpace( fileToPlay ) == true)
                return;

            //TODO: We need to play on the designated output device.  This does not do that.

            loopCounter = LoopCount;

            StartPlaying();
        }

        private void StartPlaying()
        {
            // Launch the sound play on UI thread to receive the MediaEnded event
            DispatcherHelper.CheckBeginInvokeOnUI(() =>
            {
                player.Stop();
                player.Open(new Uri( fileToPlay ));
                
                Thread.Sleep(200); // Smallish sound files seem to not play at-all like incoming message sound, so added a delay

                player.Play();
            });
        }

        void player_MediaEnded(object sender, EventArgs e)
        {
            if (!Abort && loopCounter == -1)
            {
                StartPlaying();
            }
            else
            {
                loopCounter--;

                if (Abort || loopCounter <= 0)
                {
                    loopCounter = 0;
                }
                else
                {
                    StartPlaying();
                }
            }
        }

        public void stop()
        {
            Abort = true;	//This allows complete play of sound file.  We need player.Stop() to stop immediately.

            // Stop the sound play on UI thread
            DispatcherHelper.CheckBeginInvokeOnUI(() =>
            {
                player.Stop();
            });
        }
    }	//	class Sound

}	//namespace Desktop.Model.Calling