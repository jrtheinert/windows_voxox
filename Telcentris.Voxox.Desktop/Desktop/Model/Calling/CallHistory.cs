﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Util;			//TimeUtils
using Desktop.Utils;		//Logger

using ManagedDataTypes;		//RecentCallList, VxTimestamp

using Vxapi23;				//RestfulApiManager
using Vxapi23.Model;		//CallHistoryResponse

using System;
using System.Collections.Generic;

namespace Desktop.Model.Calling
{
	class CallHistory
	{
		static public RecentCallList getCalls( Int64 timestamp, String userKey, String userCompanyUserId, int numberOfCallsToRetrieve )
		{
			RecentCallList callList = new RecentCallList();

            String serverFormatTimestamp = VxTimestamp.GetFormattedTimestampForREST(timestamp);
		    VXLogger logger				 = VXLoggingManager.INSTANCE.GetLogger("CallHistory");
			Int32	  seenCount			 = 0;

			try
			{
				//Some default values.
//				String contactFilter  = null;
//				String contactAddress = null;
//				int	   offset         = 0;

				//NOTE: This API call has way more options than this.
				//TODO: Apply timestamp
				List<CallHistoryResponse> apiCallList = RestfulAPIManager.INSTANCE.GetCallHistory( userKey, userCompanyUserId, serverFormatTimestamp, numberOfCallsToRetrieve );
                
                var db = SessionManager.Instance.UserDataStore;

                if (db == null)
                    return callList;

				if ( apiCallList != null )
				{
                    // we are not ussing userDid anywhere here.
                    //String userDid = SessionManager.Instance.User.Did;

					foreach ( CallHistoryResponse callIn in apiCallList )
					{
						//Some data conversions:
						//	- Server Inbound/Outbound values do NOT match client values (maybe)
						//	- Server has Missed as boolean, we have a Status, so we need to convert.
						//	- Suspect startEpoch needs adjusting.

						int						companyId		= callIn.companyId;		//Not used, but want to verify
						int						companyUserId	= callIn.companyUserId;	//Not used, but want to verify
						Int64					stopTime        = callIn.stopEpoch;		//Not used, but want to verify
						String					toDid			= callIn.to;			//Not used, but want to verify. Should always be user, right?
						String					startTimeString = callIn.callTime;		//Not used, but want to verify

						Boolean					incoming		= callIn.isInbound();
						RecentCall.CallStatus	status			= callIn.isMissed() ? RecentCall.CallStatus.Missed : RecentCall.CallStatus.Answered;
						RecentCall.SeenFlag     seenFlag		= callIn.isSeen() ? RecentCall.SeenFlag.Seen : RecentCall.SeenFlag.NotSeen;
							
						if ( seenFlag == RecentCall.SeenFlag.Seen )
						{
							seenCount++;
						}

						RecentCall call = new RecentCall();

						call.Uid			= callIn.uid;
                        call.OrigNumber     = incoming ? callIn.from : callIn.to;
                        call.Number         = incoming ? callIn.from : callIn.to;		//TODO: Normalize?
						call.Status			= status;
						call.Incoming		= incoming;
						call.StartTime		= new VxTimestamp( callIn.startEpoch, true );
						call.Duration		= callIn.duration;
						call.Seen			= seenFlag;

						callList.Add( call );

						System.Threading.Thread.Sleep( 1 );	//So we can get unique local timestamps.	//TODO: Drop use of LocalTimestamp as unique ID?
					}	//foreach
				}
			}

			catch (Exception ex)
			{
				logger.Error("Error while synchronizing", ex);
			}

			return callList;
		}
	}
}
