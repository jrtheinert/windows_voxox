﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Oct 2014
 */

using Desktop.Utils;											//Logger
using Desktop.Util;												//PhoneUtils, NetworkStatus

using Telcentris.Voxox.ManagedSipAgentApi;						//SipAgentApi
using Telcentris.Voxox.ManagedSipAgentApi.ManagedSipDataTypes;	//ServerInfo

using GalaSoft.MvvmLight.Messaging;

using System;
using System.Collections.Generic;								//KeyValuePair

namespace Desktop.Model.Calling
{

    //NOTE: We will need access to Session Manager or SipAccount object, however
    //	we choose to track SIP connection status, SIP DID, etc.
    // I am adding an temp object here just to get this to compile
    public class SipAccount
    {
        public SipAccount()
        {
            IsConnected = false;
        }

        public void phoneLineAuthenticationError()
        {
            //		loginStateChangedEvent(*this, EnumSipLoginState::passwordError(), "");	//TODO-LISTENER-DELEGATE.
        }

        #region Properties
        public String DisplayName { get; set; }
        public String UserName { get; set; }
        public String Identity { get; set; }
        public String Password { get; set; }
        public String Realm { get; set; }


        public ServerInfo SipProxyServer { get; set; }
        public ServerInfo SipRegistrarServer { get; set; }
        public ServerInfo SipStunServer { get; set; }
        public ServerInfo SipTurnUdpServer { get; set; }
        public ServerInfo SipTurnTcpServer { get; set; }
        public ServerInfo SipTurnTlsServer { get; set; }
		public ServerInfo SipHttpProxyServer { get; set; }

        public Boolean IsConnected { get; set; }
        public Int32 VirtualLineId { get; set; }

        #endregion
    };

    //-----------------------------------------------------------------------------

    public class PhoneLine : IDisposable
    {
        private static VXLogger logger		   = VXLoggingManager.INSTANCE.GetLogger("PhoneLine");
        private static VXLogger sipAgentLogger = VXLoggingManager.INSTANCE.GetLogger("SipAgent");
        private SipAgentApi mSipApi;
        private SipAccount mSipAccount;

        private Int32 mLineId;	//TODO = SipAgent.VirtualLineIdError;
        private PhoneLineState.LineState mState;

        private PhoneCall mActiveCall;
        private PhoneCalls mPhoneCalls = new PhoneCalls();

        private PhoneLineStateChangeHandler mStateChangeHandler = new PhoneLineStateChangeHandler();
        private SipEventHandler mSipEventHandler = null;

        private bool mSipEventHandlerAdded	= false;
        private bool mDisposed				= false; // Flag to check if resources are properly disposed

        // These will call UI layer to display appropriate error messages to user, as needed.
        public enum CallError
        {
            NoError				= 0,
            EmptyPhoneNumber	= 1,
            InvalidNumberLength = 2,
            ContactNotFound		= 3,
            NotConnected		= 4,
            CallNotHeld			= 5,
            SipError			= 6,
            CallAlreadyStarted	= 7,
            NoNetwork			= 8
        };

        public PhoneLine(SipAccount sipAccount)
        {
            logger.Info("Initialized phoneline with " + sipAccount);
            mSipAccount = sipAccount;
            mSipApi = SipAgentApi.getInstance();

            setLineIdInvalid();

            mState			 = PhoneLineState.LineState.Unknown;	//In old app this was a base class which was overridden for each actual line state.
            mActiveCall		 = null;
            mSipEventHandler = new SipEventHandler(OnSipEvent);

            // Message handler for Playing the Dtmf tone on the current call through dialpad
            Messenger.Default.Register<PlayDtmfOnCallMessage>(this, OnPlayDtmfOnCallMessage);
        }

        ~PhoneLine()
        {
            logger.Info("Initialized default phoneline with no parameters");

            disconnect(false);

            //Do not need to delete everything (_phoneLineStateList) since states are static inside the constructor
            mState = PhoneLineState.LineState.Unknown;

            Dispose();

            mSipApi = null;
        }

        public void Dispose()
        {
            if (!mDisposed)
            {
                mSipApi.terminate();

                if (mSipEventHandlerAdded == true && mSipEventHandler != null)
                {
                    mSipApi.removeSipEventListener(mSipEventHandler);
                    mSipEventHandler = null;
                    mSipEventHandlerAdded = false;
                }

                mDisposed = true;
            }
        }

		private ManagedDbManager.ManagedDbManager getUserDbm()
		{
			return SessionManager.Instance.UserDataStore;
		}

        public Boolean init()
        {
            mSipApi = SipAgentApi.getInstance();
            setLineIdInvalid();
            logger.Info("Initialized phoneline with lineid " + mLineId);
            return initSipWrapper();
        }

        public SipAccount getSipAccount()
        {
            return mSipAccount;
        }

        private Int32 getInvalidLineIdValue()
        {
            return -1;		//TODO: SipAgentApi::VirtualLineIdError;
        }

        private void setLineIdInvalid()
        {
            mLineId = getInvalidLineIdValue();
        }

        private Boolean isLineIdValid()
        {
            return mLineId != getInvalidLineIdValue();
        }


        public void redial()
        {
            String lastDialedNumber = "";	//ConfigManager::current().getLastDialedNumber()	//TODO-CONFIG:
            makeCall( lastDialedNumber, false );	//TODO
        }


        public CallError makeCall( String phoneNumber, Boolean isExtension )
        {
            if (AudioDeviceMgr.Instance.DevicesAvailable == false)
            {
                logger.Info("No audio devices available.");

				ActionManager.Instance.ShowNoAudioDevicePopup( true, false ); //Show, isAfterLogin

                return CallError.NoError;
            }

            logger.Info("make call initialized with phone number: " + phoneNumber);

            if (!mSipAccount.IsConnected)
            {
                logger.Error("SipAccount not connected");
                return CallError.NotConnected;
            }

            if (NetworkStatus.Instance.IsAvailable == false)
            {
                logger.Error("No internet");
                NetworkStatus.Instance.ShowNetworkErrorMessage(); // The alert is called from here instead of calling at several places
                return CallError.NoNetwork;
            }

            if (String.IsNullOrEmpty(phoneNumber))
            {
                logger.Error("empty phone number");
                return CallError.EmptyPhoneNumber;
            }
      
			//Valid phone number length
			if ( isExtension )
			{
				if (PhoneUtils.IsValidExtension(phoneNumber) == false)
				{
					logger.Error("invalid extension length");
					return CallError.InvalidNumberLength;
				}
			}
			else 
			{
				if (PhoneUtils.IsValidPhoneNumberLength(phoneNumber) == false)
				{
					logger.Error("invalid phone number length");
					return CallError.InvalidNumberLength;
				}
			}

            //TODO: Review this for call not held and why it appears to check only first call.
            //for ( PhoneCalls::iterator it = _phoneCallMap.begin(); it != _phoneCallMap.end(); ++it ) 
            //{
            //	PhoneCall* phoneCall = (*it).second;

            //	if (phoneCall) 
            //	{
            //		EnumPhoneCallState::PhoneCallState state = phoneCall->getState();
            //		if (state != PhoneCallState.CallState.Hold &&
            //			state != PhoneCallState.CallState.Closed ) 
            //		{
            //			return CallError.CallNotHeld;
            //		}
            //	}
            //}

			String		phoneNumberToCall = Util.PhoneUtils.GetPhoneNumberForCall( phoneNumber, isExtension );
            SipAddress	sipAddress		  = SipAddress.fromString(phoneNumberToCall, mSipAccount.Realm);

            if (mActiveCall != null)
            {
                //Check if there is an active call with the same phone number to call
                if (sipAddress.getUserName() == mActiveCall.PeerSipAddress.getUserName())
                {
                    return CallError.CallAlreadyStarted;
                }
            }

            //Puts all the PhoneCalls in the hold state before to create a new PhoneCall
            holdAllCalls();

            //Video not supported currently
            Boolean enableVideo = false;
            //		Config& config      = ConfigManager::current();
            //		bool    enableVideo = _userProfile.isOutgoingVideoEnableForCall(phoneNumber);
            //
            //		if (enableVideo) 
            //		{
            //			mSipApi.setVideoDevice( config.getVideoWebcamDevice() );
            //		}

            PhoneCall phoneCall = new PhoneCall(this, sipAddress);
            mActiveCall = phoneCall;
            int callId = mSipApi.makeCall(mLineId, sipAddress.getRawSipAddress(), enableVideo);

            if (callId < 0)
            {
                logger.Error("couldn't place the call, SipWrapper returned an error=" + callId.ToString());
                return CallError.SipError;
            }

            //		config.setLastDialedNumber( phoneNumber );	//TODO-CONFIG: Save last dialed number

            phoneCall.CallId = callId;
            mPhoneCalls.Add(callId, phoneCall);	//Adds the PhoneCall to the list of PhoneCalls

            phoneCall.setState(PhoneCallState.CallState.Dialing);

			Int64 ts = ManagedDataTypes.VxTimestamp.GetNowTimestamp();

            phoneCall.DbCallId = getUserDbm().addRecentCall( phoneNumber, ts, false, true );


            //		phoneCallCreatedEvent( *this, *phoneCall );	//TODO: Is anybody listening?

            return CallError.NoError;
        }


        public Boolean connect()
        {
            logger.Info("phoneline connect method initialized");
            Boolean result = true;

            if (!mSipAccount.IsConnected)
            {
                removeVirtualLine(true);

//                mLineId = mSipApi.addVirtualLine( mSipAccount.DisplayName, mSipAccount.UserName,
//                                                  mSipAccount.Identity,    mSipAccount.Password, mSipAccount.Realm,
//                                                  mSipAccount.SipProxyServer, mSipAccount.SipRegistrarServer);
                mLineId = mSipApi.addVirtualLine();

				logger.Info("LineId set to : " + mLineId + " for SipAccount " + mSipAccount.UserName + " with realm value " + mSipAccount.Realm);
                if (mLineId < 0)
                {
                    logger.Error("VirtualLine Creation Failed");
                    result = false;
                }
                else
                {
                    mSipAccount.VirtualLineId = mLineId;

                    if (mSipApi.registerVirtualLine(mLineId) != 0)
                    {
                        logger.Error("Couldn't register virtual line");
                        result = false;
                    }

                    logger.DebugFormat("connect username = {0}; server = {1}, lineId = {2}", mSipAccount.UserName, mSipAccount.SipRegistrarServer.Address, mLineId);
                }
            }

            return result;
        }

        void removeVirtualLine(Boolean force)
        {
            logger.Info("Removing virtual line : " + mLineId);
            if (isLineIdValid())
            {
                mSipApi.removeVirtualLine(mLineId, force);	//Force removal
                setLineIdInvalid();
            }
        }

        public void disconnect(bool force)
        {
            if (isLineIdValid())
            {
                logger.Info("disconnecting from " + mSipAccount.UserName);
                mSipAccount.IsConnected = false;
                removeVirtualLine(force);
            }
        }

        private void checkCallId(int callId)
        {
            PhoneCall phoneCall = getPhoneCall(callId);

            if (phoneCall == null)
            {
                logger.Fatal("unknown phone call callId=" + callId.ToString());	//TODO-LOG. Do we need this if all we do is LOG error?
            }
        }

        public long getCallDuration( int callId )
        {
			long result = 0;

            PhoneCall phoneCall = getPhoneCall(callId);

			if ( phoneCall != null )
			{
				result = phoneCall.getRunningDurationAsSeconds();
			}

			return result;
        }

        public void acceptCall(Int32 callId)
        {
            logger.Info("accept call from " + callId.ToString());
            holdCallsExcept(callId);

            bool enableVideo = false;
            PhoneCall phoneCall = getPhoneCall(callId);

            //Video not support in this phase
            //		Config&     config = ConfigManager::current();
            //		String		number;
            //		if ( phoneCall != null ) 
            //		{
            //			number = phoneCall.getPeerSipAddress().getUserName();
            //		}
            //
            //		enableVideo = _userProfile.isOutgoingVideoEnableForCall(number);
            //
            //		if (enableVideo) 
            //		{	
            //			mSipApi.setVideoDevice( config.getVideoWebcamDevice() );
            //		}

            mSipApi.acceptCall(callId, enableVideo);
            logger.Debug("call accepted callId=" + callId.ToString());

            getUserDbm().updateRecentCallAnswered(phoneCall.DbCallId);
        }

        public void rejectCall(int callId)
        {
            logger.Info("Rejecting call from : " + callId);
            checkCallId(callId);
            mSipApi.rejectCall(callId);
            logger.Debug("call rejected callId=" + callId.ToString());

            PhoneCall phoneCall = getPhoneCall(callId);

			if ( phoneCall != null )
				getUserDbm().updateRecentCallDeclined(phoneCall.DbCallId);
        }

        public void closeCall(int callId)
        {
            logger.Info("Close call : " + callId);
            checkCallId(callId);
            mSipApi.closeCall(callId);
            logger.Debug("call closed callId=" + callId.ToString());
        }

        public void holdCall(int callId)
        {
            logger.Info("Hold call : " + callId);
            checkCallId(callId);
            mSipApi.holdCall(callId);
            logger.Debug("call hold callId=" + callId.ToString());
        }

        public void resumeCall(int callId)
        {
            logger.Info("Resume call : " + callId);
            checkCallId(callId);
            mSipApi.resumeCall(callId);
            logger.Debug("call resumed callId=" + callId.ToString());
        }

        public void blindTransfer(int callId, String sipAddress)
        {
            checkCallId(callId);

            SipAddress sipUri = SipAddress.fromString(sipAddress, mSipAccount.Realm);
            mSipApi.blindTransfer(callId, sipUri.getRawSipAddress());
            logger.Debug("call transfered to=" + callId.ToString());
        }

        public string getSipCallId(int callId)
        {
            return mSipApi.getSipCallId(callId);
        }

        public void markCallAsMerging( int callId, Boolean value )
        {
            PhoneCall phoneCall = getPhoneCall(callId);

            if ( phoneCall != null )
            {
				phoneCall.IsMerging = value;
            }
        }

        public string getHttpListenerId(int callId)
        {
			string result = "";

            PhoneCall phoneCall = getPhoneCall(callId);

            if ( phoneCall != null )
            {
				result = phoneCall.HttpListenerId;
            }

			return result;
        }

        public void playSoundFile(int callId, String soundFile)
        {
            //No check
            //checkCallId(callId);
            mSipApi.playSoundFile(callId, soundFile);
        }

        public void playDtmf(int callId, char dtmf)
        {
            //No check
            //checkCallId(callId);
            mSipApi.playDtmf(callId, dtmf);
        }

		//public String getAudioCodecUsed(int callId)
		//{
		//	return mSipApi.getAudioCodecUsed(callId);
		//}

        //Video not supported in this phase
        //	public String getVideoCodecUsed(int callId) 
        //	{
        //		return mSipApi.getVideoCodecUsed(callId);
        //	}

        public void setPhoneCallState(int callId, PhoneCallState.CallState state, SipAddress sipAddress, int statusCode)
        {
            logger.Debug("call state changed callId=" + callId + " state=" + PhoneCallState.toString(state) + " from=" + sipAddress.getSipAddress());

            //Saves the last state
            PhoneCallState.CallState lastState = PhoneCallState.CallState.Unknown;
            PhoneCall phoneCall = getPhoneCall(callId);

            if (phoneCall != null)
            {
                lastState = phoneCall.getState();

                phoneCall.StatusCode = statusCode;

                if (phoneCall.getState() == state)
                {
                    //We are already in this state.  Prevents the state to be applied 2 times in a row.
                    return;
                }

                phoneCall.setState(state);
            }

            // This should not replace the state machine pattern PhoneCallState / PhoneLineState
            switch (state)
            {
                case PhoneCallState.CallState.Unknown:
                    break;

                case PhoneCallState.CallState.Error:
                    callClosed(callId);
                    break;

                case PhoneCallState.CallState.Resumed:
                    holdCallsExcept(callId);
                    mActiveCall = getPhoneCall(callId);
                    break;

                case PhoneCallState.CallState.Talking:
                    mActiveCall = getPhoneCall(callId);
                    if(lastState == PhoneCallState.CallState.Dialing) // Outgoing Call
                    {
                        getUserDbm().updateRecentCallAnswered(mActiveCall.DbCallId);
                        // Place other calls on hold
                        holdCallsExcept(callId);
                    }
                    break;

                case PhoneCallState.CallState.Dialing:
                    break;

                case PhoneCallState.CallState.Ringing:
                    break;

                case PhoneCallState.CallState.Closed:
                    if (lastState == PhoneCallState.CallState.Incoming)
                    {
                        //History: retrieve the call and change its state to missed
                        //				_userProfile.updateCallState( callId, HistoryMemento::MissedCall);		//TODO-DB
                        //				LOG_DEBUG("call missed callId=" + String::fromNumber(callId));			//TODO-LOG
                    }
                    callClosed(callId);
                    break;

                case PhoneCallState.CallState.Incoming:
                    {
                        PhoneCall phoneCall1 = new PhoneCall(this, sipAddress);
						Int64     ts		 = ManagedDataTypes.VxTimestamp.GetNowTimestamp();

                        phoneCall1.CallId = callId;
                        phoneCall1.setState(state);

                        phoneCall1.DbCallId = getUserDbm().addRecentCall(sipAddress.getUserName(), ts, true, true);

                        mPhoneCalls.Add(callId, phoneCall1);			//Adds the PhoneCall to the list of PhoneCall

						//We only allow 2 'active' calls for now.  These are one active call, and one on-hold call, as we have during merging conference call.
						//	If we get an incoming call at this point, just reject it, which *should* send it to VM.
						if ( mPhoneCalls.Count > 2 )	//TODO: SettingsManager.Instance.MaxConcurrentCalls )
						{
							rejectCall( callId );
						}
						else 
						{
							bool enableOutgoingVideo = false;	//_userProfile.isOutgoingVideoEnableForCall( sipAddress.getUserName() );//Username has number

							//Sends SIP code 180
							mSipApi.sendRingingNotification(callId, enableOutgoingVideo);

							mActiveCall = phoneCall1;
						}
                        break;
                    }

                case PhoneCallState.CallState.Hold:
                    break;

                case PhoneCallState.CallState.Missed:
                    //History: retrieve the and change its state to missed
                    //			_userProfile.updateCallState(callId, HistoryMemento::MissedCall);		//TODO-DB
                    //			LOG_DEBUG("call missed callId=" + String::fromNumber(callId));			//TODO-LOG
                    break;

                case PhoneCallState.CallState.Redirected:
                    break;

                case PhoneCallState.CallState.RingingStart:
                    break;

                case PhoneCallState.CallState.RingingStop:
                    break;

                //		default:
                //			LOG_FATAL("unknown PhoneCallState=" + PhoneCallState.toString(state));		//TODO-LOG
            }
        }

        private void callClosed(int callId)
        {
            logger.Info("close the call : " + callId);
            checkCallId(callId);

            PhoneCall phoneCall = getPhoneCall(callId);

            if (phoneCall != null)
            {
                if (mActiveCall == phoneCall)
                {
                    mActiveCall = null;
                }

                //Deletes the PhoneCall that is closed now
                //delete phoneCall;

                //History: update the duration of the memento associated to this phonecall
                //			_userProfile.updateCallDuration(callId, phoneCall->getDuration());		//TODO-DB

                getUserDbm().updateRecentCallTerminated(phoneCall.DbCallId);

				//Before we remove the call, clear the ConferenceId in any other calls.
				mPhoneCalls.clearConferenceId( phoneCall.ConferenceId );

                mPhoneCalls.Remove(callId);

                //			phoneCallClosedEvent(*this, *phoneCall);		//TODO-LISTENER-DELEGATE
            }
        }

        ////-----------------------------------------------------------------------------

        private void holdCallsExcept(Int32 callId)
        {
            foreach (KeyValuePair<Int32, PhoneCall> kv in mPhoneCalls)
            {
                if (kv.Key != callId)
                {
                    kv.Value.hold();
                }
            }
        }

		private void holdAllCalls()
        {
            foreach (KeyValuePair<Int32, PhoneCall> kv in mPhoneCalls)
            {
                 kv.Value.hold();
            }
        }

        public void setState(PhoneLineState.LineState state)
        {
            logger.Debug("PhoneLineState = " + PhoneLineState.toString(state));

            if (mState != state)
            {
                mState = state;

                this.mStateChangeHandler.execute(this, mState);

                if (state != PhoneLineState.LineState.AuthError)
                {
                    //				LOG_DEBUG( "line state changed lineId=" + String::fromNumber(mLineId) + " state=" + EnumPhoneLineState::toString(_state->getCode()) );	//TODO-LOG
                    //				stateChangedEvent(*this, state);	//TODO-LISTENER-DELEGATE
                }
            }
        }

        public PhoneCall getPhoneCall(Int32 callId)
        {
//            logger.Info("get call from " + callId);
            PhoneCall result;
            mPhoneCalls.TryGetValue(callId, out result);
            return result;
        }

        private bool initSipWrapper()
        {
            //Initialized before login so we have default codec list, but uninitialized on logOut, so check it.
            if (!mSipApi.isInitialized())
            {
				mSipApi.addSipEventListener(mSipEventHandler);
				mSipApi.setSipOptionUuid( SettingsManager.Instance.User.SipUuid );	//NOTE: From User, not Sip.  We want this BEFORE init();
            
				configureSipWrapper();

				int logLevel = (SettingsManager.Instance.App.MaxSipLogging ? 6 : 5);
                mSipApi.init( logLevel );	//PJSIP Use -1 to use default, or < 6 when in production. 4 = info, 5 = debug, 6 = verbose
            }

            setupAudioOnSip();

            mSipEventHandlerAdded = true;

            return mSipApi.isInitialized();
        }

        private void configureSipWrapper()
        {
            // TODO: Have to read this from Settings/Configuration
            TlsInfo tlsInfo				= new TlsInfo();

			tlsInfo.Method				= ( SettingsManager.Instance.Sip.UseTls ? SipAgent.TLSMethod.TLS_SSLv23 :  tlsInfo.Method = SipAgent.TLSMethod.TLS_SSLv2 );
            tlsInfo.VerifyCertificate	= false;		//Change to true after we have CA file
            tlsInfo.RequireCertificate	= false;		//Change to true after we have CA file
            tlsInfo.VerifyDepth			= 9;

            mSipApi.setTlsInfo(tlsInfo);

            // Add Servers list based on the SipAccount data
            ServerInfoList s_servers = new ServerInfoList();
            s_servers.addSipRegistrar( mSipAccount.SipRegistrarServer.Address,	mSipAccount.SipRegistrarServer.Port, mSipAccount.SipRegistrarServer.UseSsl );
            s_servers.addSipProxy	 ( mSipAccount.SipProxyServer.Address,		mSipAccount.SipProxyServer.Port,	 mSipAccount.SipRegistrarServer.UseSsl );
//            s_servers.addStun		 ( mSipAccount.SipStunServer.Address,		mSipAccount.SipStunServer.Port );
            s_servers.addTurnUdp	 ( mSipAccount.SipTurnUdpServer.Address,	mSipAccount.SipTurnUdpServer.Port,	 mSipAccount.SipTurnUdpServer.UserId,   mSipAccount.SipTurnUdpServer.Password );
            s_servers.addTurnTcp	 ( mSipAccount.SipTurnTcpServer.Address,	mSipAccount.SipTurnTcpServer.Port,	 mSipAccount.SipTurnTcpServer.UserId,   mSipAccount.SipTurnTcpServer.Password );
//            s_servers.addTurnTls	 ( mSipAccount.SipTurnTlsServer.Address,	mSipAccount.SipTurnTlsServer.Port,	 mSipAccount.SipTurnTlsServer.UserId,   mSipAccount.SipTurnTlsServer.Password );
			s_servers.addHttpProxy	 ( mSipAccount.SipHttpProxyServer.Address,	mSipAccount.SipHttpProxyServer.Port, mSipAccount.SipHttpProxyServer.UserId, mSipAccount.SipHttpProxyServer.Password, mSipAccount.SipHttpProxyServer.HttpProxyType );

            mSipApi.addServers(s_servers);

            // UUid & Logging
//            mSipApi.setSipOptionUuid				( SettingsManager.Instance.User.SipOptionUuid );	//NOTE: from User, not Sip. Moved prior to init().
			mSipApi.setMaxCalls						( 4 );
			mSipApi.setDoProxyCheck					( SettingsManager.Instance.Sip.DoProxyCheck );
			mSipApi.setSipLogFile					( SessionManager.Instance.SipLogFile      );

            // SSO Settings - NOTE: JRT - SIP Stack already defaults to proper values.  We should only be handling overrides.
#if QA_BUILD
            mSipApi.setDiscoveryTopology            ( SettingsManager.Instance.Advanced.EnableTURN ? SipAgent.Topology.TOPOLOGY_ICE : SipAgent.Topology.TOPOLOGY_NONE );
            mSipApi.setTopologyTurn					( SettingsManager.Instance.Advanced.EnableTURN		        );
#else
            mSipApi.setDiscoveryTopology            ( SettingsManager.Instance.Sip.Topology                     );
            mSipApi.setTopologyTurn                 ( SettingsManager.Instance.Sip.TopologyTurned               );
#endif
            mSipApi.enableIceMedia					( SettingsManager.Instance.Sip.EnableIceMedia				);
            mSipApi.enableIceSecurity				( SettingsManager.Instance.Sip.EnableIceSecurity			);
            mSipApi.setTopologyEncryption			( SettingsManager.Instance.Sip.TopologyEncryption			);

			
			mSipApi.setKeepAlive					( SettingsManager.Instance.Sip.KeepAlive					);
            mSipApi.setUseRport						( SettingsManager.Instance.Sip.UseRPort						);
            mSipApi.setRegistrationRefreshInterval	( SettingsManager.Instance.Sip.RegistrationRefreshInterval	);
            mSipApi.setFailureInterval				( SettingsManager.Instance.Sip.FailureInterval				);
            mSipApi.setOptionsKeepAliveInterval		( SettingsManager.Instance.Sip.OptionsKeepAliveInterval		);
            mSipApi.setMinSessionTime				( SettingsManager.Instance.Sip.MinimumSessionTime			);
            mSipApi.setSupportSessionTimer			( SettingsManager.Instance.Sip.SupportSessionTimer			);
            mSipApi.setInitiateSessionTimer			( SettingsManager.Instance.Sip.InitiateSessionTimer			);
            mSipApi.setTagProlog					( SettingsManager.Instance.Sip.TagProlog					);
            mSipApi.setSipOptionRegisterTimeout		( (uint)SettingsManager.Instance.Sip.SipOptionRegisterTimeout );
            mSipApi.setSipOptionPublishTimeout		( (uint)SettingsManager.Instance.Sip.SipOptionPublishTimeout  );
            mSipApi.setSipOptionUseOptionsRequest	( SettingsManager.Instance.Sip.SipOptionUseOptionsRequest	);
            mSipApi.setSipOptionP2pPresence			( SettingsManager.Instance.Sip.SipOptionP2PPresence			);
            mSipApi.setSipOptionUseTypingState		( SettingsManager.Instance.Sip.SipOptionUseTypingState		);
            mSipApi.setSipOptionChatWithoutPresence	( SettingsManager.Instance.Sip.SipOptionChatWithoutPresence	);

			mSipApi.setSipUdp(SettingsManager.Instance.Sip.SipUdp);
			mSipApi.setSipTcp(SettingsManager.Instance.Sip.SipTcp);
			mSipApi.setSipTls(SettingsManager.Instance.Sip.SipTls);

			// Phone Line Configuration
            mSipApi.enableAEC			  ( SettingsManager.Instance.Sip.EnableAEC				);
            mSipApi.enableAGC			  ( SettingsManager.Instance.Sip.EnableAGC				);
            mSipApi.enableNoiseSuppression( SettingsManager.Instance.Sip.EnableNoiseSuppression	);
            mSipApi.setCallsEncryption	  ( SettingsManager.Instance.Sip.CallEncryption			);

			mSipApi.setCredentials( mSipAccount.DisplayName, mSipAccount.UserName, mSipAccount.Identity, mSipAccount.Password, mSipAccount.Realm );
        }

        private void setupAudioOnSip()
        {
            // Volume Control            
			AudioDeviceMgr.Instance.SetAudioInputDeviceFromSettings();
			AudioDeviceMgr.Instance.SetAudioOutputDeviceFromSettings();

			//Now done via AudioDeviceMgr
//            mSipApi.setMicVolume    ((uint)SettingsManager.Instance.User.AudioInputVolume  );
//            mSipApi.setSpeakerVolume((uint)SettingsManager.Instance.User.AudioOutputVolume );
        }

        public Boolean isConnected()
        {
            return (mState == PhoneLineState.LineState.Ok);	//TODO: Move this logic to the PhoneLineState class.
        }

        //Video not support in this phase
        //	public void flipVideoImage( bool flip ) 
        //	{
        //		mSipApi.flipVideoImage(flip);
        //	}

        public Boolean isCallEncrypted(int callId)
        {
            return mSipApi.isCallEncrypted(callId);
        }

        public bool hasPendingCalls()
        {
            return (mActiveCall != null);
        }

        public void closePendingCalls()
        {
            if (mActiveCall != null)
            {
                mActiveCall.close();
            }
        }

        private void OnSipEvent(object sender, SipEventArgs e)
        {
			//Let's not log 'LOG' events, since they get logged completely below.
			if ( !e.Data.isLogEvent() )
			{ 
				String msg = ( e.Data.isInfoRequestEvent() ? " - " + e.Data.Content : "" );
				sipAgentLogger.Info("Sip event type: " + e.Data.typeAsString() + msg );
			}

			//TODO: ConnectEvent and DisconnectEvent are already covered by LineStateChangeEvent, 
			//	so we can/should simply by removing those here and in SIP stack.
            if (e.Data.isConnectEvent())
            {
                logger.Debug("SipEvent: Connect");
                mSipAccount.IsConnected = true;
				handleLineConnection( true );
            }
            else if (e.Data.isDisconnectEvent())
            {
                logger.Debug("SipEvent: Disconnect");
                mSipAccount.IsConnected = false;
				handleLineConnection( false );
            }
            else if (e.Data.isLineStateChangeEvent())
            {
                if (e.Data.LineId == mLineId)
                {
                    setState(e.Data.LineState);
                }
            }
            else if (e.Data.isCallStateChangeEvent())
            {
                PhoneCall phoneCall;

				if (mPhoneCalls.TryGetValue(e.Data.CallId, out phoneCall))
                {
                    setPhoneCallState(e.Data.CallId, e.Data.CallState, phoneCall.PeerSipAddress, e.Data.CallStatusCode);
                }
                else
                {
                    if (e.Data.CallFrom.Length > 0)
                    {
                        if (e.Data.CallState == PhoneCallState.CallState.Incoming)
                        {
                            setPhoneCallState(e.Data.CallId, e.Data.CallState, SipAddress.fromString(e.Data.CallFrom, mSipAccount.Realm), e.Data.CallStatusCode);
                        }
                    }
                }
            }
			else if ( e.Data.isInfoRequestEvent() )
			{
				if ( !String.IsNullOrEmpty( e.Data.Content ) )
				{
					String content     = e.Data.Content;
					String contentType = e.Data.ContentType;

					SipInfo sipInfo = new SipInfo( content, contentType );

					if ( sipInfo.isCallRecording() )
					{
						Boolean state = sipInfo.getCallRecordingValue();
                        Messenger.Default.Send<CallRecordingMessage>(new CallRecordingMessage { IsRecording = state });
					}
					else if ( sipInfo.isHttpListenerStarted() )
					{
						String listenerId = sipInfo.getHttpListernerIdValue();

						//Now we just need to add the ListenerId to the appropriate phoneCall
						PhoneCall phoneCall;

						if (mPhoneCalls.TryGetValue(e.Data.CallId, out phoneCall))
						{
							phoneCall.HttpListenerId = listenerId;
						}
						else
						{
							logger.Warn("SipEvent: Could not get PhoneCall for callId:" + e.Data.CallId.ToString() );
						}
					}

				}
			}
            else if (e.Data.isLogEvent())
            {
				String preamble = "SipEvent: Log - ";

				switch ( e.Data.LogEntry.Level )
				{
				case LogEntry.LogLevel.Fatal:
					sipAgentLogger.Fatal( preamble + e.Data.LogEntry.Message);
					break;
				case LogEntry.LogLevel.Error:
					sipAgentLogger.Error( preamble + e.Data.LogEntry.Message);
					break;
				case LogEntry.LogLevel.Warn:
					sipAgentLogger.Warn( preamble + e.Data.LogEntry.Message);
					break;
				case LogEntry.LogLevel.Info:
					sipAgentLogger.Info( preamble + e.Data.LogEntry.Message);
					break;
				case LogEntry.LogLevel.Debug:
					sipAgentLogger.Debug( preamble + e.Data.LogEntry.Message);
					break;
				case LogEntry.LogLevel.Verbose:
					sipAgentLogger.Debug( "VERBOSE " + preamble + e.Data.LogEntry.Message);	//Logger does not support Verbose, so fake it.
					break;
				default:
					sipAgentLogger.Debug( preamble + e.Data.LogEntry.Message);
					break;
				}
            }
            else
            {
                sipAgentLogger.Error("Unknown SIP Event " + e.Data.LogEntry.Message);	//New event type?
            }
        }

		private void handleLineConnection( bool connected )
		{
			GalaSoft.MvvmLight.Messaging.Messenger.Default.Send<ConnectionMessage>( ConnectionMessage.makeSip( connected ) );
			
			if ( !connected )
			{ 
				if ( mPhoneCalls.Count > 0  )
				{ 
	                NetworkStatus.Instance.ShowNetworkErrorWithCallsMessage();	//Show error message
					CloseAllPhoneCalls();										//Hangup all existing phone calls.
				}
			}
		}

        /// <summary>
        /// Play Dtmf char tone on the current call. Also play the sound locally.	//TODO-PJSIP: Does not play locally.
        /// </summary>
        /// <param name="obj"></param>
        private void OnPlayDtmfOnCallMessage( PlayDtmfOnCallMessage msg )
        {
            if (mActiveCall != null && mActiveCall.getState() == PhoneCallState.CallState.Talking)
            {
                mSipApi.playDtmf(mActiveCall.CallId, msg.DtmfChar);
            }
        }

        public void CloseAllPhoneCalls()
        {
            foreach (KeyValuePair<Int32, PhoneCall> kv in mPhoneCalls)
            {
                closeCall(kv.Value.CallId);
            }
        }

        //bool PhoneLines::handleVideoFrameReceived( int callId, piximage* remoteVideoFrame, piximage* localVideoFrame)
        //{
        //	bool result = false;

        //	lock();

        //	for (unsigned i = 0; i < size(); i++) 
        //	{
        //		IPhoneLine* line = (*this)[i];
        //		PhoneCall*  call = line->getPhoneCall(callId);

        //		if (call) 
        //		{
        //			//The correct PhoneCall has been found given its callId
        //			call->videoFrameReceived( remoteVideoFrame, localVideoFrame );
        //			result = true;
        //			break;
        //		}
        //	}

        //	unlock();

        //	return result;
        //}

        //=============================================================================}
    }	//class PhoneLine

}	//namespace Desktop.Model.PhoneLine

