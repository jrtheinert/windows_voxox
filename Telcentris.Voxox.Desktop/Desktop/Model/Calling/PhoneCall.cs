﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Oct 2014
 */

using Desktop.Utils;											//Logger

using Telcentris.Voxox.ManagedSipAgentApi.ManagedSipDataTypes;	//PhoneCallState

using GalaSoft.MvvmLight.Threading;
using GalaSoft.MvvmLight.Messaging;

using System;
using System.Threading;

namespace Desktop.Model.Calling
{
    public class PhoneCall
    {
        private static VXLogger logger = VXLoggingManager.INSTANCE.GetLogger("PhoneCall");

        private PhoneLine				 mPhoneLine;		// PhoneLine associated with this PhoneCall.
        private Int32					 mCallId;			// Call id of this PhoneCall.
        private SipAddress				 mSipAddress;		// Caller/callee/peer SIP address.
        private PhoneCallState.CallState mState;			// Current state of this PhoneCall.		//In old app, this was a class
        private Int32					 mStatusCode;		// SIP status/error code.
        private Int32					 mDbCallId;			// Database Call id of this call
        private string					 mSipCallId;		// From SIP stack
		private string					 mConferenceId;	
		private string					 mHttpListenerId;	//From SIP INFO via Http Listener calls.

        //	typedef List <PhoneCallState*>		PhoneCallStates;				// Defines the vector of PhoneCallState.
        //
        private PhoneCallStateChangeHandler mStateChangeHandler;
        private Int64	mDuration;				// Call duration in seconds.
        private Boolean mHoldRequest;			// If the PhoneCall should be held.
        private Boolean mResumeRequest;			// If the PhoneCall should be resumed.
        private Boolean mCallRejected;			// If this PhoneCall has been rejected as an incoming call.
        private Int64	mTimeStart;				// Computes the PhoneCall duration.
//		private Boolean mVideoEnabled;			// If this call is a video call or an audio call.


        public PhoneCall(PhoneLine phoneLine, SipAddress sipAddress)
        {
            mPhoneLine	= phoneLine;
            mSipAddress = sipAddress;

            mStateChangeHandler = new PhoneCallStateChangeHandler();

            mState			= PhoneCallState.CallState.Unknown;
            mDuration		= -1;
            mHoldRequest	= false;
            mResumeRequest	= false;
            mCallRejected	= false;
            mTimeStart		= -1;
            mStatusCode		= 0;
//			mVideoEnabled	= false;
        }

        ~PhoneCall()
        {
        }

        #region Public properties

        public Int32 StatusCode
        {
            get { return mStatusCode; }
            set { mStatusCode = value; }
        }

        public Int32 CallId
        {
            get { return mCallId; }
            set { mCallId = value; }
        }

        public SipAddress PeerSipAddress
        {
            get { return mSipAddress; }
        }

        public int DbCallId
        {
            get { return mDbCallId; }
            set { mDbCallId = value; }
        }

        public string SIPCallId
        {
            get { return mSipCallId; }
            set { mSipCallId = value; }
        }

        public string ConferenceId
        {
            get { return mConferenceId;	 }
            set { mConferenceId = value; }
        }

		public string HttpListenerId
		{
			get { return mHttpListenerId;	}
			set { mHttpListenerId = value;	}
		}

		public Boolean IsMerging		{ get; set; }

        #endregion

        public void accept()
        {
            PhoneCallStateChangeHandler.stopSoundIncomingCall();
            mPhoneLine.acceptCall(mCallId);
        }

        public void resume()
        {
            if (mState == PhoneCallState.CallState.Hold)
            {
                mResumeRequest = false;
                mPhoneLine.resumeCall(mCallId);
            }
            else
            {
                mResumeRequest = true;
            }
        }

        public void hold()
        {
            if (mState == PhoneCallState.CallState.Talking ||
                 mState == PhoneCallState.CallState.Resumed)
            {
                mHoldRequest = false;
                mPhoneLine.holdCall(mCallId);
            }
            else
            {
                mHoldRequest = true;
            }
        }

        public void blindTransfer(String sipAddress)
        {
            mPhoneLine.blindTransfer(mCallId, sipAddress);
        }


        public PhoneCallState.CallState getState()
        {
            return mState;
        }

		//SIP CallId is in the local SIP stack, but we are waiting for server-client work to get SIP CallId
		//	In most/all cases, HttpListenerHelper SHOULD use HttpListenerID (below).  We will retain this for now.
		public void retrieveSipCallId(Action<string> onDone)
		{
			if (string.IsNullOrWhiteSpace(SIPCallId))
			{
				Thread tempThread = new Thread((object callId) =>
				{
					DispatcherHelper.CheckBeginInvokeOnUI(() =>
					{
						try
						{
							int		maxWait		  = 2000;		//In ms
							int		waitIncrement =    1;		//In ms (on incoming calls, we get it immediately).
							int		totalWait	  =    0;

							while ( totalWait < maxWait && String.IsNullOrWhiteSpace(SIPCallId) )
							{ 
								SIPCallId = SessionManager.Instance.GetSipCallId( Convert.ToInt32(callId) );
			
								System.Threading.Thread.Sleep(waitIncrement);
								totalWait += waitIncrement;
							}

							if ( String.IsNullOrWhiteSpace(SIPCallId) )
							{
								logger.Error( "retrieveSipCallId - SIPCallId is invalid" );
							}
							else 
							{ 
								logger.Info( "retrieveSipCallId: Time to get SIPCallId: " + Convert.ToString( totalWait ) );
							}

							if (onDone != null)
							{
								onDone(SIPCallId);
							}
						}
						catch(Exception ex)
						{
							logger.Error("retrieveSipCallId", ex);
						}
					});
				});

				tempThread.Start(CallId);
			}
			else
			{
				if (onDone != null)
				{
					onDone(SIPCallId);
				}
			}
		}

		//HttpListenerId will be provided via SIP INFO after call is accepted, but before we call NotifyHttpListenersHelper::AcceptCall
		//	For outgoing call, httpListenerId and SipCallId are the same.  For incoming calls, they differ.
		public void retrieveHttpListenerId(Action<string> onDone)
		{
			if (string.IsNullOrWhiteSpace(HttpListenerId))
			{
				Thread tempThread = new Thread((object callId) =>
				{
					try
					{
						int		maxWait		  = 2000;		//In ms
						int		waitIncrement =   10;		//In ms
						int		totalWait	  =    0;

						while ( totalWait < maxWait && String.IsNullOrWhiteSpace(HttpListenerId) )
						{ 
							HttpListenerId = mPhoneLine.getHttpListenerId( Convert.ToInt32(callId) );
			
							System.Threading.Thread.Sleep(waitIncrement);
							totalWait += waitIncrement;
						}

						if ( String.IsNullOrWhiteSpace(HttpListenerId) )
						{
							logger.Error( "retrieveHttpListenerId - HttpListenerId is invalid" );
						}
						else 
						{ 
							logger.Info( "retrieveHttpListenerId: Time to get HttpListenerId: " + Convert.ToString( totalWait ) );
						}

						if (onDone != null)
						{
                            DispatcherHelper.CheckBeginInvokeOnUI(() =>
                            {
                                onDone(HttpListenerId);
                            });

						}
					}
					catch(Exception ex)
					{
						logger.Error("retrieveHttpListenerId", ex);
					}
				});

				tempThread.Start(CallId);
			}
			else
			{
				if (onDone != null)
				{
					onDone(HttpListenerId);
				}
			}
		}

        public void setState( PhoneCallState.CallState state )
        {
            logger.Debug("PhoneCallState = " + PhoneCallState.toString(state));

            if (mState != state)
            {
                mState = state;

                Boolean secondValue = false;

                if (mState == PhoneCallState.CallState.Incoming)
                {
                    secondValue = mPhoneLine.hasPendingCalls();
                }
				else if ( mState == PhoneCallState.CallState.Closed )
				{
					secondValue = ! IsMerging;	//Do not play hangup sound if we are merging calls.
				}

                mStateChangeHandler.execute(mState, this, secondValue);

                logger.Debug("call state changed callId=" + mCallId.ToString() + " state=" + PhoneCallState.toString(state));
                applyState(state);
                Messenger.Default.Send<PhoneCallMessage>(new PhoneCallMessage { CallId = mCallId, State = state, Call = this });
            }
        }


        private void applyState(PhoneCallState.CallState state)
        {
            //mVideoEnabled = getVideoCodecUsed().empty();	//TODO-VIDEO

            //This should not replace the state machine pattern PhoneCallState
            switch (state)
            {
                case PhoneCallState.CallState.Unknown:
                    break;

                case PhoneCallState.CallState.Error:
                    break;

                case PhoneCallState.CallState.Resumed:
                    break;

                case PhoneCallState.CallState.Talking:
                    if (mHoldRequest)
                    {
                        hold();
                    }
                    else
                    {
                        mTimeStart = getNowAsSeconds();		//Start of the call, computes duration
                    }
                    break;

                case PhoneCallState.CallState.Dialing:
                    break;

                case PhoneCallState.CallState.Ringing:
                    break;

                case PhoneCallState.CallState.Closed:
                    if (mTimeStart != -1) 	//End of the call, computes duration
                    {
                        mDuration = getNowAsSeconds() - mTimeStart;
                    }

                    if (!mCallRejected)
                    {
                        //Call missed if incoming state + closed state without being rejected
                        setState(PhoneCallState.CallState.Missed);
                    }
                    break;

                case PhoneCallState.CallState.Incoming:
                    break;

                case PhoneCallState.CallState.Hold:
                    if (mResumeRequest)
                    {
                        resume();
                    }
                    break;

                case PhoneCallState.CallState.Missed:
                    break;

                case PhoneCallState.CallState.Redirected:
                    break;

                case PhoneCallState.CallState.RingingStart:
                    break;

                case PhoneCallState.CallState.RingingStop:
                    break;

                default:
                    logger.Fatal("unknown PhoneCallState=" + PhoneCallState.toString(state));
                    break;
            }
        }


        public void close()
        {
            if (mState != PhoneCallState.CallState.Closed)
            {
                if (mState == PhoneCallState.CallState.Incoming)
                {
                    mCallRejected = true;
                    mPhoneLine.rejectCall(mCallId);
                }
                else if (mState != PhoneCallState.CallState.Error)
                {
                    mPhoneLine.closeCall(mCallId);
                }

                //		setState( PhoneCallState.CallState.Closed);		//Let's wait for signal from SIP stack to handle this.
            }
        }


        public void playSoundFile(String soundFile) //For DTMF tones?
        {
            mPhoneLine.playSoundFile(mCallId, soundFile);
        }

		//public String getAudioCodecUsed()
		//{
		//	return mPhoneLine.getAudioCodecUsed(mCallId);
		//}

        //Video not supported at this time.
        //	public String getVideoCodecUsed() 
        //	{
        //		return mPhoneLine.getVideoCodecUsed( mCallId );
        //	}

        public bool isCallEncrypted()
        {
            return mPhoneLine.isCallEncrypted(mCallId);
        }

        public Int64 getRunningDurationAsSeconds()
        {
            return getNowAsSeconds() - mTimeStart;
        }

        private Int64 getNowAsSeconds()
        {
            return System.DateTime.Now.Ticks / 10000000;	//Ten million ticks per secons
        }

        //Video not supported.  This will have to be completely re-done.
        //	public void videoFrameReceived( piximage * remoteVideoFrame, piximage * localVideoFrame ) 
        //	{
        //		videoFrameReceivedEvent(*this, remoteVideoFrame, localVideoFrame);
        //	}

    }	//class PhoneCall

    //-----------------------------------------------------------------------------

    public class PhoneCalls : System.Collections.Generic.Dictionary<Int32, PhoneCall>
    {
		public void clearConferenceId( string conferenceId )
		{
			if ( !String.IsNullOrEmpty( conferenceId ) )
			{ 
				foreach ( System.Collections.Generic.KeyValuePair<Int32, PhoneCall> kv in this )
				{
					if  ( kv.Value.ConferenceId == conferenceId )
					{
						kv.Value.ConferenceId = "";
					}
				}
			}
		}
    }

}	//namespace Desktop.Model.Calling

