﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Oct 2014
 */
using Desktop.Utils;			//Logger
using Desktop.ViewModels;		//IncomingEventMessage

using Telcentris.Voxox.ManagedSipAgentApi.ManagedSipDataTypes;	//AudioDevice, PhoneCallState

using System;

namespace Desktop.Model.Calling
{

class PhoneCallStateChangeHandler
{
    private static VXLogger logger = VXLoggingManager.INSTANCE.GetLogger("PhoneCallStateChangeHandler");
	private static Sound mSoundIncomingCall = null;
	private static Sound mSoundCallClosed   = null;

	public PhoneCallStateChangeHandler()
	{
	}


	//These are used during some of the execute() calls.
	public static void stopSoundIncomingCall()
	{
		if ( mSoundIncomingCall != null ) 
		{
			mSoundIncomingCall.stop();

//			if ( !mSoundIncomingCall.isDestroying() )
			{
				mSoundIncomingCall = null;
			}
		}
	}

	private static void stopSoundCallClosed()
	{
		if ( mSoundCallClosed != null ) 
		{
			mSoundCallClosed.stop();

//			if ( !mSoundCallClosed.isDestroying() )
			{
				mSoundCallClosed = null;
			}
		}
	}

	private static String getSoundIncomingCallFile()
	{
        return SettingsManager.Instance.User.IncomingCallSoundFile;
	}

	private static AudioDevice getRingerAudioDevice()
	{
		AudioDevice result = new AudioDevice();
//		return ConfigManager::current().getAudioIncomingCallFile();		//TODO-CONFIG
		return result;
	}

	private	static String getSoundCallClosedFile()
	{
        return SettingsManager.Instance.User.CallClosedSoundFile;
	}


//	private static String getSoundDoubleCallFile()
//	{
//		return ConfigManager::current().getAudioDoubleCallFile();
//	}

	private static String getSoundRingingFile()
	{
        return SettingsManager.Instance.User.OutgoingCallSoundFile;
	}


	//Handles the various state change handling
	public void execute( PhoneCallState.CallState state, PhoneCall phoneCall, Boolean other )
	{
		switch ( state )
		{
		case PhoneCallState.CallState.Closed:
			handleStateClosed( other );
			break;

		case PhoneCallState.CallState.Dialing:
			//Do nothing.
			break;

		case PhoneCallState.CallState.Error:
			handleStateError();
			break;

		case PhoneCallState.CallState.Hold:
			//Do nothing.
			break;

		case PhoneCallState.CallState.Incoming:
			handleStateIncoming();
			break;

		case PhoneCallState.CallState.Missed:
			handleMissedCall();
			break;

		case PhoneCallState.CallState.Resumed:
			handleStateResumed();
			break;

		case PhoneCallState.CallState.Ringing:
			handleStateRinging();
			break;

		case PhoneCallState.CallState.RingingStart:
			handleStateRingingStart();
			break;

		case PhoneCallState.CallState.RingingStop:
			handleStateRingingStop();
			break;

		case PhoneCallState.CallState.Talking:
			handleStateTalking();
			break;

		case PhoneCallState.CallState.Unknown:
			//Do nothing
			break;

//		default:
			//TODO-LOG error for unexpected value
		}
	}

	private void handleStateClosed( bool playHangup )
	{
		stopSoundIncomingCall();

		if ( playHangup )
		{
			//Call closed tonality
            try
            {
                mSoundCallClosed = new Sound(getSoundCallClosedFile());
                mSoundCallClosed.Device = getRingerAudioDevice();

                //Play the sound 4 times
                mSoundCallClosed.LoopCount = 4;
                mSoundCallClosed.play();
            }
            catch(Exception ex)
            {
                logger.Error("Error playing hangup tone", ex);
            }
		}
	}

	private void handleStateError()
	{
		stopSoundIncomingCall();
	}

	private void handleStateIncoming()
	{
//		Config& config = ConfigManager::current();				//TODO-CONFIG

//		if ( config.getNotificationPlaySoundOnIncomingCall() )
//		{
//			if ( config.getAudioRingingEnable() ) 
//			{
				//Ring-in tonality
//				if(doublecall)
//				{
//					_soundIncomingCall = new Sound(getSoundDoubleCallFile());
//				}
//				else
				{
					mSoundIncomingCall = new Sound( getSoundIncomingCallFile() );
				}

				mSoundIncomingCall.Device = getRingerAudioDevice();
				mSoundIncomingCall.LoopCount = -1;			//Play the sound indefinitely
                try
                {
                    if(SettingsManager.Instance.User.EnableSoundForIncomingCall)
						mSoundIncomingCall.play();
                }
                catch(Exception ex)
                {
                    logger.Error("Error playing incoming tone", ex);
                }
//			}
//		}
	}

	private void handleStateResumed()
	{
		stopSoundIncomingCall();
	}

	private void handleStateRinging()
	{
        if ( SettingsManager.Instance.User.EnableSoundForIncomingCall )
		{
			//Ringing tonality
			if ( mSoundIncomingCall == null )
			{
				mSoundIncomingCall = new Sound( getSoundRingingFile() );

				mSoundIncomingCall.Device = getRingerAudioDevice();
				mSoundIncomingCall.LoopCount = -1;		//Play the sound indefinitely

                try
                {
                    mSoundIncomingCall.play();
                }
                catch(Exception ex)
                {
                    logger.Error("Error playing ringing tone", ex);
                }
			}
		}
	}

	private void handleStateRingingStart()
	{
        if (SettingsManager.Instance.User.EnableSoundForIncomingCall)
		{
			//Ringing tonality
			if ( mSoundIncomingCall == null )
			{
				mSoundIncomingCall = new Sound( getSoundRingingFile() );

				mSoundIncomingCall.Device = getRingerAudioDevice();
				mSoundIncomingCall.LoopCount = -1;			//Play the sound indefinitely
                try
                {
                    mSoundIncomingCall.play();
                }
                catch( Exception ex)
                {
                    logger.Error("Error playing ringing start tone", ex);
                }
			}
		}
	}

	private void handleStateRingingStop()
	{
		stopSoundIncomingCall();
	}

	private void handleStateTalking()
	{
		stopSoundIncomingCall();
	}

	private void handleMissedCall()
	{
		//Notify UI/UX for user notification
		GalaSoft.MvvmLight.Messaging.Messenger.Default.Send<IncomingEventMessage>( new IncomingEventMessage{ Type = IncomingEventMessage.EventType.MissedCall } );
	}

}	//class PhoneCallStateChangeHandler



}	//namespace Desktop.Model.Calling

