﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

//TODO: Move all these internal messages to a common class

namespace Desktop.Model.Calling
{
    public class CallRecordingMessage
    {
        public bool IsRecording { get; set; }
    }
}
