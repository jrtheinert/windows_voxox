﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Oct 2014
 */

using Telcentris.Voxox.ManagedSipAgentApi.ManagedSipDataTypes;	//PhoneLineState

namespace Desktop.Model.Calling
{

class PhoneLineStateChangeHandler
{
	public PhoneLineStateChangeHandler()
	{
	}

	public void execute( PhoneLine phoneLine, PhoneLineState.LineState state )
	{
		switch ( state )
		{
		case PhoneLineState.LineState.AuthError:
			phoneLine.getSipAccount().phoneLineAuthenticationError();
			break;

		case PhoneLineState.LineState.Closed:
		case PhoneLineState.LineState.Progress:
		case PhoneLineState.LineState.ServerError:
		case PhoneLineState.LineState.Unknown:
			phoneLine.getSipAccount().IsConnected = false;
			break;

		case PhoneLineState.LineState.Ok:
			//Do nothing.  Old app could make call from command-line but we are not supporting that yet.
			break;

		case PhoneLineState.LineState.Timeout:
			phoneLine.getSipAccount().IsConnected = false;

			// Restart connection.
			phoneLine.disconnect(true);
			phoneLine.connect();
			break;

//		default:
//		TODO-LOG unexpected state
		}
	}

}	//class PhoneLineStateChangeHandler


}	//namespace Desktop.Model.Calling

