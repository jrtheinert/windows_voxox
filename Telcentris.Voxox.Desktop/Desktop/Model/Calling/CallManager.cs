﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

// Class to handle Call related DB, API and other issues
// Some of this code WAS in MessageManager, but I have moved it here for consistency.

using Desktop.Util;			//TimeUtils
using Desktop.Utils;		//Logger

using ManagedDataTypes;		//RecentCall

using Vxapi23;				//RestfulApiManager

using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Desktop.Model.Calling
{
	class CallManager
	{
		private static CallManager mInstance = null;
		private static VXLogger	   mLogger    = VXLoggingManager.INSTANCE.GetLogger("CallManager");

		static public CallManager getInstance()
		{
			if ( mInstance == null )
			{
				mInstance = new CallManager();
			}

			return mInstance;
		}

		private CallManager()
		{
		}

		private ManagedDbManager.ManagedDbManager getUserDbm()
		{
			return SessionManager.Instance.UserDataStore;
		}

		/**
		 * Description:
		 * RESTful Server API getCallHistory() : used to retrieve all calls.
		 * if time-stamp is NOT null, will retrieve all calls newer than time-stamp
		 * */
		public void retrieveCallHistory()
		{
            if (SessionManager.Instance.User == null)
                return;

			retrieveCallHistory( SessionManager.Instance.User.UserKey,  
								 SessionManager.Instance.User.CompanyUserId, 
								 Desktop.Config.API.DefaultNumberOfCallsFromServer );
		}

		private void retrieveCallHistory( string userKey, string userCompanyUserId, Int32 numberOfCallsToRetrieve )
		{
            try
            {
                Int64 timestamp = getTimeStampToRetrieveCalls();

                RecentCallList callList = CallHistory.getCalls(timestamp, userKey, userCompanyUserId, numberOfCallsToRetrieve);

                if (callList != null && getUserDbm() != null)
                {
                    SessionManager.Instance.LastCallTimestamp = timestamp;

                    mLogger.Info("Clearing calls marked with local flag");
                    getUserDbm().clearLocalCalls();

                    mLogger.Info("Add/Update calls in database");
                    getUserDbm().addOrUpdateCalls(callList);

                    var lastTimestamp = callList.getMostRecentStartTimestamp();

                    if (lastTimestamp > SessionManager.Instance.LastCallTimestamp)
                        SessionManager.Instance.LastCallTimestamp = lastTimestamp;
                }
            }
			catch(Exception ex)
            {
                // If user clicks on signout then Db reference before completing 
                //this process Null pointer exception may come
                mLogger.Error("Error in retrive call history " + ex);
            }
		}

		private long getTimeStampToRetrieveCalls() 
		{
			Int64 timestamp = 0;

            if (getUserDbm() != null)
			{ 
				//NOTE: We cannot just use LastCallTimestamp without considering the state of the DB table.
				//	This is because:
				//		- During dev/testing, we may manually delete the DB without uninstalling the app.
				//		- End-user may manually delete the DB which would likely trigger a support call
				//			when messages are not properly populated.
				//	So, to avoid issues, we will ask the DB if it has any RecentCall table entries.
				//		If not, then we will NOT use the LastCallTimestamp.
				//		It is unclear if we have to manually override that value or if it will
				//		be handled normally.  I think normally.
				Int64 timestamp1 = getUserDbm().getLatestCallServerTimestamp();
				Int64 timestamp2 = SessionManager.Instance.LastCallTimestamp;

				if ( timestamp1 > 0 )
				{
					//Indicates we have Messages, but may not be the *proper* value, since messages may have been deleted.
					//	Check SessionManager and use greater value.
					timestamp = (timestamp2 > 0 ? timestamp2 : timestamp1 );
				}
				else
				{
					//Since we have no DB value, we must assume DB/Table has been deleted which means we make maximum query
					timestamp = 0;
				}

				if (timestamp > 0) 
				{
					// WORKAROUND add a whole second to the time stamp as we use getMessageHistory() API which uses FULL seconds in the message time stamp. 
					//	If we will use the actual message time stamp it will retrieve it again.
					return (timestamp + 1000);
				} 
			}

			if ( timestamp == 0 )
				timestamp = ManagedDataTypes.VxTimestamp.GetServerTimestampSomeDaysBack( 30 );		//TODO: move literal to SettingsManager


			return timestamp;
		}

		//This is called from client when user views a Call Thread.
		public int markCallsSeenWhenViewed( RecentCallList callList ) 
		{
            if (getUserDbm() == null)
                return 0;

			List<String> uids = new List<String>();
			int markedCount = 0;

			// - Local portion - Use foreach loop.
			// - Server portion - Gather UIDS to make intelligent API call(s).
			foreach (var call in callList)
			{
				bool markCallAsSeen = false;

				if ( call.canBeMarkedSeen() )
				{
					//Always mark these a SEEN once Call Thread is displayed
					markCallAsSeen = !call.hasBeenSeen();
				}

				if ( markCallAsSeen )
				{
					if ( call.canBeMarkedSeen() )
					{ 
						//This may fail due to invalid UID of we have server related issue.
						if ( markCallSeenLocally( call.Uid, false ) )
						{ 
							uids.Add( call.Uid );

							markedCount++;
						}
					}
				}

			}	//foreach

			markCallsSeenOnServer( uids );

			if ( markedCount > 0 )
			{
				getUserDbm().broadcastRecentCallDatasetChanged();
			}

			return markedCount;
		}

		private bool markCallSeenLocally( String uid, bool broadcast ) 
		{
			bool result = false;

            if (getUserDbm() == null)
                return result;

			RecentCall   call = getUserDbm().getRecentCallByUid( uid );

			if ( call != null && call.canBeMarkedSeen() ) 
			{
				result = getUserDbm().updateRecentCallSeen( call.RecordId );
			}

			return result;
		}

		private void markCallsSeenOnServer( List<String> uids )
		{
			//Parallelism allows us to execute 270 calls in 8 seconds or about .03 seconds/call.
			//	Each API call actually takes about 0.5 seconds.
//			ParallelOptions options = new ParallelOptions();
//			options.MaxDegreeOfParallelism = 2;

			//StartNew() does this async'ly.
//			Task.Factory.StartNew( () => Parallel.ForEach<String>( uids, options, uid => markCallSeenOnServer( uid ) ) );
			Task.Factory.StartNew( () => Parallel.ForEach<String>( uids, uid => markCallSeenOnServer( uid ) ) );
//			Parallel.ForEach<String>( uids, uid => markCallSeenOnServer( uid ) );		//Results in non-responsive app.
		}

		//TODO: implement BATCH method for better server efficiency
		private async void markCallSeenOnServer( String uid ) 
		{
            if (SessionManager.Instance.User == null)
                return;

			Vxapi23.Model.UpdateCallSeenFlagResponse result = null;

			result =  await RestfulAPIManager.INSTANCE.UpdateCallSeenFlag( SessionManager.Instance.User.UserKey, SessionManager.Instance.User.CompanyUserId,
																			uid, (int)RecentCall.SeenFlag.Seen, true );

			if ( result != null && !result.Success )
			{
				//TODO: update local DB?
//				int xxx = 1;
			}
	
		}

        public async Task<bool> setCallerId ( string callerIdIn, bool singleUse )
        {
			//TODO: Move all this to ActionManager?
			bool result = false;

            if ( SessionManager.Instance.User != null )
			{
				Vxapi23.Model.SetCallerIdResponse response = null;

				String callerId = PhoneUtils.CleanPhoneNumberFormatting( callerIdIn );

				//TODO: This should use standard 'setStandardParms' (likely not proper term)
				response = await RestfulAPIManager.INSTANCE.SetCallerId( SessionManager.Instance.User.UserKey, SessionManager.Instance.User.CompanyUserId,
																		 callerId, singleUse, true);

				result = ( response != null && response.Success );
			}

			return result;
        }

	}	//CallManger
}
