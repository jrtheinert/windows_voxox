﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Oct 2014
 */

using System;
using System.Net;	//For URL decode

namespace Desktop.Model.Calling
{

public class SipAddress
{
	//These are used in parse()
	static readonly	String LT		 = "<";
	static readonly String GT		 = ">";
	static readonly String SIP		 = "sip:";
	static readonly String AT        = "@";
	static readonly String BACKSLASH = "\"";
	static readonly String DBL_QUOTE = "\"";
//	static readonly String SPACE	 = " ";

	//These are used in fromString()
	static readonly	String specialChars         = " .,;:()[]{}-_/#+0123456789";
	static readonly String specialCharsToRemove = " .,;:()[]{}-_/#";

	private String	mRawSipAddress;
	private String	mSipAddress;
	private String	mDisplayName;
	private String	mUserName;

	// Constructs a SipAddress given a unmodified complete string raw SIP address.
	SipAddress( String rawSipAddress )
	{
		mRawSipAddress = rawSipAddress;
		parse( rawSipAddress );
	}

//	~SipAddress()
//	{
//	}

	// Gets the unmodified SIP address.
	public String getRawSipAddress()
	{ 
		return mRawSipAddress;	
	}

	// Gets the simplified SIP address.
	public String getSipAddress()
	{ 
		return mSipAddress;		
	}

	// Gets the username part of the SIP address.
	public String getUserName()
	{ 
		return mUserName;			
	}

	// Gets the diplay name part of the SIP address.
	public String getDisplayName()
	{ 
		return mDisplayName;		
	}

	// Gets a human readable representation of this SIP address.
	public String toString()
	{
		if ( !String.IsNullOrEmpty( getDisplayName() ) )
		{
			return getDisplayName();
		}

		return getUserName();
	}

	//	Creates a SipAddress from a unknown string
	public static SipAddress fromString( String str, String realm )
	{
		String sipUri = str;
		Int32  length = sipUri.LastIndexOfAny( specialChars.ToCharArray() );
	
		if ( length == sipUri.Length ) 
		{
			//sipUri is a real phone number not a SIP URI or a pseudo
			for ( int x = 0; x < specialCharsToRemove.Length; x++ )
			{
				String toBeReplaced = specialCharsToRemove[x].ToString();
				sipUri.Replace( toBeReplaced, "" );
			}
		}

		//Prepend 'sip:', if needed
		if ( !sipUri.StartsWith( SIP ) )
		{
			sipUri = SIP + sipUri;
		}

		//If no '@' char, append realm
		if ( !sipUri.Contains( AT ) )
		{
			sipUri += AT + realm;		//Not a SIP URI
		}

		return new SipAddress( sipUri );
	}
		
	// Parses the from (SIP address) field.
	private void parse( String rawSipAddressIn )
	{
		//Known input possibilities:
		//	"jwagner_office" <sip:0170187873@192.168.70.20;user=phone>;tag=00-01430
		//	sip:"THEINERT JEFF  " <sip:+17602775395@voxox.com>
		//	sip:+17602775395@voxox.com (seems to be for outgoing off-net calls)

		String tempDisplayName;
		String tempSipAddress;

		//Split input into two strings based on "sip:".
		//	If sip: is at start, use the second occurence, which is why we start at pos 1 vs. 0
		String SipKey = LT + SIP;	//"<sip:"
		Int32 posSipAddressBegin = rawSipAddressIn.IndexOf( SipKey, 1 );
		Int32 posSipAddressEnd   = -1;

		if (  posSipAddressBegin == -1 )
		{
			//Did not find '<sip:', so try just 'sip:'
			posSipAddressBegin = rawSipAddressIn.IndexOf( SIP );
		}

		//We found it, so do the split
		if ( posSipAddressBegin != -1 ) 
		{
			tempDisplayName = rawSipAddressIn.Substring( 0, posSipAddressBegin );
			tempSipAddress  = rawSipAddressIn.Substring( posSipAddressBegin    );

			//Cleanup SIP Address, by removing leading < and trailing >
			if ( !String.IsNullOrEmpty( tempSipAddress ) ) 
			{
				posSipAddressBegin = SipKey.Length;		
	
				if ( posSipAddressBegin != -1 ) 
				{
					posSipAddressEnd = tempSipAddress.IndexOf( GT );
		
					if ( posSipAddressEnd != -1 ) 
					{
						mSipAddress = tempSipAddress.Substring( posSipAddressBegin, posSipAddressEnd - posSipAddressBegin );
					}
					else
					{
						mSipAddress = tempSipAddress;
					}
					//RESULT: mSipAddress = sip:0170187873@192.168.70.20;user=phone
				}
			}

			//Cleanup UserName by removing domain and other decorations from mSipAddress
			if ( !String.IsNullOrEmpty( mSipAddress ) ) 
			{
				Int32 posUserNameBegin = mSipAddress.IndexOf( SIP );

				posUserNameBegin = ( posUserNameBegin == -1 ? 0 : posUserNameBegin + SIP.Length );
	
				if ( posUserNameBegin != -1 ) 
				{
					Int32 posUserNameEnd = mSipAddress.IndexOf( AT, posUserNameBegin );
		
					if ( posUserNameEnd != -1 ) 
					{
						mUserName = mSipAddress.Substring( posUserNameBegin, posUserNameEnd - posUserNameBegin );
						mUserName = WebUtility.UrlDecode( mUserName );
						//RESULT: _userName = 0170187873
					}
				}
			}

			//Cleanup DisplayName
			mDisplayName = "";

			if ( !String.IsNullOrEmpty( tempDisplayName ) ) 
			{
				//Find and remove any leading 'sip:' decoration.
				Int32 posDisplayNameBegin = tempDisplayName.IndexOf( SIP );

				if ( posDisplayNameBegin != -1 ) 
				{
					mDisplayName = tempDisplayName.Substring( posDisplayNameBegin + SIP.Length );
				}
				
				//Remove known spurious characters.
				mDisplayName = mDisplayName.Replace( BACKSLASH, "" );
				mDisplayName = mDisplayName.Replace( DBL_QUOTE, "" );
				mDisplayName = mDisplayName.Trim();
				//RESULT: mDisplayName = jwagner_office
			}
		}
		else
		{
			//This should never happen.
			mSipAddress  = rawSipAddressIn;
			mUserName	 = rawSipAddressIn;
			mDisplayName = "";
		}
	}
}	//class SipAddress


}	//namespace Desktop.Model.Calling

