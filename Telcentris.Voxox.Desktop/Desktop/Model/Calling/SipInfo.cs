﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using System;
using System.Collections.Generic;			//Dictionary
using System.Net;							//For URL decode
using System.Text.RegularExpressions;

//This class will handle parsing of and be a data container for, the data received within calls via the SIP INFO request.
//	The SIP INFO is received formatted like a URL query string (for various technical reasons).
//	Since .NET desktop does NOT support parsing that without including a lot of other libs, 
//	we will have our own low-end query string parser.
//
//Content currently looks like this:
//	code=101&message=Http listener started&httpListenerId=54caff24-00006aec-04cc70f5-570991db@68.105.121.14:61475
//
//Doc: https://voxoxrocks.atlassian.net/wiki/display/CALL/Sip+Info
//Notes:
//	- Use 'code' as primary 'function' determination
//	- 'message' is primarily used for debugging to make it easier to read
//	- Each 'code' may have unique 'extra' data elements, so be flexible.

namespace Desktop.Model.Calling
{
	class SipInfo
	{
		//Existing main keys
		static private readonly String s_codeKey    = "code";
		static private readonly String s_messageKey = "message";

		//Existing 'code' values:
		static private readonly String s_codeHttpListenerStarted = "101";
		static private readonly String s_codeOnCallMenu			 = "102";

		//Existing 'extra' keys
		static private readonly String s_extraKey_httpListenerId  = "httpListenerId";		//Code 101 - value 'string'
		static private readonly String s_extraKey_callRecordingOn = "callRecordingOn";		//Code 102 - value Boolean (0/1) denoting current recording state.


		private Dictionary<String, String> mKvp = new Dictionary<String, String>();	//Optionally use NameValueCollection()

		private String	mQueryString  = "";
		private String  mContentType  = "";
		private Boolean mIsUrlEncoded = false;	

		public SipInfo( String queryString, String contentType )
		{
			mQueryString = queryString;
			mContentType = contentType;

			mIsUrlEncoded = contentType.Contains( "urlencoded" );	//This logic may expand, but will do for now.
			
			parse();
		}

		private void parse()
		{
			String work = ( mIsUrlEncoded ? WebUtility.UrlDecode( mQueryString ) : mQueryString );

			foreach (string vp in Regex.Split( work, "&" ) )
			{
				string[] singlePair = Regex.Split( vp, "=" );

				if (singlePair.Length == 2)
				{
					mKvp.Add(singlePair[0], singlePair[1]);
				}
				else
				{
					// only one key with no value specified in query string
					mKvp.Add(singlePair[0], string.Empty);
				}
			}
		}

		private Boolean isCode( String tgtValue )
		{
			Boolean result = false;

			if ( mKvp.ContainsKey( s_codeKey ) )
			{
				result = ( mKvp[s_codeKey] == tgtValue );
			}

			return result;
		}

		private Boolean hasExtraKey( String tgtKey )
		{
			return mKvp.ContainsKey( tgtKey );
		}

		private String getExtraValue( String tgtKey )
		{
			String result = null;

			if ( hasExtraKey( tgtKey ) )
			{
				result = mKvp[tgtKey];
			}

			return result;
		}

		//Code-level info
		public Boolean isHttpListenerStarted()	{ return isCode( s_codeHttpListenerStarted );	}
		public Boolean isOnCallMenu()			{ return isCode( s_codeOnCallMenu		   );	}

		//Raw values in case needed for logging/debugging
		public String getCodeValue()			{ return getExtraValue( s_codeKey    ); 	}
		public String getMessageValue()			{ return getExtraValue( s_messageKey );		}

		//Extra level values
		public Boolean isCallRecording()
		{
			Boolean result = false;

			if ( isOnCallMenu() )
			{
				result = hasExtraKey( s_extraKey_callRecordingOn );
			}

			return result;
		}

		//TODO: this code currently relies on caller to do isCallRecording().  Maybe throw exception if they call it in appropriately.
		public String getHttpListernerIdValue()
		{
			String result = null;

			if ( isHttpListenerStarted() )
			{
				result = getExtraValue( s_extraKey_httpListenerId );
			}

			return result;
		}

		public Boolean getCallRecordingValue()
		{
			Boolean result = false;

			if ( isCallRecording() )
			{
				String temp = getExtraValue( s_extraKey_callRecordingOn );
				result = (temp != "0");
			}

			return result;
		}

	}	//class SipInfo
}	//namespace Desktop.Model.Calling

