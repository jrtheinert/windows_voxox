﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model.Calling;			//PhoneLine
using Desktop.Model.Messaging;			//XmppManager
using Desktop.Util;						//Mixpanel
using Desktop.Utils;					//Logger
using Desktop.ViewModels;				//BalanceChangedMessage, UpdateUnreadItemCountsMessage
using Desktop.ViewModels.Main;			//ActiveTabEnum
using Desktop.ViewModels.Messages;		//ShowOrHideVoicemailsMessage

using ManagedDbManager;					//DbChangedEventHandler, DbChangedEventArgs
using ManagedDataTypes;					//UnreadCounts, Contact

using Telcentris.Voxox.ManagedSipAgentApi.ManagedSipDataTypes;	//ServerInfo, SipAgent

using Vxapi23;							//RestfulApiManager
using Vxapi23.Model;					//AuthenticateResponse, DataSummaryResponse, GetInitOptionsResponse

using GalaSoft.MvvmLight.Messaging;		//Messenger
using GalaSoft.MvvmLight.Threading;		//DispatcherHelper

using System;
using System.IO;						//Path, File, Directory
using System.Net;						//IWebProxy, WebRequest
using System.Reflection;				//Assembly
using System.Text.RegularExpressions;	//RegEx
using System.Threading.Tasks;

namespace Desktop.Model
{
    /// <summary>
    /// The Desktop Application Session Manager class
    /// </summary>
    public sealed class SessionManager
    {
        #region | Private Member Variables |

		private String mLogConfigFileName = "log4net.config";

		private VXLogger logger; // logger can be initialized only after setting up the logging folder

        private string appStartupPath;
        private string appDataFolder;
        private string appLogsFolder;

        private string packUri;

        private User      user;
        private PhoneLine phoneLine;
        private string	  userAppDataFolder;

        private string							  managedUserDbFile;
        private ManagedDbManager.ManagedDbManager userDataStore = null;

		private string							  managedAppDbFile;
        private ManagedDbManager.ManagedDbManager appDataStore  = null;

        private long lastMessageTimestamp		= 0;
        private long lastCallTimestamp			= 0;
        private int  hoursBackForMessageHistory = 30 * 24;	//HP wants 30 days.  TODO: This should be configurable

		//Various connections to be tracked
		private bool mInternetConnected		= false;
		private bool mSipConnected			= false;
		private bool mXmppConnected			= false;

		//Proxies for above connections
		private	IWebProxy	mIProxy	= null;

        private SyncManager			  syncManager	  = null;
        private DbChangedEventHandler dbChangeHandler = null;

        private Action<ShowOrHideVoiceMailsMessage> messageThreadDisplayChangeHandler = null;

        private bool isUserLoggedIn     = false;
		private bool hasXmppConnected   = false;
		private bool isDebugMenuEnabled = true;		//false;	//NOTE: Make this false for production builds.

		//Info to help handle marking Chats as READ for active chat thread.
		//	This will be used with ActiveNavTab.
		private int		mActiveChatCmGroup     = ManagedDataTypes.Contact.INVALID_CMGROUP;
		private string	mActiveChatPhoneNumber = "";


        #endregion

        #region | Singleton |

        /// <summary>
        /// Create the folder structures in the AppData & initialize the logging to store the logs.
        /// This step is crucial for functioning of the application 
        /// </summary>
        private SessionManager()
        {
            Messenger.Default.Register<ConnectionMessage>( this, OnConnectionChange );

			appStartupPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            appDataFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), Desktop.Config.Branding.AppDataFolderName);
            appLogsFolder = Path.Combine(appDataFolder, "Logs");

            Directory.CreateDirectory(appDataFolder);
            Directory.CreateDirectory(appLogsFolder);

			try
            {
                var logConfigFileSrc  = Path.Combine(appStartupPath, mLogConfigFileName );
                var logConfigFileDest = Path.Combine(appDataFolder,  mLogConfigFileName );

                if (File.Exists(logConfigFileDest) == false)
                {
                    File.WriteAllText(logConfigFileDest, File.ReadAllText(logConfigFileSrc).Replace(@"Logs", appLogsFolder.Replace(@"\", @"\\")));
                }
            }
            catch (Exception /*ex*/)
            { 
			}

            Assembly assembly = Assembly.GetExecutingAssembly();
            packUri = string.Format("/{0};component", assembly.GetName().Name);

            // Initialize logging to store logs in AppData
            VXLoggingManager.INSTANCE.Setup(Path.Combine(appDataFolder, mLogConfigFileName) );
            
            // Start logger
            logger = VXLoggingManager.INSTANCE.GetLogger("SessionManager");

			string version	= Assembly.GetExecutingAssembly().GetName().Version.ToString() + Config.Branding.BuildSuffix;
			logger.Debug( "App Version: " + version );

			InitProxy();
			ApplyProxy();
        }

        public static SessionManager Instance
        {
            get { return Nested.instance; }
        }

        class Nested
        {
            static Nested()		{}

            internal static readonly SessionManager instance = new SessionManager();
        }

        #endregion

		#region Private Properties

		//Since we can set this to null, as we do on logout, we should NOT expose it to rest of app.
		//	Rather, we should provide accessor methods that check for null-ness and return appropriate value if it is.
        private PhoneLine PhoneLine
        {
            get { return phoneLine; }
        }

		#endregion

		#region | Public Properties |

		public string AppStartupPath
        {
            get { return appStartupPath; }
        }

        public string AppDataFolder
        {
            get { return appDataFolder; }
        }

        public string LogsFolder
        {
            get { return appLogsFolder; }
        }

        public string SipLogFile	{ get; set;	} 

        public string PackUri
        {
            get { return packUri; }
        }

        public User User
        {
            get { return user; }
        }

        public string UserAppDataFolder
        {
            get { return userAppDataFolder; }
        }

        public string ManagedUserDbFile
        {
            get { return managedUserDbFile; }
        }

		public string ManagedAppDbFile
        {
            get { return managedAppDbFile; }
        }

        public ManagedDbManager.ManagedDbManager AppDataStore
        {
            get { return appDataStore; }
        }

        public ManagedDbManager.ManagedDbManager UserDataStore
        {
            get { return userDataStore; }
        }

        public long LastMessageTimestamp
        {
            get { return lastMessageTimestamp; }
            set
            {
                lastMessageTimestamp = value;
                SettingsManager.Instance.User.LastMessageTimestamp = value;
            }
        }

        public long LastCallTimestamp
        {
            get { return lastCallTimestamp; }
            set
            {
                lastCallTimestamp = value;
                SettingsManager.Instance.User.LastCallTimestamp = value;
            }
        }

        public int HoursBackForMessageHistory
        {
            get { return hoursBackForMessageHistory; }
        }

		public bool IsUserLoggedIn
		{
			get { return isUserLoggedIn; }
			set { isUserLoggedIn = value; }
		}

		public bool IsDebugMenuEnabled
		{
			get { return isDebugMenuEnabled;	}
			set { isDebugMenuEnabled = value; 
					ActionManager.Instance.HandleDebugMenuToggle();
				}
		}

		//Used in Debug Menu
		//	This does not need to be persisted.  It is one time only and will be reset upon login
		//	Cannot be in USER, because we want to set it before user logs in.
		public bool EnableContactSourceSelection { get; set; }

		public bool IsContactSourceSelectionEnabled()
		{
			return IsDebugMenuEnabled && EnableContactSourceSelection;
		}

		public bool InternetConnected
		{
			get { return mInternetConnected; }
			set { mInternetConnected = value; }
		}

		public bool SipConnected
		{
			get { return mSipConnected; }
			set { mSipConnected = value; }
		}

		public bool XmppConnected
		{
			get { return mXmppConnected; }
			set { 
					mXmppConnected = value; 
					HandleXmppConnected( value );
				}
		}

		public IWebProxy Proxy
		{
			get { return mIProxy;	}
		}

		//InitOptions
		public String NativeLoginUrl { get; set; }

        #endregion

        #region | Public Procedures |

        /// <summary>
        /// Initialize Application (Non-User specific initialization)
        /// </summary>
        public void Initialize()
        {
            logger.Info("Setting manager initialize");
			initializeAppDb();
            SettingsManager.Instance.Initialize();

			//TODO: This logic should be in the VXLogger class.  It is NOT Api related.
			//	2015.10.06 - Not currently used, so commenting here.
			//if(string.IsNullOrWhiteSpace(SettingsManager.Instance.Api.IgnoreLoggers) == false)
			//{
			//	string[] loggers = null;

			//	if(SettingsManager.Instance.Api.IgnoreLoggers.Contains(";"))
			//	{
			//		loggers = SettingsManager.Instance.Api.IgnoreLoggers.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
			//	}
			//	else
			//	{
			//		loggers = new string[] { SettingsManager.Instance.Api.IgnoreLoggers.Trim() };
			//	}

			//	VXLoggingManager.INSTANCE.DisableLoggingFor(loggers);
			//}

			SipLogFile = LogsFolder + "\\pjsip.log";
            MixPanel.Initialize( Desktop.Config.API.MixPanelAPIUrl, Desktop.Config.API.MixPanelToken);
        }

		private void initializeAppDb()
		{
            // Initialize User-level Persistance Database
            managedAppDbFile = Path.Combine( appDataFolder, "app.db" );
            appDataStore = new ManagedDbManager.ManagedDbManager(ManagedAppDbFile, ManagedDbManager.ManagedDbManager.DbType.App );
            appDataStore.create();

			//TODO: DO we need a second DbChangedEventHandler?  There is currently no DbChange notification when AppSettings are changed in C++ code.
//	        dbChangeHandler = new DbChangedEventHandler(OnDbChange);
//		    appDataStore.addDbChangeListener(dbChangeHandler);

            logger.Info( "Data store file " + managedAppDbFile + " Created" );
		}

        /// <summary>
        /// Call this function to retrieve InitOptions which are needed BEFORE we display login window
		/// This call *MUST* not be async, since we need it before we display login window (first UI element).
        /// </summary>        
//        public async Task<bool> GetInitOptions()
        public bool GetInitOptions()
        {
			bool result = false;

			try
			{ 
				if ( SettingsManager.Instance.App.GetInitOptionsHandlingIsIgnore() )			//Added this because of server side issues
				{
 					logger.Debug( "Ignoring getInitOptions() result.");
					return true;																//We just accept hard-coded values.  
				}

				if ( SettingsManager.Instance.App.GetInitOptionsHandlingIsSimulateFailure() )	//Added this so QA can verify proper app response to failure.
				{
 					logger.Debug( "Simulating getInitOptions() failure.");
					SettingsManager.Instance.App.GetInitOptionsHandling = 0;					//One-time shot, so reset to Normal before returning.
					return false;
				}

				VXResult<GetInitOptionsResponse> response = null;

				Task optionsTask = Task.Run( () => { response = RestfulAPIManager.INSTANCE.GetInitialOptions(); } );
//                Task.WhenAll( optionsTask );

				//Let's wait 
				int maxWait   = 10000;		//Typical time is about 900 ms.
				int incWait   =   100;
				int totalWait =     0;

				while ( response == null && totalWait < maxWait )
				{
					System.Threading.Thread.Sleep( incWait );
					totalWait += incWait;
				}

				if ( response != null && response.Success )
				{
					bool signupIsValid = false;
					bool loginIsValid  = false;

					var data = response.Data;

					//Save Response values so we can use them when needed.
					if ( data.HasSignup() )
					{
						var signUp = data.Signup[0];

						if ( signUp.TypeIsInApp() )
						{
//							Config.Urls.SignupURL = signUp.signupUrl;
							signupIsValid = true;
						}
					}

					if ( data.HasLogin() )
					{
						//There may be more than one, but I am just using 'native' for now.
						var login = data.GetNativeLogin();

						if ( login != null )
						{
							NativeLoginUrl = login.loginUrl;

							loginIsValid = true;
						}

					}

					result = loginIsValid;	//&& signupIsValid;
				}
			}

            catch (Exception ex)
            {
                logger.Error( "Exception in GetInitOptions.", ex);
            }

			return result;
		}

        /// <summary>
        /// Call this function to login the user
        /// </summary>        
        /// <param name="Username">The name used in login</param>
        /// <param name="response"></param>
        public async Task<bool> InitializeUserSession(string Username, string Password, AuthenticateResponse response)
        {
            if (isUserLoggedIn == true)
                return true;

            logger.Info("Initializing User Session");

            // Create Per User folder, which will store Persistance Database & may be caching of Avatars etc.
            userAppDataFolder = Path.Combine(appDataFolder, CleanName(Username));
            if (Directory.Exists(userAppDataFolder) == false)
            {
                logger.Info("App data Directory " + userAppDataFolder + " Created");
                Directory.CreateDirectory(userAppDataFolder);
            }

            try
            {
                // Initialize User-level Persistance Database
                managedUserDbFile = Path.Combine(userAppDataFolder, "main.db");
                userDataStore     = new ManagedDbManager.ManagedDbManager(ManagedUserDbFile, ManagedDbManager.ManagedDbManager.DbType.User );
                userDataStore.create();

	            dbChangeHandler = new DbChangedEventHandler(OnDbChange);
		        userDataStore.addDbChangeListener(dbChangeHandler);

                logger.Info("Data store file " + managedUserDbFile + " Created");
            }
            catch (Exception ex)
            {
                logger.Error("Error Creating Persistance Database", ex);
                throw ex; // As having the peristance database is critical to running of the application the exception is rethrown
            }

            messageThreadDisplayChangeHandler = new Action<ShowOrHideVoiceMailsMessage>((OnChange) => { updateUnreadItemCounts(); });
            Messenger.Default.Register<ShowOrHideVoiceMailsMessage>(this, messageThreadDisplayChangeHandler);

            // Read User object
            user = new User();

            user.Did			= response.did;
            user.FirstName		= response.firstName;
            user.LastName		= response.lastName;
            user.Email			= response.email;
            user.Username		= Username;
            user.UserKey		= response.key;
            user.MPAlias		= response.mpAlias;
            user.CompanyUserId	= response.companyUserId;
            user.XmppJid		= user.CompanyUserId + "@" + Config.Xmpp.Domain;
            user.Cid			= response.cid;
            user.Balance		= response.balance;
            user.PartnerId		= response.partnerId;		//JRT - 2014.11.05 - This *should* have proper value based on login.  For HP, this should be '2'.

			CrashReporter.OnLogIn( user.Email, user.CompanyUserId );

            AvatarManager.Instance.Generate(string.Format("{0} {1}", user.FirstName, user.LastName), "Self" + user.Did);
            user.AvatarUri = AvatarManager.Instance.GetUriForContact( "Self" + user.Did );

            // Read Sip Details
            user.SipAccount.DisplayName = Username;
            user.SipAccount.UserName	= response.sipUsername;
            user.SipAccount.Identity	= response.sipUsername;
            user.SipAccount.Password	= response.sipPassword;
            user.SipAccount.Realm		= "voxox.com"; // TODO: Read from response?

            String sipServer = response.sipServer;	//We do this to make it easier to alter QA settings.
            //String sipServer = "216.151.151.60";	//QA
            if ( SettingsManager.Instance.Advanced.UseSipServer )
            {
                sipServer = SettingsManager.Instance.Advanced.SipServer;
                logger.Warn( "Using Non-Standard SIP Server: " + sipServer );
            }

			uint sipServerPort = (uint)( SettingsManager.Instance.Sip.UseTls ? 443  : 5060 );
			bool useSSL        = SettingsManager.Instance.Sip.UseTls;

			user.SipAccount.SipProxyServer     = new ServerInfo { Address = sipServer, Port = sipServerPort, UserId = user.Did, Password = response.sipPassword, UseSsl = useSSL }; // TODO: Port number also need to come from response??
            user.SipAccount.SipRegistrarServer = new ServerInfo { Address = sipServer, Port = sipServerPort, UserId = user.Did, Password = response.sipPassword, UseSsl = useSSL };

            user.SipAccount.SipTurnUdpServer = new ServerInfo { Address = response.turnServer, Port = 3478, UserId = response.turnUsername, Password = response.turnPassword };
            user.SipAccount.SipTurnTcpServer = new ServerInfo { Address = response.turnServer, Port =   80, UserId = response.turnUsername, Password = response.turnPassword };
            user.SipAccount.SipTurnTlsServer = new ServerInfo { Address = response.turnServer, Port =  443, UserId = response.turnUsername, Password = response.turnPassword };
            user.SipAccount.SipStunServer	 = new ServerInfo { Address = "stun.voxox.com",    Port = 5050 }; // TODO: Does this also come from response?
//			user.SipAccount.SipStunServer	 = new ServerInfo { Address = response.turnServer, Port = 3478 }; //Testing per Eliad's suggestion.  443 and 3478 sort of work.
//			user.SipAccount.SipStunServer	 = new ServerInfo { Address = response.turnServer, Port =  443 }; //Testing per Eliad's suggestion.  443 and 3478 sort of work.

			//Auto-detect proxy.
            user.SipAccount.SipHttpProxyServer = new ServerInfo { Type = ServerInfo.ServerType.HttpProxy, HttpProxyType = SipAgent.HTTPProxy.HTTPPROXY_AUTO };

            // Set / Get any user specific Settings from Configuration/AppDB
            SettingsManager.Instance.InitializeForUser(Username);
            lastMessageTimestamp = SettingsManager.Instance.User.LastMessageTimestamp;
            lastCallTimestamp    = SettingsManager.Instance.User.LastCallTimestamp;

            // Initialize Phone Line based in the User / SIP / Settings
            logger.Info("Initializing Phone Line");
            phoneLine = new PhoneLine(user.SipAccount);
            try
            {
                phoneLine.init();
                phoneLine.connect();
            }
            catch (Exception ex)
            {
                logger.Error("Error initializing Phone Line", ex);
            }

            //Start XMPP service and login.  Use separate vars for clarity.
            String xmppUserId   = response.companyUserId;				//From SSO response
            String xmppPassword = Password;								//From user login.

            try
            {
                XmppManager.getInstance().login( response.xmppUsername, response.xmppPassword );
            }
            catch (Exception ex)
            {
                logger.Error("Error connecting to XMPP", ex);
            }

            // Synchronize Data
            syncManager    = new SyncManager(Username);
            isUserLoggedIn = await syncManager.Start();

            updateDataSummary();

            MixPanel.LogLogin(user.MPAlias);

            return isUserLoggedIn;
        }

        public async Task LogoutUser()
        {
            if (isUserLoggedIn == false)
                return;

            if ( UserDataStore != null && dbChangeHandler != null)
            {
                UserDataStore.removeDbChangeListener(dbChangeHandler);
                dbChangeHandler = null;
            }

            if (messageThreadDisplayChangeHandler != null)
            {
                Messenger.Default.Unregister<ShowOrHideVoiceMailsMessage>(this, messageThreadDisplayChangeHandler);
                messageThreadDisplayChangeHandler = null;
            }

            MixPanel.LogLogout();

            logger.Debug("Logging out user");

			Task shutdownServicesTask = ShutdownServices();		//This covers all services, and waits for results
            await Task.WhenAll( shutdownServicesTask );

            // Dispose User
            user = null;

            //Closing settings for user
            SettingsManager.Instance.CloseUserSettings();

            userAppDataFolder = null;
            managedUserDbFile = null;

            isUserLoggedIn = false;
			hasXmppConnected = false;

            SettingsManager.Instance.Advanced.resetToDefaults(); // Resetting any Advanced/QA settings. This can be removed if the overrides are to be maintained till the app is exited.
        }


		//This captures current chat/IM session so we can correctly mark messages as READ
		//	once they are viewed.  It will be used in conjunction with ActiveNavTab.
		public ActiveTabEnum ActiveNavTab		{ get; set; }
		public Boolean		 IsMainWindowActive	{ get; set; }

		public void SetActiveChatInfo( int cmGroup, string phoneNumber )
		{
			mActiveChatCmGroup     = cmGroup;
			mActiveChatPhoneNumber = phoneNumber;
		}

		public int SetMessagesReadIfNeeded( int cmGroup, string phoneNumber, UmwMessageList msgList )
		{
			int changeCount = 0;

			if ( IsMainWindowActive && (ActiveNavTab == ActiveTabEnum.Messages) )
			{
				if ( (cmGroup != ManagedDataTypes.Contact.INVALID_CMGROUP && mActiveChatCmGroup == cmGroup) || mActiveChatPhoneNumber == phoneNumber )
				{
					changeCount = MessageManager.getInstance().markMessagesReadWhenViewed( msgList );
				}
			}

			return changeCount;
		}

		public void ToggleDebugMenu()
		{
			IsDebugMenuEnabled = !IsDebugMenuEnabled;
		}

		#endregion

		#region Shutdown services methods

		private async Task<bool> ShutdownServices()
        {
            try
            {
				//DO NOT shutdown these services in parallel.
				//	It must be done serially to ensure we are in a clean state upon completion of shutdown.
				VXProfiler profiler0 = new VXProfiler( "SessionManager:ShutdownServices - Total" );
                logger.Debug( "ShutdownServices: Start" );
               
                // Shutdown SyncManager
				VXProfiler profiler1 = new VXProfiler( "SessionManager:ShutdownServices - SyncManager" );
                Task syncManagerShutdownTask = Task.Run(() => { ShutdownSyncManager(); });
                await Task.WhenAll( syncManagerShutdownTask );
				profiler1.Stop();

                // Shutdown SIP - Incoming calls may trigger more DB and JPush activity.
				VXProfiler profiler2 = new VXProfiler( "SessionManager:ShutdownServices - SIP" );
                Task sipShutdownTask = Task.Run(() => { ShutdownSip(); });
                await Task.WhenAll( sipShutdownTask );
				profiler2.Stop();

                // Shutdown XMPP - Do this BEFORE ShutdownDatabase, because XMPP JPush notifications may trigger more DB activity.
				VXProfiler profiler3 = new VXProfiler( "SessionManager:ShutdownServices - XMPP" );
                Task xmppShutdownTask = Task.Run(() => { ShutdownXmpp(); });
                await Task.WhenAll( xmppShutdownTask );
				profiler3.Stop();

                // Shutdown Database
				VXProfiler profiler4 = new VXProfiler( "SessionManager:ShutdownServices - Database" );
                Task databaseShutdownTask = Task.Run(() => { ShutdownDatabase(); });
                await Task.WhenAll( databaseShutdownTask );
				profiler4.Stop();

                logger.Debug( "ShutdownServices: Complete" );
				profiler0.Stop();
            }
            catch (Exception ex)
            {
                logger.Error("Error during ShutdownServices()", ex);
            }

			return true;
        }

		private bool ShutdownSyncManager()
		{
            //No event to wait for, but want to keep it similar to other shutdown methods
            if (syncManager != null)
            {
                syncManager.Stop();
            }

			return true;
		}

		private bool ShutdownSip()
		{
			//TODO: Wait for SIP Disconnect event?
            // Close Phone Line
            if (phoneLine != null)
            {
                phoneLine.disconnect(true);
				phoneLine.Dispose();		//This terminates SIP stack.  Typically takes about 1600 ms, almost all in PJSIP libDestroy() call.
				phoneLine = null;
            }

			return true;
		}

		private bool ShutdownXmpp()
		{
            //Disconnect from XMPP service
            XmppManager.getInstance().logoff();	//This will generate an event on a different thread.

			//Wait for XMPP disconnect event?
			int		maxWait = 5000;			//Typical is about 250 ms.
			int		waitInc =  100;
			int		totalWait = 0;

			while ( !XmppManager.getInstance().isShutdown() && totalWait < maxWait )
			{
				System.Threading.Thread.Sleep( waitInc );
				totalWait += waitInc;
			}

			return true;
		}

		private bool ShutdownDatabase()
		{
            // Release data store.  This is done async'ly because we may have active DB work running due to Push Notifications.
            if ( userDataStore != null)
            {
                userDataStore.close();	//This may block for short time if we have active DB work.
                userDataStore = null;
            }

			return true;
		}

		#endregion

		#region Asynchronous update methods

		public void updateDataSummary()
        {
            Task.Run(() =>
            {
                if (SessionManager.Instance.User == null)
                    return;

                var response = Vxapi23.RestfulAPIManager.INSTANCE.GetDataSummary(SessionManager.Instance.User.UserKey, SessionManager.Instance.User.CompanyUserId);

                if (isUserLoggedIn == false)
                    return;

                if (response != null && response.Count > 0)
                {
                    foreach (DataSummaryResponse ds in response)
                    {
                        User.Balance = ds.balance;
                        User.Cid = ds.cid;
                        User.Did = ds.did;
                        User.CompanyUserId = ds.userId;
                        User.UserKey = ds.userkey;
                        User.Username = ds.username;
                        User.XmppJid = User.CompanyUserId + "@" + Config.Xmpp.Domain;

                        //SIP
                        User.SipAccount.Password = ds.sipPassword;
                        User.SipAccount.UserName = ds.sipUsername;
                        //User.SipAccount.SipIP				  = ds.sipIP;			//TODO: Update SIP server info
                        //User.SipAccount.SipRegistrarServer  = ds.sipServer;

                        //Msg counts - TODO: Compare these to what we have locally to resolve differences and update code as needed.
						//				Currently, these counts are WAY off for XMPP and MissedCalls.  WIP.
						User.BadgeCounts bc = new User.BadgeCounts( ds.counts.Xmpp, ds.counts.Sms, ds.counts.Fax, ds.counts.Voicemail, ds.counts.Recorded, ds.counts.Missed_Calls );
						User.Counts = bc;

						//FindMe TODO
                        //User.FindMeOn						= ds.findmeOn;

                        // ReachMeNumbers
                        User.ReachMeNumbers.Clear();
                        if(ds.findmeNumbers != null && ds.findmeNumbers.ReachMeNumbers != null && ds.findmeNumbers.ReachMeNumbers.Count > 0)
                        {
                            foreach (var rmm in ds.findmeNumbers.ReachMeNumbers)
                            {
                                User.ReachMeNumbers.Add(new User.ReachMeNumber
                                {
                                    Enabled = rmm.Enabled,
                                    Priority = rmm.Priority,
                                    Timeout = rmm.Timeout,
                                    Label = rmm.Label,
                                    OffnetNumber = rmm.OffnetNumber
                                });
                            }
                        }

                        // Caller IDS
                        User.CallerIDs.Clear();
                        if (ds.callerIds != null && ds.callerIds.Count > 0)
                        {
                            foreach (var ci in ds.callerIds.Values)
                            {
                                User.CallerIDs.Add(new User.CallerID
                                {
                                    Name = ci.Name,
                                    Number = ci.Number
                                });
                            }
                        }

						//Now alert elements that may be affected by these changes
						GalaSoft.MvvmLight.Messaging.Messenger.Default.Send<BalanceChangedMessage>( new BalanceChangedMessage{ Balance = User.Balance } );
                    }
                }
            });
        }

		public void updateCompanyUserOptions()
		{
			if ( syncManager != null )
			{
				Task.Run(() =>
				{
					syncManager.GetCompanyUserOptions();
				});

			}
		}

		public void updateUnreadItemCounts()
		{
            if ( UserDataStore == null)
                return;

			UnreadCounts counts = UserDataStore.getUnreadItemCounts(SettingsManager.Instance.User.ShowVoicemailsInChatThread, 
																	SettingsManager.Instance.User.ShowRecordedCallsInChatThread, 
																	SettingsManager.Instance.User.ShowFaxesInChatThread); // TODO: Making this true might also hide count for Faxes tab

			UpdateUnreadItemCountsMessage mvvmMsg = new UpdateUnreadItemCountsMessage();
			mvvmMsg.Messages	= counts.getMessageCount();
			mvvmMsg.Faxes       = counts.FaxCount;
			mvvmMsg.Voicemails  = counts.VoicemailCount;	//Not currently displayed
			mvvmMsg.MissedCalls = counts.MissedCallCount;

			GalaSoft.MvvmLight.Messaging.Messenger.Default.Send<UpdateUnreadItemCountsMessage>( mvvmMsg );
		}

        private void OnDbChange(object sender, DbChangedEventArgs e)
        {
            if (e.Data.isMsgTable() || e.Data.isRecentCallTable() )
            {
				DispatcherHelper.CheckBeginInvokeOnUI( () => { updateUnreadItemCounts(); } );
            }
        }

		private void OnConnectionChange( ConnectionMessage msg )
		{
			if		( msg.isInternet() )
			{
				InternetConnected = msg.Connected;
			}
			else if ( msg.isXmpp() )
			{
				XmppConnected = msg.Connected;
			}
			else if ( msg.isSip() )
			{
				SipConnected = msg.Connected;
			}
			else
			{
				//TODO: Log warning
			}
		}

        #endregion

		#region Public PhoneLine procedures

		public Calling.PhoneLine.CallError MakeCall( string phoneNumber, bool isExtension )
		{
			Calling.PhoneLine.CallError error = Calling.PhoneLine.CallError.NoError;

			if ( phoneLine != null )
			{
				error = phoneLine.makeCall( phoneNumber, isExtension );
			}

			return error;
		}

		public PhoneCall GetPhoneCall( int callId )
		{
			PhoneCall result = null;

			if ( phoneLine != null )
			{
				result = phoneLine.getPhoneCall( callId );
			}

			return result;
		}

		public long GetCallDuration( int callId )
		{
			long result = 0;

			if ( phoneLine != null )
			{
				result = phoneLine.getCallDuration( callId );
			}

			return result;
		}

		public void AcceptCall( int callId )
		{
			if ( phoneLine != null )
			{
				phoneLine.acceptCall( callId );
			}
		}

		public void RejectCall( int callId )
		{
			if ( phoneLine != null )
			{
				phoneLine.rejectCall( callId );
			}
		}

		public void ResumeCall( int callId )
		{
			if ( phoneLine != null )
			{
				PhoneLine.resumeCall( callId );
			}
		}

		public void CloseCall( int callId )
		{
			if ( phoneLine != null )
			{
				phoneLine.closeCall( callId );
			}
		}

		//So we can control the hangup sound
		public void MarkCallAsMerging( int callId, Boolean value )
		{
			if ( phoneLine != null )
			{
				phoneLine.markCallAsMerging( callId, value );
			}
		}

		public void CloseAllPhoneCalls()
		{
			if ( phoneLine != null )
			{
				phoneLine.CloseAllPhoneCalls();
			}
		}

		public string GetSipCallId( int callId )
		{
			String result = "";

			if ( phoneLine != null )
			{
				result = phoneLine.getSipCallId( callId );
			}

			return result;
		}

		public Boolean HasPendingCalls()
		{
			Boolean result = false;

			if ( phoneLine != null )
			{ 
				result = phoneLine.hasPendingCalls();
			}

			return result;
		}

		//This is a debug items to fake an incoming call
		public void setPhoneCallState( int callId, PhoneCallState.CallState callState, SipAddress sipAddress, int statusCode )
		{
			phoneLine.setPhoneCallState( callId, callState, sipAddress, statusCode );
		}

		#endregion

		#region Private Procedures

		private string CleanName(string fileName)
        {
            string invalidChars = Regex.Escape(new string(Path.GetInvalidFileNameChars()));
            string invalidRegStr = string.Format(@"([{0}]*\.+$)|([{0}]+)", invalidChars);

            return Regex.Replace(fileName, invalidRegStr, "");
        }

		//We have this because our push notifications are XMPP-based.
		private void HandleXmppConnected( bool connected )
		{
			//Only do this on CONNECT and if user is already logged in.
			//	If user is NOT logged in, then normal startup code will handle this.
			if ( connected && isUserLoggedIn )
			{

				//TODO: Send PENDING XMPP/SMS messages in Database
				//TODO: Send Read Receipts.  (This may be handled in XMPP stack, but seems like overkill)
				//Let's NOT do this right after user login, but only after we have already had one XMPP connection.
				//	*May* need to add a timestamp to this to user Login time to refine this.
				if (hasXmppConnected )
					PushNotificationManager.getInstance().HandleXmppReconnect();
				
				if ( connected )
					hasXmppConnected = true;
			}
		}

		#endregion

		#region Private Proxy methods

		//Check for proxies, if they exist.  We will pass that info to the software
		//	stacks that may require them:
		//	- NetworkStatus
		//	- Voxox server API
		//	- XMPP stack
		//	- SIP stack
		//
		//By doing this here, we can better control proper handling when networks change
		//	and keep the proxy detection code consistent and encapsulated.
		//
		//Update software stacks, as needed.
		private void ApplyProxy()
		{
			SetProxyForNetworkStatus();
			SetProxyForVoxoxApi();
//			SetProxyInfoForXmpp();	//This throws exception when called from Initialize(), so do it externally.
			//TODO: SIP stack (we get the SIP registration server AFTER login.
		}

		private void InitProxy()
		{
			bool useManualProxy = false;	//true == for dev/testing

			//TODO: Future implementation will check for Settings -> ManualProxy and use the related data elements
			//	For now, we use manual for testing/dev only.
			//	Immediate goal is to address WPAD for HP.
			if ( useManualProxy )
			{
				String proxyAddr = "8.38.43.18";	//Test proxy at Voxox
				int    proxyPort = 8888;

				WebProxy proxy = new WebProxy( proxyAddr, proxyPort);

				proxy.BypassProxyOnLocal = false;

				mIProxy = proxy;
			}
			else
			{
				//Attempt WPAD
				//This is the IE proxy settings.  It always returns non-null value so we need a way to evaluate it.  TODO.
				mIProxy = WebRequest.GetSystemWebProxy();

				if ( mIProxy == null )
				{
					mIProxy = WebRequest.DefaultWebProxy;
				}
			}
		}

		private void SetProxyForNetworkStatus()
		{
			Desktop.Util.NetworkStatus.s_Proxy = mIProxy;	//Network status (connectivity) check.
		}

		private void SetProxyForVoxoxApi()
		{
			Vxapi23.Base.BaseService.setProxy( mIProxy );
		}

		//JRT 2015.10.06 - Not being called anywhere.
		//public void SetProxyInfoForXmpp()
		//{
		//	//Since we are calling this method, set this TRUE, even if no proxy exists
		//	//	so XMPP client does NOT attempt its own proxy check.
		//	SettingsManager.Instance.Xmpp.UseProxy = true;

		//	String url = "http://" + SettingsManager.Instance.Xmpp.Server;
		//	Uri temp = GetProxyForUrl( url );

		//	if ( temp != null )
		//	{
		//		SettingsManager.Instance.Xmpp.ProxyServer = temp.Host;
		//		SettingsManager.Instance.Xmpp.ProxyPort   = temp.Port;
		//	}
		//}

		private Uri GetProxyForUrl( string url )
		{
			Uri result = null;

			if ( mIProxy != null && !String.IsNullOrEmpty(url) )
			{
				Uri		tempUri		= new Uri(url);
				String	host		= tempUri.Scheme + "://" + tempUri.Host;
				bool	isBypassed	= mIProxy.IsBypassed( new Uri(host) );

				if ( isBypassed )	//If ByPassed == true, the proxy is NOT used.
				{
					logger.Debug( "NOT using proxy for URL " + tempUri.ToString() );
				}
				else 
				{
					result = mIProxy.GetProxy( new Uri(url) );

					logger.Debug( "Using proxy: " + result.AbsoluteUri + " for URL " + tempUri.ToString() );
				}
			}

			return result;
		}

        #endregion
    }
}
