﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Utils;	//Logger

using System;
using System.Collections.Concurrent;	//ConcurrentDictionary
using System.IO;						//File, Path, Directory
using System.Net;						//WebRequest
using System.Net.Security;				//RemoteCertificateValidationCallback

namespace Desktop.Model
{
    public class RichMediaManager
    {
        private static VXLogger logger = VXLoggingManager.INSTANCE.GetLogger("RichMediaManager");
        private static readonly NamedLocker namedlocker = new NamedLocker();

        /// <summary>
        /// Downloads the Rich Media (Photo/Video) to local cache store using the url and return the local path to the file
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string Download(string Key, string url)
        {
            string richMediaDir = CheckAndGetRichMediaDir();
            string mediaFilePath = Path.Combine(richMediaDir, Key);

            lock (namedlocker.GetLock(mediaFilePath))
            {
                if (File.Exists(mediaFilePath))
                    return mediaFilePath;

                using (WebClientEx client = new WebClientEx())
                {
                    try
                    {
                        client.DownloadFile(url, mediaFilePath);

                        return mediaFilePath;
                    }
                    catch (Exception ex)
                    {
                        logger.Error("Download", ex);
                        return url;
                    }
                }
            }
        }

        private static string CheckAndGetRichMediaDir()
        {
            string richMediaDir = Path.Combine(SessionManager.Instance.UserAppDataFolder, "RichMedia");

            try
            {
                if (Directory.Exists(richMediaDir) == false)
                    Directory.CreateDirectory(richMediaDir);
            }
            catch (Exception ex)
            {
                logger.Error("CheckAndGetRichMediaDir", ex);
            }

            return richMediaDir;
        }

        /// <summary>
        /// A Class to handle locks on the "Name/Path" of the Media file so that there are no concurrent file downloads for the same file
        /// </summary>
        private class NamedLocker
        {
            private readonly ConcurrentDictionary<string, object> lockDict = new ConcurrentDictionary<string, object>();

            //get a lock for use with a lock(){} block
            public object GetLock(string name)
            {
                return lockDict.GetOrAdd(name, s => new object());
            }
        }

        /// <summary>
        /// Extended WebClient with increased Timeout & ignoring any SSL errors
        /// </summary>
        private class WebClientEx : WebClient
        {
            public WebClientEx()
            {
                try
                {
                    ServicePointManager.ServerCertificateValidationCallback =
                        new RemoteCertificateValidationCallback(
                            delegate
                            { return true; }
                        );
                }
                catch
                {
                }
            }

            protected override WebRequest GetWebRequest(Uri uri)
            {
                WebRequest w = base.GetWebRequest(uri);
                w.Timeout = 5 * 60 * 1000; // 5 minute timeout for download
                return w;
            }
        }
    }
}
