﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model.Calling;		//SipAccount

using Vxapi23.Model;				//CompanyUserOptionsResponse

using System;
using System.Collections.Generic;

namespace Desktop.Model
{
    public class User
    {
		public class BadgeCounts
		{
			public BadgeCounts( int xmpp, int sms, int fax, int vm, int rc, int mc )
			{
				Xmpp		 = xmpp;
				Sms			 = sms;
				Fax			 = fax;
				Voicemail	 = vm;
				RecordedCall = rc;
				MissedCall   = mc;
			}

			public BadgeCounts()
			{
				Xmpp		 = 0;
				Sms			 = 0;
				Fax			 = 0;
				Voicemail	 = 0;
				RecordedCall = 0;
				MissedCall   = 0;
			}

			public int Xmpp			{  get; set; }
			public int Sms			{  get; set; }
			public int Fax			{  get; set; }
			public int Voicemail	{  get; set; }
			public int RecordedCall {  get; set; }
			public int MissedCall	{  get; set; }
			 
			public int Messages()
			{
				return Xmpp + Sms;
			}
		
		}

        public class ReachMeNumber
        {
            public int	  Enabled		{ get; set; }
            public int	  Timeout		{ get; set; }
            public int	  Priority		{ get; set; }
            public string Label			{ get; set; }
            public string OffnetNumber	{ get; set; }
        }

        public class CallerID
        {
            public string Name	 { get; set; }
            public string Number { get; set; }
        }

        private SipAccount sipAccount;

        #region | Constructors |

        public User()
        {
            sipAccount = new SipAccount();
            Options = new CompanyUserOptions();
			Balance = 0;
            ReachMeNumbers = new List<ReachMeNumber>();
            CallerIDs = new List<CallerID>();
        }

        #endregion

        #region | Public Properties |

		//Profile related - We get these in authenticate() response and/or getCompanyUserExtended() API calls.
        public string Did			{ get; set; }
        public string FirstName		{ get; set; }
        public string LastName		{ get; set; }
        public string Email			{ get; set; }
        public string CompanyName	{ get; set; }
        public string Extension		{ get; set; }

		//These are user related, but not in Profile
        public string AvatarUri		{ get; set; }
		public double Balance		{ get; set; }

		//These are system related
        public string Username		{ get; set; }
        public string UserKey		{ get; set; }
        public string MPAlias		{ get; set; }
        public string CompanyUserId { get; set; }
        public string XmppJid		{ get; set; }
        public string Cid			{ get; set; }
		public String PartnerId		{ get; set; }


        public SipAccount SipAccount
        {
            get { return sipAccount; }
        }

		public BadgeCounts			Counts			{ get; set; }
        public CompanyUserOptions	Options			{ get; set; }
        public List<ReachMeNumber>	ReachMeNumbers	{ get; set; }
        public List<CallerID>		CallerIDs		{ get; set; }

        public string Name
        {
            get
            {
                string name = string.Format("{0} {1}", FirstName, LastName).Trim();
                if (name.Length == 0)
                    return Username;
                return name;
            }
        }

        #endregion


		//TODO: This is a VERY bare-bones implementation of Company User options.
		//	API response appears to have all the data elements, but we are using just a few boolean/int values.
		//	MUCH more to do here and in the main app.
        public class CompanyUserOptions
        {
			public void fromApiResponse( CompanyUserOptionsResponse response )
			{
                if(response != null)
                {
                    if (response.ON_DEMAND_CALL_RECORDING != null)
                    {
                        IsCallRecordingAvailable = response.ON_DEMAND_CALL_RECORDING.IsFeatureEnabled;
                    }

                    if (response.CONFERENCE_MEMBERS != null)
                    {
                        ConferenceCallLimit = response.CONFERENCE_MEMBERS.QuantityLimit;	//FWIW, this is managed by server.
                    }

                    if (response.SEND_FAX != null)
                    {
                        IsFaxAvailable = response.SEND_FAX.IsFeatureEnabled;
                    }

                    if (response.CALL_ROUTING_EXT != null)
                    {
                        IsFlipAvailable = response.CALL_ROUTING_EXT.IsFeatureEnabled;
                    }

					//Commenting below.  Eavesdrop is always on and CALL_ROUTING_VM is not the proper Option to control it.
					//if (response.CALL_ROUTING_VM != null)
					//{
					//	IsEavesdropAvailable = response.CALL_ROUTING_VM.IsFeatureEnabled;
					//}			

					//Eavesdrop is always on, for now.  Rather than change all the logic elsewhere, we will control it from here.
                    IsEavesdropAvailable = true;

                    IsStoreAvailable = false;
                    IsInviteAvailable = false;
				}
			}


            public bool IsCallRecordingAvailable	{ get; set; }
            public int  ConferenceCallLimit			{ get; set; }
            public bool IsFaxAvailable				{ get; set; }
            public bool IsFlipAvailable				{ get; set; }
            public bool IsEavesdropAvailable		{ get; set; }
            public bool IsStoreAvailable		    { get; set; }
            public bool IsInviteAvailable		    { get; set; }

            // Not currently used in app

            //public int ReachMeNumbers { get; set; }

            //public bool SetOutboundCallerID { get; set; }

            //public bool VMTranscription { get; set; }
        }
    }
}

