﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

#define USE_BUGSPLAT
//#define USE_HOCKEYAPP

using Desktop.Utils;	//Logger

using System;

#if USE_BUGSPLAT
using System.Reflection;			//Assembly info
using System.Collections.Generic;	//List<> of files
using System.Windows.Forms;			//For final 'app crashed' MessageBox
#endif

#if USE_HOCKEYAPP
using System.Diagnostics;
using System.Threading.Tasks;
using HockeyApp;
#endif

//ThreadException handler, which we are not using for now.
//using System.Windows.Forms;	
//using System.Threading;

namespace Desktop.Model
{
	public sealed class CrashReporter
	{
        private static VXLogger logger = VXLoggingManager.INSTANCE.GetLogger("CrashReporter");


		private static string s_initText     = "[start up]";
		private static string s_onLogOutText = "[logged out]";
		private static string s_onQuitText   = "[quit]";
		private static string s_crashMsg     = "Voxox has encountered an error and is quiting.";
		private static string s_crashTitle   = "Voxox";

		public static string getInitText()					{ return s_initText;		}
		public static string getOnLogOutText()				{ return s_onLogOutText;	}
		public static string getOnQuitText()				{ return s_onQuitText;		}
		public static string getOnCrashMsg()				{ return s_crashMsg;		}
		public static string getOnCrashTitle()				{ return s_crashTitle;		}

		//Default C#
        public static void handleException(object sender, UnhandledExceptionEventArgs args)
        {
            Exception e = (Exception)args.ExceptionObject;

			logException( e );
            logger.Fatal("Runtime terminating: "+ args.IsTerminating);

			//Add to UserDescription, so it shows in BugSplat interface
			CrashReporter.AppendToDescription( e.ToString() );

			//NOTE:  Do NOT try for a graceful app exit here.
			//	This is because we are here BEFORE we call the BugSplat handler
			//	because we need to log the inner exception value, or we miss
			//	some key data for crash resolution.
			//
			//If you want to exit app gracefully, use BugSplat.CrashReporter.Callback()
        }

		public static void logException( Exception e )
		{
            logger.Fatal("Unhandled Exception on " + e);
		}

		//Default C#
//        public static void handleThreadException(object sender, ThreadExceptionEventArgs args)
//        {
//            logger.Fatal("Unhandled Exception on " + args.Exception);
//        }

		public static void Initialize( String crashTitle, String crashMsg )
		{
			if ( crashTitle != null )
				s_crashTitle = crashTitle;

			if ( crashMsg != null )
				s_crashMsg = crashMsg;

			//Add default handler before BugSplat, otherwise we do not log the exception,
			//	which may contain an inner exception that is not captured in the .dmp file.

			// Initialize the class to handle unhandled exceptions.
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(handleException);

			//UI Thread Exception handler.  May not need this.
//			Application.ThreadException += new ThreadExceptionEventHandler( handleThreadException );
						
#if USE_BUGSPLAT
			MyBugSplat.Init();
#endif
		}

		public static void OnLogIn( String email, String companyUserId )
		{
#if USE_BUGSPLAT
			MyBugSplat.OnLogIn( email, companyUserId );
#endif
		}

		public static void OnLogOut()
		{
#if USE_BUGSPLAT
			MyBugSplat.OnLogOut();
#endif
		}

		public static void OnQuit()
		{
#if USE_BUGSPLAT
			MyBugSplat.OnQuit();
#endif
		}

		public static void AppendToDescription( String desc )
		{
#if USE_BUGSPLAT
			MyBugSplat.AppendToDescription( desc );
#endif
		}

	}	//CrashReporter


#if USE_BUGSPLAT
	class MyBugSplat
	{ 
		private static string s_bugSplatDb    = "jrtheinert_tsasg_com";		//TODO: Configure this.  Brandable?
		private static string s_innerText     = "--- End of inner exception stack trace";
		private static int    s_maxDescLength = 2000;

		public static void Init()
		{
			// Initialize BugSplat with our database, application and version strings
			string app		= Assembly.GetExecutingAssembly().GetName().Name;
			string version	= Assembly.GetExecutingAssembly().GetName().Version.ToString();

			BugSplat.CrashReporter.Init( s_bugSplatDb, app, version);
			BugSplat.CrashReporter.QuietMode		= true;				//true = Do NOT prompt to send.

			updateEventText( CrashReporter.getInitText() );

			//Add app log file
			String filePath = VXLoggingManager.FilePath();

			if ( !String.IsNullOrEmpty( filePath ) )
			{ 
				List<String> files = new List<String>();
				files.Add( filePath );
				BugSplat.CrashReporter.AdditionalFiles = files;
			}

			// Let BugSplat know what events to handle
			System.AppDomain.CurrentDomain.UnhandledException            += BugSplat.CrashReporter.AppDomainUnhandledExceptionHandler;
			System.Windows.Forms.Application.ThreadException             += BugSplat.CrashReporter.ApplicationThreadException;
			System.Threading.Tasks.TaskScheduler.UnobservedTaskException += BugSplat.CrashReporter.TaskSchedulerUnobservedTaskExceptionHandler;

			// Add an application event handler, if desired. We just display a 'App failed' MessageBox
			BugSplat.CrashReporter.exceptionHandled += new BugSplat.CrashReporter.Callback( OnBugSplatEvent );
		}

        // Called from the BugSplat Event
		private static void OnBugSplatEvent(Exception e)
		{
			CrashReporter.logException( e );

			// An application specfic action can be taken here.  We display a simple MessageBox.
            MessageBox.Show( CrashReporter.getOnCrashMsg(), CrashReporter.getOnCrashTitle(), MessageBoxButtons.OK, MessageBoxIcon.Stop);

//			//Avoid Windows Crash dialog - Not doing this seems to work OK, except when we encounter this under Debugger.
//			System.ComponentModel.CancelEventArgs cancelArgs = new System.ComponentModel.CancelEventArgs();
//			System.Windows.Forms.Application.Exit(cancelArgs);	
//			System.Environment.Exit(99);
		}

		public static void AppendToDescription( String desc )
		{
			//For BugSplat User Description, we only want the important 'inner exception' text
			//	Should be < 2000.
			//
			//TODO: If we do NOT have an inner exception:
			//	- Do we want the descripton at all?  It would just be the call stack, right?
			int pos = desc.IndexOf( s_innerText );

			//If no inner exception, let's limit the description size
			if ( pos < 0 )
			{
				pos = Math.Min( desc.Length, s_maxDescLength );
			}

//			String newDesc = BugSplat.CrashReporter.UserDescription + "\n" + desc.Substring( 0, pos );


			//It looks like BugSplat only allows 260 chars in User Description (at least that is all I can view),
			//	so let's strip this down to the method names only.  We can always go to the log for details.
			//Criteria are:
			//	- starts with 'at '
			//	- ends with first '('
			//	- no 'at ' in between.
			String startText       = "at ";
			String endText         = "(";
			String methodSeparator = ":";

			String work = desc.Substring( 0, pos );
			String newDesc = "";
			int pos1    = 0;
			int posAt   = 0;
			int posEnd  = 0;
			int posAt2 = 0;

			while ( pos1 < work.Length )
			{
				//Find StartText
				posAt = work.IndexOf( startText, pos1 );

				if ( posAt > 0 )
				{
					//Find EndText
					posEnd = work.IndexOf( endText, posAt );

					if ( posEnd > 0 )
					{
						//Ensure no StartText between
						int tempPos = posAt + startText.Length;
						int tempLen = posEnd - tempPos;
						posAt2 = work.IndexOf( startText, tempPos, tempLen );

						//If < 0, then we are good
						if ( posAt2 < 0 )
						{
							if ( newDesc.Length > 0)
								newDesc += methodSeparator;

							newDesc += work.Substring( tempPos, tempLen );
							pos1 = posEnd + 1;
						}
						else
						{
							//We found an intermediate 'at ' which may be part of description, so let's use that for new start position.
							pos1 = posAt2;
						}
					}
					else 
					{ 
						pos1 = work.Length;	//Force end of loop
					}
				}
				else
				{
					pos1 = work.Length;	//Force end of loop
				}
			}

			BugSplat.CrashReporter.UserDescription = newDesc;
		}

		public static void OnLogIn( String email, String companyUserId )
		{
			BugSplat.CrashReporter.User             = companyUserId;
			BugSplat.CrashReporter.UserDescription  = "CompanyUserId: " + companyUserId;
			BugSplat.CrashReporter.Email			= email;
		}

		public static void OnLogOut()
		{
			updateEventText( CrashReporter.getOnLogOutText() );
		}

		public static void OnQuit()
		{
			updateEventText( CrashReporter.getOnQuitText() );
		}

		private static void updateEventText( String text )
		{
			BugSplat.CrashReporter.User            = text;
			BugSplat.CrashReporter.UserDescription = text;
			BugSplat.CrashReporter.Email		   = text;
		}
	}	//class MyBugSplat
#endif	//USE_BUGSPLAT



#if USE_HOCKEYAPP
		String mHockeyAppId = "648531ee69e534e7bec3798c2f8c63b3";		//TODO-BRAND
//		String mHockeyAppId = "77db08dfe77320fc1d22cb7f829338ad";		//TODO-BRAND

		private static async Task initHockeyApp()
		{
			//configure HockeySDK client
			//TODO: Enable uploading of our EventLog
			HockeyApp.HockeyClient.Current.Configure( mHockeyAppId )
				//.UseCustomResourceManager(HockeyApp.ResourceManager)									//register your own resourcemanager to override HockeySDK i18n strings 
				//.RegisterCustomUnhandledExceptionLogic((eArgs) => { /* do something here */ })		// define a callback that is called after unhandled exception 
				//.RegisterCustomUnobserveredTaskExceptionLogic((eArgs) => { /* do something here */ }) // define a callback that is called after unobserved task exception 
				//.RegisterCustomDispatcherUnhandledExceptionLogic((args) => { })						// define a callback that is called after dispatcher unhandled exception 
				//.SetApiDomain("https://your.hockeyapp.server") 
				//.SetContactInfo("John Smith", "email@example.com")
				; 
 
			//Optional. Should only use in debug builds. 
			//	Register an event-handler to get exceptions in HockeySDK code that are "swallowed" (like problems writing crashlogs etc.) 
#if DEBUG 
			((HockeyClient)HockeyClient.Current).OnHockeySDKInternalException += (sender, args) => 
				{ 
					if (Debugger.IsAttached) 
					{ 
						Debugger.Break(); 
					} 
				}; 
#endif	//DEBUG
 
			//send stored crashlogs to HockeyApp server
			bool x = await HockeyClient.Current.SendCrashesAsync( false );		//true == send automatically (no prompt)

			//check for updates on the HockeyApp server
//			await HockeyApp.HockeyClient.Current.CheckForUpdatesAsync(true, () =>
//				{  
//					//Replace MainWindow with your main Window or use another way to gracefully shutdown your app
//					if (Application.Current.MainWindow != null) { Application.Current.MainWindow.Close(); }
//						return true;
//				} );
		}
	}
#endif	//USE_HOCKEYAPP
}
