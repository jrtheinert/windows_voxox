﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Utils;		//Logger

using System;

using Microsoft.Win32;		//Registry 


namespace Desktop.Model
{
    /// <summary>
    /// Class to retrieve and store data as key-value pairs in windows registry
    /// </summary>
    /// <example>Example depicting how to instantiate and read keys
    /// <code>
    /// WE WILL NOT BE STORING APP/USER RELATED VALUES IN REGISTRY.
	///		THIS IS IS SOLELY FOR OS LEVEL WORK LIKE AUTO-START OF THE APP.
    /// 
    /// string strAppVersion = regMgr.Read("Version");
    /// 
    /// </code>
    /// </example>
    
	//We do NOT want Registry logic throughout our main code, so let's keep it all here and
	//	just provide some simple methods.  ENCAPSULATION!

	public class RegistryManager
    {
        private static readonly VXLogger logger = VXLoggingManager.INSTANCE.GetLogger("VXRegistryManager");

        /// <summary>
        /// HKEY_CURRENT_USER is the base registry
		///	We use HKCU because HKLM requires higher privileges on PCs.  Privileges that are not typical for an Application.
		///		INSTALLERS may have these privileges, but then our USER could not control AutoStart without raised privileges.
        /// </summary>
		///
        private static RegistryKey	sBaseUserRegistryKey = Registry.CurrentUser;		//For HKCU, this will show under HKEY_LOCAL_MACHINE\SOFTWARE
//      private static RegistryKey	sBaseUserRegistryKey = Registry.LocalMachine;		//For HKLM, this will show in RegEdit under HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node

		//AutoStart
        private static String		sAutoStartKeyName = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run";

		#region AutoStart

		public static bool AutoStartIsInStartup( String appName )
        {
            try
            {
				RegistryKey regKey = sBaseUserRegistryKey.CreateSubKey( sAutoStartKeyName, RegistryKeyPermissionCheck.ReadWriteSubTree );	//<<<

                return (regKey.GetValue(appName) != null);
            }
            catch (Exception ex)
            {
                logger.Error("AutoStartIsInStartup", ex);
                return false;
            }
        }

        public static bool AutoStartRegisterFromStartup( String appName, String executablePath )
        {
			bool result = false;

            try
            {
				RegistryKey regKey = sBaseUserRegistryKey.CreateSubKey( sAutoStartKeyName, RegistryKeyPermissionCheck.ReadWriteSubTree );	//<<<

			   if (regKey != null && !AutoStartIsInStartup( appName ) )
				{
					try
					{
						// Remove the value from the registry so that the application doesn't start
						regKey.SetValue( appName, executablePath);
						result = true;
					}
					catch (Exception ex)
					{
						logger.Error("AutoStartRegisterFromStartup", ex);
					}
				}
			}
            catch (Exception ex)
            {
                logger.Error("AutoStartRemoveFromStartup", ex);
            }

			return result;
		}

        public static bool AutoStartRemoveFromStartup( String appName )
        {
			bool result = false;

            try
            {
				RegistryKey regKey = sBaseUserRegistryKey.OpenSubKey( sAutoStartKeyName, true );	//<<<

				if (regKey != null && AutoStartIsInStartup( appName ) == true)
				{
					try
					{
						// Remove the value from the registry so that the application doesn't start
						regKey.DeleteValue(appName, false);
						result = true;
					}
					catch (Exception ex)
					{
						logger.Error("AutoStartRemoveFromStartup", ex);
					}
				}
			}
            catch (Exception ex)
            {
                logger.Error("AutoStartRemoveFromStartup", ex);
            }

			return result;
		}

		#endregion


		#region Base Registry class

		public class VxRegistryBase
		{
			protected	String		mRegistryRootName = null;
			protected	RegistryKey mRegKey			  = null;

			public VxRegistryBase()
			{
			}

			protected void init()
			{
				mRegKey	= sBaseUserRegistryKey.CreateSubKey( mRegistryRootName, RegistryKeyPermissionCheck.ReadWriteSubTree );	//<<<
			}

			public bool HasKey( String keyName )
			{
				return Read( keyName ) != null;
			}

			public bool Write( String keyName, object keyValue )
			{
				bool result = false;

				if ( mRegKey != null )
				{ 
					mRegKey.SetValue( keyName, keyValue);
					result = true;
				}

				return result;
			}

			public String ReadAsString( String keyName, String defaultValue )
			{
				return ToString( Read( keyName ), defaultValue );
			}

			public Boolean ReadAsBool( String keyName, bool defaultValue )
			{
				return ToBool( Read( keyName ), defaultValue );
			}

			public int ReadAsInt( String keyName, int defaultValue )
			{
				return ToInt( Read( keyName ), defaultValue );
			}

			public long ReadAsLong( String keyName, long defaultValue )
			{
				return ToLong( Read( keyName ), defaultValue );
			}

            public double ReadAsDouble(String keyName, double defaultValue)
            {
                return ToDouble(Read(keyName), defaultValue);
            }

			private string ToString( object obj, String defaultValue )
			{
				return ( obj == null ? defaultValue : obj.ToString() );
			}

			private bool ToBool( object obj, bool defaultValue )
			{
				bool result = defaultValue;

				if ( obj != null )
				{ 
					bool prm;

					if ( bool.TryParse( obj.ToString(), out prm ) )
					{
						result = prm;
					}
				}

				return result;
			}

			private int ToInt( object obj, int defaultValue )
			{
				int result = defaultValue;

				if ( obj != null )
				{ 
					int prm;

					if ( int.TryParse( obj.ToString(), out prm ) )
					{
						result = prm;
					}
				}

				return result;
			}

			private long ToLong( object obj, long defaultValue )
			{
				long result = defaultValue;

				if ( obj != null )
				{ 
					long prm;

					if ( long.TryParse( obj.ToString(), out prm ) )
					{
						result = prm;
					}
				}

				return result;;
			}

            private double ToDouble(object obj, double defaultValue)
            {
                double result = defaultValue;

                if (obj != null)
                {
                    double prm;

                    if (double.TryParse(obj.ToString(), out prm))
                    {
                        result = prm;
                    }
                }

                return result; ;
            }

			private object Read( String keyName )
			{
				object result = null;

				if ( mRegKey != null )
				{
					result = mRegKey.GetValue( keyName );
				}

				return result;
			}
		}

		#endregion
    }
}
