﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Config;			//LoginMode
using Desktop.Utils;			//Logger

using Vxapi23.Constants;		//RestfulContants (API settings)

using ManagedDataTypes;			//UserSettings
using Telcentris.Voxox.ManagedSipAgentApi.ManagedSipDataTypes;	//SipAgent
using Telcentris.Voxox.ManagedXmppApi.ManagedXmppDataTypes;		//XmppParameters

using System;
using System.IO;				//Path (for sound files)
using System.Reflection;		//Assembly

namespace Desktop.Model
{
	/// <summary>
    /// The Desktop Application Settings Manager class
    /// </summary>
    public sealed class SettingsManager
    {
        #region | Singleton |

        public static SettingsManager Instance
        {
            get { return Nested.instance; }
        }

        class Nested
        {
            static Nested()
            {
            }

            internal static readonly SettingsManager instance = new SettingsManager();
        }

        #endregion

        #region | Private Class Variables |

        private static VXLogger logger = VXLoggingManager.INSTANCE.GetLogger("SettingsManager");

		private bool mInitializingUser   = false;

		//Separate functionality-related settings classes
		public SettingsManager.ADVANCED Advanced;
		public SettingsManager.APP		App;
//		public SettingsManager.API		Api;		//Let's try to use Desktop.Config.Api
		public SettingsManager.SIP		Sip;
//		public SettingsManager.XMPP		Xmpp;		//Let's try to use Desktop.Config.Xmpp
		public SettingsManager.USER		User;

        #endregion

		private SettingsManager()
		{
			//These are defined at end of this file.
//			App		 = new SettingsManager.APP();
//			Api		 = new SettingsManager.API();
			Sip		 = new SettingsManager.SIP();
//			Xmpp	 = new SettingsManager.XMPP();
//			User	 = new SettingsManager.USER();		//This will be instantiated after user login.
//			Advanced = new SettingsManager.ADVANCED();	//Instantiate this in Initialize(), since some default values use other objects.
		}


		#region | Global Public Procedures |

		/// <summary>
		/// Initialize Application Settings (Non-User specific initialization)
		/// </summary>
		public void Initialize()
		{
			bool initRest = false;

			if ( Advanced == null )
			{ 
				Advanced = new SettingsManager.ADVANCED();	//Instantiate this outside ctor, since some default values use other objects.
				initRest = true;
			}

			logger.Debug("Initializing global settings");

			if ( App == null )
			{ 
				App		 = new SettingsManager.APP();
				initRest = true;
			}

			//At this point, all settings are at default values, based on Desktop.Config classes OR AppSettings table.

			//Logging - Move handling of IgnoreLoggers to VxLogger class.
//			if (regForGlobal.HasKey( SettingType.IgnoreLoggers.ToString() ) == false)
//			{
////                ignoreLoggers = "SipAgent;XmppManager;";
//			}
//			else
//			{
////				Api.IgnoreLoggers = regForGlobal.ReadAsString( SettingType.IgnoreLoggers.ToString(), Api.IgnoreLoggers );
//			}

			if ( initRest )
			{ 
				initRestfulContants();
			}
		}

		//Initialize VXAPI constants
		private void initRestfulContants()
		{
			//RestfulConstants.ApiServer					= Api.ApiServer;
			//RestfulConstants.ApiKey						= Api.ApiKey;
			//RestfulConstants.ApiModuleBase				= Api.ApiModuleBase;
			//RestfulConstants.ApiAuthModuleBase			= Api.ApiAuthModuleBase;
			//RestfulConstants.HttpFaxModuleBase			= Api.HttpFaxModuleBase;
			//RestfulConstants.HttpFileUploadModuleBase	= Api.HttpFileUploadModuleBase;
			//RestfulConstants.HttpFileDownloadModuleBase = Api.HttpFileDownloadModuleBase;
			//RestfulConstants.CallBackModuleBase			= Api.CallBackModuleBase;

			RestfulConstants.ApiServer					= Desktop.Config.API.ApiServer;
//			RestfulConstants.ApiKey						= Desktop.Config.API.ApiKey;
			RestfulConstants.ApiModuleBase				= Desktop.Config.API.ApiModuleBase;
			RestfulConstants.ApiAuthModuleBase			= Desktop.Config.API.ApiAuthModuleBase;
			RestfulConstants.HttpFaxModuleBase			= Desktop.Config.API.HttpFaxModuleBase;
			RestfulConstants.HttpFileUploadModuleBase	= Desktop.Config.API.HttpFileUploadModuleBase;
			RestfulConstants.HttpFileDownloadModuleBase = Desktop.Config.API.HttpFileDownloadModuleBase;
//			RestfulConstants.CallBackModuleBase			= Desktop.Config.API.CallBackModuleBase;		//Not used anywhere.

			//ClientInfo data
			RestfulConstants.ClientId					= Desktop.Config.Branding.ClientId;
			RestfulConstants.ClientVersion				= Assembly.GetExecutingAssembly().GetName().Version.ToString();
		}

		public XmppParameters getDefaultXmppParameters()
		{
			XmppParameters xmppParameters = new XmppParameters();

			xmppParameters.Server				= Config.Xmpp.ServerEx;		//ServerEx takes Advanced Settings into consideration.
			xmppParameters.Port					= Config.Xmpp.PortEx;		//PortEx   takes Advanced Settings into consideration.
			xmppParameters.Domain				= Config.Xmpp.Domain;
			xmppParameters.ResourcePrefix		= Config.Xmpp.ResourcePrefix;
			xmppParameters.TlsOption			= Config.Xmpp.TlsOption;
			xmppParameters.AuthMechanism		= Config.Xmpp.AuthMechanism;
			xmppParameters.AllowPlainPassword	= Config.Xmpp.AllowPlainPassword;

			xmppParameters.UseProxy				= Config.Xmpp.UseProxy;
			xmppParameters.ProxyServer			= Config.Xmpp.ProxyServer;
			xmppParameters.ProxyPort			= Config.Xmpp.ProxyPort;

			if ( Config.Xmpp.hasOverrides() )
			{
                logger.Warn( "Using Non-Standard Xmpp Server: " + xmppParameters.Server );
            }


			return xmppParameters;
		}

		#endregion

		#region | User Public Procedures |

		/// <summary>
		/// Call this function to set the user's settings
		/// </summary>
		/// <param name="Username"></param>
		public void InitializeForUser( string Username )
		{
			logger.Debug("Initializing settings for user " + Username);

			mInitializingUser = true;

			User = new SettingsManager.USER();

			mInitializingUser = false;

			//Post initialization
			if ( String.IsNullOrEmpty( User.SipUuid ) )
			{
				String uuid = Guid.NewGuid().ToString();
				User.SipUuid = uuid;
			}
		}

		public void CloseUserSettings()
		{
			User.Clear();
		}
	
		#endregion


        #region User Settings Class

		//This class will have most interaction between User and AppDB.
		public class USER
		{
			private bool	mInitializing = true;
			private bool	mIsDirty      = false;
			private bool	mBulkUpdate	  = false;

			//Private properties
			private string	mAudioInputDevice;
			private string	mAudioOutputDevice;
			private int		mAudioInputVolume;
			private int		mAudioOutputVolume;
			private bool	mShowFaxesInChatThread;
			private bool	mShowVoicemailsInChatThread;
			private bool	mShowRecordedCallsInChatThread;
			private bool	mEnableSoundForIncomingChat;
			private bool	mEnableSoundForIncomingCall;
//			private bool	mEnableSoundForOutgoingCall;	//See NOTE in SoundsViewModel
			private string	mMyChatBubbleColor;
			private string	mMyConversantChatBubbleColor;
			private string	mCountryCode;
			private string	mCountryAbbreviation;

			private string	mIncomingCallSoundFile;
			private string	mOutgoingCallSoundFile;
			private string	mCallClosedSoundFile;
			private string  mIncomingMessageSoundFile;

			//These are set from API/DB calls.
			private long	mLastMessageTimestamp;
			public long		mLastCallTimestamp;
			public string	mLastContactSourceKey;

			//SIP
			private String	mSipUuid;

            //WindowState
            private double mWindowLeft;
            private double mWindowTop;
            private double mWindowHeight;
            private double mWindowWidth;
            private int	   mWindowState;

			private ManagedDbManager.ManagedDbManager getUserDbm()
			{
				return SessionManager.Instance.UserDataStore;
			}

			private void setBulkUpdate( bool enable )
			{
				mBulkUpdate = enable;

				//Trigger Save, if disabled.
				if ( !mBulkUpdate )
				{
					SaveToDb();
				}
			}

			private void SaveToDb()
			{
				if ( !mInitializing && !mBulkUpdate && mIsDirty )
				{ 
					UserSettings settings = new UserSettings();

					settings.UserId						= SessionManager.Instance.User.Username;

					settings.AudioInputDevice			= AudioInputDevice;
					settings.AudioOutputDevice			= AudioOutputDevice;
					settings.AudioInputVolume			= AudioInputVolume;
					settings.AudioOutputVolume			= AudioOutputVolume;

					settings.ShowFaxInMsgThread			= ShowFaxesInChatThread;
					settings.ShowVmInMsgThread			= ShowVoicemailsInChatThread;
					settings.ShowRcInMsgThread			= ShowRecordedCallsInChatThread;

					settings.EnableSoundIncomingMsg		= EnableSoundForIncomingChat;	//TODO: Rename this.
					settings.EnableSoundIncomingCall	= EnableSoundForIncomingCall;
	//				settings.EnableSoundOutgoingMsg		= mEnableSoundForIncomingMsg

					settings.MyChatBubbleColor			= MyChatBubbleColor;	
					settings.ConversantChatBubbleColor	= MyConversantChatBubbleColor;	

					settings.CountryCode				= CountryCode;
					settings.CountryAbbrev				= CountryAbbreviation;

	//				settings.OutgoindCallSoundFile		= OutgoingCallSoundFile;
					settings.IncomingCallSoundFile		= IncomingCallSoundFile;
					settings.CallClosedSoundFile		= CallClosedSoundFile;

					settings.WindowLeft					= WindowLeft;
					settings.WindowTop					= WindowTop;
					settings.WindowHeight				= WindowHeight;
					settings.WindowWidth				= WindowWidth;
					settings.WindowState				= WindowState;		//TODO: Get WindowState Enum

					settings.LastMsgTimestamp			= LastMessageTimestamp;
					settings.LastCallTimestamp			= LastCallTimestamp;
					settings.LastContactSourceKey		= LastContactSourceKey;

					settings.SipUuid					= SipUuid;
				
					bool result = getUserDbm().updateUserSettings( settings );

					mIsDirty = false;
				}
			}

			public void UpdateFromDb()
			{
				UserSettings settings = getUserDbm().getUserSettings();

				if ( settings.isValid() )
				{
//					UserId							= settings.UserId;;					//This is not part of UserSettings.

					AudioInputDevice				= settings.AudioInputDevice;
					AudioOutputDevice				= settings.AudioOutputDevice;
					AudioInputVolume				= settings.AudioInputVolume;
					AudioOutputVolume				= settings.AudioOutputVolume;

					ShowFaxesInChatThread			= settings.ShowFaxInMsgThread;
					ShowVoicemailsInChatThread		= settings.ShowVmInMsgThread;
					ShowRecordedCallsInChatThread	= settings.ShowRcInMsgThread;

					EnableSoundForIncomingChat		= settings.EnableSoundIncomingMsg;		//TODO: Rename this
					EnableSoundForIncomingCall		= settings.EnableSoundIncomingCall;
	//				mEnableSoundForIncomingMsg		= settings.EnableSoundOutgoingMsg;

					MyChatBubbleColor				= settings.MyChatBubbleColor;
					MyConversantChatBubbleColor		= settings.ConversantChatBubbleColor;

					CountryCode						= settings.CountryCode;
					CountryAbbreviation				= settings.CountryAbbrev;

	//				OutgoingCallSoundFile			= settings.OutgoindCallSoundFile;
					IncomingCallSoundFile			= settings.IncomingCallSoundFile;
					CallClosedSoundFile				= settings.CallClosedSoundFile;

					WindowLeft						= settings.WindowLeft;
					WindowTop						= settings.WindowTop;
					WindowHeight					= settings.WindowHeight;
					WindowWidth						= settings.WindowWidth;
					WindowState						= settings.WindowState;							//TODO: Get WindowState Enum

					LastMessageTimestamp			= settings.LastMsgTimestamp;
					LastCallTimestamp				= settings.LastCallTimestamp;
					LastContactSourceKey			= settings.LastContactSourceKey;

					SipUuid							= settings.SipUuid;
				}
			}

			public void saveWindowSize( double width, double height )
			{
				setBulkUpdate( true );

				WindowWidth  = width;
				WindowHeight = height;

				setBulkUpdate( false );

			}

			public void saveWindowPos( double top, double left )
			{
				setBulkUpdate( true );

				WindowTop  = top;
				WindowLeft = left;

				setBulkUpdate( false );
			}

			//Public properties
			public string AudioInputDevice				
			{ 
				get { return mAudioInputDevice;		}
				set {	mIsDirty = ( mAudioInputDevice != value );
						mAudioInputDevice = value; 
						SaveToDb();
					}
			}

			public string AudioOutputDevice				
			{ 
				get { return mAudioOutputDevice; }
				set {	mIsDirty = ( mAudioOutputDevice != value );
						mAudioOutputDevice = value; 
						SaveToDb();
					}
			}

			public int AudioInputVolume				
			{ 
				get { return mAudioInputVolume; }
				set {	mIsDirty = ( mAudioInputVolume != value );
						mAudioInputVolume = value; 
						SaveToDb();
					}
			}
			
			public int AudioOutputVolume				
			{ 
				get { return mAudioOutputVolume; }
				set {	mIsDirty = ( mAudioOutputVolume != value );
						mAudioOutputVolume = value; 
						SaveToDb();
					}
			}

			public bool ShowFaxesInChatThread			
			{ 
				get { return mShowFaxesInChatThread; }
				set {	mIsDirty = ( mShowFaxesInChatThread != value );
						mShowFaxesInChatThread = value; 
						SaveToDb();
					}
			}
			
			public bool	ShowVoicemailsInChatThread
			{ 
				get { return mShowVoicemailsInChatThread; } 
				set {	mIsDirty = ( mShowVoicemailsInChatThread != value );
						mShowVoicemailsInChatThread = value; 
						SaveToDb();
					}
			}
			
			public bool	ShowRecordedCallsInChatThread
			{ 
				get { return mShowRecordedCallsInChatThread; }
				set {	mIsDirty = ( mShowRecordedCallsInChatThread != value );
						mShowRecordedCallsInChatThread = value; 
						SaveToDb();
					}
			}
			
			//These next three properties depend on Config.Settings.EnableSMS which
			//	may override user's settings.
			public bool ShowFaxesInChatThreadEx
			{
				get { return Config.Settings.EnableSMS && ShowFaxesInChatThread; }
			}

			public bool	ShowVoicemailsInChatThreadEx
			{
				get { return Config.Settings.EnableSMS && ShowVoicemailsInChatThread; }
			}

			public bool	ShowRecordedCallsInChatThreadEx
			{
				get { return Config.Settings.EnableSMS && ShowRecordedCallsInChatThread; }
			}

			public bool	EnableSoundForIncomingChat
			{ 
				get { return mEnableSoundForIncomingChat; }
				set {	mIsDirty = ( mEnableSoundForIncomingChat != value );
						mEnableSoundForIncomingChat = value; 
						SaveToDb();
					}
			}
			
			public bool	EnableSoundForIncomingCall
			{ 
				get { return mEnableSoundForIncomingCall; }
				set {	mIsDirty = ( mEnableSoundForIncomingCall != value );
						mEnableSoundForIncomingCall = value; 
						SaveToDb();
					}
			}
			
			//See NOTE in SoundsViewModel
			//public bool	EnableSoundForOutgoingCall	
			//{ 
			//	get { return mEnableSoundForOutgoingCall; }
			//	set {	mIsDirty = ( mEnableSoundForOutgoingCall != value );
			//			mEnableSoundForOutgoingCall = value; 
			//			SaveToDb();
			//		}
			//}
			
			public string MyChatBubbleColor
			{ 
				get { return mMyChatBubbleColor; }
				set {	mIsDirty = ( mMyChatBubbleColor != value );
						mMyChatBubbleColor = value; 
						SaveToDb();
					}
			}
			
			public string MyConversantChatBubbleColor
			{ 
				get { return mMyConversantChatBubbleColor; }
				set {	mIsDirty = ( mMyConversantChatBubbleColor != value );
						mMyConversantChatBubbleColor = value; 
						SaveToDb();
					}
			}
			
			public string CountryCode
			{ 
				get { return mCountryCode; }
				set {	mIsDirty = ( mCountryCode != value );
						mCountryCode = value; 
						SaveToDb();
					}
			}
			
			public string CountryAbbreviation
			{ 
				get { return mCountryAbbreviation; }
				set {	mIsDirty = ( mCountryAbbreviation != value );
						mCountryAbbreviation = value; 
						SaveToDb();
					}
			}

			public string IncomingCallSoundFile
			{ 
				get { return mIncomingCallSoundFile; }
				set {	mIsDirty = ( mIncomingCallSoundFile != value );
						mIncomingCallSoundFile = value; 
						SaveToDb();
					}
			}
			
			public string OutgoingCallSoundFile
			{ 
				get { return mOutgoingCallSoundFile; }
				set {	mIsDirty = ( mOutgoingCallSoundFile != value );
						mOutgoingCallSoundFile = value; 
						SaveToDb();
					}
			}
			
			public string CallClosedSoundFile
			{ 
				get { return mCallClosedSoundFile; }
				set {	mIsDirty = ( mCallClosedSoundFile != value );
						mCallClosedSoundFile = value; 
						SaveToDb();
					}
			}
			
			public string IncomingMessageSoundFile
			{ 
				get { return mIncomingMessageSoundFile; }
				set {	mIsDirty = ( mIncomingMessageSoundFile != value );
						mIncomingMessageSoundFile = value; 
						SaveToDb();
					}
			}

			//These are set from API/DB calls.
			public long	LastMessageTimestamp
			{ 
				get { return mLastMessageTimestamp; }
				set {	mIsDirty = ( mLastMessageTimestamp != value );
						mLastMessageTimestamp = value; 
						SaveToDb();
					}
			}
			
			public long	LastCallTimestamp
			{ 
				get { return mLastCallTimestamp; }
				set {	mIsDirty = ( mLastCallTimestamp != value );
						mLastCallTimestamp = value; 
						SaveToDb();
					}
			}

			public string LastContactSourceKey
			{ 
				get { return mLastContactSourceKey; }
				set {	mIsDirty = ( mLastContactSourceKey != value );
						mLastContactSourceKey = value; 
						SaveToDb();
					}
			}

			//SIP
			public string SipUuid
			{ 
				get { return mSipUuid; }
				set {	mIsDirty = ( mSipUuid != value );
						mSipUuid = value; 
						SaveToDb();
					}
			}

            //WindowState
            public double WindowLeft
            {
                get { return mWindowLeft; }
				set {	mIsDirty = ( mWindowLeft != value );
						mWindowLeft = value; 
						SaveToDb();
					}
            }

            public double WindowTop
            {
                get { return mWindowTop; }
				set {	mIsDirty = ( mWindowTop != value );
						mWindowTop = value; 
						SaveToDb();
					}
            }

            public double WindowHeight
            {
                get { return mWindowHeight; }
				set {	mIsDirty = ( mWindowHeight != value );
						mWindowHeight = value; 
						SaveToDb();
					}
            }

            public double WindowWidth
            {
                get { return mWindowWidth; }
				set {	mIsDirty = ( mWindowWidth != value );
						mWindowWidth = value; 
						SaveToDb();
					}
            }

            public int WindowState
            {
                get { return mWindowState; }
				set {	mIsDirty = ( mWindowState != value );
						mWindowState = value; 
						SaveToDb();
					}
            }

			public USER()
			{
				mInitializing = true;

				resetToDefaults();
				UpdateFromDb();

				mInitializing = false;
			}

			public void Clear()
			{
				mInitializing = true;

				resetToDefaults();

				mInitializing = false;
			}

			//User selectable Settings.  These may be overridden by AppDB.
			//	We need to reset them when user logs in/out.
			private void resetToDefaults()
			{ 
				AudioInputDevice				= Desktop.Config.Settings.AudioInputDevice;
				AudioOutputDevice				= Desktop.Config.Settings.AudioOutputDevice;
				AudioInputVolume				= Desktop.Config.Settings.AudioInputVolume;
				AudioOutputVolume				= Desktop.Config.Settings.AudioOutputVolume;
				ShowFaxesInChatThread			= Desktop.Config.Settings.ShowFaxesInChatThread;
				ShowVoicemailsInChatThread		= Desktop.Config.Settings.ShowVoicemailsInChatThread;
				ShowRecordedCallsInChatThread	= Desktop.Config.Settings.ShowRecordedCallsInChatThread;
				EnableSoundForIncomingChat		= Desktop.Config.Settings.EnableSoundForIncomingChat;
				EnableSoundForIncomingCall		= Desktop.Config.Settings.EnableSoundForIncomingCall;
//				EnableSoundForOutgoingCall		= Desktop.Config.Settings.EnableSoundForOutgoingCall;	//See NOTE in SoundsViewModel
				MyChatBubbleColor				= Desktop.Config.Settings.MyChatBubbleColor;
				MyConversantChatBubbleColor		= Desktop.Config.Settings.MyConversantChatBubbleColor;
				CountryCode						= Desktop.Config.Settings.CountryCode;
				CountryAbbreviation				= Desktop.Config.Settings.CountryAbbreviation;

				IncomingCallSoundFile			= Path.Combine( SessionManager.Instance.AppStartupPath, Desktop.Config.Settings.IncomingCallSoundFile	);
				OutgoingCallSoundFile			= Path.Combine( SessionManager.Instance.AppStartupPath, Desktop.Config.Settings.OutgoingCallSoundFile	);
				CallClosedSoundFile				= Path.Combine( SessionManager.Instance.AppStartupPath, Desktop.Config.Settings.CallClosedSoundFile		);
				IncomingMessageSoundFile		= Path.Combine( SessionManager.Instance.AppStartupPath, Desktop.Config.Settings.IncomingMessageSoundFile);

				//These are set from API/DB calls.
				LastMessageTimestamp			= 0;
				LastCallTimestamp				= 0;
				LastContactSourceKey			= "";

				//For SIP
				SipUuid							= "";

                //WindowState
                WindowLeft		= 0.0;
                WindowTop		= 0.0;
                WindowHeight	= 0.0;
                WindowWidth		= 0.0;
                WindowState		= 0;
			}
		}

        #endregion

		#region Advanced Settings class

    /// <summary>
    /// This class is used in maintaining Advanced/QA settings. 
    /// The values defined here are used in overriding the defaults/initial settings based on the booleans.
    /// e.g., Setting UseAPIServer="true" uses the "APIServer" value in making the VX API calls
	/// 
	/// NOTE: These values are NOT persisted between logins.
    /// </summary>
		public class ADVANCED
		{
			private bool	mUseApiServer;
//			private bool	mUseProductionApiKey;

			public ADVANCED()
			{
				resetToDefaults();
			}

			public string ApiServer { get; set; }

			public bool UseApiServer
			{
				get { return mUseApiServer; }
				set
				{
					mUseApiServer = value;
					RestfulConstants.ApiServer = ( mUseApiServer ? ApiServer : Desktop.Config.API.ApiServer );
				}
			}

			public string	XmppServer		{ get; set; }
			public bool		UseXmppServer	{ get; set; }	//TODO-QA: Similar to UseApiServer?
			
			public string	SipServer		{ get; set; }
			public bool		UseSipServer	{ get; set; }

			//public bool UseProductionApiKey
			//{
			//	get { return mUseProductionApiKey; }
			//	set
			//	{
			//		mUseProductionApiKey = value;
			//		Config.API.UseProductionApiKey = value;
			//		RestfulConstants.ApiKey = Desktop.Config.API.ApiKey;
			//	}
			//}

			public bool EnableTURN { get; set; }			//TODO-QA

			//These belong to V2 of Advanced settings
			public string	 ApiEnv				{ get; set; }
			public string	 ApiEnvModifier		{ get; set; }
			public string	 PartnerIdentifier	{ get; set; }
			public LoginMode LoginMode			{ get; set; }

			public void saveV2( String apiServer, String xmppServer, String extranetHost, String partnerId )
			{
				//API server
				RestfulConstants.ApiServer = ( String.IsNullOrEmpty( apiServer ) ? Desktop.Config.API.ApiServer : apiServer );

				//XMPP server
				XmppServer    = xmppServer;
				UseXmppServer = !String.IsNullOrEmpty( xmppServer );

				ActionManager.Instance.Rebrand( apiServer, xmppServer, extranetHost, partnerId, LoginMode );
			}


			public void resetToDefaults()
			{
				resetToDefaultsV1();
				resetToDefaultsV2();
			}

			public void resetToDefaultsV1()
			{
				ApiServer			= string.Empty;
				UseApiServer		= false;

				XmppServer			= string.Empty;
				UseXmppServer		= false;

				SipServer			= string.Empty;
				UseSipServer		= false;

//				UseProductionApiKey = true;
				EnableTURN			= true;
			}

			public void resetToDefaultsV2()
			{
				ApiEnv				= string.Empty;
				ApiEnvModifier		= string.Empty;
				PartnerIdentifier   = string.Empty;

				SipServer			= string.Empty;
				UseSipServer		= false;

//				UseProductionApiKey = true;
				EnableTURN			= true;

				ActionManager.Instance.UnRebrand( false );
			}
		}

		#endregion

		#region APP-level settings class

		public class APP
		{ 
			private bool	mInitializing = true;
			private bool	mIsDirty      = false;

			bool	mAutoLogin;
			bool	mRememberPassword;
			string	mLastLoginUserName;
			string	mLastLoginPassword;
            string  mLastLoginCountryCode;
            string  mLastLoginPhoneNumber;

			//Debug items
			string	mDebugMenu;				//This value will be compared to a secret string for security
			string  mIgnoreLoggers;
			bool	mMaxSipLogging;
			int		mGetInitOptionsHandling;

			private ManagedDbManager.ManagedDbManager getAppDbm()
			{
				return SessionManager.Instance.AppDataStore;
			}

			private void SaveToDb()
			{
				if ( !mInitializing && mIsDirty )
				{ 
					AppSettings settings = new AppSettings();

//					settings.RecorId					= 0;		//DBM will handle this.

					settings.AutoLogin					= AutoLogin;
					settings.RememberPassword			= RememberPassword;
					settings.LastLoginUsername			= LastLoginUserName;
					settings.LastLoginPassword			= LastLoginPassword;
					settings.LastLoginCountryCode		= LastLoginCountryCode;
					settings.LastLoginPhoneNumber		= LastLoginPhoneNumber;

					settings.DebugMenu					= DebugMenu;
					settings.IgnoreLoggers				= IgnoreLoggers;
					settings.MaxSipLogging				= MaxSipLogging;
					settings.GetInitOptionsHandling		= GetInitOptionsHandling;

					bool result = getAppDbm().updateAppSettings( settings );

					mIsDirty = false;
				}
			}


			public void UpdateFromDb()
			{
				AppSettings settings = getAppDbm().getAppSettings();

				if ( settings.isValid() )
				{
					AutoLogin					= settings.AutoLogin;
					RememberPassword			= settings.RememberPassword;
					LastLoginUserName			= settings.LastLoginUsername;
					LastLoginPassword			= settings.LastLoginPassword;
					LastLoginCountryCode		= settings.LastLoginCountryCode;
					LastLoginPhoneNumber		= settings.LastLoginPhoneNumber;

					DebugMenu					= settings.DebugMenu;
					mIgnoreLoggers				= settings.IgnoreLoggers;
					mMaxSipLogging				= settings.MaxSipLogging;
					mGetInitOptionsHandling		= settings.GetInitOptionsHandling;
				}
			}

			//These will have defaults from Desktop.Config, but may have AppDB overrides
			public bool	AutoLogin				
			{ 
				get { return mAutoLogin; }
				set {	mIsDirty = ( mAutoLogin != value );
						mAutoLogin = value; 
						SaveToDb();
					}
			}

			public bool	RememberPassword
			{ 
				get { return mRememberPassword; }
				set {	mIsDirty = ( mRememberPassword != value );
						mRememberPassword = value; 
						SaveToDb();
					}
			}

			public string LastLoginUserName
			{ 
				get { return mLastLoginUserName; }
				set {	mIsDirty = ( mLastLoginUserName != value );
						mLastLoginUserName = value; 
						SaveToDb();
					}
			}

			public string LastLoginPassword
			{ 
				get { return mLastLoginPassword; }
				set {	mIsDirty = ( mLastLoginPassword != value );
						mLastLoginPassword = value; 
						SaveToDb();
					}
			}

			public string LastLoginCountryCode
			{ 
				get { return mLastLoginCountryCode; }
				set {	mIsDirty = ( mLastLoginCountryCode != value );
						mLastLoginCountryCode = value; 
						SaveToDb();
					}
			}

			public string LastLoginPhoneNumber
			{ 
				get { return mLastLoginPhoneNumber; }
				set {	mIsDirty = ( mLastLoginPhoneNumber != value );
						mLastLoginPhoneNumber = value; 
						SaveToDb();
					}
			}

			//Debug items
			public string DebugMenu
			{ 
				get { return mDebugMenu; }
				set {	mIsDirty = ( mDebugMenu != value );
						mDebugMenu = value; 
						SaveToDb();
					}
			}

			public string IgnoreLoggers
			{ 
				get { return mIgnoreLoggers; }
				set {	mIsDirty = ( mIgnoreLoggers != value );
						mIgnoreLoggers = value; 
						SaveToDb();
					}
			}

			public bool MaxSipLogging
			{ 
				get { return mMaxSipLogging; }
				set {	mIsDirty = ( mMaxSipLogging != value );
						mMaxSipLogging = value; 
						SaveToDb();
					}
			}

			public int GetInitOptionsHandling
			{ 
				get { return mGetInitOptionsHandling; }
				set {	mIsDirty = ( mGetInitOptionsHandling != value );
						mGetInitOptionsHandling = value; 
						SaveToDb();
					}
			}

			public bool GetInitOptionsHandlingIsNormal()			{  return mGetInitOptionsHandling == 0; }
			public bool GetInitOptionsHandlingIsIgnore()			{  return mGetInitOptionsHandling == 1; }
			public bool GetInitOptionsHandlingIsSimulateFailure()	{  return mGetInitOptionsHandling == 2; }

			public APP()
			{
				mInitializing = true;

				resetToDefaults();
				UpdateFromDb();

				mInitializing = false;
			}

			//These will have defaults from Desktop.Config, but may have DB overrides
			private void resetToDefaults()
			{ 
				AutoLogin				= Desktop.Config.Settings.AutoLogin;
				RememberPassword		= Desktop.Config.Settings.RememberPassword;
				LastLoginUserName		= "";
				LastLoginPassword		= "";
                LastLoginCountryCode    = string.Format("{0}|{1}", Desktop.Config.Settings.CountryCode, Desktop.Config.Settings.CountryAbbreviation);
                LastLoginPhoneNumber    = "";

				//Debug items
				DebugMenu					 = "";
				IgnoreLoggers				 = "";
				MaxSipLogging				 = false;
			}
		}

        #endregion

        #region | SIP Settings class |

		public class SIP
		{
			#region SIP Settings Public Properties

			public bool		UseTls								{ get; set;	}
			public bool		DoProxyCheck						{ get; set;	}

			public SipAgent.Topology		Topology			{ get; set;	}
			public SipAgent.CallEncryption CallEncryption		{ get; set;	}

			public bool		EnableIceMedia						{ get; set;	}
			public bool		EnableIceSecurity					{ get; set;	}
			public bool		TopologyEncryption					{ get; set;	}
			public bool		TopologyTurned						{ get; set;	}
			public bool		KeepAlive							{ get; set;	}
			public bool		UseRPort							{ get; set;	}
			public int		RegistrationRefreshInterval			{ get; set;	}
			public int		FailureInterval						{ get; set;	}
			public int		OptionsKeepAliveInterval			{ get; set;	}
			public int		MinimumSessionTime					{ get; set;	}
			public bool		SupportSessionTimer					{ get; set;	}
			public bool		InitiateSessionTimer				{ get; set;	}
			public string	TagProlog							{ get; set;	}
			public int		SipOptionRegisterTimeout			{ get; set;	}
			public int		SipOptionPublishTimeout				{ get; set;	}
			public bool		SipOptionUseOptionsRequest			{ get; set;	}
			public bool		SipOptionP2PPresence				{ get; set;	}
			public bool		SipOptionUseTypingState				{ get; set;	}
			public bool		SipOptionChatWithoutPresence		{ get; set;	}
			public bool		EnableAEC							{ get; set;	}
			public bool		EnableAGC							{ get; set;	}
			public bool		EnableNoiseSuppression				{ get; set;	}

			public bool		SipUdp								
			{ 
				get { return ( UseTls ? false : true );		}
//				set;	
			}

			public bool		SipTcp
			{
				get { return ( UseTls ? false : true );		}
//				set;
			}

			public bool		SipTls
			{
				get { return ( UseTls ? true : false );		}
//				set;
			}

			//NOTE:  SipOptionUuid has been moved to USER

			#endregion

			public SIP()
			{
				resetToDefaults();
			}

			private void resetToDefaults()
			{
				UseTls							= Desktop.Config.Sip.UseTls;	//Controls SipUdp, SipTcp and SipTls.
				DoProxyCheck					= Desktop.Config.Sip.DoProxyCheck;

				Topology						= (SipAgent.Topology      ) Desktop.Config.Sip.Topology;
				CallEncryption					= (SipAgent.CallEncryption) Desktop.Config.Sip.CallEncryption;

				EnableIceMedia					= Desktop.Config.Sip.EnableIceMedia;
				EnableIceSecurity				= Desktop.Config.Sip.EnableIceSecurity;
				TopologyEncryption				= Desktop.Config.Sip.TopologyEncryption;
				TopologyTurned					= Desktop.Config.Sip.TopologyTurned;
				KeepAlive						= Desktop.Config.Sip.KeepAlive;
				UseRPort						= Desktop.Config.Sip.UseRPort;
				RegistrationRefreshInterval		= Desktop.Config.Sip.RegistrationRefreshInterval;
				FailureInterval					= Desktop.Config.Sip.FailureInterval;
				OptionsKeepAliveInterval		= Desktop.Config.Sip.OptionsKeepAliveInterval;
				MinimumSessionTime				= Desktop.Config.Sip.MinimumSessionTime;
				SupportSessionTimer				= Desktop.Config.Sip.SupportSessionTimer;
				InitiateSessionTimer			= Desktop.Config.Sip.InitiateSessionTimer;
				TagProlog						= Desktop.Config.Sip.TagProlog;
				SipOptionRegisterTimeout		= Desktop.Config.Sip.SipOptionRegisterTimeout;
				SipOptionPublishTimeout			= Desktop.Config.Sip.SipOptionPublishTimeout;
				SipOptionUseOptionsRequest		= Desktop.Config.Sip.SipOptionUseOptionsRequest;
				SipOptionP2PPresence			= Desktop.Config.Sip.SipOptionP2PPresence;
				SipOptionUseTypingState			= Desktop.Config.Sip.SipOptionUseTypingState;
				SipOptionChatWithoutPresence	= Desktop.Config.Sip.SipOptionChatWithoutPresence;
				EnableAEC						= Desktop.Config.Sip.EnableAEC;
				EnableAGC						= Desktop.Config.Sip.EnableAGC;
				EnableNoiseSuppression			= Desktop.Config.Sip.EnableNoiseSuppression;

				//The value of these depends on UseTls, so no need to 'set' them.
//				SipUdp							= Desktop.Config.Sip.SipUdp;
//				SipTcp							= Desktop.Config.Sip.SipTcp;
//				SipTls							= Desktop.Config.Sip.SipTls;

				//NOTE: SipOptionUuid has been moved to USER
			}
		}

        #endregion
	}
}
