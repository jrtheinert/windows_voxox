﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model.Calling;		//CallManager
using Desktop.Model.Contact;		//ContactManager
using Desktop.Model.Messaging;		//MessageManager
using Desktop.Utils;				//Logger

using Vxapi23;						//RestfulApiManager
using Vxapi23.Model;				//CompanyUserExtendedResponse

using System;
using System.Threading.Tasks;

namespace Desktop.Model
{
    /// <summary>
    /// Sync Manager for Contacts, Messages, Recent calls, Avatars etc.
    /// This can also be run on some timer so that the data is periodically refreshed.
    /// </summary>
    public class SyncManager
    {
        #region | Constructor |

        /// <summary>
        /// Start Sync for User
        /// </summary>
        /// <param name="Username">Username of the logged in user</param>
        public SyncManager(string Username)
        {
            username = Username;
        }

        #endregion

        #region | Private Member Variables |

        private static VXLogger logger = VXLoggingManager.INSTANCE.GetLogger("SyncManager");
        private string username;

        #endregion

        #region | Public Procedures |

        public async Task<bool> Start()
        {
            try
            {
                logger.Debug("Starting Sync for " + username);
                
                // Contacts Sync Task
                Task contactsSyncTask = Task.Run(() => { ContactManager.getInstance().SynchronizeContacts2(); });
                await Task.WhenAll( contactsSyncTask ); 	//We need Contacts BEFORE we get Message and Call Histories because we do some DB lookups.

                // Messages Sync Task
                Task messagesSyncTask = Task.Run(() => { SynchronizeMessages(); });

                // Call History Sync Task 
                Task callHistoryTask = Task.Run(() => { SynchronizeCallHistory(); });

                // Profile Sync task
                Task profileTask = Task.Run(() => { GetProfileData(); });

                // Company User Options task
                Task optionsTask = Task.Run(() => { GetCompanyUserOptions(); });

                // Wait till all Synchronization is done
                await Task.WhenAll(messagesSyncTask, callHistoryTask, profileTask, optionsTask);

				logger.Debug("Sync Complete");

                return true;
            }
            catch (Exception ex)
            {
                logger.Error("Error while synchronizing", ex);

                return false;
            }
        }

        #endregion 
        
        #region | Private Procedures |
        
        private void SynchronizeCallHistory()
        {
            logger.Debug("Synchronizing Call History from server");

			CallManager.getInstance().retrieveCallHistory();
			ContactManager.getInstance().checkCallsForInNetworkContacts();
        }

        //private bool getIsIncoming(string dialedNumber, string fromNumber)
        //{
        //    return dialedNumber.Equals(fromNumber) ? true : false;
        //}

        // When user login for the first time he will receive last 30 days messages
        // for subsequent login's it takes the latest message timestamp from local DB and sends a query to server and get's the records.
        // Need to test the functionality.
        private void SynchronizeMessages()
        {
            logger.Debug("Synchronizing Messages from server");

			MessageManager.getInstance().retrieveMessageHistory();
        }

        public void Stop()
        {
            logger.Debug("Stopping Sync");
        }

        public void GetProfileData()
        {
            try
            {
                CompanyUserExtendedResponse response = RestfulAPIManager.INSTANCE.getCompanyUserExtended(SessionManager.Instance.User.UserKey,
                        SessionManager.Instance.User.CompanyUserId);

				if ( response != null )
				{ 
					SessionManager.Instance.User.FirstName = response.firstName;
					SessionManager.Instance.User.LastName  = response.lastName;
					SessionManager.Instance.User.Email     = response.email;
					SessionManager.Instance.User.Did	   = response.company_did;
					SessionManager.Instance.User.Extension = response.dialedNumber;
					SessionManager.Instance.User.CompanyName = response.companyName;
				}
				else
				{
					logger.Warn( "getCompanyUserExtended() returned null." );
				}
            }
			
			catch(Exception ex)
            {
                logger.Error("GetProfiledata" + ex);
            }
        }

        public async void GetCompanyUserOptions()
        {
            try
            {
                CompanyUserOptionsResponse response = await RestfulAPIManager.INSTANCE.GetCompanyUserOptions(SessionManager.Instance.User.UserKey,
                                                                        SessionManager.Instance.User.CompanyUserId);

				SessionManager.Instance.User.Options.fromApiResponse( response );
            }

            catch (Exception ex)
            {
                logger.Error("GetCompanyUserOptions" + ex);
            }
        }

        #endregion
    }
}
