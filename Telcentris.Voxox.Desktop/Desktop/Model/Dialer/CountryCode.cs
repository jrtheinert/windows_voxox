﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Utils;					//Logger

using System;
using System.Collections.Generic;		//List
using System.Collections.ObjectModel;	//ObservableCollection				
using System.Linq;						//Filter

namespace Desktop.Model.Dialer
{
    public class CountryCode
    {
        public CountryCode()
        {
            Code		 = "";
            CountryName  = "";
            Abbreviation = "";
        }

        public string Code			{ get; set; }
        public string CountryName	{ get; set; }
        public string Abbreviation	{ get; set; }

        public string FlagFileName
        {
            get { return string.Format("/Branding/Flags/{0}.png", Abbreviation); }
        }

		//For totally unique key, we need Code and Abbreviation
		//	- Mostly because +1 is used for US and Canada, but there may be others.
        public string Key
        {
            get { return makeKey( Code, Abbreviation); }
        }

		public static string makeKey( string code, string abbrev )
		{
			return string.Format("{0}|{1}", code, abbrev);
		}

		public static string codeFromKey( string key )
		{
			string[] code = key.Split(new char[] { '|' });

			return code[0];
		}
	}

	public class CountryCodeList : List<CountryCode>
	{
        private VXLogger		  mLogger		= VXLoggingManager.INSTANCE.GetLogger("CountryCodeList");
 		private List<CountryCode> mCountryCodes	= null;

		private CountryCodeList()
		{
			LoadData();
		}

        public static CountryCodeList Instance
        {
            get { return Nested.instance; }
        }

        class Nested
        {
            static Nested()		{}

            internal static readonly CountryCodeList instance = new CountryCodeList();
        }

        private void LoadData()
        {
			if ( mCountryCodes == null )
			{ 
				mCountryCodes = new List<CountryCode>();

				try
				{
					var					   uri				  = new Uri(System.IO.Path.Combine("pack://application:,,,/", LocalizedString.CountryCodesFileName));
					var					   CountryCodesStream = System.Windows.Application.GetResourceStream(uri);
					System.Xml.XmlDocument doc				  = new System.Xml.XmlDocument();

					using (var stream = CountryCodesStream.Stream)
					{
						doc.Load(stream);

						using (System.Xml.XmlNodeList nodesList = doc.SelectNodes("CountryCodes/CountryCode"))
						{
							foreach (System.Xml.XmlNode node in nodesList)
							{
								mCountryCodes.Add(new CountryCode()
								{
									Code		 = node.ChildNodes[0].InnerText,
									CountryName  = node.ChildNodes[1].InnerText,
									Abbreviation = node.ChildNodes[2].InnerText
								});
							}
						}
					}
				}
				catch (Exception ex)
				{
					mLogger.Error("LoadData", ex);
				}
			}
        }

		public CountryCode getByKey( String tgtKey )
		{
			CountryCode result = null;

			foreach ( CountryCode cc in mCountryCodes )
			{
				if ( cc.Key == tgtKey )
				{
					result = cc;
					break;
				}
			}

			return result;
		}

		//We do not always have Code and Abbreviation, so we may also need to get by Code alone.
		public CountryCode getByCode( String tgtCode )
		{
			CountryCode result = null;

			foreach ( CountryCode cc in mCountryCodes )
			{
				if ( cc.Code == tgtCode )
				{
					result = cc;
					break;
				}
			}

			return result;
		}

		public ObservableCollection<CountryCode> FilterAsObserveableCollection( String filter )
		{
			ObservableCollection<CountryCode> result = new ObservableCollection<CountryCode>();

            var filteredList = (from c in mCountryCodes where (filter == "" || c.CountryName.ToLower().Contains(filter.ToLower()) || c.Code.Contains(filter) == true) select c).ToList();

            foreach (var countryCode in filteredList)
            {
                result.Add( countryCode );
            }

			return result;
		}
        
    }
}
