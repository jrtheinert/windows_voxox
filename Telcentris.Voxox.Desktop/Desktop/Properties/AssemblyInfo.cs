﻿using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Windows;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

// Version information for an assembly consists of the following four values:
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")] 
[assembly: AssemblyVersion    ("3.0.0.1799")]
[assembly: AssemblyFileVersion("3.0.0.1799")]


//Branding - We may want to have 'voxox' as the default rather than explicity use #elif BRAND_VOXOX
#if BRAND_VOICEPASS
[assembly: AssemblyTitle  ("Voice Pass")]
[assembly: AssemblyProduct("Voice Pass")]
#elif BRAND_VOXOX
[assembly: AssemblyTitle  ("Voxox")]
[assembly: AssemblyProduct("Voxox")]
#elif BRAND_CLOUDPHONE
[assembly: AssemblyTitle  ("Cloud Phone")]
[assembly: AssemblyProduct("Cloud Phone")]
#elif BRAND_VTC
[assembly: AssemblyTitle  ("VTC")]
[assembly: AssemblyProduct("VTC")]
#else
[assembly: AssemblyTitle  ("Desktop")]
[assembly: AssemblyProduct("Desktop")]
#endif

[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]

[assembly: AssemblyCopyright("Copyright © 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

//In order to begin building localizable applications, set 
//<UICulture>CultureYouAreCodingWith</UICulture> in your .csproj file
//inside a <PropertyGroup>.  For example, if you are using US english
//in your source files, set the <UICulture> to en-US.  Then uncomment
//the NeutralResourceLanguage attribute below.  Update the "en-US" in
//the line below to match the UICulture setting in the project file.

//[assembly: NeutralResourcesLanguage("en-US", UltimateResourceFallbackLocation.Satellite)]


[assembly: ThemeInfo( 
	//where theme specific resource dictionaries are located (used if a resource is not found in the page, or application resource dictionaries)
	ResourceDictionaryLocation.None,
	
	//where the generic resource dictionary is located (used if a resource is not found in the page, app, or any theme specific resource dictionaries)											
    ResourceDictionaryLocation.SourceAssembly			
)]
