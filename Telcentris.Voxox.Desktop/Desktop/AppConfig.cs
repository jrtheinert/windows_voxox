﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;					//SessionManager for re-brand due to Advanced Settings

using Telcentris.Voxox.ManagedXmppApi.ManagedXmppDataTypes;		//XMPP values

using System;							//For Exception
using System.Windows;					//For Point
using System.Reflection;				//For Assembly
using System.Windows.Media;				//For Color
using System.Windows.Media.Imaging;		//For BitmapImage

//using System.Globalization;			//For CultureInfo
//using System.Windows.Navigation;		//For BaseUriHelper

//=============================================================================
// Design considerations:
//	- We keep our configuration parameters in a separate namespace for easier identification.
//	- We have separate static classes for each functional area of configuration.
//		This makes it clearer in .cs and .xaml files EXACTLY what is intended and avoids name collisions
//	- These static classes *should* be automatically initialized BEFORE any of our code is executed.
//		This means that we can create a simple Branding class to override these defaults.
//
//	- Separate static classes fall into these major categories:
//		- UI/UX relate: These are managed mostly by the UX Team
//			- Colors
//			- Brushes
//			- UI (Images, Dimensions, etc.)
//
//		- Internal settings (set by Dev Team with input from Product Team)
//			-Branding values, mostly internal
//
//		- App Settings (sound files, default settings, etc.)
//			- Dialpad Sounds (these will likely never change)
//			- Settings - Default values for Settings portion of App
//
//		- External Settings related config classes:  These will MOST likely be branded.
//			- API (internal to Voxox and external, such as MixPanel)
//			- Extranet URLs
//			- Support contact info  (May belong in a different category)
//
//		- Model layer: These change rarely and typically for Dev/QA builds
//			- SIP settings
//			- XMPP settings
//
//	Each static class MUST have an Init() method.  In most cases, this Init()
//		method will be empty (do nothing).  Its purpose is to allow us to
//		force static initialization of each class, so that we control it,
//		rather that rely on .NET framework to initialize the static classes
//		when they are first referenced.
//		Any newly created classes should have the Init() method, and that
//		new classes Init() method should be called in Brand.initPrivate()
//		(near bottom of this code)
//
//	Exception handling is tricky because these classes are instantiated before
//		any application code is called.  Main reasons we get exceptions:
//		- Invalid URI when creating an Image or ImageBrush
//
//	Using in XAML files, please follow these conventions:
//		- Define XML namespace like this: xmlns:Cfg="clr-namespace:Desktop.Config"
//		- Reference desired element like this: Cfg:<type>.<member>
//			where <type> is one of our static classes:  UI, Brushes, Colors
//			and   <member> is the class member desired.
//
//	Using in .cs files: Simply refer to Desktop.Config.<type>.<member>
//		'using Desktop.Config' also works, but IMO, hides the source of the data.
//=============================================================================

//=============================================================================
//SPECIAL NOTES FOR COLORS AND BRUSHES as related to UX Designer:
//
//A key goal of proper branding is to ensure as much of what is branded is easily
//	viewed in Designer.  Per Microsoft docs, when using ThemeResourcs only the 'default'
//	theme is viewable under Designer, meaning that a non-default theme has to be
//	built in order to view it.  This is less than ideal for Branding which is often
//	about colors and brushes.
//
//	To address this, any colors being branded should be done in the Colors class below
//	using #if/#endif pairs, as opposed to doing this in the Brand.doBranding() method.
//	By doing this, the branded colors are available in Designer and UX team can
//	benefit by seeing their color changes without having to rebuild.
//=============================================================================

//=============================================================================
//This class handles all settings, branding and special build requirements.
//
//Theory of operation is:
//	- Standard/default values are set in main Desktop.Config static classes.
//		-- By design/convention, defaults are for a Voxox build.
//	- Branding is handled via #if values "BRAND_VOXOX", "BRAND_VOICEPASS
//		-- If no "BRAND_xxx" is defined, then default Voxox values are used.
//		-- Branding values are set in Brand::doBranding()
//	- Special builds, like QA/DEV builds pointing to non-production environments
//		-- Changes are done in Brand::doBranding() (same as Branding)
//		-- You can define whatever #if's are needed but likely at least QA and DEV will be needed
//		-- Generally speaking, API server and URLs will be overridden here for special builds.
//	- Helper methods (AppConfigUtil) to make it easier to use in XAML
//		-- XAML Image Source cannot use simple Paths, so we use makeComponentUri( path ) to get proper formatting
//		-- Similar issue for makeApplicationUri (used in SystemTray.cs only)
//		-- We can also provide actual BitmapImage to XAML
//
// NOTES about ImageSource vs. Uri:
//	- XAML <Image> elements (Source) expect a URI.  In XAML, we can hard-code a relative path ("Branding/stars.png"), but this
//		is not helpful when the Image needs to be branded.
//	- As with other brandable elements, we want brandable Images to be visible under Designer.  Sadly, I am not able to achieve this.
//		-- It *seems* like we should just be able to provide a branded URI string (much like what is hard-code), but that does not work.
//		-- We can alter the XAML Image to use Binding and our UriToImageSourceConverter.  That works OK, but has NO chance of working in Designer
//			since the converter is used at RunTime.  Plus, it requires use of the converter, binding, etc.
//		-- So I have settled on branding Images have a ImageSource (BitmapImage) type.  This allows:
//			-- Handle all pathing, creation in AppConfig (this class)
//			-- In XAML files, to use simple Source="{x:Static Cfg::UI.<member>}"
//		-- I am HOPEFUL, that at some point this static URI will be visible in designer, but as of now, I cannot make that happen.
//			So let's keep the rest of the code (XAML) as simple as possible.
//
// Adding new items:
//	- Add to appropriate class, or create new class.
//
//	- TODO-BRAND: I have not yet tested this, but this may be a (near) complete replacement for SettingsManager.cs
//
//	- Adding a new brush is a bit more work, since it may be based on Branded Colors and/or Images.
//		-- Do normal branding
//		-- In Brushes::init() method add the new brush so that the static accessor is reset, as needed.
//
//G-Doc for Branding and this class: https://docs.google.com/a/telcentris.com/document/d/13mLHlXMLrrZ4UwjoeY7UxGkMPp33_8GBLs6DUalLOlk/edit?usp=sharing
//
//==============================================================================

namespace Desktop.Config
{
	#region UI class

	//=========================================================================
	//UI/UX related config classes:
	//	- Colors
	//	- Brushes
	//	- UI ( Images, Dimensions, etc.)
	//=========================================================================
	public static class UI
	{
		public static void Init()
		{
			if ( Brand.brandIsVoicepass() )
			{ 
				LoginBackground		= AppConfigUtil.makeApplicationUri(@"/Branding/nothing.png");
				Background			= AppConfigUtil.makeApplicationUri(@"/Branding/nothing.png");
				NavBackground		= AppConfigUtil.makeApplicationUri(@"/Branding/nothing.png");
				NothingHereImage	= AppConfigUtil.makeImage( @"/Branding/_BrandVoicepass/hp-nothing-here.png" );
			}
			else if ( Brand.brandIsVoxox() )
			{ 
				LoginBackground		= AppConfigUtil.makeApplicationUri(@"/Branding/stars.png");
				Background			= AppConfigUtil.makeApplicationUri(@"/Branding/Navigation/left-panel-stars.png");
				NavBackground		= AppConfigUtil.makeApplicationUri(@"/Branding/Navigation/left-panel-stars.png");
				NothingHereImage	= AppConfigUtil.makeImage(@"/Branding/_BrandVoxox/no-entry.png");
			}
			else if ( Brand.brandIsCloudPhone() )
			{ 
				//TODO: Add CloudPhone UI elements
				LoginBackground		= AppConfigUtil.makeApplicationUri(@"/Branding/stars.png");
				Background			= AppConfigUtil.makeApplicationUri(@"/Branding/Navigation/left-panel-stars.png");
				NavBackground		= AppConfigUtil.makeApplicationUri(@"/Branding/Navigation/left-panel-stars.png");
				NothingHereImage	= AppConfigUtil.makeImage(@"/Branding/_BrandVoxox/no-entry.png");
			}
			else
			{
				//In this case, we will get original values defined below.
			}
		}

		// General UI settings                                                    
		public static string FormWidth							= "960";			//We define this as string so no conversion is needed in .xaml
		public static string FormHeight							= "648";

		// Settings default chat colors                                                                 
		public static string DefaultUserChatBubbleColor			= "DefaultUserChatBubbleColor";
		public static string DefaultConversantChatBubbleColor	= "DefaultConversantChatBubbleColor";

		// Labels                                                                 
		public static string Contact_Label						= "Voice Pass";		//TODO: Same as AppName?

		// Images                                                                 
        public static string CallingNoAvatar                    = @"/Branding/Avatars/avatar-person.png";
        public static string ContactNoAvatar                    = @"/Branding/Avatars/avatar-person.png";
        public static string MessagingNoAvatar                  = @"/Branding/Avatars/avatar-message.png";
        public static string RichMediaLoading                   = @"/Branding/Avatars/avatar-message.png";

		public static string InNetworkContact					= AppConfigUtil.makeComponentUri  ( @"/Branding/_BrandVoxox/contact.png" );
		public static string SystrayIconUri						= AppConfigUtil.makeApplicationUri( @"/Branding/_BrandVoxox/app.ico" );

		public static BitmapImage AboutLogoImage				= AppConfigUtil.makeImage( @"/Branding/_BrandVoxox/logo-xxhdpi.png" );		//TODO-BRAND: get actual asset
		
		public static BitmapImage LoginLogoImage				= AppConfigUtil.makeImage( @"/Branding/_BrandVoxox/login-logo2.png" );

		//NavBar images - Normal
        public static BitmapImage NavigationContacts			= AppConfigUtil.makeImage(@"/Branding/Navigation/navigation-contacts.png");
        public static BitmapImage NavigationFax					= AppConfigUtil.makeImage(@"/Branding/Navigation/navigation-fax.png");
        public static BitmapImage NavigationKeypad				= AppConfigUtil.makeImage(@"/Branding/Navigation/navigation-keypad.png");
        public static BitmapImage NavigationMessages			= AppConfigUtil.makeImage(@"/Branding/Navigation/navigation-messages.png");
        public static BitmapImage NavigationRecentCalls			= AppConfigUtil.makeImage(@"/Branding/Navigation/navigation-recent-calls.png");
        public static BitmapImage NavigationSettings			= AppConfigUtil.makeImage(@"/Branding/Navigation/navigation-settings.png");
        public static BitmapImage NavigationShopping			= AppConfigUtil.makeImage(@"/Branding/Navigation/navigation-shopping.png");

		//NavBar images - Hover/Active
        public static BitmapImage NavigationContactsHover		= AppConfigUtil.makeImage(@"/Branding/Navigation/navigation-contacts-active.png");
        public static BitmapImage NavigationFaxHover			= AppConfigUtil.makeImage(@"/Branding/Navigation/navigation-fax-active.png");
        public static BitmapImage NavigationKeypadHover			= AppConfigUtil.makeImage(@"/Branding/Navigation/navigation-keypad-active.png");
        public static BitmapImage NavigationMessagesHover		= AppConfigUtil.makeImage(@"/Branding/Navigation/navigation-messages-active.png");
        public static BitmapImage NavigationRecentCallsHover	= AppConfigUtil.makeImage(@"/Branding/Navigation/navigation-recent-calls-active.png");
        public static BitmapImage NavigationSettingsHover		= AppConfigUtil.makeImage(@"/Branding/Navigation/navigation-settings-active.png");
        public static BitmapImage NavigationShoppingHover		= AppConfigUtil.makeImage(@"/Branding/Navigation/navigation-shopping-active.png");

		//Images used with ImageBrushes
		public static string NoiseBackground					= AppConfigUtil.makeApplicationUri( @"/Branding/noise-background.png" );
      
//#if BRAND_VOICEPASS
        public static string LoginBackground                = AppConfigUtil.makeApplicationUri(@"/Branding/nothing.png");
        public static string Background                     = AppConfigUtil.makeApplicationUri(@"/Branding/nothing.png");
        public static string NavBackground                  = AppConfigUtil.makeApplicationUri(@"/Branding/nothing.png");
		public static BitmapImage NothingHereImage			= AppConfigUtil.makeImage( @"/Branding/_BrandVoicepass/hp-nothing-here.png" );
//#else
//		public static string LoginBackground                = AppConfigUtil.makeApplicationUri(@"/Branding/stars.png");
//		public static string Background                     = AppConfigUtil.makeApplicationUri(@"/Branding/Navigation/left-panel-stars.png");
//		public static string NavBackground                  = AppConfigUtil.makeApplicationUri(@"/Branding/Navigation/left-panel-stars.png");
//		public static BitmapImage NothingHereImage          = AppConfigUtil.makeImage(@"/Branding/_BrandVoxox/no-entry.png");
//#endif

            // Misc                                                                    
		public static string SupportPhone_CountryCode			= "+1";				//TODO: Move to Support stanza?
	}

	#endregion

	#region Brushes class

	//For clarity, we use private methods to initialize each memvar.
	//NOTE: Since we are using Colors class values here, that MUST be initialized before the Brushes.
	//	We *may* need to call an 'init()' method for this.
	public static class Brushes
	{
		//NOTE: By defining these values here, even though they will be overridden later, we get real colors in the .xaml Designer.

		public static LinearGradientBrush	Brush_AppBackground			= AppConfigUtil.makeGradientBrush( Colors.Color_App_Bg_Start,   Colors.Color_App_Bg_End   );
		public static LinearGradientBrush	Brush_AppBackgroundInverse	= AppConfigUtil.makeGradientBrush( Colors.Color_App_Bg_End,     Colors.Color_App_Bg_Start );
		public static LinearGradientBrush	Brush_LoginBackground		= AppConfigUtil.makeGradientBrush( Colors.Color_Login_Bg_Start, Colors.Color_Login_Bg_End );

		public static ImageBrush			Brush_AppBackgroundImage	= AppConfigUtil.makeImageBrush( Config.UI.Background,      true );
		public static ImageBrush			Brush_NoiseImage			= AppConfigUtil.makeImageBrush( Config.UI.NoiseBackground, true );
        public static ImageBrush            Brush_LoginImage			= AppConfigUtil.makeImageBrush( Config.UI.LoginBackground, true );
		public static ImageBrush			Brush_NavBackgroundImage	= AppConfigUtil.makeImageBrush( Config.UI.NavBackground,   true );

		//For simplicity, let's just Brand Brushes that have branded colors.  
		//	Those that do NOT have branded colors , *MAY* be defined here and undefined in BrushResources.xaml.
		//	However, that will require changing the affected .xaml, in this case needlessly.
		//	I have commented to Brushes that use non-branded colors so it will be easier to add them later if needed.
		//
//		public static SolidColorBrush		Brush_Border				= AppConfigUtil.makeSolidColorBrush( Config.Colors.Color_Border			  );	//56
//		public static SolidColorBrush		Brush_DockPanelFront		= AppConfigUtil.makeSolidColorBrush( Config.Colors.Color_Black			  );	// 6
		public static SolidColorBrush		Brush_SectionHeader			= AppConfigUtil.makeSolidColorBrush( Config.Colors.Color_Section_Header   );	// 6

		public static SolidColorBrush		Brush_BgEnd					= AppConfigUtil.makeSolidColorBrush( Config.Colors.Color_App_Bg_End		  );	// 1
//		public static SolidColorBrush		Brush_GreenButton			= AppConfigUtil.makeSolidColorBrush( Config.Colors.Color_Green_Button	  );	// 1
//		public static SolidColorBrush		Brush_DockPanel				= AppConfigUtil.makeSolidColorBrush( Config.Colors.Color_Dock_Panel		  );	// 2
//		public static SolidColorBrush		Brush_NumberDivider			= AppConfigUtil.makeSolidColorBrush( Config.Colors.Color_Number_Divider	  );	// 2
		public static SolidColorBrush		Brush_MediaDrawer			= AppConfigUtil.makeSolidColorBrush( Config.Colors.Color_Media_Drawer	  );	// 1
//		public static SolidColorBrush		Brush_NavIcon				= AppConfigUtil.makeSolidColorBrush( Config.Colors.Color_Nav_Icon		  );	// 2
//		public static SolidColorBrush		Brush_ModalBackground		= AppConfigUtil.makeSolidColorBrush( Config.Colors.Color_Modal_Background );	// 2

		//Dialpad
		public static SolidColorBrush		Brush_DialPadBackground		= AppConfigUtil.makeSolidColorBrush( Config.Colors.Color_DialPad_Background	  );	// 1
//		public static SolidColorBrush		Brush_DialpadSubtext		= AppConfigUtil.makeSolidColorBrush( Config.Colors.Color_DialPad_SubText	  );	// 1
//		public static SolidColorBrush		Brush_DialpadDropdown		= AppConfigUtil.makeSolidColorBrush( Config.Colors.Color_DialPad_Dropdown	  );	// 1

		public static SolidColorBrush		Brush_NavActiveBackground	= AppConfigUtil.makeSolidColorBrush( Config.Colors.Color_Nav_ActiveBg		);

		//public Brushes()
		//{
		//}

		public static void Init()
		{
			//NOTE: So far, we do NOT need to check the BrandId here because all the brushes we create are based on
			//	values that have already been branded.

			//LinearGradient - Creates a simplistic, standard gradient.  Feel free to add your own, more advanced makeGradientBrushXXX methods
			Brush_AppBackground			= AppConfigUtil.makeGradientBrush( Colors.Color_App_Bg_Start,   Colors.Color_App_Bg_End   );
			Brush_AppBackgroundInverse	= AppConfigUtil.makeGradientBrush( Colors.Color_App_Bg_End,     Colors.Color_App_Bg_Start );
			Brush_LoginBackground		= AppConfigUtil.makeGradientBrush( Colors.Color_Login_Bg_Start, Colors.Color_Login_Bg_End );

			//ImageBrush
            Brush_AppBackgroundImage    = AppConfigUtil.makeImageBrush( Config.UI.Background,      true );
			Brush_NoiseImage			= AppConfigUtil.makeImageBrush( Config.UI.NoiseBackground, true );
            Brush_LoginImage            = AppConfigUtil.makeImageBrush( Config.UI.LoginBackground, true );
            Brush_NavBackgroundImage    = AppConfigUtil.makeImageBrush( Config.UI.NavBackground,   true );

			//SolidColorBrush
			Brush_SectionHeader			= AppConfigUtil.makeSolidColorBrush( Config.Colors.Color_Section_Header		);
			Brush_BgEnd					= AppConfigUtil.makeSolidColorBrush( Config.Colors.Color_App_Bg_End			);
			Brush_MediaDrawer			= AppConfigUtil.makeSolidColorBrush( Config.Colors.Color_Media_Drawer		);
			Brush_DialPadBackground		= AppConfigUtil.makeSolidColorBrush( Config.Colors.Color_DialPad_Background	);
			Brush_NavActiveBackground	= AppConfigUtil.makeSolidColorBrush( Config.Colors.Color_Nav_ActiveBg		);
		}
		
	}	//class Brushes

	#endregion

	#region Colors class

	public static class Colors
	{ 
		public static void Init()
		{
			if ( Brand.brandIsVoicepass() )
			{ 
				Color_Login_Bg_Start                    = colorFromString( "#0096D6" );		// 0	1	  8
				Color_Login_Bg_End                      = colorFromString( "#0096D6" );		// 

				Color_App_Bg_Start                      = colorFromString( "#444444" );		// 1	2
				Color_App_Bg_End                        = colorFromString( "#666666" );		// 1	3	  0
        
				Color_Nav_Border                        = colorFromString( "#777777" );

				Color_Section_Header                    = colorFromString( "#0096D6" );
				Color_Login_Forgot                      = colorFromString( "#FFFFFF" );		// 0	1	  0

				Color_DialPad_Background				= colorFromString( "#222222" );		// 0	1	  2

				Color_Nav_ActiveBg                      = colorFromString( "#1F000000" );	// 6	0

				Color_Media_Drawer						= colorFromString( "#222222" );		// 6	0
			}
			else if ( Brand.brandIsVoxox() )
			{ 
				Color_Login_Bg_Start					= colorFromString( "#333C46" );		 
				Color_Login_Bg_End						= colorFromString( "#486168" );

				Color_App_Bg_Start                      = colorFromString( "#333C46" );		// 1	2
				Color_App_Bg_End                        = colorFromString( "#486168" );		// 1	3	  0

				Color_Nav_Border                        = colorFromString( "#A4B3B8" );

				Color_Section_Header                    = colorFromString( "#4A9788" );
				Color_Login_Forgot						= colorFromString( "#99A7B0" );		// 0	1	  0

				Color_DialPad_Background                = colorFromString( "#333C46" );		// 0	1	  2

				Color_Nav_ActiveBg						= colorFromString( "#8C242F33" );	// 6	0

				Color_Media_Drawer                      = colorFromString( "#394652" );		// 6	0
			}
			else if ( Brand.brandIsCloudPhone() )
			{ 
				//TODO: Define these values for CloudPhone
				Color_Login_Bg_Start					= colorFromString( "#333C46" );		 
				Color_Login_Bg_End						= colorFromString( "#486168" );

				Color_App_Bg_Start                      = colorFromString( "#333C46" );		// 1	2
				Color_App_Bg_End                        = colorFromString( "#486168" );		// 1	3	  0

				Color_Nav_Border                        = colorFromString( "#A4B3B8" );

				Color_Section_Header                    = colorFromString( "#4A9788" );
				Color_Login_Forgot						= colorFromString( "#99A7B0" );		// 0	1	  0

				Color_DialPad_Background                = colorFromString( "#333C46" );		// 0	1	  2

				Color_Nav_ActiveBg						= colorFromString( "#8C242F33" );	// 6	0

				Color_Media_Drawer                      = colorFromString( "#394652" );		// 6	0
			}
			else
			{
				//In this case, we just get values defined below
			}
		}

		public static Color colorFromString( string value)
		{
			return (Color)ColorConverter.ConvertFromString( value);
		}

		// Colors defined in the UX document
		//Columns at end of each line:
		//	#1 - Number of times Color is used in the code
		//	#2 - Number of Brushes defined with the Color
		//	#3 - Number of times the defined Brush is used in the code
		//	So, Colors with 0 1 0 are NOT used at all and can be removed, if not included for future changes.
		public static Color Color_White				= colorFromString( "#FFFFFF" );	// 7	1	126
		public static Color Color_DuskyGray			= colorFromString( "#999999" );	// 1	1	100
		public static Color Color_MediumGray		= colorFromString( "#666666" );	// 8	1	 69
        public static Color Color_Black             = colorFromString( "#333333" );	// 8	1	 69
		public static Color Color_Silver			= colorFromString( "#CCCCCC" );	// 2	1	 49
        public static Color Color_GullGray          = colorFromString( "#9BA7B3" );	// 2	1	 13

		public static Color Color_LightBlue			= colorFromString( "#279BCE" );	// 4	1	  9
		public static Color Color_Bittersweet		= colorFromString( "#FC6769" );	// 0	1	  7
		public static Color Color_Red				= colorFromString( "#FF6666" );	// 1	1	  6

		public static Color Color_DeYork			= colorFromString( "#6EC993" );	// 0	1	  4
		public static Color Color_VistaGreen		= colorFromString( "#72D198" );	// 0	1	  3
		public static Color Color_AlgeaGreen		= colorFromString( "#78DA9F" );	// 0	1	  3
		public static Color Color_LightAqua			= colorFromString( "#6D9691" );	// 0	1	  3
		public static Color Color_Pink				= colorFromString( "#FFCCFF" );	// 0	1	  3

		public static Color Color_SilverChalice		= colorFromString( "#AAAAAA" );	// 0	1	  1
		public static Color Color_FocusGray			= colorFromString( "#8C8C8C" );	// 0	1	  1
		public static Color Color_BrightJuniper		= colorFromString( "#6E9691" );	// 0	1	  2
		public static Color Color_Geraldine			= colorFromString( "#FD7E7E" );	// 0	1	  1
		public static Color Color_Wedgewood			= colorFromString( "#497B99" );	// 0	1	  2
		public static Color Color_HippieBlue		= colorFromString( "#548DAF" );	// 0	1	  2
		public static Color Color_FocusBlue			= colorFromString( "#467693" );	// 0	1	  2
		public static Color Color_LightGrey			= colorFromString( "#E8E8E8" );	// 5	1	  2

		//Appear to not be used
		public static Color Color_Carnation			= colorFromString( "#F46969" );	// 0	1	  0
		public static Color Color_DarkBlue			= colorFromString( "#497B99" );	// 0	1	  0
        public static Color Color_Feijoa            = colorFromString( "#53C681" );	// 0	1	  0
		public static Color Color_HavelockBlue		= colorFromString( "#57ACE0" );	// 0	1	  0
        public static Color Color_Juniper           = colorFromString( "#394652" );	// 0	1	  0
		public static Color Color_OlsoGray			= colorFromString( "#7F8E95" );	// 0	1	  0
		public static Color Color_Orange			= colorFromString( "#F39756" );	// 0	1	  0

		//Functional definitions
        public static Color Color_Green_Button      = colorFromString("#53C681"	 );	// 0	1	  3
        public static Color Color_Number_Divider    = colorFromString("#1A666666");
        public static Color Color_Nav_Icon			= colorFromString("#B5C8D8"	 );	

		// Additional Colors
        public static Color Color_App_Background					= colorFromString( "#EFEFEF" );	

		public static Color Color_No_Color							= colorFromString( "#00000000" );	// 0	0
		public static Color Color_Action_Bar_Background				= colorFromString( "#497B99" );		// 0	1	  0
		public static Color Color_Middle_Panel_Border_Right			= colorFromString( "#CCCCCC" );		// 0	1	  0
		public static Color Color_Dock_Panel						= colorFromString( "#F9F9F9" );		// 0	1	  2
		public static Color Color_Messages_Top_Bar_Bottom_Border	= colorFromString( "#D8D8D8" );		// 0	1	  1

		public static Color Color_MiddlePane_Bg						= colorFromString( "#F9F9F9" );		//12	0
		
		public static Color Color_DialPad_Dropdown					= colorFromString( "#212A32" );		// 1	1	  1
		public static Color Color_DialPad_SubText					= colorFromString( "#ACABAB" );		// 0	1	  1
		public static Color Color_DialPad_MouseOver					= colorFromString( "#01ACED" );		//15	0

		public static Color Color_Login_Textbox_Background			= colorFromString( "#CFD5D8" );		// 0	1	  0
		public static Color Color_Login_Textbox_Border				= colorFromString( "#FDFDFD" );		// 0	1	  2
		
		public static Color Color_Login_Error_Background			= colorFromString( "#282F35" );		// 0	1	  1
		public static Color Color_Login_Error_Text					= colorFromString( "#ED4460" );		// 3	0

        public static Color Color_Border                            = colorFromString( "#E6E6E6" );		// 0	1	  1

        public static Color Color_Modal_Background                  = colorFromString( "#80282F34" );	//50% opacity


//#if BRAND_VOICEPASS
        public static Color Color_Login_Bg_Start                    = colorFromString( "#0096D6" );		// 0	1	  8
        public static Color Color_Login_Bg_End                      = colorFromString( "#0096D6" );		// 

        public static Color Color_App_Bg_Start                      = colorFromString( "#444444" );		// 1	2
        public static Color Color_App_Bg_End                        = colorFromString( "#666666" );		// 1	3	  0
        
        public static Color Color_Nav_Border                        = colorFromString( "#777777" );

        public static Color Color_Section_Header                    = colorFromString( "#0096D6" );
        public static Color Color_Login_Forgot                      = colorFromString( "#FFFFFF" );		// 0	1	  0

        public static Color Color_DialPad_Background				= colorFromString( "#222222" );		// 0	1	  2

        public static Color Color_Nav_ActiveBg                      = colorFromString( "#1F000000" );	// 6	0

        public static Color Color_Media_Drawer						= colorFromString( "#222222" );		// 6	0
//#else
//		public static Color Color_Login_Bg_Start					= colorFromString( "#333C46" );		 
//		public static Color Color_Login_Bg_End						= colorFromString( "#486168" );

//		public static Color Color_App_Bg_Start                      = colorFromString( "#333C46" );		// 1	2
//		public static Color Color_App_Bg_End                        = colorFromString( "#486168" );		// 1	3	  0

//		public static Color Color_Nav_Border                        = colorFromString( "#A4B3B8" );

//		public static Color Color_Section_Header                    = colorFromString( "#4A9788" );
//		public static Color Color_Login_Forgot						= colorFromString( "#99A7B0" );		// 0	1	  0

//		public static Color Color_DialPad_Background                = colorFromString( "#333C46" );		// 0	1	  2

//		public static Color Color_Nav_ActiveBg						= colorFromString( "#8C242F33" );	// 6	0

//		public static Color Color_Media_Drawer                      = colorFromString( "#394652" );		// 6	0
//#endif

        public static Color Color_CallOption_Normal					= colorFromString( "#253A42" );		// 0	1	  2
		public static Color Color_CallOption_Hover					= colorFromString( "#2E4854" );		// 0	1	  1
		public static Color Color_CallOption_Focus					= colorFromString( "#243942" );		// 0	1	  0

		public static Color Color_Gray_DE							= colorFromString( "#DEDEDE" );		// 0	1	 11
		public static Color Color_Gray_EF							= colorFromString( "#EFEFEF" );		// 3	1	 17
		public static Color Color_Gray_F5							= colorFromString( "#F5F5F5" );		// 4	1	  2
		public static Color Color_Gray_F7							= colorFromString( "#F7F7F7" );		// 1	1	  0
		public static Color Color_Gray_F9							= colorFromString( "#F9F9F9" );		//21	1	  4
		public static Color Color_Gray_FA							= colorFromString( "#FAFAFA" );		// 0	1	  6

		public static Color Color_Slider_Thumb_Light				= colorFromString( "#808080" );		// 0	1	  4
		public static Color Color_Slider_Thumb_Dark					= colorFromString( "#404040" );		// 0	1	  1
		public static Color Color_Slider_Thumb_GradientEnd			= colorFromString( "#BCBCBC" );		// 1	0

		public static Color Color_ToggleButton_SelectedBorder		= colorFromString( "#E6E6E6" );		// 1	0

		public static Color Color_Overlay							= colorFromString( "#44000000" );	// 6	0

		public static Color Color_CallPane_TopPopup					= colorFromString( "#EE253A42" );	// 2	0

		public static Color Color_ListItem_Selected					= colorFromString( "#EDEDED" );		//10	0
		public static Color Color_ListItem_Hover					= colorFromString( "#F2F2F2" );

		public static Color Color_ComboxItem_DisabledForeground		= colorFromString( "#FF888888" );	// 1	0
		public static Color Color_ComboxItem_SelectedBackground		= colorFromString( "#FFC5CBF9" );	// 1	0
		public static Color Color_ComboxItem_SelectedUnfocused		= colorFromString( "#FFDDDDDD" );	// 1	0

		//HP Colors - TODO: move to branding area.
		public static Color Color_Hp_VoicePass_Background			= colorFromString( "#0096D6" );		// 0	1	  8
		public static Color Color_SaturatedBlack					= colorFromString( "#232323" );		// 0	1	  0
		public static Color Color_Very_LightGray					= colorFromString( "#EDEDED" );		// 2	1	  0
		public static Color Color_Dark_Gray							= colorFromString( "#666666" );		// 0	1	  9

		//Notes on LinearGradientBrush from BrushResource.xaml
		//	- App_Background		 - 4
		//	- App_Background_Inverse - 0

		//Notes on ImageBrush from BrushResource.xaml
		//	- App_Background_Stars	 - 3

		//Overall, we use Brushes defined in BrushResources.xaml in about 491 locations.  
		//	So it is no small task to move ALL Brush definitions to this AppConfig class.
		//	However, it should be relatively simple to move SOME key brushes here, like the
		//	LinearGradientBrush and the ImageBrush brushes.

	}	//Colors

	#endregion
			

	//=========================================================================
	// Internal settings (set by Dev Team with input from Product Team)
	//	-Branding values, mostly internal
	//=========================================================================
	#region Branding class

    // Enum to control username field in login screen
    public enum LoginMode
    {
        Default = 0,
        Email   = 1,
        Mobile  = 2
    }

	public enum BrandId
	{
		None		= 0,
		Voxox		= 1,
		CloudPhone	= 2,
		VoicePass	= 3,
		Vtc			= 4
	};


	//MUST-HAVE branding values
	//TODO: Throw exception if any of these are NOT valid.              
	public static class Branding
	{ 
		public static void Init()
		{
		}

		// App-level settings                                                             
		public static string AppMutex			= "{7DAFAEE5-20F3-4DA9-B9CC-3155655B9542}";

		// App name, Company Name, App icon and folder settings       
        public static string AppName			= "Voice Pass";
        public static string CompanyName		= "HP";
		public static string AppDataFolderName	= "VoicePass";
		public static string ApplicationIcon	= @"/Branding/_BrandVoicepass/hp-contact.ico";
		public static string ApplicationIcon2	= @"/Branding/_BrandVoicepass/HPIcon.ico";	//Temp for HP Beta on Login window
		public static String PartnerIdentifier  = "";
		public static String ClientId			="";

		public static string BuildSuffix		= "";				//Suffix to be applied to special builds, mostly used for logging

        public static LoginMode LoginMode       = LoginMode.Email;
	}

	#endregion


	//=========================================================================
	// External Settings related config classes:
	//	- API (internal to Voxox and external, such as MixPanel)
	//	- Extranet URLs
	//	- Support contact info  (May belong in a different category)
	//=========================================================================
	#region API  class

	//API servers, API modules, MixPanel keys, etc.

	//TODO: Remove comment below once we verify remove API key works in expected environments.
	//NOTE: Every branded build will have unique API key and unique API key for QA.
	//	This means we MUST have DEFINED overrides for every branded build.
	public static class API
	{
//		public const String StdApiKeyForQa			= "757ccb4e-7f08-11e3-a6a4-74867aa814e2";
		public const String StdMixPanelTokenForQa	= "277c7b3f67c5c211fc57cfdf9b45193d";

		public static void Init()
		{
		}
		

		//public static string ApiKey
		//{ 
		//	get { return ( UseProductionApiKey ? ApiKeyPrivate : ApiKeyForQaPrivate ); }

		//	private set {}
		//}

		// API servers and keys                                                          
//		public static string ApiKeyPrivate					= "52a36e27-b3b5-11e4-b90b-74867aa81509";	//These are VoicePass keys and just used for placeholders
//		public static string ApiKeyForQaPrivate				= StdApiKeyForQa;
		public static string ApiServer						= "vx-api.voxox.com";
//		public static bool	 UseProductionApiKey			= true;						//Used to override API key in lab/QA environments

		// API module bases (URIs)                                                 
		public static string ApiModuleBase					= "vxclient/rest";			//This is main VX API module

		public static string ApiAuthModuleBase				= "vxauth/rest";
//		public static string CallBackModuleBase				= "callback/rest/?";		//Not used anywhere
		public static string HttpFaxModuleBase				= "vxclient/file/sendfax?";
		public static string HttpFileDownloadModuleBase		= "vxclient/file/download?";
		public static string HttpFileUploadModuleBase		= "vxclient/file/upload?";

		// API misc values                                                         
		public static int	DefaultNumberOfMessagesFromServer	= 1000;
		public static int	DefaultNumberOfCallsFromServer		= 1000;
		public static bool	ServerUsesUtc						= false;	//TODO: Change this to 'true' when server supports UTC

		// Logging                                                                
		public static string IgnoreLoggers					= "";			//This most likely will be set after RC and maybe via 'debug menu'

		// MixPanel settings                                                       
		public static string MixPanelAPIUrl					= "http://api.mixpanel.com/track/?data=";	//TODO: This may move to Branding
		public static string MixPanelToken					= "cab2034892750a484aca559ab96178e7";

		public static string RemotePingUrl					= "http://www.google.com";
		public static int	 RemotePingTimeoutSecs			=  5;
		public static int	 RemotePingTimerSecs			= 10;
	}

	#endregion

	#region Support class
			
	public static class Support
	{ 
		public static void Init()
		{
		}

		public static string HelpContactSupportLink = "http://www.voxox.com/contact";
		public static string Settings_PhoneNo		= "(844) 447-8391";
		public static string Settings_Email			= "hpbeta@voxox.com";
		}

	#endregion

	#region URLs class

	//These will likely be overridden during branding/QA/Dev builds
	public static class Urls
	{
		public static void Init()
		{
			createExtranetUrls( "vp-extranet.voxox.com" );

			//If Extranet URLs do NOT follow expected naming scheme, feel free to override them individually.
		}

		//This is NOT Extranet URL.
        public static string MapProviderTemplateURL = "https://www.google.com/maps?q={0}"; // This URL should contain a "{0}" parameter


		public static string ExtranetBase_URL;

		public static string BuyCredits_URL;
		public static string CallerIdNavigationLink;
		public static string ForgotPasswordURL;
		public static string ReachmeNavigationLink;
		public static string SignupURL;
		public static string ManageAccountURL;
		public static string TermsandConditionsURL;

		static public void createExtranetUrls( String extranetBase )
		{
			Config.Urls.ExtranetBase_URL		= AppConfigUtil.makeExtranetUrl( extranetBase, true,  ""						);

			Config.Urls.BuyCredits_URL			= AppConfigUtil.makeExtranetUrl( extranetBase, true,  "vxlogin"					);
			Config.Urls.CallerIdNavigationLink	= AppConfigUtil.makeExtranetUrl( extranetBase, true,  "vxportal/people"			);
			Config.Urls.ForgotPasswordURL		= AppConfigUtil.makeExtranetUrl( extranetBase, true,  "vxlogin/password/forgot" );
			Config.Urls.ReachmeNavigationLink	= AppConfigUtil.makeExtranetUrl( extranetBase, true,  "vxportal/people"			);
			Config.Urls.SignupURL				= AppConfigUtil.makeExtranetUrl( extranetBase, true,  "signupvx"				);

			Config.Urls.ManageAccountURL		= AppConfigUtil.makeExtranetUrl( extranetBase, false, "vxportal/settings"		);
			Config.Urls.TermsandConditionsURL	= AppConfigUtil.makeExtranetUrl( extranetBase, false, "signupvx/index/terms"	);
		}
	}

	#endregion


	//=========================================================================
	// App Settings related config classes:
	//	- Dialpad Sounds (these will likely never change)
	//	- Settings - Default values for Settings portion of App
	//=========================================================================
	#region DialpadSounds class

	public static class DialpadSounds
	{
		public static void Init()
		{
		}

		public static string Key0SoundFile		= @"Branding\Dialpad\sound_0.wav";
		public static string Key1SoundFile		= @"Branding\Dialpad\sound_1.wav";
		public static string Key2SoundFile		= @"Branding\Dialpad\sound_2.wav";
		public static string Key3SoundFile		= @"Branding\Dialpad\sound_3.wav";
		public static string Key4SoundFile		= @"Branding\Dialpad\sound_4.wav";
		public static string Key5SoundFile		= @"Branding\Dialpad\sound_5.wav";
		public static string Key6SoundFile		= @"Branding\Dialpad\sound_6.wav";
		public static string Key7SoundFile		= @"Branding\Dialpad\sound_7.wav";
		public static string Key8SoundFile		= @"Branding\Dialpad\sound_8.wav";
		public static string Key9SoundFile		= @"Branding\Dialpad\sound_9.wav";
		public static string KeyPoundSoundFile	= @"Branding\Dialpad\sound_pound.wav";
		public static string KeyStarSoundFile	= @"Branding\Dialpad\sound_star.wav";
	}

	#endregion

	#region Settings class

	//These will rarely change, except maybe the default Country info.
	public static class Settings
	{ 
		public static void Init()
		{
		}

		// User selectable settings (default values)                              
		public static int	 AudioInputVolume				= 200;				//TODO: Shouldn't these be 100?  What is max?
		public static int	 AudioOutputVolume				= 200;
		public static bool   ShowFaxesInChatThread			= true;
		public static bool   ShowVoicemailsInChatThread		= true;
		public static bool   ShowRecordedCallsInChatThread	= true;
		public static bool   EnableSoundForIncomingChat		= true;
		public static bool   EnableSoundForIncomingCall		= true;
//		public static bool   EnableSoundForOutgoingCall		= true;				//See NOTE in SoundsViewModel
		public static string MyChatBubbleColor				= "Blue";
		public static string MyConversantChatBubbleColor	= "Gull Gray";		//TODO: What does this match to.
		public static string AudioInputDevice				= "";				//No default
		public static string AudioOutputDevice				= "";				//No default

		public static string CountryCode					= "+1";				//Likely will change with home country of branded vendor.
		public static string CountryAbbreviation			= "us";

		// Non-User selectable settings                                           
		public static string IncomingCallSoundFile			= @"Branding\Calling\IncomingCall.wav";
		public static string OutgoingCallSoundFile			= @"Branding\Calling\OutgoingCall.wav";
		public static string CallClosedSoundFile			= @"Branding\Calling\CallClosed.wav";
		public static string IncomingMessageSoundFile		= @"Branding\Messages\IncomingMessage.wav";

		// Auto Login settings                                                    
		public static bool AutoLogin						= true;
		public static bool RememberPassword					= true;

        // Enable SMS
        public static bool EnableSMS                        = true;
	}

	#endregion


	//=========================================================================
	//Model related config classes:
	//	- SIP
	//	- XMPP
	//=========================================================================
	#region SIP settings class

	//This should almost NEVER be overridden.
	public static class Sip
	{ 
		public static void Init()
		{
		}

		public static bool		UseTls							= true;	//Added so we can override in Debug menu
		public static bool		DoProxyCheck					= true;	//Added so we can override in Debug menu

		public static bool		EnableIceMedia					= false;
		public static bool		EnableIceSecurity				= true;
		public static bool		TopologyEncryption				= true;
		public static bool		TopologyTurned					= true;
		public static bool		SipUdp							= true;
		public static bool		SipTcp							= true;
		public static bool		SipTls							= false;
		public static bool		KeepAlive						= true;
		public static bool		UseRPort						= true;
		public static int		RegistrationRefreshInterval		= 0;
		public static int		FailureInterval					= 0;
		public static int		OptionsKeepAliveInterval		= 30;
		public static int		MinimumSessionTime				= 1800;
		public static bool		SupportSessionTimer				= true;
		public static bool		InitiateSessionTimer			= false;
		public static string	TagProlog						= "Voxox";					//TODO-BRAND?
		public static int		SipOptionRegisterTimeout		= 2940;
		public static int		SipOptionPublishTimeout			= 300;
		public static bool		SipOptionUseOptionsRequest		= true;
		public static bool		SipOptionP2PPresence			= false;
		public static bool		SipOptionUseTypingState			= true;
		public static bool		SipOptionChatWithoutPresence	= true;
		public static bool		EnableAEC						= true;
		public static bool		EnableAGC						= true;
		public static bool		EnableNoiseSuppression			= true;
		public static bool		DebugLogging					= true;
		public static int		Topology						= 1;	//SipAgent.Topology.TOPOLOGY_ICE
		public static int		CallEncryption					= 0;

		//NOTE: SipOptionUuid is set for each user, so it is NOT set as a global setting.  
		//	Goal is to retain it between user logins, but that is not critical.
	}

	#endregion

	#region XMPP settings class

	public static class Xmpp
	{
		private static int mPortDefault = 5222;
		private static int mPortSecure  =  443;

		public static void Init()
		{
		}

		public static void setOverrides( string server, int port )
		{
			//NOTE: Not acutally using the 'port' option right now.
			//If 'server' is empty or mathches original server, clear the overrides.
			if ( String.IsNullOrEmpty( server ) || server == Server )
			{
				ServerOverride = String.Empty;
				PortOverride   = 0;

			}
			else 
			{ 
				ServerOverride = server;
				PortOverride   = ( port > 0  ? port : mPortDefault );
			}
		}

		public static bool hasOverrides()
		{
			return !String.IsNullOrEmpty( ServerOverride );
		}

		public static string ServerEx
		{ 
			get { return ( String.IsNullOrEmpty(ServerOverride)  ? Server : ServerOverride ); }
			private set {}
		}

		public static int PortEx
		{ 
			get { return ( PortOverride > 0  ? PortOverride : Port ); }
			private set {}
		}

		// API servers and keys                                                          
		public static string Server					= "vx-im.voxox.com";
		public static string Domain					= "voxox.com";
		public static string ResourcePrefix			= "voxox3.0";
		public static int    Port					=  mPortSecure;
		public static bool   AllowPlainPassword		= true;

		public static XmppParameters.TlsOptionType TlsOption	 = XmppParameters.TlsOptionType.TLS_ENABLED;
		public static XmppParameters.AuthMechType  AuthMechanism = XmppParameters.AuthMechType.AUTH_MECH_SCRAM_SHA_1;

		public static bool	 UseProxy				= false;
		public static string ProxyServer			= "";
		public static int	 ProxyPort				= 0;

		//A couple overrides to handle lab/QA environments.
		private static string ServerOverride = "";
		private static int    PortOverride	 = 0;
	}

	#endregion


	//=========================================================================
	// A separate static class that contains some config/branding utility methods
	//=========================================================================
	#region AppConfigUtil

	public static class AppConfigUtil
	{
		//This will take a filepath and create properly formatted URI
		public static string makeComponentUri( string pathIn )
		{
			string result = pathIn;

			if ( !pathIn.Contains( "component" ) )
			{
				Assembly assembly = Assembly.GetExecutingAssembly();
				string packUri = string.Format("/{0};component", assembly.GetName().Name);
				result = packUri + pathIn;
			}

			return result;
		}

		public static string makeApplicationUri( string pathIn )
		{
			string result = pathIn;

			if ( !pathIn.Contains( "pack://application:" ) )
			{ 
//				result = "pack://application:,,," + pathIn;
				result = "pack://application:,,," + makeComponentUri( pathIn );
			}

			return result;
		}

		public static Uri makeUri( string pathIn )
			{
			//makeApplicationUri() is the only format that works on startup.
			//	However, we get an exception in .xaml file.
			//	Do we need some info from the generated URI?
			string ensurePackSchemeIsKnown = System.IO.Packaging.PackUriHelper.UriSchemePack;
			string uriPath = makeApplicationUri( pathIn );
			Uri    uri     = null;

			try
			{
				uri = new Uri( uriPath );
			}

			catch ( Exception /*ex*/ )
			{
				uri = null;
			}

			return uri;
		}

		public static BitmapImage makeImage( string imagePath )
		{
			string ensurePackSchemeIsKnown = System.IO.Packaging.PackUriHelper.UriSchemePack;
			BitmapImage result = new BitmapImage();

			try 
			{
				//BitmapCacheOption.OnDemand works with makeComponentUri, but OnLoad does NOT!
//				String uriPath = makeComponentUri  (imagePath);
//				String uriPath = makeApplicationUri(imagePath);
				String uriPath = imagePath;
//				Uri uri = new Uri( makeComponentUri(imagePath), UriKind.RelativeOrAbsolute );
				Uri uri = new Uri( uriPath, UriKind.RelativeOrAbsolute );
	//			Uri uri = new Uri( BaseUriHelper.GetBaseUri(imagePath), imagePath );
	//			BitmapImage result = new BitmapImage( uri );

				result.BeginInit();
				result.CacheOption = BitmapCacheOption.OnDemand;
				result.UriSource = uri;
				result.EndInit();
			}

			catch ( Exception /*ex*/ )
			{
				result = null;
			}

			return result;
		}

		public static SolidColorBrush makeSolidColorBrush( Color color )
		{
			SolidColorBrush result = new SolidColorBrush( color );
			return result;
		}

		public static ImageBrush makeImageBrush( String imagePath, bool applyStdAttributes )
		{
			string ensurePackSchemeIsKnown = System.IO.Packaging.PackUriHelper.UriSchemePack;
			ImageBrush  result;

			Uri uri = new Uri( imagePath, UriKind.RelativeOrAbsolute );
			BitmapImage bm = new BitmapImage();

			try
			{
				bm.BeginInit();
				bm.CacheOption = BitmapCacheOption.OnDemand;
				bm.UriSource = uri;
				bm.EndInit();

				result = new ImageBrush( bm );

				if ( applyStdAttributes )
				{ 
					if ( result != null )		//TODO-BRAND: Can these properties be set in xaml?
					{
						result.AlignmentY = AlignmentY.Top;
						result.Stretch	  = Stretch.None;
					}
				}
			}

			catch (Exception /*ex*/ )
			{
				result = null;
			}

			return result;
		}

		public static LinearGradientBrush makeGradientBrush( Color startColor, Color endColor )
		{
			GradientStopCollection gsc = new GradientStopCollection();
			gsc.Add( new GradientStop() { Color = startColor, Offset = 0.0 } );
			gsc.Add( new GradientStop() { Color = endColor,   Offset = 1.0 } );

			Point startPoint = new Point( 0.5, 0.0 );
			Point   endPoint = new Point( 0.5, 1.0 );

			return new LinearGradientBrush( gsc, startPoint, endPoint );
		}

		public static String makeExtranetUrl( String extranetBase, bool https, String suffix )
		{
			String result = https ? "https://" : "http://";

			result += extranetBase;

			if ( !String.IsNullOrEmpty( suffix ) )
			{
				result += "/" + suffix;
			}

			//Should look like this:
			//	Non-production: https://lando-cp-extranet.voxox.com/xxx
			//	Production:		https://cp-extranet.voxox.com/xxx
			return result;
		}

		public static String makeExtranetUrl( String env, String partnerId, bool https, String suffix )
		{
			const String extranetBase = "extranet.voxox.com";

			String result = https ? "https://" : "http://";

			//Allow for actual production values.
			if ( !String.IsNullOrEmpty( env ) )
			{ 
				result += env + "-";
			}
			
			if ( !String.IsNullOrEmpty( partnerId ) )	//Likely should NEVER be empty or null.
			{ 
				result += partnerId + "-";
			}
			else 
			{ 
				result += Config.Branding.PartnerIdentifier + "-";
			}
			
			result += extranetBase;

			if ( !String.IsNullOrEmpty( suffix ) )
			{
				result += "/" + suffix;
			}

			//Should look like this:
			//	Non-production: https://lando-cp-extranet.voxox.com/xxx
			//	Production:		https://cp-extranet.voxox.com/xxx
			return result;
		}

	}	//class AppConfigUtil

		#endregion


	#region Brand class - Main branding logic.

	//=========================================================================
	// A separate static class that contains config/branding:
	//	- Forced static class initialization
	//	- Branding based on #if
	//	- Special builds based on #if
	//=========================================================================
	//This is used to force all other static classes to initialize
	public static class Brand
	{

		private static String	 origApiServer    = "";
		private static String	 origXmppServer   = "";
		private static String	 origExtranetHost = "";
		private static String	 origPartnerId    = "";
		private static LoginMode origLoginMode	  = LoginMode.Default;
		private static BrandId   origBrandId	  = BrandId.None;

		public static Boolean brandIsVoxox()		{  return mBrandId == BrandId.Voxox;		}	
		public static Boolean brandIsCloudPhone()	{  return mBrandId == BrandId.CloudPhone;	}
		public static Boolean brandIsVoicepass()	{  return mBrandId == BrandId.VoicePass;	}
		public static Boolean brandIsVtc()			{  return mBrandId == BrandId.Vtc;			}
		public static Boolean brandIsNone()			{  return mBrandId == BrandId.None;			}

		//These methods are intended to allow Advanced Settings to 're-brand' an app completely without recompiling.
		public static void setBrandVoxox()			{  mBrandId = BrandId.Voxox;		}	
		public static void setBrandCloudPhone()		{  mBrandId = BrandId.CloudPhone;	}
		public static void setBrandVoicepass()		{  mBrandId = BrandId.VoicePass;	}
		public static void setBrandVtc()			{  mBrandId = BrandId.Vtc;			}
		public static void setBrandNone()			{  mBrandId = BrandId.None;			}

		public static BrandId mBrandId = BrandId.None;

		public static void init()
		{ 
			if ( origBrandId == BrandId.None )
			{ 
				initBrandId();
			}

			initPhase2();
		}


		public static void reinit( BrandId brandId )
		{
			origBrandId = mBrandId;
			mBrandId	= brandId;
//			initPhase2();
		}

		private static void initPhase2()
		{
			initPrivate();
			doBranding();
			doSpecialBuilds();

			Brushes.Init();		//Re-init Brushes since colors may have changed.

			applyAdvancedSettings();
		}

		private static void initBrandId()
		{
			//NOTE: This should be the ONLY place we use #if, based on new 'universal build' work.
#if BRAND_VOICEPASS
			mBrandId = BrandId.VoicePass;
#elif BRAND_CLOUDPHONE
			mBrandId = BrandId.CloudPhone;
#elif BRAND_VOXOX
			mBrandId = BrandId.Voxox;
#elif BRAND_VTC
			mBrandId = BrandId.Vtc;
#else
			mBrandId = BrandId.Voxox;		//TODO: What should default be?  Just throw error?
#endif
		}

		private static void applyAdvancedSettings()
		{ 
			if ( SettingsManager.Instance.Advanced != null )
			{ 
				String	  apiEnv    = SettingsManager.Instance.Advanced.ApiEnv;
				String	  apiEnvMod = SettingsManager.Instance.Advanced.ApiEnvModifier;
				String	  partnerId = SettingsManager.Instance.Advanced.PartnerIdentifier;
				LoginMode loginMode = SettingsManager.Instance.Advanced.LoginMode;

				String effectiveApiServer    = Config.Brand.makeApiServer   ( apiEnv, apiEnvMod );
				String effectiveXmppServer   = Config.Brand.makeXmppServer  ( apiEnv, apiEnvMod );
				String effectiveExtranetHost = AppConfigUtil.makeExtranetUrl( apiEnv, apiEnvMod, true, "" );

				Rebrand( effectiveApiServer, effectiveXmppServer, effectiveExtranetHost, partnerId, loginMode );
			}
		}


		//This forces initialization of Config classes.
		//	Be sure to add any NEW classes here.  Be mindful of dependencies, as we have with Colors and Brushes.
		private static void initPrivate()
		{
			UI.Init();
			Colors.Init();
			Brushes.Init();		//Brushes MUST came after Colors, since Colors is used by Brushes

			Branding.Init();

			DialpadSounds.Init();
			Settings.Init();

			API.Init();
			Urls.Init();
			Support.Init();

			Sip.Init();
			Xmpp.Init();
		}

		//Set brand-specific values here, likely use #if for 'brand' and/or 'release/dev/qa'
		//We have two types of 'branding'
		//	- White-label as we do for HP
		//	- Dev/QA - when we need to point to different environments for testing
		private static void doBranding()
		{
			if ( Brand.brandIsVoicepass() )
			{ 
				brandAsVoicepass();
			}
			else if ( Brand.brandIsVoxox() )
			{ 
				brandAsVoxox();
			}
			else if ( Brand.brandIsCloudPhone() )
			{ 
				brandAsCloudPhone();
			}
			else if ( Brand.brandIsVtc() )
			{ 
				brandAsVtc();
			}
			else 
			{ 
				//In this case, we just be using previously defined values.
			}
		}

		//Dev, QA, Lab, Prosumer... Whatever you want to define and bracket in #if/#endif directives.
		private static void doSpecialBuilds()
		{

		}
	
		private static void brandAsVoicepass()
		{
			bool useProduction = false;

			if ( useProduction )
			{ 
				//App-level settings
//				Config.API.ApiKeyPrivate	= "52a36e27-b3b5-11e4-b90b-74867aa81509";
				Config.API.ApiServer		= "vx-api.voxox.com";

				//URLs
				Config.Urls.createExtranetUrls( "vp-extranet.voxox.com" );

				//API-Level settings
				Config.API.MixPanelToken	= "f303a134ca321ade36c5a4bc74702a59";
			}
			else
			{ 
				String devEnv             = "lando";
				String extranetIdentifier = "hp";
				String partnerIdentifier  = "vx";
				String mixpanelToken      = "f303a134ca321ade36c5a4bc74702a59";	//Lab	- Using same as VoicePass

				brandForNonProduction( devEnv, extranetIdentifier, partnerIdentifier, mixpanelToken, false );
			}


			// App-level settings                                                             
			Config.Branding.AppMutex					= "{7DAFAEE5-20F3-4DA9-B9CC-3155655B9542}";

			// App name, Company Name, App icon and folder settings       
			Config.Branding.AppName						= "Voice Pass";	
			Config.Branding.CompanyName					= "HP";
			Config.Branding.AppDataFolderName			= "VoicePass";
			Config.Branding.ApplicationIcon				= @"/Branding/_BrandVoicepass/hp-contact.ico";
			Config.Branding.ApplicationIcon2			= @"/HPIcon.ico";	//Temp for HP Beta on Login window
			Config.Branding.PartnerIdentifier			= "hp";
			Config.Branding.ClientId					= "com.hp.windows";
			Config.Branding.LoginMode					= LoginMode.Email;

			//Support
			Config.Support.HelpContactSupportLink		= "https://hpconnectedapps.centercode.com/login.html";
			Config.Support.Settings_PhoneNo				= "(844) 447-8391";
			Config.Support.Settings_Email				= "hpbeta@voxox.com";

			//UI
			Config.UI.Contact_Label						= Config.Branding.AppName;

			// UI - Images                                                                 
			Config.UI.InNetworkContact					= AppConfigUtil.makeComponentUri  ( @"/Branding/_BrandVoicepass/hp-contact.png" );
			Config.UI.SystrayIconUri					= AppConfigUtil.makeApplicationUri( @"/Branding/_BrandVoicepass/hp-contact.ico" );

			Config.UI.AboutLogoImage					= AppConfigUtil.makeImage( @"/Branding/_BrandVoicepass/logo-xxhdpi.png" );
			Config.UI.NothingHereImage					= AppConfigUtil.makeImage( @"/Branding/_BrandVoicepass/hp-nothing-here.png" );
			Config.UI.LoginLogoImage					= AppConfigUtil.makeImage( @"/Branding/_BrandVoicepass/voice-pass.png" );

            Config.UI.NavigationContacts                = AppConfigUtil.makeImage(@"/Branding/_BrandVoicepass/nav/navigation-contacts.png");
            Config.UI.NavigationFax                     = AppConfigUtil.makeImage(@"/Branding/_BrandVoicepass/nav/navigation-fax.png");
            Config.UI.NavigationKeypad                  = AppConfigUtil.makeImage(@"/Branding/_BrandVoicepass/nav/navigation-keypad.png");
            Config.UI.NavigationMessages                = AppConfigUtil.makeImage(@"/Branding/_BrandVoicepass/nav/navigation-messages.png");
            Config.UI.NavigationRecentCalls             = AppConfigUtil.makeImage(@"/Branding/_BrandVoicepass/nav/navigation-recent-calls.png");
            Config.UI.NavigationSettings                = AppConfigUtil.makeImage(@"/Branding/_BrandVoicepass/nav/navigation-settings.png");
            Config.UI.NavigationShopping                = AppConfigUtil.makeImage(@"/Branding/_BrandVoicepass/nav/navigation-shopping.png");

			//NavBar images - Hover/Active
			//	NOTE: We just use 'defaults' previously defined for Hover/Active
//			Config.UI.NavigationContactsHover			= AppConfigUtil.makeImage(@"/Branding/_BrandVoicepass/nav/navigation-contacts-active.png");
//			Config.UI.NavigationFaxHover				= AppConfigUtil.makeImage(@"/Branding/_BrandVoicepass/nav/navigation-fax-active.png");
//			Config.UI.NavigationKeypadHover				= AppConfigUtil.makeImage(@"/Branding/_BrandVoicepass/nav/navigation-keypad-active.png");
//			Config.UI.NavigationMessagesHover			= AppConfigUtil.makeImage(@"/Branding/_BrandVoicepass/nav/navigation-messages-active.png");
//			Config.UI.NavigationRecentCallsHover		= AppConfigUtil.makeImage(@"/Branding/_BrandVoicepass/nav/navigation-recent-calls-active.png");
//			Config.UI.NavigationSettingsHover			= AppConfigUtil.makeImage(@"/Branding/_BrandVoicepass/nav/navigation-settings-active.png");
//			Config.UI.NavigationShoppingHover			= AppConfigUtil.makeImage(@"/Branding/_BrandVoicepass/nav/navigation-shopping-active.png");

			//UI - Images for backgrounds
			//	None

			//Colors
			Config.Colors.Color_Login_Bg_Start			= Config.Colors.Color_Hp_VoicePass_Background;	//Same values intentionally.
			Config.Colors.Color_Login_Bg_End			= Config.Colors.Color_Hp_VoicePass_Background;
		}	//brandAsVoicepass()

		private static void brandAsVoxox()
		{
			bool useProduction = false;

			if ( useProduction )
			{ 
				//App-level settings
//				Config.API.ApiKeyPrivate	= "52a36e27-b3b5-11e4-b90b-74867aa81509";
				Config.API.ApiServer		= "vx-api.voxox.com";

				//URLs
				Config.Urls.createExtranetUrls( "vx-extranet.voxox.com" );

				//API-Level settings
				Config.API.MixPanelToken	= "f303a134ca321ade36c5a4bc74702a59";	//Using same as VoicePass
			}
			else
			{ 
				String devEnv             = "lando";
				String extranetIdentifier = "vx";
				String partnerIdentifier  = "vx";
				String mixpanelToken      = "f303a134ca321ade36c5a4bc74702a59";	//Lab	- Using same as VoicePass

				brandForNonProduction( devEnv, extranetIdentifier, partnerIdentifier, mixpanelToken, false );
			}

			// App-level settings                                                             
			Config.Branding.AppMutex					= "{52938947-836F-41E3-9DA4-200995F50248}";

			// App name, Company Name, App icon and folder settings       
			Config.Branding.AppName						= "Voxox";
			Config.Branding.CompanyName					= "Telcentris";
			Config.Branding.AppDataFolderName			= "Voxox";
			Config.Branding.ApplicationIcon				= @"/Branding/_BrandVoxox/app.ico";
			Config.Branding.ApplicationIcon2			= @"/Branding/_BrandVoxox/app.ico";	//Same as above, at least until HP Beta is over.
			Config.Branding.PartnerIdentifier			= "vx";
			Config.Branding.ClientId					= "com.voxox.windows";
			Config.Branding.LoginMode					= LoginMode.Mobile;

			//Support
			Config.Support.HelpContactSupportLink		= "http://www.voxox.com/contact";
			Config.Support.Settings_PhoneNo				= "(844) 447-8391";
			Config.Support.Settings_Email				= "hpbeta@voxox.com";

			//UI
			Config.UI.Contact_Label						= Config.Branding.AppName;

			// UI - Images                                                                 
			Config.UI.InNetworkContact					= AppConfigUtil.makeComponentUri  ( @"/Branding/_BrandVoxox/contact.png" );
			Config.UI.SystrayIconUri					= AppConfigUtil.makeApplicationUri( @"/Branding/_BrandVoxox/app.ico" );

//			Config.UI.AboutLogoImage					= AppConfigUtil.makeImage( @"/Branding/_BrandVoxox/logo-xxhdpi.png" );
			Config.UI.AboutLogoImage					= AppConfigUtil.makeImage( @"/Branding/_BrandVoxox/contact.png" );		//TODO-BRAND: Temporary until we get actual asset.
			Config.UI.LoginLogoImage					= AppConfigUtil.makeImage( @"/Branding/_BrandVoxox/login-logo2.png" );
			Config.UI.NothingHereImage					= AppConfigUtil.makeImage( @"/Branding/_BrandVoxox/no-entry.png" );

			//NavBar images - Normal
			//NOTE: Just use 'defaults' previously defined.
//			Config.UI.NavigationContacts                = AppConfigUtil.makeImage(@"/Branding/Navigation/navigation-contacts.png");
//			Config.UI.NavigationFax                     = AppConfigUtil.makeImage(@"/Branding/Navigation/navigation-fax.png");
//			Config.UI.NavigationKeypad                  = AppConfigUtil.makeImage(@"/Branding/Navigation/navigation-keypad.png");
//			Config.UI.NavigationMessages                = AppConfigUtil.makeImage(@"/Branding/Navigation/navigation-messages.png");
//			Config.UI.NavigationRecentCalls             = AppConfigUtil.makeImage(@"/Branding/Navigation/navigation-recent-calls.png");
//			Config.UI.NavigationSettings                = AppConfigUtil.makeImage(@"/Branding/Navigation/navigation-settings.png");
//			Config.UI.NavigationShopping                = AppConfigUtil.makeImage(@"/Branding/Navigation/navigation-shopping.png");

			//NavBar images - Hover/Active
			//NOTE: Just use 'defaults' previously defined.
//			Config.UI.NavigationContactsHover           = AppConfigUtil.makeImage(@"/Branding/Navigation/navigation-contacts.png");
//			Config.UI.NavigationFaxHover                = AppConfigUtil.makeImage(@"/Branding/Navigation/navigation-fax.png");
//			Config.UI.NavigationKeypadHover             = AppConfigUtil.makeImage(@"/Branding/Navigation/navigation-keypad.png");
//			Config.UI.NavigationMessagesHover           = AppConfigUtil.makeImage(@"/Branding/Navigation/navigation-messages.png");
//			Config.UI.NavigationRecentCallsHover        = AppConfigUtil.makeImage(@"/Branding/Navigation/navigation-recent-calls.png");
//			Config.UI.NavigationSettingsHover           = AppConfigUtil.makeImage(@"/Branding/Navigation/navigation-settings.png");
//			Config.UI.NavigationShoppingHover           = AppConfigUtil.makeImage(@"/Branding/Navigation/navigation-shopping.png");

		}	//brandAsVoxox()

		private static void brandAsCloudPhone()
		{
			bool useProduction = false;

			if ( useProduction )
			{ 
				//App-level settings
//				Config.API.ApiKeyPrivate	= "52a36e27-b3b5-11e4-b90b-74867aa81509";	//TODO: Get from server team <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
				Config.API.ApiServer		= "vx-api.voxox.com";						//TODO: Get from server team <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

				Config.Branding.PartnerIdentifier = "cp";		//Temporary because lab/QA environments are set correctly.

				//URLs
//				Config.Urls.createExtranetUrls( "cp-extranet.voxox.com" );

				//See NOTE following this code block.
				//NOTE: The CloudPhone URLs do NOT follow previous naming pattern, so we create them individually here.
				String baseHost = "manage.cloudphone.com";
				Config.Urls.ExtranetBase_URL			= AppConfigUtil.makeExtranetUrl( baseHost, false, "" );

				Config.Urls.BuyCredits_URL				= "";	//Not supported in CloudPhone
				Config.Urls.CallerIdNavigationLink		= AppConfigUtil.makeExtranetUrl( baseHost, false, "vxportal/people/detail" );
				Config.Urls.ForgotPasswordURL			= AppConfigUtil.makeExtranetUrl( baseHost, false, "vxlogin/password/forgot" );
				Config.Urls.ReachmeNavigationLink		= AppConfigUtil.makeExtranetUrl( baseHost, false, "vxportal/people/detail" );
				Config.Urls.SignupURL					= AppConfigUtil.makeExtranetUrl( baseHost, false, "signupvx" );
				Config.Urls.ManageAccountURL			= AppConfigUtil.makeExtranetUrl( baseHost, false, "" );
				Config.Urls.TermsandConditionsURL		= "http://voxox.com/terms";

				//API-Level settings
				Config.API.MixPanelToken				= "b30d64b0a115c66116bac2c15fa06173";
//				Config.API.MixPanelToken				= "20cadf101ec5ae591304ece2953a3022";	///Some reference to this in Docs, so....?			
			}
			else
			{ 
//				String devEnv             = "vader";
				String devEnv             = "lando";
				String extranetIdentifier = "cp";
				String partnerIdentifier  = "vx";
				String mixpanelToken      = "ff117c952318fab78d761654f7d77903";	//Lab	- Using same as VoicePass

				brandForNonProduction( devEnv, extranetIdentifier, partnerIdentifier, mixpanelToken, true );
			}

			// App-level settings                                                             
			Config.Branding.AppMutex					= "{24748C8A-CB4E-402A-B51E-9ADD74BAD36E}";

			// App name, Company Name, App icon and folder settings       
			Config.Branding.AppName						= "Cloud Phone";
			Config.Branding.CompanyName					= "Cloud Phone";
			Config.Branding.AppDataFolderName			= "CloudPhone";
			Config.Branding.ApplicationIcon				= @"/Branding/_BrandCloudPhone/app.ico";
			Config.Branding.ApplicationIcon2			= @"/Branding/_BrandCloudPhone/app.ico";	//Same as above, at least until HP Beta is over.
//			Config.Branding.PartnerIdentifier			= "cp";		//See overrides above.
			Config.Branding.ClientId					= "com.cloudphone.windows";
//			Config.Branding.ClientId					= "com.telcentris.cloudphoneenterprise";	//TODO: Temp until we get proper value set on server.
			Config.Branding.LoginMode					= LoginMode.Mobile;

//			Config.Settings.EnableSMS					= false;	//This *should* come from login/CompanyOptions, but server team is not there yet.
			Config.Settings.EnableSMS					= true;		//FOR GODADDY DEMO.

			//Support
			Config.Support.HelpContactSupportLink		= "http://assist.voxox.com";
//			Config.Support.Settings_PhoneNo				= "866-514-VOIP (8647)";
			Config.Support.Settings_PhoneNo				= "866-514-8647";
			Config.Support.Settings_Email				= "assist@voxox.com";

			//UI
			Config.UI.Contact_Label						= Config.Branding.AppName;

			// UI - Images                                                                 
			// Images - used and interpreted in .xaml                                                        
			Config.UI.CallingNoAvatar                    = @"/Branding/_BrandCloudPhone/avatar-person.png";
			Config.UI.ContactNoAvatar                    = @"/Branding/_BrandCloudPhone/avatar-person.png";
			Config.UI.MessagingNoAvatar                  = @"/Branding/_BrandCloudPhone/avatar-message.png";
			Config.UI.RichMediaLoading                   = @"/Branding/_BrandCloudPhone/avatar-message.png";

			Config.UI.InNetworkContact					= AppConfigUtil.makeComponentUri  ( @"/Branding/_BrandCloudPhone/InNetworkContact.png" );
			Config.UI.SystrayIconUri					= AppConfigUtil.makeApplicationUri( @"/Branding/_BrandCloudPhone/app.ico" );

			Config.UI.AboutLogoImage					= AppConfigUtil.makeImage( @"/Branding/_BrandCloudPhone/AboutLogoImage.png" );
			Config.UI.LoginLogoImage					= AppConfigUtil.makeImage( @"/Branding/_BrandCloudPhone/LoginLogoImage.png" );
			Config.UI.NothingHereImage					= AppConfigUtil.makeImage( @"/Branding/_BrandVoxox/no-entry.png" );


			//NavBar images - Normal
			Config.UI.NavigationContacts				= AppConfigUtil.makeImage(@"/Branding/_BrandCloudPhone/Nav/navigation-contacts.png");
			Config.UI.NavigationFax						= AppConfigUtil.makeImage(@"/Branding/_BrandCloudPhone/Nav/navigation-fax.png");
			Config.UI.NavigationKeypad					= AppConfigUtil.makeImage(@"/Branding/_BrandCloudPhone/Nav/navigation-keypad.png");
			Config.UI.NavigationMessages				= AppConfigUtil.makeImage(@"/Branding/_BrandCloudPhone/Nav/navigation-messages.png");
			Config.UI.NavigationRecentCalls				= AppConfigUtil.makeImage(@"/Branding/_BrandCloudPhone/Nav/navigation-recent-calls.png");
			Config.UI.NavigationSettings				= AppConfigUtil.makeImage(@"/Branding/_BrandCloudPhone/Nav/navigation-settings.png");
			Config.UI.NavigationShopping				= AppConfigUtil.makeImage(@"/Branding/_BrandCloudPhone/Nav/navigation-shopping.png");

			//NavBar images - Hover/Active
			Config.UI.NavigationContactsHover			= AppConfigUtil.makeImage(@"/Branding/_BrandCloudPhone/Nav/navigation-contacts-active.png");
			Config.UI.NavigationFaxHover				= AppConfigUtil.makeImage(@"/Branding/_BrandCloudPhone/Nav/navigation-fax-active.png");
			Config.UI.NavigationKeypadHover				= AppConfigUtil.makeImage(@"/Branding/_BrandCloudPhone/Nav/navigation-keypad-active.png");
			Config.UI.NavigationMessagesHover			= AppConfigUtil.makeImage(@"/Branding/_BrandCloudPhone/Nav/navigation-messages-active.png");
			Config.UI.NavigationRecentCallsHover		= AppConfigUtil.makeImage(@"/Branding/_BrandCloudPhone/Nav/navigation-recent-calls-active.png");
			Config.UI.NavigationSettingsHover			= AppConfigUtil.makeImage(@"/Branding/_BrandCloudPhone/Nav/navigation-settings-active.png");
			Config.UI.NavigationShoppingHover			= AppConfigUtil.makeImage(@"/Branding/_BrandCloudPhone/Nav/navigation-shopping-active.png");
		}	//brandAsCloudPhone()

		//JRT - 2016.01.11 - This is temporary for demo.  Use CloudPhone branding and change as needed to work with Andorid VTC builds on Skywalker.
		private static void brandAsVtc()
		{
			//Copy CloudPhone
			brandAsCloudPhone();

			String devEnv             = "skywalker";
			String extranetIdentifier = "vtc";
			String partnerIdentifier  = "vtc";
			String mixpanelToken      = "ff117c952318fab78d761654f7d77903";	//Lab	- Using same as VoicePass

			brandForNonProduction( devEnv, extranetIdentifier, partnerIdentifier, mixpanelToken, true );

			//Now add needed changes. 
			//NOTE: there are no Production values here.

			Config.Branding.ClientId		  = "com.vtc.windows";
			Config.Settings.EnableSMS		  = true;	//This *should* come from login/CompanyOptions, but server team is not there yet.
		}

		private static void brandForNonProduction( String devEnv, String extranetIdentifier, String partnerIdentifier, String mixpanelToken, bool isCloudPhone )
		{
			//App-level settings
			Config.API.ApiServer		= devEnv + "-api.voxox.com";
			Config.API.MixPanelToken	= mixpanelToken;
				
			Config.Branding.PartnerIdentifier = partnerIdentifier;		//I think no longer used.

			//XMPP-level settings
			Config.Xmpp.Server			= devEnv + "-im.voxox.com";
//			Config.Xmpp.Port			= 5222;		//Dev XMPP servers now support port 443.

			//NOTE: The CloudPhone URLs do NOT follow previous naming pattern, so we create them individually here.
			String baseHost = devEnv + "-" + extranetIdentifier + "-extranet.voxox.com";

			if ( isCloudPhone )
			{ 
				Config.Urls.ExtranetBase_URL			= AppConfigUtil.makeExtranetUrl( baseHost, false, "" );

				Config.Urls.BuyCredits_URL				= "";	//Not supported in CloudPhone
				Config.Urls.CallerIdNavigationLink		= AppConfigUtil.makeExtranetUrl( baseHost, false, "vxportal/people/detail" );
				Config.Urls.ForgotPasswordURL			= AppConfigUtil.makeExtranetUrl( baseHost, false, "vxlogin/password/forgot" );
				Config.Urls.ReachmeNavigationLink		= AppConfigUtil.makeExtranetUrl( baseHost, false, "vxportal/people/detail" );
				Config.Urls.SignupURL					= AppConfigUtil.makeExtranetUrl( baseHost, false, "signupvx" );
				Config.Urls.ManageAccountURL			= AppConfigUtil.makeExtranetUrl( baseHost, false, "" );
				Config.Urls.TermsandConditionsURL		= "http://voxox.com/terms";
			}
			else
			{
				Config.Urls.createExtranetUrls( baseHost );
			}
		}

		//---------------------------------------------------------------------------
		//Rebranding related methods
		//---------------------------------------------------------------------------

		public static String getOriginalApiServer()
		{
			return ( String.IsNullOrEmpty( origApiServer ) ? API.ApiServer : origApiServer );
		}

		private static void ClearOriginalData()
		{ 
			origApiServer    = "";
			origXmppServer   = "";
			origExtranetHost = "";
			origPartnerId    = "";
		}

		private static void SaveOriginalData()
		{ 
			if ( String.IsNullOrEmpty( origApiServer ) )	//So we do not overwrite the original data
			{ 
				origApiServer    = API.ApiServer;
				origXmppServer   = Xmpp.Server;
				origExtranetHost = Urls.ExtranetBase_URL;
				origPartnerId    = Branding.PartnerIdentifier;
				origLoginMode	 = Branding.LoginMode;
			}
		}


		public static void UnRebrand()
		{
			Rebrand( origApiServer, origXmppServer, origExtranetHost, origPartnerId, origLoginMode );

			ClearOriginalData();
		}

		public static void Rebrand( String apiServer, String xmppServer, String extranetHost, String partnerId, LoginMode loginMode )
		{
			SaveOriginalData();

			if ( !String.IsNullOrEmpty( apiServer ) )
			{
				Config.API.ApiServer = apiServer;
			}

			if ( !String.IsNullOrEmpty( xmppServer ) )
			{
				Config.Xmpp.setOverrides( xmppServer, 0 );
			}

			if ( !String.IsNullOrEmpty( partnerId ) )
			{
				Config.Branding.PartnerIdentifier = partnerId;
			}

			if ( loginMode != LoginMode.Default )
			{
				Config.Branding.LoginMode = loginMode;
			}

			if ( !String.IsNullOrEmpty( extranetHost ) )
			{
				String extranetBase = extranetHost;
				String protocolKey  = "://";

				//Remove leading protocol
				int pos = extranetHost.IndexOf( protocolKey );

				if ( pos >= 0 )
				{
					extranetBase = extranetHost.Substring( pos + protocolKey.Length );
				}

				Config.Urls.createExtranetUrls( extranetBase );
			}
		}

		public static String makeApiServer( String env, String partnerId )
		{
			//Here we currently use either env or PartnerId.  Env takes precendence.
			String result = ( !String.IsNullOrEmpty( env ) ? env : partnerId );

			//If we have nothing, then use existing Branded PartnerIdentifier
			if ( String.IsNullOrEmpty( result ) )
			{
				result = Config.Branding.PartnerIdentifier;
			}

			result += "-api.voxox.com"; 

			return result;
		}

		public static String makeXmppServer( String env, String partnerId )
		{
			//Here we currently use either env or PartnerId.  Env takes precendence.
			String result = ( !String.IsNullOrEmpty( env ) ? env : partnerId );

			//If we have nothing, then use existing Branded PartnerIdentifier
			if ( String.IsNullOrEmpty( result ) )
			{
				result = Config.Branding.PartnerIdentifier;
			}

			result += "-im.voxox.com";

			return result;

		}

	}	//class Brand

		#endregion

}	//namespace Desktop.Config
