﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.ViewModels.Settings;	//SettingsViewModel

using System.Windows;				//Window

namespace Desktop.Views.Settings
{
    /// <summary>
    /// Interaction logic for Settings.xaml
    /// </summary>
    public partial class Settings : Window
    {
        public Settings()
        {
            InitializeComponent();
            
			DataContext = SettingsViewModel.Instance;
        }

        #region |RightPanel Navigation controls|

        protected override void OnClosing( System.ComponentModel.CancelEventArgs e )
        {
            e.Cancel = true;
            Visibility = System.Windows.Visibility.Hidden;
        }

        #endregion
    }
}

