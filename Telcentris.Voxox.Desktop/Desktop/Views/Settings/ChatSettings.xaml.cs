﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.ViewModels.Settings;	//ChatSettingsViewModel

using System.Windows.Controls;	//UserControl

namespace Desktop.Views.Settings
{
    /// <summary>
    /// Interaction logic for ChatSettings.xaml
    /// </summary>
    public partial class ChatSettings : UserControl
    {
        public ChatSettings()
        {
            InitializeComponent();
            this.DataContext = new ChatSettingsViewModel();
        }
    }
}
