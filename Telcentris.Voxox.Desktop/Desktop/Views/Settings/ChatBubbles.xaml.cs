﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.ViewModels.Settings;	//ChatBubblesViewModel

using System.Windows.Controls;	//UserControl

namespace Desktop.Views.Settings
{
    /// <summary>
    /// Interaction logic for ChatBubbles.xaml
    /// </summary>
    public partial class ChatBubbles : UserControl
    {
        public ChatBubbles()
        {
            InitializeComponent();
            this.DataContext = new ChatBubblesViewModel();
        }
    }
}
