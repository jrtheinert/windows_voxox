﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;				//ActionManager
using Desktop.ViewModels.Settings;	//SupportViewModel

using System.Diagnostics;			//Process
using System.Windows;				//Application, MessageBox, RoutedEventArgs
using System.Windows.Controls;		//UserControl
using System.Windows.Navigation;	//RequestNavigateEventArgs

namespace Desktop.Views.Settings
{
    /// <summary>
    /// Interaction logic for Support.xaml
    /// </summary>
    public partial class Support : UserControl
    {
        public Support()
        {
            InitializeComponent();
            this.DataContext = new SupportViewModel();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(LocalizedString.NotImplemented);
        }

        private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
			ActionManager.Instance.HandleUrl( e.Uri.AbsoluteUri );
            e.Handled = true;
        }

        private void PhoneNo_Click(object sender, RoutedEventArgs e)
        {
            foreach (Window win in Application.Current.Windows)
            {
                if (win.IsActive)
                    win.Hide();
            }
        }
    }
}
