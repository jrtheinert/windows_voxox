﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Sep 2015
 */
using Desktop.Model;				//BrandingChangedMessage
using Desktop.ViewModels.Settings;	//AdvancedSettingsViewModel

using GalaSoft.MvvmLight.Messaging;	//Messenger

using System.Windows;				//Window
using System.Windows.Controls;		//SelectionChangedEventArgs

namespace Desktop.Views.Settings
{
    /// <summary>
    /// Interaction logic for AdvancedSettingsWindow.xaml
    /// </summary>
    public partial class AdvancedSettingsWindowV2 : Window
    {
        public AdvancedSettingsWindowV2()
        {
            InitializeComponent();
            DataContext = new AdvancedSettingsViewModelV2();

			Messenger.Default.Register<BrandingChangedMessage>(this, OnBrandingChanged);
        }

		private void OnBrandingChanged( BrandingChangedMessage msg ) 
		{
			if ( msg.isActionCloseWindow() )
			{
				Close();
			}
			else if ( msg.isActionUpdateBrandComboBox() )
			{
				comboBoxBrand.SelectedIndex = msg.BrandSelectedIndex;
			}
		}

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
			AdvancedSettingsViewModelV2 vm = (AdvancedSettingsViewModelV2)DataContext;

			if ( vm != null )
			{
				if ( vm.Save() )
				{
					Close();
				}
			}
			else 
			{
				Close();
			}
        }
    }
}
