﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Telcentris.Voxox.ManagedSipAgentApi.ManagedSipDataTypes;	//PhoneCallState
using Desktop.Controls;				//UserControlWithAnimation
using Desktop.Model;				//PhoneCallMessage
using Desktop.ViewModels.Calling;

using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.Threading;	

using System;						//Type, TimeSpan, NotImplementedException
using System.Globalization;			//CultureInfo	(TODO: Move converter to Util)
using System.Windows.Controls;		//UserControl
using System.Windows.Data;			//IValueConverter

namespace Desktop.Views.Calling
{
    /// <summary>
    /// Interaction logic for CallsControl.xaml
    /// </summary>
    public partial class CallsControl : UserControlWithAnimation	//UserControl
    {
        public CallsControl()
        {
            InitializeComponent();

            //We CANNOT include this in base class UserControlWithAnimation, because it throws an exception doing FindResource.
            //	So we add it here.
            base.initializeClosingAnimation();

            // Message registers to monitor for Call list change
            Messenger.Default.Register<PhoneCallMessage>(this, (PhoneCallMessage msg) => { OnCallListChange(); });
            Messenger.Default.Register<MergeCallMessage>(this, (MergeCallMessage msg) => { OnCallListChange(); });
            Messenger.Default.Register<EndAndAcceptCallMessage>(this, (EndAndAcceptCallMessage msg) => { OnCallListChange(); });
            Messenger.Default.Register<JoinCallMessage>(this, (JoinCallMessage msg) => { OnCallListChange(); });
            Messenger.Default.Register<CallDoneMessage>(this, (CallDoneMessage msg) => { OnCallListChange(); });
        }

        private void OnCallListChange()
        {
            DispatcherHelper.CheckBeginInvokeOnUI(() =>
            {
                // Set Scroll to top
                SVCalls.ScrollToHome();
            });
        }
    }

    public class PhoneCallStateToTextConverter: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return value;

            var state = (PhoneCallState.CallState)value;

            switch(state)
            {
                case PhoneCallState.CallState.Ringing:
                case PhoneCallState.CallState.Dialing:
                    return "Calling...";
                case PhoneCallState.CallState.Incoming:
                    return "is Calling...";  
            }

            return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// Converter class used to show time in active calls
    /// </summary>
    public class CallTimeValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var v = value as TimeSpan?;
            if (v == null)
            {
                return value;
            }

            return Convert(v.Value);
        }

        public static string Convert(TimeSpan v)
        {
            return Util.TimeUtils.FormatTimeSpan(v);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
