﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using System.Windows;			//RoutedEventArgs
using System.Windows.Controls;	//UserControl

namespace Desktop.Views.Calling
{
	/// <summary>
	/// Interaction logic for FindMeNumbersNotSetupControl.xaml
	/// </summary>
	public partial class FindMeNumbersNotSetupControl : UserControl
	{
		public FindMeNumbersNotSetupControl()
		{
			InitializeComponent();
		}

        private void CustomButton_Click(object sender, RoutedEventArgs e)
        {
			Desktop.Model.ActionManager.Instance.ShowReachMeUrl();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
			Desktop.Model.ActionManager.Instance.ShowFindMeNumberNotSetupControl( false );
        }
	}
}
