﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Controls;				//UserControlWithAnimation

using System.Windows;				//RoutedEventArgs, DependencyPropertyChangedEventArgs, Visibility
using System.Windows.Controls;		//UserControl, TextChangedEventArgs
using System.Windows.Input;			//Key, KeyEventArgs
using System.Windows.Media;			//Brush

namespace Desktop.Views.Calling
{
    /// <summary>
    /// Interaction logic for AddToCallControl.xaml
    /// </summary>
    public partial class AddToCallControl : UserControlWithAnimation	//UserControl
    {
        #region | Private Class Variables |

        bool isInSearchMode = false;
		
		Brush brushNormal;
		Brush brushOverlay = new SolidColorBrush( Desktop.Config.Colors.Color_Overlay );

        #endregion

        #region | Constructors |

        public AddToCallControl()
        {
            InitializeComponent();
            
            //We CANNOT include this in base class UserControlWithAnimation, because it throws an exception doing FindResource.
            //	So we add it here.
            base.initializeClosingAnimation();

			initImages();

			brushNormal = ContactListArea.Background;
        }

        #endregion

        #region | Private Event Handler Procedures |

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            isInSearchMode = true;
            UpdateVisibilities();

            SearchText.Focus();
        }

        private void SearchText_LostFocus(object sender, RoutedEventArgs e)
        {
            CheckSearchMode();
        }

        private void SearchText_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                CheckSearchMode();
            }
        }

        private void SearchText_TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdateVisibilities();
        }

        #endregion

        #region | Private Procedures |

        /// <summary>
        /// Checks if the search mode is valid & Updates UI elements Visiblity accordingly
        /// </summary>
        private void CheckSearchMode()
        {
            if (string.IsNullOrWhiteSpace(SearchText.Text))
            {
                isInSearchMode = false;
            }

            UpdateVisibilities();
        }

        /// <summary>
        /// Update UI elements visiblity based on mode the control is in
        /// </summary>
        private void UpdateVisibilities()
        {
            if (isInSearchMode)
            {
                // Show Search Textbox, hiding the caption
                TitleWrap.Visibility  = Visibility.Collapsed;
                SearchWrap.Visibility = Visibility.Visible;

                // Display overlay if no text has been entered yet
                ContactListArea.Background = string.IsNullOrWhiteSpace(SearchText.Text) ? brushOverlay : brushNormal;
            }
            else
            {
                // Show the caption & search button, hiding the search filter
                TitleWrap.Visibility     = Visibility.Visible;
                SearchWrap.Visibility    = Visibility.Collapsed;

				ContactListArea.Background = brushNormal;
            }
        }

        private void OnDCChange(object sender, DependencyPropertyChangedEventArgs e)
        {
            var data = e.NewValue as Desktop.ViewModels.Contacts.ContactListViewModel;

            if (data != null && !string.IsNullOrWhiteSpace(data.SearchContacts))
            {
                isInSearchMode = true;

                TitleWrap.Visibility  = Visibility.Collapsed;
                SearchWrap.Visibility = Visibility.Visible;

				ContactListArea.Background = brushNormal;
            }
        }

		#endregion

		#region Custom ToggleButton TODO: Create custom class

		private System.Windows.Controls.Primitives.ToggleButton LastCheckedButton { get; set; }

		private static Image mCheckedImage;
		private static Image mUncheckedImage;

		Image CheckedImage   { get; set; }
		Image UncheckedImage { get; set; }

		private static void initImages()
		{
			if ( mCheckedImage == null )
			{ 
				mCheckedImage = new Image();
				mCheckedImage.Source = Desktop.Config.AppConfigUtil.makeImage( @"/Branding/select-circle-selected.png"  );
				mCheckedImage.Margin = new Thickness(0,0,0,0);

				mUncheckedImage = new Image();
				mUncheckedImage.Source = Desktop.Config.AppConfigUtil.makeImage( @"/Branding/select-circle.png"  );
				mCheckedImage.Margin = new Thickness(0,0,0,0);
			}
		}

		private void CheckedHandler( object sender, System.Windows.RoutedEventArgs  e )
		{
			if ( LastCheckedButton != null )
			{
				LastCheckedButton.IsChecked = false;
			}

			System.Windows.Controls.Primitives.ToggleButton b = (System.Windows.Controls.Primitives.ToggleButton) sender;
			
			ContentControl cc = (ContentControl)b.Content;
			cc.Content = mCheckedImage;

			LastCheckedButton = b;
		}

		private void UncheckedHandler( object sender, System.Windows.RoutedEventArgs  e )
		{
			System.Windows.Controls.Primitives.ToggleButton b = (System.Windows.Controls.Primitives.ToggleButton) sender;

			ContentControl cc = (ContentControl)b.Content;
			cc.Content = mUncheckedImage;

			if ( b == LastCheckedButton )
				LastCheckedButton = null;
		}

        #endregion
    }
}
