﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;				//ActionManager
using Desktop.ViewModels;			//ShowUpgradePlanMessage

using GalaSoft.MvvmLight.Messaging;	//Messenger

using System.Windows;				//RoutedEventArgs
using System.Windows.Controls;		//UserControl

namespace Desktop.Views.Calling
{
    /// <summary>
    /// Interaction logic for FeatureNotAvailableControl.xaml
    /// </summary>
    public partial class FeatureNotAvailableControl : UserControl
    {
        public FeatureNotAvailableControl()
        {
            InitializeComponent();
        }

        private void CustomButton_Click(object sender, RoutedEventArgs e)
        {
			ActionManager.Instance.ShowCallerIdUrl();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Send<ShowUpgradePlanMessage>(new ShowUpgradePlanMessage { ShowPopUp = false });
        }
    }
}
