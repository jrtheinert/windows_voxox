﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Controls;				//UserControlWithAnimation

using System.Windows.Controls;	//UserControl

namespace Desktop.Views.Calling
{
    /// <summary>
    /// Interaction logic for IncomingCallControl.xaml
    /// </summary>
    public partial class IncomingCallControl : UserControlWithAnimation	//UserControl
    {
        public IncomingCallControl()
        {
            InitializeComponent();

            //We CANNOT include this in base class UserControlWithAnimation, because it throws an exception doing FindResource.
            //	So we add it here.
            base.initializeClosingAnimation();
        }
    }
}
