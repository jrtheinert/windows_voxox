﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;				//ActionManager, SessionManager
using Desktop.Util;					//MixPanel
using Desktop.ViewModels;			//SignOutMessage, NotImplementedMessage, UpdateUnreadItemCountsMessage
using Desktop.ViewModels.Dialer;	//DialPadPointerSetMessage
using Desktop.ViewModels.Main;		//MainWindowViewModel, MainTabNavigateMessage, KeypadRegionClickMessage

using GalaSoft.MvvmLight.Messaging;	//Messenger, IMessenger

using System;						//EventArgs, Nullable
using System.Windows;				//Window, RoutedEventArgs, Point, FrameworkElement, MessageBox

//=============================================================================
// NOTES on Navigation logic - See NOTES in MainTabNavViewModel
//=============================================================================

namespace Desktop.Views.Main
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region | Private Class Variables |

        private IMessenger	messenger;
        private bool		doSignOut		= false;
        private bool		isCleaned		= false;
        private bool		isWindowLoaded	= false;

        #endregion

        #region | Constructors |

        public MainWindow()
        {
            InitializeComponent();
            messenger = Messenger.Default;
            SetupUI();

			Desktop.Model.MenuManager.Instance.buildMenuBar( MainMenu, false );

			messenger.Send<DialPadPointerSetMessage>(new DialPadPointerSetMessage { DialpadPointerVerticalOffset = MainNavTab.DialpadPointerVerticalOffset });
        }

        #endregion

        #region | Protected Overrides |

		//This is needed to address limitations in Menu/MenuItem being in more than one menu.
		override protected void OnActivated(EventArgs e)
		{
 			base.OnActivated(e);

			Desktop.Model.MenuManager.Instance.buildMenuBar( MainMenu, false );
			calcDialPadPopupPlacement();
			
			SessionManager.Instance.IsMainWindowActive = true;
			ActionManager.Instance.ReloadMessages();	//To force 'read' receipts to be sent.
		}

		//So we can determine if Message thread should be marked 'read' if main window is in background
		override protected void OnDeactivated(EventArgs e)
		{
 			base.OnDeactivated(e);

			SessionManager.Instance.IsMainWindowActive = false;
		}

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            if (doSignOut == false)
            {
                e.Cancel = true;
                WindowState = System.Windows.WindowState.Minimized;
//              //We do NOT hide the window because UX team wants Minimize and Close to behave the same way.
            }
            else
            {
                base.OnClosing(e);

                var viewModel = DataContext as MainWindowViewModel;
                if (viewModel != null)
                {
                    viewModel.Cleanup();
                }

                isCleaned = true;
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            if (doSignOut == true)
            {
                messenger.Send<SignoutMessage>(new SignoutMessage { MainWindowClosed = true });
            }
        }

        #endregion

        #region | Private Procedures |

        /// <summary>
        /// Set up UI related actions & messages 
        /// </summary>
        private void SetupUI()
        {
            messenger.Register<MainTabNavigateMessage>		 ( this, OnNavigateTo);
            messenger.Register<SignoutMessage>				 ( this, OnSignout);
			messenger.Register<UpdateUnreadItemCountsMessage>( this, OnUpdateUnreadItemCounts );
            messenger.Register<ProfileCloseMessage>			 ( this, (ProfileCloseMessage msg) => { Profile.Visibility = Visibility.Hidden; });
            messenger.Register<NotImplementedMessage>		 ( this, (NotImplementedMessage msg) => { NotImplementedMessage(); });
			messenger.Register<RebuildMenuBarMessage>		 ( this, OnRebuildMenuBar );
        }

		private void OnRebuildMenuBar( RebuildMenuBarMessage msg )
		{
			Desktop.Model.MenuManager.Instance.buildMenuBar( MainMenu, true );
		}

        /// <summary>
        /// Navigate app to selected Tab
        /// </summary>
        /// <param name="msg">Message containing the Tab to navigate to / show</param>
        private void OnNavigateTo( MainTabNavigateMessage msg )
        {
			//Any time Profile is active and we navigate to a different tab, close the Profile
            Profile.Visibility = ( msg.NavigateTo == Navigation.Profile && Profile.Visibility == Visibility.Hidden ) ? Visibility.Visible : Visibility.Hidden;

            if (msg.NavigateTo != Navigation.Profile && Profile.Visibility == Visibility.Hidden)
            {
				ActionManager.Instance.CloseProfile();
            }
        }

        private void OnSignout(SignoutMessage msg)
        {
            if ((msg.MainWindowClosed == false || msg.Quit == true) && isCleaned == false)
            {
                doSignOut = true;
                Close();
            }
        }

        private void NotImplementedMessage()
        {
            MessageBox.Show(LocalizedString.NotImplemented);
        }

		private void OnUpdateUnreadItemCounts( UpdateUnreadItemCountsMessage msg )
		{
			MainNavTab.MessageTab.BadgeCount = msg.Messages;						//XMPP and SMS
			MainNavTab.CallTab.BadgeCount    = msg.MissedCalls + msg.Voicemails;	//TODO: Is this correct per Product Team?
			MainNavTab.FaxTab.BadgeCount	 = msg.Faxes;
//			MainNavTab.VoicemailTab.BadgeCount = msg.Voicemails;	//TODO: Same tab as MissedCalls.
			//NOTE: We have RecordedCalls, but no UI element to apply them to.
		}

        #endregion


		#region Window State Events

		private void Window_StateChanged(object sender, EventArgs e)
        {
            if (WindowState == System.Windows.WindowState.Minimized)
            {
                MixPanel.StopSession();
                messenger.Send<AppStateChangeMessage>(new AppStateChangeMessage { AppState = "MinimizedState" });
            }
            else
            {
                MixPanel.StartSession();	//TODO: If user clicks Maximize, we also get here, which may result in back-to-back StartSession() calls.
            }

            SettingsManager.Instance.User.WindowState = (int)this.WindowState;
            if (WindowState == System.Windows.WindowState.Maximized || WindowState == System.Windows.WindowState.Minimized)
            {
				SettingsManager.Instance.User.saveWindowPos ( this.RestoreBounds.Top,   this.RestoreBounds.Left   );
                SettingsManager.Instance.User.saveWindowSize( this.RestoreBounds.Width, this.RestoreBounds.Height );
               
            }
           
            if(this.WindowState == System.Windows.WindowState.Normal || this.WindowState == System.Windows.WindowState.Maximized)
            {
                messenger.Send<AppStateChangeMessage>(new AppStateChangeMessage { AppState = "NormalState" });
            }
        }

        private void Window_LocationChanged(object sender, EventArgs e)
        {
            var offset = PhoneNumbersPopup.HorizontalOffset;
            PhoneNumbersPopup.HorizontalOffset = offset + 1;
            PhoneNumbersPopup.HorizontalOffset = offset;

			//Let's move the Dialpad in concert with main window.
			//	See NOTE at calcDialPadPopupPlacement()
			calcDialPadPopupPlacement();

            if (isWindowLoaded)
            {
                SettingsManager.Instance.User.saveWindowPos( this.Top, this.Left );
            }
        }

        private void Window_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
			//All this does is to detect a click in main window, but outside the dialer.
			//	Seems overly complicated.  Should be able to use some sort of 'HitTest' on MainWindow and DialpadPopup.
            Point p = e.GetPosition(this);
            double dialerY = MainNavTab.MessageTab.Height + MainNavTab.CallTab.Height + MainNavTab.ContactTab.Height + (Desktop.Model.SessionManager.Instance.User.Options.IsFaxAvailable == false ? 0.0 : MainNavTab.FaxTab.Height) + 22;

            if (((p.X > 0 && p.X < MainNavTab.KeypadTab.ActualWidth) && (p.Y > dialerY && p.Y < dialerY + MainNavTab.KeypadTab.ActualHeight)) == false)
            {
                if (DialPadPopup.IsOpen == true && ((p.X > 55 && p.X < MainNavTab.ActualWidth + DialPadPopup.Width) && (p.Y > 30 && p.Y < 30 + DialPadPopup.Child.DesiredSize.Height)) == false)
				{
					ActionManager.Instance.CloseDialer();
					Messenger.Default.Send<KeypadRegionClickMessage>(new KeypadRegionClickMessage());
				}
            }
		}

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.Left		 = SettingsManager.Instance.User.WindowLeft;
            this.Top		 = SettingsManager.Instance.User.WindowTop;
            this.Height		 = SettingsManager.Instance.User.WindowHeight;
            this.Width		 = SettingsManager.Instance.User.WindowWidth;

            this.WindowState = (WindowState)SettingsManager.Instance.User.WindowState;

			calcDialPadPopupPlacement();

            isWindowLoaded = true;
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (isWindowLoaded)
            {
                SettingsManager.Instance.User.saveWindowSize( this.Width, this.Height );
            }
        }

		//JRT - 2015.11.18
		//	On Microsoft Surface tablet, with Windows 10 , using .NET 4.6, the DialPad is not placed properly.
		//	The issue appears to be the 'handed-ness' setting in Settings -> Devices -> Pen.
		//	It defaults to Right which makes this 'Relative' placement popup display to LEFT of
		//	designated control.  Changing Pen -> Left fixes the issue, but we cannot expect
		//	users to change this setting, nor should we.
		//
		//	So, I will attempt to calcuate the absolute position to display Dialpad
		private void calcDialPadPopupPlacement()
		{
			//First hard-coded digits are from .xaml file
			//Second hard-coded digits are adjusments.
			//	We *should* calculate based on window adornments and NavBar geometry, but for some reason that does not work consistently.
			double top  = this.Top  +  8 + 62;
			double left = this.Left + 76 +  8;

			DialPadPopup.Placement		  = System.Windows.Controls.Primitives.PlacementMode.Absolute;
			DialPadPopup.VerticalOffset   = top;
			DialPadPopup.HorizontalOffset = left;
		}
		#endregion
	}
}
