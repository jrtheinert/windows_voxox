﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using System.Windows.Controls;	//UserControl

namespace Desktop.Views.Main
{
    /// <summary>
    /// Interaction logic for ConfirmBlockContact.xaml
    /// </summary>
    public partial class ConfirmBlockContact : UserControl
    {
        public ConfirmBlockContact()
        {
            InitializeComponent();
        }
    }
}
