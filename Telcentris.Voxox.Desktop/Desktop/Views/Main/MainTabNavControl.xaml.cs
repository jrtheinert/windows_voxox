﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using System.Windows.Controls;	//UserControl

namespace Desktop.Views.Main
{
    /// <summary>
    /// Interaction logic for MainTabNavControl.xaml
    /// </summary>
    public partial class MainTabNavControl : UserControl
    {
        #region | Constructors |

        public MainTabNavControl()
        {
            InitializeComponent();
        }

        #endregion

        #region | Public Properties |

        public double DialpadPointerVerticalOffset
        {
            get 
            {
                double dialerVertOffset = MessageTab.Height + CallTab.Height + ContactTab.Height + (KeypadTab.Height / 2.0) + (Desktop.Model.SessionManager.Instance.User.Options.IsFaxAvailable == false ? 0.0 : FaxTab.Height);

                dialerVertOffset -= 8;

                return dialerVertOffset;
            }
        }

        #endregion
    }
}
