﻿
using Vxapi23.Model;				//ContactSourcesResponse;

using Desktop.Model;				//ActionManager
using Desktop.ViewModels.Settings;	//ContactSourceSelectionViewModel

using System.Windows;				//Window, RoutedEventArgs
using System.Windows.Input;			//MouseButtonEventArgs

namespace Desktop.Views.Contacts
{
	/// <summary>
	/// Interaction logic for ContactSourceSelection.xaml
	/// </summary>
	public partial class ContactSourceSelection : Window
	{
		private bool mSaved = false;

		public ContactSourceSelection( ContactSourcesResponse response, string defaultSourceKey )
		{
			InitializeComponent();
            DataContext = new ContactSourceSelectionViewModel( response, defaultSourceKey );
		}

		public void OnDoubleClick( object sender, MouseButtonEventArgs e )
		{
			Exit( false );
		}

		protected override void OnClosed( System.EventArgs e )
		{
			base.OnClosed( e );

			if ( ! mSaved )
				ExitWithDefault();
		}

		public void CancelButton_Click( object sender, RoutedEventArgs e )
		{
			ExitWithDefault();
		}

        private void OKButton_Click( object sender, RoutedEventArgs e )
        {
			Exit( false );
        }

		private void ExitWithDefault()
		{
			Exit( true );
		}

		private void Exit( bool useDefault )
		{
			ContactSourceSelectionViewModel vm = (ContactSourceSelectionViewModel)DataContext;

			if ( vm != null )
			{
				bool result = vm.Save( useDefault );

				mSaved = true;

				if ( result )
				{
					Close();
				}
			}
			else 
			{
				Close();
			}
		}
	}

}
