﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Aug 2015
 */

//using Desktop.ViewModels;			//StartAnimationMessage, AnimationDoneMessage
using Desktop.Controls;				//UserControlWithAnimation

//using GalaSoft.MvvmLight.Messaging;	//Messenger
//using GalaSoft.MvvmLight.Threading;	//DispatcherHelper

//using System.Windows;				//RoutedEventArgs
//using System.Windows.Controls;		//UserControl

namespace Desktop.Views.Contacts
{
	/// <summary>
	/// Interaction logic for ContactInfoControl.xaml
	/// </summary>
	public partial class ContactInfoControl : UserControlWithAnimation	//UserControl
	{
		public ContactInfoControl()
		{
			InitializeComponent();

			//We CANNOT include this in base class UserControlWithAnimation, because it throws an exception doing FindResource.
			//	So we add it here.
			base.initializeClosingAnimation();
		}
	}
}
