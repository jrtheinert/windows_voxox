﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;				//MenuManager

using System.Windows;				//RoutedEventArgs
using System.Windows.Controls;		//UserControl
using System.Windows.Input;			//Key, KeyEventArgs, MouseEventArgs

namespace Desktop.Views.Contacts
{
    /// <summary>
    /// Interaction logic for ContactListControl.xaml
    /// 
    /// The Search Mode UI interaction are handled through Event Handlers.
    /// TODO: Does this break MVVM pattern? Because having Commands/Properties to show/hide overlays, textboxes in ViewModel might make VM => View hard-coupling.
    /// </summary>
    public partial class ContactListControl : UserControl
    {
        #region | Private Class Variables |

        bool isInSearchMode = false;

        #endregion

        #region | Constructors |

        public ContactListControl()
        {
            InitializeComponent();
        }

        #endregion

        #region | Private Event Handler Procedures |

        private void OnControlLoaded(object sender, RoutedEventArgs e)
        {
            if(isInSearchMode)
            {
                SearchText.Focus();
                if (string.IsNullOrWhiteSpace(SearchText.Text) == false)
                {
                    SearchText.Select(SearchText.Text.Length, 0);
                }
                else
                {
                    SearchText.SelectAll();
                }
            }
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            isInSearchMode = true;
            UpdateVisibilities();

            SearchText.Focus();
        }

        private void SearchText_LostFocus(object sender, RoutedEventArgs e)
        {
            CheckSearchMode();
        }

        private void SearchText_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                CheckSearchMode();
            }
        }

        private void SearchText_TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdateVisibilities();
        }

        private void CloseMessagesButton_Click(object sender, RoutedEventArgs e)
        {
            SearchText.Text = "";
            CheckSearchMode();
        }

        #endregion

        #region | Private Procedures |

        /// <summary>
        /// Checks if the search mode is valid & Updates UI elements Visiblity accordingly
        /// </summary>
        private void CheckSearchMode()
        {
            if (string.IsNullOrWhiteSpace(SearchText.Text))
            {
                isInSearchMode = false;
            }

            UpdateVisibilities();
        }

        /// <summary>
        /// Update UI elements visiblity based on mode the control is in
        /// </summary>
        private void UpdateVisibilities()
        {
            if (isInSearchMode)
            {
                // Show Search Textbox, hiding the caption
                TitleWrap.Visibility = System.Windows.Visibility.Collapsed;
                SearchWrap.Visibility = System.Windows.Visibility.Visible;

                // Display overlay if no text has been entered yet
                if (string.IsNullOrWhiteSpace(SearchText.Text))
                {
                    SearchOverlay.Visibility = System.Windows.Visibility.Visible;
                }
                else
                {
                    SearchOverlay.Visibility = System.Windows.Visibility.Collapsed;
                }
            }
            else
            {
                // Show the caption & search button, hiding the search filter
                TitleWrap.Visibility = System.Windows.Visibility.Visible;
                SearchWrap.Visibility = System.Windows.Visibility.Collapsed;
                SearchOverlay.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        private void OnDCChange(object sender, DependencyPropertyChangedEventArgs e)
        {
            var data = e.NewValue as Desktop.ViewModels.Contacts.ContactListViewModel;

            if (data != null && !string.IsNullOrWhiteSpace(data.SearchContacts))
            {
                isInSearchMode = true;
                TitleWrap.Visibility = System.Windows.Visibility.Collapsed;
                SearchWrap.Visibility = System.Windows.Visibility.Visible;
                SearchOverlay.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

		private void ItemWrap_RightClick( object sender, MouseEventArgs e )
		{
			Border border = (Border)sender;

			if (border != null)
			{
				object temp = border.Parent;

				Desktop.ViewModels.Contacts.ContactViewModel vm = (Desktop.ViewModels.Contacts.ContactViewModel) border.DataContext;

				if ( vm != null )
				{ 
					MenuManager.Instance.showContactContextMenu( vm.ContactKey, vm.PhoneNumber, vm.CmGroup );
				}
			}
		}

        #endregion

    }
}
