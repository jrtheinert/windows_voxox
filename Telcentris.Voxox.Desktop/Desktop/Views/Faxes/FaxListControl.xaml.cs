﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using System.Windows;			//RoutedEventArgs, DependencyPropertyChangedEventArgs
using System.Windows.Controls;	//UserControl
using System.Windows.Input;		//Key, KeyEventArgs

namespace Desktop.Views.Faxes
{
    /// <summary>
    /// Interaction logic for FaxListControl.xaml
    /// </summary>
    public partial class FaxListControl : UserControl
    {
        #region | Private Class Variables |

        bool isInSearchMode = false;

        #endregion

        public FaxListControl()
        {
            InitializeComponent();
        }

        #region | Private Event Handler Procedures |

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            isInSearchMode = true;
            UpdateVisibilities();

            SearchText.Focus();
        }

        private void SearchText_LostFocus(object sender, RoutedEventArgs e)
        {
            CheckSearchMode();
        }

        private void SearchText_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                CheckSearchMode();
            }
        }

        private void SearchText_TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdateVisibilities();
        }

        #endregion

        #region | Private Procedures |

        /// <summary>
        /// Checks if the search mode is valid & Updates UI elements Visiblity accordingly
        /// </summary>
        private void CheckSearchMode()
        {
            if (string.IsNullOrWhiteSpace(SearchText.Text))
            {
                isInSearchMode = false;
            }

            UpdateVisibilities();
        }

        /// <summary>
        /// Update UI elements visiblity based on mode the control is in
        /// </summary>
        private void UpdateVisibilities()
        {
            if (isInSearchMode)
            {
                // Show Search Textbox, hiding the caption
                TitleWrap.Visibility = System.Windows.Visibility.Collapsed;
                SearchWrap.Visibility = System.Windows.Visibility.Visible;

                // Display overlay if no text has been entered yet
                if (string.IsNullOrWhiteSpace(SearchText.Text))
                {
                    SearchOverlay.Visibility = System.Windows.Visibility.Visible;
                }
                else
                {
                    SearchOverlay.Visibility = System.Windows.Visibility.Collapsed;
                }
            }
            else
            {
                // Show the caption & search button, hiding the search filter
                TitleWrap.Visibility = System.Windows.Visibility.Visible;
                SearchWrap.Visibility = System.Windows.Visibility.Collapsed;
                SearchOverlay.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        private void OnDCChange(object sender, DependencyPropertyChangedEventArgs e)
        {
            var data = e.NewValue as Desktop.ViewModels.Faxes.FaxListViewModel;

            if (data != null && !string.IsNullOrWhiteSpace(data.SearchFaxes))
            {
                isInSearchMode = true;
                TitleWrap.Visibility = System.Windows.Visibility.Collapsed;
                SearchWrap.Visibility = System.Windows.Visibility.Visible;
                SearchOverlay.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        #endregion
    }
}
