﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using System;							//Action, Boolean, String, Char
using System.Windows;					//DependencyPropertyChangedEventHandler, DependencyPropertyChangedEventArgs
using System.Windows.Controls;			//UserControl
using System.Windows.Threading;			//DispatcherPriority

namespace Desktop.Views.Dialer
{

    /// <summary>
    /// Interaction logic for DialPadControl.xaml
    /// </summary>
    public partial class DialPadControl : UserControl
    {
        public DialPadControl()
        {
            InitializeComponent();

            this.IsVisibleChanged += new DependencyPropertyChangedEventHandler(DailPadControl_IsVisibleChanged);

			//Set up textbox for dialpad.
			PhoneNumberTextBox.AssociatedTextBlock = this.txtAlphaPhoneNumber;
        }

        void DailPadControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue == true)
            {
				Action action = new Action( delegate()
                {
                    PhoneNumberTextBox.Focus();
                    PhoneNumberTextBox.CaretIndex = int.MaxValue;
                });

                Dispatcher.BeginInvoke( DispatcherPriority.ContextIdle, action );
				PhoneNumberTextBox.IsAlphaPhoneNumber = false;
            }
        }
    }
}
