﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;				//ActionManager

using GalaSoft.MvvmLight.Messaging;	//Messenger

using System;						//EventArgs
using System.Windows;				//Window
using System.Windows.Input;			//Key, Keyboard, KeyStates, KeyEventArgs (For CapsLock detection)
using System.Windows.Navigation;	//RequestNavigateEventArgs

namespace Desktop.Views.Login
{
    /// <summary>
    /// Interaction logic for LoginControl.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
     
			Messenger.Default.Register<RebuildMenuBarMessage>( this, OnRebuildMenuBar );
        }

        #region | Protected Overrides |

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            Hide();
        }

		//This is needed to address limitations in Menu/MenuItem being in more than one menu.
		override protected void OnActivated(EventArgs e)
		{
 			base.OnActivated(e);

			Desktop.Model.MenuManager.Instance.buildMenuBar( MainMenu, false );
		}

        #endregion

		private void OnRebuildMenuBar( RebuildMenuBarMessage msg )
		{
			Desktop.Model.MenuManager.Instance.buildMenuBar( MainMenu, true );
		}

        /// <summary>
        /// Logic to handle hyperlink for LoginControl.xaml file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
			ActionManager.Instance.HandleUrl( e.Uri.AbsoluteUri );
            e.Handled = true;
        }

		#region CapLock warning methods

		private void Password_KeyDown(object sender, KeyEventArgs e)
        {
            CapsLockWarnPopup.IsOpen = ( Keyboard.GetKeyStates(Key.CapsLock) == KeyStates.Toggled );
        }

        //When window location changes we need to change popup location as well
        private void Window_LocationChanged(object sender, EventArgs e)
        {
            var offset = CapsLockWarnPopup.HorizontalOffset;
            CapsLockWarnPopup.HorizontalOffset = offset + 1;
            CapsLockWarnPopup.HorizontalOffset = offset;
        }

        // When window deactivated, explicitly we need to close popup
        private void Window_Deactivated(object sender, EventArgs e)
        {
            CapsLockWarnPopup.IsOpen = false;
		}

		#endregion
	}
}
