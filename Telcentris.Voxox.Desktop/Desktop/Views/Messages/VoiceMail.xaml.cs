﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using System.Windows;			//DependencyPropertyChangedEventArgs
using System.Windows.Controls;	//UserControl

namespace Desktop.Views.Messages
{
    /// <summary>
    /// Interaction logic for VoiceMail.xaml
    /// </summary>
    public partial class VoiceMail : UserControl
    {
        public VoiceMail()
        {
            InitializeComponent();
        }

        private void AudioPlayer_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            AudioPlayer.ResetPlayer();
        }
    }
}
