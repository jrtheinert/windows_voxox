﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.ViewModels.Messages;	//MessagesModifiedMessage, ChooseFileMessage, SendFaxMessage, SendFileMessage, MessageDirection, MessageBodyType

using GalaSoft.MvvmLight.Messaging;	//Messenger
using GalaSoft.MvvmLight.Threading;	//DispatcherHelper

using System;					//DateTime, Type, Nullable, NotImplementedException (Mostly for Converter)
using System.Globalization;		//CultureInfo (For converter)
using System.Windows;			//Visibility, RoutedEventArgs, Application, Thickness
using System.Windows.Controls;	//UserControl
using System.Windows.Data;		//IValueConverter

namespace Desktop.Views.Messages
{
    /// <summary>
    /// Interaction logic for MessagesControl.xaml
    /// </summary>
    public partial class MessagesControl : UserControl
    {
        #region | Constructor |

        public MessagesControl()
        {
            InitializeComponent();

            // Defaults
            MessageDrawer.Visibility = Visibility.Collapsed;
            DrawerUp.Visibility = Visibility.Visible;
            DrawerDown.Visibility = Visibility.Collapsed;

            Messenger.Default.Register<MessagesModifiedMessage>(this, OnMessagesModified);
            Messenger.Default.Register<ChooseFileMessage>(this, OnSendFile);
            Messenger.Default.Register<SendFaxMessage>(this, OnSendFax);

            Messenger.Default.Register<HideDrawerMessage>(this, (HideDrawerMessage msg) =>
            {
                MessageDrawer.Visibility = Visibility.Collapsed;
                DrawerUp.Visibility = Visibility.Visible;
                DrawerDown.Visibility = Visibility.Collapsed;
            });

//			this.ScrollChanged += new ScrollChangedEventHandler( OnScrollChanged );
        }

        private void MessagesControl_Loaded(object sender, RoutedEventArgs e)
        {
            SVMessages.ScrollToBottom();
        }

		//NOTE 2014.12.29: This code was developed to detect when an UNREAD message was visibile to user
		//	and user that to trigger code to mark the message as READ.  It was WIP.
		//Then Product Team changed criteria to "when message thread is opened, mark all UNREAD messages as READ'.
		//	Commenting this code for now, in case they change the spec again.
		//	To make this work, add [ ScrollChanged="OnScrollChanged" ] around line 160 in MessagesControl.xaml.

//		void OnScrollChanged(object sender, ScrollChangedEventArgs e)
//		{
//			if ( e.VerticalChange == 0.0 )
//				return;

//			int count = MsgListControl.Items.Count;
//			int visibleCount = 0;
//			int invisibleCount = 0;
//			int updateCount    = 0;
//			int itemNumber     = 0;

//			var scrollViewer =(FrameworkElement)sender;

//			Point pt0 = new Point(0,0);
//			Rect ownerRectangle = new Rect( pt0, scrollViewer.RenderSize );

//			foreach (Desktop.ViewModels.Messages.MessageThreadItem msgThreadItem in MsgListControl.Items )
//			{
//				var itemContainer = (FrameworkElement)MsgListControl.ItemContainerGenerator.ContainerFromItem( msgThreadItem );

//				//Is it visible?
//				var childTransform = itemContainer.TransformToAncestor( scrollViewer );
//				var childRectangle = childTransform.TransformBounds( new Rect( pt0, itemContainer.RenderSize));

//				bool isVisible = ownerRectangle.IntersectsWith( childRectangle );	//Any part intersects parent (may be partially visible)
////				isVisible = ownerRectangle.Contains( childRectangle );				//Completely within parent (will be totally visible)

//				if ( msgThreadItem.MessageDirection == MessageDirection.Incoming )
//				{ 
//					if ( msgThreadItem.MessageStatus != MessageStatus.Read )
//					{
//						updateCount++;
//					}
//				}

//				if ( isVisible )
//				{
//					visibleCount++;
//				}
//				else
//				{ 
//					invisibleCount++;
//				}

//				itemNumber++;

//			}
//		}

        #endregion

        #region | Private Event Handlers |

        private void DrawerUp_Click(object sender, RoutedEventArgs e)
        {
            ToggleDrawer();
        }

        private void DrawerDown_Click(object sender, RoutedEventArgs e)
        {
            ToggleDrawer();
        }

        #endregion

        #region | Private Procedures |

        private void ToggleDrawer()
        {
            if (MessageDrawer.Visibility == Visibility.Visible)
            {
                MessageDrawer.Visibility = Visibility.Collapsed;
                DrawerUp.Visibility = Visibility.Visible;
                DrawerDown.Visibility = Visibility.Collapsed;
            }
            else
            {
                MessageDrawer.Visibility = Visibility.Visible;
                DrawerUp.Visibility = Visibility.Collapsed;
                DrawerDown.Visibility = Visibility.Visible;
            }
        }

        private void OnMessagesModified(MessagesModifiedMessage msg)
        {
            DispatcherHelper.CheckBeginInvokeOnUI(() =>
            {
                SVMessages.ScrollToBottom();
            });            
        }

        private void OnSendFile(ChooseFileMessage msg)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            dlg.CheckFileExists = true;
            dlg.CheckPathExists = true;
            dlg.Multiselect = false;
            dlg.ShowReadOnly = false;
            dlg.Title = "Choose Picture or Video..."; // TODO: Localization
            dlg.Filter = "Image Files|*.jpg;*.jpeg|Video Files|*.mpg;*.mpeg";

            Nullable<bool> result = dlg.ShowDialog();

            if (result == true)
            {
                Messenger.Default.Send<SendFileMessage>(new SendFileMessage { SendFile = true, File = dlg.FileName });
            }
        }

        private void OnSendFax(SendFaxMessage msg)
        {
            if (msg.ChooseFile == true)
            {
                Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

                dlg.CheckFileExists = true;
                dlg.CheckPathExists = true;
                dlg.Multiselect = false;
                dlg.ShowReadOnly = false;
                dlg.Title = "Choose PDF File to Fax...";
                dlg.Filter = "PDF Files|*.pdf";

                Nullable<bool> result = dlg.ShowDialog();

                if (result == true)
                {
                    Messenger.Default.Send<SendFaxMessage>(new SendFaxMessage { SendFile = true, File = dlg.FileName });
                }
            }
        }

        #endregion

    }

    /// <summary>
    /// Converter class used to show time in messages listing
    /// </summary>
    public class MessageTimeValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var v = value as DateTime?;
            if (v == null)
            {
                return value;
            }

            return Convert(v.Value);
        }

        public static string Convert(DateTime v)
        {
            return v.ToString("hh:mmtt").ToLower();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// Converter class used to show date only in messages listing
    /// </summary>
    public class MessageDateValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var v = value as DateTime?;
            if (v == null)
            {
                return value;
            }

            return Convert(v.Value);
        }

        public static string Convert(DateTime v)
        {
            return v.ToString("MMMM d");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class MessageBodyTypeToBubblePaddingConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var convertedValue = (MessageBodyType)value;

            // TODO: Read from Settings
            switch (convertedValue)
            {
                case MessageBodyType.Picture:
                    return new Thickness(0);
                default:
                    return new Thickness(16, 8, 16, 8);
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
