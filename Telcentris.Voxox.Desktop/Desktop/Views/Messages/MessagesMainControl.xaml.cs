﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using System.Windows.Controls;	//UserControl

namespace Desktop.Views.Messages
{
    /// <summary>
    /// Interaction logic for MessagesMainControl.xaml
    /// </summary>
    public partial class MessagesMainControl : UserControl
    {
        public MessagesMainControl()
        {
            InitializeComponent();
        }
    }
}
