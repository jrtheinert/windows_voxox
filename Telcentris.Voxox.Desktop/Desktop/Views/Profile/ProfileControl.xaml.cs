﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using System;
using System.Windows;			//RoutedEventArgs
using System.Windows.Controls;	//UserControl, TextBox
using System.Windows.Input;		//MouseButtonEventArgs

namespace Desktop.Views.Profile
{
    /// <summary>
    /// Interaction logic for ProfileControl.xaml
    /// </summary>
    public partial class ProfileControl : UserControl
    {
        public ProfileControl()
        {
            InitializeComponent();
        }

		//Move cursor to end of text 
        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            tb.CaretIndex = int.MaxValue;
        }

		//Select all text on double-click.  Can we not make this a standard behavior?  Seems like it should be. TODO:
        private void TextBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            tb.SelectAll();
        }
    }
}
