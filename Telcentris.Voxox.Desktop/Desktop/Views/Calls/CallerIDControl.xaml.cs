﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.ViewModels.Calls;		//CallerIDListViewModel

using System.Windows.Controls;		//UserControl

namespace Desktop.Views.Calls
{
    /// <summary>
    /// Interaction logic for CallerIDControl.xaml
    /// </summary>
    public partial class CallerIDControl : UserControl
    {
        public CallerIDControl()
        {
            InitializeComponent();
            this.DataContext = new CallerIDListViewModel();
        }
    }
}
