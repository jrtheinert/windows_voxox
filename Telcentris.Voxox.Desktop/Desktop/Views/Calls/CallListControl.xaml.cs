﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using System.Windows;						//RoutedEventArgs, DependencyPropertyChangedEventArgs
using System.Windows.Controls;				//UserControl, Border, TextChangedEventArgs
using System.Windows.Controls.Primitives;	//Popup
using System.Windows.Input;					//Key, KeyEventArgs, MouseEventArgs

namespace Desktop.Views.Calls
{
    /// <summary>
    /// Interaction logic for CallListControl.xaml
    /// 
    /// The Search Mode UI interaction are handled through Event Handlers.
    /// TODO: Does this break MVVM pattern? Because having Commands/Properties to show/hide overlays, textboxes in ViewModel might make VM => View hard-coupling.
    /// </summary>
    public partial class CallListControl : UserControl
    {
        #region | Private Class Variables |

        bool isInSearchMode = false;

        #endregion

        #region | Constructors |

        public CallListControl()
        {
            InitializeComponent();
        }

        #endregion

        #region | Private Event Handler Procedures |

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            isInSearchMode = true;
            UpdateVisibilities();

            SearchText.Focus();
        }

        private void SearchText_LostFocus(object sender, RoutedEventArgs e)
        {
            CheckSearchMode();
        }

        private void SearchText_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                CheckSearchMode();
            }
        }

        private void SearchText_TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdateVisibilities();
        }

        private void CloseMessagesButton_Click(object sender, RoutedEventArgs e)
        {
            SearchText.Text = "";
            CheckSearchMode();
        }

        #endregion

        #region | Private Procedures |

        /// <summary>
        /// Checks if the search mode is valid & Updates UI elements Visiblity accordingly
        /// </summary>
        private void CheckSearchMode()
        {
            if (string.IsNullOrWhiteSpace(SearchText.Text))
            {
                isInSearchMode = false;
            }

            UpdateVisibilities();
        }

        /// <summary>
        /// Update UI elements visiblity based on mode the control is in
        /// </summary>
        private void UpdateVisibilities()
        {
            if (isInSearchMode)
            {
                // Show Search Textbox, hiding the caption
                TitleWrap.Visibility = System.Windows.Visibility.Collapsed;
                SearchWrap.Visibility = System.Windows.Visibility.Visible;

                // Display overlay if no text has been entered yet
                if (string.IsNullOrWhiteSpace(SearchText.Text))
                {
                    SearchOverlay.Visibility = System.Windows.Visibility.Visible;
                }
                else
                {
                    SearchOverlay.Visibility = System.Windows.Visibility.Collapsed;
                }
            }
            else
            {
                // Show the caption & search button, hiding the search filter
                TitleWrap.Visibility = System.Windows.Visibility.Visible;
                SearchWrap.Visibility = System.Windows.Visibility.Collapsed;
                SearchOverlay.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        private void OnDCChange(object sender, DependencyPropertyChangedEventArgs e)
        {
            var data = e.NewValue as Desktop.ViewModels.Calls.CallListViewModel;

            if (data != null && !string.IsNullOrWhiteSpace(data.SearchCalls))
            {
                isInSearchMode = true;
                TitleWrap.Visibility = System.Windows.Visibility.Collapsed;
                SearchWrap.Visibility = System.Windows.Visibility.Visible;
                SearchOverlay.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        #endregion

		private void ItemWrap_MouseEnter(object sender, MouseEventArgs e)
		{
			handleMouseChange( sender, true );
		}

		private void ItemWrap_MouseLeave(object sender, MouseEventArgs e)
		{
			handleMouseChange( sender, false );
		}

		private void handleMouseChange( object sender, bool isOver )
		{
			Border border = (Border)sender;

			if (border != null)
			{
				object temp = border.Parent;

				Desktop.ViewModels.Calls.CallItemViewModel vm = (Desktop.ViewModels.Calls.CallItemViewModel) border.DataContext;

				if ( vm != null )
				{ 
					vm.updateIsMouseOver( isOver );
				}
			}
		}

		private void ItemWrap_RightClick( object sender, MouseEventArgs e )
		{
			Border border = (Border)sender;

			if (border != null)
			{
				object temp = border.Parent;

				Desktop.ViewModels.Calls.CallItemViewModel vm = (Desktop.ViewModels.Calls.CallItemViewModel) border.DataContext;

				if ( vm != null )
				{ 
					vm.displayContextMenu();
				}
			}
		}

    }
}
