﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.ViewModels.Calls;		//CallDetailViewModel

using System.Windows;				//DependencyPropertyChangedEventArgs
using System.Windows.Controls;		//UserControl

namespace Desktop.Views.Calls
{
    /// <summary>
    /// Interaction logic for CallsControl.xaml
    /// </summary>
    public partial class CallDetailControl : UserControl
    {
        public CallDetailControl()
        {
            InitializeComponent();
        }

        private void VMRCPlayer_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            VMRCPlayer.ResetPlayer();

            var vm = e.NewValue as CallDetailViewModel;
            if(vm != null)
            {
                VMRCPlayer.MediaDuration = vm.CallHistoryItemVM.Duration;
            }
        }

        private void WrapPanel_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if(TranscriptionText.IsVisible)
            {
                if (e.NewSize.Width < PlayerControlGrid.Width + TranscriptionText.MinWidth)
                {
                    TranscriptionText.Width = double.NaN;
                }
                else
                {
                    TranscriptionText.Width = e.NewSize.Width - PlayerControlGrid.Width;
                }
            }
        }
    }
}
