﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;				//ActionManager

using System.Windows;				//Window, RoutedEventArgs

namespace Desktop.Views.Calls
{
    /// <summary>
    /// Interaction logic for OutofCredits.xaml
    /// </summary>
    public partial class OutofCredits : Window
    {
        public OutofCredits()
        {
            InitializeComponent();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Credits_Click(object sender, RoutedEventArgs e)
        {
			ActionManager.Instance.ShowBuyCreditsUrl();

			this.Close();
        }
    }
}
