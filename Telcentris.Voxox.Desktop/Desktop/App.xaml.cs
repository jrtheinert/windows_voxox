﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;
using Desktop.Model.Calling;			//Sound
using Desktop.Util;
using Desktop.Utils;					//VXLogger
using Desktop.ViewModels.Login;
using Desktop.ViewModels.Main;
using Desktop.Views.Login;
using Desktop.Views.Main;

using GalaSoft.MvvmLight.Messaging;		//Messenger
using GalaSoft.MvvmLight.Threading;		//DispatcherHelper

using System;
using System.Runtime.InteropServices;	//DllImport
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interop;			//HwndSource...
using System.Windows.Input;				//KeyEventHandler, KeyEventArgs for Debug Menu handler


namespace Desktop
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
		VXLogger	logger = null;		//Initialize AFTER logger is initialized.
        LoginWindow loginWindow;
        MainWindow	mainWindow;
		bool		initializing = false;

        protected override void OnStartup(StartupEventArgs e)
        {
            // Do internal bootstrapping
            base.OnStartup(e);

			//Allow more than two simultaneous WebClient connections
			System.Net.ServicePointManager.DefaultConnectionLimit = 100;	//10 will likely do...

            // Start App Initialization
            NativeMethods.openssl_init(false);	//No compression.

			DoInitialization( false );		//We create a method so we can re-brand via Advanced Settings.

            Messenger.Default.Register<BrandingChangedMessage>(this, OnBrandingChanged);
            Messenger.Default.Register<LoginSuccessMessage>   (this, OnLoginSuccess);
            Messenger.Default.Register<SignoutMessage>        (this, onSignout);
            Messenger.Default.Register<ViewModels.IncomingEventMessage>(this, OnIncomingEvent);
        }

		//We create a method so we can re-brand via Advanced Settings.
		//	Some of this code may need to move back up to OnStartUp().
		private void DoInitialization( bool isReinit )
		{
			initializing = true;

			Desktop.Config.Brand.init();											//Force static classes to initialize and do branding
			LocalizedString.Rebrand();
			ManagedDataTypes.Localization.setOrText( LocalizedString.GenericOr );	//Set localized 'or' value for compound contact names 'Bob or Tom'

            // Check if single instance is running & quit if there is already an instance
            if (CheckSingleInstance() == false)
                return;

            //This inits logging.  Ideally, we would init Logging separately for maximum logging.
            //It also initializes MixPanel.  Not sure that is the proper place.
            SessionManager.Instance.Initialize();

			logger = VXLoggingManager.INSTANCE.GetLogger("App");

			String testString = "Test log support for UTF-8: Зарегистрируйтесь  გთხოვთ ახლავე ";
			logger.Debug( testString );

            // Networking checking - This is AFTER SessionManager because we catch the Network status there.
			NetworkStatus.Instance.StartWatching();				

            //We need logging initialized so we can get the Log filepath for crash reporting.
            //AppDomain.CurrentDomain.UnhandledException is set in CrashReporter, so ideally this would come BEFORE
            //	everything except log initialization.  TODO
            CrashReporter.Initialize( LocalizedString.MainWindowTitle, LocalizedString.CrashReporter_Message );		

            DispatcherHelper.Initialize();
            SystemTray.Instance.Init();
//            AvatarManager.Instance.Initialize();	//With Instance, we should not need this.

			//Register KeyEventHandler so we can show/hide debug menu.
			EventManager.RegisterClassHandler( typeof(Window), Window.PreviewKeyUpEvent, new KeyEventHandler(OnKeyDown) );

			var gotOptions = SessionManager.Instance.GetInitOptions();

			if ( gotOptions )
			{
				InitializeLoginWindow();

				AudioDeviceNotification.Register();

				initializing = false;
			}
			else 
			{ 
				//TODO: Display message Box to user.
				ActionManager.Instance.HandleInitOptionsFail();
                App.Current.Shutdown();
			}
		}
		
		private void InitializeLoginWindow()
		{
			//Handle possible re-branding via Advanced Settings.
			if ( loginWindow != null )
			{
	            loginWindow.Hide();
				loginWindow.Close();
				SystemTray.Instance.Attach(loginWindow, false);
				loginWindow = null;	
			}

            //Show login window, on successful authentication the login window is hidden and the main window is shown.
            loginWindow = new LoginWindow();
            LoginViewModel loginViewModel = new LoginViewModel();
            loginWindow.DataContext = loginViewModel;
            loginWindow.Show();

            SystemTray.Instance.Attach(loginWindow, false);

			//Check network availability AFTER LoginWindow is displayed so user has some context for the possible error message.
			if (NetworkStatus.Instance.IsAvailable == false)
			{
				NetworkStatus.Instance.ShowNetworkErrorMessage();
			}

            HwndSource source = HwndSource.FromHwnd(new WindowInteropHelper(loginWindow).Handle);

			if ( source != null )
			{ 
	            // Attach Windows message handler so that the current instance window can be brought to focus when launching another instance
				source.AddHook(new HwndSourceHook(HandleMessages));

				//Register for USB device changes - Now is handled with AudioDeviceNotification
//				UsbNotification.RegisterUsbDeviceNotification( source.Handle );
			}
		}

		private void OnBrandingChanged( BrandingChangedMessage msg )
		{
			if ( msg.isActionRebrand() )
			{ 
				if ( !initializing )
				{ 
					DoInitialization( true );
				}
			}
		}

		private void OnKeyDown( object source, KeyEventArgs e )
		{
			bool toggleDebugMenu = false;

			//NOTE: Some key combinations have defined behaviors on some laptops, like Ctrl-Alt-F4 adjusts screen.
			//	If that is encountered, find an alternate keystroke and add it to the logic below.
			//	Please do NOT remove any existing, unless you know for sure you should.

			//Check for Control-Alt-F5
			if ( e.KeyboardDevice.Modifiers == ( ModifierKeys.Control | ModifierKeys.Alt ) )
			{
				if ( e.Key == Key.F5 )
				{
					toggleDebugMenu = true;
				}
			}

			if ( toggleDebugMenu )
			{
				SessionManager.Instance.ToggleDebugMenu();
			}
		}

        private void OnLoginSuccess( LoginSuccessMessage msg )
        {
            MainWindowViewModel mwvm = new MainWindowViewModel();
            mainWindow = new MainWindow();
            mainWindow.DataContext = mwvm;

            loginWindow.Hide();

            mainWindow.Show();
            SessionManager.Instance.updateUnreadItemCounts();

            if ( ! AudioDeviceMgr.Instance.DevicesAvailable )
            {
				ActionManager.Instance.ShowNoAudioDevicePopup( true, true ); //Show, isAfterLogin
            }

            SystemTray.Instance.Attach(mainWindow, true);
        }

        private void onSignout( SignoutMessage msg)
        {
            // User wants to logout & re-login
            if (msg.MainWindowClosed == true || msg.Quit == true)
            {
                //TODO: We get here twice on QUIT.  First with QUIT, then with SignOut.  No crash, but wrong.
                CrashReporter.OnLogOut();
                
				Task.Run(() => { SessionManager.Instance.LogoutUser(); });

				//Let's wait for logout to complete
				int maxWait   = 10000;		//Typical time is about 1200 ms.
				int incWait   =   100;
				int totalWait =     0;

				while ( SessionManager.Instance.IsUserLoggedIn && totalWait < maxWait )
				{
					System.Threading.Thread.Sleep( incWait );
					totalWait += incWait;
				}

				if ( totalWait > 0 )
					logger.Info( "LogoutUser time: " + Convert.ToString( totalWait ) + " ms." );

                if (msg.Quit)
                {
					logger.Info( "Quitting App.---------------------------------------------------------------------" );
                    App.Current.Shutdown();
                }
                else
                {
                    SystemTray.Instance.Attach(loginWindow, false);
                    loginWindow.Show();
                }
            }
        }

        protected override void OnExit(ExitEventArgs e)
        {
			AudioDeviceNotification.Unregister();
//			UsbNotification.UnregisterUsbDeviceNotification();	//Now handled in AudioDeviceNotification.
            CrashReporter.OnQuit();
            AvatarManager.Instance.Dispose();
            SystemTray.Instance.Dispose();
            NativeMethods.openssl_terminate();
            NetworkStatus.Instance.StopWatching();
            CleanMutex();
            base.OnExit(e);
        }

        #region | Single Instance Related Procedures |

        static Mutex mutex = new Mutex(true, Desktop.Config.Branding.AppMutex + typeof(App).ToString());
        static bool ownedMutex = false;

        /// <summary>
        /// Checks for single intance of application
        /// </summary>
        /// <returns>True if the current instance is the only one running</returns>
        private bool CheckSingleInstance()
        {
            if (mutex.WaitOne(TimeSpan.Zero, true) == false)
            {
                NativeMethods.PostMessage((IntPtr)NativeMethods.HWND_BROADCAST,
                    NativeMethods.WM_SHOWME,
                    IntPtr.Zero,
                    IntPtr.Zero);

                Current.Shutdown();

                return false;
            }

            ownedMutex = true;

            return true;
        }

        private void OnIncomingEvent(ViewModels.IncomingEventMessage msg)
        {
            DispatcherHelper.CheckBeginInvokeOnUI(() => { HandleIncomingEvent(msg); });
        }

        private void HandleIncomingEvent(ViewModels.IncomingEventMessage msg)
        {
            //Bring to foreground, topmost, withOUT taking focus.
            //	We may also want the taskbar icon to flash, but why do that, if main window is brought to foreground?
            if (mainWindow != null)
            {
                //If mainwindow has focus, then do nothing.
                if (!mainWindow.IsFocused)		//This does not seem to work.
                {
                    if (msg.isIncomingCall())
                    {
                        //Bring window to front but do NOT give it focus.
                        System.Windows.WindowState newState = (mainWindow.WindowState == System.Windows.WindowState.Minimized ? System.Windows.WindowState.Normal : mainWindow.WindowState);
                        mainWindow.ShowActivated = false;

                        mainWindow.Topmost = true;
                        mainWindow.WindowState = newState;
                        mainWindow.BringIntoView();

                        mainWindow.Topmost = false;
                        //						mainWindow.WindowState   = newState;
                        //						mainWindow.BringIntoView();
                        //TODO: The above code makes the window TopMost and it remains so until window is Activated.  I am not sure why.

                        //Flash window and taskbar icon, at default rate.
                        FlashWindowHelper flashWindow = new FlashWindowHelper(mainWindow);
                        flashWindow.FlashApplicationWindow(0);		//Flash forever.  If call is not answered, it will become a missed call.
                    }
                    else if (msg.isMessage() || msg.isMissedCall())
                    {
                        //Flash window and taskbar icon, at default rate.
                        FlashWindowHelper flashWindow = new FlashWindowHelper(mainWindow);
                        flashWindow.FlashApplicationWindow(10);

                        //	IMO, we only need the 'missed message' sound, since the 'missed call' sound should be the RINGING!
                        if(msg.isMessage())
                        {
							if ( SettingsManager.Instance.User.EnableSoundForIncomingChat )
							{ 
								PlaySound();
							}
                        }
                    }
                    else
                    {
                        //TODO: log a warning? 
                    }
                }
            }
        }

        /// <summary>
        /// Cleanup Mutex resources
        /// </summary>
        private void CleanMutex()
        {
            if (ownedMutex)
            {
                mutex.ReleaseMutex();
            }

            mutex.Dispose();
        }

        private void  PlaySound()
        {
            try
            {
                var sound = new Sound( SettingsManager.Instance.User.IncomingMessageSoundFile );
                sound.play();
            }
            catch (Exception ex)
			{
                logger.Warn( "Exception in PlaySound(): ", ex );
            }
        }

        /// <summary>
        /// Windows message handler to bring current window to focus
        /// </summary>
        /// <param name="handle"></param>
        /// <param name="message"></param>
        /// <param name="wParameter"></param>
        /// <param name="lParameter"></param>
        /// <param name="handled"></param>
        /// <returns></returns>
        private IntPtr HandleMessages(IntPtr handle, Int32 message, IntPtr wParameter, IntPtr lParameter, ref Boolean handled)
        {
            if (message == NativeMethods.WM_SHOWME)
            {
                SystemTray.Instance.ShowCurrentWindow();
            }
			else if ( message == UsbNotification.WM_DEVICECHANGE )
			{
				//Device change (USB mostly, but need to check Bluetooth, also).
				//Looking for Add and Remove completions only at this time.
				bool update = false;
				bool wait   = false;
				switch( (int)wParameter )
				{
				case UsbNotification.DBT_DEVNODES_CHANGED:		//For test/debug
					update = false;
					break;
				case UsbNotification.DBT_DEVICEARRIVAL:
					update = true;
					wait   = true;
					break;

				case UsbNotification.DBT_DEVICEREMOVECOMPLETE:
					update = true;
					wait   = false;
					break;
				default:
					update = false;
					break;
				}

				if ( update )
				{
					Task.Run(() =>
					{
						//Device arrival lags about 6-12 seconds, even in the OS Sound->Playback UI.  So sleep for a bit.
						//TODO: Need better way to do this.
						if ( wait )
							System.Threading.Thread.Sleep( 15000 );

						AudioDeviceMgr.Instance.HandleDeviceChange();
					} );
				}

				return IntPtr.Zero;
			}

            return IntPtr.Zero;
        }

        private class NativeMethods
        {
			//To ensure a single instance of app is running
            public const int HWND_BROADCAST = 0xffff;
            public static readonly int WM_SHOWME = RegisterWindowMessage("WM_SHOWME");

            [DllImport("user32")]
            public static extern bool PostMessage(IntPtr hwnd, int msg, IntPtr wparam, IntPtr lparam);

            [DllImport("user32")]
            public static extern int RegisterWindowMessage(string message);

			//OpenSSL 
            [DllImport("OpenSslInitializer", CallingConvention = CallingConvention.Cdecl)]
            public static extern int openssl_init(bool compression);

            [DllImport("OpenSslInitializer", CallingConvention = CallingConvention.Cdecl)]
            public static extern int openssl_terminate();

        }	//class NativeMethods


		internal static class UsbNotification
		{
			public const int DBT_DEVNODES_CHANGED		= 0x0007;	// int =      7
			public const int DBT_DEVICEARRIVAL			= 0x8000;	// int = 32,768
			public const int DBT_DEVICEREMOVECOMPLETE	= 0x8004;	// int = 32,772
			public const int WM_DEVICECHANGE			= 0x0219;	// int =    537

			private const int DBT_DEVTYP_DEVICEINTERFACE = 0x0005;	// int = 5

			private static readonly Guid   GuidDevinterfaceUSBDevice       = new Guid("A5DCBF10-6530-11D2-901F-00C04FB951ED"); // USB devices
//			private static readonly Guid   GuidDevinterfaceBluetoothDevice = new Guid("0850302A-B344-4fda-9BE9-90576B8D46F0"); // Bluetooth devices
			private static			IntPtr notificationHandle			   = IntPtr.Zero;

			/// <summary>
			/// Registers a window to receive notifications when USB devices are plugged or unplugged.
			/// </summary>
			/// <param name="windowHandle">Handle to the window receiving notifications.</param>
			public static void RegisterUsbDeviceNotification(IntPtr windowHandle)
			{
				DevBroadcastDeviceinterface dbi = new DevBroadcastDeviceinterface
				{
					DeviceType = DBT_DEVTYP_DEVICEINTERFACE,
					Reserved = 0,
					ClassGuid = GuidDevinterfaceUSBDevice,
					Name = 0
				};

				dbi.Size = Marshal.SizeOf(dbi);
				IntPtr buffer = Marshal.AllocHGlobal(dbi.Size);
				Marshal.StructureToPtr(dbi, buffer, true);

				notificationHandle = RegisterDeviceNotification(windowHandle, buffer, 0);
			}

			/// <summary>
			/// Unregisters the window for USB device notifications
			/// </summary>
			public static void UnregisterUsbDeviceNotification()
			{
				UnregisterDeviceNotification(notificationHandle);
			}

			[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
			private static extern IntPtr RegisterDeviceNotification(IntPtr recipient, IntPtr notificationFilter, int flags);

			[DllImport("user32.dll")]
			private static extern bool UnregisterDeviceNotification(IntPtr handle);

			//DEV_BROADCAST_DEVICEINTERFACE 
			[StructLayout(LayoutKind.Sequential)]
			private struct DevBroadcastDeviceinterface
			{
				internal int Size;
				internal int DeviceType;
				internal int Reserved;
				internal Guid ClassGuid;
				internal short Name;
			}
		}	//class UsbNotification

		internal static class AudioDeviceNotification
		{
			//Test code for audio device change notifications.  Consider putting all this in a single object
			private static NAudio.CoreAudioApi.MMDeviceEnumerator		mAudioDeviceEnumerator   = new NAudio.CoreAudioApi.MMDeviceEnumerator();
			private static Desktop.Util.NAudio.VxMMNotificationClient	mAudioNotificationClient = null;

			public static void Register()
			{
				if ( mAudioNotificationClient == null )
				{ 
					mAudioNotificationClient = new Desktop.Util.NAudio.VxMMNotificationClient();
					mAudioDeviceEnumerator.RegisterEndpointNotificationCallback( mAudioNotificationClient );
				}
			}

			public static void Unregister()
			{
				if ( mAudioNotificationClient != null )
				{
					mAudioDeviceEnumerator.UnregisterEndpointNotificationCallback( mAudioNotificationClient );
				}

			}

			public static NAudio.CoreAudioApi.MMDeviceCollection GetDevices( NAudio.CoreAudioApi.DataFlow dataflow, NAudio.CoreAudioApi.DeviceState deviceStateMask )
			{ 
				NAudio.CoreAudioApi.MMDeviceCollection	devices	= mAudioDeviceEnumerator.EnumerateAudioEndPoints( dataflow, deviceStateMask );
			
				return devices;
			}


		}	//class AudioDeviceNotification

        #endregion

    }
}
