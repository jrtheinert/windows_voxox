﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using System.Windows;

namespace Desktop.Controls
{
    public class NavButton : ImageButton
    {
        /// <summary>
        /// Count to show on the Badge. The Badge will be hidden if the count it ZERO.
        /// </summary>
        public int BadgeCount
        {
            get { return (int)GetValue(BadgeCountProperty); }
            set { SetValue(BadgeCountProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for BadgeCount.
        /// </summary>
        public static readonly DependencyProperty BadgeCountProperty =
            DependencyProperty.Register("BadgeCount", typeof(int), typeof(NavButton), new UIPropertyMetadata(null));

        /// <summary>
        /// Property to set active tab (faux) status
        /// </summary>
        public bool IsActiveTab
        {
            get { return (bool)GetValue(IsActiveTabProperty); }
            set { SetValue(IsActiveTabProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for IsActive.
        /// </summary>
        public static readonly DependencyProperty IsActiveTabProperty =
            DependencyProperty.Register("IsActiveTab", typeof(bool), typeof(NavButton), new UIPropertyMetadata(null));
    }
}
