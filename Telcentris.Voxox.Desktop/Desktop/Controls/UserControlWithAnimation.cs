﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.ViewModels;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Windows;
using System.Windows.Media.Animation;

namespace Desktop.Controls
{
	public class UserControlWithAnimation : System.Windows.Controls.UserControl
	{
		private Storyboard	mAnimation		= null;
		private string		mAnimationKey	= null;		// = "OnCloseStoryboard";
		private bool		mEnded			= false;	//Necessary since we do NOT YET identify specific instances in OnStartAnimation and GC lags in cleaning up repeated instantiations.
        private object      mDataKey        = null;     // Additional Animation Key provided by StartAnimationMessage
        private object      mData           = null;     // Additional Animation Data provided by StartAnimationMessage

		public UserControlWithAnimation()
		{
            Messenger.Default.Register<StartAnimationMessage>(this, OnStartAnimation);

//			initializeAnimation();		//Derived class must call this, otherwise FindResource throws an exception.
		}

        /// <summary>
        /// This acts as a binding between ViewModel & the Control, so that only appropriate animation fires
        /// </summary>
        public string AnimationReferenceID
        {
            get { return (string)GetValue(AnimationReferenceIDProperty); }
            set { SetValue(AnimationReferenceIDProperty, value); }
        }

        // Dependency Property to bind the value from ViewModel in XAML
        public static readonly DependencyProperty AnimationReferenceIDProperty =
            DependencyProperty.Register("AnimationReferenceID", typeof(string), typeof(UserControlWithAnimation), new UIPropertyMetadata(null));

		protected void initializeClosingAnimation()
		{
			mAnimationKey = (string)FindResource("CloseStoryboardKey");

			if ( mAnimationKey != null )
			{
				try 
				{ 				
					var obj1 = this.FindResource( mAnimationKey );
					mAnimation = (System.Windows.Media.Animation.Storyboard) obj1;

					if ( mAnimation == null )
					{
						sendAnimationDoneMessage( null );
					}
					else
					{ 
						mAnimation.Completed += animation_Completed;
					}
				}
				catch( System.Windows.ResourceReferenceKeyNotFoundException /*ex*/)
				{
//					int xxx = 1;
				}
			}
		}

		private void OnStartAnimation( StartAnimationMessage msg )
		{
            if (msg.AnimationReferenceID != AnimationReferenceID)
                return;

			if ( !mEnded )
			{
                mDataKey = msg.DataKey; // Store additional data to be used when animation is completed
                mData = msg.Data;

				if ( mAnimation == null || msg.SkipAnimation == true )
				{
					sendAnimationDoneMessage( null );
				}
				else
				{
					mAnimation.Begin();
				}

				mEnded = true;
			}
		}

		private void animation_Completed( object sender, System.EventArgs e )
		{
			sendAnimationDoneMessage( sender );		
		}

		private void sendAnimationDoneMessage( object sender )
		{
			mAnimation = null;
            Messenger.Default.Send<AnimationDoneMessage>( new AnimationDoneMessage() { AnimationReferenceID = AnimationReferenceID, Object = this, DataKey = mDataKey, Data = mData } );
            mDataKey = null;
            mData = null;
		}
	}
}
