﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using System.Windows;
using System.Windows.Controls;

namespace Desktop.Controls
{
    class SettingsButton : Button
    {
        /// <summary>
        /// Property to set active tab (faux) status
        /// </summary>
        public bool IsActiveTab
        {
            get { return (bool)GetValue(IsActiveTabProperty); }
            set { SetValue(IsActiveTabProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for IsActive.
        /// </summary>
        public static readonly DependencyProperty IsActiveTabProperty =
            DependencyProperty.Register("IsActiveTab", typeof(bool), typeof(SettingsButton), new UIPropertyMetadata(null));

        /// <summary>
        /// To set the Header for Button
        /// </summary>
        public string Header
        {
            get { return (string)GetValue(HeaderProperty); }
            set { SetValue(HeaderProperty, value); }
        }

        public static readonly DependencyProperty HeaderProperty =
            DependencyProperty.Register("Header", typeof(string), typeof(SettingsButton), new UIPropertyMetadata(null));

        /// <summary>
        /// Explanatory text for the screens
        /// </summary>
        public string Description
        {
            get { return (string)GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }

        public static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register("Description", typeof(string), typeof(SettingsButton), new UIPropertyMetadata(null));
    }
}
