﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Dec 2015
 */

using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;			//Visual

namespace Desktop.Controls
{
    public class KeypadButton : Button
    {
        /// <summary>
        /// Key char
        /// </summary>
        public string KeyChar
        {
            get { return (string)GetValue(KeyCharProperty); }
            set { SetValue(KeyCharProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for KeyChar.
        /// </summary>
        public static readonly DependencyProperty KeyCharProperty =
            DependencyProperty.Register("KeyChar", typeof(string), typeof(KeypadButton), new UIPropertyMetadata(null));

        /// <summary>
        /// Property to set Subline
        /// </summary>
        public string Subline
        {
            get { return (string)GetValue(SublineProperty); }
            set { SetValue(SublineProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for Subline.
        /// </summary>
        public static readonly DependencyProperty SublineProperty =
            DependencyProperty.Register("Subline", typeof(string), typeof(KeypadButton), new UIPropertyMetadata(null));
    }
}
