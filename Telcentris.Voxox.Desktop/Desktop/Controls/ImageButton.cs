﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using System;						//Uri
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;	//BitmapImage

namespace Desktop.Controls
{
    public class ImageButton : Button
    {
        /// <summary>
        /// Button in normal state
        /// </summary>
        public ImageSource ImageSource
        {
            get { return (ImageSource)GetValue(ImageSourceProperty); }
            set { SetValue(ImageSourceProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for ImageSource. This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty ImageSourceProperty =
            DependencyProperty.Register("ImageSource", typeof(ImageSource), typeof(ImageButton), new UIPropertyMetadata(null));


        public ImageSource ImageSourceDisabled
        {
            get { return (ImageSource)GetValue(ImageSourceDisabledProperty); }
            set { SetValue(ImageSourceDisabledProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for ImageSource. This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty ImageSourceDisabledProperty =
            DependencyProperty.Register("ImageSourceDisabled", typeof(ImageSource), typeof(ImageButton), new UIPropertyMetadata(null));


        /// <summary>
        /// Button in hover state
        /// </summary>
        public ImageSource ImageSourceHover
        {
            get { return (ImageSource)GetValue(ImageSourceHoverProperty); }
            set { SetValue(ImageSourceHoverProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for ImageSourceHover. This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty ImageSourceHoverProperty =
            DependencyProperty.Register("ImageSourceHover", typeof(ImageSource), typeof(ImageButton), new UIPropertyMetadata(null));

        /// <summary>
        /// Button in active state
        /// </summary>
        public ImageSource ImageSourceActive
        {
            get { return (ImageSource)GetValue(ImageSourceActiveProperty); }
            set { SetValue(ImageSourceActiveProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for ImageSourceActive. This enables animation, styling, binding, etc...
        /// </summary>
        public static readonly DependencyProperty ImageSourceActiveProperty =
            DependencyProperty.Register("ImageSourceActive", typeof(ImageSource), typeof(ImageButton), new UIPropertyMetadata(null));
    }


	//Standard, defined ImageButtons
	public class InCallImageButtonBase : ImageButton
	{
		public InCallImageButtonBase( string imageSource, string imageSourceActive, string imageSourceHover, string imageSourceDisabled )
		{
			this.Style = FindResource( "ImageButtonBaseStyle" ) as Style;

			//Ideally, these would come from branded values or be in Style above
			this.Width	 = 28;
			this.Height	 = 28;

			//Image sources
			this.ImageSource		 = makeImage( imageSource		  );
			this.ImageSourceActive	 = makeImage( imageSourceActive   );
			this.ImageSourceHover	 = makeImage( imageSourceHover	  );
			this.ImageSourceDisabled = makeImage( imageSourceDisabled );
		}

		private ImageSource makeImage( string imagePath )
		{
			BitmapImage result = null;

			if ( !string.IsNullOrEmpty( imagePath ) )
			{
				string uriSource = Desktop.Config.AppConfigUtil.makeApplicationUri( imagePath );

				result = Desktop.Config.AppConfigUtil.makeImage( uriSource );
			}

			return result;
		}
	}

	public class InCallImageButtonEavesdrop : InCallImageButtonBase
	{
		public InCallImageButtonEavesdrop() : base( @"/Branding/Calling/calling-voicemail.png",					//White
													@"/Branding/Calling/calling-voicemail-active.png",			//Blue
													@"/Branding/Calling/calling-voicemail-active.png",			//Blue
													@"/Branding/Calling/calling-voicemail-deactivated.png" )	//Gray
		{
		}
	}

	public class InCallImageButtonIgnore : InCallImageButtonBase
	{
		public InCallImageButtonIgnore() : base( @"/Branding/Calling/calling-ignore.png",
												 @"/Branding/Calling/calling-ignore-active.png",
												 @"/Branding/Calling/calling-ignore-active.png",	
												 null )
		{
		}
	}

	public class InCallImageButtonMessage : InCallImageButtonBase
	{
		public InCallImageButtonMessage() : base( @"/Branding/Calling/calling-message.png",
												  @"/Branding/Calling/calling-message-active.png",
												  @"/Branding/Calling/calling-message-active.png",	
												  null )
		{
		}
	}
}
