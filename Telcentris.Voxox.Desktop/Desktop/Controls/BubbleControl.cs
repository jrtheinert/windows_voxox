﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Desktop.Controls
{
    public enum BubbleAnchorPosition
    {
        Left,
        Right
    }

    public class BubbleControl : ContentControl
    {
        public BubbleControl()
        {
            //DefaultStyleKeyProperty.OverrideMetadata(typeof(BubbleControl),
            //       new FrameworkPropertyMetadata(typeof(BubbleControl)));
        }

        public double AnchorOffset
        {
            get { return (double)GetValue(AnchorOffsetProperty); }
            set { SetValue(AnchorOffsetProperty, value); }
        }

        public static readonly DependencyProperty AnchorOffsetProperty =
            DependencyProperty.Register("AnchorOffset", typeof(double), typeof(BubbleControl), new FrameworkPropertyMetadata((double)20));

        public BubbleAnchorPosition AnchorPosition
        {
            get { return (BubbleAnchorPosition)GetValue(AnchorPositionProperty); }
            set { SetValue(AnchorPositionProperty, value); }
        }

        public static readonly DependencyProperty AnchorPositionProperty =
            DependencyProperty.Register("AnchorPosition", typeof(BubbleAnchorPosition), typeof(BubbleControl), new FrameworkPropertyMetadata(BubbleAnchorPosition.Left));

        public bool IsImage
        {
            get { return (bool)GetValue(IsImageProperty); }
            set { SetValue(IsImageProperty, value); }
        }

        public static readonly DependencyProperty IsImageProperty =
            DependencyProperty.Register("IsImage", typeof(bool), typeof(BubbleControl), new FrameworkPropertyMetadata(false));

        public Brush ImageMaskBackground
        {
            get { return (Brush)GetValue(IsImageMaskBackgroundProperty); }
            set { SetValue(IsImageMaskBackgroundProperty, value); }
        }

        public static readonly DependencyProperty IsImageMaskBackgroundProperty =
            DependencyProperty.Register("IsImageMaskBackground", typeof(Brush), typeof(BubbleControl), new FrameworkPropertyMetadata(Brushes.White));
    }
}
