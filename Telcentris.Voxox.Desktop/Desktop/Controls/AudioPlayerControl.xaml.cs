﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.ViewModels.Messages;		//VoicemailPlayClickedMessage
using Desktop.Model;					//RichMediaManager
using Desktop.Util;						//NetworkStatus	

using GalaSoft.MvvmLight.Messaging;

using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Threading;

namespace Desktop.Controls
{
    /// <summary>
    /// Interaction logic for AudioPlayerControl.xaml
    /// </summary>
    public partial class AudioPlayerControl : UserControl
    {
        private bool userIsDraggingSlider = false;
        System.Threading.CancellationTokenSource cancelToken = new System.Threading.CancellationTokenSource();

        public AudioPlayerControl()
        {
            InitializeComponent();

            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += timer_Tick;
            timer.Start();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if ((mePlayer.Source != null) && (mePlayer.NaturalDuration.HasTimeSpan) && (!userIsDraggingSlider))
            {
                sliderProgress.Minimum = 1;
                sliderProgress.Maximum = mePlayer.NaturalDuration.TimeSpan.TotalSeconds;
                sliderProgress.Value = mePlayer.Position.TotalSeconds;
                CompetedDuration.Text = mePlayer.Position.ToString("m\\:ss");

                if (string.IsNullOrEmpty(MediaDuration))
                {
                    TotalDuration.Text = mePlayer.NaturalDuration.TimeSpan.ToString("m\\:ss");
                }
            }
        }

        private void sliderProgress_DragStarted(object sender, DragStartedEventArgs e)
        {
            userIsDraggingSlider = true;
        }

        private void sliderProgress_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            userIsDraggingSlider = false;
            mePlayer.Position = TimeSpan.FromSeconds(sliderProgress.Value);
        }

        private void PlayButton_Click(object sender, RoutedEventArgs e)
        {
            if (NetworkStatus.Instance.IsAvailable == false)
            {
                NetworkStatus.Instance.ShowNetworkErrorMessage();
                return;
            }

			//Notify app that THIS message has been listened to.
			GalaSoft.MvvmLight.Messaging.Messenger.Default.Send<ViewModels.MessageListenedToMessage>( new ViewModels.MessageListenedToMessage { Timestamp = Convert.ToInt64( MsgTimestamp ) } );

            PlayButton.Visibility = System.Windows.Visibility.Collapsed;
            PauseButton.Visibility = System.Windows.Visibility.Collapsed;
            LoadingAnim.Visibility = System.Windows.Visibility.Visible;
            sliderProgress.IsEnabled = false;

            string msgTimestamp = MsgTimestamp;
            string mediaSource = MediaSource.AbsoluteUri;

            cancelToken = new System.Threading.CancellationTokenSource(); // Token used to cancel this operation if the user navigates off/loads different media

            Task.Run(() =>
            {
                var path = RichMediaManager.Download(msgTimestamp + ".wav", mediaSource);

                if (cancelToken.IsCancellationRequested)
                    return;

                GalaSoft.MvvmLight.Threading.DispatcherHelper.CheckBeginInvokeOnUI(() =>
                {
                    mePlayer.Source = new Uri(path);
                    StartPlayerUI();
                });

            }, cancelToken.Token);
        }

        private void PauseButton_Click(object sender, RoutedEventArgs e)
        {
            mePlayer.Pause();
            PlayButton.Visibility = System.Windows.Visibility.Visible;
            PauseButton.Visibility = System.Windows.Visibility.Collapsed;
        }

        /// <summary>
        /// Property to set audio file
        /// </summary>
        public Uri MediaSource
        {
            get { return (Uri)GetValue(MediaSourceProperty); }
            set { SetValue(MediaSourceProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for MediaSource.
        /// </summary>
        public static readonly DependencyProperty MediaSourceProperty = DependencyProperty.Register("MediaSource", typeof(Uri), typeof(AudioPlayerControl), new UIPropertyMetadata(null));

        public string MediaDuration
        {
            get { return (string)GetValue(MediaDurationProperty); }
            set { SetValue(MediaDurationProperty, value); }
        }

        public static readonly DependencyProperty MediaDurationProperty = DependencyProperty.Register("MediaDuration", typeof(string), typeof(AudioPlayerControl), new UIPropertyMetadata(null));

	
        /// <summary>
        /// Property to set msg timestamp
        /// </summary>
        public string MsgTimestamp
        {
            get { return (string)GetValue(MsgTimestampProperty); }
            set { SetValue(MsgTimestampProperty, value); }
        }

        public static readonly DependencyProperty MsgTimestampProperty = DependencyProperty.Register("MsgTimestamp", typeof(string), typeof(AudioPlayerControl), new UIPropertyMetadata(null));

        private void mePlayer_MediaEnded(object sender, RoutedEventArgs e)
        {
            var me = (MediaElement)sender;
            me.Stop();

            PlayButton.Visibility = System.Windows.Visibility.Visible;
            PauseButton.Visibility = System.Windows.Visibility.Collapsed;
        }

        public void ResetPlayer()
        {
            cancelToken.Cancel(false); // Cancel existing start operation

            StopPlayerUI();
        }

        private void StartPlayerUI()
        {
            mePlayer.Play();

            PlayButton.Visibility = System.Windows.Visibility.Collapsed;
            PauseButton.Visibility = System.Windows.Visibility.Visible;
            LoadingAnim.Visibility = System.Windows.Visibility.Collapsed;
            TotalDuration.Text = MediaDuration;
            sliderProgress.IsEnabled = true;

            Messenger.Default.Send<VoicemailPlayClickedMessage>(new VoicemailPlayClickedMessage { Timestamp = MsgTimestamp });
        }

        private void StopPlayerUI()
        {
            mePlayer.Stop();

            PlayButton.Visibility = System.Windows.Visibility.Visible;
            PauseButton.Visibility = System.Windows.Visibility.Collapsed;
            LoadingAnim.Visibility = System.Windows.Visibility.Collapsed;
            sliderProgress.IsEnabled = false;

            sliderProgress.Minimum = 0;
            sliderProgress.Maximum = 1;
            sliderProgress.Value = 0;
            CompetedDuration.Text = "0:00";
        }
    }
}
