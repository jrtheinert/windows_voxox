﻿
using Desktop.Model;				//ActionManager

using System.Windows;				//Thickness
using System.Windows.Controls;		//Border
using System.Windows.Media;			//Visual
using System.Windows.Input;			//MouseEventArgs

namespace Desktop.Controls
{
    public class VxListBox : System.Windows.Controls.ListBox
    {
		//public VxListBox()
		//{
		//	//Appears we need a DependencyProperty for VirtualizingPanel?
		//	VirtualizingPanel.IsVirtualizing			 = true;
		//	VirtualizingPanel.IsVirtualizingWhenGrouping = true;
		//	VirtualizingPanel.IsContainerVirtualizable	 = true;
		//}

		//Remove all padding
		override public void OnApplyTemplate()
        {
			Visual myVisual = (System.Windows.Media.Visual)this;

            ((Border)VisualTreeHelper.GetChild( myVisual, 0)).Padding = new Thickness(0);
        }

		//Profile is displayed in center panel.
		//	Any time list box in left panel is clicked, close the profile.
		protected override void OnMouseUp( MouseButtonEventArgs e )
		{
			base.OnMouseUp( e );

			ActionManager.Instance.CloseProfile();
		}
    }
}
