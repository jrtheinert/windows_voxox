﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;					//ActionManager
using Desktop.Util;						//PhoneUtils

using System;							//Uri, EventArgs, RoutedEventHandler
using System.Collections.Generic;		//List
using System.Linq;						//Text processing
using System.Text.RegularExpressions;	//RegEx
using System.Windows;					//Style
using System.Windows.Controls;			//Text, Textbox, FindResource
using System.Windows.Documents;			//Hyperlink, Run

namespace Desktop.Controls
{
    /// <summary>
    /// A Textbox/Textblock control to show formatted text
    /// </summary>
    public class RichTextView : TextBox
    {
        // RegEx to match URL
        static Regex urlRegEx = new Regex(@"\b(?:https?://|www\.)\S+\b", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        static Regex phoneRegEx = new Regex(@"(?<=\s)([\+][0-9]{1,3}([ \.\-])?)?([\(]{1}[0-9]{3}[\)])?([0-9A-Z \.\-]{1,32})((x|ext|extension)?[0-9]{1,4}?)", RegexOptions.Compiled);

        
        /// <summary>
        /// This procedure is called by WPF XAML rendering
        /// </summary>
        public override void OnApplyTemplate()
        {
            var displayTextBlock = GetTemplateChild("DisplayTextBlock") as TextBlock;

            if (displayTextBlock != null)
            {
                UpdateTextblock(displayTextBlock, Text);
            }

            base.OnApplyTemplate();
        }

        /// <summary>
        /// Update the displayTextBlock with the parsed text finding any URLs, Phone Numbers etc
        /// </summary>
        /// <param name="displayTextBlock"></param>
        /// <param name="Text"></param>
        private void UpdateTextblock(TextBlock displayTextBlock, string Text)
        {
            displayTextBlock.Inlines.Clear();

            if (string.IsNullOrWhiteSpace(Text))
                return;

            Text = " " + Text;  //Adding space before text to identify phonenumber when text contains phonenumber with country code.

            List<Range> ranges = ProcessText(Text, urlRegEx, Range.RangeType.Link); // External Link

            foreach (var r in ranges)
            {
                if (r.Type == Range.RangeType.Link)
                {
                    Hyperlink hyperl = new Hyperlink(new Run(r.Text));
                    hyperl.NavigateUri = new Uri(FixUrl(r.Text));
                    hyperl.RequestNavigate += OnExternalLinkNavigate;
                    hyperl.Style = FindResource("LinkStyleInMessageBubble") as Style;
                    
                    displayTextBlock.Inlines.Add(hyperl);
                }
                else
                {
                    List<Range> phoneRanges = ProcessText(r.Text, phoneRegEx, Range.RangeType.Phone);
                    
                    foreach (var pr in phoneRanges)
                    {
                        if (pr.Type == Range.RangeType.Phone)
                        {
							String temp = PhoneUtils.CleanPhoneNumberFormatting( pr.Text );	//Let's clean the number before we determine valid length.

                            if ( temp.Length > 3 && PhoneUtils.IsValidPhoneNumberLength( temp ) ) 
                            {
                                Hyperlink hyperl = new Hyperlink(new Run(pr.Text));
                                hyperl.Tag    = pr.Text;    // Store the Phone Number, using the entire number entered by user as country code is required.
                                hyperl.Click += OnPhoneLinkClick;
                                hyperl.Style  = FindResource("LinkStyleInMessageBubble") as Style;

                                displayTextBlock.Inlines.Add(hyperl);
                            }
                            else
                            {
                                displayTextBlock.Inlines.Add(new Run(pr.Text));
                            }
                        }
                        else
                        {
                            displayTextBlock.Inlines.Add(new Run(pr.Text));
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Get Ranges of text with matched RegEx
        /// </summary>
        /// <param name="text"></param>
        /// <param name="RegEx"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private static List<Range> ProcessText(string text, Regex RegEx, Range.RangeType type)
        {
            List<Range> ranges = new List<Range>();
            var matches = RegEx.Matches(text);

            if (matches != null && matches.Count > 0)
            {
                int lastEnd = 0;

                foreach (Match match in matches)
                {
                    // Get text range before the start of the match
                    ranges.Add(new Range { Start = lastEnd, End = match.Index, Type = Range.RangeType.Plain });

                    // Get matched text range
                    ranges.Add(new Range { Start = match.Index, End = match.Index + match.Length, Type = type });

                    // remember the ending index 
                    lastEnd = match.Index + match.Length;
                }

                // Get the last remaining range
                ranges.Add(new Range { Start = lastEnd, End = text.Length, Type = Range.RangeType.Plain });

                // Remove ranges that are not valid
                ranges = (from r in ranges
                          where r.End - r.Start > 0
                          select r).ToList();

                // Splice the text
                foreach (var r in ranges)
                {
                    r.Text = text.Substring(r.Start, r.End - r.Start);
                }
            }
            else
            {
                ranges.Add(new Range { Start = 0, End = text.Length, Text = text, Type = Range.RangeType.Plain });
            }

            return ranges;
        }

        /// <summary>
        /// Checks the URL for valid protocol & appends http:// if there isn't
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        private static string FixUrl(string url)
        {
            if (url == null)
                return "";

            if (!url.ToLower().StartsWith("http://") &&
                !url.ToLower().StartsWith("https://") &&
                !url.ToLower().StartsWith("ftp://"))
            {
                return "http://" + url;
            }

            return url;
        }

        /// <summary>
        /// Event fired when a link is clicked on the TextBlock
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnExternalLinkNavigate(object sender, System.Windows.Navigation.RequestNavigateEventArgs e)
        {
            System.Diagnostics.Process.Start(e.Uri.AbsoluteUri);
        }

        private void OnPhoneLinkClick(object sender, EventArgs e)
        {
            var hyperl = (Hyperlink)sender;

            var phoneNumber = Desktop.Util.PhoneUtils.GetPhoneNumberForCall( hyperl.Tag.ToString(), false );	//TODO: current hyperlink parsing will not detect short extensions like '101'
            var countryCode = Desktop.Util.PhoneUtils.GetCountryCodeByPhone(phoneNumber);

            ActionManager.Instance.ShowDialer(false, -1, phoneNumber, countryCode);
        }

        /// <summary>
        /// Text Range objects
        /// </summary>
        private class Range
        {
            public enum RangeType
            {
                Plain = 0,
                Link,
                Phone
            }

            public int Start { get; set; }

            public int End { get; set; }

            public string Text { get; set; }

            public RangeType Type { get; set; }
        }
    }
}
