﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Desktop.Controls
{
    public class CustomButton : Button
    {
        public Brush NormalBackground
        {
            get { return (Brush)GetValue(NormalBackgroundProperty); }
            set { SetValue(NormalBackgroundProperty, value); }
        }

        public static readonly DependencyProperty NormalBackgroundProperty =
            DependencyProperty.Register("NormalBackground", typeof(Brush), typeof(CustomButton), new FrameworkPropertyMetadata(null));

        public Brush HoverBackground
        {
            get { return (Brush)GetValue(HoverBackgroundProperty); }
            set { SetValue(HoverBackgroundProperty, value); }
        }

        public static readonly DependencyProperty HoverBackgroundProperty =
            DependencyProperty.Register("HoverBackground", typeof(Brush), typeof(CustomButton), new FrameworkPropertyMetadata(null));

        public Brush FocusBackground
        {
            get { return (Brush)GetValue(FocusBackgroundProperty); }
            set { SetValue(FocusBackgroundProperty, value); }
        }

        public static readonly DependencyProperty FocusBackgroundProperty =
            DependencyProperty.Register("FocusBackground", typeof(Brush), typeof(CustomButton), new FrameworkPropertyMetadata(null));

        public double CornerRadius
        {
            get { return (double)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }

        public static readonly DependencyProperty CornerRadiusProperty =
            DependencyProperty.Register("CornerRadius", typeof(double), typeof(CustomButton), new FrameworkPropertyMetadata(null));

    }

	//Here we combine the ActionButton style defined in CustomButtonResource.xaml
	//	with ActionButtonImage style also defined in CustomButtonResource.xaml
	//	to create defined ActionButtons.
	//
	//These are larger and round, used in Contact and Call Detail windows.
	public class ActionButtonBase : CustomButton
	{
		private Image mImage;

		public ActionButtonBase( string style, string buttonImagePath )
		{
			//Get base-style (mostly colors) and adjust with standard action button values
			this.Style = FindResource( style ) as Style;

			//Ideally, these would come from branded values
			this.VerticalAlignment	 = System.Windows.VerticalAlignment.Center;
			this.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
			this.Width				 = 43;
			this.Height				 = 43;
			this.CornerRadius		 = 21;

			string uriSource = Desktop.Config.AppConfigUtil.makeApplicationUri( buttonImagePath );
			mImage = new Image();
			mImage.Style = FindResource( "ActionButtonImage" ) as Style;

			mImage.Source = Desktop.Config.AppConfigUtil.makeImage( uriSource );

			this.Content = mImage;
		}
	}

	public class ActionButtonCall : ActionButtonBase
	{
		public ActionButtonCall() : base( "GreenButton", "/Branding/Calls/call-details-call.png")
		{
		}
	}

	public class ActionButtonDelete : ActionButtonBase
	{
		public ActionButtonDelete() : base( "RedButton", "/Branding/Calls/call-details-delete.png")
		{
		}
	}

	public class ActionButtonHistory : ActionButtonBase
	{
		public ActionButtonHistory() : base( "BlueButton", "/Branding/Calls/call-details-history.png")
		{
		}
	}

	//Here we combine the ActionButton style defined in CustomButtonResource.xaml
	//	with ActionButtonImage style also defined in CustomButtonResource.xaml
	//	to create defined ActionButtons.
	//
	//These are larger and round, used in Contact and Call Detail windows.
	public class ActionButtonBase2 : CustomButton
	{
		private Image mImage;

		public ActionButtonBase2( string style, string buttonImagePath )
		{
			//Get base-style (mostly colors) and adjust with standard action button values
			this.Style = FindResource( style ) as Style;

			//Ideally, these would come from branded values
			this.VerticalAlignment	 = System.Windows.VerticalAlignment.Center;
			this.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
			this.Width				 = 48;
			this.Height				 = 28;
			this.CornerRadius		 = 16;

			string uriSource = Desktop.Config.AppConfigUtil.makeApplicationUri( buttonImagePath );
			mImage = new Image();
			mImage.Style = FindResource( "ActionButtonImageSmall" ) as Style;

			mImage.Source = Desktop.Config.AppConfigUtil.makeImage( uriSource );

			this.Content = mImage;
		}
	}

	public class ActionButtonCallLong : ActionButtonBase2
	{
		public ActionButtonCallLong() : base( "GreenButton", @"/Branding/Calls/call-details-call.png")
		{
		}
	}

	public class ActionButtonMessageLong : ActionButtonBase2
	{
		public ActionButtonMessageLong() : base( "GreenButton", @"/Branding/Contacts/contact-detail-message.png")
		{
		}
	}

	//public class ActionButtonHistoryLong : ActionButtonBase
	//{
	//	public ActionButtonHistoryLong() : base( "BlueButton", "/Branding/Calls/call-details-history.png")
	//	{
	//	}
	//}

 	//Here we combine the ActionButton style defined in CustomButtonResource.xaml
	//	with ActionButtonImage style also defined in CustomButtonResource.xaml
	//	to create defined ActionButtons.
	//
	//These are used for IncomingCall view.
	public class ActionButtonInCallBase : CustomButton
	{
		public ActionButtonInCallBase( string style, string content )
		{
			//Get base-style (mostly colors) and adjust with standard action button values
			this.Style = FindResource( style ) as Style;

			//Ideally, these would come from branded values
			this.Height				 = 36;
			this.CornerRadius		 = 18;
			this.Margin				 = new Thickness( 32, 4, 32, 4 );
			this.Padding			 = new Thickness( 8 );
			this.FontFamily			 = FindResource( "Font_AFSB" ) as FontFamily;
			this.FontSize			 = 14;

			//Custom settings
			this.Content = content;
		}
	}

	public class ActionButtonInCallAccept : ActionButtonInCallBase
	{
		public ActionButtonInCallAccept() : base( "GreenButton", LocalizedString.IncomingCall_Accept )
		{
//			this.Margin = new Thickness( 32, 8, 32, 8 );	//TODO: Not sure why this margin is different from others.
		}
	}

	public class ActionButtonInCallAcceptAndHold : ActionButtonInCallBase
	{
		public ActionButtonInCallAcceptAndHold() : base( "GreenButton", LocalizedString.IncomingCall_AcceptAndHold )
		{
		}
	}

	public class ActionButtonInCallAcceptAndEnd : ActionButtonInCallBase
	{
		public ActionButtonInCallAcceptAndEnd() : base( "BlueButton", LocalizedString.IncomingCall_AcceptAndEnd )
		{
		}
	}

	public class ActionButtonInCallDecline : ActionButtonInCallBase
	{
		public ActionButtonInCallDecline() : base( "RedButton", LocalizedString.IncomingCall_Decline )
		{
//			this.Margin = new Thickness( 32, 0, 32, 0 );	//TODO: Not sure why this margin is different from others.
		}
	}
}
