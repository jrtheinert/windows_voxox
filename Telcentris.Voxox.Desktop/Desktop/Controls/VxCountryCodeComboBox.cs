﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Dec 2015
 */

using Desktop.Model;					//SettingsManager
using Desktop.Model.Dialer;				//CountryCode, CountryCodeList	- TODO: Move to Model

using GalaSoft.MvvmLight.Messaging;		//Messenger

using System;
using System.Collections.ObjectModel;	//ObservableCollection				
using System.ComponentModel;			//INotifyPropertyChanged
using System.Windows.Media;				//Brush
using System.Windows;					//DependencyProperty
using System.Windows.Controls;			//ComboBox

namespace Desktop.Controls
{
	public enum CountryCodeComboBoxUsage
	{
    	Dialpad	= 0,
    	Login   = 1,
    };

	class CountryCodeDataTemplateSelector : DataTemplateSelector
	{
		public CountryCodeComboBoxUsage Usage { get; set; }

		public CountryCodeDataTemplateSelector()
		{
			Usage = CountryCodeComboBoxUsage.Dialpad;
		}

		public override DataTemplate SelectTemplate( object item, DependencyObject container )
		{
			DataTemplate result = null;

			FrameworkElement element = container as FrameworkElement;

			if ( element != null )
			{
				switch ( Usage )
				{
				case CountryCodeComboBoxUsage.Dialpad:
					result = element.FindResource( "CountryCodeComboBoxDataTemplateForDialpad" ) as DataTemplate;
					break;
				case CountryCodeComboBoxUsage.Login:
					result = element.FindResource( "CountryCodeComboBoxDataTemplateForLogin" ) as DataTemplate;
					break;
				default:
					result = base.SelectTemplate( item, container );
					break;
				}
			}

			return result;
		}
	}

	class VxCountryCodeComboBox : ComboBox, INotifyPropertyChanged
	{
		private Boolean	mLoading		   = false;
        private String	mCountryFilter;
		private object	mPrevSelectedItem  = null;

		private CountryCodeDataTemplateSelector mItemDataTemplateSelector;
		private CountryCodeComboBoxUsage	    mUsage					  = CountryCodeComboBoxUsage.Dialpad;

		public ObservableCollection<CountryCode> CountryCodes			{ get; set; }
		public CountryCodeComboBoxUsage			 Usage
		{
			get { return mUsage; }
			set { 
					mUsage = value; 
					mItemDataTemplateSelector.Usage = mUsage;
				}
		}

		public CountryCodeDataTemplateSelector MyDataTemplateSelector
		{ 
			get { return mItemDataTemplateSelector; }
			private set { mItemDataTemplateSelector = value; }
		}

		public VxCountryCodeComboBox()
		{
			//Avoid error in Designer.
			if ( System.ComponentModel.DesignerProperties.GetIsInDesignMode(this) )
				return;

 			mItemDataTemplateSelector = new CountryCodeDataTemplateSelector();
            CountryCodes			  = new ObservableCollection<Model.Dialer.CountryCode>();

            LoadCountryCodes(string.Empty);

            Messenger.Default.Register<SettingsChangeMessage>( this, OnSettingsChange );
		}

		private void OnSettingsChange( SettingsChangeMessage msg )
		{
			if ( msg.isForDefaultCountryCode() )
			{ 
				setDefaultCountryCode();
			}
		}

		private void setDefaultCountryCode()
		{
			switch ( Usage )
			{ 
			case CountryCodeComboBoxUsage.Dialpad:
				if ( SettingsManager.Instance.User != null )
				{ 
					String key = CountryCode.makeKey( SettingsManager.Instance.User.CountryCode, SettingsManager.Instance.User.CountryAbbreviation );
					this.SelectedItem = CountryCodeList.Instance.getByKey( key );
				}
				break;

			case CountryCodeComboBoxUsage.Login:
				this.SelectedItem = CountryCodeList.Instance.getByKey( SettingsManager.Instance.App.LastLoginCountryCode );
				break;
			}
		}

        /// <summary>
        /// Filter country codes
        /// </summary>
        /// <param name="key">Partial country name</param>
        private void LoadCountryCodes( string key )
        {
			mLoading = true;

			CountryCodes = CountryCodeList.Instance.FilterAsObserveableCollection( key );
			ItemsSource  = CountryCodes;
			
			mLoading = false;
        }

		protected override void OnInitialized( EventArgs e )
		{
			base.OnInitialized( e );

			setDefaultCountryCode();
		}

		protected override void OnSelectionChanged( SelectionChangedEventArgs e )
		{
			base.OnSelectionChanged( e );

			if ( !mLoading )
			{ 
				SelectedCountryCode = (CountryCode)SelectedItem;
			}
		}

		protected override void OnDropDownOpened( System.EventArgs e )
		{
			base.OnDropDownOpened( e );

			if ( IsDropDownOpen )
			{ 
				if ( SelectedItem != null )
					mPrevSelectedItem  = SelectedItem;
			}
        }

		protected override void OnDropDownClosed( EventArgs e )
		{
			base.OnDropDownClosed( e );

			if ( ! IsDropDownOpen )
			{
				//If user clicks outside dropdown, it closes with nothing selected.
				//	In that case restore previously selected item.
				if ( SelectedItem == null )
				{ 
					SelectedItem = mPrevSelectedItem;
				}

				CountryFilter = string.Empty;
			}
        }


		#region Dependency Properties to allow 'styling' of nested elements

        public Brush ToggleButtonDisabledBrush
        {
            get { return (Brush)GetValue(ToggleButtonDisabledBrushProperty); }
            set { SetValue(ToggleButtonDisabledBrushProperty, value); }
        }

        public static readonly DependencyProperty ToggleButtonDisabledBrushProperty =
            DependencyProperty.Register("ToggleButtonDisabledBrush", typeof(Brush), typeof(VxCountryCodeComboBox), new UIPropertyMetadata(null));

        public Brush ComboBoxItemHighlightedBrush
        {
            get { return (Brush)GetValue(ComboBoxItemHighlightedBrushProperty); }
            set { SetValue(ComboBoxItemHighlightedBrushProperty, value); }
        }


        public static readonly DependencyProperty ComboBoxItemHighlightedBrushProperty =
            DependencyProperty.Register("ComboBoxItemHighlightedBrush", typeof(Brush), typeof(VxCountryCodeComboBox), new UIPropertyMetadata(null));


        public int PopupVerticalOffset
        {
            get { return (int)GetValue(PopupVerticalOffsetProperty); }
            set { SetValue(PopupVerticalOffsetProperty, value); }
        }

        public static readonly DependencyProperty PopupVerticalOffsetProperty =
            DependencyProperty.Register("PopupVerticalOffset", typeof(int), typeof(VxCountryCodeComboBox), new UIPropertyMetadata(null));


        public int PopupPanelZIndex
        {
            get { return (int)GetValue(PopupPanelZIndexProperty); }
            set { SetValue(PopupPanelZIndexProperty, value); }
        }

        public static readonly DependencyProperty PopupPanelZIndexProperty =
            DependencyProperty.Register("PopupPanelZIndex", typeof(int), typeof(VxCountryCodeComboBox), new UIPropertyMetadata(null));


        public double PopupDropDownWidth
        {
            get { return (double)GetValue(PopupDropDownWidthProperty); }
            set { SetValue(PopupDropDownWidthProperty, value); }
        }

        public static readonly DependencyProperty PopupDropDownWidthProperty =
            DependencyProperty.Register("PopupDropDownWidth", typeof(double), typeof(VxCountryCodeComboBox), new UIPropertyMetadata(null));


        public Brush PopupDropDownBackgroundBrush
        {
            get { return (Brush)GetValue(PopupDropDownBackgroundBrushProperty); }
            set { SetValue(PopupDropDownBackgroundBrushProperty, value); }
        }

        public static readonly DependencyProperty PopupDropDownBackgroundBrushProperty =
            DependencyProperty.Register("PopupDropDownBackgroundBrush", typeof(Brush), typeof(VxCountryCodeComboBox), new UIPropertyMetadata(null));


        public Brush PopupDropDownBorderBrush
        {
            get { return (Brush)GetValue(PopupDropDownBorderBrushProperty); }
            set { SetValue(PopupDropDownBorderBrushProperty, value); }
        }

        public static readonly DependencyProperty PopupDropDownBorderBrushProperty =
            DependencyProperty.Register("PopupDropDownBorderBrush", typeof(Brush), typeof(VxCountryCodeComboBox), new UIPropertyMetadata(null));


        public Brush PopupDropDownTextBoxForegroundBrush
        {
            get { return (Brush)GetValue(PopupDropDownTextBoxForegroundBrushProperty); }
            set { SetValue(PopupDropDownTextBoxForegroundBrushProperty, value); }
        }

        public static readonly DependencyProperty PopupDropDownTextBoxForegroundBrushProperty =
            DependencyProperty.Register("PopupDropDownTextBoxForegroundBrush", typeof(Brush), typeof(VxCountryCodeComboBox), new UIPropertyMetadata(null));


        public Brush PopupDropDownTextBoxCaretBrush
        {
            get { return (Brush)GetValue(PopupDropDownTextBoxCaretBrushProperty); }
            set { SetValue(PopupDropDownTextBoxCaretBrushProperty, value); }
        }

        public static readonly DependencyProperty PopupDropDownTextBoxCaretBrushProperty =
            DependencyProperty.Register("PopupDropDownTextBoxCaretBrush", typeof(Brush), typeof(VxCountryCodeComboBox), new UIPropertyMetadata(null));

		#endregion


		/// <summary>
        /// SelectedCountryCode - So ViewModel can simply bind to this property
        /// </summary>
        public CountryCode SelectedCountryCode
        {
            get { return (CountryCode)GetValue(SelectedCountryCodeProperty); }
            set { SetValue(SelectedCountryCodeProperty, value); }
        }

        /// <summary>
        /// Using a DependencyProperty as the backing store for SelectedCountryCode.
        /// </summary>
        public static readonly DependencyProperty SelectedCountryCodeProperty =
            DependencyProperty.Register("SelectedCountryCode", typeof(CountryCode), typeof(VxCountryCodeComboBox), new UIPropertyMetadata(null));


		public string CountryFilter
		{
			get { return mCountryFilter; }
			set
			{
				mCountryFilter = value;
				RaisePropertyChanged( "CountryFilter" );
				LoadCountryCodes(value);
			}
		}

		//So we can clear the TextBox after selection.
		public event PropertyChangedEventHandler PropertyChanged;

		public void RaisePropertyChanged( String propertyName )
		{
			var propertyChanged = this.PropertyChanged;

			if(propertyChanged!=null)
			{
				PropertyChangedEventArgs ev = new PropertyChangedEventArgs( propertyName );
				propertyChanged( this, ev );
			}
		}

	}
}
