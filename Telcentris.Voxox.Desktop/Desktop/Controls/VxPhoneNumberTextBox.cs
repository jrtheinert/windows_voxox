﻿
using Desktop;					//PhoneUtils, Keyboard
using Desktop.Model;			//ActionManager

using System;
using System.Windows;			//DataObject
using System.Windows.Controls;	//TextBox, TextChangedEventArgs
using System.Windows.Input;		//KeyEventArgs

namespace Desktop.Controls
{
	public enum PhoneNumberTextBoxUsage
	{
    	Unknown = 0,
    	Dialpad	= 1,
    	Login   = 2,
    };

	class VxPhoneNumberTextBox : TextBox
	{
        private bool mIsPasteOperation	= false;

		public TextBlock				AssociatedTextBlock	{ get; set; }
        public bool						IsAlphaPhoneNumber	{ get; set; }
		public PhoneNumberTextBoxUsage	Usage				{ get; set; }

		public VxPhoneNumberTextBox()
		{
			IsAlphaPhoneNumber  = false;
			Usage			= PhoneNumberTextBoxUsage.Unknown;
			DataObject.AddPastingHandler( this, OnPaste);
		}

		private bool usageIsDialpad()
		{
			return Usage == PhoneNumberTextBoxUsage.Dialpad;
		}

        //To prevent illegal chars from being entered into TextBox.
        //	If we do not like the key entered, then in keypress, mark the event as Handled. 
		protected override void OnKeyDown( System.Windows.Input.KeyEventArgs e )
		{
			base.OnKeyDown( e );

			Boolean isValid = false;

			if ( e.Key == Key.Tab && !usageIsDialpad() )
			{ 
				isValid =  true;
			}
			else
			{ 
				String newChar = new String(Util.Keyboard.GetCharFromKey(e.Key), 1);

				isValid = Util.PhoneUtils.IsValidChar(newChar);
			}

            e.Handled = !isValid;
        }

		protected override void OnKeyUp( KeyEventArgs e )
		{
			base.OnKeyUp( e );

			if ( mIsPasteOperation )
            {
                HandlePaste();
                mIsPasteOperation = false;
            }
			else if (e.KeyboardDevice.Modifiers != ModifierKeys.None && e.KeyboardDevice.Modifiers != ModifierKeys.Shift)
			{ 
                return;
			}
			else 
			{ 
				Char ch  = Util.Keyboard.GetCharFromKey(e.Key);
				Char key = Util.PhoneUtils.TranslateKey(ch);

				if (e.Key >= Key.A && e.Key <= Key.Z)
				{
					int caretIndex	= this.CaretIndex;
					this.Text		= this.Text.ToUpper().Replace(e.Key.ToString().ToCharArray()[0], key);
					this.CaretIndex = caretIndex;
				}

				if ( usageIsDialpad() )
				{ 
					ActionManager.Instance.PlayDtmf( key, false );	//TODO: May need to revisit 'false'
				}
			}
        }

		protected override void OnTextChanged( TextChangedEventArgs e )
		{
			base.OnTextChanged( e );

			if ( mIsPasteOperation )
            {
                HandlePaste();
            }
			else
			{
				if ( usageIsDialpad() )
				{
					if (IsAlphaPhoneNumber)
					{
						IsAlphaPhoneNumber = false;
					}
					else
					{
						if ( AssociatedTextBlock.Visibility == System.Windows.Visibility.Visible)
						{
							var vm = (Desktop.ViewModels.Dialer.DialPadViewModel)this.DataContext;

							if (vm != null)
							{
								vm.AlphaPhoneNumber   = "";
								vm.IsAlphaPhoneNumber = IsAlphaPhoneNumber;
							}
						}
					}
				}
			}
        }

        private void OnPaste(object sender, DataObjectPastingEventArgs e)
        {
            var isText = e.SourceDataObject.GetDataPresent( DataFormats.UnicodeText, true );

            mIsPasteOperation = isText;
        }

        private void HandlePaste()
        {
			char[] numDigits = Util.PhoneUtils.TranslateKey( this.Text );
          
			IsAlphaPhoneNumber = this.Text != numDigits.ToString();

			if ( usageIsDialpad() )
			{
				if ( mIsPasteOperation )
				{
					var vm = (Desktop.ViewModels.Dialer.DialPadViewModel)this.DataContext;

					if (vm != null)
					{
						vm.AlphaPhoneNumber   = string.Format("\"{0}\"", this.Text);
						vm.IsAlphaPhoneNumber = IsAlphaPhoneNumber;
					}
				}
			}

            mIsPasteOperation = false;

            int caretIndex	= this.CaretIndex;
            this.Text		= numDigits.Length > 0 ? string.Join("", numDigits) : this.Text;
            this.CaretIndex = caretIndex;
        }
	}
}
