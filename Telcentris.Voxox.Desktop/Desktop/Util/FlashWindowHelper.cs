﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using System;
using System.Windows;					//Window
using System.Runtime.InteropServices;	//DllImport, DllImportAttribute, MarshalAs, Marshal

namespace Desktop.Util
{
	public class FlashWindowHelper
	{
		private IntPtr			mWindowHWnd;
		static private UInt32	mFlashFlags = FLASHW_ALL | FLASHW_TIMERNOFG;
//		static private UInt32	mFlashFlags = FLASHW_TRAY | FLASHW_TIMERNOFG;

		public FlashWindowHelper( Window win )
		{
			mWindowHWnd = new System.Windows.Interop.WindowInteropHelper( win ).Handle;
		}

		public void FlashApplicationWindow( UInt16 seconds )
		{
			Flash( mWindowHWnd, seconds );
		}

		public void StopFlashing()
		{
			if (Win2000OrLater)
			{
				FLASHWINFO fi = CreateFlashInfoStruct( mWindowHWnd, FLASHW_STOP, uint.MaxValue, 0);
				FlashWindowEx(ref fi);
			}
		}

		[DllImport("user32.dll")]
		[return: MarshalAs(UnmanagedType.Bool)]
		private static extern bool FlashWindowEx(ref FLASHWINFO pwfi);

		[StructLayout(LayoutKind.Sequential)]
		private struct FLASHWINFO
		{
			/// <summary>
			/// The size of the structure in bytes.
			/// </summary>
			public uint cbSize;
			/// <summary>
			/// A Handle to the Window to be Flashed. The window can be either opened or minimized.
			/// </summary>
			public IntPtr hwnd;
			/// <summary>
			/// The Flash Status.
			/// </summary>
			public uint dwFlags;
			/// <summary>
			/// The number of times to Flash the window.
			/// </summary>
			public uint uCount;
			/// <summary>
			/// The rate at which the Window is to be flashed, in milliseconds. If Zero, the function uses the default cursor blink rate.
			/// </summary>
			public uint dwTimeout;
		}

		/// <summary>
		/// Stop flashing. The system restores the window to its original stae.
		/// </summary>
		public const uint FLASHW_STOP = 0;

		/// <summary>
		/// Flash the window caption.
		/// </summary>
		public const uint FLASHW_CAPTION = 1;

		/// <summary>
		/// Flash the taskbar button.
		/// </summary>
		public const uint FLASHW_TRAY = 2;

		/// <summary>
		/// Flash both the window caption and taskbar button.
		/// This is equivalent to setting the FLASHW_CAPTION | FLASHW_TRAY flags.
		/// </summary>
		public const uint FLASHW_ALL = 3;

		/// <summary>
		/// Flash continuously, until the FLASHW_STOP flag is set.
		/// </summary>
		public const uint FLASHW_TIMER = 4;

		/// <summary>
		/// Flash continuously until the window comes to the foreground.
		/// </summary>
		public const uint FLASHW_TIMERNOFG = 12;

		/// <summary>
		/// Flash the specified Window until it receives focus.
		/// </summary>
		/// <param name="hwnd"></param>
		/// <returns></returns>
		public static bool Flash(IntPtr hwnd)
		{
			// Make sure we're running under Windows 2000 or later
			if (Win2000OrLater)
			{
				FLASHWINFO fi = CreateFlashInfoStruct(hwnd, mFlashFlags, uint.MaxValue, 0);

				return FlashWindowEx(ref fi);
			}
			return false;
		}

		private static FLASHWINFO CreateFlashInfoStruct(IntPtr handle, uint flags, uint count, uint timeout)
		{
			FLASHWINFO fi = new FLASHWINFO();
			fi.cbSize = Convert.ToUInt32(Marshal.SizeOf(fi));
			fi.hwnd = handle;
			fi.dwFlags = flags;
			fi.uCount = count;
			fi.dwTimeout = timeout;
			return fi;
		}

		/// <summary>
		/// Flash the specified Window (form) for the specified number of times
		/// </summary>
		/// <param name="hwnd">The handle of the Window to Flash.</param>
		/// <param name="count">The number of times to Flash.</param>
		/// <returns></returns>
		public static bool Flash(IntPtr hwnd, uint count)
		{
			if (Win2000OrLater)
			{
				FLASHWINFO fi = CreateFlashInfoStruct(hwnd, mFlashFlags, count, 0);

				return FlashWindowEx(ref fi);
			}            

			return false;
		}

		/// <summary>
		/// A boolean value indicating whether the application is running on Windows 2000 or later.
		/// </summary>
		private static bool Win2000OrLater
		{
			get { return Environment.OSVersion.Version.Major >= 5; }
		}

	}	//class FlashWindowHelper
}	//namespace Desktop.Util

