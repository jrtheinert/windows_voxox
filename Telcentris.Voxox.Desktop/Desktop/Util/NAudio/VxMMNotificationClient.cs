﻿
using Desktop.Model;					//AudioDeviceMgr
using Desktop.Utils;					//Logger

using NAudio.CoreAudioApi;				//Dataflow, Role
using NAudio.CoreAudioApi.Interfaces;	//IMMNotificationClient

using System;
using System.Threading.Tasks;			//Task.Run

// NAudio project" https://naudio.codeplex.com/

//CoreAudioAPI in .NET: https://github.com/hirekoke/CoreAudioApi/tree/master/CoreAudioApi
//	I found this AFTER using NAudio.  It is useful for Jack detection, etc.
//	TODO: Move to CoreAudioAPI completely?

namespace Desktop.Util.NAudio
{
	//NOTE:  You have to be very careful when implementing IMMNotificationClient methods so as to never wait on anything that might cause a deadlock.
	//	Post a message is probably best, or Task
	class VxMMNotificationClient : IMMNotificationClient
	{
        private static VXLogger logger = VXLoggingManager.INSTANCE.GetLogger("VxMMNotificationClient");

		public VxMMNotificationClient()
		{
			if (System.Environment.OSVersion.Version.Major < 6)
			{
				throw new NotSupportedException("This functionality is only supported on Windows Vista or newer.");
			}
		}

		//Works as expected.
		public void OnDefaultDeviceChanged( DataFlow dataFlow, Role deviceRole, string defaultDeviceId )
		{
			//Default Device:				 DeviceRole is 'Console' and 'Multimedia'  (we get two events)
			//Default Communications device: DeviceRole is 'Communications'
			//I am unclear as to what the differences are.
			logIt( String.Format( "OnDefaultDeviceChanged - Default DeviceId: {0}, DeviceType: {1}, DeviceRole: {2}", defaultDeviceId, dataFlow.ToString(), deviceRole.ToString() ) );

			HandleDeviceChange();
		}

		//NOTE: This handles audio ADAPTER being removed from system, NOT an audio endpoint device
		public void OnDeviceAdded(string deviceId)
		{
			logIt( "OnDeviceAdded" );

			HandleDeviceChange();
		}

		//NOTE: This handles audio ADAPTER being removed from system, NOT an audio endpoint device
		public void OnDeviceRemoved(string deviceId)
		{
			logIt( "OnDeviceRemoved" );

			HandleDeviceChange();
		}

		//TODO: I do NOT get this event for my RealTek Audio.  I expect it when I plug/unplug a headset, etc.
		//I do get this for USB devices, but not 3.5mm mini-plug devices.
		public void OnDeviceStateChanged(string deviceId, DeviceState newState)
		{
			logIt( String.Format( "OnDeviceStateChanged - Device Id: {0}, Device State: {1}", deviceId, newState ) );

			HandleDeviceChange();
		}

		//NOTE: For some unknown reason, if we set a breakpoint here, the app locks up WITHOUT hitting the breakpoint.
		//	Must use Task Manager to force stop Desktop.exe.
		//	This happens even if the method is empty.
		//
		//For RealTek audio:
		//	We get here when device is plugged, but not when it is unplugged.
		//	We also do NOT get the OnDeviceAdded, OnDeviceRemoved, or DeviceStateChanged events.
		//
		//For USB devices, we get about 17 of these messages, so let's not log them all, unless we do something with them.
		//
		//I get this ONCE on registering for events.
		public void OnPropertyValueChanged( string deviceId, PropertyKey propertyKey)
		{
			logIt( String.Format( "OnPropertyValueChanged:  deviceId: {0}, fmtid: {1}, PropID: {2}", deviceId, propertyKey.formatId.ToString(), propertyKey.propertyId.ToString() ) );
		}

		private void logIt( String msg )
		{
			logger.Debug( msg );
		}

		private void HandleDeviceChange()
		{
			Task.Run( () => 
			{ 
				AudioDeviceMgr.Instance.HandleDeviceChange();
			});
		}
	}
}
