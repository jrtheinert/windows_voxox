﻿/*
  LICENSE
  -------
  Copyright (C) 2007 Ray Molenkamp

  This source code is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this source code or the software it produces.

  Permission is granted to anyone to use this source code for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this source code must not be misrepresented; you must not
     claim that you wrote the original source code.  If you use this source code
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original source code.
  3. This notice may not be removed or altered from any source distribution.
*/
// modified for NAudio
// milligan22963 - updated to include audio session manager

//NOTE: For Voxox, I commented out portions we do not need for our limited requirments.

using NAudio.CoreAudioApi.Interfaces;
using Desktop.Util.NAudio;

using System;
using System.Runtime.InteropServices;

namespace NAudio.CoreAudioApi
{
    /// <summary>
    /// MM Device
    /// </summary>
    public class MMDevice
    {
        #region Variables
        private readonly IMMDevice deviceInterface;
        private PropertyStore propertyStore;
//        private AudioMeterInformation audioMeterInformation;
//        private AudioEndpointVolume audioEndpointVolume;
//        private AudioSessionManager audioSessionManager;
        #endregion

        #region Guids
//		private static Guid IID_IAudioMeterInformation	= new Guid("C02216F6-8C67-4B5B-9D00-D008E73E0064");
//		private static Guid IID_IAudioEndpointVolume	= new Guid("5CDF2C82-841E-4546-9722-0CF74078229A");
//		private static Guid IID_IAudioClient			= new Guid("1CB9AD4C-DBFA-4c32-B178-C2F568A703B2");
//		private static Guid IDD_IAudioSessionManager	= new Guid("BFA971F1-4D5E-40BB-935E-967039BFBEE4");

		//VOXOX
		private static Guid IID_IDeviceTopology			= new Guid("2A07407E-6497-4A18-9787-32F79BD0D98F");
//		private static Guid IID_IConnector				= new Guid("9C2C4058-23F5-41DE-877A-DF3AF236A09E");
//		private static Guid IID_IPart					= new Guid("6DAA848C-5EB0-45CC-AEA5-998A2CDA1FFB");
		private static Guid IID_IKsJackDescription2		= new Guid("478F3A9B-E0C9-4827-9228-6F5505FFE76A");
        #endregion

		#region COM values
		private static int   S_OK			= 0;
		private static uint  E_NOINTERFACE	= 0x80004002;
		#endregion

        #region Init
        private void GetPropertyInformation()
        {
            IPropertyStore propstore;
            Marshal.ThrowExceptionForHR(deviceInterface.OpenPropertyStore(StorageAccessMode.Read, out propstore));
            propertyStore = new PropertyStore(propstore);
        }

		//private AudioClient GetAudioClient()
		//{
		//	object result;
		//	Marshal.ThrowExceptionForHR(deviceInterface.Activate(ref IID_IAudioClient, ClsCtx.ALL, IntPtr.Zero, out result));
		//	return new AudioClient(result as IAudioClient);
		//}

		//private void GetAudioMeterInformation()
		//{
		//	object result;
		//	Marshal.ThrowExceptionForHR(deviceInterface.Activate(ref IID_IAudioMeterInformation, ClsCtx.ALL, IntPtr.Zero, out result));
		//	audioMeterInformation = new AudioMeterInformation(result as IAudioMeterInformation);
		//}

		//private void GetAudioEndpointVolume()
		//{
		//	object result;
		//	Marshal.ThrowExceptionForHR(deviceInterface.Activate(ref IID_IAudioEndpointVolume, ClsCtx.ALL, IntPtr.Zero, out result));
		//	audioEndpointVolume = new AudioEndpointVolume(result as IAudioEndpointVolume);
		//}

		//private void GetAudioSessionManager()
		//{
		//	object result;
		//	Marshal.ThrowExceptionForHR(deviceInterface.Activate(ref IDD_IAudioSessionManager, ClsCtx.ALL, IntPtr.Zero, out result));
		//	audioSessionManager = new AudioSessionManager(result as IAudioSessionManager);
		//}
        #endregion

        #region Properties

		///// <summary>
		///// Audio Client
		///// </summary>
		//public AudioClient AudioClient
		//{
		//	get
		//	{
		//		// now makes a new one each call to allow caller to manage when to dispose
		//		// n.b. should probably not be a property anymore
		//		return GetAudioClient();
		//	}
		//}

		///// <summary>
		///// Audio Meter Information
		///// </summary>
		//public AudioMeterInformation AudioMeterInformation
		//{
		//	get
		//	{
		//		if (audioMeterInformation == null)
		//			GetAudioMeterInformation();

		//		return audioMeterInformation;
		//	}
		//}

		///// <summary>
		///// Audio Endpoint Volume
		///// </summary>
		//public AudioEndpointVolume AudioEndpointVolume
		//{
		//	get
		//	{
		//		if (audioEndpointVolume == null)
		//			GetAudioEndpointVolume();

		//		return audioEndpointVolume;
		//	}
		//}

		///// <summary>
		///// AudioSessionManager instance
		///// </summary>
		//public AudioSessionManager AudioSessionManager
		//{
		//	get
		//	{
		//		if (audioSessionManager == null)
		//		{
		//			GetAudioSessionManager();
		//		}
		//		return audioSessionManager;
		//	}
		//}

        /// <summary>
        /// Properties
        /// </summary>
        public PropertyStore Properties
        {
            get
            {
                if (propertyStore == null)
                    GetPropertyInformation();
                return propertyStore;
            }
        }

        /// <summary>
        /// Friendly name for the endpoint
        /// </summary>
        public string FriendlyName
        {
            get
            {
                if (propertyStore == null)
                {
                    GetPropertyInformation();
                }
                if (propertyStore.Contains(PropertyKeys.PKEY_Device_FriendlyName))
                {
                    return (string)propertyStore[PropertyKeys.PKEY_Device_FriendlyName].Value;
                }
                else
				{ 
                    return "Unknown";
				}
            }
        }

       /// <summary>
       /// Friendly name of device
       /// </summary>
        public string DeviceFriendlyName
        {
            get
            {
                if (propertyStore == null)
                {
                    GetPropertyInformation();
                }
                if (propertyStore.Contains(PropertyKeys.PKEY_DeviceInterface_FriendlyName))
                {
                    return (string)propertyStore[PropertyKeys.PKEY_DeviceInterface_FriendlyName].Value;
                }
                else
                {
                    return "Unknown";
                }
            }
        }

        /// <summary>
        /// Icon path of device
        /// </summary>
        public string IconPath
        {
            get
            {
                if (propertyStore == null)
                {
                    GetPropertyInformation();
                }
                if (propertyStore.Contains(PropertyKeys.PKEY_Device_IconPath))
                {
                    return (string)propertyStore[PropertyKeys.PKEY_Device_IconPath].Value;
                }
                else
				{ 
                    return "Unknown";
				}
            }
        }

        /// <summary>
        /// Device ID
        /// </summary>
        public string ID
        {
            get
            {
                string result;
                Marshal.ThrowExceptionForHR(deviceInterface.GetId(out result));
                return result;
            }
        }

        /// <summary>
        /// Data Flow
        /// </summary>
        public DataFlow DataFlow
        {
            get
            {
                DataFlow result;
                var ep = deviceInterface as IMMEndpoint;
                ep.GetDataFlow(out result);
                return result;
            }
        }

        /// <summary>
        /// Device State
        /// </summary>
        public DeviceState State
        {
            get
            {
                DeviceState result;
                Marshal.ThrowExceptionForHR(deviceInterface.GetState(out result));
                return result;
            }
        }

		//VoxOx
		public string JackDetectionMsg { get; set; }

		public bool SupportsJackDetection
		{
			get { return CheckJackDetection(); }
		}

        #endregion

        #region Constructor
        internal MMDevice(IMMDevice realDevice)
        {
            deviceInterface = realDevice;
        }
        #endregion

        /// <summary>
        /// To string
        /// </summary>
        public override string ToString()
        {
            return FriendlyName;
        }

		//VOXOX
		private bool Succeeded( int hr )
		{
			return hr >= S_OK;
		}

		private bool Failed( int hr )
		{
			return hr < S_OK;
		}

		private bool CheckJackDetection()
		{
			JackDetectionMsg = "<checking>";

			bool result1 = false;
			int	 hr		 = 0;

			object iDeviceTopologyObj;
			Marshal.ThrowExceptionForHR( deviceInterface.Activate(ref IID_IDeviceTopology, ClsCtx.ALL, IntPtr.Zero, out iDeviceTopologyObj) );

			if ( iDeviceTopologyObj != null )
			{
				IConnector iConnector = null;

				IDeviceTopology iDeviceTopology = (IDeviceTopology)iDeviceTopologyObj;

				hr = iDeviceTopology.GetConnector( 0, out iConnector );

				if ( Succeeded( hr ) )
				{
					IConnector iConnector2 = null;

					hr = iConnector.GetConnectedTo( out iConnector2 );

					if ( Succeeded( hr ) )
					{
						IPart iPart = (IPart)iConnector2;

						//TODO: Somewhere in here, check the busType(?).  If USB, then return 'true'.
						if ( iPart != null ) 
						{
							object tempObj = null;

							UInt32 context = (UInt32)ClsCtx.ALL;
							hr = iPart.Activate( context, ref IID_IKsJackDescription2, out tempObj );
							
							if (E_NOINTERFACE == hr) 
							{
								String msg = "IKsJackDescription2 not supported";
								JackDetectionMsg = msg;
								return false;
							} 
							else if ( Failed( hr ) ) 
							{
								String msg = String.Format( "IPart::Activate(IKsJackDescription2) failed: hr = 0x{0:X8}", hr );
								JackDetectionMsg = msg;
								return false;
							}
							else if ( tempObj == null )
							{ 
								String msg = "IPart.Activate() returned NULL object";
								JackDetectionMsg = msg;
								return false;
							}
							else 
							{ 
								IKsJackDescription2 iKsJackDescription2 = (IKsJackDescription2) tempObj;
								uint jacks = 0;
								hr = iKsJackDescription2.GetJackCount( out jacks );

								if ( Succeeded( hr ) ) 
								{
									if ( 0 == jacks ) 
									{
										String msg = "IKsJackDescription2 says there are no jacks";
										JackDetectionMsg = msg;
										return false;
									}

									DeviceTopology.KSJACK_DESCRIPTION2 description;// = {};
									hr = iKsJackDescription2.GetJackDescription2( 0, out description );
		
									if ( Succeeded( hr ) ) 
									{
										result1 = (description.JackCapabilities & DeviceTopology.JACKDESC2_PRESENCE_DETECT_CAPABILITY) != 0;
										JackDetectionMsg = "SupportsJackDetection property is valid.";
									}
									else
									{
										String msg = String.Format( "IKsJackDescription2::GetJackDescription2 failed: hr = 0x{0:X8}", hr );
										JackDetectionMsg = msg;
										return false;
									}
								}
								else
								{
									String msg = String.Format( "IKsJackDescription2::GetJackCount failed: hr =0x{0:X8}", hr );
									JackDetectionMsg = msg;
									return false;
								}
							}
						}
						else
						{
							String msg = String.Format( "IConnector did not cast to IPart" );
							JackDetectionMsg = msg;
						}
					}
					else
					{
						String msg = String.Format( "IConnector::GetConnectedTo failed: hr = 0x{0:X8}", hr );
						JackDetectionMsg = msg;
					}
				}
				else
				{
					String msg = String.Format( "IDeviceTopology::GetConnector(0) failed: hr = 0x{0:X8}", hr );
					JackDetectionMsg = msg;
				}
			}

			return result1;
		}
	}
}
