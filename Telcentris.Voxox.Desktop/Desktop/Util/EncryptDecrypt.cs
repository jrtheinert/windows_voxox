﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Utils;				//Logger

using System;				
using System.Security.Cryptography;	//AesManaged, ICryptoTransfomr
using System.Text;					//Encoding, UTF8Encoding

namespace Desktop.Util
{
    /// <summary>
    /// The class handles the Encryption/Decryption of local data to be stored on end users system.
    /// AES encryption is used (https://msdn.microsoft.com/en-us/library/system.security.cryptography.aescryptoserviceprovider(v=vs.110).aspx)
    /// </summary>
    internal sealed class EncryptDecrypt
    {
        #region | Variables |

        private static VXLogger logger = VXLoggingManager.INSTANCE.GetLogger("EncryptDecrypt");
        private Encoding utf = new UTF8Encoding();
        private byte[] key;
        private byte[] iv;
        private const string format = "{0}|{1}|VX"; // The data is first formatted (salted) further before encrypting to avoid dictionary attack
        private const char pipe = '|';

        #endregion

        #region | Constructor |

        private EncryptDecrypt()
        {
            Init();
        }

        #endregion

        #region | Singleton |

        private static EncryptDecrypt Instance
        {
            get { return Nested.instance; }
        }

        class Nested
        {
            // Explicit static constructor to tell C# compiler
            // not to mark type as beforefieldinit
            static Nested()
            {
            }

            internal static readonly EncryptDecrypt instance = new EncryptDecrypt();
        }

        #endregion

        #region | Private Procedures |

        private void Init()
        {
            // The key length must be 16 (128 bit), 24 (192 bits) or 32 (256 bit) characters 
            string initPhrase = "MAKVTELCENTRIS20150203VX";

            // Create a scrambled Key & IV from the initial phrase
            // The initial key is split into 2 parts and then the pass key is constructed with joining the each parts reversed.
            // The IV is created by reversing the derived pass key
            string part1 = initPhrase.Substring(0, initPhrase.Length / 2);
            string part2 = initPhrase.Substring(initPhrase.Length / 2);
            string passPhrase = Reverse(part1) + Reverse(part2);
            string ivpassPhrase = Reverse(passPhrase);
            ivpassPhrase = ivpassPhrase.Length > 16 ? ivpassPhrase.Substring(0, 16) : ivpassPhrase; // IV has to be 128 bits for AES

            key = utf.GetBytes(passPhrase);
            iv = utf.GetBytes(ivpassPhrase);
        }

        private string Reverse(string value)
        {
            if (string.IsNullOrEmpty(value))
                return value;

            var array = value.ToCharArray();
            Array.Reverse(array);

            return new string(array);
        }

        private bool EncryptImpl(string data, out string encryptedData)
        {
            encryptedData = string.Empty;

            if (string.IsNullOrEmpty(data))
                return false;

            if (data.Trim().Length == 0)
                return false;

            using (AesManaged aes = new AesManaged())
            {
                using (ICryptoTransform encrypt = aes.CreateEncryptor(key, iv))
                {
                    try
                    {
                        // The string to be encrypted is created by 3 parts, the first being data itself, 
                        // the second part being the data reversed and the third part a constant (currently VX), all seperated by a pipe.
                        // e.g., Password => Password|drowssaP|VX
                        string encryptData = string.Format(format, data, Reverse(data));
                        byte[] cipher = encrypt.TransformFinalBlock(utf.GetBytes(encryptData), 0, utf.GetByteCount(encryptData));

                        encryptedData = Convert.ToBase64String(cipher);
                        return true;
                    }
                    catch (Exception ex)
                    {
                        logger.Error("EncryptImpl", ex);
                        return false;
                    }
                }
            }
        }

        private bool DecryptImpl(string data, out string decryptedData)
        {
            decryptedData = string.Empty;

            if (string.IsNullOrEmpty(data))
                return false;

            if (data.Trim().Length == 0)
                return false;

            using (AesManaged aes = new AesManaged())
            {
                using (ICryptoTransform decrypt = aes.CreateDecryptor(key, iv))
                {
                    try
                    {
                        byte[] cipher = Convert.FromBase64String(data);
                        byte[] decoded = decrypt.TransformFinalBlock(cipher, 0, cipher.Length);

                        string result = utf.GetString(decoded, 0, decoded.Length);

                        if (result.Contains(pipe.ToString()) == false)
                            return false;

                        string[] parts = result.Split(new char[] { pipe });

                        if (parts == null || parts.Length != 3) // Must contain 3 parts
                            return false;

                        if (Reverse(parts[0]) == parts[1] && parts[2] == "VX") // Check if decryption was successful
                        {
                            decryptedData = parts[0];
                            return true;
                        }

                        return false;
                    }
                    catch (Exception ex)
                    {
                        logger.Error("DecryptImpl", ex);
                        return false;
                    }
                }
            }
        }

        #endregion

        /// <summary>
        /// Encrypts the data. If the return value is true then encryptedData contains the encrypted data.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="encryptedData"></param>
        /// <returns></returns>
        public static bool Encrypt(string data, out string encryptedData)
        {
            return Instance.EncryptImpl(data, out encryptedData);
        }

        /// <summary>
        /// Decrypts the data. If the return value is true then decryptedData contains the decrypted data.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="decryptedData"></param>
        /// <returns></returns>
        public static bool Decrypt(string data, out string decryptedData)
        {
            return Instance.DecryptImpl(data, out decryptedData);
        }
    }
}
