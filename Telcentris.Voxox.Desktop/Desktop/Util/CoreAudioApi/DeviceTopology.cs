﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Desktop.Util.NAudio		//Not really.  Added by VoxOx based on a separate resource
{
	public class DeviceTopology
	{
		public enum ConnectorType 
		{
			Unknown_Connector	= 0,
			Physical_Internal	= 1,
			Physical_External	= 2,
			Software_IO			= 3,
			Software_Fixed		= 4,
			Network				= 5
		};

		public enum DataFlow 
		{
			In	= 0,
			Out = 1 
		};

		public enum PartType 
		{
			Connector	= 0,
			Subunit		= 1 
		};

		/* NOTE: EChannelMapping enumeration is obsoleted in Windows/7:
		* SPEAKER_FRONT_LEFT and friends from ksmedia.h are to
		* be used as KSJACK_DESCRIPTION->ChannelMapping member
		* values who also changed to a DWORD. See:
		* http://msdn.microsoft.com/en-us/library/dd316543(VS.85).aspx
		*/
		//public enum EChannelMapping 
		//{
		//	ePcxChanMap_FL_FR = 0,
		//	ePcxChanMap_FC_LFE,
		//	ePcxChanMap_BL_BR,
		//	ePcxChanMap_FLC_FRC,
		//	ePcxChanMap_SL_SR,
		//	ePcxChanMap_Unknown
		//};

		//public enum EPcxConnectionType 
		//{
		//	eConnTypeUnknown = 0,
		//	eConnTypeEighth,
		//	eConnType3Point5mm = eConnTypeEighth,
		//	eConnTypeQuarter,
		//	eConnTypeAtapiInternal,
		//	eConnTypeRCA,
		//	eConnTypeOptical,
		//	eConnTypeOtherDigital,
		//	eConnTypeOtherAnalog,
		//	eConnTypeMultichannelAnalogDIN,
		//	eConnTypeXlrProfessional,
		//	eConnTypeRJ11Modem,
		//	eConnTypeCombination
		//};

		//public enum EPcxGeoLocation 
		//{
		//	eGeoLocRear = 0,
		//	eGeoLocFront,
		//	eGeoLocLeft,
		//	eGeoLocRight,
		//	eGeoLocTop,
		//	eGeoLocBottom,
		//	eGeoLocRearOPanel,
		//	eGeoLocRearPanel = eGeoLocRearOPanel,
		//	eGeoLocRiser,
		//	eGeoLocInsideMobileLid,
		//	eGeoLocDrivebay,
		//	eGeoLocHDMI,
		//	eGeoLocOutsideMobileLid,
		//	eGeoLocATAPI
		//};

		//public enum EPcxGenLocation 
		//{
		//	eGenLocPrimaryBox = 0,
		//	eGenLocInternal,
		//	eGenLocSeperate,
		//	eGenLocSeparate = eGenLocSeperate,
		//	eGenLocOther
		//};

		public enum EPxcPortConnection 
		{
			ePortConnJack = 0,
			ePortConnIntegratedDevice,
			ePortConnBothIntegratedAndJack,
			ePortConnUnknown
		};

		//public struct KSJACK_DESCRIPTION 
		//{
		//	EChannelMapping    ChannelMapping; /* see note up above for EChannelMapping */
		//	COLORREF           Color;
		//	EPcxConnectionType ConnectionType;
		//	EPcxGeoLocation    GeoLocation;
		//	EPcxGenLocation    GenLocation;
		//	EPxcPortConnection PortConnection;
		//	Boolean            IsConnected;
		//};

		//private static int SPEAKER_FRONT_LEFT				=	0x1;
		//private static int SPEAKER_FRONT_RIGHT				=	0x2;
		//private static int SPEAKER_FRONT_CENTER				=	0x4;
		//private static int SPEAKER_LOW_FREQUENCY			=	0x8;
		//private static int SPEAKER_BACK_LEFT				=	0x10;
		//private static int SPEAKER_BACK_RIGHT				=	0x20;
		//private static int SPEAKER_FRONT_LEFT_OF_CENTER		=	0x40;
		//private static int SPEAKER_FRONT_RIGHT_OF_CENTER	=	0x80;
		//private static int SPEAKER_BACK_CENTER				=	0x100;
		//private static int SPEAKER_SIDE_LEFT				=	0x200;
		//private static int SPEAKER_SIDE_RIGHT				=	0x400;
		//private static int SPEAKER_TOP_CENTER				=	0x800;
		//private static int SPEAKER_TOP_FRONT_LEFT			=	0x1000;
		//private static int SPEAKER_TOP_FRONT_CENTER			=	0x2000;
		//private static int SPEAKER_TOP_FRONT_RIGHT			=	0x4000;
		//private static int SPEAKER_TOP_BACK_LEFT			=	0x8000;
		//private static int SPEAKER_TOP_BACK_CENTER			=	0x10000;
		//private static int SPEAKER_TOP_BACK_RIGHT			=	0x20000;

		public enum KSJACK_SINK_CONNECTIONTYPE 
		{
			KSJACK_SINK_CONNECTIONTYPE_HDMI          = 0,
			KSJACK_SINK_CONNECTIONTYPE_DISPLAYPORT   = 1 
		};

		public struct KSJACK_DESCRIPTION2 
		{
			public UInt32 DeviceStateInfo;
			public UInt32 JackCapabilities;
		};

		public static UInt32 JACKDESC2_PRESENCE_DETECT_CAPABILITY		 = 0x00000001;
		public static UInt32 JACKDESC2_DYNAMIC_FORMAT_CHANGE_CAPABILITY = 0x00000002;

//		private static Int32  MAX_SINK_DESCRIPTION_NAME_LENGTH = 32;

		public struct LUID 
		{
			public UInt32 LowPart;
			public Int32  HighPart;
		};

//		public struct KSJACK_SINK_INFORMATION 
//		{
//			KSJACK_SINK_CONNECTIONTYPE	ConnType;
//			UInt16						ManufacturerId;
//			UInt16						ProductId;
//			UInt16						AudioLatency;
//			Boolean						HDCPCapable;		//May be 'byte;
//			Boolean						AICapable;			//May be 'byte;
//			Byte						SinkDescriptionLength;
////			fixed Char					SinkDescription[MAX_SINK_DESCRIPTION_NAME_LENGTH];
//			Char						SinkDescription01;
//			Char						SinkDescription02;
//			Char						SinkDescription03;
//			Char						SinkDescription04;
//			Char						SinkDescription05;
//			Char						SinkDescription06;
//			Char						SinkDescription07;
//			Char						SinkDescription08;
//			Char						SinkDescription09;
//			Char						SinkDescription10;
//			Char						SinkDescription11;
//			Char						SinkDescription12;
//			Char						SinkDescription13;
//			Char						SinkDescription14;
//			Char						SinkDescription15;
//			Char						SinkDescription16;
//			Char						SinkDescription17;
//			Char						SinkDescription18;
//			Char						SinkDescription19;
//			Char						SinkDescription20;
//			Char						SinkDescription21;
//			Char						SinkDescription22;
//			Char						SinkDescription23;
//			Char						SinkDescription24;
//			Char						SinkDescription25;
//			Char						SinkDescription26;
//			Char						SinkDescription27;
//			Char						SinkDescription28;
//			Char						SinkDescription29;
//			Char						SinkDescription30;
//			Char						SinkDescription31;
//			Char						SinkDescription32;
//			LUID						PortId;
//		};
	}
}
