﻿using System;
using System.Runtime.InteropServices;

namespace NAudio.CoreAudioApi.Interfaces	//Actually VoxOx
{
    /// <summary>
	/// Represents a hardware subunit (for example, a volume control) that lies in the data path between a client and an audio endpoint device.
    /// </summary>
    /// <remarks>
	/// MSDN Reference: http://msdn.microsoft.com/en-us/library/dd316540.aspx
    /// </remarks>
    [Guid("82149A85-DBA6-4487-86BB-EA8F7FEFCC71"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	partial interface ISubunit
    {
		// The ISubunit interface inherits from the IUnknown interface but does not define additional methods or properties.
    }
}
