﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using System;

namespace Desktop.Util
{
    /// <summary>
    /// This class handles all the Date Time related conversion and formatting.
    /// </summary>
    public class TimeUtils
    {
        public static string GetDateWithFormatting(DateTime date)		//CallListViewModel, FaxListViewModel, MessageListViewModel
        {
            var timeSpan = DateTime.Today - new DateTime(date.Year, date.Month, date.Day);

            if (timeSpan < TimeSpan.FromDays(1))
                return "Today";							//TODO-LOCALIZE

            if (timeSpan < TimeSpan.FromDays(2))
                return "Yesterday";						//TODO-LOCALIZE

            if (timeSpan > TimeSpan.FromDays(6))
                return date.ToString("M/d/y");

            return date.DayOfWeek.ToString();
        }

        public static string GetOnlyFormattedDate(DateTime date)		//CallHistoryViewModel
        {
            return date.ToString("yyyyMMdd");
        }

        public static string GetDateWithLongFormatting(DateTime date)	//CallDetailViewModel, CallHistoryViewModel
        {
            return date.ToString("hh:mm tt MMMM dd");
        }

        public static string FormatDuration(int durationSeconds)		//CallDetailViewModel, CallHistoryViewModel, CallListViewModel
        {
            return FormatTimeSpan(new TimeSpan(0, 0, 0, durationSeconds));
        }

        public static string FormatTimeSpan(TimeSpan ts)				//IncomingCallViewModel, CallsControls.xaml.cs
        {
            if (ts.TotalHours >= 1)
            {
                return ts.ToString(@"h\:mm\:ss");
            }

            return ts.ToString(@"m\:ss");
        }
    }
}
