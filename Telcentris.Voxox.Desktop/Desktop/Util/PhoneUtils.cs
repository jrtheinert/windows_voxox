﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;					//SettingsManager
using Desktop.Utils;					//Logger
using PhoneNumbers;						//PhoneNumber, PhoneNumberUtil, NumberParseException (NOT VOXOX lib)

using System;
//using System.Text;
using System.Text.RegularExpressions;	//RegEx

namespace Desktop.Util
{
    public class PhoneUtils
    {
        #region | Private Class Variables |

        private static VXLogger			logger				= VXLoggingManager.INSTANCE.GetLogger("PhoneUtils");
        private static PhoneNumberUtil	phoneUtil			= PhoneNumberUtil.GetInstance();
		private static Regex			s_numberRegEx		= new Regex(@"[a-zA-Z0-9]");

		private static int				MIN_EXTENSION_LEN	= 3;
		private static int				MAX_EXTENSION_LEN	= 6;

		private const int				NUMBER_911_LENGTH	= 3;
		private const String			s_specialChars		= "().-*#";	//Because I suck at RegEx.
		private const String			s_intlPrefix		= "011";

		private const String			s_keyCharsIn		= "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		private const String			s_keyCharsOut		= "22233344455566677778889999";

		private const String			s_KeysWithTones     = "1234567890*#";

        #endregion

        #region | Public Procedures |

        public static bool IsValidNumber(string phoneNumber)
        {
            if (string.IsNullOrEmpty(phoneNumber) || phoneNumber.Length == 0)
                return false;

            string searchText = phoneNumber;
            string number = CleanPhoneNumberFormatting(searchText).Replace("+", "");

            foreach (char ch in number.ToCharArray())
            {
                if (!char.IsDigit(ch))
                {
                    return false;
                }
            }

            return true;
        }

        public static string GetCountryCodeByPhone(string phoneNumber)
        {
            try
            {
                if (phoneNumber.StartsWith("+"))
                {
                    PhoneNumber parsedPhone = phoneUtil.ParseAndKeepRawInput(phoneNumber, string.Empty);

                    if (parsedPhone.HasCountryCode)
                    {
                        //String region = phoneUtil.GetRegionCodeForNumber(parsedPhone);

                        return string.Format("+{0}", parsedPhone.CountryCode);
                    }
                }
            }
            catch (NumberParseException ex)
            {
                logger.Debug("GetCountryCodeByPhone: " + phoneNumber, ex);

                return "";
            }
            catch (Exception ex)
            {
                logger.Error("GetCountryCodeByPhone: " + phoneNumber, ex);
            }

            return "";
        }

		//This is used so we can return proper formatting.
		private static bool HasCountryCode( string newNumberIn, string origNumberIn )
		{
			bool result = true;

			//This logic fails if origNumber has country code, but not the +.
			//try
			//{ 
			//	PhoneNumber parsedPhone  = phoneUtil.ParseAndKeepRawInput( origNumber, string.Empty);
			//}
			//catch (NumberParseException ex)
			//{
			//	result = ( ex.ErrorType != ErrorType.INVALID_COUNTRY_CODE );
			//}

			string newNumber  = newNumberIn.Replace ( "+", "" );
			string origNumber = origNumberIn.Replace( "+", "" );

			result = (newNumber == origNumber);

			return result;
		}

        public static string FormatPhoneNumber( string phoneNumber )
        {
            try
            {
                if ( !IsValidNumber(phoneNumber) )
                {
                    return phoneNumber;
                }

                if ( IsValidExtension(phoneNumber) )
                {
                    return phoneNumber;
                }

                string number         = GetPhoneNumberForCall( phoneNumber, false );	//This will add country code, if it did not already exist.
				bool   hasCountryCode = HasCountryCode( phoneNumber, number );			//So we can format number for International or National format, based on how user entered it in AB.
				//string number2 = "";
				//string number3 = "";
				//string number4 = "";

                PhoneNumber parsedPhone = phoneUtil.ParseAndKeepRawInput(number, string.Empty);

                if (parsedPhone.HasCountryCode)
                {
//					number  = phoneUtil.Format( parsedPhone, PhoneNumberFormat.INTERNATIONAL );	//+1 909-600-2538
//					number2 = phoneUtil.Format( parsedPhone, PhoneNumberFormat.NATIONAL      );	//(909) 600-2538
//					number3 = phoneUtil.Format( parsedPhone, PhoneNumberFormat.E164		     );	//+19096002538
//					number4 = phoneUtil.Format( parsedPhone, PhoneNumberFormat.RFC3966	     );	//tel:+1-909-600-2538
				
					//Select format based on whether or not country existed in original number.
					PhoneNumbers.PhoneNumberFormat format = ( hasCountryCode ? PhoneNumberFormat.INTERNATIONAL : PhoneNumberFormat.NATIONAL );
					number  = phoneUtil.Format( parsedPhone, format );

					return number;
                }
            }
            catch (NumberParseException ex)
            {
                logger.Debug("FormatPhoneNumber: " + phoneNumber, ex);

                return phoneNumber;
            }
            catch (Exception ex)
            {
                logger.Error("FormatPhoneNumber: " + phoneNumber, ex);
            }

            return phoneNumber;
        }

        public static string GetPhoneNumberForFax(string phoneNumber)
        {
            try
            {
                return CleanPhoneNumberFormatting(FormatPhoneNumber(phoneNumber)).Replace("+", "");
            }
            catch (Exception ex)
            {
                logger.Error("GetPhoneNumberForFax", ex);
            }

            return phoneNumber;
        }

        public static string GetPhoneNumberForCall( string phoneNumber, bool isExtension )
        {
			string callablePhoneNumber = CleanPhoneNumberFormatting( phoneNumber );

			if ( isExtension )
			{
				if ( !IsValidExtension( callablePhoneNumber ) )
				{
                    logger.Debug(string.Format("GetPhoneNumberForCall - IsValidExtension failed: " + callablePhoneNumber) );
				}

				return callablePhoneNumber;
			}

            try
            {

                // If number starts with "+" return as is
                if (callablePhoneNumber.StartsWith("+"))
                    return callablePhoneNumber;

                // Try parsing the number by appending user country code to phonenumber
                try
                {
                    PhoneNumber parsedNumber = phoneUtil.ParseAndKeepRawInput(SettingsManager.Instance.User.CountryCode + callablePhoneNumber, string.Empty);

                    if (phoneUtil.IsValidNumber(parsedNumber))
                    {
                        return phoneUtil.Format(parsedNumber, PhoneNumberFormat.E164);
                    }
                }
                catch (NumberParseException nex)
                {
                    logger.Debug(string.Format("GetPhoneNumberForCall - Append Country Code - {0} :: {1}", SettingsManager.Instance.User.CountryCode, callablePhoneNumber), nex);
                }

                // Try parsing the number as it is using User Country region
                try
                {
                    PhoneNumber parsedNumber = phoneUtil.ParseAndKeepRawInput(callablePhoneNumber, SettingsManager.Instance.User.CountryAbbreviation);

                    if (phoneUtil.IsValidNumber(parsedNumber))
                    {
                        return phoneUtil.Format(parsedNumber, PhoneNumberFormat.E164);
                    }
                }
                catch( NumberParseException nex)
                {
                    logger.Debug(string.Format("GetPhoneNumberForCall - User Country Region - {0} :: {1}", SettingsManager.Instance.User.CountryAbbreviation, callablePhoneNumber), nex);
                }

                return "+" + callablePhoneNumber; // All failed, return number preappended with "+"
            }
            catch (Exception ex)
            {
                logger.Error("GetPhoneNumberForCall", ex);
            }

            return phoneNumber;
        }

        public static string GetPhoneNumberNoCountry( string phoneNumber, bool isExtension )
        {
			string callablePhoneNumber = CleanPhoneNumberFormatting( phoneNumber );

			if ( isExtension )
			{
				if ( !IsValidExtension( callablePhoneNumber ) )
				{
                    logger.Debug(string.Format("GetPhoneNumberNoCountry - IsValidExtension failed: " + callablePhoneNumber) );
				}

				return callablePhoneNumber;
			}

            try
            {

                // If number starts with "+" return as is
                if (callablePhoneNumber.StartsWith("+"))
                    return callablePhoneNumber;

                // Try parsing the number by appending user country code to phonenumber
                try
                {
                    PhoneNumber parsedNumber = phoneUtil.ParseAndKeepRawInput(SettingsManager.Instance.User.CountryCode + callablePhoneNumber, string.Empty);

                    if (phoneUtil.IsValidNumber(parsedNumber))
                    {
                        return Convert.ToString( parsedNumber.NationalNumber );
                    }
                }
                catch (NumberParseException nex)
                {
                    logger.Debug(string.Format("GetPhoneNumberNoCountry - Append Country Code - {0} :: {1}", SettingsManager.Instance.User.CountryCode, callablePhoneNumber), nex);
                }

                // Try parsing the number as it is using User Country region
                try
                {
                    PhoneNumber parsedNumber = phoneUtil.ParseAndKeepRawInput(callablePhoneNumber, SettingsManager.Instance.User.CountryAbbreviation);

                    if (phoneUtil.IsValidNumber(parsedNumber))
                    {
                        return Convert.ToString( parsedNumber.NationalNumber );
                    }
                }
                catch( NumberParseException nex)
                {
                    logger.Debug(string.Format("GetPhoneNumberNoCountry - User Country Region - {0} :: {1}", SettingsManager.Instance.User.CountryAbbreviation, callablePhoneNumber), nex);
                }

                return callablePhoneNumber; // All failed, return number.
            }
            catch (Exception ex)
            {
                logger.Error("GetPhoneNumberForCall", ex);
            }

            return phoneNumber;
        }

        /// <summary>
        /// Removes any formatting characters from the number. 
        /// Does not remove the "+" sign
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <returns></returns>
        public static string CleanPhoneNumberFormatting(string phoneNumber)
        {
            if(string.IsNullOrWhiteSpace(phoneNumber) == false )
            {
				//This should really remove all invalid chars, not just formatting chars.
				//	Leaving only numeric digits and leading plus.
				//	What about '*' and '#'?
				//	Consider phoneNumber vs. SIP Address.  Not so easy now.
				phoneNumber = phoneNumber.Trim()
										 .Replace("(", "")
										 .Replace(")", "")
										 .Replace("-", "")
										 .Replace(".", "")
										 .Replace(" ", "");
            }

            return phoneNumber;
        }

        public static bool Is911Or411(string number, out string cleanedNumber)
        {
            cleanedNumber = "";

            if (string.IsNullOrWhiteSpace(number))
                return false;

            if (number.Contains("+"))
            {
                string countryCode = PhoneUtils.GetCountryCodeByPhone(number);

                if (countryCode.Length > 0)
                {
                    string numberWithoutCC = number.Substring(countryCode.Length);

                    if (numberWithoutCC.Length == NUMBER_911_LENGTH)
                    {
                        cleanedNumber = numberWithoutCC;
                        return true;
                    }
                    else
                    {
                        cleanedNumber = countryCode + numberWithoutCC;
                        return false;
                    }
                }
            }
            else
            {
                if (number.Length == NUMBER_911_LENGTH)
                {
                    cleanedNumber = number;
                    return true;
                }
                else
                {
                    cleanedNumber = number;
                    return false;
                }
            }

            return false;
        }

		public static bool IsValidExtension( string phoneNumber )
		{
			return ( phoneNumber.Length >= MIN_EXTENSION_LEN && phoneNumber.Length <= MAX_EXTENSION_LEN );
		}

		//This does NOT include extensions.
        public static bool IsValidPhoneNumberLength(string phoneNumber)
        {
			int  len				  = phoneNumber.Length;
			bool startsWithPlus		  = phoneNumber[0] == '+';
			bool startsWithIntlPrefix = phoneNumber.StartsWith( s_intlPrefix );

            if(string.IsNullOrWhiteSpace(phoneNumber))
            {
                return false;
            }

            if ( !startsWithPlus && len == NUMBER_911_LENGTH) // check for 911 or 411
            {
                return true;
            }

            //Validating if phoneNumber value has the correct length of seven or more characters
            if ( ( !startsWithPlus		  && len <  7 ) ||	//i.e. 1234567
                 (  startsWithPlus		  && len <  8 ) ||	//i.e. +1234567
                 (  startsWithIntlPrefix  && len < 10 ) )	//0111234567		
            {   
                return false;
            }

            return true;
        }

		public static bool IsValidChar( string newChar )
		{
			Boolean result = false;

			if ( s_specialChars.Contains( newChar ) )
			{
				result = true;
			}
			else
			{ 
				if  (s_numberRegEx.IsMatch( newChar ) )
				{
					result = true;
				}
			}

			return result;
		}

		public static Char TranslateKey( Char chIn )
		{
			Char ch     = Char.ToUpper( chIn );
			int  index  = s_keyCharsIn.IndexOf( ch );
			Char result = ( index >= 0 ? s_keyCharsOut[index] : ch );

			return result;
		}

		public static Char[] TranslateKey( String textIn )
		{
            char[] numDigits = textIn.ToUpper().ToCharArray();

            for (int ii = 0; ii < numDigits.Length; ii++)
            {
				numDigits[ii] = Util.PhoneUtils.TranslateKey(numDigits[ii]);
            }

			return numDigits;
		}

		public static Boolean KeyPlaysTone( Char key )
		{
			String temp = key.ToString();
			return s_KeysWithTones.Contains( temp );
		}
		
		//Logic to determine best label to use
		public static String BestLabel( ManagedDataTypes.PhoneNumber pn )
		{
			return BestLabel( pn.Label, pn.NumberType );
		}

		public static String BestLabel( String assignedLabel, ManagedDataTypes.PhoneNumber.PnType type )
		{
			String result = assignedLabel;

			if ( String.IsNullOrEmpty( result ) )
			{
				String defaultLabel = LocalizedString.GenericDefaultPhoneLabel;
				String onnetLabel   = Config.Branding.AppName;

				//Try OnnetLabel, then defaultLabel
				result = (type == ManagedDataTypes.PhoneNumber.PnType.DID_NUMBER) ? onnetLabel : defaultLabel;
			}

			return result;
		}

        #endregion
    }
}
