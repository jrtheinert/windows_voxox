﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Desktop.Util.Settings
{
    class Constants
    {
        public const String AUDIO_INPUT_VOLUME = "AudioInputVolume";
        public const String AUDIO_OUTPUT_VOLUME = "AudioOutputVolume";
        public const String AUDIO_INPUT_INDEX = "AudioInputIndex";
        public const String AUDIO_OUTPUT_INDEX = "AudioOutputIndex";
        public const String VOICEMAILS_ENABLED = "VoicemailsEnabled";
        public const String FAXES_ENABLED = "FaxesEnabled";
        public const String RECORDEDCALLS_ENABLED = "RecordedmailsEnabled";
        public const String INCOMINGCHAT_ENABLED = "IncomingChatEnabled";
        public const String INCOMINGCALL_ENABLED = "IncominCallEnabled";
        public const String OUTGOINGCHAT_ENABLED = "OutgoingChatEnabled";
        public const String USERCHATBUBBLECOLORINDEX = "UserChatBubbleColorIndex";
        public const String CONVERSANTCHATBUBBLECOLORINDEX = "ConversantChatBubbleColorIndex";
        public const String USERCHATBUBBLECOLOR = "UserChatBubbleColor";
        public const String CONVERSANTCHATBUBBLECOLOR = "ConversantChatBubbleColor";
        public const String SELECTEDCOUNTRYCODE = "SelectedCountryCode";
        public static string SELECTEDCOUNTRYINDEX = "SelectedCountryIndex";
    }
}
