﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Desktop.Util.Settings
{
    public class Defaults
    {
        /// <summary>
        /// To set the default value for audio input/ output devices
        /// </summary>
        public static int defaultIndex = 0;
        /// <summary>
        /// Default volume for Audio input device
        /// </summary>
        public static int inputVolume = 50;
        /// <summary>
        /// Default value for Audio output device
        /// </summary>
        public static int outputVolume = 70;

        /// <summary>
        /// Default text when input/output Audio devices are not present
        /// </summary>
        public static String defaultText = "None";      //CODE-REVIEW: Does this need to be translated?  IOW, is this displayed to user?

        /// <summary>
        /// Show faxes in messages thread
        /// </summary>
        public static bool faxesEnabled = true;

        /// <summary>
        /// Show voicemails in messages thread
        /// </summary>
        public static bool voicemailsEnabled = true;

        /// <summary>
        /// Show recordedcalls in messages thread
        /// </summary>
        public static bool recordedcallsEnabled = true;

        /// <summary>
        /// Show Incoming chat in messages thread
        /// </summary>
        public static bool incomingChatEnabled = true;

        /// <summary>
        ///  show Incoming calls in message thread
        /// </summary>
        public static bool incomingCallEnabled = true;

        /// <summary>
        /// Show outgoingChat in message thread
        /// </summary>
        public static bool outgoingChatEnabled = true;

        /// <summary>
        /// Phone num to contact voxox
        /// </summary>
        public static string PhoneNo = "866-514-VOIP(8647)";

        /// <summary>
        /// Email to contact voxox
        /// </summary>
        public static string Email = "support@voxox.com";

        /// <summary>
        /// User Chat bubble colorcode
        /// </summary>
        public const string defalut_userchatbubble= "Blue";

        /// <summary> 
        /// Conversant chat bubble color code
        /// </summary>
        public const string defalut_conversantchatbubble= "Gull Gray";

        /// <summary>
        /// Country 
        /// </summary>
        public const string default_countryCode = "+91";

    }
}
