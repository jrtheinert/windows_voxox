﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;				//AutoStartManager, SessionManager, AutoStartChangeMessage
using Desktop.Utils;				//Logger
using Desktop.ViewModels;			//SignOutMessage

using GalaSoft.MvvmLight.Messaging;	//Messenger

using System;						//Uri, EventArgs, Exception
using System.Drawing;				//Icon
using System.Windows;				//Window, Application
using System.Windows.Forms;			//ToolStripMenuItem, NotifyIcon, ContextMenuStrip, etc.

namespace Desktop.Util
{
    internal class SystemTray
    {
        VXLogger			logger = VXLoggingManager.INSTANCE.GetLogger("SystemTray");

        Window				window;
        NotifyIcon			notifyIcon;
        ContextMenuStrip	contextMenu;
        ToolStripMenuItem	ctxMenuSignOut;
		ToolStripMenuItem	ctxMenuAutoStart;

		bool				initialized = false;


        public static SystemTray Instance
        {
            get { return Nested.instance; }
        }

        class Nested
        {
            static Nested()		{}

            internal static readonly SystemTray instance = new SystemTray();
        }

        private SystemTray()
        {
        }

        public void Init()
        {
			if ( initialized )
				return;

            var uri		= new Uri(Config.UI.SystrayIconUri);
            var stream	= System.Windows.Application.GetResourceStream(uri);

            notifyIcon			= new NotifyIcon();
            notifyIcon.Icon		= new Icon(stream.Stream);
            notifyIcon.Visible	= true;

            //Context Menu Strip implementation
            contextMenu				= new ContextMenuStrip();
			contextMenu.Opening		+= contextMenu_Opening;
            contextMenu.RenderMode  = ToolStripRenderMode.System;

			//AutoStart
            ctxMenuAutoStart = new ToolStripMenuItem(LocalizedString.SystemTrayMenu_AutoStart);
            ctxMenuAutoStart.Checked = AutoStartManager.IsInStartup();
            ctxMenuAutoStart.Click += toggleAutoStart;
            contextMenu.Items.Add(ctxMenuAutoStart);

			//OpenApp
            ToolStripMenuItem ctxOpenApp = new ToolStripMenuItem(LocalizedString.SystemTrayMenu_OpenAppName);
            ctxOpenApp.Click += handleShowCurrentWindow;
            contextMenu.Items.Add(ctxOpenApp);

			//Open Preferences
            ToolStripMenuItem ctxMenuPreferences = new ToolStripMenuItem(LocalizedString.SystemTrayMenu_Settings);
            ctxMenuPreferences.ShortcutKeys = ((Keys)((Keys.Control | Keys.Oemcomma)));
            ctxMenuPreferences.ShortcutKeyDisplayString = "Ctrl+,";
            ctxMenuPreferences.Click += (object sender, EventArgs e) => { ActionManager.Instance.NavigateToSettings(); };
            contextMenu.Items.Add(ctxMenuPreferences);

			//Separator
            ToolStripSeparator separator1 = new ToolStripSeparator();
            separator1.Size = new System.Drawing.Size(205, 6);
            contextMenu.Items.Add(separator1);

			//SignOut
            ctxMenuSignOut = new ToolStripMenuItem(LocalizedString.SystemTrayMenu_SignOut);
            ctxMenuSignOut.ShortcutKeys = ((Keys)(((Keys.Control | Keys.Alt) | Keys.Q)));
            ctxMenuSignOut.Enabled = false;
            ctxMenuSignOut.Click += (object sender, EventArgs e) => { ActionManager.Instance.Logout(); };
            contextMenu.Items.Add(ctxMenuSignOut);

			//Quit
            ToolStripMenuItem ctxMenuQuit = new ToolStripMenuItem(LocalizedString.SystemTrayMenu_Quit);
            ctxMenuQuit.ShortcutKeys = ((Keys)((Keys.Control | Keys.Q)));
            ctxMenuQuit.Click += (object sender, EventArgs e) => { ActionManager.Instance.Quit(); };
            contextMenu.Items.Add(ctxMenuQuit);

			//Notify Icon handlers
            notifyIcon.ContextMenuStrip = contextMenu;

            //Handled notify icon double click to show window in normal state
            notifyIcon.DoubleClick += handleShowCurrentWindow;

			initialized = true;
        }


		//Handlers
		private void contextMenu_Opening( object sender, System.ComponentModel.CancelEventArgs e )
		{
            ctxMenuAutoStart.Checked = AutoStartManager.IsInStartup();
		}

		private void handleShowCurrentWindow(object sender, EventArgs e)
		{
			ShowCurrentWindow();
		}

        private void toggleAutoStart( object sender, EventArgs e)
        {
            ctxMenuAutoStart.Checked = !ctxMenuAutoStart.Checked;

            try
            {
                if ( ctxMenuAutoStart.Checked )
                {
                    AutoStartManager.RegisterInStartup();
                }
                else
                {
                    AutoStartManager.RemoveFromStartup();
                }
            }
            catch (Exception ex)
            {
                logger.Error("Register/Unregister - Startup", ex);
            }
        }

        public void ShowCurrentWindow()
        {
            if (window != null)
            {
                window.Show();
                window.WindowState = WindowState.Normal;
                window.Activate();
            }
        }

        public void Dispose()
        {
            window = null;

            if (contextMenu != null)
            {
                contextMenu.Dispose();
                contextMenu = null;
            }

            if (notifyIcon != null)
            {
                notifyIcon.Visible = false;
                notifyIcon.Icon = null;
                notifyIcon.Dispose();
                notifyIcon = null;
            }
        }

        public void Attach( Window win, bool showSignout )
        {
            if (window != win)
            {
                window = win;
            }

            ctxMenuSignOut.Enabled = showSignout;
        }
    }
}
