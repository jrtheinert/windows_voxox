﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using System;                               //Predicate
using System.Linq;                          //IList<T>.Where
using System.Collections.Generic;			//IEnumerable
using System.Collections.ObjectModel;		//ObservableCollection, OnCollectionChanged
using System.Collections.Specialized;		//NotifyCollectionChangedEventArgs

namespace Desktop.Util
{
    public class VxObservableCollection<T> : ObservableCollection<T>
    {
        public void AddRange(IEnumerable<T> collection)
        {
            foreach (var i in collection)
			{ 
				Items.Add(i);
			}

            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        public void RemoveAll(Predicate<T> predicate)
        {
            List<T> itemsToRemove = Items.Where(x => predicate(x)).ToList();

            itemsToRemove.ForEach(item => Remove(item));
        }
    }
}
