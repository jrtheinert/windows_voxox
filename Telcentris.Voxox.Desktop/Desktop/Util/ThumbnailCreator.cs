﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using System;						//Math
using System.Drawing;				//Image, Bitmap, Graphics
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;		//PixelFormat, ImageCodedInfo, etc.
using System.Linq;					//Array

namespace Desktop.Util
{
    public class ThumbnailCreator
    {
        private static int THUMB_PORTRAIT_WIDTH = 100;
        private static int THUMB_PORTRAIT_HEIGHT = 133;
        private static int THUMB_LANDSCAPE_WIDTH = 120;
        private static int THUMB_LANDSCAPE_HEIGHT = 90;

        public static bool ResizeImage(string SourceFile, string DestinationFile)
        {
            try
            {
                using (var img = Image.FromFile(SourceFile))
                {
                    int originalWidth = img.Width;
                    int originalHeight = img.Height;
                    bool isPortrait = img.Width > img.Height;
                    int maxWidth = isPortrait ? THUMB_PORTRAIT_WIDTH : THUMB_LANDSCAPE_WIDTH;
                    int maxHeight = isPortrait ? THUMB_PORTRAIT_HEIGHT : THUMB_LANDSCAPE_HEIGHT;

                    // To preserve the aspect ratio
                    float ratioX = (float)maxWidth / (float)originalWidth;
                    float ratioY = (float)maxHeight / (float)originalHeight;
                    float ratio = Math.Min(ratioX, ratioY);

                    // New width and height based on aspect ratio
                    int newWidth = (int)(originalWidth * ratio);
                    int newHeight = (int)(originalHeight * ratio);

                    // Convert other formats (including CMYK) to RGB.
                    using (Bitmap newImage = new Bitmap(newWidth, newHeight, PixelFormat.Format24bppRgb))
                    {
                        // Draws the image in the specified size with quality mode set to HighQuality
                        using (Graphics graphics = Graphics.FromImage(newImage))
                        {
                            graphics.CompositingQuality = CompositingQuality.HighQuality;
                            graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                            graphics.SmoothingMode = SmoothingMode.HighQuality;
                            graphics.DrawImage(img, 0, 0, newWidth, newHeight);
                        }

                        // Get an ImageCodecInfo object that represents the JPEG codec.
                        ImageCodecInfo imageCodecInfo = ImageCodecInfo.GetImageDecoders().SingleOrDefault(c => c.FormatID == ImageFormat.Jpeg.Guid);

                        // Create an EncoderParameters object. 
                        EncoderParameters encoderParameters = new EncoderParameters(1);

                        // Save the image as a JPEG file with quality level.
                        EncoderParameter encoderParameter = new EncoderParameter(Encoder.Quality, 80L);
                        encoderParameters.Param[0] = encoderParameter;
                        newImage.Save(DestinationFile, imageCodecInfo, encoderParameters);
                    }
                }

                return true;
            }
            catch
            {
            }

            return false;
        }
    }
}
