﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using System;					//Type, NotImplementedException
using System.Globalization;		//CultureInfo
using System.Windows;			//Visibility
using System.Windows.Data;		//IValueConverter

namespace Desktop.Util
{
	class CountToVisibilityConverter  : IValueConverter
	{
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null )
                return Visibility.Collapsed;

			Int32 temp = System.Convert.ToInt32( value );

            return temp == 0 ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
	}
}
