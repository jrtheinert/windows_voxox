﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Utils;					//Logger

using System;							//Type, String, Uri
using System.Globalization;				//CultureInfo
using System.IO;						//FileNotFoundException
using System.Windows;					//DependencyProperty
using System.Windows.Data;				//IValueConverter
using System.Windows.Media.Imaging;		//BitmapImage, BitmapCacheOption

namespace Desktop.Util
{
    public class UriToImageSourceConverter : IValueConverter
    {
        static VXLogger logger = VXLoggingManager.INSTANCE.GetLogger("UriToImageSourceConverter");

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
			//For debugging/logging
			String resource = ( value == null ? "<null>" : (String) value );

			if ( resource == null )
			{
				resource = "<null>";
			}

            try
            {
                if (value == null || (value is string && string.IsNullOrWhiteSpace((string)value)))
                    return DependencyProperty.UnsetValue;

                if (value is string)
                {
                    string url = value.ToString().Trim();
                    bool isEmbedded = url.Contains(";component/"); // If the url is from an embedded resource, then the Uri must be created by specifying UriKind.Relative

                    try
                    {
                        var image = new BitmapImage();
                        image.BeginInit();
                        image.CacheOption = isEmbedded ? BitmapCacheOption.Default : BitmapCacheOption.OnLoad;
                        image.CreateOptions = isEmbedded ? BitmapCreateOptions.None : BitmapCreateOptions.IgnoreImageCache;
                        image.UriSource = isEmbedded ? new Uri(url, UriKind.Relative) : new Uri(url);
                        image.EndInit();

                        return image;
                    }
                    catch (FileNotFoundException fex)
                    {
                        // The file/avatar does not exists / deleted 
                        logger.Error("Convert - BitmapImage", fex);
                        return DependencyProperty.UnsetValue;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Convert: " + resource, ex);
            }

			//JRT - 2015.03.19
			//If we got here, then we had an exception.  This could be solely due to users PC setup/configuration.
			//	https://swimmingpooldotnet.wordpress.com/2010/08/09/wpf-error-no-imaging-component-suitable-to-complete-this-operation-was-found/
			//	'value' is likely a string which is not valid as ImageSource in XAML, so, let's return a empty image.
//          return value;
			logger.Error( "Returning empty BitmapImage for: " + resource );
			return new BitmapImage();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
