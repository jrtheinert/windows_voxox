﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using Desktop.Model;		//ConnectionMessage
using Desktop.Utils;		//Logger

using System;							//String, Uri
using System.Net;						//IWebProxy, WebRequest, WebClient
using System.Net.NetworkInformation;	//Network related items
using System.Timers;					//Timer, ElapsedEventArgs
using System.Windows;					//MessageBox, MessageBoxButton, MessageBoxImage

namespace Desktop.Util
{
    /// <summary>
    /// Class to check network status.
    /// It checks for active available interfaces & also pings a remote host to check actual connectivity
    /// </summary>
    public class NetworkStatus
    {
		private VXLogger mLogger = VXLoggingManager.INSTANCE.GetLogger("NetworkStatus");
		
		private bool   mIsAvailable;
        private Timer  mPingTimer;
		private String mHost;

		public  static IWebProxy s_Proxy = null;	//To be set by SessionManager.

        public static NetworkStatus Instance
        {
            get { return Nested.instance; }
        }

        class Nested
        {
            static Nested()		{}

            internal static readonly NetworkStatus instance = new NetworkStatus();
        }

        /// <summary>
        /// Initialize the class by detecting the start condition.
        /// </summary>
        private NetworkStatus()
        {
			mHost = Desktop.Config.API.RemotePingUrl;

			//Get configured host without scheme for PING.  Not currently used.
			//int index = s_Host.IndexOf( "://");

			//if ( index >= 0 )
			//{
			//	s_Host = s_Host.Substring( index + 3 );
			//}

            mIsAvailable = IsNetworkAvailable() && CheckInternetConnection();
            mPingTimer   = new Timer(Desktop.Config.API.RemotePingTimerSecs * 1000);

			LogProxyInfo();

			HandleConnectionChange( mIsAvailable );		//Notify UI.
        }

        /// <summary>
        /// Start Network check
        /// </summary>
        public void StartWatching()
        {
			setEventHandlers( true );
        }

        /// <summary>
        /// Stop Network check
        /// </summary>
        public void StopWatching()
        {
			setEventHandlers( false );
        }

        /// <summary>
        /// Gets a Boolean value indicating the current state of Internet connectivity.
        /// </summary>
        public bool IsAvailable
        {
            get { return mIsAvailable; }
        }

        public void ShowNetworkErrorMessage()
        {
            MessageBox.Show( LocalizedString.NetworkErrorMsg, LocalizedString.MainWindowTitle, MessageBoxButton.OK, MessageBoxImage.Error );
        }

        public void ShowNetworkErrorWithCallsMessage()
        {
            MessageBox.Show( LocalizedString.NetworkErrorWithCallsMsg, LocalizedString.MainWindowTitle, MessageBoxButton.OK, MessageBoxImage.Error );
        }


		#region private static methods

		private void LogProxyInfo()
		{
			if ( s_Proxy != null )
			{ 
				var client = new WebClientEx();

				client.Proxy = s_Proxy;

				Uri temp      = s_Proxy.GetProxy  ( new Uri( Desktop.Config.API.RemotePingUrl) );
				bool byPassed = s_Proxy.IsBypassed( new Uri( Desktop.Config.API.RemotePingUrl) );

				if ( !byPassed )
				{
					mLogger.Debug( "Using proxy: " + temp.AbsoluteUri );
				}
			}
		}

		private void setEventHandlers( bool initialize )
		{
			if ( initialize )
			{ 
				NetworkChange.NetworkAvailabilityChanged += new NetworkAvailabilityChangedEventHandler( DoNetworkAvailabilityChanged );
				NetworkChange.NetworkAddressChanged      += new NetworkAddressChangedEventHandler     ( DoNetworkAddressChanged		 );

				mPingTimer.Elapsed += pingTimer_Elapsed;
				mPingTimer.Enabled = true;
			}
			else
			{
				NetworkChange.NetworkAvailabilityChanged -= new NetworkAvailabilityChangedEventHandler( DoNetworkAvailabilityChanged );
				NetworkChange.NetworkAddressChanged      -= new NetworkAddressChangedEventHandler     ( DoNetworkAddressChanged      );

				mPingTimer.Elapsed -= pingTimer_Elapsed;
				mPingTimer.Enabled = false;
			}

		}

		/// <summary>
        /// Evaluate the online network adapters to determine if at least one of them
        /// is capable of connecting to the Internet (But this might not tell if internet is accessible, this is a quick test)
        /// </summary>
        /// <returns></returns>
        private bool IsNetworkAvailable()
        {
			bool result = false;

            // only recognizes changes related to Internet adapters
            if (NetworkInterface.GetIsNetworkAvailable())
            {
                // however, this will include all adapters
                NetworkInterface[] interfaces = NetworkInterface.GetAllNetworkInterfaces();
                foreach (NetworkInterface face in interfaces)
                {
                    // filter so we see only Internet adapters
                    if (face.OperationalStatus == OperationalStatus.Up)
                    {
                        if ((face.NetworkInterfaceType != NetworkInterfaceType.Tunnel) &&
                            (face.NetworkInterfaceType != NetworkInterfaceType.Loopback))
                        {
                            IPv4InterfaceStatistics statistics = face.GetIPv4Statistics();

                            // all testing seems to prove that once an interface comes online
                            // it has already accrued statistics for both received and sent...

                            if ((statistics.BytesReceived > 0) && (statistics.BytesSent > 0))
                            {
                                result = true;
                            }
                        }
                    }
                }
            }

            return result;
        }

        private void DoNetworkAddressChanged(object sender, EventArgs e)
        {
            SignalAvailabilityChange();
        }

        private void DoNetworkAvailabilityChanged(object sender, NetworkAvailabilityEventArgs e)
        {
            SignalAvailabilityChange();
        }

        private void pingTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            mPingTimer.Enabled = false; // Stop Timer till the availability is checked

//            s_IsAvailable = CheckInternetConnection();
            mIsAvailable = IsNetworkAvailable() && CheckInternetConnection();

            mPingTimer.Enabled = true; // Start Timer back          
        }

        private void SignalAvailabilityChange()
        {
//            bool change = IsNetworkAvailable();
			bool change = IsNetworkAvailable() && CheckInternetConnection();

//            if (change != s_IsAvailable)
            {
//				s_IsAvailable = (change ? CheckInternetConnection() : false );
				mIsAvailable = change;
				HandleConnectionChange( mIsAvailable );
            }            
        }

		private void HandleConnectionChange( bool connected )
		{
			GalaSoft.MvvmLight.Messaging.Messenger.Default.Send<ConnectionMessage>( ConnectionMessage.makeInternet( connected ) );
		}

        /// <summary>
        /// Ping a remote host to check for actual Internet
        /// </summary>
        /// <returns></returns>
        private bool CheckInternetConnection()
        {
			//Let's try the System.Net.NetworkInformation.Ping class, for simplicity.
			//	We *could* use a more complicated version of Ping.Send(), but this works.
			//
			//Internet search indicates PING is not reliable for our purposes due to various
			//	reasons (icmp filtering, OS/socket level vs. service level, etc.)
			//	Recommendation is to just make an service call.
			//bool result = false;

			//try
			//{ 
			//	int		timeOut = Desktop.Config.API.RemotePingTimeoutSecs * 1000;	//Typically 5 seconds

			//	for ( int x = 1; x < 100; x++ )
			//	{ 
			//		Ping	ping	= new Ping();
			//		PingReply reply = ping.Send( s_Host, timeOut );

			//		switch ( reply.Status )
			//		{
			//		case IPStatus.Success:
			//			result = true;
			//			break;

			//		case IPStatus.TimedOut:
			//			break;

			//		default:
			//			result = false;
			//			break;
			//		}

			//		System.Threading.Thread.Sleep( 100 );
			//	}
			//}

			//catch ( Exception /*ex*/ )
			//{
			//	result = false;
			//}

			//return result;

			try
			{
				using (var client = new WebClientEx())
				{
					client.Proxy = s_Proxy;

					using (var stream = client.OpenRead( Desktop.Config.API.RemotePingUrl) )
					{
						return true;
					}
				}
			}
			catch
			{
				return false;
			}
        }

		/// <summary>
		/// Extended WebClient with small Timeout & ignoring any SSL errors
		/// </summary>
		private class WebClientEx : WebClient
		{
			public WebClientEx()
			{
				try
				{
					ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback( delegate { return true; } );
				}
				catch
				{
				}
			}

			protected override WebRequest GetWebRequest(Uri uri)
			{
				WebRequest w = base.GetWebRequest(uri);
				w.Timeout = Desktop.Config.API.RemotePingTimeoutSecs * 1000; // 5 second timeout for download
				return w;
			}
		}

		#endregion
	}
}
