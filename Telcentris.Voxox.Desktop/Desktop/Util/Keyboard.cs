﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using System;							//Char
using System.Runtime.InteropServices;	//DllImport, DllImportAttribute, Out, OutAttribute
using System.Text;						//StringBuilder
//using System.Windows;
using System.Windows.Input;				//Key, KeyInterop
	
namespace Desktop.Util
{
	class Keyboard
	{
		public enum MapType : uint
		{
			MAPVK_VK_TO_VSC    = 0x0,
			MAPVK_VSC_TO_VK    = 0x1,
			MAPVK_VK_TO_CHAR   = 0x2,
			MAPVK_VSC_TO_VK_EX = 0x3,
		}

		[DllImport("user32.dll")]
		public static extern int ToUnicode( int wVirtKey, uint wScanCode, byte[] lpKeyState, 
											[Out, MarshalAs(UnmanagedType.LPWStr, SizeParamIndex = 4)] StringBuilder pwszBuff,
											int cchBuff, uint wFlags);

		[DllImport("user32.dll")]
		public static extern bool GetKeyboardState( byte[] lpKeyState );

		[DllImport("user32.dll")]
		public static extern uint MapVirtualKey( uint uCode, MapType uMapType );

		public static Char GetCharFromKey( Key key )
		{
			char ch = ' ';

			int    virtualKey    = KeyInterop.VirtualKeyFromKey(key);
			byte[] keyboardState = new byte[256];

			GetKeyboardState(keyboardState);

			uint          scanCode      = MapVirtualKey( (uint)virtualKey, MapType.MAPVK_VK_TO_VSC );
			StringBuilder stringBuilder = new StringBuilder(2);

			int result = ToUnicode( virtualKey, scanCode, keyboardState, stringBuilder, stringBuilder.Capacity, 0 );

			switch (result)
			{
			case -1: 
				break;
			case 0: 
				break;
			case 1:
			{
				ch = stringBuilder[0];
				break;
			}
			default:
			{
				ch = stringBuilder[0];
				break;
			}
			}
			return ch;
		}	
	}
}
