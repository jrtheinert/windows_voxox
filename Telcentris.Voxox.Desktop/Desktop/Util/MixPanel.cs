﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using System;
using System.Net;		//WebClient
using System.Text;		//Encoding

namespace Desktop.Util
{
    //CODE-REVIEW:  Poorly implemented class:
    //	- Should be singleton
    //	- Should have defined values for event names (not hard-coded within string.Format() )
    //		- destkop_gen_login, desktop_gen_logout, desktop_session, desktop_session_length
    //		- This will make it easier to review the values, ensure they are correct and possibly
    //			suggest renaming them based on how data looks in MP reports
    //	- Should get ALL non-volatile data on instantiation, e.g. User.CompanyUserId
    //	- Should have standard method(s) to format JSON
    //We have a very robust MixPanel class in -Android-.  That should have been used as a model
    //	as was discussed during scrums.  MixPanel can be very complicated.
    //
    internal sealed class MixPanel
    {
        #region | Private Class Variables |

        private static string mixPanelAPIUrl;
        private static string mixPanelToken;
        private static DateTime? sessionStartTime;
        private static string mixPanelAlias;

        #endregion

        #region | Public Procedures |

        public static void Initialize(string mxPanelAPIUrl, string mxPanelToken)
        {
            mixPanelAPIUrl = mxPanelAPIUrl;
            mixPanelToken = mxPanelToken;
        }

        public static bool LogLogin(string mpAlias)
        {
            mixPanelAlias = mpAlias;

            string json = string.Format("{{ \"event\": \"{0}\", \"properties\": {{ \"distinct_id\": \"{1}\", \"token\": \"{2}\" }} }}", "desktop_gen_login", mixPanelAlias, mixPanelToken);

            StartSession();

            return LogEvent(json);
        }

        public static bool LogLogout()
        {
            string json = string.Format("{{ \"event\": \"{0}\", \"properties\": {{ \"distinct_id\": \"{1}\", \"token\": \"{2}\" }} }}", "desktop_gen_logout", mixPanelAlias, mixPanelToken);

            StopSession();

            return LogEvent(json);
        }

        public static void StartSession()
        {
            if (sessionStartTime == null)
                sessionStartTime = DateTime.Now;
        }

        public static bool StopSession()
        {
            if (sessionStartTime == null)
                return false;

            int duration = (int)(DateTime.Now - sessionStartTime.Value).TotalMinutes;
            sessionStartTime = null;

            string json = string.Format("{{ \"event\": \"{0}\", \"properties\": {{ \"distinct_id\": \"{1}\", \"token\": \"{2}\", \"desktop_session_length\": \"{3}\" }} }}", "desktop_session", mixPanelAlias, mixPanelToken, duration);

            return LogEvent(json);
        }

        #endregion

        #region | Private Procedures |

        private static bool LogEvent(string json)
        {
            using (var client = new WebClient())
            {
                string url = string.Format("{0}{1}", mixPanelAPIUrl, Base64Encode(json));
				string value = "";

				//In case of Exceptions, let's retry, rather than crash
				int maxTries = 3;
				int tryCount = 0;
				int waitIncrement = 500;	//In ms

				while ( tryCount < maxTries )
				{ 
					tryCount++;

					try 
					{ 
						value = client.DownloadString(url);
						break;
					}

					catch ( System.Net.WebException )
					{
						//No sense logging exception, since we will never see it unless app crashes.
						System.Threading.Thread.Sleep( waitIncrement );
					}
				}

                return !string.IsNullOrEmpty(value);
            }
        }

        private static string Base64Encode(string text)
        {
            var textBytes = Encoding.UTF8.GetBytes(text);

            return Convert.ToBase64String(textBytes);
        }

        #endregion
    }
}
