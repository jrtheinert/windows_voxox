﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Apr 2015
 */

using System;

namespace Desktop.Util
{
	//Full JID: user@domain.com/resource
	class Jid
	{
		private static readonly String s_resourceSep = "/";
		private static readonly String s_domainSep   = "@";

		//Removes resource portion of Jid
		static public String getCleanJid( String jidIn)
		{
			String result = jidIn;

			int pos = jidIn.IndexOf( s_resourceSep );
			if ( pos >= 0 )
			{ 
				result = jidIn.Substring( 0, pos );
			}

			return result;
		}

		//Returns just userId portion of Jid
		static public String getUserId( String jidIn )
		{
			String result = jidIn;

			int pos = jidIn.IndexOf( s_domainSep );
			if ( pos >= 0 )
			{ 
				result = jidIn.Substring( 0, pos );
			}

			return result;
		}

		//Returns Domain portion
		static public String getDomain( String jidIn)
		{
			String result = getCleanJid( jidIn );

			int pos = result.IndexOf( s_domainSep );

			if ( pos >= 0 )
			{ 
				result = jidIn.Substring( pos + 1 );
			}

			return result;
		}

		//Returns domain portion
		static public String getResource( String jidIn)
		{
			String result = jidIn;

			int pos = jidIn.IndexOf( s_resourceSep );

			if ( pos >= 0 )
			{ 
				result = jidIn.Substring( pos + 1 );
			}

			return result;
		}

		//Make full Jid based on components
		static public String makeFullJid( String userId, String domain, String resource )
		{
			String result = null;

			//We need userId and domain to be valid.
			if ( !String.IsNullOrEmpty( userId ) && !String.IsNullOrEmpty( domain ) )
			{
				if ( userId.Contains( s_domainSep ) )
				{ 
					result = userId;
				}
				else 
				{
					result = userId + s_domainSep + domain;

					if ( !String.IsNullOrEmpty( resource ) )
					{
						result += s_resourceSep + resource;
					}
				}
			}

			return result;
		}

		static public Boolean isSameXmppUser( String jid1, String jid2 )
		{
			Boolean result = false;

			String temp1 = getCleanJid( jid1 );
			String temp2 = getCleanJid( jid2 );

			result = temp1 == temp2;

			return result;
		}

		static public Boolean hasResourceOf( String jid, String tgtResource )
		{
			Boolean result = false;

			String tempResource = getResource( jid );

			//If both are emtpy there are NOT considered the same.
			if ( !String.IsNullOrEmpty( tempResource ) && !String.IsNullOrEmpty( tgtResource ) )
			{ 
				result = tempResource == tgtResource;
			}

			return result;
		}

	}
}
