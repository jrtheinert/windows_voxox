﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Desktop.Util
{
    public class CallTypeToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return Visibility.Collapsed;

            var currentState = value.ToString();
            var found = false;

            found = currentState == "Missed" || currentState == "Normal";

            return found ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
