#PowerShell file to copy PDB files to save directory after each build.
# Optional 'Directory Suffix' parameter for QA builds
param([String]$DirSuffix="") 

CD D:\Dev\Telcentris\_DesktopApp\Telcentris.Voxox.Desktop

Set-Variable -name BINPATH -value "Desktop\bin\release"

$EXEPATH  = $BINPATH + "\Desktop.exe"
$APPVER = (get-item $exepath).VersionInfo.FileVersion
$SaveDir = "..\PdbFiles\$APPVer$DirSuffix"

ECHO "binpath = $BINPATH"
ECHO "exepath = $EXEPATH"
ECHO "appver  = $APPVER"

MKDIR $SaveDir -Force
XCOPY /Q /Y $BINPATH\*.pdb $SaveDir
