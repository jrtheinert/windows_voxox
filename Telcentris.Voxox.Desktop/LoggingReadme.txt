Desktop Logging 
------------------

  - Log4Net is used as the logging component, https://logging.apache.org/log4net/, wrapped using VXLoggingManager & VXLogger classes.

  - "log4net.config" file under the Desktop project is used as initial/default settings configuration template. For information about tags/attributes check https://logging.apache.org/log4net/release/manual/configuration.html
     - Current logging defaults to a file rolled based on date and file size & backups (currently 20MB and up to 10 backups)
     - The log files are stored in "Logs" folder under the app's AppData folder (%APPDATA%\VoicePass\Logs)


PJSIP Logging
-----------------

