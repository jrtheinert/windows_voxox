
@echo off
set USER=jrtheinert@tsasg.com
set PASSWORD=jrtjrt
set DATABASE=jrtheinert_tsasg_com
set APPNAME=Desktop
REm set BINPATH=.\Desktop\bin\Release;..\XmppApi-Native\Release
set BINPATH=.\Desktop\bin\Release
set LIBRARY=.\Desktop\bin\Release\Desktop.exe
REM set VERSION=3.0.0.0590

REM Copy a couple other .pdb files to main location
XCOPY /Q /Y ..\OpenSsl-Native\Release\OpenSslInitializer.pdb %BINPATH%

set FILES=^
Desktop.exe;^
Desktop.pdb;^
Desktop.Utils.dll;^
Desktop.Utils.pdb;^
ManagedDataTypes.dll;^
ManagedDataTypes.pdb;^
ManagedDbManager.dll;^
ManagedDbManager.pdb;^
ManagedSipAgentApi.dll;^
ManagedSipAgentApi.pdb;^
ManagedSipDataTypes.dll;^
ManagedSipDataTypes.pdb;^
ManagedXmppApi.dll;^
ManagedXmppApi.pdb;^
ManagedXmppDataTypes.dll;^
ManagedXmppDataTypes.pdb;^
VxApi23.dll;^
VxApi23.pdb;^
LibJingleXmpp.dll;^
LibJingleXmpp.pdb;^
OpenSslInitializer.dll;^
OpenSslInitializer.pdb;^
SipAgent.dll;^
SipAgent.pdb;^
SipAgentApi.dll;^
SipAgentApi.pdb;^
VoiceEngine.dll;^
VoiceEngine.pdb;^
VoxoxDbManager.dll;^
VoxoxDbManager.pdb;^
GalaSoft.MvvmLight.dll;^
GalaSoft.MvvmLight.pdb;^
GalaSoft.MvvmLight.Extras.dll;^
GalaSoft.MvvmLight.Extras.pdb;^
Microsoft.Practices.ServiceLocation.dll;^
Microsoft.Practices.ServiceLocation.pdb;^


echo user = %USER%
echo password = %PASSWORD%
echo db = %DATABASE%
echo appName = %APPNAME%
echo library = %LIBRARY%
REM echo version = %VERSION%
echo binPath = %BINPATH%
echo files = %FILES%
echo .
echo .


REM Remove any previous files
SendPdbs.exe /u %USER% /p %PASSWORD% /a %APPNAME% /l %LIBRARY%  /b %DATABASE% /d %BINPATH% /r

REM Send new files
SendPdbs.exe /u %USER% /p %PASSWORD% /a %APPNAME% /l %LIBRARY%  /b %DATABASE% /d %BINPATH% /f "%FILES%"

REM Save PDB files locally for use in resolving crashes (in case BugSplat has issues)
PowerShell -File SavePdbFiles.ps1
REM PowerShell -File SavePdbFiles.ps1 -DirSuffix QA

