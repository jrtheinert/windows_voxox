﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Vxapi;
using Vxapi.Model;

namespace Vxapi.Tests
{
    [TestClass]
    public class RestfulAPITests
    {
        [TestMethod]
        public void TestIsInternetConnectionAvailable()
        {
            bool isInternetAvailable = RestfulAPIManager.INSTANCE.isInternetConnectionAvailable();
            Assert.IsTrue(isInternetAvailable);
        }

        [TestMethod]
        public void TestPing()
        {
            PingResponse resp = RestfulAPIManager.INSTANCE.Ping();
            Assert.IsNotNull(resp);
        }

        [TestMethod]
        public void TestLogin()
        {
            LoginResponse resp = RestfulAPIManager.INSTANCE.Login("sunil.ot", "Abc@123");
            Assert.IsNotNull(resp);
        }

        [TestMethod]
        public void TestTranslatorLanguageParameters()
        {
            TranslatorLanguageParametersResponse resp = RestfulAPIManager.INSTANCE.GetTranslateLanguagesParameters();
            Assert.IsNotNull(resp);
        }

        [TestMethod]
        public void TestGetAssignCreditAndDid()
        {
            AssignCreditAndDidResponse resp = RestfulAPIManager.INSTANCE.GetAssignCreditAndDid("2174359");
            Assert.IsNotNull(resp);
        }
        
        [TestMethod]
        public void TestGetMessageHistoryResponse()
        {
            MessageHistoryResponse resp = RestfulAPIManager.INSTANCE.GetMessageHistoryResponse("2174359");
            Assert.IsNotNull(resp);
        }
    }
}
