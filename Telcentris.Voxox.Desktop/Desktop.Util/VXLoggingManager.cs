﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using log4net.Repository.Hierarchy;
using log4net.Core;
using log4net.Appender;
using log4net.Layout;
using System.IO;

namespace Desktop.Utils
{
    /// <summary>
    /// This is the class used by client applications to bind to instantiate
    /// logging and bind to logger instances.
    /// </summary>
    /// <remarks>
    /// <para>
    /// See the <see cref="VXLogger"/> for more details.
    /// </para>
    /// <para>
    /// It uses log4net as the framework and provides a wrapping logging 
    /// framework. Which means that the backend logging framework can be changed  
    /// without affecting other projects\modules
    /// </para>
    /// </remarks>
    /// <example>Simple example of logging messages
    /// <code>
    /// VXLogger logger = VXLogManager.INSTANCE.GetLogger("MainClass");
    /// 
    /// log.Info("Application Start");
    /// log.Debug("This is a debug message");
    /// 
    /// if (log.IsDebugEnabled)
    /// {
    ///		log.Debug("This is another debug message");
    /// }
    /// </code>
    /// </example>
    /// <seealso cref="VXLogger"/>
	
    public class VXLoggingManager
    {
        #region Private Instance Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="VXLoggingManager" /> class. 
        /// </summary>
        /// <remarks>
        /// Uses a private access modifier to prevent external instantiation of this class,
        /// as this class uses Singleton Pattern.
        /// </remarks>		
        private VXLoggingManager() {
        }
        #endregion Private Instance Constructors

        #region Private Instance Fields
        
        public static VXLoggingManager INSTANCE = new VXLoggingManager();

        private static bool INIT_DONE = false;

        private static readonly ILog logger = LogManager.GetLogger("VXLoggingManager");

        private static Hashtable classLoggers = new Hashtable();
        
        #endregion Private Instance Fields

        #region Public Instance Methods

        /// <summary>
        /// Returns true\false, true if the Logging framework was instantiated without any issues.
        /// false otherwise
        /// </summary>
        /// <returns>true if logging was setup without any issues, false if loggin setup fails</returns>
        public bool initSuccess()
        {
            return INIT_DONE;
        }

        /// <summary>
        /// Instantiates or sets up the logging framework. If the instantiation fails,
        /// this method can be called again to reset it.
        /// </summary>
        public void Setup(string rootFolderPath)
        {
            if(INIT_DONE)
                return;
            try
            {
                // Reading config data from lognet.config file
                log4net.Config.XmlConfigurator.ConfigureAndWatch(new FileInfo(rootFolderPath));

                logger.Debug("Logging initialized. --------------------------------------------------------------------");
                INIT_DONE = true;                
            }
            catch (Exception /*e*/)  //CODE-REVIEW: Avoid warnings if parameter is not used.
            {
                INIT_DONE = false;
                //TODO Added Windows EventLog here with critical severity
            }
        }

        /// <summary>
        /// Retrieves or creates a named logger.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Retrieves a logger named as the <paramref name="className"/>
        /// parameter. If the named logger already exists, then the
        /// existing instance will be returned. Otherwise, a new instance is
        /// created.
        /// </para>
        /// <para>By default, loggers do not have a set level but inherit
        /// it from the hierarchy. 
        /// </para>
        /// </remarks>
        /// <param name="name">The name of the logger to retrieve.</param>
        /// <returns>The logger with the name specified.</returns>
        public VXLogger GetLogger(string className)
        {
            if (classLoggers.Contains(className))
                return (VXLogger) classLoggers[className];
            else
            {
                VXLogger newClassLogger = new VXLogger(this, className);
                classLoggers.Add(className, newClassLogger);
                return newClassLogger;
            }
        }

        public void DisableLoggingFor(string[] listOfLoggers)
        {
            Hierarchy hierarchy = (Hierarchy)LogManager.GetRepository();
            var appenders = hierarchy.GetAppenders();
            foreach(var appender in appenders)
            {
                var castAppender = appender as AppenderSkeleton;
                if (castAppender != null)
                {
                    castAppender.ClearFilters();
                    foreach (var logger in listOfLoggers)
                    {
                        castAppender.AddFilter(new log4net.Filter.LoggerMatchFilter { LoggerToMatch = logger.Trim(), AcceptOnMatch = false });
                    }
                }
            }
        }

		static public String FilePath()
		{
			VXLogger logger = VXLoggingManager.INSTANCE.GetLogger("");

			return logger.FilePath();
		}

        #endregion Public Instance Methods
    }
}
