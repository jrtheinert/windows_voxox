﻿using System.Reflection;
//using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;		//ComVisible, GUID

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
//Branding - We may want to have 'voxox' as the default rather than explicity use #elif BRAND_VOXOX
#if BRAND_VOICEPASS
[assembly: AssemblyTitle("Voice Pass Utils")]
[assembly: AssemblyProduct("Voice Pass Utils")]
#elif BRAND_VOXOX
[assembly: AssemblyTitle("Voxox Utils")]
[assembly: AssemblyProduct("Voxox Utils")]
#elif BRAND_CLOUDPHONE
[assembly: AssemblyTitle  ("Cloud Phone Utils")]
[assembly: AssemblyProduct("Cloud Phone Utils")]
#elif BRAND_VTC
[assembly: AssemblyTitle  ("VTC Utils")]
[assembly: AssemblyProduct("VTC Utils")]
#else
[assembly: AssemblyTitle("Desktop Utils")]
[assembly: AssemblyProduct("Desktop Utils")]
#endif


[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyCopyright("Copyright ©  2015")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("9adaa5ba-9fff-4b59-9d85-388592f7a4d6")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("3.0.0.945")]
[assembly: AssemblyFileVersion("3.0.0.945")]
