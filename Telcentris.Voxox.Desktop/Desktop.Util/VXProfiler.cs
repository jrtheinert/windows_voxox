﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Desktop.Utils
{
    /// <summary>
    /// A Profiler class to measure code execution time.
    /// </summary>
    /// <example>Example usage
    /// <code>
    /// VXProfiler prof = new VXProfiler("Some code to profile");
    /// ... do some work
    /// prof.End(); // The time spent will be logged to log file
    /// </code>
    /// </example>
    /// <example>Example usage with using block
    /// <code>
    /// using(VXProfiler prof = new VXProfiler("Server API call")
    /// {
    ///    ... do server calls
    /// } // After end of this line the time spent will be logged to log file
    /// </code>
    /// </example>
    public class VXProfiler : IDisposable
    {
        #region | Private Variables |

        private static readonly VXLogger logger = VXLoggingManager.INSTANCE.GetLogger("VXProfiler");
        private bool isDisposed = false;
        private Stopwatch stopWatch = null;
        private string profileName = string.Empty;

        #endregion

        // The profiling can be disabled using this property. //TODO: Read this from AppDB/Settings?
        public static bool Disable { get; set; }


        #region | Constructor |

        public VXProfiler(string profileName)
        {
            if (Disable == false)
            {
                this.profileName = profileName;
                AdditionalData = new StringBuilder();
                stopWatch = new Stopwatch();
                stopWatch.Start();
            }
        }

        #endregion

        #region | Public Procedures |

        /// <summary>
        /// Get the execution milliseconds after calling the End method
        /// </summary>
        public long ElapsedMilliseconds { get; set; }

        /// <summary>
        /// Set any additional data that should be appended to log
        /// </summary>
        public StringBuilder AdditionalData { get; private set; }

        /// <summary>
        /// Stops the profiling &amp; logs the result if skipLogging = false
        /// </summary>
        public void Stop(bool skipLogging = false)
        {
            LogResult(skipLogging);
            Dispose();
        }

		public void LogElapsedTime( String comment )
		{
			AdditionalData.Clear();
			AdditionalData.Append( comment );

			LogResult( false );

			AdditionalData.Clear();
		}

        /// <summary>
        /// Disposes the objects, logs the result if End() was not called
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        #region | Private Procedures |

        private void Dispose(bool disposing)
        {
            try
            {
                if (!isDisposed)
                {
                    if (disposing)
                    {
                        LogResult();
                        AdditionalData = null;
			            stopWatch = null;
                    }
                }
            }
            finally
            {
                isDisposed = true;
            }
        }

        private void LogResult(bool skipLogging = false)
        {
            if (stopWatch == null)
                return;

            ElapsedMilliseconds = stopWatch.ElapsedMilliseconds;

            if ( !skipLogging )
            {
				if (stopWatch.Elapsed.TotalSeconds > 1)
				{
					logger.DebugFormat("{0} - {1}{2}{3}", profileName, stopWatch.ElapsedMilliseconds / 1000.0, "s", AdditionalData.Length > 0 ? " - " + AdditionalData.ToString() : "");
				}
				else
				{
                    logger.DebugFormat("{0} - {1}{2}{3}", profileName, stopWatch.ElapsedMilliseconds, "ms", AdditionalData.Length > 0 ? " - " + AdditionalData.ToString() : "");
                }
            }

            AdditionalData.Clear();
        }

        #endregion
    }
}
