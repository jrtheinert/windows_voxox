﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using log4net;                  //http://logging.apache.org/log4net/  Apache License 2.0
using log4net.Repository.Hierarchy;
using log4net.Core;
using log4net.Appender;
using log4net.Layout;

namespace Desktop.Utils
{
    /// <summary>
    /// This is the class used by client applications to log messages.
    /// </summary>
    /// <remarks>
    /// <para>
    /// See the <see cref="VXLoggingManager"/> for more details.
    /// </para>
    /// <para>
    /// It uses log4net as the framework and provides a wrapping logging 
    /// framework. Which means that the backend logging framework can be changed  
    /// without affecting other projects\modules
    /// </para>
    /// </remarks>
    /// <example>Simple example of logging messages
    /// <code>
    /// VXLogger logger = VXLogManager.INSTANCE.GetLogger("MainClass");
    /// 
    /// log.Info("Application Start");
    /// log.Debug("This is a debug message");
    /// 
    /// if (log.IsDebugEnabled)
    /// {
    ///		log.Debug("This is another debug message");
    /// }
    /// </code>
    /// </example>
    /// <seealso cref="VXLoggingManager"/>
	
    public class VXLogger
    {
        #region Private Instance Constructors
        
        /// <summary>
        /// Holds the Singleton instance of VXLoggingManager
        /// </summary>
        private VXLoggingManager vxLoggingManager = null;

        /// <summary>
        /// Holds the reference to the backend log4net ILog interface
        /// </summary>
        private readonly ILog logger = null;

        #endregion Private Instance Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="VXLogger" /> class with 
        /// the singleton instance of VXLoggingManager and the className or Logger
        /// name.
        /// </summary>
        /// <param name="loggingManager">Singleton instance of VXLoggingManager.</param>
        /// <param name="className">The class or logger name for which the logging is to be instantiated</param>
        public VXLogger(VXLoggingManager loggingManager, string className) {
            vxLoggingManager = loggingManager;
            logger = LogManager.GetLogger(className);
        }

        /// <summary>
        /// Logs a message object with the Debug level.
        /// </summary>
        /// <remarks>
        /// <para>
        /// This method first checks if this logger is <c>DEBUG</c>
        /// enabled by comparing the level of this logger with the 
        /// Debug level. If this logger is /// <c>DEBUG</c> enabled, 
        /// then it converts the message object
        /// (passed as parameter) to a string by invoking the appropriate
        /// Object renderer. It then proceeds to call all the registered 
        /// appenders in this logger and also higher in the hierarchy 
        /// depending on the value of the additivity flag.
        /// </para>
        /// <para><b>WARNING</b> Note that passing an <see cref="Exception"/> 
        /// to this method will print the name of the <see cref="Exception"/> 
        /// but no stack trace. To print a stack trace use the 
        /// <see cref="Debug(object,Exception)"/> form instead.
        /// </para>
        /// </remarks>
        /// <param name="message">The message object to log.</param>
        /// <seealso cref="Debug(object,Exception)"/>
        /// <seealso cref="IsDebugEnabled"/>
        public void Debug(object message, Exception exception)
        {
            logger.Debug(message, exception);
        }

        /// <summary>
        /// Log a message object with the Debug level including
        /// the stack trace of the <see cref="Exception"/> passed
        /// as a parameter.
        /// </summary>
        /// <remarks>
        /// See the <see cref="Debug(object)"/> form for more detailed information.
        /// </remarks>
        /// <param name="message">The message object to log.</param>
        /// <param name="t">The exception to log, including its stack trace.</param>
        /// <seealso cref="Debug(object)"/>
        /// <seealso cref="IsDebugEnabled"/>
        public void Debug(object message)
        {
            logger.Debug(message);
        }

        public void DebugFormat(IFormatProvider provider, string format, params object[] args)
        {
            logger.DebugFormat(provider, format, args);
        }

        public void DebugFormat(string format, object arg0, object arg1, object arg2)
        {
            logger.DebugFormat(format, arg0, arg1, arg2);
        }

        public void DebugFormat(string format, object arg0, object arg1)
        {
            logger.DebugFormat(format, arg0, arg1);
        }

        public void DebugFormat(string format, object arg0)
        {
            logger.DebugFormat(format, arg0);
        }

        public void DebugFormat(string format, params object[] args)
        {
            logger.DebugFormat(format, args);
        }

        public void Error(object message, Exception exception)
        {
            logger.Error(message, exception);
        }

        public void Error(object message)
        {
            logger.Error(message);
        }

        public void ErrorFormat(IFormatProvider provider, string format, params object[] args)
        {
            logger.ErrorFormat(provider, format, args);
        }

        public void ErrorFormat(string format, object arg0, object arg1, object arg2)
        {
            logger.ErrorFormat(format, arg0, arg1, arg2);
        }

        public void ErrorFormat(string format, object arg0, object arg1)
        {
            logger.ErrorFormat(format, arg0, arg1);
        }

        public void ErrorFormat(string format, object arg0)
        {
            logger.ErrorFormat(format, arg0);
        }

        public void ErrorFormat(string format, params object[] args)
        {
            logger.ErrorFormat(format, args);
        }

        public void Fatal(object message, Exception exception)
        {
            logger.Fatal(message, exception);
        }

        public void Fatal(object message)
        {
            logger.Fatal(message);
        }

        public void FatalFormat(IFormatProvider provider, string format, params object[] args)
        {
            logger.FatalFormat(provider, format, args);
        }

        public void FatalFormat(string format, object arg0, object arg1, object arg2)
        {
            logger.FatalFormat(format, arg0, arg1, arg2);
        }

        public void FatalFormat(string format, object arg0, object arg1)
        {
            logger.FatalFormat(format, arg0, arg1);
        }

        public void FatalFormat(string format, object arg0)
        {
            logger.FatalFormat(format, arg0);
        }

        public void FatalFormat(string format, params object[] args)
        {
            logger.FatalFormat(format, args);
        }

        public void Info(object message, Exception exception)
        {
            logger.Info(message, exception);
        }

        public void Info(object message)
        {
            logger.Info(message);
        }

        public void InfoFormat(IFormatProvider provider, string format, params object[] args)
        {
            logger.InfoFormat(provider, format, args);
        }

        public void InfoFormat(string format, object arg0, object arg1, object arg2)
        {
            logger.InfoFormat(format, arg0, arg1, arg2);
        }

        public void InfoFormat(string format, object arg0, object arg1)
        {
            logger.InfoFormat(format, arg0, arg1);
        }

        public void InfoFormat(string format, object arg0)
        {
            logger.InfoFormat(format, arg0);
        }

        public void InfoFormat(string format, params object[] args)
        {
            logger.InfoFormat(format, args);
        }

        public bool IsDebugEnabled()
        {
            return logger.IsDebugEnabled;            
        }

        public bool IsErrorEnabled()
        {
            return logger.IsErrorEnabled;
        }

        public bool IsFatalEnabled()
        {
            return logger.IsFatalEnabled;
        }

        public bool IsInfoEnabled()
        {
            return logger.IsInfoEnabled;
        }

        public bool IsWarnEnabled()
        {
            return logger.IsWarnEnabled;
        }

        public void Warn(object message, Exception exception)
        {
            logger.Warn(message, exception);
        }

        public void Warn(object message)
        {
            logger.Warn(message);
        }

        public void WarnFormat(IFormatProvider provider, string format, params object[] args)
        {
            logger.WarnFormat(provider, format, args);
        }

        public void WarnFormat(string format, object arg0, object arg1, object arg2)
        {
            logger.WarnFormat(format, arg0, arg1, arg2);
        }

        public void WarnFormat(string format, object arg0, object arg1)
        {
            logger.WarnFormat(format, arg0, arg1);
        }

        public void WarnFormat(string format, object arg0)
        {
            logger.WarnFormat(format, arg0);
        }

        public void WarnFormat(string format, params object[] args)
        {
            logger.WarnFormat(format, args);
        }

		public String FilePath()
		{
			String filePath = "";

			log4net.Repository.ILoggerRepository repo = LogManager.GetRepository();

			IAppender[] appenders = repo.GetAppenders();

			foreach ( IAppender appender in appenders )
			{
				//Type may be FileAppender (base), or RollingFileAppender
				if ( appender.GetType().BaseType == typeof(FileAppender))
				{ 
					FileAppender fileApp = appender as FileAppender;

					if ( fileApp != null )
					{
						filePath = fileApp.File;

						//Break on first non-empty value.
						if ( !String.IsNullOrEmpty( filePath ) )
							break;
					}
				}
			}
			return filePath;
		}
    }
}
