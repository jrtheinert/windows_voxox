﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Vxapi23.Constants;
using Vxapi23.Model;
using Vxapi23.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Vxapi23.Tests
{
    [TestClass]
    public class RestfulAPITests
    {
        private string userKey = "9b2653444e287ebe2a0d85ca217efca4";
        private string companyUserId = "7361";

        private void initApi()
        {
            RestfulConstants.ApiServer = "hp.api.telcentris.com";
            RestfulConstants.ApiAuthModuleBase = "vxauth/rest/?";
//            RestfulConstants.ApiKey = "757ccb4e-7f08-11e3-a6a4-74867aa814e2";
            RestfulConstants.ApiModuleBase = "vxclient/rest/?";
            RestfulConstants.HttpFileUploadModuleBase = "vxclient/file/upload?";
        }
		//CODE-REVIEW: JRT - I would add a initApi() method and move the duplicated RestfulContants initializers there.
		//		Then just call initApi() from each test method.
        [TestMethod]
        public void TestLogin()
        {
            initApi();
            VXResult<AuthenticateResponse> resp = RestfulAPIManager.INSTANCE.Authenticate("ganeshk@orchestratec.com", "ganesh.ot", "vx", "add proper URL" );//NOTE that 'vx' is branded so may need to change for tests.
            Console.WriteLine(resp);
            Assert.IsNotNull(resp);
            Assert.IsNotNull(resp.Data);
        }

        [TestMethod]
        public void TestSendSMS()
        {
            initApi();
			//JRT - Does not compile
            List<SMSResponse> smsList = RestfulAPIManager.INSTANCE.sendSMS(userKey, "15142257688", "917893229077", "TestSMS", "guid", companyUserId);
            Assert.IsNotNull(smsList);

        }

        [TestMethod]
        public void TestGetDataSummary()
        {
            initApi();
            List<DataSummaryResponse> dataSummaryList = RestfulAPIManager.INSTANCE.GetDataSummary(userKey, companyUserId);
            Assert.IsNotNull(dataSummaryList);
        }

        [TestMethod]
        public void TestGetMessageHistory()
        {
            initApi();
            List<MessageHistoryResponse> messageHistoryList = RestfulAPIManager.INSTANCE.GetMessageHistory(userKey, companyUserId,null,null,null,0,0);
            Assert.IsNotNull(messageHistoryList);
        }

        [TestMethod]
        public void GetCallHistory()
        {
            initApi();
            int    limit     = 25;
			string timestamp = "";
            List<CallHistoryResponse> messageHistoryList = RestfulAPIManager.INSTANCE.GetCallHistory(userKey, companyUserId, timestamp, limit);
            Assert.IsNotNull(messageHistoryList);
        }

		//[TestMethod]
		//public void GetPlanInfo()
		//{
		//	initApi();
		//	List<PlanInfoResponse> messageHistoryList = RestfulAPIManager.INSTANCE.GetPlanInfo(userKey, companyUserId);
		//	Assert.IsNotNull(messageHistoryList);
		//}

        [TestMethod]
        public void GetFindMeNumbers()
        {
            initApi();
            GetFindMeNumbersResponse findMeNumbersList = RestfulAPIManager.INSTANCE.GetFindMeNumbers(userKey, companyUserId);
            Assert.IsNotNull(findMeNumbersList);
        }

        [TestMethod]
        public async Task SendFileTest()
        {
            initApi();
            var response = await RestfulAPIManager.INSTANCE.SendFile(userKey, companyUserId, @"C:\Users\Public\Pictures\Sample Pictures\Desert.jpg");
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void GetCompanyUserExtended()
        {
            initApi();
            CompanyUserExtendedResponse response = RestfulAPIManager.INSTANCE.getCompanyUserExtended(userKey, companyUserId);
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public async Task GetCompanyUserOptions()
        {
            initApi();
            CompanyUserOptionsResponse response = await RestfulAPIManager.INSTANCE.GetCompanyUserOptions(userKey, companyUserId);
            Assert.IsNotNull(response);
        }

        #region | Old Json parsing code tests |

        //[TestMethod]
        //public void TestRichTextMessage()
        //{
        //    string jsonString = "{\"voxoxrichmessage\":{\"data\":{\"file\":\"http://my.voxox.com/downloadvoxox/index/serve/?hash=066b9c67f762a51cbdcbce70965e9906-1369768874.vcf\",\"thumbnail\":\"http://my.voxox.com/downloadvoxox/index/serve/?hash=931ec0162bfe18ac596f00f7716d34e8-1369768874.\"},\"type\":\"contact\",\"body\":\"Curtis Kaffer\"}}";

        //    RichTextMessage textMessage = new RichTextMessage();
        //    if (textMessage != null)
        //    {
        //        textMessage = RestfulAPIManager.INSTANCE.GetRichTextMessage(jsonString);
        //        Assert.IsNotNull(textMessage);
        //    }
        //}

        //[TestMethod]
        //public void TestRichTextAudioMessage()
        //{
        //    string jsonString = "{ \"voxoxrichmessage\": { \"type\": \"audio\", \"data\": { \"file\": \"http://pre.my.voxox.com/downloadvoxox/index/serve/?hash=70ba7dda7c33cb20de3fc6de5b83b5ee-1360043989\"}, \"body\": \"\" }}";
        //    RichTextAudioResponse textMessage = new RichTextAudioResponse();
        //    if (textMessage != null)
        //    {
        //        textMessage = RestfulAPIManager.INSTANCE.GetRichTextAudioMessage(jsonString);
        //        Assert.IsNotNull(textMessage);
        //    }
        //}

        //[TestMethod]
        //public void TestRichTextContactMessage()
        //{
        //    string jsonString = "{\"voxoxrichmessage\":{\"data\":{\"file\":\"http://my.voxox.com/downloadvoxox/index/serve/?hash=066b9c67f762a51cbdcbce70965e9906-1369768874.vcf\",\"thumbnail\":\"http://my.voxox.com/downloadvoxox/index/serve/?hash=931ec0162bfe18ac596f00f7716d34e8-1369768874.\"},\"type\":\"contact\",\"body\":\"Curtis Kaffer\"}}";
        //    RichTextContactResponse textMessage = new RichTextContactResponse();
        //    if (textMessage != null)
        //    {
        //        textMessage = RestfulAPIManager.INSTANCE.GetRichTextContactMessage(jsonString);
        //        Assert.IsNotNull(textMessage);
        //    }
        //}

        //[TestMethod]
        //public void TestRichTextLocationMessage()
        //{
        //    string jsonString = "{ \"voxoxrichmessage\": { \"type\": \"location\", \"data\": { \"longitude\": 32.805080,\"latitude\": -117.202650 }, \"body\": \"3560 Shawnee Rd.\\nSan Diego, CA  92117\" }}";
        //    RichTextLocationResponse textMessage = new RichTextLocationResponse();
        //    if (textMessage != null)
        //    {
        //        textMessage = RestfulAPIManager.INSTANCE.GetRichTextLocationMessage(jsonString);
        //        Assert.IsNotNull(textMessage);
        //    }
        //}

        //[TestMethod]
        //public void TestRichTextMediaMessage()
        //{
        //    string jsonString = "{\"voxoxrichmessage\": {\"type\": \"jpeg\", \"data\": {\"file\": \"http://pre.my.voxox.com/downloadvoxox/index/serve/?hash=70ba7dda7c33cb20de3fc6de5b83b5ee-1360043989\",\"thumbnail\": \"http://pre.my.voxox.com/downloadvoxox/index/serve/?hash=2099e7305270c43abe56801e0f0f1ac9-1360043990\"},\"body\": \"London!\"}}";
        //    RichTextMediaResponse textMessage = new RichTextMediaResponse();
        //    if (textMessage != null)
        //    {
        //        textMessage = RestfulAPIManager.INSTANCE.GetRichTextMediaMessage(jsonString);
        //        Assert.IsNotNull(textMessage);
        //    }
        //}

        #endregion
    }
}