#TODO Items - JRT - 2015.01.22
# - What is $(^Name) and what does it evaluate to?
# - Set AutoStart ON for app.
# - How to handle <appData> directory on Uninstall.

#See <partner>.nsi file for vars defined on command line or in that file

#Source Path - For Product files
!define SOURCE_PATH "..\Desktop\bin\Release"
!define ICON_NAME   "App.Ico"

#Compress the setup file
SetCompressor /SOLID lzma
SetCompressorDictSize 8

# Registry Keys
!define REGKEY            "SOFTWARE\${PRODUCT_REGISTRY_PATH}"
!define REGKEY_ROOT       "SOFTWARE\${PRODUCT_REGISTRY_PATH_ROOT}"
!define AUTO_START_REGKEY "SOFTWARE\Microsoft\Windows\CurrentVersion\Run"
!define UNINSTALL_REGKEY  "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)"
!define APPDATA_PATH	  "$APPDATA\${PRODUCT_NAME}" 	#Program data will be stored here.

# MultiUser Symbol Definitions
!define MULTIUSER_EXECUTIONLEVEL                         Highest
!define MULTIUSER_INSTALLMODE_DEFAULT_REGISTRY_KEY       "${REGKEY}"
!define MULTIUSER_INSTALLMODE_DEFAULT_REGISTRY_VALUENAME MultiUserInstallMode
!define MULTIUSER_INSTALLMODE_COMMANDLINE
!define MULTIUSER_INSTALLMODE_INSTDIR                    "${PRODUCT_PATH}"
!define MULTIUSER_INSTALLMODE_INSTDIR_REGISTRY_KEY       "${REGKEY}"
!define MULTIUSER_INSTALLMODE_INSTDIR_REGISTRY_VALUE     "Path"

!define MUI_ABORTWARNING

#Icons
!define MUI_ICON   "${PRODUCT_ICON_SOURCE}"   	#Branded .ico file will be copied to this name under INSTDIR
!define MUI_UNICON "${PRODUCT_ICON_SOURCE}"   


#Header and side banner
!ifdef PRODUCT_SIDE_BANNER
    !define MUI_WELCOMEFINISHPAGE_BITMAP "${PRODUCT_SIDE_BANNER}"
!endif

!ifdef PRODUCT_TOP_BANNER
    !define MUI_HEADERIMAGE
    !define MUI_HEADERIMAGE_BITMAP "${PRODUCT_TOP_BANNER}"
!endif

# NSIS Included files
!include MultiUser.nsh
!include Sections.nsh
!include MUI2.nsh
!include nsDialogs.nsh
!include winver.nsh

#.NET Checker
!include "DotNetChecker.nsh"

;------------------------------------------------------------------------------
;Begin MUI page settings
;------------------------------------------------------------------------------
;------ WelCome Page ----------------------------------------------------------
!insertmacro MUI_PAGE_WELCOME

;------ License Page ----------------------------------------------------------
!insertmacro MUI_PAGE_LICENSE $(license)

;------ InstFiles Page --------------------------------------------------------
!insertmacro MUI_PAGE_INSTFILES

;------ Finish Page -----------------------------------------------------------
!define MUI_FINISHPAGE_RUN "$INSTDIR\${PRODUCT_EXE}"
!insertmacro MUI_PAGE_FINISH


;------ Uninstaller Pages -----------------------------------------------------
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES
!insertmacro MUI_UNPAGE_FINISH

# Languages
!include lang\ex_lang_main.nsh

# EULA
!define PRODUCT_EULA                      "EULA"
LicenseLangString license ${LANG_ENGLISH} "${PRODUCT_EULA}"

# Installer attributes

Name       "${PRODUCT_NAME_DISPLAY} ${PRODUCT_VERSION_SHORT}"  #Use through-out Installer screens.
OutFile    Build\${INSTALLER_NAME}
InstallDir "$PROGRAMFILES\${PRODUCT_PATH}"
CRCCheck   on
XPStyle    on

ShowInstDetails show

VIProductVersion                ${PRODUCT_VERSION_LONG}

#These are quoted because they may have embedded spaces
VIAddVersionKey ProductName     "${PRODUCT_NAME_DISPLAY}"
VIAddVersionKey ProductVersion  "${PRODUCT_VERSION_LONG}"
VIAddVersionKey CompanyName     "${PRODUCT_PUBLISHER}"
VIAddVersionKey CompanyWebsite  "${PRODUCT_WEB_SITE}"
VIAddVersionKey FileVersion     "${PRODUCT_VERSION_LONG}"
VIAddVersionKey FileDescription "${PRODUCT_VERSION_COMMENT}"
VIAddVersionKey LegalCopyright  "${PRODUCT_VERSION_TRADEMARK}"

InstallDirRegKey HKLM "${REGKEY}" Path

ShowUninstDetails show

!define UNINSTALL_EXE "${PRODUCT_NAME}-Uninstall.exe"

# Installer sections
Section -Main SEC0000

    # Check if .NET Framework is installed
    # .NET 4.5 is minimum (45)
    !insertmacro CheckNetFramework 45

    # Install C++ Runtime (VS2013 or later)
    SetOutPath $INSTDIR
    File       ..\vcredist_x86.exe
    ExecWait   '$INSTDIR\vcredist_x86.exe /install /quiet /norestart'

    # Install the app files
    !include Files_Install.nsi

# We have no components
#    #Mark section as complete
#    WriteRegStr HKLM "${REGKEY}\Components" Main 1

    #Make Windows Firewall entry 
    liteFirewall::AddRule "$INSTDIR\${PRODUCT_EXE}" "${PRODUCT_NAME_DISPLAY}"

    SetShellVarContext "current"	#So we get proper $AppData value.
#    MessageBox MB_OK "Adding AppData: ${APPDATA_PATH}"
    CreateDirectory ${APPDATA_PATH}

SectionEnd

Section -post SEC0001
    DeleteRegKey     /ifempty HKCU "${REGKEY}"		#JRT - 2015.08.19 - We do this because of a previous out of sync condition with main App between HKCU and HKLM.  Likely can be removed in a couple months.
    DeleteRegKey     /ifempty HKCU "${REGKEY_ROOT}"	#JRT - 2015.08.19 - We do this because of a previous out of sync condition with main App between HKCU and HKLM.  Likely can be removed in a couple months.

    #Install app in HKLM
    WriteRegStr      HKLM "${REGKEY}" Path $INSTDIR
    SetOutPath       $INSTDIR
    WriteUninstaller $INSTDIR\${UNINSTALL_EXE}
    WriteRegStr      HKLM "${UNINSTALL_REGKEY}" DisplayName     "$(^Name)"
    WriteRegStr      HKLM "${UNINSTALL_REGKEY}" DisplayVersion  "${PRODUCT_VERSION_LONG}"
    WriteRegStr      HKLM "${UNINSTALL_REGKEY}" Publisher       "${PRODUCT_PUBLISHER}"
    WriteRegStr      HKLM "${UNINSTALL_REGKEY}" URLInfoAbout    "${PRODUCT_WEB_SITE}"
    WriteRegStr      HKLM "${UNINSTALL_REGKEY}" DisplayIcon     $INSTDIR\${ICON_NAME}
    WriteRegStr      HKLM "${UNINSTALL_REGKEY}" UninstallString $INSTDIR\${UNINSTALL_EXE}
    WriteRegDWORD    HKLM "${UNINSTALL_REGKEY}" NoModify 1
    WriteRegDWORD    HKLM "${UNINSTALL_REGKEY}" NoRepair 1

    #Set AutoStart.  Just use ${PRODUCT_NAME_DISPLAY}, not "$(^Name)" which includes the version info.
    WriteRegStr      HKCU "${AUTO_START_REGKEY}" "${PRODUCT_NAME_DISPLAY}" "$INSTDIR\${PRODUCT_NAME}.exe"

SectionEnd

# Macro for selecting uninstaller sections
!macro SELECT_UNSECTION SECTION_NAME UNSECTION_ID
    Push $R0
    ReadRegStr $R0 HKLM "${REGKEY}\Components" "${SECTION_NAME}"	#TODO - This can be removed by 12.31.2015.  We have NO components
    StrCmp $R0 1 0 next${UNSECTION_ID}
    !insertmacro SelectSection "${UNSECTION_ID}"
    GoTo done${UNSECTION_ID}
next${UNSECTION_ID}:
    !insertmacro UnselectSection "${UNSECTION_ID}"
done${UNSECTION_ID}:
    Pop $R0
!macroend

# Uninstaller sections
Section /o -un.Main UNSEC0000
    
    # Mark section as complete
    DeleteRegValue HKLM "${REGKEY}\Components" Main		#TODO - This can be removed by 12.31.2015.  We have NO components
SectionEnd

Section -un.post UNSEC0001

    DeleteRegKey   /ifempty HKCU "${REGKEY}"		#JRT - 2015.08.19 - We do this because of a previous out of sync condition with main App between HKCU and HKLM.  Likely can be removed in a couple months.
    DeleteRegKey   /ifempty HKCU "${REGKEY_ROOT}"	#JRT - 2015.08.19 - We do this because of a previous out of sync condition with main App between HKCU and HKLM.  Likely can be removed in a couple months.
    DeleteRegKey   /ifempty HKCU "${AUTO_START_REGKEY}"	#JRT - 2015.08.19

    DeleteRegKey   /ifempty HKLM "${UNINSTALL_REGKEY}"

    Delete         $INSTDIR\${UNINSTALL_EXE}
    DeleteRegValue HKLM "${REGKEY}" Path

    DeleteRegKey   /ifempty HKLM "${REGKEY}\Components"		#TODO - This can be removed by 12.31.2015.  We have NO components
    DeleteRegKey   /ifempty HKLM "${REGKEY}"

    # Remove AppData directory
    SetShellVarContext "current"	#So we get proper $AppData value.
#    MessageBox MB_OK "Removing AppData: ${APPDATA_PATH}"
    RmDir  /r ${APPDATA_PATH}	  

    # Remove application files
    SetShellVarContext "all"		#So we get proper $AppData value.
#    MessageBox MB_OK "Running Files_Uninstall.nsi"
    !include Files_Uninstall.nsi

    RmDir          $INSTDIR
SectionEnd


# Macro to check minimum OS level
!macro hasMinOsLevel
    # Currently Win7 (but we will allow Vista for some internal testing)
#    ${If} ${AtLeastWin7}
    ${If} ${AtLeastWinVista}
      DetailPrint "Yes, it's Win8! oops..."
    ${Else}
      MessageBox MB_OK "${PRODUCT_NAME_DISPLAY} requires Windows 7 or later." # TODO- Translate
      Abort
    ${EndIf}
!macroend


# Macro to check if installer is already running
!define INSTALLER_MUTEX "${PRODUCT_NAME}-Installer"
!macro isInstallerAlreadyRunning
    System::Call 'kernel32::CreateMutexA(i 0, i 0, t "${INSTALLER_MUTEX}") ?e'
    Pop $R0
    StrCmp $R0 0 done2
       IfSilent 0 exitMessage
       Abort
  exitMessage:
      MessageBox MB_OK "$(INSTALLER_ALREADY_RUNNING)"	#THIS DOES *NOT* TRANSLATE!
      Abort
  done2:
!macroend


# Macro to Terminate App
!macro TerminateApp
	FindProcDLL::FindProc "${PRODUCT_EXE}"
    IntCmp $R0 1 0 notRunning
        KillProcDLL::KillProc "${PRODUCT_EXE}"
  notRunning:
!macroend

# Installer functions
Function .onInit
    !insertmacro isInstallerAlreadyRunning
    !insertmacro TerminateApp
    !insertmacro hasMinOsLevel
    InitPluginsDir
    !insertmacro MULTIUSER_INIT
FunctionEnd

# Uninstaller functions
Function un.onInit
    !insertmacro TerminateApp
    SetAutoClose true
    !insertmacro MULTIUSER_UNINIT
    !insertmacro SELECT_UNSECTION Main ${UNSEC0000}
FunctionEnd
