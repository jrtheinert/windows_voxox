
#Remove Desktop icon
Delete $DESKTOP\${PRODUCT_NAME}.lnk

#Remove Start Menu Entries
Delete "$SMPROGRAMS\${PRODUCT_NAME}\*"
RMDir  "$SMPROGRAMS\${PRODUCT_NAME}\"

#Remove Branding resouce files (typically .wav files)
RmDir  /r $INSTDIR\Branding

#Remove DLL
Delete $INSTDIR\*.dll

#Remove C++ Runtime installer
Delete $INSTDIR\vcredist_x86.exe

#Remove Crash Handler
Delete $INSTDIR\BsSndRpt.exe

#Remove PDB
Delete $INSTDIR\*.pdb

#Remove Logging config file
Delete $INSTDIR\log4net.config

#Remove Main Exe & Config
Delete $INSTDIR\${PRODUCT_EXE}.config
Delete $INSTDIR\${PRODUCT_EXE}

#Remove Icon
Delete $INSTDIR\${ICON_NAME}
