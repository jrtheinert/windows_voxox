#TODO Items - JRT - 2015.01.22
# - Verify PRODUCT_NAME
# - Verify PRODUCT_PUBLISHER
# - Verify PRODUCT_WEB_SITE
# - Verify PRODUCT_REGISTRY_PATH (Telcentris?)
# - Do we need PRODUCT_PDB?  We will have many PDB files and separate build flag

#Vars defined on command line
# PRODUCT_VERSION_LONG		e.g. 3.0.0.0764 	For use in detailed build identification
# PRODUCT_VERSION_SHORT		e.g. 3.0.0		For display to user
# INSTALLER_NAME		e.g. VoicePass-3.0.0.0764-setup-release.exe

#Company Information & Version
!define PRODUCT_NAME                    "Voxox"	#No spaces please
!define PRODUCT_NAME_DISPLAY            "Voxox"
!define PRODUCT_PUBLISHER               "Telcentris, Inc."
!define PRODUCT_WEB_SITE                "https://extranet.telcentris.com/vxlogin"
!define PRODUCT_PATH                    "${PRODUCT_NAME}" #Install root path in Program Files

!define PRODUCT_REGISTRY_PATH_ROOT      "Telcentris"
!define PRODUCT_REGISTRY_PATH           "Telcentris\${PRODUCT_NAME}"

!define PRODUCT_EXE                     "${PRODUCT_NAME}.exe"
!define PRODUCT_PDB                     "${PRODUCT_NAME}.pdb"
!define PRODUCT_FULLPATH                "${PRODUCT_PATH}\${PRODUCT_EXE}"
!define PRODUCT_VERSION_COMMENT         "Voxox"
!define PRODUCT_VERSION_TRADEMARK       "Copyright (c) 2015 ${PRODUCT_PUBLISHER}"

!define PRODUCT_ICON_SOURCE             "Brands\Voxox\App.ico"
!define PRODUCT_SIDE_BANNER		"Brands\Voxox\SideBanner.bmp"
!define PRODUCT_TOP_BANNER		"Brands\Voxox\TopBanner.bmp"

#Call the core
!include CoreInstaller.nsi