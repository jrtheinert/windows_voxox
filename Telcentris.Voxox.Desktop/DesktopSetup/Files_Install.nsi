#TODO Items - JRT - 2015.01.22
# - We only want to include PDB files if we explicitly do a build to include them.
# - I think renaming the Desktop.pdb to <productName>.pdb will make it inaccessible for debugging.
# - Move vcredist_x86.exe from main project to NSIS directory.

#Install Directory
SetOutPath   $INSTDIR
SetOverwrite on

#Icon
File "/oname=${ICON_NAME}" ${PRODUCT_ICON_SOURCE}

#Main Application & Config file. NOTE that these files are Renamed by this code.
File "/oname=${PRODUCT_EXE}"        ${SOURCE_PATH}\Desktop.exe
File "/oname=${PRODUCT_EXE}.config" ${SOURCE_PATH}\Desktop.exe.config

#Logging Config file
File ${SOURCE_PATH}\log4net.config

#Support DLLs
File ${SOURCE_PATH}\*.dll

#Crash Handler
File ${SOURCE_PATH}\BsSndRpt.exe

#PDB Files
File   ${SOURCE_PATH}\*.pdb
File   "/oname=${PRODUCT_PDB}" ${SOURCE_PATH}\Desktop.pdb
Delete $INSTDIR\Desktop.pdb

#Call related Wav files
SetOutPath $INSTDIR\Branding\Calling
File       ${SOURCE_PATH}\Branding\Calling\*.wav

#DialPad related Wav files
SetOutPath $INSTDIR\Branding\DialPad
File       ${SOURCE_PATH}\Branding\DialPad\*.wav

#Message related Wav files
SetOutPath $INSTDIR\Branding\Messages
File       ${SOURCE_PATH}\Branding\Messages\*.wav

# Start Menu Shortcuts
CreateDirectory "$SMPROGRAMS\${PRODUCT_NAME}"
CreateShortCut  "$SMPROGRAMS\${PRODUCT_NAME}\${PRODUCT_NAME}.lnk"           "$INSTDIR\${PRODUCT_EXE}"
CreateShortCut  "$SMPROGRAMS\${PRODUCT_NAME}\Uninstall ${PRODUCT_NAME}.lnk" "$INSTDIR\${UNINSTALL_EXE}"

#Desktop Shortcut
SetOutPath     $DESKTOP
CreateShortcut "$DESKTOP\${PRODUCT_NAME}.lnk" "$INSTDIR\${PRODUCT_EXE}" "" "$INSTDIR\${ICON_NAME}"

