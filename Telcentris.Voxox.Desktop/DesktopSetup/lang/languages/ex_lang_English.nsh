!define EXTENDED_LANG  "ENGLISH"
;
; Add Macro text strings similar to this example.
;!insertmacro EXTENDED_LANG_STRING BONJOUR_INTRO "Bonjour, is an open, standards-based networking technology that allows for dynamic discovery of services without any user configuration (also known as zero-configuration networking).\r\n\r\nInstallation of the Bonjour service (mDNSResponder.exe) is a necessary component for the best performance of this software.  For more information about Bonjour technology, please visit:"

!insertmacro EXTENDED_LANG_STRING APP_ALREADY_RUNNING     "$(^Name) is running. Please stop it and try again."
!insertmacro EXTENDED_LANG_STRING INSTALLER_ALREADY_RUNNING "The $(^Name) installer is already running."

;
!undef EXTENDED_LANG
