#PowerShell file to build installer(s).
# Requirements are:
#  + Determine product version info x.x.x from Desktop.exe assembly
#  + Determine SVN revision from SVN command-line (IT appears we can use FileVersion info)
#  + Generate NSIS command-line with needed defines, filenames, paths, .nsi files, etc.
#    + Include option for .PDB files.
#  - Sign the created installers.

# Mandatory parameter for BrandName, e.g. Voxox, VoicePass/HP, etc.
param([String]$BrandName="VoicePass",    #Remove this test default value.
      [String]$BuildType="release")      #Default is 'release'.  Options include 'debug', 'release-pdb'

#Some parameter error checks
if ( [string]::IsNullOrEmpty( $BrandName ) )
{
    ECHO "ERROR: You must provide a valid Brand Name."
    Exit
}

if ( [string]::IsNullOrEmpty( $BuildType ) )
{
    $BuildType = 'release'
}

if ( $BuildType -ne 'release' -and
     $BuildType -ne 'debug'   -and
     $BuildType -ne 'release-pdb' )
{
    ECHO "ERROR: BuildType is invalid.  Must be 'release', 'debug' or 'release-pdb'."
    Exit
}

#Let's start building the command line parameters
CD D:\Dev\Telcentris\_DesktopApp\Telcentris.Voxox.Desktop

$NsisPath = '"C:\Dev\Program Files (x86)\NSIS\makensis.exe"'  #Change this for YOUR dev environment

$ExePath   = "Desktop\bin\release\Desktop.exe"
$AppVerRaw = (get-item $ExePath).VersionInfo.FileVersion  #Used only for debugging

ECHO "exepath   = $ExePath"
ECHO "appverRaw = $AppVerRaw"

#NOTE: we want 4-char revision so need to pad left with 0.
# $AppVerRaw is ONLY for debugging.

#NSIS script name
$NsisScript = "Installer." + $BrandName + ".nsi"   #This may change based on where we start the script.
$Prod1 = (get-item $ExePath).VersionInfo.FileMajorPart.ToString()
$Prod2 = (get-item $ExePath).VersionInfo.FileMinorPart.ToString()
$Prod3 = (get-item $ExePath).VersionInfo.FileBuildPart.ToString()
$Prod4 = (get-item $ExePath).VersionInfo.FilePrivatePart.ToString().PadLeft( 4, "0" ) #So we get '0xxx'

$AppVer = $Prod1 + '.' + $Prod2 + '.' + $Prod3 + '.' + $Prod4

#Installer name
$InstallerName = $BrandName + "-" + $AppVer + "-setup-" + $BuildType + ".exe"
$InstallerNameDef = "/DINSTALLER_NAME=" + $InstallerName

#Command line parameters
$ProdVerLongDef  = "/DPRODUCT_VERSION_LONG="  + $Prod1 + '.' + $Prod2 + '.' + $Prod3 + '.' + $Prod4
$ProdVerShortDef = "/DPRODUCT_VERSION_SHORT=" + $Prod1 + '.' + $Prod2 + '.' + $Prod3

$ProdDefs = $ProdVerLongDef + " " + $ProdVerShortDef

ECHO "prod defs = $ProdDefs"

$NsisDefs = $ProdDefs + " " + $InstallerNameDef

#TODO:
# - BUILD_DIR maybe
# - BuildType as define to control inclusion/exclusion of .PDB files


#Create NSIS command-line
#$BuildCmd = "PowerShell.exe -Command {" + $NsisPath + " " + $NsisDefs + " " + $NsisScript + "}"
$BuildCmd = "'" + $NsisPath + ' ' + $NsisDefs + ' ' + $NsisScript + "'"

ECHO "build cmd = $BuildCmd"

#Execute
invoke-expression $BuildCmd

