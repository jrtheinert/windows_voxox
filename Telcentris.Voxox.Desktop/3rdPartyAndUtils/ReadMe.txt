This is where we will add:
 - 3rd party software, DLLS, etc. such as NSIS plugins.
 - Utility programs, dlls, etc. which we must build occassionally

1. NSIS liteFirewall
   - This is available on Internet in compiled form, but it does NOT work.
     So I have added the VS .sln and created DLLs

2. Other NSIS plugins - I think what is on the web works, so no need to add them here,
   though it may be a good idea to do so.