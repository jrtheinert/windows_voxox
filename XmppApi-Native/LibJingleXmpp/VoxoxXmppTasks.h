/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Oct 2014
 */

#pragma once

#include <string>

#include "webrtc/base/sigslot.h"
#include "talk/xmpp/xmpptask.h"


//=============================================================================
//Simple class to:
//	- parse XML for we are interested in
//	- use data for internal handling
//	- provide a structure for callbacks to app
class XmppMsg
{
private:
	enum Type
	{ 
		None				= 0,
		DeliveredReceipt	= 1,	
		ReadReceipt			= 2,	
		ChatState			= 3,
		Msg					= 4,	//Implies ChatState
		Carbon				= 5,	//Implies Msg
	};

public:
	XmppMsg();
	~XmppMsg();

	//Sets -----------------------------------------------------------------------
	void setType		( Type                      value )						{ mType         = value;	}
	void setTo		    ( const std::string&		value )						{ mTo			= value;	}
	void setFrom	    ( const std::string&		value )						{ mFrom			= value;	}
	void setBody        ( const std::string&		value )						{ mBody			= value;	}
	void setMessageId   ( const std::string&		value )						{ mMessageId	= value;	}
	void setChatState   ( const std::string&		value )						{ mChatState	= value;	}
	void setFromResource( const std::string&		value )						{ mFromResource	= value;	}
	void setMyResource  ( const std::string&		value )						{ mMyResource	= value;	}
	void setReceiptMsgId( const std::string&		value )						{ mReceiptMsgId	= value;	}

//	void setErrorCode  ( const std::string&			value )						{ mErrorCode = value;   }
//	void setErrorType  ( const std::string&			value )						{ mErrorType = value;   }

	//Gets -----------------------------------------------------------------------
	Type						getType    ()		const						{ return mType;			}
	std::string					getTo	    ()		const						{ return mTo;			}
	std::string					getFrom	    ()		const						{ return mFrom;			}
	std::string					getBody	    ()		const						{ return mBody;			}
	std::string					getMessageId()		const						{ return mMessageId;	}
	std::string					getChatState()		const						{ return mChatState;	}
	std::string					getFromResource()	const						{ return mFromResource; }
	std::string					getMyResource()		const						{ return mMyResource;   }
	std::string					getReceiptMsgId()	const						{ return mReceiptMsgId;  }

//	std::string					getErrorCode()	const							{ return mErrorCode;	}
//	std::string					getErrorType()	const							{ return mErrorType;	}

	//Misc -----------------------------------------------------------------------
	XmppMsg& operator= ( const XmppMsg& src );

	void setTypeDeliveredReceipt()				{ mType = Type::DeliveredReceipt;	}
	void setTypeReadReceipt()					{ mType = Type::ReadReceipt;		}
	void setTypeChatState()						{ mType = Type::ChatState;			}
	void setTypeMsg()							{ mType = Type::Msg;				}
	void setTypeCarbon()						{ mType = Type::Carbon;				}

	bool isTypeDeliveredReceipt()	const		{ return mType == Type::DeliveredReceipt;	}
	bool isTypeReadReceipt()		const		{ return mType == Type::ReadReceipt;		}
	bool isTypeChatState()			const		{ return mType == Type::ChatState;			}
	bool isTypeMsg()				const		{ return mType == Type::Msg;				}
	bool isTypeCarbon()				const		{ return mType == Type::Carbon;				}

private:
	Type						mType;
	std::string					mTo;
	std::string					mFrom;
	std::string					mBody;
	std::string					mMessageId;
	std::string					mChatState;
	std::string					mFromResource;
	std::string					mMyResource;
	std::string					mReceiptMsgId;

//	std::string					mErrorCode;
//	std::string					mErrorType;
};

//=============================================================================


//=============================================================================

 class XmppSendTask : public buzz::XmppTask
 {
 public:
	explicit XmppSendTask( buzz::XmppTaskParentInterface* parent );

	buzz::XmppReturnStatus sendMessage        ( const buzz::Jid& to, const std::string& body, const std::string& msgId );
	buzz::XmppReturnStatus sendChatState      ( const buzz::Jid& to, bool composing );
	buzz::XmppReturnStatus sendDeliveryReceipt( const buzz::Jid& to, const std::string& msgId );
	buzz::XmppReturnStatus sendReadReceipt    ( const buzz::Jid& to, const std::string& msgId );

	void requestVCard( const buzz::Jid& to );

	//In case we want to override our default.
	static void setMsgIdPrefix( const std::string& value )		{ s_msgIdPrefix = value; }

protected:
	int  ProcessStart();
	bool HandleStanza( const buzz::XmlElement* stanza );
	
	std::string generateMsgId( const std::string& msgIdIn );

 private:
	buzz::XmlElement* makeMessageXmlElement( const buzz::Jid& to, const std::string& msgIdIn, bool isReceipt );
	std::string generateUuid();

 private:
	int mNextMsgId;

	static std::string s_msgIdPrefix;
};

//=============================================================================


//=============================================================================
 
// A task to receive MESSAGE (IM and ChatState) from the XMPP server.
class MessageReceiveTask : public buzz::XmppTask 
{
public:
	explicit MessageReceiveTask( XmppTaskParentInterface* parent );

	virtual ~MessageReceiveTask();

	virtual int ProcessStart();		// Starts pulling queued status messages and dispatching them to the SignalXmppMsg() callback.

	sigslot::signal1<const XmppMsg&> SignalXmppMsg;

protected:
	virtual bool HandleStanza( const buzz::XmlElement* stanza );

private:
	// Handles Message stanzas by converting the data to XmppMsg objects 
	//	and passing those along to the SignalIncomingMessage() callback.
	void HandleMessage( const buzz::XmlElement* stanza );

	// Extracts Message information for the Message stanza sent from the server.
	XmppMsg		DecodeMessage( const buzz::XmlElement* stanza );
	std::string	ChatStateFromXmlElement( const buzz::XmlElement* stanza );
	bool		CheckForCarbons( const buzz::XmlElement* stanza, XmppMsg& xmppMsg );
	bool		CheckForReceipt( const buzz::XmlElement* stanza, XmppMsg& xmppMsg );
};

//=============================================================================


//=============================================================================
 
// A task to receive IQ from the XMPP server. WIP
class IqReceiveTask : public buzz::XmppTask 
{
public:
	explicit IqReceiveTask( XmppTaskParentInterface* parent );

	virtual ~IqReceiveTask();

	virtual int ProcessStart();		// Starts pulling queued status messages and dispatching them to the SignalXmppMsg() callback.

	sigslot::signal1<const XmppMsg&> SignalIqMsg;

protected:
	virtual bool HandleStanza( const buzz::XmlElement* stanza );

private:
	// Handles Message stanzas by converting the data to XmppMsg objects 
	//	and passing those along to the SignalIncomingMessage() callback.
	void HandleMessage( const buzz::XmlElement* stanza );

	// Extracts Message information for the Message stanza sent from the server.
	static XmppMsg		DecodeMessage( const buzz::XmlElement* stanza );
};

//=============================================================================
