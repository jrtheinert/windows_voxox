/*
 * By J. R. Theinert, VOXOX 2016.02.02
 */

#ifndef TALK_XMPP_SASL_DIGEST_MD5_MECHANISM_H_
#define TALK_XMPP_SASL_DIGEST_MD5_MECHANISM_H_

#include "saslscramsha1mechanism.h"

//#include "webrtc/libjingle/xmllite/xmlelement.h"
#include "talk/xmpp/constants.h"

// See http://tools.ietf.org/html/rfc6120#section-6.4.6 Section 9.1.2 for example

namespace buzz 
{

SaslScramSha1Mechanism::SaslScramSha1Mechanism( const buzz::Jid jid, const std::string& password )
	:	SaslMechanism(),
	    mechanism_( "SCRAM-SHA-1" ),
		user_jid_ ( jid ),
		password_( password )
	{
		username_ = user_jid_.node();

		client_ = IScramSha1Client::CreateInstance();

		client_->initialize( username_, password_ );
	}

//SaslScramSha1Mechanism::SaslScramSha1Mechanism( const buzz::Jid jid, const std::string& password, int cnonce_byte_length )
//	:	mechanism_( "SCRAM-SHA-1" ),
//		user_jid_ ( jid ),
//		password_( password )
//	{
//	}

	XmlElement* SaslScramSha1Mechanism::StartSaslAuth() 
	{
		// send initial request
		XmlElement* el = new XmlElement(QN_SASL_AUTH, true);
		el->AddAttr(QN_MECHANISM, mechanism_);

		std::string firstMsg = client_->getClientFirstMessage();

		el->AddText(Base64Encode(firstMsg));
	
		return el;
	}

	// Should respond to a SASL "<challenge>" request.  
	// Default is to abort (for mechanisms that do not do challenge-response)
	XmlElement* SaslScramSha1Mechanism::HandleSaslChallenge( const XmlElement* challenge )
	{
		XmlElement* response = nullptr;

		if ( challenge != nullptr )
		{
			std::string content1 = challenge->BodyText();
			std::string content2 = Base64Decode( challenge->BodyText() );

			std::string clientFinalMessage = client_->getClientFinalMessage( content2 );

			//Create 'response'
			response = new XmlElement(QN_SASL_RESPONSE, true);
			response->AddText( Base64Encode( clientFinalMessage ) );
		}

		return response;
	}

	// Notification of a SASL "<success>".  Sometimes information is passed on success.
	void SaslScramSha1Mechanism::HandleSaslSuccess( const XmlElement* success )
	{
		if ( success != nullptr )
		{
			std::string content1 = success->BodyText();
			std::string content2 = Base64Decode( success->BodyText() );

			client_->verifyServerSignature( content2 );
		}
	}

	// Notification of a SASL "<failure>".  Sometimes information for the user is passed on failure.
	void SaslScramSha1Mechanism::HandleSaslFailure( const XmlElement* failure )
	{
	}

}	//namespace buzz

#endif  // TALK_XMPP_SASL_DIGEST_MD5_MECHANISM_H_
