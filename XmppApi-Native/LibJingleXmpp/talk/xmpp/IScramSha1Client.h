
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Feb 2016
 */

//Interface to hide the managed implementation from the dll/lib client.

#pragma once

#include "DllExport.h"
#include <string>

class IScramSha1Client
{
public:
	virtual void		setUseTestVector( bool value ) = 0;
	virtual void		initialize( const std::string userName, const std::string password ) = 0;

	virtual std::string getClientFirstMessage() = 0;
	virtual std::string getClientFinalMessage( const std::string serverFirstMsg ) = 0;
	virtual bool        verifyServerSignature( const std::string serverFinalMsg ) = 0;

	//Factory methods
	DLLEXPORT static IScramSha1Client* CreateInstance();
	DLLEXPORT static void			   Destroy( IScramSha1Client* instance );
};
