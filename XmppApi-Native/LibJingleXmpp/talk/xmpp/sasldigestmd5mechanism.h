/*
 * By J. R. Theinert, VOXOX 2016.02.02
 *
 * Based on LibJingle saslcookiemechanism.h and saslplainmechanism.h
 *  and this helpful web site: http://web.archive.org/web/20050224191820/http://cataclysm.cx/wip/digest-md5-crash.html
 * RFC-2831: https://www.ietf.org/rfc/rfc2831.txt
 *
 *	Sample with test values: http://www.deusty.com/2007/09/example-please.html
 */

#ifndef TALK_XMPP_SASL_DIGEST_MD5_MECHANISM_H_
#define TALK_XMPP_SASL_DIGEST_MD5_MECHANISM_H_

#include "webrtc/libjingle/xmllite/qname.h"
#include "webrtc/libjingle/xmllite/xmlelement.h"
#include "talk/xmpp/constants.h"
#include "talk/xmpp/saslmechanism.h"
#include "webrtc/base/messagedigest.h"
#include "webrtc/base/stringencode.h"

namespace buzz 
{

class SaslDigestMD5Mechanism : public SaslMechanism 
{
public:
	//For cnonce generation
	struct rnd_gen 
	{ 
		//For our purposes, alpha chars MUST be lowercase.
//		rnd_gen(char const* range = "abcdefghijklmnopqrstuvwxyz0123456789") : range(range), len(std::strlen(range))		{}
		rnd_gen(char const* range = "0123456789") : range(range), len(std::strlen(range))		{}

		char operator ()() const 
		{
			return range[static_cast<std::size_t>(std::rand() * (1.0 / (RAND_MAX + 1.0 )) * len)];
		}

	private:
		char const* range;
		std::size_t len;
	};

	//SaslDigestMD5Mechanism(const std::string& mechanism, const std::string& username,  const std::string& password )
	//:	mechanism_(mechanism),
	//	username_ (username),
	//	password_ ( password )
	//{
	//	initKeys();
	//}

	//SaslDigestMD5Mechanism( const buzz::Jid jid,  const rtc::CryptString& encryptedPassword )
	//:	mechanism_( "DIGEST-MD5" ),
	//	user_jid_ ( jid ),
	//	encryptedPassword_( encryptedPassword )
	//{
	//	initKeys();
	//}

	SaslDigestMD5Mechanism( const buzz::Jid jid,  const std::string& password, const std::string& server )
	:	mechanism_( "DIGEST-MD5" ),
		user_jid_ ( jid ),
		password_( password ),
		server_(server)
	{
		initKeys();
	}

	virtual std::string GetMechanismName() { return mechanism_; }

	virtual XmlElement* StartSaslAuth() 
	{
		// send initial request
		XmlElement * el = new XmlElement(QN_SASL_AUTH, true);
		el->AddAttr(QN_MECHANISM, mechanism_);

		//TODO: Flesh this out per doc
		//if (!token_service_.empty()) {
		//	el->AddAttr(QN_GOOGLE_AUTH_SERVICE, token_service_);
		//}

		//std::string credential;

		//credential.append("\0", 1);
		//credential.append(username_);
		//credential.append("\0", 1);
		//
		//el->AddText(Base64Encode(credential));
		
		el->AddText( "=" );
	
		return el;
	}

	// Should respond to a SASL "<challenge>" request.  
	// Default is to abort (for mechanisms that do not do challenge-response)
	XmlElement* HandleSaslChallenge( const XmlElement* challenge )
	{
		XmlElement* response = nullptr;

		if ( challenge != nullptr )
		{
			//Parse 'challenge' to get elements we need for 'response'.
			if ( parseChallenge( challenge ) )
			{
				determineResponseValues();
				std::string text = makeResponseText();

				//Create 'response'
				response = new XmlElement(QN_SASL_RESPONSE, true);	//TODO: what does true do?
				response->AddText( Base64Encode( text ) );

				std::string test = Base64Decode( response->BodyText() );

				bool same = ( test == text );

			}
		}

		return response;
	}

	// Notification of a SASL "<success>".  Sometimes information is passed on success.
	void HandleSaslSuccess(const XmlElement * success)
	{
	}

	// Notification of a SASL "<failure>".  Sometimes information for the user is passed on failure.
	void HandleSaslFailure(const XmlElement * failure)
	{
	}

private:
	bool parseChallenge( const XmlElement* challenge )
	{
		bool result = true;

		if ( challenge == nullptr )
		{
			result = false;
		}
		else
		{
			//Decode base64 content to something like this: 
			//	realm="cataclysm.cx",nonce="OA6MG9tEQGm2hh",qop="auth",charset=utf-8,algorithm=md5-sess
			std::string content = Base64Decode( challenge->BodyText() );

			if ( content.empty() )
			{
				result = false;
			}
			else
			{
				size_t		startPos = 0;
				size_t		endPos   = 0;
				std::string kvp;

				while ( startPos < content.size() && result )
				{
					size_t pos = content.find( pairSep_, startPos );

					//Get next/last kvp.
					if ( pos == std::string::npos )
					{
						kvp      = content.substr( startPos );
						startPos = content.size();
					}
					else
					{
						int len = pos - startPos;	//Do NOT include the pairSep.

						kvp      = content.substr( startPos, len );
						startPos = pos + pairSep_.size();
					}

					if ( kvp.empty() )		//Should never happen.
					{
						result = false;
					}
					else
					{
						size_t sepPos = kvp.find( kvpSep_ );

						if ( sepPos == std::string::npos )
						{
							result = false;
						}
						else
						{
							std::string key   = kvp.substr( 0, sepPos );
							std::string value = kvp.substr( sepPos + kvpSep_.size() );

							//NOTE: 'nonce' and 'qop' are quoted.
							//		'charset' and 'algorithm' are unquoted.
							//		Not sure about 'realm' since I do not see that in challege from Voxox server.

							value = cleanValue( value );

							if		( key == keyRealm_ )
							{
								realms_ = value;		//TODO: Could have zero or more.
							}
							else if ( key == keyNonce_ )
							{
								nonce_ = value;
							}
							else if ( key == keyQop_ )
							{
								qop_ = value;
							}
							else if ( key == keyCharset_ )
							{
								charset_ = value;
							}
							else if ( key == keyAlgorithm_ )
							{
								algorithm_ = value;
							}
						}
					}
				}	//while

				//Validate that we have required values
				if ( result )
				{
					result = !nonce_.empty() && !algorithm_.empty();
				}
			}
		}

		return result;
	}

	std::string cleanValue( const std::string& value )
	{
		const char quote = '"';

		std::string result;

		if ( value.at(0) == quote && value.at( value.size()-1) == quote )
		{
			int len = value.size() - 2;
			result = value.substr( 1, len );
		}
		else
		{
			result = value;
		}

		return result;
	}


	void determineResponseValues()
	{
		if ( !realms_.empty() )
		{
			if ( realms_.find( user_jid_.domain() ) != std::string::npos )
			{
				realm_ = user_jid_.domain();		//TODO: Is this good enough?  Currently we have empty 'realms_', so not a big deal.
			}
		}

		username_  = user_jid_.node();
		cnonce_    = generateNonce( 8 );	//Tested with Base64 encoding.  Spec does NOT specifiy Base64, but since nothing else works...  That did not work either.
		nc_		   = "00000001";
		servType_  = "xmpp";
		host_	   = server_;						//Set to hostname of the remote server
		digestUri_ = servType_ + "/" + host_;
//		authZid_   = user_jid_.Str() + "/Test001";	//If used, and server does not support, then auth will fail. Omit for now.

		//Must be last since it depends on other values.
		response_  = generateResponseValue2();
	}

	std::string makeResponseText()
	{
		//Create text of 'response' looking something like this:
		//	username="rob",realm="cataclysm.cx",nonce="OA6MG9tEQGm2hh",cnonce="OA6MHXh6VqTrRk",nc=00000001,qop=auth,digest-uri="xmpp/cataclysm.cx",response=d388dad90d4bbd760a152321f2143af7,charset=utf-8,authzid="rob@cataclysm.cx/myResource"

		std::string result;

		result += makeResponseKvp( keyUsername_,  username_,  true  );
		result += makeResponseKvp( keyRealm_,     realm_,     true  );
		result += makeResponseKvp( keyNonce_,     nonce_,     true  );
		result += makeResponseKvp( keyCNonce_,    cnonce_,    true  );

		result += makeResponseKvp( keyNC_,        nc_,		  false );
		result += makeResponseKvp( keyQop_,       qop_,		  false );
		result += makeResponseKvp( keyDigestUri_, digestUri_, true  );

		result += makeResponseKvp( keyResponse_,  response_,  false );
		result += makeResponseKvp( keyCharset_,   charset_,   false );
		result += makeResponseKvp( keyAuthZid_,   authZid_,   true  );

		//Remove trailing pairSep_
		size_t expectedPos = result.size() - pairSep_.size();
		size_t pos = result.rfind( pairSep_ );

		if ( pos == expectedPos )
			result = result.substr( 0, expectedPos );

		return result;
	}

	std::string makeResponseKvp( const std::string& key, const std::string& value, bool quoteValue )
	{
		std::string result;

		if ( !value.empty() )
		{
			std::string quote  = quoteValue ? "\"" : "";
			
			result = key + kvpSep_ + quote + value + quote + pairSep_;
		}

		return result;
	}

	//This is where the magic happens.
	// Result is a string of 32 hex digits.  Alpha chars MUST be lowercase.
	std::string generateResponseValue2()
	{
		//TODO: 'username', 'realm' and 'password' are ASSUMED to be UTF-8.
		//	Check 'charset' for 'UTF-8' to determine if translation is needed to ISO 8859-1.
		std::string A1;
		
		if ( realm_.empty() )
			A1 = username_ + ":" + password_;
		else
			A1 = username_ + ":" + realm_ + ":" + password_;

		if ( !authZid_.empty() )
			A1 += ":" + authZid_;

		std::string A2;

		if		( qop_.empty() )
		{
			A2 = "";		//TODO: Error?
		}
		else if ( qop_ == "auth")
		{
			A2 = "AUTHENTICATE:" + digestUri_;
		}
		else if ( qop_ == "auth-int" || 
				  qop_ == "auth-conf" )
		{
			A2 = "AUTHENTICATE:" + digestUri_ + ":00000000000000000000000000000000";
		}

		std::string middle1 = nonce_ + ":" + cnonce_;								//Used with A1 to determine HA1
		std::string middle2 = nonce_ + ":" + nc_ + ":" + cnonce_ + ":" + qop_;		//Used with HA1 and HA2 to determine KD.

		//Override some values for testing.  COMMENT when testing is complete.
//		A1		= "test:osXstream.local:secret";									//Expected hash is "3a4f5725a748ca945e506e30acd906f0". 
//		A2		= "AUTHENTICATE:xmpp/osXstream.local";								//Expected hash is "2b09ce6dd013d861f2cb21cc8797a64d".
//		middle1 = "392616736:05E0A6E7-0B7B-4430-9549-0FE1C244ABAB";					//Expected hash is "b9709c3cdb60c5fab0a33ebebdd267c4".	//Nonce and cnonce.				
//		middle2 = "392616736:00000001:05E0A6E7-0B7B-4430-9549-0FE1C244ABAB:auth";	//Expected hash is "37991b870e0f6cc757ec74c47877472b".	//Nonce, nc, cnonce and qop.	

		std::string HA1;
		std::string HA2;
		
		char tempBuff[512];
		char outBuff[512];
		int inLen = 0;
		size_t tempLen = 0;

		//Get hash for A1, but do NOT convert to string
		memcpy( tempBuff, A1.c_str(), A1.size() );
		inLen = A1.size();
		tempBuff[inLen] = '\0';
		tempLen = rtc::ComputeDigest( "md5",  tempBuff, inLen, outBuff, 512 );

		if ( algorithmIsMd5() )
		{
			//HA1=MD5(username:realm:password)
			HA1 = rtc::hex_encode( outBuff, tempLen );					//Expected testing hash is "3a4f5725a748ca945e506e30acd906f0". 
		}
		else if ( algorithmIsMd5Sess())
		{
			//HA1=MD5(MD5(username:realm:password):nonce:cnonce)

			//This is just to see if converted string matches test values.  Comment when testing is done.
//			std::string temp1 = rtc::hex_encode( outBuff, tempLen );	//Expected testing hash is "3a4f5725a748ca945e506e30acd906f0". 

			//Append A1 hash with 'middle1' and determine hash as string.
			inLen = tempLen + middle1.size() + 1;
			memcpy( tempBuff, outBuff, tempLen );
			memcpy( tempBuff + tempLen, ":", 1 );
			memcpy( tempBuff + tempLen + 1, middle1.c_str(), middle1.size() );
			tempBuff[inLen] = '\0';

			tempLen = rtc::ComputeDigest( "md5", tempBuff, inLen, outBuff, 512 );
			outBuff[tempLen] = '\0';

			HA1 = rtc::hex_encode( outBuff, tempLen );		//Expected testing hash is "b9709c3cdb60c5fab0a33ebebdd267c4". 
		}

		HA2 = rtc::MD5( A2 );								//Expected testing hash is "2b09ce6dd013d861f2cb21cc8797a64d".

		//Create a string of the form "HA1:nonce:nc:cnonce:qop:HA2", aka 'middle2'. Call this string KD.
		std::string KD = HA1 + ":" + middle2 + ":" + HA2;

		//Compute the 32 hex digit MD5 hash of KD. Call the result Z.
		std::string Z = rtc::MD5( KD );						//Expected testing hash is "37991b870e0f6cc757ec74c47877472b".

		//The resultant string Z should be sent to the server as the value of the "response" directive. 
		return Z;
	}

	void initKeys()
	{
		pairSep_		= ",";
		kvpSep_			= "=";

		keyRealm_		= "realm";
		keyNonce_		= "nonce";
		keyCharset_		= "charset";

		keyQop_			= "qop";
		keyAlgorithm_	= "algorithm";

		keyUsername_	= "username";
		keyCNonce_		= "cnonce";
		keyNC_			= "nc";
		keyServType_	= "serv-type";
		keyHost_		= "host";
		keyDigestUri_	= "digest-uri";
		keyResponse_	= "response";
		keyAuthZid_		= "authzid";
	}

	// Must be Base64 or hex, implementation dependent.  No double-quote chars unless escaped.	
	// Recommended at least 64 bits (8 bytes) of entropy.
	std::string generateNonce( size_t size )
	{
		char* buf = new char[size+1];
		size_t len = std::strlen( buf );

		std::generate_n( buf, len, rnd_gen() );
		buf[len] = '\0';

		return std::string( buf );
	}

	bool algorithmIsMd5()
	{
		return ( algorithm_.empty() ||
			     algorithm_ == "MD5" ||
				 algorithm_ == "md5" );
	}

	bool algorithmIsMd5Sess()
	{
		return ( algorithm_ == "MD5-sess" ||
				 algorithm_ == "md5-sess" );
	}

private:
	Jid			user_jid_;
	std::string password_;
	std::string server_;
	std::string mechanism_;

	//Values in the server challenge
	std::string realms_;	//zero or more
	std::string nonce_;		//one
	std::string qop_;		//zero or one, containing one or more of the following: auth, auth-int, auth-conf.  We must pick one in response
	std::string charset_;	//zero or one
	std::string algorithm_;	//one

	//Values for the response
	std::string	username_;	//one	//From JID.
	std::string realm_;		//zero or one. Derived from realms_ from server challenge.
	std::string cnonce_;	//one	//Use generateNonce()
	std::string nc_;		//one	//For now, always '00000001'.
	std::string servType_;	//one	//For our purposes, 'xmpp'
	std::string host_;		//one	//DNS host name (preferred), or IP.
	std::string digestUri_;	//one
	std::string response_;	//one
	std::string authZid_;	//one

	//Separators
	std::string pairSep_;
	std::string kvpSep_;

	//Keys
	std::string keyRealm_;		//Challenge, Response
	std::string keyNonce_;		//Challenge, Response
	std::string keyCharset_;	//Challenge, Response

	std::string keyQop_;		//Challenge
	std::string keyAlgorithm_;	//Challenge

	std::string keyUsername_;	//Response
	std::string keyCNonce_;		//Response
	std::string keyNC_;			//Response
	std::string keyServType_;	//Response
	std::string keyHost_;		//Response
	std::string keyDigestUri_;	//Response
	std::string keyResponse_;	//Response
	std::string keyAuthZid_;	//Response

};	//class

}	//namespace buzz

#endif  // TALK_XMPP_SASL_DIGEST_MD5_MECHANISM_H_
