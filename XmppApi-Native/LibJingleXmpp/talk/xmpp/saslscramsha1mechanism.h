/*
 * By J. R. Theinert, VOXOX 2016.02.02
 *
 * RFC-2831: https://www.ietf.org/rfc/rfc2831.txt
 *
 * See https://gist.github.com/markokr/4654875 for test vectors
 *
 *
 */

#ifndef TALK_XMPP_SASL_SCRAM_SHA1_MECHANISM_H_
#define TALK_XMPP_SASL_SCRAM_SHA1_MECHANISM_H_

#include "webrtc/libjingle/xmllite/xmlelement.h"
#include "talk/xmpp/jid.h"
#include "talk/xmpp/saslmechanism.h"
#include "talk/xmpp/IScramSha1Client.h"	//Voxox header

namespace buzz 
{

class SaslScramSha1Mechanism : public SaslMechanism 
{
public:
	SaslScramSha1Mechanism( const buzz::Jid jid, const std::string& password );

//	SaslScramSha1Mechanism( const buzz::Jid jid, const std::string& password, int cnonce_byte_length );

	virtual std::string GetMechanismName() { return mechanism_; }

	virtual XmlElement* StartSaslAuth();

	// Should respond to a SASL "<challenge>" request.  
	// Default is to abort (for mechanisms that do not do challenge-response)
	XmlElement* HandleSaslChallenge( const XmlElement* challenge );

	// Notification of a SASL "<success>".  Sometimes information is passed on success.
	void HandleSaslSuccess(const XmlElement * success);

	// Notification of a SASL "<failure>".  Sometimes information for the user is passed on failure.
	void HandleSaslFailure(const XmlElement * failure);

private:
	std::string mechanism_;

	Jid			user_jid_;
	std::string	username_;
	std::string password_;

	IScramSha1Client*	client_;
};	//class

}	//namespace buzz

#endif  // TALK_XMPP_SASL_SCRAM_SHA1_MECHANISM_H_
