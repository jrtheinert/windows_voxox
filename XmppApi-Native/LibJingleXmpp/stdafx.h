// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#pragma message( "Include winsock2" )

#include <WinSock2.h>				//Force this to be included before winsock.h (thought he WIN32_LEAN_AND_MEAN above should do that.
#include <windows.h>



// TODO: reference additional headers your program requires here
