
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Oct 2014
 */

#pragma once

#include "DllExport.h"
#include "DataTypes.h"

#include <string>

class VoxoxXmpp;
class XmppParameters;

class MyAppEventHandler : public AppEventHandler
{
public:
	MyAppEventHandler();
	~MyAppEventHandler();

	void signOnEvent();
	void signOffEvent();

	//void disconnectedEvent( bool connectionError, const std::string& reason ) = 0;

	void presenceUpdateEvent ( const std::string& fromJid, int priority, int show, const std::string& status, const std::string& nickname, bool available, const std::string& timeSent );
	void incomingMsgEvent    ( const std::string& toJid, const std::string& fromJid, const std::string& msgId, const std::string& body, const std::string& chatState,
							   const std::string& fromResource, const std::string& myRessource, const std::string& receiptMsgId, int msgType );
	void chatStateChangeEvent( int chatState );
	void sendLogEvent        ( const LogEntry& logEntry );

	//Callback
	void setCallbackFunc( XmppEventCallbackFunc value )				{ mEventCallback = value; }

private:
	void sendEvent( XmppEventData* eventData );

private:
	XmppEventCallbackFunc	mEventCallback;
};

//=============================================================================


//=============================================================================

class VoxoxXmppApi
{
public:
	DLLEXPORT VoxoxXmppApi();
	DLLEXPORT ~VoxoxXmppApi();

	DLLEXPORT void login( const XmppParameters& xmppParameters );
	DLLEXPORT void logoff();
	DLLEXPORT bool isConnected() const;

	DLLEXPORT void updateMyStatus ( PresenceUpdate::Show show, const std::string& status );
	DLLEXPORT bool sendChat       ( const std::string& jid, const std::string& body, const std::string& msgId );
	DLLEXPORT bool sendChatState  ( const std::string& jid, bool composing );
	DLLEXPORT bool sendReadReceipt( const std::string& jid, const std::string& msgId );

	DLLEXPORT void requestVCard  ( const std::string& jid );

	DLLEXPORT void setAppEventHandler( AppEventHandler* value );
	DLLEXPORT AppEventHandler*	getAppEventHandler() const;				//Used for Unit Tests

	DLLEXPORT void setCallbackFunc( XmppEventCallbackFunc value );

	DLLEXPORT static bool testWebRequest   ( const std::string& url, bool connectOnly );
	DLLEXPORT static bool testWinHttpClient( const std::string& url, const std::string& proxy, bool connectOnly );

private:
	VoxoxXmpp*			getXmpp()	  const			{ return mXmpp;	}
	bool				isValidXmpp() const			{ return mXmpp != NULL; }

private:
	VoxoxXmpp*			mXmpp;
	MyAppEventHandler*	mAppEventHandler;
};

//=============================================================================
