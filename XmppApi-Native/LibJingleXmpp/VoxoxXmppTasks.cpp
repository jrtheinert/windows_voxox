
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Oct 2014
 */

#include "VoxoxXmppTasks.h"
#include <assert.h>

#include <sstream>	//For hex conversion
#include <iomanip>	//For hex conversion

#include "talk/xmpp/constants.h"

#include <Rpc.h>	//Windows only for UUID


//In sendRequest.
const buzz::StaticQName QN_VOXOX = { "jabber:client", "message" };	//TODO: What is this, and is it important?

std::string XmppSendTask::s_msgIdPrefix = "Voxox";

//Carbons
const char NS_CARBONS[]  = "urn:xmpp:carbons:2";	//"urn:xmpp:carbons"
const char NS_FORWARD[]  = "urn:xmpp:forward:0";	//"urn:xmpp:forward"

const buzz::StaticQName QN_FORWARDED = { NS_FORWARD, "forwarded" };
const buzz::StaticQName QN_SENT      = { NS_CARBONS, "sent"		};

////Receipts - XEP-0184 http://xmpp.org/extensions/xep-0184.html
//const char NS_RECEIPTS[] = "urn:xmpp:receipts";	//"urn:xmpp:receipts"
//const buzz::StaticQName QN_RECEIVED  = { NS_RECEIPTS, "received"  };
//const buzz::StaticQName QN_DISPLAYED = { NS_RECEIPTS, "displayed" };

//Naked <body> element in Carbons
const buzz::StaticQName QN_NAKED_BODY = { "", "body" };



//=============================================================================

XmppMsg::XmppMsg()
{
}

XmppMsg::~XmppMsg()
{
}

XmppMsg& XmppMsg::operator= ( const XmppMsg& src )
{
	if ( this != &src )
	{
		setType		   ( src.getType()			);
		setTo		   ( src.getTo()			);
		setFrom		   ( src.getFrom()			);
		setBody        ( src.getBody()			);
		setMessageId   ( src.getMessageId()		);
		setChatState   ( src.getChatState()		);
		setFromResource( src.getFromResource()	);
		setMyResource  ( src.getMyResource()	);
		setReceiptMsgId( src.getReceiptMsgId()	);

//		setErrorCode( src.getErrorCode() );
//		setErrorType( src.getErrorType() );
	}

	return *this;
}

//=============================================================================



//=============================================================================
//This class does not actually listen for ANY message types, so HL_SINGLE is fine.
XmppSendTask::XmppSendTask( buzz::XmppTaskParentInterface* parent ) : XmppTask( parent, buzz::XmppEngine::HL_SINGLE ) 
{
	srand (time(NULL));
	mNextMsgId = rand() % 1000000;
}

buzz::XmppReturnStatus XmppSendTask::sendMessage( const buzz::Jid& to, const std::string& body, const std::string& msgId ) 
{
	//Message and attributes.
	rtc::scoped_ptr<buzz::XmlElement> message( makeMessageXmlElement( to, msgId, false ) );

	//ChatState
    buzz::XmlElement* chatState = new buzz::XmlElement( buzz::QN_CS_ACTIVE, true );
    message->AddElement( chatState );

	//Body
    buzz::XmlElement* bodyElement = new buzz::XmlElement( buzz::QN_BODY, true );
    bodyElement->SetBodyText( body );
    message->AddElement(bodyElement);

	std::string temp = message.get()->Str();

    return SendStanza( message.get() );
}

buzz::XmppReturnStatus XmppSendTask::sendChatState( const buzz::Jid& to, bool composing )
{
	//Message and attributes.
	rtc::scoped_ptr<buzz::XmlElement> message( makeMessageXmlElement( to, "", false ) );

	//ChatState
	const buzz::StaticQName qName = (composing ? buzz::QN_CS_COMPOSING : buzz::QN_CS_ACTIVE );
    buzz::XmlElement* chatState = new buzz::XmlElement( qName, true );
    message->AddElement( chatState );

	std::string temp = message.get()->Str();

    return SendStanza( message.get() );
}

buzz::XmppReturnStatus XmppSendTask::sendDeliveryReceipt( const buzz::Jid& to, const std::string& msgId ) 
{
	//Message and attributes.
	rtc::scoped_ptr<buzz::XmlElement> message( makeMessageXmlElement( to, "", true ) );	//"" will cause UUID msgId to be generated

	//Receipt
    buzz::XmlElement* receipt = new buzz::XmlElement( buzz::QN_RECEIVED, true );
	receipt->SetAttr( buzz::QN_ID,  msgId );
    message->AddElement( receipt );

	std::string temp = message.get()->Str();

    return SendStanza( message.get() );
}

buzz::XmppReturnStatus XmppSendTask::sendReadReceipt( const buzz::Jid& to, const std::string& msgId ) 
{
	//Message and attributes.
	rtc::scoped_ptr<buzz::XmlElement> message( makeMessageXmlElement( to, "", true ) );	//"" will cause UUID msgId to be generated

	//Receipt
    buzz::XmlElement* receipt = new buzz::XmlElement( buzz::QN_DISPLAYED, true );
	receipt->SetAttr( buzz::QN_ID,  msgId );
    message->AddElement( receipt );

//	std::string temp = message.get()->Str();

    return SendStanza( message.get() );
}

//TODO: This does not work  'Bad Request'
void XmppSendTask::requestVCard( const buzz::Jid& to )
{
	//NOTE: QN_FROM is auto added by framework
	rtc::scoped_ptr<buzz::XmlElement> message( new buzz::XmlElement( buzz::QN_IQ, true ) );
	message->SetAttr( buzz::QN_TO,   to.Str()       );
	message->SetAttr( buzz::QN_TYPE, buzz::STR_GET  );
	message->SetAttr( buzz::QN_ID,   generateMsgId( "" ) );

//    buzz::XmlElement* vcard = new buzz::XmlElement( buzz::QN_VCARD );
    buzz::XmlElement* vcard = new buzz::XmlElement( buzz::kQnVCardX, true );
	
    message->AddElement( vcard );

	std::string temp = message.get()->Str();

	SendStanza( message.get() );
}

buzz::XmlElement* XmppSendTask::makeMessageXmlElement( const buzz::Jid& to, const std::string& msgIdIn, bool isReceipt )
{
	buzz::XmlElement* message = new buzz::XmlElement( buzz::QN_MESSAGE, true );

	//NOTE: QN_FROM is auto added by the framework
	//NOTE: QN_XMLNS_CLIENT is auto added by the framework
	message->SetAttr( buzz::QN_TO,   to.Str()   );
	message->SetAttr( buzz::QN_ID,   generateMsgId( msgIdIn ) );

	if ( !isReceipt )
	{
		message->SetAttr( buzz::QN_TYPE, buzz::STR_SHOW_CHAT );		//TODO: Should this be based on user's 'show' value?
	}

	std::string temp1 = message->Str();

	return message;
}

int XmppSendTask::ProcessStart() 
{
	// Call NextStanza
//	std::cout << "PROCESS START" << std::endl;		//TODO-LOG
  
	return STATE_BLOCKED;
}

bool XmppSendTask::HandleStanza( const buzz::XmlElement* stanza ) 
{
//	std::string msg = ">>>>> HandleStanza() " + stanza->Str() + "\n";		//TODO-LOG
//	OutputDebugStringA( msg.c_str() );
	
	//So far I have seen the following stanza 'types':
	//	- presence (of contact, on initial login)
	//	- Typing indicator (chatstates, included in 'message') - composing, active
	//	- Incoming 'message'
	if (!MatchRequestIq( stanza, "get", QN_VOXOX ) ) 
	{
		return false;
	}

	return true;
}

std::string XmppSendTask::generateMsgId( const std::string& msgIdIn )
{
//	std::stringstream ss;
//	ss << std::setfill( '0') << std::setw( sizeof(mNextMsgId)*2) << std::hex << ++mNextMsgId;
//	
//	std::string result = s_msgIdPrefix + "-" + ss.str();

	std::string result;

	if ( msgIdIn.empty() )
		result = s_msgIdPrefix + "-" + generateUuid();
	else
		result = msgIdIn;

	return result;
}

std::string XmppSendTask::generateUuid()
{
	std::string result;

	UUID * pUUID = new UUID();
	unsigned char *sUUID = NULL;

	if (UuidCreate(pUUID) == RPC_S_OK) 
	{
		UuidToStringA(pUUID, &sUUID);
		result = std::string((char *)sUUID);
		RpcStringFreeA(&sUUID);
	}

	delete pUUID;

	return result;
}


//=============================================================================


//=============================================================================

MessageReceiveTask::MessageReceiveTask( XmppTaskParentInterface* parent ) : XmppTask( parent, buzz::XmppEngine::HL_TYPE ) 
{
}

MessageReceiveTask::~MessageReceiveTask() 
{
}

int MessageReceiveTask::ProcessStart() 
{
	const buzz::XmlElement* stanza = NextStanza();

	if (stanza == NULL) 
	{
		return STATE_BLOCKED;
	}

	HandleMessage( stanza );

	return STATE_START;
}

bool MessageReceiveTask::HandleStanza( const buzz::XmlElement* stanza ) 
{
	bool result = false;

	// Verify that this is a message stanza
	if (stanza->Name() == buzz::QN_MESSAGE ) 
	{
		QueueStanza( stanza );		// Queue it up
		result = true;
	}

	return result;
}

void MessageReceiveTask::HandleMessage( const buzz::XmlElement* stanza ) 
{
	if ( stanza->Attr( buzz::QN_TYPE ) != buzz::STR_ERROR ) 	//TODO: How to handle errors?
	{
		XmppMsg xmppMsg = DecodeMessage( stanza );
		SignalXmppMsg( xmppMsg );
	}
}

XmppMsg MessageReceiveTask::DecodeMessage( const buzz::XmlElement* stanza ) 
{
	std::string temp = stanza->Str();

	XmppMsg result;

	//Basic message handling
	std::string to		  = stanza->Attr( buzz::QN_TO   );
	std::string from	  = stanza->Attr( buzz::QN_FROM );
	std::string msgId	  = stanza->Attr( buzz::QN_ID   );
	std::string type	  = stanza->Attr( buzz::QN_TYPE );
	std::string chatState = ChatStateFromXmlElement( stanza );
	std::string body	  = stanza->TextNamed( buzz::QN_BODY );

	result.setTo	   ( to			);
	result.setFrom	   ( from		);
	result.setMessageId( msgId		);
	result.setChatState( chatState	);
	result.setBody     ( body		);

	if ( body.empty() )
		result.setTypeChatState();
	else
		result.setTypeMsg();

	if ( ! CheckForReceipt( stanza, result ) )
	{
		CheckForCarbons( stanza, result );
	}

	return result;
}

std::string MessageReceiveTask::ChatStateFromXmlElement( const buzz::XmlElement* stanza )
{
	std::string result;

	const buzz::XmlElement* chatStateElement = stanza->FirstWithNamespace( buzz::NS_CHATSTATE );
		
	if ( chatStateElement )
	{
		buzz::QName	qName  = chatStateElement->Name();
		result = qName.LocalPart();
	}

	return result;
}

bool MessageReceiveTask::CheckForReceipt( const buzz::XmlElement* stanza, XmppMsg& xmppMsg )
{
	bool result = false;
	const buzz::XmlElement* receivedElement = stanza->FirstNamed( buzz::QN_RECEIVED  );		//Not standard in buzz namespace
	const buzz::XmlElement* readElement     = stanza->FirstNamed( buzz::QN_DISPLAYED );		//Not standard in buzz namespace
		
	//Should have one or the other, but not both.
	if ( receivedElement != nullptr )
	{
		std::string msgId = receivedElement->Attr( buzz::QN_ID   );
		if ( !msgId.empty() )
		{
			xmppMsg.setReceiptMsgId( msgId );
			xmppMsg.setTypeDeliveredReceipt();
			result = true;
		}
	}
	else if ( readElement != nullptr )
	{
		std::string msgId = readElement->Attr( buzz::QN_ID   );
		if ( !msgId.empty() )
		{
			xmppMsg.setReceiptMsgId( msgId );
			xmppMsg.setTypeReadReceipt();
			result = true;
		}
	}

	return result;
}

bool MessageReceiveTask::CheckForCarbons( const buzz::XmlElement* stanza, XmppMsg& xmppMsg )
{
	bool result = false;
	const buzz::XmlElement* sentElement = stanza->FirstNamed( QN_SENT );					//Not standard in buzz namespace
		
	if ( sentElement )
	{
		const buzz::XmlElement* forwardElement = sentElement->FirstNamed( QN_FORWARDED );		//Not standard in buzz namespace

		if ( forwardElement )
		{
			const buzz::XmlElement* messageElement = forwardElement->FirstElement();	//BIG ASSUMPTION here.

			if ( messageElement ) 
			{
				std::string temp = messageElement->Str();

				//Now we have the <message>, its attribute 'id' and its child <body>.
				std::string to		  = messageElement->Attr( buzz::QN_TO   );
				std::string msgId	  = messageElement->Attr( buzz::QN_ID   );
				std::string chatState = ChatStateFromXmlElement( messageElement );

				//Our <body> text is NOT in a QN namespace, it is just <body> with no NS attribute.
				//	So we have to iterate over elements.  TODO: I am sure there is a better way.
				std::string body	  = messageElement->TextNamed( QN_NAKED_BODY );

				if ( body.empty() )
				{
					const buzz::XmlElement* tempElement = messageElement->FirstElement();

					while ( tempElement )
					{
						buzz::QName tempQName = tempElement->Name();

						if ( tempQName.LocalPart() == "body" )
						{
							body = tempElement->BodyText();
							break;
						}
						
						tempElement = tempElement->NextElement();
					}
				}

				xmppMsg.setTo		   ( to		   );
				xmppMsg.setChatState   ( chatState );
				xmppMsg.setBody        ( body      );
				xmppMsg.setMessageId   ( msgId     );
				xmppMsg.setReceiptMsgId( msgId	   );		//TODO: This is temporary

				xmppMsg.setTypeCarbon();

				result = true;
			}
		}
	}

	return result;
}

//=============================================================================


//=============================================================================
//WIP
IqReceiveTask::IqReceiveTask( XmppTaskParentInterface* parent ) : XmppTask( parent, buzz::XmppEngine::HL_TYPE ) 
{
}

IqReceiveTask::~IqReceiveTask() 
{
//	Stop();	//This may cause crash so test when we implement this class.
}

int IqReceiveTask::ProcessStart() 
{
	const buzz::XmlElement* stanza = NextStanza();

	if (stanza == NULL) 
	{
		return STATE_BLOCKED;
	}

	HandleMessage( stanza );

	return STATE_START;
}

bool IqReceiveTask::HandleStanza( const buzz::XmlElement* stanza ) 
{
	bool result = false;

	// Verify that this is a IQ stanza
	if (stanza->Name() == buzz::QN_IQ ) 
	{
		QueueStanza( stanza );		// Queue it up
		result = true;
	}

	return result;
}

void IqReceiveTask::HandleMessage( const buzz::XmlElement* stanza ) 
{
	if ( stanza->Attr( buzz::QN_TYPE ) != buzz::STR_ERROR )	//TODO: How to handle errors?
	{
		XmppMsg xmppMsg = DecodeMessage( stanza );
		SignalIqMsg( xmppMsg );
	}
}

XmppMsg IqReceiveTask::DecodeMessage( const buzz::XmlElement* stanza ) 
{
	XmppMsg result;	//TODO: return type may not be XmppMsg.  New class?
	int xxx = 0;

	const buzz::XmlElement* errorElement      = stanza->FirstNamed( buzz::QN_ERROR );
	const buzz::XmlElement* discoQueryElement = stanza->FirstNamed( buzz::QN_DISCO_INFO_QUERY );

	std::string type  = stanza->Attr( buzz::QN_TYPE );

	if ( errorElement != nullptr )
	{
		//Bad Request on send chat message.
		std::string code = errorElement->Attr( buzz::QN_CODE );
		std::string type = errorElement->Attr( buzz::QN_TYPE );

//		result.setErrorCode( code );
//		result.setErrorType( type );
	}
	else if ( discoQueryElement != nullptr )
	{
		if ( type == buzz::STR_GET )
		{
//			result.setType( XmppMsg::TYPE_DISCO_QUERY );
			//Something from 'ed' & 'kevin'.
			xxx = 90;
		}
		else
		{
			xxx = 90;
		}
	}

	return result;
}

//=============================================================================
