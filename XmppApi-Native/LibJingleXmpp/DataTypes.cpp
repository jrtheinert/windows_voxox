
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Oct 2014
 */

#include "DataTypes.h"

#include <map>
#include <assert.h>	

#include <ctime>

#include <stdarg.h>		//For variable args in LogEntry::Helper
#include "Windows.h"	//For ThreadId

//LogEntry
std::string	     LogEntry::s_component       = "VoxoxXmpp";
LogSendCallback* LogEntry::s_logSendCallback = nullptr;

//=============================================================================

ChatMsg::ChatMsg()
{
}

ChatMsg::~ChatMsg()
{
}

ChatMsg& ChatMsg::operator=( const ChatMsg& src )
{
	if ( this != &src )
	{
		setToJid	   ( src.getToJid()			);
		setFromJid	   ( src.getFromJid()		);
		setMsgId	   ( src.getMsgId()			);
		setBody		   ( src.getBody()			);
		setChatState   ( src.getChatState()		);
		setFromResource( src.getFromResource()	);
		setMyResource  ( src.getMyResource()	);
		setReceiptMsgId( src.getReceiptMsgId()	);
		setType        ( src.getType()			);
	}

	return *this;
}

std::string ChatMsg::toString() const
{
	std::string result;

	result += "To: "			+ getToJid()		+ ", ";
	result += "From: "			+ getFromJid()		+ ", ";
	result += "FromResouce: "	+ getFromResource() + ", ";
	result += "MsgId: "			+ getMsgId()		+ ", ";
	result += "Body: "			+ getBody()			+ ", ";
	result += "ChatState: "		+ getChatState()	+ ", ";
	result += "MyResource: "	+ getMyResource()	+ ", ";
	result += "ReceiptMsgId: "	+ getMyResource()	+ ", ";
	result += "Type: "			+ getMyResource();

	return result;
}

//=============================================================================


//=============================================================================

PresenceUpdate::PresenceUpdate() : mAvailable( false ), mPriority( 0 ), mShow( SHOW_NONE )
{
}

PresenceUpdate::~PresenceUpdate()
{
}

PresenceUpdate& PresenceUpdate::operator=( const PresenceUpdate& src )
{
	if ( this != &src )
	{
		setFromJid  ( src.getFromJid()	);
		setStatus   ( src.getStatus()	);
		setNickname ( src.getNickname()	);
		setTimeSent ( src.getTimeSent()	);
		setPriority ( src.getPriority()	);
		setShow	    ( src.getShow()		);
		setAvailable( src.isAvailable()	);
	}

	return *this;
}

std::string PresenceUpdate::toString() const
{
	std::string result;

	result += "From: "		+ getFromJid()  + ", ";
	result += "Status: "	+ getStatus()   + ", ";
	result += "NickName: "	+ getNickname() + ", ";
	result += "TimeSent: "	+ getTimeSent() + ", ";
	result += "Priority: "	+ std::to_string( getPriority() ) + ", ";
	result += "Show: "		+ std::to_string( getShow()     ) + ", ";
	result += "Available: " + std::string( ( isAvailable() ? "True" : "False" ) );

	return result;
}

//=============================================================================

	
//=============================================================================

LogEntry::LogEntry()
{
	setLevel	 ( Debug );
	setComponent ( "" );
	setClassName ( "" );
	setMessage   ( "" );
	setFileName  ( "" );
	setLineNumber( 0  );
	setThreadId  ( 0  );
	setTime		 ( std::tm() );
}

LogEntry::LogEntry( const std::string& component, Level level, const std::string& className, const std::string& message, 
					const char* filename, int line, unsigned long threadId )
{
	setLevel	 ( level	 );
	setComponent ( component );
	setClassName ( className );
	setMessage   ( message	 );
	setLineNumber( line		 );
	setThreadId  ( threadId  );

	//Get current time
	std::time_t curTime = time(nullptr);					//Current time.
	struct std::tm * timeinfo = std::localtime(&curTime);	//Convert to local
	setTime		 ( *timeinfo    );							//Save

	if ( filename )
	{
		setFileName  ( filename  );
	}
}

LogEntry LogEntry::operator=( const LogEntry& src )
{
	if ( this != &src )
	{
		setLevel	 ( src.getLevel()	   );
		setComponent ( src.getComponent()  );
		setClassName ( src.getClassName()  );
		setMessage   ( src.getMessage()    );
		setFileName  ( src.getFileName()   );
		setLineNumber( src.getLineNumber() );
		setThreadId  ( src.getThreadId()   );
		setTime		 ( src.getTime()	   );
	}

	return *this;
}

//static 
void LogEntry::makeAndSend( const char* component, Level level, const char* className, const char* fileName, int line, const std::string& message )
{
	unsigned long threadId = getOsThreadId();
	LogEntry entry = LogEntry( component, level, className, message, fileName, line, threadId );
	sendEntry( entry );
}

//static 
void LogEntry::makeAndSend( const char* component, Level level, const char* className, const char* fileName, int line, const char* format, ...  )
{
	std::string   message;
	unsigned long threadId = getOsThreadId();
	const LogEntry entry = LogEntry( component, level, className, message, fileName, line, threadId );
	sendEntry( entry );
}

//static
void LogEntry::sendEntry( const LogEntry& entry )
{
	if ( s_logSendCallback != nullptr )
	{
		s_logSendCallback->sendLog( entry );
	}
}

//static
unsigned long LogEntry::getOsThreadId()
{
	unsigned long result = -1;
#ifdef _WINDOWS
	result = ::GetCurrentThreadId();
#endif

	return result;
}

//=============================================================================


//=============================================================================

XmppParameters::XmppParameters() : mPort( 0 ), mAllowPlainPassword( false ), mTlsOption( TLS_DISABLED ), mUseProxy( false ), mProxyPort( 0 )
{
}

XmppParameters::~XmppParameters()
{
}

XmppParameters& XmppParameters::operator=( const XmppParameters& src )
{
	if ( this != &src )
	{
		//User
		setUserId			 ( src.getUserId()			);
		setPassword			 ( src.getPassword()		);
		setStatus			 ( src.getStatus()			);
		setInitialShow		 ( src.getInitialShow()		);

		//Connection
		setDomain			 ( src.getDomain()			);
		setServer			 ( src.getServer()			);
		setResourcePrefix	 ( src.getResourcePrefix()	);
		setPort			     ( src.getPort()			);

		setAllowPlainPassword( src.getAllowPlainPassword()	);
		setTlsOption		 ( src.getTlsOption()			);
		setAuthMechanism	 ( src.getAuthMechanism()		);

		setUseProxy			 ( src.getUseProxy()			);
		setProxyServer		 ( src.getProxyServer()			);
		setProxyPort		 ( src.getProxyPort()			);
	}

	return *this;
}

//=============================================================================



//=============================================================================
// XmppEvent class
//=============================================================================

XmppEventData::XmppEventData()
{
	initVars();
}

XmppEventData::XmppEventData( EventType eventType )
{
	initVars();
	setEventType( eventType );
}

void XmppEventData::initVars()
{
	mEventType			= EventType::Unknown;
	
//	mConnectionError	= false;
//	mDisconnectMsg		= "";
}

//static 
XmppEventData* XmppEventData::makeSignOnEvent()
{
	XmppEventData* result = new XmppEventData( EventType::SignOn );

	return result;
}

//static 
XmppEventData* XmppEventData::makeSignOffEvent()
{
	XmppEventData* result = new XmppEventData( EventType::SignOff );

	return result;
}

////static 
//XmppEventData* XmppEventData::makeDisconnectEvent( bool connectError, const std::string& disconnectMsg )
//{
//	XmppEventData* result = new XmppEventData( EventType::Disconnect );
//
//	result->setConnectionError( connectError  );
//	result->setDisconnectMsg  ( disconnectMsg );
//
//	return result;
//}

//static
XmppEventData* XmppEventData::makePresenceChangeEvent( const std::string& fromJid, int priority, int show, const std::string& status, const std::string& nickname, bool available, const std::string& timeSent )
{
	PresenceUpdate temp;
	temp.setFromJid  ( fromJid   );
	temp.setPriority ( priority  );
	temp.setShow     ( (PresenceUpdate::Show) show );
	temp.setStatus   ( status    );
	temp.setNickname ( nickname  );
	temp.setAvailable( available );
	temp.setTimeSent ( timeSent  );

	XmppEventData* result = new XmppEventData( EventType::PresenceChange );

	result->setPresenceUpdate( temp  );

	return result;
}

XmppEventData* XmppEventData::makeIncomingMsgEvent( const std::string& toJid, const std::string& fromJid, const std::string& msgId, const std::string& body, 
													const std::string& chatState, const std::string& fromResource, const std::string& myResource,
													const std::string& receiptMsgId, int msgType)
{
	ChatMsg temp;

	temp.setToJid		( toJid			);
	temp.setFromJid		( fromJid		);
	temp.setMsgId		( msgId			);
	temp.setBody		( body			);
	temp.setChatState	( chatState		);
	temp.setFromResource( fromResource	);
	temp.setMyResource  ( myResource	);
	temp.setReceiptMsgId( receiptMsgId  );
	temp.setType        ( (ChatMsg::Type) msgType );

	XmppEventData* result = new XmppEventData( EventType::IncomingMsg );

	result->setChatMsg( temp  );

	return result;
}

//static
XmppEventData* XmppEventData::makeLogEvent( const LogEntry& logEntry )
{
	XmppEventData* result = new XmppEventData( EventType::Log );

	result->setLogEntry( logEntry );

	return result;
}

//=============================================================================
