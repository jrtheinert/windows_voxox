

#include "WebRequest.h"
#include <winsock2.h>
#include <WS2tcpip.h>
#include <windows.h>
#include <iostream>

#include <string>

WebRequest::WebRequest() : mInitialized( false )
{
	init();
}


WebRequest::~WebRequest()
{
	shutdown();
}

void WebRequest::init()
{
	bool result = true;

	// Initialize Dependencies to the Windows Socket.
	WSADATA wsaData;
	if (WSAStartup(MAKEWORD(2,2), &wsaData) != 0) 
	{
		logIt( "WSAStartup failed." );
		result = false;
	}

	mInitialized = result;
}

void WebRequest::shutdown()
{
	if ( mInitialized )
	{
		WSACleanup();
		mInitialized = false;
	}
}

bool WebRequest::makeRequest( const std::string& url, bool connectOnly )	//TODO: Add proxy support?
{
	bool result = false;

	// Initialize Dependencies to the Windows Socket.
	if ( mInitialized )
	{
		// We first prepare some "hints" for the "getaddrinfo" function
		// to tell it, that we are looking for a IPv4 TCP Connection.
		struct addrinfo hints;
		ZeroMemory(&hints, sizeof(hints));
		hints.ai_family		= AF_INET;			// We are targeting IPv4
		hints.ai_protocol	= IPPROTO_TCP;		// We are targeting TCP
		hints.ai_socktype	= SOCK_STREAM;		// We are targeting TCP so its SOCK_STREAM

		// Aquiring of the IPv4 address of a host using the newer "getaddrinfo" function which outdated "gethostbyname".
		// It will search for IPv4 addresses using the TCP-Protocol.
		struct addrinfo* targetAdressInfo = NULL;
		DWORD  getAddrRes = getaddrinfo( url.c_str(), NULL, &hints, &targetAdressInfo);

		if (getAddrRes != 0 || targetAdressInfo == NULL)
		{
			logIt( "Could not resolve the Host Name: " + url );
			return result;
		}

		// Create the Socket Address Informations, using IPv4
		// We don't have to take care of sin_zero.  It is only used to extend the length of SOCKADDR_IN to the size of SOCKADDR
		SOCKADDR_IN sockAddr;
		sockAddr.sin_addr	= ((struct sockaddr_in*) targetAdressInfo->ai_addr)->sin_addr;    // The IPv4 Address from the Address Resolution Result
		sockAddr.sin_family = AF_INET;		// IPv4
		sockAddr.sin_port	= htons(80);	// HTTP Port: 80

		// We have to free the Address-Information from getaddrinfo again
		freeaddrinfo(targetAdressInfo);

		// Creation of a socket for the communication with the Web Server,
		// using IPv4 and the TCP-Protocol
		SOCKET webSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

		if (webSocket == INVALID_SOCKET)
		{
			logIt( "Creation of the Socket Failed" );
			return result;
		}

		// Establishing a connection to the web Socket
		logIt( "Connecting..." );

		if ( connect(webSocket, (SOCKADDR*)&sockAddr, sizeof(sockAddr)) != 0 )
		{
			logIt( "Could not connect" );
			closesocket(webSocket);
			return result;
		}

		logIt( "Connected." );

		if ( connectOnly )
		{
			result = true;
		}
		else
		{
			// Sending a HTTP-GET-Request to the Web Server
			std::string httpRequest = "GET / HTTP/1.1\r\nHost: " + url + "\r\nConnection: close\r\n\r\n";
			int			sentBytes   = send( webSocket, httpRequest.c_str(), httpRequest.size(), 0 );

			if ( sentBytes < (int)httpRequest.size() || sentBytes == SOCKET_ERROR)
			{
				logIt( "Could not send the request to the Server");
				closesocket(webSocket);
				return result;
			}

			// Receiving and Displaying an answer from the Web Server
			int  dataLen = 0;
			char buffer[10000];
			ZeroMemory(buffer, sizeof(buffer));
			std::string line;

			while ((dataLen = recv(webSocket, buffer, sizeof(buffer), 0) > 0))
			{
				line.clear();
				int i = 0;

				while (buffer[i] >= 32 || buffer[i] == '\n' || buffer[i] == '\r') 
				{
					line += buffer[i];
					i += 1;
				}

				logIt( line );
				line.clear();
			}

			logIt( line );
		}
   
		closesocket(webSocket);

	}

	return result;
}	//makeRequest

void WebRequest::logIt( const std::string& msgIn )
{
	//Just to Debug window for now.
	std::string msg = msgIn + "\n";
	OutputDebugStringA( msg.c_str() );
}