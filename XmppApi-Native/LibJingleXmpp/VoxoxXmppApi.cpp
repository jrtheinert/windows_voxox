
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Oct 2014
 */

#include "VoxoxXmppApi.h"
#include "VoxoxXmpp.h"

//--------------------------------------------------------------------------------------
// We want std::mutex to synchronize DB calls, but CLI/CLR does not like std::mutex.
//	So we cannot put this in the .h file because .h file is called by the managed code.
//	We are forced to use std::mutex as static variable
//--------------------------------------------------------------------------------------
#include <mutex>

static std::recursive_mutex s_xmppMutex;

//=============================================================================

MyAppEventHandler::MyAppEventHandler()
{
}

MyAppEventHandler::~MyAppEventHandler()	
{
}

void MyAppEventHandler::signOnEvent()
{
	XmppEventData* eventData = XmppEventData::makeSignOnEvent();
	sendEvent( eventData );
}

void MyAppEventHandler::signOffEvent()
{
	XmppEventData* eventData = XmppEventData::makeSignOffEvent();
	sendEvent( eventData );
}

//void MyAppEventHandler::disconnectedEvent( bool connectionError, const std::string& reason )

void MyAppEventHandler::presenceUpdateEvent( const std::string& fromJid, int priority, int show, const std::string& status, const std::string& nickname, bool available, const std::string& timeSent )
{
	XmppEventData* eventData = XmppEventData::makePresenceChangeEvent( fromJid, priority, show, status, nickname, available, timeSent );
	sendEvent( eventData );
}

void MyAppEventHandler::incomingMsgEvent( const std::string& toJid, const std::string& fromJid, const std::string& msgId, const std::string& body, 
										  const std::string& chatState, const std::string& fromResource, const std::string& myResource,
										  const std::string& receiptMsgId, int msgType )
{
	XmppEventData* eventData = XmppEventData::makeIncomingMsgEvent( toJid, fromJid, msgId, body, chatState, fromResource, myResource, receiptMsgId, msgType );
	sendEvent( eventData );
}

void MyAppEventHandler::sendLogEvent( const LogEntry& logEntry )
{
	XmppEventData* eventData = XmppEventData::makeLogEvent( logEntry );
	sendEvent( eventData );
}

void MyAppEventHandler::sendEvent( XmppEventData* eventData )
{
	if ( mEventCallback != nullptr )
	{
		mEventCallback( *eventData );

		delete eventData;
	}
}

//=============================================================================


//=============================================================================

VoxoxXmppApi::VoxoxXmppApi()
{
	mXmpp			 = new VoxoxXmpp();
	mAppEventHandler = new MyAppEventHandler();		//Default handler

	setAppEventHandler( mAppEventHandler );			//Set default hanlder
}

VoxoxXmppApi::~VoxoxXmppApi()
{
	std::lock_guard<std::recursive_mutex> lock( s_xmppMutex );

	delete mXmpp;
	delete mAppEventHandler;

	mXmpp			 = NULL;
	mAppEventHandler = NULL;

}
	
void VoxoxXmppApi::login( const XmppParameters& xmppParameters )
{
	std::lock_guard<std::recursive_mutex> lock( s_xmppMutex );

	if ( isValidXmpp() )
	{
		getXmpp()->login( xmppParameters );
	}
}

void VoxoxXmppApi::logoff()
{
	std::lock_guard<std::recursive_mutex> lock( s_xmppMutex );

	if ( isValidXmpp() )
	{
		getXmpp()->logoff();
	}
}

bool VoxoxXmppApi::isConnected() const
{
	std::lock_guard<std::recursive_mutex> lock( s_xmppMutex );

	bool result = false;

	if ( isValidXmpp() )
	{
		result = getXmpp()->isConnected();
	}

	return result;
}

void VoxoxXmppApi::updateMyStatus( PresenceUpdate::Show showIn, const std::string& status )
{
	std::lock_guard<std::recursive_mutex> lock( s_xmppMutex );

	if ( isValidXmpp() )
	{
		buzz::PresenceStatus::Show show = (buzz::PresenceStatus::Show) showIn;
		getXmpp()->updateMyStatus( show, status );
	}
}

bool VoxoxXmppApi::sendChat( const std::string& jid, const std::string& body, const std::string& msgId )
{
	std::lock_guard<std::recursive_mutex> lock( s_xmppMutex );

	bool result = false;

	if ( isValidXmpp() )
	{
		result = getXmpp()->sendChat( jid, body, msgId );
	}

	return result;
}

bool VoxoxXmppApi::sendChatState( const std::string& jid, bool composing )
{
	std::lock_guard<std::recursive_mutex> lock( s_xmppMutex );

	bool result = false;

	if ( isValidXmpp() )
	{
		result = getXmpp()->sendChatState( jid, composing );
	}

	return result;
}

bool VoxoxXmppApi::sendReadReceipt( const std::string& jid, const std::string& msgId )
{
	std::lock_guard<std::recursive_mutex> lock( s_xmppMutex );

	bool result = false;

	if ( isValidXmpp() )
	{
		result = getXmpp()->sendReadReceipt( jid, msgId );
	}

	return result;
}

void VoxoxXmppApi::requestVCard( const std::string& jid )
{
	std::lock_guard<std::recursive_mutex> lock( s_xmppMutex );

	if ( isValidXmpp() )
	{
		getXmpp()->requestVCard( jid );
	}
}


//AppEventHander and callback
void VoxoxXmppApi::setAppEventHandler( AppEventHandler* value )
{
	std::lock_guard<std::recursive_mutex> lock( s_xmppMutex );

	if ( isValidXmpp() )
	{
		getXmpp()->setAppEventHandler( value );
	}
}

AppEventHandler* VoxoxXmppApi::getAppEventHandler() const
{
	std::lock_guard<std::recursive_mutex> lock( s_xmppMutex );

	AppEventHandler* result = NULL;

	if ( isValidXmpp() )
	{
		result = getXmpp()->getAppEventHandler();
	}
	
	return result;
}

void VoxoxXmppApi::setCallbackFunc( XmppEventCallbackFunc value )
{
	if ( mAppEventHandler != nullptr )
	{
		mAppEventHandler->setCallbackFunc( value );
	}
}


//static 
bool VoxoxXmppApi::testWebRequest( const std::string& url, bool connectOnly )
{
	bool result = VoxoxXmpp::testWebRequest( url, connectOnly );

	return result;
}

//static 
bool VoxoxXmppApi::testWinHttpClient( const std::string& url, const std::string& proxy, bool connectOnly )
{
	bool result = VoxoxXmpp::testWinHttpClient( url, proxy, connectOnly );

	return result;
}
