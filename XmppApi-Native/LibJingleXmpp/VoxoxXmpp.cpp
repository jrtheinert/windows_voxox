
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Oct 2014
 */

#include "VoxoxXmpp.h"
#include <assert.h>

//For WebRequest/Connectivity test
#include "WebRequest.h"
#include "WinHttpClient.h"
#include "codecvt"
//End

#include "talk/xmpp/constants.h"			//For AUTH_MECHANISM_xxx
#include "webrtc/base/autodetectproxy.h"
#include "webrtc/base/logging.h"
#include "webrtc/base/winping.h"

// Must be period >= timeout.
const uint32 kPingPeriodMillis  = 120000;	//120 seconds.  TODO-XMPP: Make configurable?
const uint32 kPingTimeoutMillis =  10000;

static const std::string sPingMsgId = "urn:xmpp:ping";

//-----------------------------------------------------------------------------
//NOTES on connection/reconnection
//	- It appears that XmppPump is not cleanly reset (and cannot be) between DoLogin()
//		and any loss of connection (NIC, internet, jabber server).
//	- As a result, we create a new XmppPump memvar on each connection attempt.
//	- It also appears that 'delete'-ing the XmppPump memvar also causes issues.
//		I am not sure why, but is most likley something I am doing wrong.
//		For now, we just do NOT delete the mXmppPump, which *may* result in a 
//		memory leak, but is acceptable for now.
//
//	- PresenceReceiveTask (not native to LibJingle, but part of download) causes
//		crash on disconnect/reconnect, so I am disabling that for now.
//		We *may* need to fix this to get Presence handling done, but the 
//		buzz::XmppRosterModuleImpl class may allow us to avoid using PresenceReceiveTask
//
//MainThread notes:
//	- MainThread (signal thread) should only be started once upon class instantiation.
//	- This keeps things running until we disconnect, but IMO, we should only disconnect
//		when app quits.  IOW, do NOT call DoDisconnet() except in dtor.
//	- Testing indicates we can:
//		- handle initial login/connection
//		- handle loss of connection (NIC, Internet, remote server) by using the 
//		  PingTask class and responding to the timeout signal.
//		
//-----------------------------------------------------------------------------

//=============================================================================

RosterItem::RosterItem() : mShow( buzz::PresenceStatus::SHOW_NONE )
{
}

RosterItem::RosterItem( const buzz::Jid jid, buzz::PresenceStatus::Show show, const std::string& status )
	: mJid( jid ), mShow( show ), mStatus( status )
{
}

RosterItem& RosterItem::operator= ( const RosterItem& src )
{
	if ( this != &src )
	{
		setJid   ( src.getJid()    );
		setShow  ( src.getShow()   );
		setStatus( src.getStatus() );
	}

	return *this;
}

bool RosterItem::operator==( const RosterItem& src )
{
	return getJid() == src.getJid();
}

//=============================================================================


//=============================================================================

void Roster::add( const RosterItem& item )
{
	std::string key = item.getJid().Str();
	std::pair<std::string, RosterItem> pair( key, item );
	insert( pair );
}

void Roster::remove( const RosterItem& item )
{
	std::string key = item.getJid().Str();
	erase( key );
}

//=============================================================================


//=============================================================================

VoxoxRosterHandler::VoxoxRosterHandler( VoxoxXmpp* voxoxXmpp )
{
	mVoxoxXmpp = voxoxXmpp;
}

void VoxoxRosterHandler::SubscriptionRequest( buzz::XmppRosterModule* roster, const buzz::Jid& requesting_jid, buzz::XmppSubscriptionRequestType type, const buzz::XmlElement* raw_xml )
{
	std::string xml = raw_xml->Str();

	//TODO: Auto accept
}

void VoxoxRosterHandler::SubscriptionError( buzz::XmppRosterModule* roster, const buzz::Jid& from, const buzz::XmlElement* raw_xml )
{
	std::string xml = raw_xml->Str();
	//TODO: Log error?
}

void VoxoxRosterHandler::RosterError( buzz::XmppRosterModule* roster, const buzz::XmlElement* raw_xml )
{
	std::string xml = raw_xml->Str();
	//TODO: Log error?
}

void VoxoxRosterHandler::IncomingPresenceChanged( buzz::XmppRosterModule* roster, const buzz::XmppPresence* presence )
{
	buzz::Jid   jid		 = presence->jid();
	std::string nickname = presence->nickname();
	int			priority = presence->priority();
	int			show	 = presence->presence_show();

	//TODO: call RosterModule and pass to VoxoxXmpp
}

void VoxoxRosterHandler::ContactChanged( buzz::XmppRosterModule* roster, const buzz::XmppRosterContact* old_contact, size_t index )
{
	buzz::Jid					jid		 = old_contact->jid();
	std::string					name	 = old_contact->name();
	buzz::XmppSubscriptionState subState = old_contact->subscription_state();

	//TODO: call RosterModule and pass to VoxoxXmpp OR do nothing
}

void VoxoxRosterHandler::ContactsAdded( buzz::XmppRosterModule* roster, size_t index, size_t number )
{
	//TODO: call RosterModule and pass to VoxoxXmpp OR do nothing.
}

void VoxoxRosterHandler::ContactRemoved( buzz::XmppRosterModule* roster, const buzz::XmppRosterContact* removed_contact, size_t index )
{
	buzz::Jid					jid		 = removed_contact->jid();
	std::string					name	 = removed_contact->name();
	buzz::XmppSubscriptionState subState = removed_contact->subscription_state();

	//TODO: call RosterModule and pass to VoxoxXmpp OR do nothing.
}


//=============================================================================


//=============================================================================
//Simple thread to do execute shutdown thread NOT on SignalThread

ShutdownThread::ShutdownThread( VoxoxXmpp* voxoxXmpp ) : Thread(nullptr), mVoxoxXmpp( voxoxXmpp )
{
}

ShutdownThread::~ShutdownThread()
{
	Stop();
}

void ShutdownThread::Run()
{
	if ( mVoxoxXmpp != nullptr )
	{
		mVoxoxXmpp->handleSignOff();
	}
}

//=============================================================================


//=============================================================================
//Thread to check connectivity
ConnectivityThread::ConnectivityThread( const std::string& server, const rtc::ProxyInfo& proxyInfo ) 
	: Thread( nullptr ), mConnected( false ), mServer( server ), mProxyInfo( proxyInfo ), mProxyParsed( false ), mUseProxy( false )
{
	normalizeServer();
}

ConnectivityThread::~ConnectivityThread()
{
	Stop();
}

void ConnectivityThread::Run()
{
	bool result = false;

	uint32 timeout	   =  5000;	//ms
	uint32 sleepInc    =   250;	//ms
	bool   canConnect  = false;
	bool   connectOnly = true;

	while ( !fStop_  )
	{
		canConnect = testConnectivity( connectOnly );

		handleChange( canConnect );

		//Let's not delay killing this thread by too much
		uint32 totalSleep = 0;

		while ( totalSleep < timeout &&  !fStop_ )
		{
			Sleep( sleepInc );
			totalSleep += sleepInc;
		}
	}
}

bool ConnectivityThread::testConnectivity( bool connectOnly )
{
	initProxyInfo();

	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;	//C++11

	std::string urlIn        = mServer;
	std::wstring url		 = converter.from_bytes( urlIn );

	WinHttpClient client( url );

	if ( mUseProxy )
	{
		client.SetProxy        ( mProxyServer );
		client.SetProxyUsername( mProxyUserName );
		client.SetProxyPassword( mProxyPassword );
	}
		
	bool canConnect = client.SendHttpRequest( connectOnly );

	return canConnect;
}

void ConnectivityThread::initProxyInfo()
{
	if ( !mProxyParsed )
	{
		std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;	//C++11

		std::string urlIn = mServer;
		std::string proxyAddress;

		//Clear any previous settings
		mUseProxy = false;
		mProxyServer.clear();
		mProxyUserName.clear();
		mProxyPassword.clear();

		//If valid proxyInfo, get needed info.
		if ( !mProxyInfo.address.IsNil() )
		{
			proxyAddress = mProxyInfo.address.HostAsURIString() + ":" + mProxyInfo.address.PortAsString();
			mUseProxy    = true;
		}

		std::wstring url		 = converter.from_bytes( urlIn );
		bool		 connectOnly = true;

		if ( mUseProxy )
		{
			mProxyServer   = converter.from_bytes( proxyAddress         );
			mProxyUserName = converter.from_bytes( mProxyInfo.username  );
			mProxyPassword;	//converter.from_bytes( mProxyInfo.password );	//TODO: decrypt proxy password
		}

		mProxyParsed = true;
	}
}

void ConnectivityThread::handleChange( bool canConnect )
{
	if ( canConnect != mConnected )
	{
		mConnected = canConnect;
		SignalConnectionChange( mConnected );
	}
}

//So WinHttpClient behaves properly.
void ConnectivityThread::normalizeServer()
{
	if ( mServer.find( "http") == std::string::npos )
	{
		mServer = "http://" + mServer;
	}
}

//=============================================================================


//=============================================================================

VoxoxXmpp::VoxoxXmpp() : mConnected( false ), mAutoConnect( false )
{
	mAppEventHandler	 = nullptr;

//	mPresenceReceiveTask = nullptr;
	mSendTask			 = nullptr;
	mPresenceOutTask	 = nullptr;
	mMessageReceiveTask  = nullptr;
	mIqReceiveTask		 = nullptr;
	mPingTask			 = nullptr;

	mRoster1			 = nullptr;
	mRosterHandler		 = nullptr;

	mXmppThread			 = nullptr;
	mConnectivityThread  = nullptr;
}

VoxoxXmpp::~VoxoxXmpp()
{
	disconnect();

	rtc::ThreadManager::Instance()->SetCurrentThread( nullptr );

//	delete mXmppThread;		//May cause crash.
	mXmppThread = nullptr;

	delete mRosterHandler;

	taskCleanUp();
}

std::string VoxoxXmpp::generateResourceSuffix()
{
	std::string result;

	__int64 temp = getSystemTimeMillis();
	
	std::stringstream ss;
	ss << std::setfill( '0') << std::setw( sizeof(__int64)*2) << std::hex << temp;
	
	result = "-" + ss.str();

	return result;
}

void VoxoxXmpp::initXmppThread()
{
	if ( mXmppThread == nullptr )
	{
		//NOTE: SSL is initialized at App-level for Voxox

		//Create signaling thread
		mXmppThread = new buzz::XmppThread( getTlsOption() );
		mXmppThread->SetName( "XmppSignalThread", nullptr );

		rtc::ThreadManager::Instance()->SetCurrentThread( mXmppThread );

		mXmppThread->Start();

		initSignalHandlers();
	
		initConnectivityThread();
	}
}

//Once we call Disconnect(), the XmppThread appears to be no longer valid.
//	We clear it, so we can create a new one, in event of network loss.
//	Otherwise, a simple call to connect() fails because we do not have a valid XmppClient.
void VoxoxXmpp::destroyXmppThread()
{
	if ( mXmppThread )
	{
		mXmppThread->Quit();
		delete mXmppThread;
		mXmppThread = nullptr;
	}
}
	
void VoxoxXmpp::startPingTask()
{
	if ( mPingTask == nullptr )
	{
		mPingTask = new buzz::PingTask( getXmppClient(), rtc::ThreadManager::Instance()->CurrentThread(), kPingPeriodMillis, kPingTimeoutMillis);

		mPingTask->SignalTimeout.connect(this, &VoxoxXmpp::onPingTimeout);
		mPingTask->SignalSuccess.connect(this, &VoxoxXmpp::onPingSuccess);
		mPingTask->Start();
	}
}

void VoxoxXmpp::initConnectivityThread()
{
	if ( mConnectivityThread == nullptr )
	{
		mConnectivityThread = new ConnectivityThread( getServer(), mProxyInfo );

		mConnectivityThread->SetName( "XmppConnectivityThread", nullptr );
		mConnectivityThread->SignalConnectionChange.connect( this, &VoxoxXmpp::onConnectivityChange );
		mConnectivityThread->Start();
	}
}

void VoxoxXmpp::destroyConnectivityThread()
{
	if ( mConnectivityThread )
	{
		mConnectivityThread->Quit();
		delete mConnectivityThread;
		mConnectivityThread = nullptr;
	}
}

void VoxoxXmpp::initSignalHandlers()
{
	buzz::XmppClient* client = getXmppClient();

	if ( client != nullptr )
	{
		//Logging
		client->SignalLogInput.connect ( this, &VoxoxXmpp::onInputLogging  );
		client->SignalLogOutput.connect( this, &VoxoxXmpp::onOutputLogging );

		//StateChange handler.
		client->SignalStateChange.connect( this, &VoxoxXmpp::onStateChange );
	}
}

void VoxoxXmpp::login( const XmppParameters& xmppParameters )
{
	mXmppParameters = xmppParameters;
	mResource	    = getResourcePrefix() + generateResourceSuffix();

	initProxyInfo();

	mJid = buzz::Jid( getUserId(), getDomain(), mResource );

	initClientSettings();

	//Force same logic no matter how we connect (login or reconnect).
	mAutoConnect = true;
	onSignOff();	
}

void VoxoxXmpp::connect()
{		
	initXmppThread();

	//NOTE: DoLogin() could fail, but it does NOT return an error code.  DOH!
	mXmppThread->Login( mClientSettings );
}

void VoxoxXmpp::initProxyInfo()
{
	if ( mXmppParameters.getUseProxy() )
	{
		if ( !mXmppParameters.getProxyServer().empty() && mXmppParameters.getProxyPort() != 0 )
		{
			logIt( LogEntry::Level::Debug, "Using provided proxy info..." );
			mProxyInfo.address = rtc::SocketAddress( mXmppParameters.getProxyServer(), mXmppParameters.getProxyPort() );
		}
	}
	else
	{
		//Detect proxy on our own.
		logIt( LogEntry::Level::Debug, "Auto-Detect proxy info..." );
		mProxyInfo = doAutoDetectProxy( "MSIE", mXmppParameters.getServer() );	//TODO: get this from main app.  Could be MSIE or Firefox.
	}
	
	if ( !mProxyInfo.address.IsNil() )
	{
		std::string name;
		std::string password;
		
		if ( mProxyInfo.username.size() == 0 )
		{
			name     = "<none>";
			password = "<none>";
		}
		else
		{
			name     = mProxyInfo.username;
			password = "<encrypted>";
		}

		std::string msg = "Using Proxy - Address: "		+ mProxyInfo.address.HostAsURIString() + 
										 ", Port: "		+ mProxyInfo.address.PortAsString()    +
										 ", UserName: " + name								   +
										 ", Password: " + password;
	
		logIt( LogEntry::Level::Debug, msg );
	}
}

void VoxoxXmpp::initClientSettings()
{
	if (mJid.IsValid() && mJid.node() != "")
	{
		rtc::InsecureCryptStringImpl pass;
		pass.password() = getPassword().c_str();

		rtc::CryptString cryptPassword(pass);

		//Password may also be AuthToken.  Maybe.
		mClientSettings.set_user(mJid.node());
		mClientSettings.set_host(mJid.domain());
		mClientSettings.set_resource(mJid.resource());
		mClientSettings.set_pass(cryptPassword);
		mClientSettings.set_auth_token(getAuthMechanism(), getPassword().c_str());

		mClientSettings.set_server(rtc::SocketAddress(getServer(), getPort()));
		mClientSettings.set_use_tls(getTlsOption());
		mClientSettings.set_allow_plain(getAllowPlainPassword());

		if ( !mProxyInfo.address.IsNil() )
		{
			//IT APPEARS THESE PROXY VALUES ARE *NEVER* USED IN LIBJINGLE.  DOH!
			//mClientSettings.set_proxy     ( mProxyInfo.type				  );
			//mClientSettings.set_proxy_host( mProxyInfo.address.hostname() );
			//mClientSettings.set_proxy_port( mProxyInfo.address.port()     );

			////TODO: This code is untested
			////Proxy credentials
			//bool			 useProxyAuth = mProxyInfo.username.size() > 0;
			//rtc::CryptString cryptProxyPassword(mProxyInfo.password);

			//mClientSettings.set_use_proxy_auth( useProxyAuth		);
			//mClientSettings.set_proxy_user    ( mProxyInfo.username );
			//mClientSettings.set_proxy_pass    ( cryptProxyPassword	);

			//This was added to ClientSettings by VOXOX.  This value IS used.
			mClientSettings.set_proxyInfo( mProxyInfo );
		}
	}
	else
	{
		//LOG "Invalid JID.";	//TODO-LOG
	}
}

void VoxoxXmpp::logoff()
{
	disconnect();

	destroyConnectivityThread();
}

void VoxoxXmpp::disconnect()
{
	mXmppThread->Disconnect();		//This will trigger the onSignOff event
}

void VoxoxXmpp::initMyStatus( const buzz::Jid& jid, buzz::PresenceStatus::Show show, const std::string& status  /*, const std::string& nickName*/ )
{
	mMyStatus.set_jid      ( jid    );
	mMyStatus.set_show     ( show   );		//Default SHOW_NONE
	mMyStatus.set_available( true   );		//Default 'false'
	mMyStatus.set_status   ( status );		//Default empty string
//	mMyStatus.set_nick( nickName );

//	mMyStatus.set_priority( int pri );		//Default 0		//Not sure how this is used

//	mMyStatus.set_error(int e_code, const std::string e_str);	//Default 0

	//Just use default 'false' for these.  We do NOT need these for simple XMPP
//	mMyStatus.set_know_capabilities( false );
//	mMyStatus.set_voice_capability ( false );
//	mMyStatus.set_pmuc_capability  ( false );
//	mMyStatus.set_video_capability ( false );
//	mMyStatus.set_camera_capability( false );

	//mMyStatus.set_caps_node(const std::string& f)

 //   feedback_probation_(false),

	//mMyStatus.set_version(const std::string& v)
	//mMyStatus.set_feedback_probation(bool f)
	//mMyStatus.set_sent_time(const std::string& time)
}

void VoxoxXmpp::updateMyStatus( buzz::PresenceStatus::Show show, const std::string& status )
{
	mMyStatus.set_show     ( show   );		//Default SHOW_NONE
	mMyStatus.set_status   ( status );		//Default empty string
//	mMyStatus.set_available( true   );		//Default 'false'		//TODO: Confirm how this works with set_show

	mPresenceOutTask->Send( mMyStatus );
	mPresenceOutTask->Start();
}

bool VoxoxXmpp::sendChat( const std::string& toJidString, const std::string& body, const std::string& msgId )
{
	bool		result = false;
	buzz::Jid   toJid  = toJidFromString( toJidString );

	if ( isFromJidValid() )
	{
		if ( mSendTask != nullptr )
		{
			buzz::XmppReturnStatus status = mSendTask->sendMessage( toJid, body, msgId );
			result = (status == buzz::XmppReturnStatus::XMPP_RETURN_OK);
		}
	}
	else
	{
		//TODO:  Log Error
	}

	return result;
}

bool VoxoxXmpp::sendChatState( const std::string& toJidString, bool composing )
{
	bool		result = false;
	buzz::Jid   toJid  = toJidFromString( toJidString );

	if ( isFromJidValid() )
	{
		if ( mSendTask != nullptr )
		{
			buzz::XmppReturnStatus status = mSendTask->sendChatState( toJid, composing );
			result = (status == buzz::XmppReturnStatus::XMPP_RETURN_OK);
		}
	}
	else
	{
		//TODO:  Log Error
	}

	return result;
}
	
bool VoxoxXmpp::sendReadReceipt( const std::string& jid, const std::string& msgId )
{
	buzz::Jid toJid = toJidFromString( jid );

	bool result = false;

	if ( isFromJidValid() )
	{
		if ( mSendTask != nullptr )
		{
			buzz::XmppReturnStatus status = mSendTask->sendReadReceipt( toJid, msgId );
			result = (status == buzz::XmppReturnStatus::XMPP_RETURN_OK);
		}
	}
	else
	{
		//TODO:  Log Error
	}

	return result;
}

void VoxoxXmpp::requestVCard( const std::string& toJidString )
{
	buzz::Jid   toJid = toJidFromString( toJidString );

	if ( isFromJidValid() )
	{
		mSendTask->requestVCard( toJid );	//TODO: Results in 'error' iq response.
	}
	else
	{
		//TODO:  Log Error
	}
}

buzz::Jid VoxoxXmpp::toJidFromString( const std::string& jidString )
{
	buzz::Jid temp( jidString );
	buzz::Jid result = temp.BareJid();		//Due to Carbons we send to bare JIDs.

	return result;
}

bool VoxoxXmpp::isFromJidValid()
{
	return ( mJid.IsValid() && mJid.node() != "" );
}

void VoxoxXmpp::initDiscovery()
{
	taskCleanUp();
	initTasks();
}

void VoxoxXmpp::taskCleanUp()
{
	//These appear to be deleted by the XmppPump/XmppClient to which they are added, so just nullify the ptrs
//	mPresenceReceiveTask = nullptr;
	mPresenceOutTask	 = nullptr;
	mSendTask			 = nullptr;
	mMessageReceiveTask  = nullptr;
	mIqReceiveTask		 = nullptr;
	mPingTask			 = nullptr;
}

void VoxoxXmpp:: initTasks()
{
	buzz::XmppClient* xmppClient = getXmppClient();

	if ( xmppClient != nullptr )
	{
//		mPresenceReceiveTask = new buzz::PresenceReceiveTask( xmppClient );
		mPresenceOutTask     = new buzz::PresenceOutTask    ( xmppClient );
		mSendTask			 = new XmppSendTask				( xmppClient );
		mMessageReceiveTask  = new MessageReceiveTask		( xmppClient );
		mIqReceiveTask		 = new IqReceiveTask			( xmppClient );

		startPingTask();

//		mPresenceReceiveTask->PresenceUpdate.connect( this, &VoxoxXmpp::onPresenceUpdate);	//NOTE: this signal does NOT follow naming convention 'Signalxxx'.
		
		mMessageReceiveTask->SignalXmppMsg.connect( this, &VoxoxXmpp::onXmppMsg );
		mIqReceiveTask->SignalIqMsg.connect       ( this, &VoxoxXmpp::onXmppMsg );
	}
}

//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
//Begin convenience methods (mostly for XmppParameters)
//-----------------------------------------------------------------------------
//User
std::string	VoxoxXmpp::getUserId() const
{
	return mXmppParameters.getUserId();
}

std::string	VoxoxXmpp::getPassword() const
{
	return mXmppParameters.getPassword();
}

std::string	VoxoxXmpp::getStatus() const
{
	return mXmppParameters.getStatus();
}

buzz::PresenceStatus::Show VoxoxXmpp::getInitialShow() const
{
	return (buzz::PresenceStatus::Show)mXmppParameters.getInitialShow();
}

//Connection
std::string	VoxoxXmpp::getDomain() const
{
	return mXmppParameters.getDomain();
}

std::string	VoxoxXmpp::getServer() const
{
	return mXmppParameters.getServer();
}

std::string	VoxoxXmpp::getResourcePrefix() const
{
	return mXmppParameters.getResourcePrefix();
}

std::string	VoxoxXmpp::getAuthMechanism() const
{
	std::string result;

	switch ( mXmppParameters.getAuthMechanism() )
	{
	case XmppParameters::AUTH_MECH_NONE:	//This is invalid value
		break;

	case XmppParameters::AUTH_MECH_PLAIN:
		result = buzz::AUTH_MECHANISM_PLAIN;
		break;

//	case XmppParameters::AUTH_MECH_DIGEST_MD5:
//		result = buzz::AUTH_MECHANISM_DIGEST_MD5;
//		break;

	case XmppParameters::AUTH_MECH_SCRAM_SHA_1:
		result = buzz::AUTH_MECHANISM_SCRAM_SHA_1;
		break;

	case XmppParameters::AUTH_MECH_OAUTH2:
		result = buzz::AUTH_MECHANISM_OAUTH2;
		break;
	}

	return result;
}

int	VoxoxXmpp::getPort() const
{
	return mXmppParameters.getPort();
}

bool VoxoxXmpp::getAllowPlainPassword()	const
{
	return mXmppParameters.getAllowPlainPassword();
}

buzz::TlsOptions VoxoxXmpp::getTlsOption() const
{
	return (buzz::TlsOptions ) mXmppParameters.getTlsOption();
}

//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
//Begin signal handlers (slots)
//	- all these method names should start with 'on' by convention
//-----------------------------------------------------------------------------

void VoxoxXmpp::onPingTimeout()
{
	//When we hit timeout, PingTask stops, so do some cleanup.
	mPingTask  = nullptr;	//Should self-destruct, so just reset ptr.	

	onConnectivityChange( false );
}

void VoxoxXmpp::onPingSuccess()
{
	onConnectivityChange( true );
}

void VoxoxXmpp::onConnectivityChange( bool canConnect )
{
	if ( mConnected != canConnect )
	{
		if ( canConnect )
		{
			if ( !mConnected && mAutoConnect )
			{
				connect();
			}
		}
		else
		{
			if ( mConnected )
			{
				mAutoConnect = true;
				disconnect();
			}
		}
	}
}

void VoxoxXmpp::onStateChange( buzz::XmppEngine::State state )
{
	//NOTE: these are all internal state changes.  No need to report all of them, though we may consider logging them.  TODO-LOG?
	switch (state) 
	{
	case buzz::XmppEngine::STATE_START:
		// Attempting sign in.
		//TODO: Update progress?
		break;

	case buzz::XmppEngine::STATE_OPENING:
		// Negotiating with server.
		//TODO: Update progress?
		break;

	case buzz::XmppEngine::STATE_OPEN:
		// Connection succeeded. 
		//	Send your presence information and sign up to receive presence notifications.
		onSignOn();
		break;

	case buzz::XmppEngine::STATE_CLOSED:
		// Connection ended. 
		onSignOff();
		break;
	}
}

void VoxoxXmpp::onSignOn()
{
	mConnected   = true;
	mAutoConnect = false;

	initMyStatus(mJid, getInitialShow(), getStatus());
	initDiscovery();

	//Do this before sending our own presence to avoid missing presence notifications.
	mRosterHandler = new VoxoxRosterHandler( this );

	//mRoster1 = buzz::XmppRosterModuleImpl::Create();
	//mRoster1->set_roster_handler( mRosterHandler );		//For callbacks  buzz::XmppRosterHandler*
	//mRoster1->RegisterEngine( mXmppPump->client()->engine() );
	//mRoster1->BroadcastPresence();
	//mRoster1->RequestRosterUpdate();

	//Prepare to receive presence Updates
//	mPresenceReceiveTask->Start();

	mSendTask->Start();
	mMessageReceiveTask->Start();
//	mIqReceiveTask->Start();

	//Send initial My Presence.
	mPresenceOutTask->Send( mMyStatus );
	mPresenceOutTask->Start();

	signOnEvent();
}

//We want to handle this on a thread separate from SignalThread (XmppThread)
void VoxoxXmpp::onSignOff()
{
	ShutdownThread* thread = new ShutdownThread( this );
	thread->SetName( "XmppShutdownThread", nullptr );
	thread->Start();	//Will self-destruct when complete.
}

void VoxoxXmpp::handleSignOff()
{
	destroyXmppThread();

	if ( mConnected )
	{
		mConnected = false;
		signOffEvent();
	}

	//Handle/server loss of network.
	if ( mAutoConnect )
	{
		initConnectivityThread();
	}
}

void VoxoxXmpp::onPresenceUpdate( const buzz::PresenceStatus& status )
{
	RosterItem item( status.jid(), status.show(), status.status() );

	std::string key = item.getJid().Str();

	//TODO: Review this code from example.  It appears to add/remove based on status.available()
	if ( status.available() /*&& status.voice_capability()*/ )	//Sample was from CallClient.
	{
		mRoster.add( item );
	} 
	else 
	{
		mRoster.remove( item );
	}

	presenceUpdateEvent( status );
}

void VoxoxXmpp::onXmppMsg( const XmppMsg& msg )
{
	//Add some extra data to make it easier for main app
	buzz::Jid fromJid( msg.getFrom() );
	const_cast<XmppMsg&>(msg).setFromResource( fromJid.resource() );
	const_cast<XmppMsg&>(msg).setMyResource( mResource );

	incomingMsgEvent( msg );
}

void VoxoxXmpp::onInputLogging( const char* data, int len ) 
{
	prepareLog( std::string( data, len ), false );
}

void VoxoxXmpp::onOutputLogging( const char* data, int len ) 
{
	prepareLog( std::string( data, len ), true );
}

void VoxoxXmpp::prepareLog( const std::string& msgIn, bool outgoing )
{
	if ( !isPingMsg( msgIn, outgoing ) )
	{
		std::string prefix = (outgoing ? "OUT: " : "IN:  " );
		std::string msg    = prefix + msgIn;

		logIt( LogEntry::Level::Debug, msg );
	}
}

bool VoxoxXmpp::isPingMsg( const std::string& msg, bool outgoing )
{
	bool result = false;

	if ( outgoing )
	{
		result = ( msg.find(sPingMsgId) != std::string::npos );

		if ( result )
		{
			extractMsgId( msg );
		}
	}
	else
	{
		if ( msg.find( "<iq" ) == 0 )
		{
			if ( !mPrevPingMsgId1.empty() )
			{
				result = ( msg.find( mPrevPingMsgId1 ) != std::string::npos );

				if ( !result )
					result = ( msg.find( mPrevPingMsgId2 ) != std::string::npos );
			}
		}

		mPrevPingMsgId1.clear();
		mPrevPingMsgId2.clear();
	}

	return result;
}

void VoxoxXmpp::extractMsgId( const std::string& msg )
{
	const std::string idTag       = "id=";
	const std::string singleQuote = "'";
	const std::string dblQuote    = "\"";

	//Find id=
	size_t pos1 = msg.find( idTag );
	size_t pos2 = std::string::npos;

	if ( pos1 != std::string::npos )
	{
		//id= can use single or double quote, so test for which it is.
		std::string quote = msg.substr( pos1 + idTag.size(), 1 );

		//Get position of SECOND quote
		pos2 = msg.find( quote, pos1 + idTag.size() + 1 );

		if ( pos2 != std::string::npos )
		{
			mPrevPingMsgId1 = msg.substr( pos1, pos2 - pos1 + 1 );

			//Get version with other type of quotes.
			if ( quote == singleQuote )
				mPrevPingMsgId2 = replaceAll( mPrevPingMsgId1, singleQuote, dblQuote );
			else
				mPrevPingMsgId2 = replaceAll( mPrevPingMsgId1, dblQuote, singleQuote );
		}
		else
		{
			//This is an error we can ignore
		}
	}
}

std::string VoxoxXmpp::replaceAll( const std::string& textIn, const std::string& before, const std::string& after )
{
	std::string result = textIn;

    if( !before.empty() )
	{
        for( size_t pos = 0; (pos = result.find(before, pos)) != std::string::npos; pos += after.size() )
		{
            result.replace( pos, before.size(), after);
		}
	}	

    return result;
}

//-----------------------------------------------------------------------------
// End Signal handlers (slots)
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
//App callbacks for events
//-----------------------------------------------------------------------------

void VoxoxXmpp::signOnEvent()
{
	if ( mAppEventHandler != nullptr )
	{
		mAppEventHandler->signOnEvent();
	}
}

void VoxoxXmpp::signOffEvent()
{
	if ( mAppEventHandler != nullptr )
	{
		mAppEventHandler->signOffEvent();
	}
}

void VoxoxXmpp::presenceUpdateEvent( const buzz::PresenceStatus& status )
{
	if ( mAppEventHandler != nullptr )
	{
		mAppEventHandler->presenceUpdateEvent( status.jid().Str(), status.priority(), status.show(), status.status(), status.nick(), status.available(), status.sent_time() );
	}
}

void VoxoxXmpp::incomingMsgEvent( const XmppMsg& msg )
{
	if ( mAppEventHandler != nullptr )
	{
		//Auto-send receipt
		if ( msg.isTypeMsg() )
		{
			buzz::Jid toJid( msg.getFrom() );

			buzz::XmppReturnStatus status = mSendTask->sendDeliveryReceipt( toJid, msg.getMessageId() );
			bool result = (status == buzz::XmppReturnStatus::XMPP_RETURN_OK);
		}

		mAppEventHandler->incomingMsgEvent( msg.getTo(), msg.getFrom(), msg.getMessageId(), msg.getBody(), msg.getChatState(), 
											msg.getFromResource(), msg.getMyResource(), msg.getReceiptMsgId(), msg.getType() );
	}
}

void VoxoxXmpp::sendLogEvent( const LogEntry& logEntry )
{
	if ( mAppEventHandler != nullptr )
	{
		mAppEventHandler->sendLogEvent( logEntry );
	}
}

//-----------------------------------------------------------------------------
//End App callbacks for events
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
//Helper methods that maybe should be in utility class
//-----------------------------------------------------------------------------

__int64 VoxoxXmpp::getSystemTimeMillis()
{
	FILETIME fileTime;		//100-nanosecond intervals, so divide by 10,000 to get millis
	::GetSystemTimeAsFileTime( &fileTime );

	__int64 nanos;
	memcpy( &nanos, &fileTime, 8);
	return nanos / 10000;
}
	
buzz::XmppClient* VoxoxXmpp::getXmppClient() const
{ 
	return  (mXmppThread == nullptr ? nullptr : mXmppThread->client() );	
}

void VoxoxXmpp::logIt( LogEntry::Level level, const std::string& msg )
{
	LogEntry log;
	log.setLevel( level );
	log.setMessage( msg );

	sendLogEvent( log );
}

//-----------------------------------------------------------------------------
//Begin Proxy-related methods
//-----------------------------------------------------------------------------

rtc::ProxyInfo VoxoxXmpp::doAutoDetectProxy( const std::string& userAgent, const std::string& server )
{
	rtc::AutoDetectProxy* autoDetectProxy = new rtc::AutoDetectProxy( userAgent );

	//Expectation is that 'server' will be naked, no scheme.  IOW, 'im.voxox.com' vs. 'http://im.voxox.com'
	//	However, AutoDetectProxy expects (or at least responds better) with the scheme, so let's normalize it.
	//	The WebRtc Url class does NOT handle this cleanly, so we do the work in normalizeServer().
	std::string url = normalizeServer( server );

	autoDetectProxy->set_server_url( url );
    autoDetectProxy->SignalWorkDone.connect( this, &VoxoxXmpp::onAutoDetectProxyWorkDone );

	mProxyDetectionDone = false;
	autoDetectProxy->Start();

	int maxWait   = 10000;
	int waitInc   =   100;
	int totalWait =     0;

	while ( ! mProxyDetectionDone && totalWait < maxWait )
	{
		Sleep( waitInc );
		totalWait += waitInc;
	}

	std::string msg = "doAutoDetectProxy took: " + std::to_string( totalWait ) + " ms.";
	logIt( LogEntry::Level::Debug, msg );

	rtc::ProxyInfo proxy = autoDetectProxy->proxy();

	autoDetectProxy->Release();
	autoDetectProxy = nullptr;

	return proxy;
}

void VoxoxXmpp::onAutoDetectProxyWorkDone( rtc::SignalThread* thread ) 
{
	mProxyDetectionDone = true;
}

// The WebRtc Url class does NOT handle this cleanly, so we do the work here.
// Goal is to get something like 'http://server.com'
std::string VoxoxXmpp::normalizeServer( const std::string& serverIn )
{
	const std::string preferredScheme = "http://";
	const std::string schemeSeparator = "://";

	std::string server = serverIn;

	size_t pos = server.find( schemeSeparator );

	if ( pos < 7 )
	{
		server = server.substr( pos + schemeSeparator.size() );
	}

	std::string result = "http://" + server;

	return result;
}

//-----------------------------------------------------------------------------
//End Proxy-related methods
//-----------------------------------------------------------------------------

//static
bool VoxoxXmpp::testWebRequest( const std::string& url, bool connectOnly )
{
	bool result = false;

	WebRequest request;

	result = request.makeRequest( url, connectOnly );

	return result;
}

//static
bool VoxoxXmpp::testWinHttpClient( const std::string& urlIn, const std::string& proxyIn, bool connectOnly )
{
	bool result = false;

	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;	//C++11

	std::wstring url   = converter.from_bytes( urlIn   );
	std::wstring proxy = converter.from_bytes( proxyIn );

    WinHttpClient client( url );

    client.SetProxy( proxy );

    result = client.SendHttpRequest( connectOnly );

	if ( result && !connectOnly )
	{
		wstring httpResponseHeader  = client.GetResponseHeader();
		wstring httpResponseContent = client.GetResponseContent();
	}

	return result;
}
