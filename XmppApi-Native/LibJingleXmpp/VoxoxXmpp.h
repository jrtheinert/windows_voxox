
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Oct 2014
 */

#pragma once

#include "DataTypes.h"			//Mostly for AppEventHandler
#include "VoxoxXmppTasks.h"

#include <string>
#include <map>					//rostermoduleimpl.h needs this, so rather than change the open-source, let's include it here.
#include <vector>

#include "webrtc/base/sigslot.h"
#include "webrtc/base/signalthread.h"	//For AutoDetectProxy

#include "talk/xmpp/jid.h"
#include "talk/xmpp/pingtask.h"
#include "talk/xmpp/presenceouttask.h"
#include "talk/xmpp/presencereceivetask.h"
#include "talk/xmpp/presencestatus.h"
#include "talk/xmpp/rostermoduleimpl.h"
#include "talk/xmpp/xmppengine.h"		//TlsOption
#include "talk/xmpp/xmppclientsettings.h"
#include "talk/xmpp/xmppthread.h"

class VoxoxXmpp;

// sigslot::has_slots interface is required for any class that will be receiving/handling
//	signals/events from the LibJingle XMPP code.

//=============================================================================

class RosterItem
{
public:
	RosterItem();
	RosterItem( const buzz::Jid jid, buzz::PresenceStatus::Show show, const std::string& status );

	//Sets -----------------------------------------------------------------------
	void setJid   ( const buzz::Jid				value )			{ mJid    = value;	}
	void setShow  ( buzz::PresenceStatus::Show	value )			{ mShow   = value;	}
	void setStatus( const std::string&			value )			{ mStatus = value;	}

	//Sets -----------------------------------------------------------------------
	buzz::Jid					getJid   ()	const				{ return mJid;		}
	buzz::PresenceStatus::Show	getShow  () const				{ return mShow;		}
	std::string					getStatus()	const				{ return mStatus;	}

	//Misc -----------------------------------------------------------------------
	RosterItem& operator= ( const RosterItem& src );
	bool	    operator==( const RosterItem& src );

private:
	buzz::Jid					mJid;
	buzz::PresenceStatus::Show	mShow;
	std::string					mStatus;
};

//=============================================================================


//=============================================================================

class Roster : public std::map<std::string, RosterItem>
{
public:
	void add   ( const RosterItem& item );
	void remove( const RosterItem& item );
};

//=============================================================================


//=============================================================================

class VoxoxRosterHandler : public buzz::XmppRosterHandler
{
public:
	VoxoxRosterHandler( VoxoxXmpp* voxoxXmpp );

	void SubscriptionRequest	( buzz::XmppRosterModule* roster, const buzz::Jid& requesting_jid, buzz::XmppSubscriptionRequestType type, const buzz::XmlElement* raw_xml );
	void SubscriptionError		( buzz::XmppRosterModule* roster, const buzz::Jid& from,           const buzz::XmlElement* raw_xml );

	void IncomingPresenceChanged( buzz::XmppRosterModule* roster, const buzz::XmppPresence* presence );
	
	void ContactChanged			( buzz::XmppRosterModule* roster, const buzz::XmppRosterContact* old_contact,     size_t index );
	void ContactRemoved			( buzz::XmppRosterModule* roster, const buzz::XmppRosterContact* removed_contact, size_t index );
	void ContactsAdded			( buzz::XmppRosterModule* roster, size_t index, size_t number );
	
	void RosterError			( buzz::XmppRosterModule* roster, const buzz::XmlElement* raw_xml );

private:
	VoxoxXmpp*	mVoxoxXmpp;
};

//=============================================================================


//=============================================================================
//Simple thread to do execute shutdown thread NOT on SignalThread

class ShutdownThread : public rtc::Thread
{
public:
	ShutdownThread( VoxoxXmpp* voxoxXmpp );
	~ShutdownThread();

	void Run();

private:
	VoxoxXmpp*	mVoxoxXmpp;
};

//=============================================================================


//=============================================================================

class ConnectivityThread : public rtc::Thread, public sigslot::has_slots<>
{
public:
	ConnectivityThread( const std::string& server, const rtc::ProxyInfo& proxyInfo );
	~ConnectivityThread();

	void Run();

	sigslot::signal1<bool> SignalConnectionChange;

private:
	bool testConnectivity( bool connectOnly );
	void initProxyInfo();
	void normalizeServer();
	void handleChange( bool canConnect );

private:
	bool mConnected;
	std::string		mServer;

	//Proxy info
	rtc::ProxyInfo	mProxyInfo;

	//Parsed proxy info to use with WinHttpClient
	bool		 mProxyParsed;
	bool		 mUseProxy;
	std::wstring mProxyServer;
	std::wstring mProxyUserName;
	std::wstring mProxyPassword;
};

//=============================================================================


//=============================================================================

class VoxoxXmpp : public sigslot::has_slots<>
{
public:
	VoxoxXmpp();
	~VoxoxXmpp();

	void login( const XmppParameters& xmppParameters );
	void logoff();

	void updateMyStatus( buzz::PresenceStatus::Show show, const std::string& status );

	bool sendChat       ( const std::string& jid, const std::string& body, const std::string& msgId );
	bool sendChatState  ( const std::string& jid, bool composing );
	bool sendReadReceipt( const std::string& jid, const std::string& msgId );
	
	void requestVCard( const std::string& jid );

	void setAppEventHandler( AppEventHandler* value )		{ mAppEventHandler = value;	}
	AppEventHandler* getAppEventHandler() const				{ return mAppEventHandler;  }	//Mostly for Unit Tests.

	bool isConnected() const								{ return mConnected;	}

	void handleSignOff();	//Public so it can be accessed via separate thread.

	static bool testWebRequest   ( const std::string& url, bool connectOnly );
	static bool testWinHttpClient( const std::string& url, const std::string& proxy, bool connectOnly );

private:
	void connect();
	void disconnect();

	void initProxyInfo();
	void initXmppThread();
	void initSignalHandlers();
	void initClientSettings();
	void initDiscovery();
	void initMyStatus ( const buzz::Jid& jid, buzz::PresenceStatus::Show show, const std::string& status /*, const std::string& nickName*/ );	//TODO: Support nickName/alias?

	void initTasks();
	void taskCleanUp();
	void destroyXmppThread();

	bool isFromJidValid();
	std::string generateResourceSuffix();

	buzz::Jid toJidFromString( const std::string& jidString );

	//Proxy
	rtc::ProxyInfo	doAutoDetectProxy( const std::string& userAgent, const std::string& server );
	void			onAutoDetectProxyWorkDone( rtc::SignalThread* thread );
	std::string		normalizeServer( const std::string& server );

	//Connectivity
	void startPingTask();
	void initConnectivityThread();
	void destroyConnectivityThread();

	//Slots (for internal event handling)
	void onStateChange( buzz::XmppEngine::State state );
	void onSignOn();
	void onSignOff();
	void onPresenceUpdate( const buzz::PresenceStatus& status );
	void onXmppMsg( const XmppMsg& msg );
	void onInputLogging ( const char* data, int len );
	void onOutputLogging( const char* data, int len ); 

	//Connectivity
	void onConnectivityChange( bool canConnect );
	void onPingTimeout();
	void onPingSuccess();

	//App callback events
	void signOnEvent();
	void signOffEvent();
	void presenceUpdateEvent( const buzz::PresenceStatus& status );
	void incomingMsgEvent( const XmppMsg& msg );
	void sendLogEvent( const LogEntry& logEntry );

	//Convenience/Helper methods for XmppParameters
	//User
	std::string					getUserId()				const;
	std::string					getPassword()			const;
	std::string					getStatus()				const;
	buzz::PresenceStatus::Show	getInitialShow()		const;

	//Connection
	std::string					getDomain()				const;
	std::string					getServer()				const;
	std::string					getResourcePrefix()		const;
	std::string					getAuthMechanism()		const;
	int							getPort()				const;
	bool						getAllowPlainPassword()	const;
	buzz::TlsOptions			getTlsOption()			const;

	//Helper/Utility
	__int64		getSystemTimeMillis();
	std::string replaceAll( const std::string& textIn, const std::string& before, const std::string& after );

	buzz::XmppClient*			getXmppClient() const;
	
	void logIt     ( LogEntry::Level level,  const std::string& msg );

	void prepareLog  ( const std::string& msg, bool outgoing );
	bool isPingMsg   ( const std::string& msg, bool outgoing );
	void extractMsgId( const std::string& msg );


private:
	AppEventHandler*			mAppEventHandler;

	XmppParameters				mXmppParameters;

	std::string					mResource;
	bool						mConnected;
	bool						mAutoConnect;			//In case of network loss.
	bool						mProxyDetectionDone;

	std::string					mPrevPingMsgId1;
	std::string					mPrevPingMsgId2;

	rtc::ProxyInfo				mProxyInfo;

	ConnectivityThread*			mConnectivityThread;

	buzz::XmppThread*			mXmppThread;
	buzz::PingTask*				mPingTask;

	buzz::XmppRosterModule*		mRoster1;
	buzz::Jid					mJid;
	buzz::XmppClientSettings	mClientSettings;
	buzz::PresenceStatus		mMyStatus;

//	buzz::PresenceReceiveTask*	mPresenceReceiveTask;	//This causes crash on disconnect/reconnect
	buzz::PresenceOutTask*		mPresenceOutTask;

	XmppSendTask*				mSendTask;
	MessageReceiveTask*			mMessageReceiveTask;	//IMs and ChatState
	IqReceiveTask*				mIqReceiveTask;			//IQs

	Roster						mRoster;
	VoxoxRosterHandler*			mRosterHandler;
};
