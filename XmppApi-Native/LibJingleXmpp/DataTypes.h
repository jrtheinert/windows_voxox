/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Oct 2014
 */

//-----------------------------------------------------------------------------
// This file will contain all the data type definitions used in Xmpp interface.
//
// Most/All of these classes will have C# counterparts in the main app.
//	So we will have to match the C# / C++ classes during data merging.
//
// As such, there will be almost no logic in these classes.  Just set/get methods.
//
// Keeping all the definitions in a single class makes C# intergration easier,
//	but should not be considered an overriding concern.
//
// The logic in existing Android classes should be implemented in the C# classes, 
//	not here.  Except where it will improve readability of Xmpp code.
//
// Let's try to keep the classes in alphabetical order.  In some instances we cannot because
//	we use class types that need to be defined.
//		TODO: Consider moving all the enums into a single defined class.
//
// Many of these classes will also have 'collection' versions, typically a list.
//	I hope that in most cases, std::list<class> will suffice.
//	These may just become typedefs.
//
// Each class should have:
//	- setters/getters
//	- operator=
//	- copy ctor (maybe)
//
//-----------------------------------------------------------------------------
#pragma once

#include <string>
#include <list>

#include "DllExport.h"

class LogEntry;
class XmppEventData;

typedef void (*XmppEventCallbackFunc)(const XmppEventData&);

//=============================================================================
//Small container class to handle multiple elements of incoming chat message to faciliate XmppEventData
class ChatMsg
{
public:
	//NOTE: These come from XMPP stack so do NOT change them, except to match changes in XMPP stack.
	enum Type
	{ 
		None				= 0,
		DeliveredReceipt	= 1,	
		ReadReceipt			= 2,
		ChatState			= 3,
		Msg					= 4,	//Implies ChatState
		Carbon				= 5,	//Implies Msg
	};

	DLLEXPORT ChatMsg();
	DLLEXPORT ~ChatMsg();

	//Sets --------------------------------------------------------------------
	DLLEXPORT void setToJid		  ( const std::string& value )			{ mToJid		= value;	}
	DLLEXPORT void setFromJid	  ( const std::string& value )			{ mFromJid		= value;	}
	DLLEXPORT void setMsgId		  ( const std::string& value )			{ mMsgId		= value;	}
	DLLEXPORT void setBody		  ( const std::string& value )			{ mBody			= value;	}
	DLLEXPORT void setChatState	  ( const std::string& value )			{ mChatState	= value;	}
	DLLEXPORT void setFromResource( const std::string& value )			{ mFromResource = value;	}
	DLLEXPORT void setMyResource  ( const std::string& value )			{ mMyResource	= value;	}
	DLLEXPORT void setReceiptMsgId( const std::string& value )			{ mReceiptMsgId	= value;	}
	DLLEXPORT void setType        ( Type			   value )			{ mType			= value;	}

	//Gets --------------------------------------------------------------------
	DLLEXPORT std::string	getToJid()			const					{ return mToJid;		}
	DLLEXPORT std::string	getFromJid()		const					{ return mFromJid;		}
	DLLEXPORT std::string	getMsgId()			const					{ return mMsgId;		}
	DLLEXPORT std::string	getBody()			const					{ return mBody;			}
	DLLEXPORT std::string	getChatState()		const					{ return mChatState;	}
	DLLEXPORT std::string	getFromResource()	const					{ return mFromResource;	}
	DLLEXPORT std::string	getMyResource()		const					{ return mMyResource;	}
	DLLEXPORT std::string	getReceiptMsgId()	const					{ return mReceiptMsgId;	}
	DLLEXPORT Type			getType()			const					{ return mType;	}
	
	//Misc --------------------------------------------------------------------
	DLLEXPORT ChatMsg& operator=( const ChatMsg& src );

	DLLEXPORT bool isDeliveredReceipt()			{ return getType() == Type::DeliveredReceipt;	}
	DLLEXPORT bool isReadReceipt()				{ return getType() == Type::ReadReceipt;		}
	DLLEXPORT bool isChatState()				{ return getType() == Type::ChatState;	}
	DLLEXPORT bool isMsg()						{ return getType() == Type::Msg;		}
	DLLEXPORT bool isCarbon()					{ return getType() == Type::Carbon;		}

	DLLEXPORT std::string	toString() const;

private:
	Type			mType;
	std::string	mToJid;
	std::string	mFromJid;
	std::string	mMsgId;
	std::string	mBody;
	std::string mChatState;
	std::string	mFromResource;
	std::string	mMyResource;
	std::string mReceiptMsgId;
};

//=============================================================================


//=============================================================================
//Small container class to handle multiple elements of PresenceUpdate to faciliate XmppEventData
class PresenceUpdate
{
public:
	//These are copied from buzz::PresenceStatus.  DO NOT change them.
	enum Show 
	{
		SHOW_NONE     = 0,
		SHOW_OFFLINE  = 1,
		SHOW_XA       = 2,
		SHOW_AWAY     = 3,
		SHOW_DND      = 4,
		SHOW_ONLINE   = 5,
		SHOW_CHAT     = 6,
	};

	DLLEXPORT PresenceUpdate();
	DLLEXPORT ~PresenceUpdate();

	//Sets -----------------------------------------------------------------------
	DLLEXPORT void setFromJid  ( const std::string& value )					{ mFromJid	 = value;	}
	DLLEXPORT void setStatus   ( const std::string& value )					{ mStatus	 = value;	}
	DLLEXPORT void setNickname ( const std::string& value )					{ mNickname  = value;	}
	DLLEXPORT void setTimeSent ( const std::string& value )					{ mTimeSent  = value;	}
	DLLEXPORT void setPriority ( int				value )					{ mPriority  = value;	}
	DLLEXPORT void setShow	   ( Show				value)					{ mShow		 = value;	}
	DLLEXPORT void setAvailable( bool				value)					{ mAvailable = value;	}

	//Gets -----------------------------------------------------------------------
	DLLEXPORT std::string	getFromJid()	const								{ return mFromJid;	}
	DLLEXPORT std::string	getStatus()		const								{ return mStatus;	}
	DLLEXPORT std::string	getNickname()	const								{ return mNickname;	}
	DLLEXPORT std::string	getTimeSent()	const								{ return mTimeSent;	}
	DLLEXPORT int			getPriority()	const								{ return mPriority;	}
	DLLEXPORT Show			getShow()		const								{ return mShow;		}
	DLLEXPORT bool			isAvailable()	const								{ return mAvailable;}

	//Misc -----------------------------------------------------------------------
	DLLEXPORT PresenceUpdate& operator=( const PresenceUpdate& src );

	DLLEXPORT std::string toString() const;

private:
	std::string	mFromJid;
	std::string	mStatus;
	std::string	mNickname;
	std::string	mTimeSent;
	int			mPriority;
	Show		mShow;
	bool		mAvailable;
};

//=============================================================================


//=============================================================================
//This class simply gathers log info which is then passed to main app for actual logging
//	Expectation is that each LogEntry will trigger callback to main app, so no need for a LogEntryList
//	For our purposes, LOGGER_COMPONENT will always be the same 'SipAgent'.

class LogSendCallback
{
public:
	virtual void sendLog( const LogEntry& logEntry ) = 0;
};

// Macros for the LogEntry class.
#define LOG_DEBUG( ... ) { LogEntry::makeAndSend( LogEntry::s_component.c_str(), LogEntry::Debug, __FUNCTION__, NULL, 0, __VA_ARGS__ );	}		//TODO-LOG
#define LOG_INFO( ... )  { LogEntry::makeAndSend( LogEntry::s_component.c_str(), LogEntry::Info,  __FUNCTION__, NULL, 0, __VA_ARGS__ );	}		//TODO-LOG
#define LOG_WARN( ... )  { LogEntry::makeAndSend( LogEntry::s_component.c_str(), LogEntry::Warn,  __FUNCTION__, NULL, 0, __VA_ARGS__ );	}		//TODO-LOG
#define LOG_ERROR( ... ) { LogEntry::makeAndSend( LogEntry::s_component.c_str(), LogEntry::Error, __FUNCTION__, NULL, 0, __VA_ARGS__ );	}		//TODO-LOG
#define LOG_FATAL( ... ) { LogEntry::makeAndSend( LogEntry::s_component.c_str(), LogEntry::Fatal, __FUNCTION__, __FILE__, __LINE__, __VA_ARGS__ );	}	

class LogEntry
{
public:
	static std::string	    s_component;
	static LogSendCallback* s_logSendCallback;

	enum Level 
	{
		Debug	= 0,
		Info	= 1,
		Warn	= 2,
		Error	= 3,
		Fatal	= 4
	};

	LogEntry();
	LogEntry( const std::string & component, Level level, const std::string & className, const std::string & message, const char* filename, int line, unsigned long threadId );

	static void makeAndSend( const char* component, Level level, const char* className, const char* fileName, int line, const std::string& msg );
	static void makeAndSend( const char* component, Level level, const char* className, const char* fileName, int line, const char* format, ... );

	static void	sendEntry( const LogEntry& entry );
	static void setLogSendCallback( LogSendCallback* value )		{ s_logSendCallback = value;}

	static unsigned long getOsThreadId();	//TODO: should be in util class, maybe

	LogEntry operator=( const LogEntry& src );

	Level		 getLevel()		 const				{ return _level;		}
	std::string  getComponent()	 const				{ return _component;	}
	std::string	 getClassName()	 const				{ return _className;	}
	std::string	 getMessage()	 const				{ return _message;		}
	std::string	 getFileName()	 const				{ return _fileName;		}
	int			 getLineNumber() const				{ return _lineNumber;	}
	unsigned int getThreadId()	 const				{ return _threadId;		}
	std::tm		 getTime()		 const				{ return _time;			}

	bool		 hasFileName()	 const				{ return !_fileName.empty();	}

	void setLevel	  ( Level		 val )			{ _level	  = val;	}
	void setComponent ( std::string	 val )			{ _component  = val;	}
	void setClassName ( std::string  val )			{ _className  = val;	}
	void setMessage   ( std::string  val )			{ _message	  = val;	}
	void setFileName  ( std::string  val )			{ _fileName	  = val;	}
	void setLineNumber( int			 val )			{ _lineNumber = val;	}
	void setThreadId  ( unsigned int val )			{ _threadId	  = val;	}
	void setTime      ( std::tm&	 val )			{ _time		  = val;	}

private:
	Level		 _level;
	std::string	 _component;
	std::string	 _className;
	std::string	 _message;
	std::string	 _fileName;
	int			 _lineNumber;
	unsigned int _threadId;
	std::tm		 _time;
};

//=============================================================================


//=============================================================================

class XmppParameters
{
public:
	//These come from LibJingle (buzz).  DO NOT change them
	enum TlsOption 
	{
		TLS_DISABLED,
		TLS_ENABLED,
		TLS_REQUIRED
	};

	enum AuthMechanism
	{
		AUTH_MECH_NONE		  = 0,
		AUTH_MECH_PLAIN		  = 1,
//		AUTH_MECH_DIGEST_MD5  = 2,
		AUTH_MECH_SCRAM_SHA_1 = 3,
		AUTH_MECH_OAUTH2	  = 4,
	};

	DLLEXPORT XmppParameters();
	DLLEXPORT ~XmppParameters();

	//Sets -----------------------------------------------------------------------
	//User
	void setUserId			  ( const std::string&			value )			{ mUserId				= value;	}
	void setPassword		  ( const std::string&			value )			{ mPassword				= value;	}
	void setStatus			  ( const std::string&			value )			{ mStatus				= value;	}
	void setInitialShow		  ( const PresenceUpdate::Show	value )			{ mInitialShow			= value;	}

	//Connection
	void setDomain			  ( const std::string&			value )			{ mDomain				= value;	}
	void setServer			  ( const std::string&			value )			{ mServer				= value;	}
	void setResourcePrefix	  ( const std::string&			value )			{ mResourcePrefix		= value;	}

	void setPort			  ( int							value )			{ mPort					= value;	}
	void setAllowPlainPassword( bool						value )			{ mAllowPlainPassword	= value;	}
	void setTlsOption		  ( TlsOption					value )			{ mTlsOption			= value;	}
	void setAuthMechanism	  ( AuthMechanism				value )			{ mAuthMechanism		= value;	}

	//Proxy
	void setUseProxy		  ( bool						value )			{ mUseProxy				= value;	}
	void setProxyServer		  ( const std::string&			value )			{ mProxyServer			= value;	}		
	void setProxyPort		  ( int							value )			{ mProxyPort			= value;	}		

	//Gets -----------------------------------------------------------------------
	//User
	std::string			 getUserId			  ()	const				{ return mUserId;				}
	std::string			 getPassword		  ()	const				{ return mPassword;				}
	std::string			 getStatus			  ()	const				{ return mStatus;				}
	PresenceUpdate::Show getInitialShow		  ()	const				{ return mInitialShow;			}

	//Connection
	std::string			 getDomain			  ()	const				{ return mDomain;				}
	std::string			 getServer			  ()	const				{ return mServer;				}
	std::string			 getResourcePrefix	  ()	const				{ return mResourcePrefix;		}

	int					 getPort			  ()	const				{ return mPort;					}
	bool				 getAllowPlainPassword()	const				{ return mAllowPlainPassword;	}
	TlsOption			 getTlsOption		  ()	const				{ return mTlsOption;			}
	AuthMechanism		 getAuthMechanism	  ()	const				{ return mAuthMechanism;		}

	//Proxy
	bool				 getUseProxy		  ()	const				{ return mUseProxy;				}
	std::string			 getProxyServer		  ()	const				{ return mProxyServer;			}		
	int					 getProxyPort		  ()	const				{ return mProxyPort;			}		

	//Misc --------------------------------------------------------------------
	XmppParameters& operator=( const XmppParameters& src );

private:
	//User
	std::string				mUserId;
	std::string				mPassword;
	PresenceUpdate::Show	mInitialShow;
	std::string				mStatus;

	//Connection
	std::string		mDomain;
	std::string		mServer;
	std::string		mResourcePrefix;
	TlsOption		mTlsOption;
	AuthMechanism	mAuthMechanism;
	int				mPort;
	bool			mAllowPlainPassword;

	//Proxy
	bool			mUseProxy;
	std::string		mProxyServer;
	int				mProxyPort;
};

//=============================================================================


//=============================================================================
//Abstract class to be implemented by main app and ptr provided to VoxoxXmpp
class AppEventHandler
{
public:
	AppEventHandler()		{}

	//Connect and Disconnect
	virtual void signOnEvent () = 0;
	virtual void signOffEvent() = 0;

//	virtual void disconnectedEvent( bool connectionError, const std::string& reason ) = 0;	//Network loss

	virtual void presenceUpdateEvent( const std::string& fromJid, int priority, int show, const std::string& status, const std::string& nickname, bool available, const std::string& timeSent ) = 0;

	virtual void incomingMsgEvent( const std::string& toJid, const std::string& fromJid, const std::string& msgId, const std::string& body, 
									const std::string& chatState, const std::string& fromResource, const std::string& myResource,
									const std::string& receiptMsgId, int msgType ) = 0;

	//Logging
	virtual void sendLogEvent( const LogEntry& logEntry ) = 0;

	//Callback
	virtual void setCallbackFunc( XmppEventCallbackFunc value ) = 0;
};
//=============================================================================



//=============================================================================
// XmppEvent class
//	- Event type and related data for XMPP events handled in UI layer.
//	- The data elements we get from the specific events in AppEventHandler
//		will be used to create this and pass to managed code.
//	- TODO: Consider just using this one class in unmanaged code as well.
//=============================================================================

class XmppEventData
{
public:
	enum EventType		//Which type of event occurred.  Allow listeners to select events they care about.
	{
		Unknown			= 0,	//So we can test that all events set type properly
		SignOn			= 1,
		SignOff			= 2,
		Disconnect		= 3,	//May not use.
		PresenceChange  = 4,
		IncomingMsg		= 5,
		Log				= 6,
	};


	XmppEventData();								//This is private so user must call static Make methods to ensure data is complete.
	XmppEventData( EventType eventType);			//This is private so user must call static Make methods to ensure data is complete.

	void initVars();

public:
	DLLEXPORT static XmppEventData*	makeSignOnEvent();
	DLLEXPORT static XmppEventData*	makeSignOffEvent();
//	DLLEXPORT static XmppEventData*	makeDisconnectEvent    ( bool connectError, const std::string& disconnectMsg );
	DLLEXPORT static XmppEventData*	makePresenceChangeEvent( const std::string& fromJid, int priority, int show, const std::string& status, 
															 const std::string& nickname, bool available, const std::string& timeSent );
	DLLEXPORT static XmppEventData*	makeIncomingMsgEvent( const std::string& toJid, const std::string& fromJid, const std::string& msgId, const std::string& body, 
															const std::string& chatState, const std::string& fromResource, const std::string& myResource,
															const std::string& receiptMsgId, int msgType );
	DLLEXPORT static XmppEventData*	makeLogEvent( const LogEntry& logEntry );

	DLLEXPORT ~XmppEventData()        {}

	//Gets ---------------------------------------------------------------------
	DLLEXPORT EventType				getEventType()			const	{ return mEventType;	}

	//SignOn  event has no extras
	//SignOff event has no extras

	//Disconnect Event extras
	//DLLEXPORT	bool				getConnectionError()	const	{ return mConnectionError;	}
	//DLLEXPORT	std::string			getDisconnectMsg()		const	{ return mDisconnectMsg;	}

	//PresenceUpdate
	DLLEXPORT PresenceUpdate		getPresenceUpdate()		const	{ return mPresenceUpdate;	}

	//Incoming Chat Message / Chat State
	DLLEXPORT ChatMsg				getChatMsg()			const	{ return mChatMsg;			}

	//Log Event extras
	DLLEXPORT LogEntry				getLogEntry()			const	{ return mLogEntry;	}

	//Sets ---------------------------------------------------------------------
	//Do NOT export these
	 void setEventType	  ( EventType						 value )	{ mEventType		= value;	}

	//Connect event has no extras

	//Disconnect Event extras
	//void setConnectionError( bool							 value )	{ mConnectionError	= value;	}
	//void setDisconnectMsg  ( const std::string&			 value )	{ mDisconnectMsg	= value;	}

	//PresenceUpdate
	void setPresenceUpdate	( const PresenceUpdate&	value )				{ mPresenceUpdate = value;	}

	//Incoming Chat Message / Chat State
	void setChatMsg			( const ChatMsg&				value )		{ mChatMsg		  = value;	}

	//Log Event extras
	void setLogEntry		( const LogEntry&				value )		{ mLogEntry		  = value;	}

    //Convenience methods -----------------------------------------------------------------------------------------
	DLLEXPORT bool	isSignOnEvent()				const		{ return mEventType == EventType::SignOn;			}
	DLLEXPORT bool	isSignOffEvent()			const		{ return mEventType == EventType::SignOff;			}
	DLLEXPORT bool	isDisconnectEvent()			const		{ return mEventType == EventType::Disconnect;		}
	DLLEXPORT bool	isPresenceChangeEvent()		const		{ return mEventType == EventType::PresenceChange;	}
	DLLEXPORT bool	isIncomingMsgEvent()		const		{ return mEventType == EventType::IncomingMsg;		}
	DLLEXPORT bool	isLogEvent()				const		{ return mEventType == EventType::Log;				}

private:
	EventType		mEventType;

	//Disconnect Event extras
//	bool			mConnectionError;
//	std::string		mDisconnectMsg;

	//PresenceUpdate
	PresenceUpdate	mPresenceUpdate;

	//Incoming Chat Message / ChatState
	ChatMsg			mChatMsg;
		
	//Log Event extras
	LogEntry		mLogEntry;

};

//=============================================================================

