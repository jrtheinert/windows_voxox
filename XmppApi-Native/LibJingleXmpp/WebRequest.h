//-----------------------------------------------------------------------------------------
//A simple class to make a simple WebRequest to be used for connectivity test.
//
//Based on work from StackOverflow: 
//	http://stackoverflow.com/questions/1011339/how-do-you-make-a-http-request-with-c
//-----------------------------------------------------------------------------------------
#pragma once

#include <string>

class WebRequest
{
public:
	WebRequest();
	~WebRequest();

	bool makeRequest( const std::string& url, bool connectOnly );

private:
	void init();
	void shutdown();
	void logIt( const std::string& msg );


private:
	bool mInitialized;
};