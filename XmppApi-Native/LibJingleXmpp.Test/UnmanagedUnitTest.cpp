
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Oct 2014
 */

#include "stdafx.h"
#include "CppUnitTest.h"

#include "../LibJingleXmpp/VoxoxXmppApi.h"

#include "Windows.h"	//For Sleep()

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

#define USE_NEW_SERVER 1

namespace LibJingleXmppTest
{		
	void wait( int seconds )
	{
		if ( seconds > 0)
		{
			for ( int x = 0; x < seconds; x++ )
			{
				Sleep( 1000 );
			}
		}
		else
		{
			while ( true )		//Wait indefinitely
			{
				Sleep( 1000 );
			}
		}
	}

	void myCallback( const XmppEventData& eventData )
	{
		if		( eventData.isSignOnEvent() )
		{
			OutputDebugStringA( ">>>> SignOn event.\n");
		}
		else if ( eventData.isSignOffEvent() )
		{
			OutputDebugStringA( ">>>> SignOff event.\n");
		}
		else if ( eventData.isPresenceChangeEvent() )
		{
			std::string data = eventData.getPresenceUpdate().toString();
			std::string msg = ">>>> Presence update event: " + data + "\n";
			OutputDebugStringA( msg.c_str() );
		}
		else if ( eventData.isIncomingMsgEvent() )
		{
			std::string data = eventData.getChatMsg().toString();
			std::string msg = ">>>> Incoming Chat event: " + data + "\n";
			OutputDebugStringA(  msg.c_str() );
		}
		else if ( eventData.isLogEvent() )
		{
			std::string data = eventData.getLogEntry().getMessage();
			std::string msg = ">>>> Log event: " + data + "\n";
			OutputDebugStringA(  msg.c_str() );
		}
		else
		{
			OutputDebugStringA( ">>>> UNKNOWN XMPP EVENT.\n");
		}
	}



	TEST_CLASS(UnmanagedXmppApiUnitTest)
	{
#ifdef USE_NEW_SERVER
		//User
		std::string mUserId		= "7994";		//SSO companyUserId
		std::string	mPassword	= "jrtjrt";		//User login password

		//Connection
		std::string mServer		= "vx-im.voxox.com";

		//Test contact
		std::string	mToJid		= "7995@voxox.com";
#else
		//User
		std::string	mUserId		= "jrtheinert";	//Old Voxox account
		std::string	mPassword	= "jrtjrt1";	//User login password

		//Connection
		std::string	mServer		= "voxox.com";

		//Test contact
		std::string	mToJid		= "jrttest7b@voxox.com";
#endif

		//Connection
		std::string						mDomain				= "voxox.com";
		std::string						mResourcePrefix		= "voxox3.0";
		int								mPort				= 5222;
		XmppParameters::TlsOption		mTlsOption			= XmppParameters::TLS_DISABLED;
//		XmppParameters::AuthMechanism	mAuthMechanism		= XmppParameters::AUTH_MECH_PLAIN;	//TODO: No longer supported on most Voxox Jabber servers.  Change to DIGEST-MD5 once that is working.
		XmppParameters::AuthMechanism	mAuthMechanism		= XmppParameters::AUTH_MECH_SCRAM_SHA_1;	//TODO: No longer supported on most Voxox Jabber servers.  Change to DIGEST-MD5 once that is working.
		bool							mAllowPlainPassword	= true;

		TEST_METHOD( TestLoginAndDisconnect )
		{
			XmppParameters params;

			//User
			params.setUserId	 ( mUserId   );
			params.setPassword	 ( mPassword );
			params.setStatus	 ( "HI!"     );
			params.setInitialShow( PresenceUpdate::SHOW_ONLINE );

			//Connection
			params.setDomain			( mDomain				);
			params.setServer			( mServer				);
			params.setResourcePrefix	( mResourcePrefix		);
			params.setPort				( mPort					);
			params.setTlsOption			( mTlsOption			);
			params.setAuthMechanism     ( mAuthMechanism		);
			params.setAllowPlainPassword( mAllowPlainPassword	);
			
			VoxoxXmppApi* xmppApi = new VoxoxXmppApi();

			xmppApi->setCallbackFunc( myCallback );

			xmppApi->login( params );

			wait( 3 );
	
//			xmppApi->updateMyStatus( PresenceUpdate::SHOW_AWAY, "BRB" );
//
//			wait( 3 );
//
//			xmppApi->updateMyStatus( PresenceUpdate::SHOW_CHAT, "I am back" );
//
//			wait( 3 );
//
////			xmppApi->requestVCard( toJid );			//TODO: Does not work yet.
////
////			wait( 3 );
//
//			xmppApi->sendChatState( mToJid, true );
//
//			wait( 3 );
//
//			xmppApi->sendChatState( mToJid, false );
//
//			wait( 3 );
//
//			xmppApi->sendChatState( mToJid, true );
//
//			wait( 3 );
//
//			xmppApi->sendReadReceipt( mToJid, "xxxxx" );
//
//			xmppApi->sendChat( mToJid, "Test msg 1", "" );
//
//			wait( 3 );
//
//			xmppApi->sendChat( mToJid, "Test msg 2", "" );
//
//			wait( 3 );

			wait( 5 );
			xmppApi->logoff();

			wait( 5 );
			xmppApi->login( params );

			wait( -1 );	//Forever
//			wait( 5 );
			int xxx = 1;

			delete xmppApi;

		}

		TEST_METHOD( TestWebRequest )
		{
//			bool result = VoxoxXmppApi::testWebRequest( "vx-im.voxox.com", true );	//true = connectOnly
//			bool result = VoxoxXmppApi::testWinHttpClient( "http://vx-im.voxox.com", "8.38.43.18:8888" );
//			bool result = VoxoxXmppApi::testWinHttpClient( "http://vx-im.voxox.com", "", true );	//true == connectOnly
			bool result = VoxoxXmppApi::testWinHttpClient( "http://vx-im.voxox.com", "4-freeproxyserver.com:80", true );	//true == connectOnly

		}

//		TEST_METHOD( TestProxy )
//		{
//			std::string server = "http://vx-im.voxox.com";
////			std::string server = "vx-im.voxox.com";
//			VoxoxXmppApi* xmppApi = new VoxoxXmppApi();
//
////			xmppApi->testProxy          ( "MSIE", server );
//			xmppApi->testAutoDetectProxy( "MSIE", server );
//
//			xmppApi->testAutoDetectProxy( "MSIE", "vx-im.voxox.com" );
//			xmppApi->testAutoDetectProxy( "MSIE", "http://vx-im.voxox.com" );
//			xmppApi->testAutoDetectProxy( "MSIE", "https://vx-im.voxox.com" );
//			xmppApi->testAutoDetectProxy( "MSIE", "ftp://vx-im.voxox.com" );
//			xmppApi->testAutoDetectProxy( "MSIE", "8.38.42.135" );
//			xmppApi->testAutoDetectProxy( "MSIE", "http://8.38.42.135" );
//
//		}
	};
}