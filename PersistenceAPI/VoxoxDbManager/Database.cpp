/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

#include "Database.h"
#include "DatabaseTables.h"
#include "DatabaseUpgrader.h"
#include "QueryBuilder.h"
#include "DbUtils.h"

#include <cstddef>	//For NULL definition.

char* Database::mConflictValues[] = { "", " OR ROLLBACK ", " OR ABORT ", " OR FAIL", " OR IGNORE ", " OR REPLACE " };

//=============================================================================
//A small class to more easily handle the query parameters and defaults.
//=============================================================================
QueryParameters::QueryParameters()
{
}

QueryParameters::~QueryParameters()
{
}

QueryParameters& QueryParameters::operator=( const QueryParameters& src )
{
	if ( this != &src )
	{
		setTable		( src.getTable()		 );
		setSelection	( src.getSelection()	 );
		setGroupBy		( src.getGroupBy()		 );
		setHaving		( src.getHaving()		 );
		setOrderBy		( src.getOrderBy()		 );
		setLimit		( src.getLimit()		 );
		setProjection	( src.getProjection()	 );
		setSelectionArgs( src.getSelectionArgs() );
		setForceQuery   ( src.forceQuery()		 );
	}

	return *this;
}

size_t QueryParameters::addProjection  ( const std::string& value )
{
	mProjection.push_back( value );
	return mProjection.size();
}

size_t QueryParameters::addSelectionArg( const std::string& value )
{
	mSelectionArgs.push_back( value );
	return mSelectionArgs.size();
}

size_t QueryParameters::addSelectionArg( int value )
{
	return addSelectionArg( std::to_string( value ) );
}

size_t QueryParameters::addSelectionArg( __int64 value )
{
	return addSelectionArg( std::to_string( value ) );
}


size_t QueryParameters::addContentValue( const std::string& key, const std::string& value )
{
	mContentValues.put( key, value );
	return mContentValues.size();
}

size_t QueryParameters::addContentValue( const std::string& key, const char* value )
{
	mContentValues.put( key, value );
	return mContentValues.size();
}

size_t QueryParameters::addContentValue( const std::string& key, bool	value )
{
	mContentValues.put( key, value );
	return mContentValues.size();
}

size_t QueryParameters::addContentValue( const std::string& key, long	value )
{
	mContentValues.put( key, value );
	return mContentValues.size();
}

size_t QueryParameters::addContentValue( const std::string& key, __int64 value )
{
	mContentValues.put( key, value );
	return mContentValues.size();
}

size_t QueryParameters::addContentValue( const std::string& key, float	value )
{
	mContentValues.put( key, value );
	return mContentValues.size();
}

size_t QueryParameters::addContentValue( const std::string& key, double value )
{
	mContentValues.put( key, value );
	return mContentValues.size();
}

bool QueryParameters::isValid() const
{
	bool result = true;

	//Must have TABLE.
	if ( mTable.empty() )
		result = false;

	//Must have at least one PROJECTION.  What about "*"?
	if ( mProjection.size() < 1 )
		result = false;

	return true;
}

//=============================================================================


//=============================================================================

//Some convenience methods for UnitTests
void Database::clearMessageTable()
{
	clearTable( DatabaseTables::Message::TABLE );
}

void Database::clearPhoneNumberTable()
{
	clearTable( DatabaseTables::PhoneNumber::TABLE );
}

void Database::clearTable( const std::string& tableName )
{
	std::string sql = "DELETE FROM " + tableName;
	mDb->execDml( sql.c_str() );
}

void Database::setDbName( const char* dbName )
{ 
	mDbName = dbName;	
}

const char* Database::getDbName()
{
	return mDbName.c_str();
}

bool Database::isOpen()
{
	bool result = false;

	result = ( mDb != NULL );

	return result;
}


Database::Database( const char* dbName ) : mDb( NULL ), mInitializingContactsOrUserSignOut( false ), mDbName ( dbName ), mUserVersion( -1 )
{
	createSQLiteDb();
}

Database::~Database()
{
	close();
}

//static
bool Database::checkDatabase( const char* dbName )
{
	return VoxoxSQLiteDb::doesDbExist( dbName );
}

//Check if DB exists.  This could be static method.
bool Database::checkDatabase()
{
	return VoxoxSQLiteDb::doesDbExist( mDbName.c_str() );
}

bool Database::checkTableExists( const std::string& tableName )
{
	return mDb->tableExists( tableName.c_str() );
}

void Database::createSQLiteDb()
{
	if ( mDb == NULL )
	{
		mDb = new VoxoxSQLiteDb();
	}
}

//Create User Database and Tables
void Database::create( const StringList& tableDml, bool doUpgradeCheck )
{
	try
	{
		mUserVersion = DatabaseUpgrader::getCurrentVersion();

		createSQLiteDb();

		//Create Database
  		mDb->openOrCreateWriteable( mDbName.c_str() );

		//Create Tables
		for ( StringList::const_iterator it = tableDml.begin(); it != tableDml.end(); it++ ) 
		{
			mDb->execDml( (*it).c_str()	);
		}

		if ( doUpgradeCheck )		//TODO-HACK: Need to add upgradeCheck for AppSettings.
			checkForUpgrade();
	}

	catch ( VoxoxSQLiteException e )
	{
		//Log error.  Notify user.
		throw e;
	}

	mInitializingContactsOrUserSignOut = false;
}

//Should only be called from dtor, but there may be exceptions to that, like during upgrade.
void Database::close()
{
	mInitializingContactsOrUserSignOut = true;
	mDb->close();
	
	delete mDb;
	mDb = NULL;

//	delete mInstance;		//TODO: likely memory leak, but don't have time address this.  Should not be calling this method unless exiting app anyway.
//	mInstance = NULL;
}

//We need new version and existing version.
void Database::checkForUpgrade()
{
	int oldVersion = mDb->getUserVersion();
	int newVersion = getUserVersion();			//Set when this Database object is created.

	if ( oldVersion < newVersion )
	{
		onUpgrade( mDb, oldVersion, newVersion );
		mDb->setUserVersion( newVersion );
	}
	else if ( oldVersion > newVersion )
	{
		assert( false );		//We cannot downgrade
	}
}

//We need new version and existing version
//	VoxoxSQLiteDb should be writeable
void Database::onUpgrade( VoxoxSQLiteDb* db, int oldVersion, int newVersion )
{
	DatabaseUpgrader dbUpgrader;
	dbUpgrader.upgrade(db, oldVersion, newVersion);
}

VoxoxSQLiteCursor* Database::query( const QueryParameters& params ) 
{
	VoxoxSQLiteCursor* result = NULL;

	if ( !mInitializingContactsOrUserSignOut || params.forceQuery() )
	{
		//TODO: Consider using QueryParameters for QueryBuilder.
		std::string sql = QueryBuilder::buildQueryString( false, params.getTable(), params.getProjection(), params.getSelection(), params.getGroupBy(), params.getHaving(), params.getOrderBy(), params.getLimit() );
		result = mDb->execQuery( sql.c_str(), params.getSelectionArgs() );
	}

	return result;
}

VoxoxSQLiteCursor* Database::rawQuery( const std::string& sql, const StringList& selectionArgs)
{
	VoxoxSQLiteCursor* result = NULL;

	if ( !mInitializingContactsOrUserSignOut )
	{
		result = mDb->execQuery( sql.c_str(), selectionArgs );
	}

	return result;
}

long Database::getLongForQuery( const QueryParameters params ) 
{
	VoxoxSQLiteCursor* cursor = query( params );
	return mDb->getLongFromCursor( cursor, 0 );
}

long Database::getLongForQuery( const std::string& sql, const StringList& selectionArgs ) 
{
	long result = -1;

	if ( !mInitializingContactsOrUserSignOut )
	{
		result = mDb->simpleQueryForLong( sql.c_str(), 0, selectionArgs );
	}

	return result;
}

__int64 Database::getInt64ForQuery( const QueryParameters params ) 
{
	VoxoxSQLiteCursor* cursor = query( params );
	return mDb->getInt64FromCursor( cursor, 0LL );
}

__int64 Database::getInt64ForQuery( const std::string& sql, const StringList& selectionArgs ) 
{
	__int64 result = -1;

	if ( !mInitializingContactsOrUserSignOut )
	{
		result = mDb->simpleQueryForLong( sql.c_str(), 0, selectionArgs );
	}

	return result;
}

std::string Database::getStringForQuery( const QueryParameters params ) 
{
	VoxoxSQLiteCursor* cursor = query( params );
	return mDb->getStringFromCursor( cursor, "" );
}

std::string Database::getStringForQuery( const std::string& sql, const StringList& selectionArgs ) 
{
	std::string result;

	if ( !mInitializingContactsOrUserSignOut )
	{
		result = mDb->simpleQueryForString( sql.c_str(), "", selectionArgs );
	}

	return result;
}

long Database::insert( const std::string& table, const ContentValues& values, bool returnRowId ) 
{
	long result = -1;

	if ( !mInitializingContactsOrUserSignOut )
	{
		result = insertWithOnConflict( table, std::string(), values, ConflictNone );

		if ( result > 0 && returnRowId )
		{
			result = getLastRowId();
		}
	}

	return result;
}

long Database::insertWithConflict( const std::string& table, const ContentValues& values, int conflict, bool returnRowId )
{
	long result = -1;

	if ( !mInitializingContactsOrUserSignOut )
	{
		result = insertWithOnConflict( table, std::string(), values, conflict );

		if ( result > 0 && returnRowId )
		{
			result = getLastRowId();
		}
	}

	return result;
}

int Database::update( const QueryParameters& params )
{
	int result = -1;

	if ( !mInitializingContactsOrUserSignOut )
	{
		result = updateWithOnConflict( params.getTable(), params.getContentValues(), params.getSelection(), params.getSelectionArgs(), ConflictNone );
	}

	return result;
}

int Database::update( const std::string& table, const ContentValues& values, const std::string& selection, const StringList& selectionArgs)
{
	int result = -1;

	if ( !mInitializingContactsOrUserSignOut )
	{
		result = updateWithOnConflict( table, values, selection, selectionArgs, ConflictNone );
	}

	return result;
}

int Database::deleteRecords( const std::string& table, const std::string& whereClause, const StringList& whereArgs)
{
	long result = -1;

	if ( !mInitializingContactsOrUserSignOut )
	{
		result = mDb->deleteRecords( table, whereClause, whereArgs );
	}

	return result;
}

int Database::getLastRowId()
{
	int result = (int) mDb->getLastRowId();

	return result;
}

//============================================================================================



//============================================================================================
//Some private methods modeled on -Android- SQLiteDatabase methods.
//============================================================================================

long Database::insertWithOnConflict( const std::string& table, const std::string& nullColumnHack, const ContentValues& initialValues, int conflictAlgorithm )
{
	long result = 0;

	try {
		std::string sql;
		sql.append( "INSERT" );
        sql.append( mConflictValues[conflictAlgorithm]);
		sql.append( " INTO " );
		sql.append( table );
		sql.append( " ( " );

		StringList bindArgs;
		size_t	   size = ( initialValues.size() > 0) ? initialValues.size() : 0;

		if (size > 0) 
		{
			size_t i = 0;

			KeyValueMap& rMap = const_cast<ContentValues&>(initialValues).getMap();

			for ( KeyValueMap::iterator it = rMap.begin(); it != rMap.end(); it++ ) 
			{
				//Append column name
				sql.append((i > 0) ? ", " : "");
				sql.append( (*it).first );

				//Save value in bindArgs
				std::string temp = (*it).second.getValueAsString();
				bindArgs.push_back( temp );
				i++;
			}

			sql.append(" ) ");

			//Create VALUES clause like this:  "VALUES ( ?, ?, ? )"
			sql.append(" VALUES ( ");

			for (i = 0; i < size; i++) 
			{
				sql.append((i > 0) ? ", ?" : "?");
			}
		} else 
		{
			sql.append(nullColumnHack + " ) VALUES ( NULL");
		}

		sql.append( " ) ");

        try 
		{
			result = mDb->execDml( sql.c_str(), bindArgs );
        } 

		catch ( VoxoxSQLiteException e )
		{
			int xxx = 1;		//TODO
		}
	} 
	
	catch ( VoxoxSQLiteException e )
	{
		int xxx = 1;			//TODO
	}

	return result;
}

int Database::updateWithOnConflict( const std::string& table, const ContentValues& values, const std::string& whereClause, const StringList& whereArgs, int conflictAlgorithm) 
{
	int result = 0;

    if ( values.size() == 0) 
	{
		int xxx = 1;
//        throw new IllegalArgumentException("Empty values");		//TODO
    }

//    acquireReference();

    try 
	{
        std::string sql;

        sql.append( "UPDATE " );
        sql.append( mConflictValues[conflictAlgorithm]);
        sql.append( table );
        sql.append( " SET " );

        // move all bind args to one array
        size_t		 setValuesSize = values.size();
        size_t		 bindArgsSize  = (whereArgs.size() == 0) ? setValuesSize : (setValuesSize + whereArgs.size());
        size_t		 i			   = 0;
		KeyValueMap& rMap		   = const_cast<ContentValues&>(values).getMap();
		StringList	 bindArgs;

        for ( KeyValueMap::iterator it = rMap.begin(); it != rMap.end(); it++ ) 
		{
            sql.append((i > 0) ? ", " : "");
            sql.append( (*it).first );

			std::string temp = (*it).second.getValueAsString();
			bindArgs.push_back( temp );
            sql.append( " = ?" );
			i++;
        }

        if ( whereArgs.size() > 0 ) 
		{
            for (i = setValuesSize; i < bindArgsSize; i++)
			{
				std::string temp = DbUtils::getNthListEntry( whereArgs, i - setValuesSize );
				bindArgs.push_back( temp );
            }
        }

        if ( !whereClause.empty() ) 
		{
            sql.append(" WHERE ");
            sql.append(whereClause);
        }

        try 
		{
			result = mDb->execDml( sql.c_str(), bindArgs );
        } 

		catch ( VoxoxSQLiteException e )
		{
			int xxx = 1;		//TODO
		}
    } 
	
	catch ( VoxoxSQLiteException e )
	{
		int xxx = 1;		//TODO
	}

	return result;
}
