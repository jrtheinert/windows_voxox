/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

#include "DatabaseTables.h"

#include <string>

//=============================================================================
//SQL snippets useable in all tables
//	This should likely be private/protected.
//=============================================================================

const std::string DatabaseTables::CREATE_TABLE				= "CREATE TABLE IF NOT EXISTS ";
const std::string DatabaseTables::DELETE_TABLE				= "DROP TABLE IF EXISTS ";

const std::string DatabaseTables::INTEGER					= " INTEGER, ";
const std::string DatabaseTables::INTEGER_DEFAULT_INVALID	= " INTEGER DEFAULT -1, ";
const std::string DatabaseTables::INTEGER_DEFAULT_1			= " INTEGER DEFAULT 1, ";
const std::string DatabaseTables::INTEGER_DEFAULT_0			= " INTEGER DEFAULT 0, ";

const std::string DatabaseTables::REAL						= " REAL, ";
const std::string DatabaseTables::REAL_DEFAULT_0			= " REAL DEFAULT 0, ";

const std::string DatabaseTables::TEXT						= " TEXT NULL, ";
const std::string DatabaseTables::PRIMARY_KEY				= " INTEGER PRIMARY KEY AUTOINCREMENT, ";

const std::string DatabaseTables::OPEN_PARENS 				= " ( ";
const std::string DatabaseTables::CLOSE_PARENS				= " ) ";

//=============================================================================
//Contact table - Not used now, but may be with Desktop Address Books.
//=============================================================================

//const std::string DatabaseTables::Contacts::TABLE = "contact";
//const std::string DatabaseTables::Contacts::NAME  = "name";
//const std::string DatabaseTables::Contacts::ID    = "id";
//
//const std::string DatabaseTables::Contacts::CREATE_TABLE =
//	DatabaseTables::CREATE_TABLE
//	+ TABLE
//	+ OPEN_PARENS
//	;
//
//=============================================================================


//=============================================================================
//PhoneNumber table
//=============================================================================
const std::string DatabaseTables::PhoneNumber::TABLE 				= "phone_number";

const std::string DatabaseTables::PhoneNumber::ID 					= "_id";
const std::string DatabaseTables::PhoneNumber::SOURCE	 			= "source";
const std::string DatabaseTables::PhoneNumber::CM_GROUP				= "cm_group";		//Used to properly group messages in ContactsMessagesActivity.
const std::string DatabaseTables::PhoneNumber::GROUP_ID 			= "group_id";
const std::string DatabaseTables::PhoneNumber::NAME					= "name";
const std::string DatabaseTables::PhoneNumber::NUMBER 				= "number";			// number used to optimize database queries
const std::string DatabaseTables::PhoneNumber::DISPLAY_NUMBER 		= "display_number"; // display_number (as appears in the native database)
const std::string DatabaseTables::PhoneNumber::CM_GROUP_NAME 		= "cm_group_name";	// compound name for numbers that exist on multiple contacts.
const std::string DatabaseTables::PhoneNumber::LABEL 				= "label";
const std::string DatabaseTables::PhoneNumber::IS_VOXOX 			= "is_voxox";		//NOT a boolean: 0 - Normal, 1 - Voxox, 2 - Mobile Registration number.
const std::string DatabaseTables::PhoneNumber::IS_FAVORITE 			= "is_favorite";

//New Contact Syncing
const std::string DatabaseTables::PhoneNumber::COMPANY	 			= "company";
const std::string DatabaseTables::PhoneNumber::CONTACT_KEY 			= "contact_key";
const std::string DatabaseTables::PhoneNumber::FIRST_NAME			= "first_name";
const std::string DatabaseTables::PhoneNumber::LAST_NAME 			= "last_name";
const std::string DatabaseTables::PhoneNumber::USER_ID	 			= "user_id";
const std::string DatabaseTables::PhoneNumber::USER_NAME 			= "user_name";		//Appears to be contacts Voxox login name.
const std::string DatabaseTables::PhoneNumber::IS_BLOCKED 			= "is_blocked";
const std::string DatabaseTables::PhoneNumber::IS_EXT 				= "is_ext";			//Denotes phoneNumber an Extension

//For Voxox numbers
const std::string DatabaseTables::PhoneNumber::XMPP_JID 			= "xmpp_jid";
const std::string DatabaseTables::PhoneNumber::XMPP_GROUP 			= "xmpp_group";
const std::string DatabaseTables::PhoneNumber::XMPP_STATUS 	 		= "xmpp_status";
const std::string DatabaseTables::PhoneNumber::XMPP_PRESENCE 		= "xmpp_presence";
const std::string DatabaseTables::PhoneNumber::XMPP_SUBSCRIPTION	= "xmpp_subscription";

//DML
const std::string DatabaseTables::PhoneNumber::DELETE_TABLE = DatabaseTables::DELETE_TABLE + DatabaseTables::PhoneNumber::TABLE;

const std::string DatabaseTables::PhoneNumber::CREATE_TABLE =
	  DatabaseTables::CREATE_TABLE 
	+ DatabaseTables::PhoneNumber::TABLE
	+ DatabaseTables::OPEN_PARENS

	+ DatabaseTables::PhoneNumber::ID				+ PRIMARY_KEY
	+ DatabaseTables::PhoneNumber::CONTACT_KEY		+ TEXT
	+ DatabaseTables::PhoneNumber::SOURCE			+ TEXT
	+ DatabaseTables::PhoneNumber::CM_GROUP			+ INTEGER_DEFAULT_INVALID		//For message grouping (not the same as Group Messaging)
	+ DatabaseTables::PhoneNumber::GROUP_ID			+ TEXT							//For use with Group Messaging.
	+ DatabaseTables::PhoneNumber::NAME				+ TEXT
	+ DatabaseTables::PhoneNumber::FIRST_NAME		+ TEXT
	+ DatabaseTables::PhoneNumber::LAST_NAME		+ TEXT
	+ DatabaseTables::PhoneNumber::NUMBER			+ TEXT
	+ DatabaseTables::PhoneNumber::DISPLAY_NUMBER	+ TEXT
	+ DatabaseTables::PhoneNumber::LABEL			+ TEXT
	+ DatabaseTables::PhoneNumber::CM_GROUP_NAME	+ TEXT
	+ DatabaseTables::PhoneNumber::IS_VOXOX			+ INTEGER_DEFAULT_0
	+ DatabaseTables::PhoneNumber::IS_EXT			+ INTEGER_DEFAULT_0
	+ DatabaseTables::PhoneNumber::IS_FAVORITE		+ INTEGER_DEFAULT_0
	+ DatabaseTables::PhoneNumber::IS_BLOCKED		+ INTEGER_DEFAULT_0
	+ DatabaseTables::PhoneNumber::COMPANY			+ TEXT

	//For Voxox numbers only
	+ DatabaseTables::PhoneNumber::USER_ID			+ INTEGER_DEFAULT_0
	+ DatabaseTables::PhoneNumber::USER_NAME		+ TEXT
	+ DatabaseTables::PhoneNumber::XMPP_JID			+ TEXT
	+ DatabaseTables::PhoneNumber::XMPP_GROUP		+ TEXT
	+ DatabaseTables::PhoneNumber::XMPP_STATUS		+ INTEGER_DEFAULT_INVALID
//	+ DatabaseTables::PhoneNumber::XMPP_PRESENCE	+ " TEXT DEFAULT " + //XmppConstants.Status.XMPP_PRESENCE_UNKNOWN + ", "	//TODO
	+ DatabaseTables::PhoneNumber::XMPP_PRESENCE	+ TEXT
	+ DatabaseTables::PhoneNumber::XMPP_SUBSCRIPTION + TEXT
	
	+ " UNIQUE (" + DatabaseTables::PhoneNumber::CONTACT_KEY + "," +  DatabaseTables::PhoneNumber::NUMBER + ") ON CONFLICT REPLACE "

	+ DatabaseTables::CLOSE_PARENS
	;

//=============================================================================


//=============================================================================
//Message table
//=============================================================================
const std::string DatabaseTables::Message::TABLE				= "message";

//Base message
const std::string DatabaseTables::Message::ID 					= "_id";
const std::string DatabaseTables::Message::MESSAGE_ID 			= "message_id";
const std::string DatabaseTables::Message::THREAD_ID 			= "thread_id";
const std::string DatabaseTables::Message::DIRECTION 			= "direction";
const std::string DatabaseTables::Message::TYPE 				= "type";
const std::string DatabaseTables::Message::STATUS 				= "status";
const std::string DatabaseTables::Message::BODY 				= "body";

//Rich message
const std::string DatabaseTables::Message::FILE_LOCAL 			= "file_local";
const std::string DatabaseTables::Message::THUMBNAIL_LOCAL 		= "thumbnail_local";
const std::string DatabaseTables::Message::DURATION 			= "duration";

//Translation
const std::string DatabaseTables::Message::TRANSLATED 			= "translated";
const std::string DatabaseTables::Message::INBOUND_LANG 		= "inbound_lang";
const std::string DatabaseTables::Message::OUTBOUND_LANG 		= "outbound_lang";

//Time stamps
const std::string DatabaseTables::Message::LOCAL_TIMESTAMP 		= "local_timestamp";
const std::string DatabaseTables::Message::SERVER_TIMESTAMP 	= "server_timestamp";
const std::string DatabaseTables::Message::READ_TIMESTAMP		= "read_timestamp";			//Ideally, this would be named 'read_timestamp', but not worth DB upgrade effort.
const std::string DatabaseTables::Message::DELIVERED_TIMESTAMP 	= "delivered_timestamp";	//Ideally, this would be named 'read_timestamp', but not worth DB upgrade effort.

//Not sure if these are used.
const std::string DatabaseTables::Message::IACCOUNT 			= "iaccount";
const std::string DatabaseTables::Message::CNAME 				= "cname";
const std::string DatabaseTables::Message::MODIFIED 			= "modified";

//SMS and Chat - but may not be totally correct.
const std::string DatabaseTables::Message::CONTACT_DID 			= "contact_did";

//SMS Message
const std::string DatabaseTables::Message::FROM_DID 			= "from_did";
const std::string DatabaseTables::Message::TO_DID 				= "to_did";

//Chat Message
const std::string DatabaseTables::Message::FROM_JID 			= "from_jid";
const std::string DatabaseTables::Message::TO_JID 				= "to_jid";

//Group Messaging
const std::string DatabaseTables::Message::TO_GROUP_ID			= "to_group_id";
const std::string DatabaseTables::Message::FROM_USER_ID			= "from_user_id";

//Aggregate values - NOT FIELDS IN TABLE, BUT USED IN QUERIES.
const std::string DatabaseTables::Message::MSG_COUNT 			= "msg_count";

//DML
const std::string DatabaseTables::Message::DELETE_TABLE = DatabaseTables::DELETE_TABLE + DatabaseTables::Message::TABLE;

const std::string DatabaseTables::Message::CREATE_TABLE =
	  DatabaseTables::CREATE_TABLE 
	+ DatabaseTables::Message::TABLE
	+ DatabaseTables::OPEN_PARENS

	+ DatabaseTables::Message::ID					+ PRIMARY_KEY
	+ DatabaseTables::Message::THREAD_ID			+ INTEGER
	+ DatabaseTables::Message::MESSAGE_ID			+ TEXT
	+ DatabaseTables::Message::DIRECTION			+ INTEGER_DEFAULT_1
	+ DatabaseTables::Message::TYPE					+ INTEGER
	+ DatabaseTables::Message::STATUS				+ INTEGER

	+ DatabaseTables::Message::CONTACT_DID			+ TEXT

	//SMS
	+ DatabaseTables::Message::FROM_DID				+ TEXT
	+ DatabaseTables::Message::TO_DID				+ TEXT

	//Chat
	+ DatabaseTables::Message::FROM_JID				+ TEXT
	+ DatabaseTables::Message::TO_JID				+ TEXT

	//Group Messaging
	+ DatabaseTables::Message::TO_GROUP_ID			+ TEXT
	+ DatabaseTables::Message::FROM_USER_ID			+ INTEGER_DEFAULT_INVALID

	+ DatabaseTables::Message::BODY					+ TEXT

	//Timestamps
	+ DatabaseTables::Message::LOCAL_TIMESTAMP		+ INTEGER
	+ DatabaseTables::Message::SERVER_TIMESTAMP		+ INTEGER
	+ DatabaseTables::Message::READ_TIMESTAMP		+ INTEGER
	+ DatabaseTables::Message::DELIVERED_TIMESTAMP	+ INTEGER

	//Voicemail?
	+ DatabaseTables::Message::MODIFIED				+ TEXT
	+ DatabaseTables::Message::IACCOUNT				+ TEXT
	+ DatabaseTables::Message::CNAME				+ TEXT

	//Translation
	+ DatabaseTables::Message::TRANSLATED			+ TEXT
	+ DatabaseTables::Message::INBOUND_LANG			+ TEXT
	+ DatabaseTables::Message::OUTBOUND_LANG		+ TEXT

	//RichData
	+ DatabaseTables::Message::FILE_LOCAL			+ TEXT
	+ DatabaseTables::Message::THUMBNAIL_LOCAL		+ TEXT
	+ DatabaseTables::Message::DURATION				+ INTEGER

	//TODO-DB: We need to revisit this.  Server defines message uniqueness as 'messageId' + 'type'
	//	We can also do that because we now have 'messageId' on all messages, but we have to ensure we have 'messageId'
	//	when message is first added to table.  I am certain this is not the case for Chats, but we can adjust code to fix that.
//	+ " UNIQUE (" + DatabaseTables::Message::FROM_DID + "," + DatabaseTables::Message::CONTACT_DID + "," + DatabaseTables::Message::DIRECTION + "," + DatabaseTables::Message::SERVER_TIMESTAMP + ") ON CONFLICT IGNORE "
	+ " UNIQUE (" + DatabaseTables::Message::MESSAGE_ID + "," + DatabaseTables::Message::TYPE + ") ON CONFLICT IGNORE "

	+ DatabaseTables::CLOSE_PARENS
	;

//=============================================================================


//=============================================================================
//Group Message Group table
//=============================================================================
const std::string  DatabaseTables::GroupMessageGroup::TABLE				= "group_message_group";

const std::string  DatabaseTables::GroupMessageGroup::ID				= "_id";
const std::string  DatabaseTables::GroupMessageGroup::GROUP_ID			= "group_id";
const std::string  DatabaseTables::GroupMessageGroup::NAME				= "name";
const std::string  DatabaseTables::GroupMessageGroup::STATUS			= "status";
const std::string  DatabaseTables::GroupMessageGroup::LAST_COLOR		= "last_color";			//Last color name assigned.  Used to determine NEXT color to assign a group member

//These are crossed out in API doc.  May be Phase 2, so leave in DB for now.  Check with Curtis
const std::string  DatabaseTables::GroupMessageGroup::INVITE_TYPE		= "invite_type"; 		// type of invite to send (0=none, 1=welcome message, 2=opt-in)
const std::string  DatabaseTables::GroupMessageGroup::ANNOUNCE_MEMBERS	= "announce_members"; 	// send new member announcements to the group
const std::string  DatabaseTables::GroupMessageGroup::CONFIRMATION_CODE	= "confirmation_code";	// code required for opting in "Yes"

//TODO-GM: we not typically store avatar in DB.  How will we be handling this?  Phase 2, per Curtis.
const std::string  DatabaseTables::GroupMessageGroup::AVATAR			= "avatar";

//Aggregate values - NOT FIELDS IN TABLE, BUT USED IN QUERIES.
const std::string  DatabaseTables::GroupMessageGroup::MEMBER_COUNT		= "member_count";
const std::string  DatabaseTables::GroupMessageGroup::ADMIN_USER_ID		= "admin_userid";

//SELECT AS field names to disambiguate PN and GMG 'name'
const std::string  DatabaseTables::GroupMessageGroup::GROUP_NAME		= "group_name";

const std::string DatabaseTables::GroupMessageGroup::DELETE_TABLE = DatabaseTables::DELETE_TABLE + DatabaseTables::GroupMessageGroup::TABLE;

const std::string DatabaseTables::GroupMessageGroup::CREATE_TABLE =
	  DatabaseTables::CREATE_TABLE 
	+ DatabaseTables::GroupMessageGroup::TABLE
	+ DatabaseTables::OPEN_PARENS

    + DatabaseTables::GroupMessageGroup::ID 				+ PRIMARY_KEY
    + DatabaseTables::GroupMessageGroup::GROUP_ID 			+ TEXT					//Server
    + DatabaseTables::GroupMessageGroup::NAME 				+ TEXT
    + DatabaseTables::GroupMessageGroup::STATUS			+ INTEGER_DEFAULT_1
    + DatabaseTables::GroupMessageGroup::LAST_COLOR		+ TEXT					//Last color assigned.  Used to assign color to next Member.

    + DatabaseTables::GroupMessageGroup::INVITE_TYPE 		+ INTEGER_DEFAULT_1
    + DatabaseTables::GroupMessageGroup::ANNOUNCE_MEMBERS 	+ INTEGER_DEFAULT_1
    + DatabaseTables::GroupMessageGroup::CONFIRMATION_CODE + TEXT
    + DatabaseTables::GroupMessageGroup::AVATAR 			+ TEXT
    + " UNIQUE (" + DatabaseTables::GroupMessageGroup::GROUP_ID + ") ON CONFLICT REPLACE "

	+ DatabaseTables::CLOSE_PARENS
	;

//=============================================================================


//=============================================================================
//Group Message Member table
//=============================================================================
const std::string DatabaseTables::GroupMessageMember::TABLE			= "group_message_member";

const std::string DatabaseTables::GroupMessageMember::ID			= "_id";
const std::string DatabaseTables::GroupMessageMember::GROUP_ID		= "group_id";
const std::string DatabaseTables::GroupMessageMember::USER_ID  		= "user_id";
const std::string DatabaseTables::GroupMessageMember::COLOR			= "color";				//Color name used for chat bubbles

const std::string DatabaseTables::GroupMessageMember::MEMBER_ID 	= "member_id";
const std::string DatabaseTables::GroupMessageMember::REFERRER_ID 	= "referred_id";
const std::string DatabaseTables::GroupMessageMember::TYPE_ID 		= "type_id";
const std::string DatabaseTables::GroupMessageMember::NICKNAME 		= "nickname";
const std::string DatabaseTables::GroupMessageMember::IS_ADMIN 		= "is_admin";
const std::string DatabaseTables::GroupMessageMember::IS_INVITED	= "is_invited";
const std::string DatabaseTables::GroupMessageMember::HAS_CONFIRMED	= "has_confirmed";
const std::string DatabaseTables::GroupMessageMember::OPT_OUT		= "opt_out";
const std::string DatabaseTables::GroupMessageMember::NOTIFY		= "notify";		//Per discussion in API doc, this will be used in later phases.  Put in DB now, to avoid schema changes.

//Aggregate values - NOT FIELDS IN TABLE, BUT USED IN QUERIES.
const std::string DatabaseTables::GroupMessageMember::DISPLAY_NAME  = "displayName";

const std::string DatabaseTables::GroupMessageMember::DELETE_TABLE = DatabaseTables::DELETE_TABLE + DatabaseTables::GroupMessageMember::TABLE;

const std::string DatabaseTables::GroupMessageMember::CREATE_TABLE = 
	  DatabaseTables::CREATE_TABLE 
	+ DatabaseTables::GroupMessageMember::TABLE
	+ DatabaseTables::OPEN_PARENS

    + DatabaseTables::GroupMessageMember::GROUP_ID 		+ TEXT
    + DatabaseTables::GroupMessageMember::USER_ID 		+ INTEGER
    + DatabaseTables::GroupMessageMember::COLOR 		+ TEXT					//Default null will trigger assignment on first incoming message.


    + DatabaseTables::GroupMessageMember::MEMBER_ID   	+ INTEGER_DEFAULT_0		//From Server
    + DatabaseTables::GroupMessageMember::REFERRER_ID 	+ INTEGER_DEFAULT_0		//From Server
    + DatabaseTables::GroupMessageMember::TYPE_ID 		+ INTEGER_DEFAULT_0		//From Server 1=SMS, 2=IM
    + DatabaseTables::GroupMessageMember::NICKNAME 		+ TEXT					//From Server
    + DatabaseTables::GroupMessageMember::IS_ADMIN 		+ INTEGER_DEFAULT_0		//From Server
    + DatabaseTables::GroupMessageMember::IS_INVITED 	+ INTEGER_DEFAULT_0		//From Server
    + DatabaseTables::GroupMessageMember::HAS_CONFIRMED + INTEGER_DEFAULT_0		//From Server
    + DatabaseTables::GroupMessageMember::OPT_OUT 		+ INTEGER_DEFAULT_0		//From Server

    + DatabaseTables::GroupMessageMember::NOTIFY 		+ INTEGER_DEFAULT_0		//From Server	//Later phase
	
	+ " UNIQUE (" + DatabaseTables::GroupMessageMember::GROUP_ID + "," + DatabaseTables::GroupMessageMember::USER_ID + ") ON CONFLICT REPLACE "

	+ DatabaseTables::CLOSE_PARENS
	;

//=============================================================================


//=============================================================================
//Translation table
//=============================================================================

const std::string DatabaseTables::Translation::TABLE 						= "translation";

const std::string DatabaseTables::Translation::ID 							= "_id";
const std::string DatabaseTables::Translation::ENABLED 						= "enabled";
const std::string DatabaseTables::Translation::USER_LANGUAGE 				= "user_language";
const std::string DatabaseTables::Translation::CONTACT_LANGUAGE 			= "contact_language";
const std::string DatabaseTables::Translation::TRANSLATE_MESSAGE_DIRECTION 	= "translate_message_direction";
const std::string DatabaseTables::Translation::CONTACT_ID 					= "contact_id";
const std::string DatabaseTables::Translation::PHONE_NUMBER 				= "phone_number";
const std::string DatabaseTables::Translation::JID 							= "jid";

const std::string DatabaseTables::Translation::DELETE_TABLE = DatabaseTables::DELETE_TABLE + DatabaseTables::Translation::TABLE;

const std::string DatabaseTables::Translation::CREATE_TABLE =
	  DatabaseTables::CREATE_TABLE 
	+ DatabaseTables::Translation::TABLE
	+ DatabaseTables::OPEN_PARENS

	+ DatabaseTables::Translation::ID							+ PRIMARY_KEY
	+ DatabaseTables::Translation::ENABLED						+ INTEGER_DEFAULT_0
	+ DatabaseTables::Translation::CONTACT_ID					+ INTEGER_DEFAULT_0
	+ DatabaseTables::Translation::PHONE_NUMBER					+ TEXT
	+ DatabaseTables::Translation::JID							+ TEXT
	+ DatabaseTables::Translation::USER_LANGUAGE				+ TEXT
	+ DatabaseTables::Translation::CONTACT_LANGUAGE				+ TEXT
	+ DatabaseTables::Translation::TRANSLATE_MESSAGE_DIRECTION	+ INTEGER

	+ " UNIQUE (" + DatabaseTables::Translation::CONTACT_ID + "," +  DatabaseTables::Translation::PHONE_NUMBER + "," +  DatabaseTables::Translation::JID + ") ON CONFLICT REPLACE "

	+ DatabaseTables::CLOSE_PARENS
	;

//=============================================================================


//=============================================================================
//Recent Call table
//=============================================================================
const std::string DatabaseTables::RecentCall::TABLE 			= "recent_call";

const std::string DatabaseTables::RecentCall::ID 				= "_id";
const std::string DatabaseTables::RecentCall::UID 				= "uid";
const std::string DatabaseTables::RecentCall::DURATION 			= "duration";
const std::string DatabaseTables::RecentCall::TIMESTAMP 		= "timestamp";
const std::string DatabaseTables::RecentCall::START_TIMESTAMP 	= "start_timestamp";
const std::string DatabaseTables::RecentCall::INCOMING 			= "incoming";
const std::string DatabaseTables::RecentCall::STATUS			= "status";
const std::string DatabaseTables::RecentCall::PHONE_NUMBER		= "phone_number";
const std::string DatabaseTables::RecentCall::ORIG_PHONE_NUMBER	= "orig_phone_number";
const std::string DatabaseTables::RecentCall::IS_LOCAL          = "is_local";
const std::string DatabaseTables::RecentCall::SEEN	            = "seen";

const std::string DatabaseTables::RecentCall::DELETE_TABLE = DatabaseTables::DELETE_TABLE + DatabaseTables::RecentCall::TABLE;

const std::string DatabaseTables::RecentCall::CREATE_TABLE =
	  DatabaseTables::CREATE_TABLE 
	+ DatabaseTables::RecentCall::TABLE
	+ DatabaseTables::OPEN_PARENS

	+ DatabaseTables::RecentCall::ID				+ PRIMARY_KEY
	+ DatabaseTables::RecentCall::UID				+ TEXT			//Unique ID from SIP Stack
	+ DatabaseTables::RecentCall::STATUS			+ INTEGER_DEFAULT_0
	+ DatabaseTables::RecentCall::INCOMING			+ INTEGER_DEFAULT_0
	+ DatabaseTables::RecentCall::PHONE_NUMBER		+ TEXT		//This will be the normalized, 10-digit number
	+ DatabaseTables::RecentCall::ORIG_PHONE_NUMBER	+ TEXT		//As we get from user.	
	+ DatabaseTables::RecentCall::DURATION			+ INTEGER
	+ DatabaseTables::RecentCall::TIMESTAMP			+ INTEGER
	+ DatabaseTables::RecentCall::IS_LOCAL          + INTEGER
	+ DatabaseTables::RecentCall::SEEN				+ INTEGER_DEFAULT_0


	//NOTE: We do not (yet) have a UNIQUE key defined AND the "INTEGER" ends with a comma which gives us bad SQL.
	//	So for now, we will hard-code the default value.
	+ DatabaseTables::RecentCall::START_TIMESTAMP	+ " INTEGER "
//	+ DatabaseTables::RecentCall::START_TIMESTAMP	+ INTEGER

	+ DatabaseTables::CLOSE_PARENS
	;

//=============================================================================


//=============================================================================
//Settings table
//=============================================================================
const std::string DatabaseTables::Settings::TABLE							= "settings";

const std::string DatabaseTables::Settings::USER_ID							= "user_id";						//String

const std::string DatabaseTables::Settings::AUDIO_INPUT_DEVICE				= "audio_input_device";				//String
const std::string DatabaseTables::Settings::AUDIO_OUTPUT_DEVICE				= "audio_output_device";			//String
const std::string DatabaseTables::Settings::AUDIO_INPUT_VOLUME				= "audio_input_volume";				//int
const std::string DatabaseTables::Settings::AUDIO_OUTPUT_VOLUME				= "audio_output_volume";			//Int

const std::string DatabaseTables::Settings::SHOW_FAX_IN_MSG_THREAD			= "show_fax_in_msg_thread";			//Bool
const std::string DatabaseTables::Settings::SHOW_VM_IN_MSG_THREAD			= "show_vm_in_msg_thread";			//Bool
const std::string DatabaseTables::Settings::SHOW_RC_IN_MSG_THREAD			= "show_rc_in_msg_thread";			//Bool

const std::string DatabaseTables::Settings::ENABLE_SOUND_INCOMING_MSG		= "enable_sound_incoming_msg";		//Bool
const std::string DatabaseTables::Settings::ENABLE_SOUND_INCOMING_CALL		= "enable_sound_incoming_call";		//Bool
//const std::string DatabaseTables::Settings::ENABLE_SOUND_OUTGOING_MSG		= "enable_sound_outgoing_msg";		//Bool

const std::string DatabaseTables::Settings::MY_CHAT_BUBBLE_COLOR			= "my_chat_bubble_color";			//String
const std::string DatabaseTables::Settings::CONVERSANT_CHAT_BUBBLE_COLOR	= "conversant_chat_bubble_color";	//String

const std::string DatabaseTables::Settings::COUNTRY_CODE					= "country_code";					//String
const std::string DatabaseTables::Settings::COUNTRY_ABBREV					= "country_abbrev";					//String

//const std::string DatabaseTables::Settings::OUTGOING_CALL_SOUND_FILE		= "outgoing_call_sound_file";		//String	- This is controlled by server.
const std::string DatabaseTables::Settings::INCOMING_CALL_SOUND_FILE		= "incoming_call_sound_file";		//String
const std::string DatabaseTables::Settings::CALL_CLOSED_SOUND_FILE			= "call_closed_sound_file";			//String

const std::string DatabaseTables::Settings::WINDOW_LEFT						= "window_left";					//Double
const std::string DatabaseTables::Settings::WINDOW_TOP						= "window_top";						//Double
const std::string DatabaseTables::Settings::WINDOW_HEIGHT					= "window_height";					//Double
const std::string DatabaseTables::Settings::WINDOW_WIDTH					= "window_width";					//Double
const std::string DatabaseTables::Settings::WINDOW_STATE					= "window_state";					//Int

//From API/DB calls
const std::string DatabaseTables::Settings::LAST_MSG_TIMESTAMP				= "last_msg_timestamp";				//Long
const std::string DatabaseTables::Settings::LAST_CALL_TIMESTAMP				= "last_call_timestamp";			//Long
const std::string DatabaseTables::Settings::LAST_CONTACT_SOURCE_KEY			= "last_contact_source_key";		//String

//For SIP
const std::string DatabaseTables::Settings::SIP_UUID						= "sip_uuid";						//String


const std::string DatabaseTables::Settings::DELETE_TABLE = DatabaseTables::DELETE_TABLE + DatabaseTables::Settings::TABLE;

const std::string DatabaseTables::Settings::CREATE_TABLE =
	  DatabaseTables::CREATE_TABLE 
	+ DatabaseTables::Settings::TABLE
	+ DatabaseTables::OPEN_PARENS

	+ DatabaseTables::Settings::USER_ID							+ TEXT
	
	+ DatabaseTables::Settings::AUDIO_INPUT_DEVICE				+ TEXT
	+ DatabaseTables::Settings::AUDIO_OUTPUT_DEVICE				+ TEXT
	+ DatabaseTables::Settings::AUDIO_INPUT_VOLUME				+ INTEGER_DEFAULT_0
	+ DatabaseTables::Settings::AUDIO_OUTPUT_VOLUME				+ INTEGER_DEFAULT_0
	
	+ DatabaseTables::Settings::SHOW_FAX_IN_MSG_THREAD			+ INTEGER_DEFAULT_0
	+ DatabaseTables::Settings::SHOW_VM_IN_MSG_THREAD			+ INTEGER_DEFAULT_0
	+ DatabaseTables::Settings::SHOW_RC_IN_MSG_THREAD			+ INTEGER_DEFAULT_0
	
	+ DatabaseTables::Settings::ENABLE_SOUND_INCOMING_MSG		+ INTEGER_DEFAULT_1
	+ DatabaseTables::Settings::ENABLE_SOUND_INCOMING_CALL		+ INTEGER_DEFAULT_1
//	+ DatabaseTables::Settings::ENABLE_SOUND_OUTGOING_MSG		+ INTEGER_DEFAULT_1

	+ DatabaseTables::Settings::MY_CHAT_BUBBLE_COLOR			+ TEXT
	+ DatabaseTables::Settings::CONVERSANT_CHAT_BUBBLE_COLOR	+ TEXT

	+ DatabaseTables::Settings::COUNTRY_CODE					+ TEXT
	+ DatabaseTables::Settings::COUNTRY_ABBREV					+ TEXT

//	+ DatabaseTables::Settings::OUTGOING_CALL_SOUND_FILE		+ TEXT
	+ DatabaseTables::Settings::INCOMING_CALL_SOUND_FILE		+ TEXT
	+ DatabaseTables::Settings::CALL_CLOSED_SOUND_FILE			+ TEXT

	+ DatabaseTables::Settings::WINDOW_LEFT						+ REAL_DEFAULT_0
	+ DatabaseTables::Settings::WINDOW_TOP						+ REAL_DEFAULT_0
	+ DatabaseTables::Settings::WINDOW_HEIGHT					+ REAL_DEFAULT_0
	+ DatabaseTables::Settings::WINDOW_WIDTH					+ REAL_DEFAULT_0
	+ DatabaseTables::Settings::WINDOW_STATE					+ INTEGER_DEFAULT_0

	//From API/DB calls
	+ DatabaseTables::Settings::LAST_MSG_TIMESTAMP				+ INTEGER_DEFAULT_0
	+ DatabaseTables::Settings::LAST_CALL_TIMESTAMP				+ INTEGER_DEFAULT_0
	+ DatabaseTables::Settings::LAST_CONTACT_SOURCE_KEY			+ TEXT

	//For SIP
	+ DatabaseTables::Settings::SIP_UUID						+ TEXT

	//Table should have a single entry, but let's add a key just in case.
	+ " UNIQUE (" + DatabaseTables::Settings::USER_ID + ") ON CONFLICT REPLACE "	

	+ DatabaseTables::CLOSE_PARENS
	;

//=============================================================================
//Contact related definitions NOT in any table, but will be used when queries 
//	and in Contact table when it is re-implemented.
//=============================================================================

const std::string DatabaseTables::Contact::DISPLAY_NAME			= "display_name";
const std::string DatabaseTables::Contact::TYPE					= "type";	//For Contact list query that has Contacts and Groups (Via UNION)

const std::string DatabaseTables::Contact::VOXOX_COUNT			= "voxox_count";
const std::string DatabaseTables::Contact::BLOCKED_COUNT		= "blocked_count";
const std::string DatabaseTables::Contact::FAVORITE_COUNT		= "favorite_count";
const std::string DatabaseTables::Contact::PHONE_NUMBER_COUNT	= "phone_number_count";
const std::string DatabaseTables::Contact::EXTENSION_COUNT		= "extension_count";

const std::string DatabaseTables::Contact::EXTENSION			= "extension";
const std::string DatabaseTables::Contact::PREFERRED_NUMBER		= "preferred_number";

//=============================================================================



//=============================================================================
//AppSettings table - this is NOT part of user DBs.
//	This is APP level info and table will contain a single row of data
//=============================================================================
//TODO: Rename these fields so use is not so obvious, e.g parm1, parm2, parm3
const std::string DatabaseTables::AppSettings::TABLE					= "app_settings";

const std::string DatabaseTables::AppSettings::ID						= "id";							//Integer

const std::string DatabaseTables::AppSettings::AUTO_LOGIN				= "auto_login";					//Boolean
const std::string DatabaseTables::AppSettings::REMEMBER_PASSWORD		= "rem_pw";						//Boolean
const std::string DatabaseTables::AppSettings::LAST_LOGIN_USERNAME		= "last_login_username";		//String
const std::string DatabaseTables::AppSettings::LAST_LOGIN_PASSWORD		= "last_login_password";		//String
const std::string DatabaseTables::AppSettings::LAST_LOGIN_COUNTRY_CODE	= "last_login_country_code";	//String
const std::string DatabaseTables::AppSettings::LAST_LOGIN_PHONE_NUMBER	= "last_login_phone_number";	//String

const std::string DatabaseTables::AppSettings::DEBUG_MENU				= "debug_menu";					//String
const std::string DatabaseTables::AppSettings::IGNORE_LOGGERS			= "ignore_loggers";				//String
const std::string DatabaseTables::AppSettings::MAX_SIP_LOGGING			= "max_sip_logging";			//Boolean
const std::string DatabaseTables::AppSettings::GET_INIT_OPTIONS_HANDLING = "get_init_options_handling";	//Int


const std::string DatabaseTables::AppSettings::DELETE_TABLE = DatabaseTables::DELETE_TABLE + DatabaseTables::AppSettings::TABLE;

const std::string DatabaseTables::AppSettings::CREATE_TABLE =
	  DatabaseTables::CREATE_TABLE 
	+ DatabaseTables::AppSettings::TABLE
	+ DatabaseTables::OPEN_PARENS
	
	+ DatabaseTables::AppSettings::ID						 + INTEGER_DEFAULT_INVALID

	+ DatabaseTables::AppSettings::AUTO_LOGIN				 + INTEGER_DEFAULT_0
	+ DatabaseTables::AppSettings::REMEMBER_PASSWORD		 + INTEGER_DEFAULT_0
	
	+ DatabaseTables::AppSettings::LAST_LOGIN_USERNAME		 + TEXT
	+ DatabaseTables::AppSettings::LAST_LOGIN_PASSWORD		 + TEXT
	+ DatabaseTables::AppSettings::LAST_LOGIN_COUNTRY_CODE	 + TEXT
	+ DatabaseTables::AppSettings::LAST_LOGIN_PHONE_NUMBER	 + TEXT

	+ DatabaseTables::AppSettings::DEBUG_MENU				 + TEXT
	+ DatabaseTables::AppSettings::IGNORE_LOGGERS			 + TEXT
	+ DatabaseTables::AppSettings::MAX_SIP_LOGGING			 + INTEGER_DEFAULT_0
	+ DatabaseTables::AppSettings::GET_INIT_OPTIONS_HANDLING + INTEGER_DEFAULT_0

	//Table should have a single entry, but let's add a key just in case.
	+ " UNIQUE (" + DatabaseTables::AppSettings::ID + ") ON CONFLICT REPLACE "	

	+ DatabaseTables::CLOSE_PARENS
	;
