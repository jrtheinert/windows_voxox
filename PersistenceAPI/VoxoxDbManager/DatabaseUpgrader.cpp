#include "DatabaseUpgrader.h"
#include "DatabaseTables.h"


DatabaseUpgrader::DatabaseUpgrader()
{
}


DatabaseUpgrader::~DatabaseUpgrader()
{
}

//Main upgrade logic
void DatabaseUpgrader::upgrade( const VoxoxSQLiteDb* db, int oldVersion, int newVersion )
{
	switch ( oldVersion )
	{
	case DBVER_PRE_RELEASE:
		if ( newVersion == DBVER_RECENT_CALL_SEEN )
		{
			updateForRecentCallSeen_V1( db );
		}
	}
}

//-----------------------------------------------------------------------------
//Begin version-specific upgrade methods
//-----------------------------------------------------------------------------

void DatabaseUpgrader::updateForRecentCallSeen_V1( const VoxoxSQLiteDb* db )
{
	//Add new RecentCall::SEEN field.
	addColumnIfNotExist( db, DatabaseTables::RecentCall::TABLE, DatabaseTables::RecentCall::SEEN, "INTEGER DEFAULT 0");
}

//-----------------------------------------------------------------------------
//End version-specific upgrade methods
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
//Begin helper methods
//-----------------------------------------------------------------------------

//Add columName to tableName.
//	NOTE: This should NOT be used if the columnName data Type is being changed. ( A rare occurrence.)
void DatabaseUpgrader::addColumnIfNotExist( const VoxoxSQLiteDb* db, const std::string& tableName, const std::string& columnName, const std::string& columnType ) 
{
	if ( ! columnExists( db, tableName, columnName ) ) 
	{
		std::string sql = "ALTER TABLE " +  tableName + " ADD COLUMN " + columnName + " " + columnType + ";";
		const_cast<VoxoxSQLiteDb*>(db)->execDml( sql.c_str() );
	}
}

//Determine if columnName exists within tableName.
//	This should be included as a condition for every ALTER TABLE command in DatabaseUpgrader, as a safeguard.
//	NOTE: This should NOT be used if the columnName data Type is being changed. ( A rare occurrence.)
bool DatabaseUpgrader::columnExists( const VoxoxSQLiteDb* db, const std::string& tableName, const std::string& columnName )
{
	bool result = true;

	std::string query = "SELECT " + columnName +
						" FROM "  + tableName;

	try 
	{
		VoxoxSQLiteCursor* cursor = const_cast<VoxoxSQLiteDb*>(db)->execQuery( query.c_str() );

		if ( cursor != nullptr ) 
		{
			int index = cursor->fieldIndexOrThrowException( columnName.c_str() );
			result = index >= 0;
		}
	} 
	catch ( VoxoxSQLiteException e ) 
	{
		result = false;
	}

	return result;
}

//-----------------------------------------------------------------------------
//End helper methods
//-----------------------------------------------------------------------------
