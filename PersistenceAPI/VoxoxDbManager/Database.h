/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

//=====================================================================================================================
//This class will handle all basic DB access
//	- Instantiating DB
//	- Creating and Dropping DB
//	- Simplified SQL calls be used by DatabaseManager
//
//	It will be a singleton.
//=====================================================================================================================

#pragma once

#include "VoxoxSQLite.h"
#include "ContentValues.h"
#include "DataTypes.h"			//For ContactImportList in addAllContacts()
#include "StringList.h"
#include "DllExport.h"

#include <string>
#include <list>

using namespace VoxoxSQLite;

//=============================================================================
//A small class to more easily handle the query parameters and defaults.
//=============================================================================
class QueryParameters
{
public:
	DLLEXPORT QueryParameters();
	DLLEXPORT ~QueryParameters();

	//Sets --------------------------------------------------------------
	DLLEXPORT void setTable		   ( const std::string&   value )			{ mTable		 = value;	}
	DLLEXPORT void setSelection	   ( const std::string&   value )			{ mSelection	 = value;	}
	DLLEXPORT void setGroupBy	   ( const std::string&   value )			{ mGroupBy		 = value;	}
	DLLEXPORT void setHaving	   ( const std::string&   value )			{ mHaving		 = value;	}
	DLLEXPORT void setOrderBy	   ( const std::string&   value )			{ mOrderBy		 = value;	}
	DLLEXPORT void setLimit		   ( const std::string&   value )			{ mLimit		 = value;	}
	DLLEXPORT void setLimit		   ( int                  value )			{ mLimit		 = std::to_string( value );	}
	DLLEXPORT void setProjection   ( const StringList&    value)			{ mProjection	 = value;	}
	DLLEXPORT void setSelectionArgs( const StringList&    value )			{ mSelectionArgs = value;	}
	DLLEXPORT void setForceQuery   ( const bool			  value )			{ mForceQuery    = value;	}
	DLLEXPORT void setContentValues( const ContentValues& value )			{ mContentValues = value;	}


	//Gets --------------------------------------------------------------
	DLLEXPORT const std::string&	getTable()			const			{ return mTable;		}
	DLLEXPORT const std::string&	getSelection()		const			{ return mSelection;	}
	DLLEXPORT const std::string&	getGroupBy()		const			{ return mGroupBy;		}
	DLLEXPORT const std::string&	getHaving()			const			{ return mHaving;		}
	DLLEXPORT const std::string&	getOrderBy()		const			{ return mOrderBy;		}
	DLLEXPORT const std::string&	getLimit()			const			{ return mLimit;		}
	DLLEXPORT const StringList&		getProjection()		const			{ return mProjection;	}
	DLLEXPORT const StringList&		getSelectionArgs()	const			{ return mSelectionArgs;}
	DLLEXPORT const bool			forceQuery()		const			{ return mForceQuery;	}
	DLLEXPORT const ContentValues&	getContentValues()	const			{ return mContentValues;}

	//Misc.
	DLLEXPORT QueryParameters& operator=( const QueryParameters& src );

	DLLEXPORT size_t addProjection  ( const std::string& projection );
	DLLEXPORT size_t addSelectionArg( const std::string& arg        );
	DLLEXPORT size_t addSelectionArg( int				  arg        );
	DLLEXPORT size_t addSelectionArg( __int64			  arg        );

	DLLEXPORT size_t addContentValue( const std::string& key, const std::string& value );
	DLLEXPORT size_t addContentValue( const std::string& key, const char*        value );
	DLLEXPORT size_t addContentValue( const std::string& key, bool				  value );
	DLLEXPORT size_t addContentValue( const std::string& key, long				  value );
	DLLEXPORT size_t addContentValue( const std::string& key, __int64			  value );
	DLLEXPORT size_t addContentValue( const std::string& key, float			  value );
	DLLEXPORT size_t addContentValue( const std::string& key, double			  value );

	DLLEXPORT bool isValid() const;

private:
	std::string mTable;
	std::string mSelection;
	std::string mGroupBy;
	std::string mHaving;
	std::string mOrderBy;
	std::string mLimit;

	StringList mProjection;
	StringList mSelectionArgs;

	ContentValues	mContentValues;

	bool	   mForceQuery;			//Mostly for testing.
};

//=============================================================================


//=============================================================================

class Database
{
public:
	enum Conflict
	{
		ConflictNone	 = 0,
		ConflictRollback = 1,
		ConflictAbort    = 2,
		ConflictFail	 = 3,
		ConflictIgnore   = 4,
		ConflictReplace  = 5,
	};

	DLLEXPORT Database( const char* dbName );
	DLLEXPORT virtual ~Database();

	DLLEXPORT void		  setDbName( const char* dbName );
	DLLEXPORT const char* getDbName();
	DLLEXPORT bool        isOpen();

	DLLEXPORT static bool checkDatabase( const char* dbName );

	//Db setup and shut-down
	DLLEXPORT bool checkDatabase();
	DLLEXPORT bool checkTableExists( const std::string& tableName );
	DLLEXPORT void create( const StringList& tableNames, bool doUpgradeCheck );

	DLLEXPORT void close();

	//Some convenience methods for UnitTests
	DLLEXPORT void clearMessageTable();
	DLLEXPORT void clearPhoneNumberTable();
	DLLEXPORT void clearTable( const std::string& tableName );


	//Query related
	/**
	 * Generic query method. All functions in database manager that require a cursor
	 * to be returned should call this method.
	 * It is the responsible of the calling function to close the cursor
	 */
//	DLLEXPORT VoxoxSQLiteCursor* query( const std::string& table,     const StringList& projection, 
//										const std::string& selection, const StringList& selectionArgs, 
//										const std::string& groupBy,   const std::string& having,
//										const std::string& orderBy,   const std::string& limit );
	DLLEXPORT VoxoxSQLiteCursor* Database::query( const QueryParameters& params ) ;
	
	/**
	 * Generic raw query method. Functions with complex queries that require a cursor
	 * to be returned may call this method.
	 * The method runs the provided SQL and returns a Cursor over the result set.
	 * It is the responsible of the calling function to close the cursor.
	 */
	DLLEXPORT VoxoxSQLiteCursor* rawQuery( const std::string& sql, const StringList& selectionArgs);

	/**
	 * Utility method to run the query on the database and return the value in the first column of the first row.
	*/
	DLLEXPORT long getLongForQuery( const std::string& sql, const StringList& selectionArgs);
	DLLEXPORT long getLongForQuery( const QueryParameters params ) ;

	DLLEXPORT __int64 getInt64ForQuery( const std::string& sql, const StringList& selectionArgs);
	DLLEXPORT __int64 getInt64ForQuery( const QueryParameters params ) ;

	DLLEXPORT std::string getStringForQuery( const std::string& sql, const StringList& selectionArgs);
	DLLEXPORT std::string getStringForQuery( const QueryParameters params ) ;

	//synchronized 
	DLLEXPORT int deleteRecords( const std::string& table, const std::string& whereClause, const StringList& whereArgs);

	/**
	 * Generic update method. All functions in database manager that need to update a record
	 * in the local database should call this method.
	 * From SQLite documentation: An UPDATE statement is used to modify a subset of the values stored in zero or more rows of the database table identified
	 * by the qualified-table-name specified as part of the UPDATE statement.
	 * If the UPDATE statement does not have a WHERE clause, all rows in the table are modified by the UPDATE. Otherwise, the UPDATE affects only those rows for
	 * which the result of evaluating the WHERE clause expression as a boolean expression is true.
	 * It is not an error if the WHERE clause does not evaluate to true for any row in the table - this just means that the UPDATE statement affects zero rows.
	 */
	//	synchronized 
	DLLEXPORT int update( const std::string& table, const ContentValues& values, const std::string& selection, const StringList& selectionArgs );
	DLLEXPORT int update( const QueryParameters& params );

	/**
	 * Generic insert method. All functions in database manager that needs to insert a new record
	 * to the local database should call this method.
	 */
	//synchronized
	DLLEXPORT long insert( const std::string& table, const ContentValues& values, bool returnRowId );

	/**
	 * Generic insert with conflict method. All functions in database manager that needs to insert a new recored
	 * with a conflict resolver to the local database should call this method.
	 */
	//synchronized 
	DLLEXPORT long insertWithConflict( const std::string& table, const ContentValues& values, int conflict, bool returnRowId );

	DLLEXPORT int getLastRowId();

	//Let's expose some advance methods.
	int	beginTransaction()				{ return mDb->beginTransaction();		}
	int	commitTransaction()				{ return mDb->commitTransaction();		}
	int	rollbackTransaction()			{ return mDb->rollbackTransaction();	}
	int setSynchronous( bool value )	{ return mDb->setSynchronous( value );	}

	VoxoxSQLiteStatement compileStatement( const char* sql )	{ return mDb->compileStatement( sql ); }

	void setInitializingContacts( bool value )	{ mInitializingContactsOrUserSignOut = value; }

private:
	void createSQLiteDb();		//Helper

    /**
     * General method for inserting a row into the database.
     *
     * @param table the table to insert the row into
     * @param nullColumnHack optional; may be <code>null</code>.
     *            SQL doesn't allow inserting a completely empty row without
     *            naming at least one column name.  If your provided <code>initialValues</code> is
     *            empty, no column names are known and an empty row can't be inserted.
     *            If not set to null, the <code>nullColumnHack</code> parameter
     *            provides the name of nullable column name to explicitly insert a NULL into
     *            in the case where your <code>initialValues</code> is empty.
     * @param initialValues this map contains the initial column values for the
     *            row. The keys should be the column names and the values the
     *            column values
     * @param conflictAlgorithm for insert conflict resolver
     * @return the row ID of the newly inserted row
     * OR the primary key of the existing row if the input param 'conflictAlgorithm' =
     * {@link #CONFLICT_IGNORE}
     * OR -1 if any error
     */
    long insertWithOnConflict( const std::string& table, const std::string& nullColumnHack, const ContentValues& initialValues, int conflictAlgorithm);

	//Helper methods modeled on -Android- SQLiteDatabase methods
	/**
	 * Convenience method for updating rows in the database.
	 *
	 * @param table the table to update in
	 * @param values a map from column names to new column values. null is a
	 *            valid value that will be translated to NULL.
	 * @param whereClause the optional WHERE clause to apply when updating.
	 *            Passing null will update all rows.
	 * @param whereArgs You may include ?s in the where clause, which
	 *            will be replaced by the values from whereArgs. The values
	 *            will be bound as Strings.
	 * @param conflictAlgorithm for update conflict resolver
	 * @return the number of rows affected
	*/
	int updateWithOnConflict( const std::string& table, const ContentValues& values, const std::string& whereClause, const StringList& whereArgs, int conflictAlgorithm );


	//Upgrade related
	int	 getUserVersion()					{ return mUserVersion; }
	void checkForUpgrade();
	void onUpgrade( VoxoxSQLiteDb* db, int oldVersion, int newVersion );


private:
//	static Database*			mInstance;

//#ifdef _DEBUG
	/*static*/ std::string			mDbName;		//Non-CONST for testing.
//#else
//	static const std::string	mDbName;
//#endif

//	static StringList			mConflictValues;
	static char*				mConflictValues[];

	VoxoxSQLiteDb*				mDb;
	bool						mInitializingContactsOrUserSignOut;
	int							mUserVersion;
};

