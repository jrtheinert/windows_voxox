/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

#pragma once

#include "DllExport.h"

#include <string>
#include <map>

class DataType;	//Fwd declaration

typedef std::map<std::string, DataType> KeyValueMap;

//=============================================================================
//Small class to support various and supported data types
//=============================================================================

class DataType
{
	enum Type
	{
		Type_Null   = 0,
		Type_String = 1,
		Type_Long   = 2,
		Type_Float  = 3,
		Type_Double = 4,
		Type_Bool   = 5,
	};

public:
	DLLEXPORT DataType();
	DLLEXPORT DataType( void*			   value );
	DLLEXPORT DataType( const std::string& value );
	DLLEXPORT DataType( int				   value );
	DLLEXPORT DataType( long			   value );
	DLLEXPORT DataType( __int64			   value );
	DLLEXPORT DataType( float			   value );
	DLLEXPORT DataType( double			   value );
	DLLEXPORT DataType( bool		       value );

	DLLEXPORT ~DataType();

	DLLEXPORT bool isNull()					{ return mType == Type_Null;	}
	DLLEXPORT bool isString()				{ return mType == Type_String;	}
	DLLEXPORT bool isLong()					{ return mType == Type_Long;	}
	DLLEXPORT bool isFloat()				{ return mType == Type_Float;	}
	DLLEXPORT bool isDouble()				{ return mType == Type_Double;	}
	DLLEXPORT bool isBool()					{ return mType == Type_Bool;	}

//	std::string getValueAsNull();
	DLLEXPORT std::string	getValueAsString();
	DLLEXPORT __int64		getValueAsLong();
	DLLEXPORT float			getValueAsFloat();
	DLLEXPORT double		getValueAsDouble();
	DLLEXPORT bool			getValueAsBool();

private:
	void initVars();

private:
	Type		mType;

	std::string	mString;
	__int64		mLong;
	float		mFloat;
	double		mDouble;
	bool		mBool;
};

//=============================================================================


//=============================================================================

class ContentValues
{
public:
	DLLEXPORT ContentValues();
	DLLEXPORT ~ContentValues();

    /**
     * Creates a set of values copied from the given set
     *
     * @param from the values to copy
     */
    DLLEXPORT ContentValues( const ContentValues& from );


	//----------------------------------------------------
	//Group of methods to PUT key/values into map
	//----------------------------------------------------

    /**
     * Adds all values from the passed in ContentValues.
     *
     * @param other the ContentValues from which to copy
     */
    DLLEXPORT void putAll( ContentValues other );

    /**
     * Adds a value to the set.
     *
     * @param key the name of the value to put
     * @param value the data for the value to put
     */
	DLLEXPORT void put( const std::string& key, const std::string& value );
	DLLEXPORT void put( const std::string& key, const char*        value );

    /**
     * Adds a value to the set.
     *
     * @param key the name of the value to put
     * @param value the data for the value to put
     */
    DLLEXPORT void put( const std::string& key, bool value );

    /**
     * Adds a value to the set.
     *
     * @param key the name of the value to put
     * @param value the data for the value to put
     */
    DLLEXPORT void put( const std::string& key, int     value );
    DLLEXPORT void put( const std::string& key, long    value );
    DLLEXPORT void put( const std::string& key, __int64 value );

    /**
     * Adds a value to the set.
     *
     * @param key the name of the value to put
     * @param value the data for the value to put
     */
	DLLEXPORT void put( const std::string& key, float value);

    /**
     * Adds a value to the set.
     *
     * @param key the name of the value to put
     * @param value the data for the value to put
     */
	DLLEXPORT void put( const std::string& key, double value);

    /**
     * Adds a null value to the set.
     *
     * @param key the name of the value to make null
     */
	DLLEXPORT void putNull( const std::string& key );

    /**
     * Returns the number of values.
     *
     * @return the number of values
     */
	DLLEXPORT size_t size() const;

    /**
     * Remove a single value.
     *
     * @param key the name of the value to remove
     */
	DLLEXPORT void remove( const std::string& key );

    /**
     * Removes all values.
     */
	DLLEXPORT void clear();

    /**
     * Returns true if this object has the named value.
     *
     * @param key the value to check for
     * @return {@code true} if the value is present, {@code false} otherwise
     */
    DLLEXPORT bool containsKey( const std::string& key);

	/**
     * Gets a value. Valid value types are {@link String}, {@link Boolean}, and
     * {@link Number} implementations.
     *
     * @param key the value to get
     * @return the data for the value
     */
//    Object get(String key) {
//        return mValues.get(key);
//    }

    /**
     * Gets a value and converts it to a String.
     *
     * @param key the value to get
     * @return the String for the value
     */
    DLLEXPORT std::string getAsString( const std::string& key);

    /**
     * Gets a value and converts it to a Long.
     *
     * @param key the value to get
     * @return the Long value, or null if the value is missing or cannot be converted
     */
    DLLEXPORT __int64 getAsLong( const std::string& key );
    /**
     * Gets a value and converts it to a Double.
     *
     * @param key the value to get
     * @return the Double value, or null if the value is missing or cannot be converted
     */
    DLLEXPORT double getAsDouble( const std::string& key );

    /**
     * Gets a value and converts it to a Float.
     *
     * @param key the value to get
     * @return the Float value, or null if the value is missing or cannot be converted
     */
    DLLEXPORT float getAsFloat( const std::string& key );

    /**
     * Gets a value and converts it to a Boolean.
     *
     * @param key the value to get
     * @return the Boolean value, or null if the value is missing or cannot be converted
     */
    DLLEXPORT bool getAsBoolean( const std::string& key );

    /**
     * Returns a set of all of the keys and values
     *
     * @return a set of all of the keys and values
     */
//    Set<Map.Entry<String, Object>> valueSet() {
//        return mValues.entrySet();
//    }
	DLLEXPORT KeyValueMap& getMap()					{ return mValues;	}

    /**
     * Returns a set of all of the keys
     *
     * @return a set of all of the keys
     */
//    Set<String> keySet() {
//        return mValues.keySet();
//    }

    /**
     * Returns a string containing a concise, human-readable description of this object.
     * @return a printable representation of this object.
     */
//	public String toString() {
//		StringBuilder sb = new StringBuilder();
//		for (String name : mValues.keySet()) {
//			String value = getAsString(name);
//			if (sb.length() > 0) sb.append(" ");
//			sb.append(name + "=" + value);
//		}
//		return sb.toString();
//	}

private:
	void add( const std::string& key, DataType data );

private:
    KeyValueMap	mValues;

};

