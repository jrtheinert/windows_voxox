/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

#include "QueryBuilder.h"

QueryBuilder::QueryBuilder()
{
}

QueryBuilder::~QueryBuilder()
{
}

//Static
std::string QueryBuilder::buildQueryString( bool distinct, const std::string& tables, StringList columns,
						 					const std::string& where,  const std::string& groupBy, 
											const std::string& having, const std::string& orderBy, 
											const std::string& limit )
{
    if ( groupBy.empty()  && !having.empty() )
	{
//        throw new IllegalArgumentException( "HAVING clauses are only permitted when using a groupBy clause" );	//TODO:
    }

//  if ( !limit.empty() && !sLimitPattern.matcher(limit).matches() )	//TODO
//	{
//        throw new IllegalArgumentException("invalid LIMIT clauses:" + limit);		//TODO
//  }

    std::string query;

    query.append("SELECT ");

    if (distinct) 
	{
        query.append("DISTINCT ");
    }
    
	if ( columns.size() != 0 ) 
	{
        appendColumns(query, columns);
    } 
	else 
	{
        query.append("* ");
    }
    
	query.append("FROM ");
    query.append(tables);
    
	appendClause(query, " WHERE ", where);
    appendClause(query, " GROUP BY ", groupBy);
    appendClause(query, " HAVING ", having);
    appendClause(query, " ORDER BY ", orderBy);
    appendClause(query, " LIMIT ", limit);

    return query;
}

//static 
void QueryBuilder::appendClause( std::string& sql, const std::string& name, const std::string& clause)
{
    if ( !clause.empty() ) 
	{
        sql.append(name);
        sql.append(clause);
    }
}

//static 
void QueryBuilder::appendColumns( std::string& sql, StringList columns ) 
{
	size_t n = columns.size();

	for ( StringList::iterator it = columns.begin(); it != columns.end(); it++ ) 
	{
		std::string column = (*it);

		if ( !column.empty() ) 
		{
			if ( it != columns.begin() ) 
			{
				sql.append(", ");
			}

			sql.append(column);
		}
	}

	sql.append( " " );
}
