/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

#pragma once

#include "StringList.h"
#include "DllExport.h"

#include <string>

//Forward delcarations so we don't need the sqlite3.h dependency in this .h file
struct sqlite3;
struct sqlite3_stmt;

typedef __int64 sqlite_int64;

namespace VoxoxSQLite 
{

#define VOXOX_SQLITE_ERROR				1000
#define VOXOX_SQLITE_ERROR_DB_NOT_OPEN	1001

//=============================================================================
//	VoxoxSQLiteError class - Collection of static methods to handle errors in other classes
//=============================================================================

class DLLEXPORT VoxoxSQLiteError
{
public:
	static bool			isOk   ( int value );	//Return code from low-level DB call
	static bool			isError( int value );	//Return code from low-levle DB call
	static bool			isDone ( int value );	//Return code from low-levle DB call
	static bool			hasRow ( int value );	//Return code from low-levle DB call

	static void			throwVoxoxSQLiteException( const char* errMsg );
	static bool			throwExceptionOnError( int errorCode, sqlite3* db );
	static bool			throwExceptionOnError( int errorCode, sqlite3* db, const char* errMsg );

	static const char*	getDbErrorMsg( sqlite3* db );
	
	static void			checkDB       ( sqlite3* db );
	static void			checkStatement( sqlite3_stmt* statement );	//VM?

private:
	VoxoxSQLiteError()		{}		//So cannot be instantiated
};

//=============================================================================



//=============================================================================
//	VoxoxSQLiteException class
//	- TODO: Add more specific exceptions based on specific error
//=============================================================================

class VoxoxSQLiteException
{
public:
	DLLEXPORT VoxoxSQLiteException( const int errCode, const char* errMsg );

	DLLEXPORT VoxoxSQLiteException( const VoxoxSQLiteException& e );

	DLLEXPORT virtual ~VoxoxSQLiteException();

	DLLEXPORT const int	getErrorCode()		{ return mErrorCode;		}
	DLLEXPORT const char*	getErrorMsg()		{ return mErrorMsg.c_str();	}

	static const char* VoxoxSQLiteException::errorCodeToString( int nErrCode );	//Useful for debugging.


private:
	int			mErrorCode;
	std::string	mErrorMsg;
};

//=============================================================================


//=============================================================================
//	VoxoxSQLiteBuffer class
//	- Provides proper handling of embedded quotes, quite handy for string queries!
//=============================================================================

class DLLEXPORT VoxoxSQLiteBuffer
{
public:
    VoxoxSQLiteBuffer();
    ~VoxoxSQLiteBuffer();

    const char* format( const char* szFormat, ... );

    operator const char*()		{ return mBuf; }

    void clear();

private:
    char* mBuf;
};

//=============================================================================


//=============================================================================
//	VoxoxSQLiteCursor class
//=============================================================================

class DLLEXPORT VoxoxSQLiteCursor
{
public:
    VoxoxSQLiteCursor();
    VoxoxSQLiteCursor( const VoxoxSQLiteCursor& src );
    VoxoxSQLiteCursor( sqlite3* db, sqlite3_stmt* statement, bool eof, bool ownStatement = true );

    VoxoxSQLiteCursor& operator=( const VoxoxSQLiteCursor& src );

    virtual ~VoxoxSQLiteCursor();

	bool isValid();
    int  numFields();
    bool eof();
    void nextRow();
    void finalize();

	std::string toString();		//Mostly for debugging

    int			fieldIndex( const char* name );						//Just return -1 if field name is invalid.
	int			fieldIndexOrThrowException( const char* name );		//Throw exception on error
	bool		isFieldIndexValid( int index );


    const char* fieldName      ( int index );
    const char* fieldType	   ( int index );
//  const char* fieldDeclType  ( int index );	//This crashes with aggregate fields, so let's avoid it.  Use fieldType() instead.
    int			fieldDataType  ( int index );

	//By Field Index - These are MORE efficient than the Field Name based methods
    const char*			 fieldValue    ( int index );
	bool				 fieldIsNull   ( int index );

    int					 getInt		   ( int index, int          nullValue = 0	 );
    double				 getFloat	   ( int index, double       nullValue = 0.0 );
    const char*			 getString     ( int index, const char*  nullValue = ""  );
 	sqlite_int64		 getInt64      ( int index, sqlite_int64 nullValue = 0	 );
	const unsigned char* getBlob       ( int index, int& nLen);

    int					 getIntEx	   ( int index, int          nullValue = 0	 );
    double				 getFloatEx	   ( int index, double       nullValue = 0.0 );
    const char*			 getStringEx   ( int index, const char*  nullValue = ""  );
 	sqlite_int64		 getInt64Ex    ( int index, sqlite_int64 nullValue = 0	 );
	const unsigned char* getBlobEx     ( int index, int& nLen);

	//By Field Name - These are LESS efficient than the Field Indiex based methods
    const char*			 fieldValue    ( const char* name );
    bool				 fieldIsNull   ( const char* name );
    int					 getInt        ( const char* name, int           nullValue = 0	 );
    double				 getFloat      ( const char* name, double        nullValue = 0.0 );
    const char*			 getString     ( const char* name, const char*   nullValue = ""	 );
	sqlite_int64		 getInt64      ( const char* name, sqlite_int64  nullValue = 0	 );
    const unsigned char* getBlob       ( const char* name, int& nLen);

private:
	void checkStatement();
	void checkFieldIndex( int index );
	void throwErrorOnInvalidIndex( int index );
	int	 fieldIndex( const char* name, bool throwOnError );	//Optionally throw exception on error

private:
	sqlite3*		mDb;
    sqlite3_stmt*	mStatement;
    bool			mEof;
    int				mCols;
    bool			mOwnStatement;
};

//=============================================================================



//=============================================================================
//	VoxoxSQLiteStatement class
//=============================================================================

class DLLEXPORT VoxoxSQLiteStatement
{
public:
    VoxoxSQLiteStatement();
    VoxoxSQLiteStatement( const VoxoxSQLiteStatement& statement );
    VoxoxSQLiteStatement( sqlite3* db, sqlite3_stmt* statement );
	VoxoxSQLiteStatement( sqlite3* db, const char* sql, const StringList& args );

    virtual ~VoxoxSQLiteStatement();

    VoxoxSQLiteStatement& operator=( const VoxoxSQLiteStatement& src );

    int execDml();

    VoxoxSQLiteCursor* execQuery();

    void bind    ( int param, const char*		   value );
    void bind    ( int param, const int			   value );
    void bind    ( int param, const double		   value );
    void bind    ( int param, const unsigned char* value, int nLen );
    void bindNull( int param );

	int bindParameterIndex( const char* param );
    void bind			  ( const char* param, const char*			value );
    void bind			  ( const char* param, const int			value );
    void bind			  ( const char* param, const double			value );
    void bind			  ( const char* param, const unsigned char*	value, int nLen);
    void bindNull		  ( const char* param );

	void reset();
    void finalize();
	void close();			//Convenenience

private:
    void checkDB();
    void checkStatement();

private:
    sqlite3*		mDb;
    sqlite3_stmt*	mStatement;
};

//=============================================================================


//=============================================================================
//	VoxoxSQLiteDb class
//=============================================================================

class DLLEXPORT VoxoxSQLiteDb
{
public:
	VoxoxSQLiteDb();
	virtual ~VoxoxSQLiteDb();

	void openOrCreateReadOnly ( const char* fileName );
	void openOrCreateWriteable( const char* fileName );
	void open		          ( const char* fileName, int flags );
	void close();

	bool tableExists( const char* tableName );

	//User version for DB upgrade
	void	setUserVersion( int version );
	int		getUserVersion();

	//Statement preparation and execution
	VoxoxSQLiteStatement compileStatement  ( const char* sql );
	VoxoxSQLiteCursor*   execQuery         ( const char* sql );
	VoxoxSQLiteCursor*	 execQuery         ( const char* sql, const StringList& args );
	int					 execDml           ( const char* sql );
	int					 execDml		   ( const char* sql, const StringList& args );

	long				 simpleQueryForLong( const char* sql, long nullValue );
	long				 simpleQueryForLong( const char* sql, long nullValue, const StringList& args );
	long				 getLongFromCursor ( VoxoxSQLiteCursor* cursor, long nullValue );

	__int64				 simpleQueryForInt64( const char* sql, __int64 nullValue );
	__int64				 simpleQueryForInt64( const char* sql, __int64 nullValue, const StringList& args );
	__int64				 getInt64FromCursor ( VoxoxSQLiteCursor* cursor, __int64 nullValue );

	std::string			 simpleQueryForString( const char* sql, const char* nullValue );
	std::string			 simpleQueryForString( const char* sql, const char* nullValue, const StringList& args );
	std::string			 getStringFromCursor ( VoxoxSQLiteCursor* cursor, const char* nullValue );

	//DB configuration/settings
	int					deleteRecords( const std::string& table, const std::string& whereClause, const StringList& whereArgs);
	sqlite_int64		getLastRowId();

	void interrupt();
	void setBusyTimeout( int nMillisecs );		//Busy Timeout
	bool isAutoCommitOn();						//Read-only

	//Some convenience methods
	static bool			doesDbExist( const char* dbName );		//Determine if Db already exists AS A DATABASE.  Not a simple file check.
	bool				isOpen();
	bool				isReadOnly();
	bool				isWriteable();

	int					beginTransaction();
	int					commitTransaction();
	int					rollbackTransaction();
	int					setSynchronous( bool value );

	//Some static version methods
    static const char* SQLiteVersion();
    static const char* SQLiteHeaderVersion();
    static const char* SQLiteLibraryVersion();
    static int		   SQLiteLibraryVersionNumber();

private:
	VoxoxSQLiteCursor*  execStatement( sqlite3_stmt* stmt );

    void checkDB();

	//Convenience methods
	const char* getDbErrorMsg();		//May be NULL, if no error

private:
    sqlite3*	mDb;

	//We may want to consider a separate 'Configuration' class if we have too many of these
	bool		mIsOpen;
	int			mOpenFlags;
    int			mBusyTimeoutMs;

	static const char* mBeginTransactionSql;
	static const char* mCommitTransactionSql;
	static const char* mRollbackTransactionSql;
	static const char* mSynchronousOff;
	static const char* mSynchronousOn;


};	//VoxoxSQLiteDb

};	//namespace

