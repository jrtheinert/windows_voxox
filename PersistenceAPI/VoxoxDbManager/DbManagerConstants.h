/**
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

//=====================================================================================================================
// This class will contain some static memvars to define some common and typically large query items
//	to keep VoxoxDbManager code cleaner.
//	- All methods/memvars are static
//	- This helps keep the VoxoxDbManager code clearer.
//  
//=====================================================================================================================

#pragma once

#include "StringList.h"

class DbManagerConstants
{
public:
	static StringList	getGetMessagesProjection();
	static StringList	getGetMessagesForGroupIdProjection();
	static StringList	getGetMessagesForCmGroupProjection();
	static StringList	getGetPhoneNumberProjection();
	static StringList	getMessageTranslationPropertyProjection();
	static StringList	getGetRecentCallProjection();
	static StringList	getGetSettingsProjection();
	static StringList	getGetAppSettingsProjection();


	static std::string	getNoDeletedMessagesCondition();
	static std::string	getContactSelect();
	static std::string	getPhoneNumberAsContactSelectLong();
	static std::string	getPhoneNumberAsContactSelectShort();
//	static std::string	getGroupMessageGroupAsContactSelectLong();
//	static std::string	getGroupMessageGroupAsContactSelectShort();

	static std::string  getContactsMessagesDisplayNameSubquery();
	static std::string  getRecentCallRawQuery( const std::string& whereClause );

	static std::string	getVoxoxCountSubquery();
	static std::string	getBlockedCountSubquery();
	static std::string	getFavoriteCountSubquery();
	static std::string	getPhoneNumberCountSubquery();
	static std::string	getExtensionCountSubquery();
	static std::string	getExtensionSubquery();
	static std::string	getBestPhoneNumberSubquery();

	static std::string	getGmGroupActiveClause();
	static std::string  getXmppContactsMessagesOnlyRawQuery();
	static std::string	getAllContactsMessagesSummaryRawQuery();
	static std::string	getAllContactsMessagesSummaryRawQuery( bool includeVoicemail, bool includeRecordedCalls, bool includeFaxes );
	static std::string  getMessageToPhoneNumberJoin();

	static std::string	getSourceInClause();

private:
	DbManagerConstants()	{}	//Prevents instantiation
	~DbManagerConstants()	{}

	static std::string DbManagerConstants::formatPhoneNumberBasedCountSubquery( const std::string& criteria, const std::string& asClause );

	static StringList   mGetMessagesProjection;
	static StringList   mGetMessagesForCmGroupProjection;
	static StringList   mGetMessagesForGroupIdProjection;
	static StringList	mGetPhoneNumberProjection;
	static StringList	mMessageTranslationPropertyProjection;
	static StringList	mGetRecentCallProjection;
	static StringList	mGetSettingsProjection;
	static StringList	mGetAppSettingsProjection;

	static std::string	mNoDeletedMessagesCondition;		//Used to exclude DELETED messages from query.
	static std::string	mContactSelect;
	static std::string	mPhoneNumberAsContactSelectLong;
	static std::string	mPhoneNumberAsContactSelectShort;
	static std::string  mGroupMessageGroupAsContactSelectLong;
	static std::string  mGroupMessageGroupAsContactSelectShort;

	static std::string	mContactsMessagesDisplayNameSubquery;
	static std::string	mRecentCallRawQuery;

	static std::string	mVoxoxCountSubquery;
	static std::string	mBlockedCountSubquery;
	static std::string	mFavoriteCountSubquery;
	static std::string	mPhoneNumberCountSubquery;
	static std::string	mExtensionCountSubquery;
	static std::string	mExtensionSubquery;
	static std::string	mBestPhoneNumberSubquery;
	
	static std::string  mGmGroupActiveClause;
	static std::string  mXmppContactsMessagesOnlyRawQuery;
	static std::string	mAllContactsMessagesSummaryRawQuery;
	static std::string  mMessageToPhoneNumberJoin;

	static std::string	mSourceInClause;
};

