/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

//=====================================================================================================================
//This class will handle all DB upgrades.  Initially, it will be just a placeholder, but this will allow us to 
//	confirm the logic for detecting and setting DB user version in underly DB provider (SQLite)
//=====================================================================================================================
#pragma once

#include "VoxoxSQLite.h"

using namespace VoxoxSQLite;

class DatabaseUpgrader
{
private:
	//DB user versions are 32-bit int stored in SQLite file header.
	//List each new version here as an enum
	enum DbVersion
	{
		DBVER_PRE_RELEASE		= 0,
		DBVER_RECENT_CALL_SEEN	= 1,
	};

	static const int s_DbVersion = DBVER_RECENT_CALL_SEEN;	//Set this to the most recent DbVersion

public:
	DatabaseUpgrader();
	~DatabaseUpgrader();

	//Main upgrader logic
	void upgrade( const VoxoxSQLiteDb* db, int oldVersion, int newVersion );

	static int getCurrentVersion()		{ return s_DbVersion; }

private:
	//Add upgrade methods here.
	void updateForRecentCallSeen_V1( const VoxoxSQLiteDb* db );

	//Helper methods
	bool columnExists       ( const VoxoxSQLiteDb* db, const std::string& tableName, const std::string& columnName );
	void addColumnIfNotExist( const VoxoxSQLiteDb* db, const std::string& tableName, const std::string& columnName, const std::string& columnType );


};

