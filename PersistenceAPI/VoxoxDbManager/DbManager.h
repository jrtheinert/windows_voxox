/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

/*
 * This class represents the public interface for our Persistence API.
 *  ALL calls to Persistence API MUST use this interface.
 *
 *	It is instantiated as a singleton.
 *
 *	In general, all PUBLIC methods should return well-defined object or object lists, NOT Cursors.
 *
 *	It is OK for PRIVATE methods to return cursors, but those cursors must be converted
 *	  the well-defined objects to be returned to the main app.
 *
 *	NOTE: That some of the NOTEs here are directly from the -Android- code upon which this class is based.
 *		They may not pertain here, but I will leave them until I have verified that is the case.
 */

#pragma once

#include "DataTypes.h"
#include "BroadcastManager.h"

#include "StringList.h"
#include "VoxoxSQLite.h"
#include "DllExport.h"

#include <string>
#include <list>

class Database;	//Fwd declaration

class VoxoxDbManager
{
public:
	//Do NOT re-order or change these values.  Feel free to append
	enum DbType
	{
		User	= 0,
		App		= 1,
	};

	DLLEXPORT VoxoxDbManager( const std::string& dbName, DbType dbType );
	DLLEXPORT ~VoxoxDbManager();

	DLLEXPORT bool		checkDatabase();
	DLLEXPORT bool		checkTableExists( const std::string& tableName );
	DLLEXPORT bool		create();
	DLLEXPORT std::string	getDbName() const				{ return mDbName;	}
	DLLEXPORT void		setDbChangeCallback( DbChangeCallbackFunc callback );

	DLLEXPORT static bool checkDatabase( const std::string& dbName );

	//These methods help support unit testing.
	DLLEXPORT void		close();
	DLLEXPORT void		clearMessageTable();
	DLLEXPORT void		clearPhoneNumberTable();

private:
	void		deleteDatabase();		//I cannot see an instance where we would want to call this, but I will create it anyway and make it private.
	Database*	getDb()				{ return mDb;	}
	bool		isDbValid() const;

private:
	Database*	mDb;
	DbType		mDbType;
	std::string	mDbName;

	bool		mShuttingDown;			//Flag used to indicate getInstance should NOT re-instantiate.

	BroadcastManager	mBroadcastMgr;


//	private static  int MESSAGE_OR_CALL_RECEIVED_NOTIFICATION_ID=1;

//	public enum GroupMessagingBroadcastUpdate{
//		ADD, EDIT, DELETE, EMPTY;
//	}

	//---------------------------------------
	// PUBLIC Utility Methods
	//---------------------------------------
public:
	DLLEXPORT std::string normalizePhoneNumber( const std::string& phoneNumber );
	DLLEXPORT std::string normalizeJid	      ( const std::string& jid		   );

public:
	//---------------------------------------
	// PUBLIC Contact Methods
	//---------------------------------------
/**
 * Adds contacts by iterating over the list.
 */
	DLLEXPORT void addAllContacts( const ContactImportList& contacts );

private:
	/**
	 * This assigns cm_group which makes query for ContactsMessagesActivity much easier.
	 * 	- Assign unique cm_group for each Contactkey.
	 * 	- If pn.number occurs more than once, cm_group becomes MAX(cm_group) + 1, even it overrides value previously cm_group value.
	 *  - This should cover import from native AB. Any Voxox numbers added later will use MAX(cm_group) + 1
	 */
	void assignContactMessageGroup( bool doStep2 );
	void assignContactMessageGroup( int step );

	/**
	* Get next available CM_GROUP value
	*/
	int  getNextCmGroup();
	bool cmGroupIsValid( int value );	//Because we have instances where we use 0 or -1 as invalid.

	//---------------------------------------
	// PUBLIC Messages Methods
	//---------------------------------------
public:
	/**
	 * returns the last inserted server_timestamp, so we can query server for newer messages.
	 */
	DLLEXPORT __int64 getLatestMessageServerTimestamp( Message::Type type, Message::Direction direction, Message::Status status );

	/**
	 * returns Message with given LOCAL timestamp.
	 */
	DLLEXPORT Message getMessage( __int64 timeStamp );

	/**
	 * returns List of messages based on criteria.
	 */
	DLLEXPORT MessageList getMessages( Message::Status messageStatus, Message::Direction messageDirection);	//Appears to be unused

	/**
	 * returns List of ALL VM and RECORDED CALL messages for ALL CONTACTS, except deleted messages.
	 */
	DLLEXPORT UmwMessageList getVmAndRecordedCallMessages();

	/**
	 * returns List of ALL FAX messages for ALL CONTACTS, except deleted messages.
	 */
	DLLEXPORT UmwMessageList getFaxMessages();

	/**
	 * Description: returns a cursor to all the messages of a phone number.
	 * used when we receive an SMS message with a phone number that is not in our address book.
	 *
	 * NOTE: TODO: WE WILL CONVERT THE CURSOR TO A LIST OF OBJECTS
	 *
	 * Used by UMW
	 */
	DLLEXPORT UmwMessageList getMessagesForPhoneNumber( const std::string& did );	

	/**
	 * Description: returns a cursor to all the messages of a contact.
	 *
	 * Used by UMW
	 */
	DLLEXPORT UmwMessageList getMessagesForCmGroup( int cmGroup);

	/**
	 * returns a cursor to all the messages from an GM GroupId
	 *
	 */
	DLLEXPORT UmwMessageList getMessagesForGroupId( const std::string& groupId );

	/**
	 * Description:
	 * 	Update Message in DB with RichData.
	 * 
	*/
	DLLEXPORT void updateMessageRichData( __int64 timeStamp, const RichData& richData, bool broadcast );

	/**
	 * Description:
	 * 	Update GM message in DB with RichData.
	 * 	We need this separate method because RichData is embedded in body so we cannot simply replace BODY.
	 * */
	DLLEXPORT void updateGroupMessageRichData( __int64 timeStamp, const RichData& richData, bool broadcast, const std::string& origBody );

	/**
	* Update message status based on LOCAL timestamp
	*/
	DLLEXPORT void updateMessage( __int64 timeStamp, Message::Status status);

	/**
	* Update message status and MessageId based on LOCAL timestamp
	*/
	DLLEXPORT void updateMessage( __int64 timeStamp, Message::Status status, const std::string& messageId );

	/**
	* Update message status and SERVER timestamp based on LOCAL timestamp
	*/
	DLLEXPORT void updateMessage( __int64 timeStamp, __int64 serverTimeStamp, Message::Status status);

	/**
	* Update message status, SERVER timestamp and MessageId based on LOCAL timestamp
	*/
	DLLEXPORT void updateMessage( __int64 timeStamp, __int64 serverTimeStamp, Message::Status status, const std::string& messageId );

	/**
	* Update message translation message based on LOCAL timestamp
	*/
	DLLEXPORT void updateTranslatedMessage( __int64 timeStamp, const Message& message );

	/**
	* Some times we receive message from contacts not in our address book.
	*
	* We log the original message, then use the phone_number to determine if it is a Voxox user.  This is done asynchrously.
	*
	* This method updates those messages, by calling the single instance version below.
	*/
	DLLEXPORT void updateMessageContactInfo( const VoxoxContactList& vcs);

	/**
	* Appears to be a convenience method to udpate message status as DELIVERED
	*/
	DLLEXPORT void markMessageDelivered( __int64 timeStamp, bool broadcast );

	/**
	* Appears to be a convenience method to udpate message status as READ
	*/
	DLLEXPORT void markMessageRead( __int64 timeStamp, bool broadcast );

	DLLEXPORT bool addMessage( const Message& message, bool broadcast );

	/**
	*	Add list of messages to DB.
	*	Typically done from getMessageHistory() API call.
	*/
	DLLEXPORT void addMessages( const MessageList&  messages );

	DLLEXPORT int deleteMessages( const MessageList& messages );		//TODO: We just need the LOCAL timestamp, so consider just an array of LONGs.

	/*
	* This REMOVES matches records from the DB.  This is used to keep DBs in sync
	*	among various platforms.  These messages come from getMessageHistory()
	*/
	DLLEXPORT int removeDeletedMessages( const MessageList& messages );		//TODO: We just need the LOCAL timestamp, so consider just an array of LONGs.

	/*
	*	Gathers data needed to delete messages from the server.
	*
	*	This looks for all local messages with status DELETED.
	*	Once this data is used in the API call to delete the messages on server, 
	*	a separate method will delete these from the local DB.
	*/
	DLLEXPORT DeleteMessageDataList getMessageDataForDeletedMessages();
	

	/**
	 * Determine if an incoming GM is from an outgoing GM that we sent.
	 * 	We do not want duplicates.
	 *
	 * @param messageId
	 * @return
	 */
	DLLEXPORT bool hasMessageId( const std::string& messageId );

	DLLEXPORT __int64 getLocalTimestampForMsgId( const std::string& msgId );


	/*
	 * Do we have any messages in local DB that are flagged for deletion on server?
	 */
	DLLEXPORT bool hasDeletedMessages();

	/**
	 * Delete messages from local DB based on success of REST API
	 *
	 * Iterate over mMessagesToDelete for:
	 * 	- successful items: Delete them from DB.  Individual DBM calls.
	 *	- failed items:		We leave them flagged as DELETED and try again at a later time.
	*/

	DLLEXPORT void deleteMessages( const DeleteMessageDataList& mMessagesToDelete );

	/**
	* Utility method get RichData object from JSON string so managed code does not have to 
	*	duplicate the JSON parsing
	*/
	DLLEXPORT RichData richDataFromJsonText( const std::string& jsonText );

	/**
	* Retrieve unread item counts for:
	*	- Missed calls
	*	- Messages (IM, Chat, and GM)
	*	- Fax;
	*	- Voicemail
	*/
	DLLEXPORT UnreadCounts getUnreadItemCounts();
	
	/**
	* Retrieve unread item counts for:
	*	- Missed calls
	*	- Messages (IM, Chat, and GM) ( with option to include VM, RC or Fax message counts )
	*	- Fax;
	*	- Voicemail
	*/
	DLLEXPORT UnreadCounts getUnreadItemCounts( bool includeVoicemail, bool includeRecordedCalls, bool includeFaxes );

	//---------------------------------------
	// PRIVATE Messages Methods
	//---------------------------------------

private:
	/**
	 * Description:
	 * 	Update Message in DB with RichData.
	 * if it doesn't exist, throw IllegalArgumentException
	 * */
	void updateMessageRichData( __int64 timeStamp, const RichData& richData, bool broadcast, const std::string& newBody );

	/**
	 * Description:
	 * Update a Message entry with its ContactDid.
	 * This is necessary because we may get Voxox related contact info AFTER a message has been added,
	 * 	such as when we get Message History on reconnect, or Chat message from user not in our AB.
	 *
	 *	Currently, we just update Contact_did for Chat messages.
	 */
	void updateMessageContactInfo( const VoxoxContact& vc );

	/**
	 * Description:
	 * Update a Message entry with its ContactDid.
	 *
	 * Does the actual work.
	 */
	void updateMessageContactInfo( const VoxoxContact& vc, bool doInbound );

	/**
	* Determine if message exists with given MessageID and MessageType.  This combination is unique.
	*/
	bool messageExists( Message::Type msgType, const std::string& msgId );

	/*
	* Helper method to convert MessageList to IN clause with LocalTimestamps
	*/
	std::string makeInClauseFromLocalTimestamp( const MessageList& messages );

	/*
	* Helper method to convert MessageList to IN clause with MsgIds
	*/
	std::string makeInClauseFromMsgId( const MessageList& messages );


	//---------------------------------------
	// PUBLIC Contact/PhoneNumber Methods
	//---------------------------------------
public:
	DLLEXPORT void addAddressBookContacts( const ContactImportList& contacts );		//TODO: May be Contacts/PhoneNumbers

	/**
	* Add or updated DB based on VoxoxContact info.
	*/
	DLLEXPORT void addOrUpdateVoxoxContacts( const VoxoxContactList& vcs);

	/**
	* Add naked numbers to DB based on VoxoxContact info.
	*/
	DLLEXPORT void addNakedNumbers( const VoxoxContactList& vcs);

	/**
	* Update PhoneNumber table with XMPP contact info, based on Voxox PhoneNumber
	* Simply calls singular version below.
	*/
//	DLLEXPORT void addOrUpdateXmppContacts( const VoxoxPhoneNumberList& voxoxPhoneNumbers );

	/**
	 * Description:
	 * Add OR update an XMPP contact to the phone number table. an XMPP contact may not have an AB ContactKey associated with it. it can happen if we
	 *	received a CHAT message from a VoxOx user that is not in our address book. The contact will be added to the phone number table with a unique common group (cm_group).
	 */
	DLLEXPORT void addOrUpdateXmppContact( const std::string& xmppAddress, const std::string& phoneNumber, const std::string& contactKey, const std::string& source );

	/**
	 * Description:
	 * update the presence and the status of an XMPP contact.
	 * status values may be: UNAVAILABLE, AVAILABLE, AVAILABLE_AWAY ...
	 * presence: a status message of the presence update, or null if there is not a status. The status is free-form text describing a user's presence (i.e., "gone to lunch").
	 * */
	DLLEXPORT void updateXmppPresence( const XmppContact& xc );

	/**
	 * Description:
	 * update the new favorite status of a phone number entry.
	 * */
	DLLEXPORT void updatePhoneNumberFavorite( const std::string& phoneNumber, bool newValue );

	/**
	 * Description:
	 * Returns a cursor that is UNION of phone numbers AND GM Groups that match the filter.
	 *
	 * As we group by the ContactKey (we want the contact), we may loss the VoxOx flag of one of the phone numbers
	 * so we added a little trick in the query to count if any VoxOx numbers and added a field to the table called VOXOX_COUNT.
	 *
	 * Note:
	 * it is the responsibility of the calling function to close the cursor.
	 * Used by the CONTACTS tab.
	 * */
	DLLEXPORT Contact     getContactByKey( const std::string& contactKey );	

	DLLEXPORT ContactList getContacts( const std::string& searchFilter,  PhoneNumber::ContactFilter contactsFilter );	

	/**
	* Description: In some cases, a better alternative to getContacts(), 
	*/
	DLLEXPORT PhoneNumberList getContactPhoneNumbers( const std::string& searchFilter,  PhoneNumber::ContactFilter contactsFilter );	

	/**
	 * Description:
	 * */
	DLLEXPORT PhoneNumberList getContactDetailsPhoneNumbersByKey( const std::string& contactKey );

	/**
	 * Description:
	 * TODO currently may return duplicated numbers
	 * */
	DLLEXPORT PhoneNumberList getContactDetailsPhoneNumbersByCmGroup( long cmGroup );

	/**
	 * Description: Return all contacts with all phone numbers, ordered by name,
	*/
	DLLEXPORT PhoneNumberList getAllContactDetailsPhoneNumbers();

	/**
	 * Description:
	 * returns a list of PhoneNumber objects associated with the CmGroup.
	 * */
	DLLEXPORT PhoneNumberList getContactPhoneNumbersListForCmGroup( long cmGroup, bool isVoxox );

	/**
	 * Description: Get phoneNumber object for given phoneNumber string and/or CMGroup
	 * Used in UMW and notification
	 * */
	DLLEXPORT PhoneNumber getPhoneNumber( const std::string& phoneNumberString,  int cmGroup );

	/**
	* Get Voxox userId for given phoneNumber string.
	*/
	DLLEXPORT int getVoxoxUserId( const std::string& phoneNumberString );

	/**
	* Get Voxox userId for given CMGroup.	//TODO: I don't think this logic holds since a CMGroup may have more than one VoxoxID (one contact with multiple Voxox numbers).
	*/
	DLLEXPORT int getVoxoxUserId( int cmGroup);

	/**
	* Get CmGroup for given phoneNumber string.
	*/
	DLLEXPORT int getCmGroup( const std::string& phoneNumberString );

	/**
	* Get DID for given JID
	*/
	DLLEXPORT std::string getDidByJid( const std::string& jid );

	/**
	 * Description:
	 * returns a list of VoxOx (XMPP) names (from CHAT messages) that don't have an associated phone number in the phone number table.
	 * */
	DLLEXPORT StringList getVoxoxUserNamesWithNoAssociatedPhoneNumbers();

	/**
	 * Description:
	 * returns a list of phoneNumbers with invalid CmGroup, indicating we need to check for inNetwork info via API call.
	 * */
	DLLEXPORT StringList getPhoneNumbersWithInvalidCmGroup();

	/**
	* Determine if given XMPP JID is in our DB
	*/
	DLLEXPORT bool isXmppContactInLocalAddressBook( const std::string& jid);

	/**
	* Determine if given phone number is in our DB
	*/
	DLLEXPORT bool phoneNumberExists( const std::string& phoneNumber );

	/**
	* Determine if given phone number is in our DB
	*/
	DLLEXPORT bool isNumberAnExtension( const std::string& phoneNumber );

	/**
	 * returns number of new in-bound messages for a CM Group
	 * This is called in for each Contact in ContactsMessages (or its array/cursor adapter).  So could be called a lot, especially when scrolling.
	 */
	DLLEXPORT int getNewInboundMessageCountForCmGroup(  int cmGroup );

	/**
	* returns number of new in-bound messages for a CM Group with option to include VM, RC or Fax message counts
	*/
	DLLEXPORT int getNewInboundMessageCountForCmGroup(  int cmGroup, bool includeVoicemail, bool includeRecordedCalls, bool includeFaxes );

	/**
	 * returns number of new in-bound messages for a PhoneNumber string or JID.
	 *
	 * (I am not sure but...)This is called in for each Contact in ContactsMessages (or its array/cursor adapter).  So could be called a lot, especially when scrolling.
	 */
	DLLEXPORT int getNewInboundMessageCount( const std::string& phoneNumber,  const std::string& xmppAddress );

	/**
	* returns number of new in-bound messages for a PhoneNumber string or JID with option to include VM, RC or Fax message counts
	*
	*/
	DLLEXPORT int getNewInboundMessageCount( const std::string& phoneNumber,  const std::string& xmppAddress, bool includeVoicemail, bool includeRecordedCalls, bool includeFaxes );

	/**
	* Return list of ContactMessage objects.
	* Used on main Messages window.
	*/
	DLLEXPORT ContactMessageSummaryList getContactsMessageSummary();

	/**
	* Return list of ContactMessage objects with option to include VM, RC or Fax messages
	* Used on main Messages window.
	*/
	DLLEXPORT ContactMessageSummaryList getContactsMessageSummary( bool includeVoicemail, bool includeRecordedCalls, bool includeFaxes );

	/**
	 * returns number of new in-bound messages for a GM GroupId
	 *
	 * This is called in for each Contact in ContactsMessages (or its array/cursor adapter).  So could be called a lot, especially when scrolling.
	 */
	DLLEXPORT int getNewInboundMessageCountForGroupId( const std::string& groupId );

	/**
	* returns number of new in-bound messages for a GM GroupId with option to include VM, RC or Fax message counts
	*
	*/
	DLLEXPORT int getNewInboundMessageCountForGroupId( const std::string& groupId, bool includeVoicemail, bool includeRecordedCalls, bool includeFaxes );

	DLLEXPORT MessageTranslationProperties getTranslationModeProps( const std::string& phoneNumber, const std::string& jid );

	/**
	 * Add translation mode to a contact. What required is wither the phone number of jid.
	 * CONTACT_ID should -NOT- be used for detecting a contact as it may change when user refresh the contacts list.
	 * returns true for success.
	 * */
	DLLEXPORT bool addContactMessageTranslation( const MessageTranslationProperties& mt );

	//---------------------------------------
	// PRIVATE Contact/PhoneNumber Methods
	//---------------------------------------
private:
	/**
	 * Description:
	 * update a phone number entry with its VoxOx user name and flag the phone number as "is VoxOx".
	 * the VoxOx did number will be set to VoxoxNumberType.DID_NUMBER while the VoxOx mobile registration number will be set to VoxoxNumberType.MOBILE_REGISTRATION_NUMBER.
	 * this method DOESN'T update the XMMP status, it may be updated later by one of the XMPP update methods (updateXmppPresence()).
	 */
	void addOrUpdateVoxoxContact( const VoxoxContact& vc);

	/**
	 * Description:
	 * add a phone number entry which is not in AB and is not InNetwork.
	 *	This will reduce number of API calls we make to determine if number is InNetwork or not.
	 */
	void addNakedNumber( const VoxoxContact& vc);

	/*
	 * Return UNION of:
	 * 	- all Phone Numbers for associated CmGroup
	 *  - all GM Groups that have associated voxoxUserIds (plural) as members
	 */
	PhoneNumberList getContactPhoneNumbersList( long cmGroup, bool isVoxox );	

	/**
	* Get compound name.  This may or may not actually return a compound name, but it will always return a name.
	*/
	std::string getCompoundName( const std::string& number );

	//Used only within DBM.  Return Cursor is OK, but let's NOT.
	MessageTranslationProperties getTranslationMode( const std::string& phoneNumber, const std::string& jid );

	//---------------------------------------
	// PUBLIC RecentCall Methods
	//---------------------------------------
public:
	//THIS GROUP WAS DEFINED HIGHER IN THE .H FILE
	/**
	* Add or updated Call to DB.
	*/
	DLLEXPORT bool addOrUpdateCall( const RecentCall& call, bool broadcast );

	/**
	* Add or updated Calls to DB.
	*/
	DLLEXPORT void addOrUpdateCalls( const RecentCallList&  messages );

	/**
	* Add or updated Calls to DB.
	*/
	DLLEXPORT bool callExists( const std::string& uid );

	/**
	* returns the last inserted server_timestamp, so we can query server for newer calls.
	*/
	DLLEXPORT __int64 getLatestCallServerTimestamp();
	//END THIS GROUP WAS DEFINED HIGHER IN THE .H FILE


	/**
	* Add new call to DB. Return DB recordId.
	*	This will be called when an incoming call event is detected or 
	*	user makes an outgoing call.
	*/
	DLLEXPORT int addRecentCall( const std::string& phoneNumber, __int64 startTime, bool isIncoming ); // Call for compatibility
	DLLEXPORT int addRecentCall( const std::string& phoneNumber, __int64 startTime, bool isIncoming, bool isLocal );
	DLLEXPORT int addRecentCall( const std::string& phoneNumber, bool isIncoming, RecentCall::Status status, __int64 startTime );		//For unit tests only.  DO NOT CALL THIS IN APP.

	/**
	* Update call status on Answer, Decline, or Hangup (even for unanswered call)
	*/
	DLLEXPORT void updateRecentCallAnswered  ( int callId );
	DLLEXPORT void updateRecentCallDeclined  ( int callId );
	DLLEXPORT void updateRecentCallTerminated( int callId );

	//Seen flag
	DLLEXPORT bool VoxoxDbManager::updateRecentCallSeen( int callId );


	/**
	* Delete call - TODO: this may have to be 2-phase, like deleting messages is. Depends on sending call history to server.
	*/
	DLLEXPORT int deleteRecentCall( int callId );

	/**
	* Retrieve a single recent call by callId
	*/
	DLLEXPORT RecentCall getRecentCall( int callId );

	//Retrieve a single recent call by UID
	DLLEXPORT RecentCall getRecentCallByUid( const std::string& uid );

	/**
	* Retrieve a list of recent calls by phoneNumber
	*/
	DLLEXPORT RecentCallList getRecentCalls( const std::string& phoneNumber );

	/**
	* Retrieve a list of recent calls by cmGroup
	*/
	DLLEXPORT RecentCallList getRecentCalls( int cmGroup );

	/**
	* Clears all calls marked with "local" flag
	*/
	DLLEXPORT int clearLocalCalls();

private:
	/**
	* Update call status on Answer, Decline, or Hangup (even for unanswered call)
	*/
	void updateRecentCall    ( int callId, RecentCall::Status status );
	void updateRecentCall    ( int callId, int duration );
	bool updateRecentCallSeen( int callId, RecentCall::Seen seen );

	RecentCallList getRecentCalls( const std::string& whereClause, const StringList& selectionArgs );

	long getUnseenCallCount();


//-----------------------------------------------------------------
//PUBLIC Settings methods
//-----------------------------------------------------------------
public:
	DLLEXPORT UserSettings getUserSettings();
	DLLEXPORT bool		   updateUserSettings( UserSettings& src );


//-----------------------------------------------------------------
//PUBLIC App Settings methods - Not user level
//-----------------------------------------------------------------
public:
	DLLEXPORT AppSettings getAppSettings();
	DLLEXPORT bool		  updateAppSettings( AppSettings& src );

//-----------------------------------------------------------------
//TODO: Message broadcast for DB changes
//-----------------------------------------------------------------
private:
	void broadcastMessagesDatasetChanged( Message::Status newStatus );
	void broadcastMessagesAdded( size_t msgCount );

	void broadcastContactsDatasetChanged();
	void broadcastContactsDatasetChanged( int voxoxId );

	void broadcastGroupMessagingDatasetChanged( const std::string& groupId, DbChangeData::Action action );



public:
	DLLEXPORT void broadcastMessagesDatasetChanged();
	DLLEXPORT void broadcastRecentCallDatasetChanged();
	void broadcastGroupMessagingDatasetChanged();			//I want this accessible from GroupMessagingManager, at least until I have time to implement better.


	//---------------------------------------
	// PUBLIC GroupMessaging methods
	//---------------------------------------
//public:
//	//TODO-GM: For Phase 1, we are not using inviteType, announceMembers or confirmation, so we may want to just provide default values.
//	//	TODO-GM: adminNickName *should* be added to Members, but we need to verify.
//	/**
//	 * We sometimes get existing group info from GM API calls.  Since we only store COLOR values locally, we must preserve that field.
//	 */
//	DLLEXPORT void addOrUpdateGroupMessageGroup( const std::string& groupId, const std::string& groupName, GroupMessageGroup::Status status, int inviteType, bool announceMembers, const std::string& confirmationCode );
//
//	/**
//	 * We sometimes get existing group info from GM API calls.  Since we only store COLOR
//	 * 	values locally, we must preserve that field.
//	 */
//	DLLEXPORT void addOrUpdateGroupMessageGroup( const GroupMessageGroup& group );
//
//	/**
//	 * Deleting a GM Group may be triggered by user action or Admin action.  In case of Admin action,
//	 * 	we typically want this user to retain access to messages in thread, group name, etc.
//	 * 	In this case, we 'zombify' the Group by marking it inactive.
//	 */
//	DLLEXPORT void deleteGroupMessageGroup( const std::string& groupId, bool zombify );
//
//	/**
//	 * Simply 'unzombify' a group.  This could happen if user was removed from group Admin, and later re-added to group by group Admin.
//	 */
//	DLLEXPORT void activateGroupMessageGroup( const std::string& groupId );
//
//	/**
//	 * Simply 'zombify' a group.  This could happen if user was removed from group Admin, or user removed self from group.
//	 * 	Allows group messages to be retained, but the message thread is now zombified, which limits user interaction with group.
//	 */
//	DLLEXPORT void deactivateGroupMessageGroup( const std::string& groupId );
//
//	DLLEXPORT void renameGroupMessageGroup( const std::string& groupId, const std::string& newGroupName );
//
//	/**
//	 * Does specified group have specified userId?
//	 */
//	DLLEXPORT bool groupMessageGroupHasUser( const std::string& groupId, int userId );
//
//	//TODO-GM: We may be able to use defaults for some of these parameters for Phase 1.
//	DLLEXPORT void addOrUpdateGroupMessageMember( const std::string& groupId, int userId, const std::string& nickname, bool isAdmin, bool isInvited,
//													bool hasConfirmed, bool optOut, bool notify, bool doBroadcast );
//
//	//TODO-GM: We may be able to use defaults for some of these parameters for Phase 1.
//	DLLEXPORT void addOrUpdateGroupMessageMember( const GroupMessageMember& member, bool doBroadcast );
//
//	/**
//	* Remove member from given GM Group
//	*/
//	DLLEXPORT void removeGroupMessageMember( const std::string& groupId, int userId );
//
//	/**
//	* Set MemberNotify property for given GM Group and UserId.
//	*/
//	DLLEXPORT void setGroupMessageMemberNotify( const std::string& groupId, int userId, bool notify );
//
//	/**
//	* Get get list of local Contacts that are members of a given group
//	* Used to display the membership of a group.
//	*/
//	DLLEXPORT GroupMessageMemberOfList getContactsInGroupMessageGroup( const std::string& groupId, const std::string& userId );
//
//	/**
//	* Get get list of local Contacts that are NOT members of a given group
//	* Used to display contacts that can be added to the group.
//	*/
//	DLLEXPORT ContactList getContactsNotInGroupMessageGroup( const std::string& searchFilter, const std::string& groupId );
//
//	/**
//	* Get UserId of group Admin
//	*/
//	DLLEXPORT int getGroupAdminUserId( const std::string& groupId );
//
//	/**
//	* Get GroupMessageGroup object for a given groupId.
//	*/
//	DLLEXPORT GroupMessageGroup getGroupMessageGroup( const std::string& groupId );
//
//	//Some methods related to GM Chat Bubble colors
//	/**
//	 * Called by DatabaseUpgrader to set initial 'lastColor' for GM Groups.
//	 *
//	 * NOTE: We may get here from DatabaseUpgrader, which already has a 'writable' db, so we cannot use our
//	 * 		 normal DBM.instance().update().  If we do, we get an error.
//	 * 		So we pass in 'db'.  If it is non-null, we use it, otherwise we get our own.
//	 */
//	DLLEXPORT void setGroupLastColorIfNeeded( const std::string& colorName /*, SQLiteDatabase db*/ );
//
//	/**
//	 * Called by GroupMessagingManger to assign GM Member colors, if needed.
//	 *
//	 * We intentionally ignore the order of incoming messages.  IMO, not worth consideration as long as userIds have unique colors.
//	 *
//	 * NOTE: We may get here from DatabaseUpgrader, which already has a 'writable' db, so we cannot use our
//	 * 		 normal DBM.instance().query().  If we do, we get an error.
//	 * 		So we pass in 'db'.  If it is non-null, we use it, otherwise we get our own.
//	 */
//	DLLEXPORT void setGroupMemberColorsIfNeeded( /*SQLiteDatabase db*/ );
//
//	/**
//	 * NOTE: We may get here from DatabaseUpgrader, which already has a 'writable' db, so we cannot use our
//	 * 		 normal DBM.instance().query().  If we do, we get an error.
//	 * 		So we pass in 'db'.  If it is non-null, we use it, otherwise we get our own.
//	 */
//	DLLEXPORT std::string getGroupLastColor( const std::string& groupId /*, SQLiteDatabase db*/ );
//
//	/**
//	 * NOTE: We may get here from DatabaseUpgrader, which already has a 'writable' db, so we cannot use our
//	 * 		 normal DBM.instance().query().  If we do, we get an error.
//	 * 		So we pass in 'db'.  If it is non-null, we use it, otherwise we get our own.
//	 */
//	DLLEXPORT void setGroupLastColor( const std::string& groupId, const std::string& colorName /*, SQLiteDatabase db*/ );
//
//
//	/**
//	 * NOTE: We may get here from DatabaseUpgrader, which already has a 'writable' db, so we cannot use our
//	 * 		 normal DBM.instance().query().  If we do, we get an error.
//	 * 		So we pass in 'db'.  If it is non-null, we use it, otherwise we get our own.
//	 */
//	DLLEXPORT std::string getGroupMemberColor( const std::string& groupId,  int userId /*, SQLiteDatabase db*/ );
//
//	/**
//	 * NOTE: We may get here from DatabaseUpgrader, which already has a 'writable' db, so we cannot use our
//	 * 		 normal DBM.instance().query().  If we do, we get an error.
//	 * 		So we pass in 'db'.  If it is non-null, we use it, otherwise we get our own.
//	 */
//	DLLEXPORT void setGroupMemberColor( const std::string& groupId, int userId, const std::string& colorName /*, SQLiteDatabase db*/ );

	//---------------------------------------
	// PRIVATE GroupMessaging methods
	//---------------------------------------
//private:
//	/**
//	 * Used to determine if GM GroupId already exists, so we can determine need for INSERT or UPDATE.
//	 */
//	bool groupMessageGroupExists( const std::string& groupId );
//
//
//	void updateGroupMessageGroupStatus( const std::string& groupId, GroupMessageGroup::Status status );
//
//	//TODO-GM: setGroupMessageGroupAvatar()
//	//TODO-GM: getGroupMessageGroupAvatar()
//
//	int deleteMessage( const std::string& msgId, Message::Type msgType );

};	//class VoxoxDbManager

