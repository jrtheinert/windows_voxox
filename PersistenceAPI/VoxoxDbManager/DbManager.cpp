/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

#include "DbManager.h"				//See here for details.
#include "Database.h"
#include "DatabaseTables.h"
#include "DataTypes.h"				//TODO: May just need the defined ENUMs. 
#include "DbManagerConstants.h"		//Standard SQL projections, etc.
#include "DbUtils.h"				//Some useful utilities.  TODO: Review and consider just including them here in VoxoxDbManager

//Cursors
#include "Cursors/AppSettingsCursor.h"		//Not part of user DB

#include "Cursors/BuildMessageCursor.h"
#include "Cursors/ContactDetailCursor.h"
#include "Cursors/ContactListCursor.h"
#include "Cursors/ContactMessageSummaryCursor.h"
#include "Cursors/MessageTranslationPropertyCursor.h"
#include "Cursors/PhoneNumberCursor.h"
#include "Cursors/RecentCallCursor.h"
#include "Cursors/UmwMessageCursor.h"
#include "Cursors/UserSettingsCursor.h"


//--------------------------------------------------------------------------------------
// We want std::mutex to synchronize DB calls, but CLI/CLR does not like std::mutex.
//	So we cannot put this in the .h file because .h file is called by the managed code.
//	We are forced to use std::mutex as static variable
//--------------------------------------------------------------------------------------
#include <mutex>

static std::recursive_mutex s_dbMutex;

//-----------------------------------------------------------------------------
//NOTE: We have had issues with DBM calls being made during shutdown which results in
//	crash because memvar is closed/deleted whild call is active.
//To avoid this, we scope lock EVERY method that uses getDb() (or the underlying mDb)
//	Proper implementation will look like this:
//
//	void VoxoxDbManager::myNewMethod()
//	{
//		std::lock_guard<std::recursive_mutex> lock( s_dbMutex );
//
//		if (!isDbValid() )
//			return result;
//
//		... remaining code
//	}
//-----------------------------------------------------------------------------

VoxoxDbManager::VoxoxDbManager( const std::string& dbName, DbType dbType ) : mDbName( dbName ), mDbType( dbType ), mDb( NULL )
{
	create();
}

VoxoxDbManager::~VoxoxDbManager()	  //TODO-DBTEST
{
	delete mDb;
	mDb = NULL;

	//TODO: Close or destroy DB?
}

bool VoxoxDbManager::create()
{
	if ( mDb == NULL )
	{
		mDb	= new Database( mDbName.c_str() );
	}

	bool		doUpgradeCheck = false;
	StringList	tableDml;

	switch ( mDbType )
	{
	case DbType::User:
		tableDml.push_back( DatabaseTables::PhoneNumber::CREATE_TABLE.c_str()			);
		tableDml.push_back( DatabaseTables::Message::CREATE_TABLE.c_str()				);
		tableDml.push_back( DatabaseTables::GroupMessageGroup::CREATE_TABLE.c_str()		);
		tableDml.push_back( DatabaseTables::GroupMessageMember::CREATE_TABLE.c_str()	);
		tableDml.push_back( DatabaseTables::Translation::CREATE_TABLE.c_str()			);
		tableDml.push_back( DatabaseTables::RecentCall::CREATE_TABLE.c_str()			);
		tableDml.push_back( DatabaseTables::Settings::CREATE_TABLE.c_str()				);

		doUpgradeCheck = true;
		break;

	case DbType::App:
		tableDml.push_back( DatabaseTables::AppSettings::CREATE_TABLE.c_str()			);

		doUpgradeCheck = false;		//TODO: Implement proper Upgrade Check for AppSettings.
		break;

	default:
		assert( false );	//New DbType?
	}

	mDb->create( tableDml, doUpgradeCheck );		//Create DB and tables

	return true;
}

//static
bool VoxoxDbManager::checkDatabase( const std::string& dbName )		  //TODO-DBTEST
{
	return Database::checkDatabase( dbName.c_str() );
}

bool VoxoxDbManager::checkDatabase()
{
	return getDb()->checkDatabase();
}

bool VoxoxDbManager::checkTableExists( const std::string& tableName )
{
	return getDb()->checkTableExists( tableName );
}

void VoxoxDbManager::deleteDatabase()		  //TODO-DBTEST
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	getDb()->close();	//TODO: Do we need an actual delete?
	mDb = nullptr;
}

void VoxoxDbManager::setDbChangeCallback( DbChangeCallbackFunc callback )
{
	mBroadcastMgr.setCallback( callback );
}

bool VoxoxDbManager::isDbValid() const
{
	return (mDb != nullptr);
}

void VoxoxDbManager::close()			  //TODO-DBTEST
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	if (!isDbValid() )
		return;

	getDb()->close();
	mDb = nullptr;
}

//These methods help support unit testing.
void VoxoxDbManager::clearMessageTable()
{ 
	getDb()->clearMessageTable();		
}

void VoxoxDbManager::clearPhoneNumberTable()
{ 
	getDb()->clearPhoneNumberTable();	
}


//-----------------------------------------------------------------------------
//Contact Import related methods
//-----------------------------------------------------------------------------

void VoxoxDbManager::addAllContacts( const ContactImportList& contacts ) 
{
	// verify that we start from an empty phone number table, delete all rows.
	getDb()->deleteRecords( DatabaseTables::PhoneNumber::TABLE, std::string(), StringList() );

	getDb()->setInitializingContacts( true );

	if ( contacts.size() > 0 ) 
	{
		StringList	fields = {	
								DatabaseTables::PhoneNumber::NAME,
								DatabaseTables::PhoneNumber::NUMBER,
								DatabaseTables::PhoneNumber::DISPLAY_NUMBER,
								DatabaseTables::PhoneNumber::LABEL,
								DatabaseTables::PhoneNumber::IS_VOXOX,
								DatabaseTables::PhoneNumber::XMPP_JID,
								DatabaseTables::PhoneNumber::IS_FAVORITE,

								//Added with new Contact Syncing
								DatabaseTables::PhoneNumber::CONTACT_KEY,
								DatabaseTables::PhoneNumber::SOURCE,
								DatabaseTables::PhoneNumber::COMPANY,
								DatabaseTables::PhoneNumber::FIRST_NAME,
								DatabaseTables::PhoneNumber::LAST_NAME,
								DatabaseTables::PhoneNumber::USER_ID,
								DatabaseTables::PhoneNumber::USER_NAME,
								DatabaseTables::PhoneNumber::IS_BLOCKED,
								DatabaseTables::PhoneNumber::IS_EXT,
							};

		//Format the VALUES clause, based on number of entries in 'fields'
		std::string valuesClause = " VALUES ( ";
		for ( size_t x = 0; x < fields.size(); x++ )
		{
			valuesClause += " ?";

			if ( x != fields.size() - 1 )
				valuesClause += ",";
		}
		valuesClause += " )";

		std::string			 insertSql = "INSERT OR IGNORE INTO " + DatabaseTables::PhoneNumber::TABLE + " "
										 "( " + DbUtils::stringListToString( fields, ", " ) + ") " +
										 valuesClause;

		VoxoxSQLiteStatement statement = getDb()->compileStatement( insertSql.c_str() );

		std::string phoneLabel;
		std::string	displayNumber;
		std::string displayName;
		std::string number;
		std::string xmppJid;
		int         isVoxox     = 0;
		int			isFavorite  = 0;
		int			isExtension = 0;

		getDb()->beginTransaction();

		for ( ContactImport contactImport : contacts )
		{
			phoneLabel	  = contactImport.getPhoneLabel();
			displayNumber = contactImport.getPhoneNumber();
			displayName	  = contactImport.getName();
			number		  = DbUtils::getNormalizedNumberNoPrefix( displayNumber );
			xmppJid       = contactImport.getJid();
			isVoxox       = !contactImport.getJid().empty();
			isFavorite	  = contactImport.getIsFavorite();
			isExtension   = contactImport.getIsExtension();

			//Detect if phoneNumber is an Extension
			if ( isVoxox )
			{
				if ( DbUtils::isValidExtensionLength( displayNumber.length() ) )
				{
					isExtension = 1;
				}
			}

			//Verify that we don't have an empty strings for both the phone number and the display name!
			if (  (! displayNumber.empty() ) || ( ! displayName.empty() ) ) 
			{
				int index = 0;

				//Save data to database
				try 
				{
					statement.bind( ++index, displayName.c_str()	);
					statement.bind( ++index, number.c_str()			);		// number used to optimize database queries
					statement.bind( ++index, displayNumber.c_str()	);		// display_number (as appears in the native database)
					statement.bind( ++index, phoneLabel.c_str()		);

					statement.bind( ++index, isVoxox				);
					statement.bind( ++index, xmppJid.c_str()		);
					statement.bind( ++index, isFavorite				);

					//New contact syncing
					statement.bind( ++index, contactImport.getContactKey().c_str()	);
					statement.bind( ++index, contactImport.getSource().c_str()		);
					statement.bind( ++index, contactImport.getCompany().c_str()		);
					statement.bind( ++index, contactImport.getFirstName().c_str()	);
					statement.bind( ++index, contactImport.getLastName().c_str()	);
					statement.bind( ++index, contactImport.getUserId()				);
					statement.bind( ++index, contactImport.getUserName().c_str()	);
					statement.bind( ++index, contactImport.getIsBlocked()			);
					statement.bind( ++index, isExtension							);

					statement.execDml();
				} 
					
				catch ( VoxoxSQLiteException e)		// if you get an exception with one number, just continue with the rest
				{
					//TODO-LOG
				}
			}
		}

		getDb()->commitTransaction();

		statement.close();
	}

	getDb()->setInitializingContacts( false );
}

//JRT - 2015.10.29 - Because we now have 'contact_key' when syncing with server, we will assign our own CMGROUP.
//		This is now a two step process:
//		- Assign CMGROUP by grouping by ContactKey
//		- Re-assign CMGROUP for Phone Numbers that occur more than once (the current logic).
void VoxoxDbManager::assignContactMessageGroup( bool doStep2 ) 
{
	getDb()->beginTransaction();

	assignContactMessageGroup( 1 );

	if ( doStep2 )
		assignContactMessageGroup( 2 );

	getDb()->commitTransaction();
}

void VoxoxDbManager::assignContactMessageGroup( int step ) 
{
	//Step = 1: We are assigning based on ContactKey
	//Step = 2: We are re-assigning based on numbers occurring more than once. 
	//			We will also update the Name to be a Compound Name

	//Let's pre-define SQL for easier review
	std::string step1Query = "SELECT contact_key, count(*) as count1 " 
							 "FROM phone_number pn " 
							 "WHERE cm_group = -1 "
							 "GROUP BY contact_key ";

	std::string step2Query = "SELECT number, count(*) as count1 " 
							 "FROM phone_number pn " 
							 "GROUP BY number " 
							 "HAVING count(*) > 1 ";

	std::string step1Update = "UPDATE " + DatabaseTables::PhoneNumber::TABLE + 
							  " SET "   + DatabaseTables::PhoneNumber::CM_GROUP + " = ? " +
							  " WHERE " + DatabaseTables::PhoneNumber::CONTACT_KEY + " = ? ";

	std::string step2Update = "UPDATE " + DatabaseTables::PhoneNumber::TABLE + 
							  " SET "   + DatabaseTables::PhoneNumber::CM_GROUP		 + " = ?, " +
										  DatabaseTables::PhoneNumber::CM_GROUP_NAME + " = ?  " +
							  " WHERE " + DatabaseTables::PhoneNumber::NUMBER		 + " = ?  ";

	std::string step2UpdateName = "UPDATE " + DatabaseTables::PhoneNumber::TABLE + 
							      " SET "   + DatabaseTables::PhoneNumber::NAME + " = ? " +
								  " WHERE " + DatabaseTables::PhoneNumber::CM_GROUP   + " = ? ";

	std::string step1KeyField = DatabaseTables::PhoneNumber::CONTACT_KEY;
	std::string step2KeyField = DatabaseTables::PhoneNumber::NUMBER;

	std::string cmGroupQuery;
	std::string cmGroupUpdate;
	std::string keyField;

	switch ( step )
	{
	case 1:
		cmGroupQuery  = step1Query;
		cmGroupUpdate = step1Update;
		keyField	  = step1KeyField;
		break;

	case 2:
		cmGroupQuery  = step2Query;
		cmGroupUpdate = step2Update;
		keyField	  = step2KeyField;
		break;

	default:
		assert(false);
		break;
	}

	VoxoxSQLiteCursor* cursor = getDb()->rawQuery( cmGroupQuery, StringList() );

	// cm_group is how we handle having same number on multiple contacts. Any such number is assigned its own cm_group.
	if ( cursor != NULL ) 
	{
		std::string			 compoundName;
		int					 keyFieldIndex = cursor->fieldIndex( keyField.c_str() );
		VoxoxSQLiteStatement stmt          = mDb->compileStatement( cmGroupUpdate.c_str() );

		while ( ! cursor->eof() ) 
		{
			int			cmg	= getNextCmGroup();
			std::string key	= cursor->getStringEx( keyFieldIndex, "" );

			try 
			{
				switch ( step )
				{
				case 1:
					stmt.bind( 1, cmg		  );
					stmt.bind( 2, key.c_str() );
					break;

				case 2:
					compoundName = getCompoundName( key );
					stmt.bind( 1, cmg		  );
					stmt.bind( 2, compoundName.c_str() );
					stmt.bind( 3, key.c_str() );
					break;
				}

				stmt.execDml();
			} 
			catch ( VoxoxSQLiteException e ) 
			{
				//TODO-LOG: ?
			}

			cursor->nextRow();
		}

		stmt.close();

		delete cursor;
	}
}

int VoxoxDbManager::getNextCmGroup() 
{
	int result = -1;

	try
	{
		std::string query = "SELECT MAX(" + DatabaseTables::PhoneNumber::CM_GROUP + ") FROM " + DatabaseTables::PhoneNumber::TABLE;
		result = getDb()->getLongForQuery( query, StringList() ) + 1;
	}

	catch (VoxoxSQLiteException e )
	{
		//TODO-LOG?
	}

	return result;
}

bool VoxoxDbManager::cmGroupIsValid( int value )
{
	return (value > 0);
}

//-----------------------------------------------------------------------------
// PUBLIC Message API method
//	- Approx. 20 methods
//-----------------------------------------------------------------------------

__int64 VoxoxDbManager::getLatestMessageServerTimestamp( Message::Type type, Message::Direction direction, Message::Status status )
{
	__int64 result = 0;

	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	if (!isDbValid() )
		return result;

	//Construct the WHERE clause
	std::string selection;

	if ( type != Message::ALL_TYPES )
	{
		selection = DatabaseTables::Message::TYPE + " = " + std::to_string( type );
	}

	if ( direction != Message::ALL_DIR ) 
	{
		if ( !selection.empty() )
			selection += " AND ";

		selection += DatabaseTables::Message::DIRECTION + " = " + std::to_string( direction );
	}

	if ( status != Message::ALL_STATUS ) 
	{
		if ( !selection.empty() )
			selection += " AND ";

		selection += DatabaseTables::Message::STATUS + " = " + std::to_string( status );
	}

	QueryParameters qp;
	qp.setTable( DatabaseTables::Message::TABLE );
	qp.addProjection( DatabaseTables::Message::SERVER_TIMESTAMP );
	qp.setSelection ( selection );
	qp.setOrderBy   ( DatabaseTables::Message::SERVER_TIMESTAMP + " DESC" );
	qp.setLimit		( 1 );

	try 
	{
		result = getDb()->getInt64ForQuery( qp );
	} 
	
	catch( VoxoxSQLiteException e )
	{
		int xxx = 1;		//TODO: On exception
	}

	return result;
}

Message VoxoxDbManager::getMessage( __int64 timestamp )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	Message message;

	if (!isDbValid() )
		return message;

	QueryParameters qp;
	qp.setTable     ( DatabaseTables::Message::TABLE );
	qp.setProjection( DbManagerConstants::getGetMessagesProjection() );
	qp.setSelection ( DatabaseTables::Message::LOCAL_TIMESTAMP + " = ?" );
	qp.addSelectionArg( std::to_string( timestamp ) );
	qp.setLimit( 1 );	

	try 
	{
		BuildMessageCursor* cursor = new BuildMessageCursor( getDb()->query( qp ) );

		if ( cursor ) 
		{
			MessageList list = cursor->getData();

			if ( list.size() > 0 )
			{
				message = list.front();		//Should be only one, but just in case, take the first one.
			}

			delete cursor;
		}
	} 
	catch( VoxoxSQLiteException e )
	{
		int xxx = 1;		//TODO: On exception
	}

	return message;
}

//TODO: Appears unused
MessageList VoxoxDbManager::getMessages( Message::Status messageStatus, Message::Direction messageDirection)
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	MessageList messages;

	if (!isDbValid() )
		return messages;

	std::string selection = DatabaseTables::Message::STATUS    + " = ? " + " AND " +
						    DatabaseTables::Message::DIRECTION + " = ? ";
	QueryParameters qp;
	qp.setTable       ( DatabaseTables::Message::TABLE );
	qp.setProjection  ( DbManagerConstants::getGetMessagesProjection() );
	qp.setSelection   ( selection        );
	qp.addSelectionArg( messageStatus    );
	qp.addSelectionArg( messageDirection );
	qp.setOrderBy     ( DatabaseTables::Message::LOCAL_TIMESTAMP + " ASC" );

	try 
	{
		BuildMessageCursor* cursor = new BuildMessageCursor( getDb()->query( qp ) );

		if ( cursor ) 
		{
			messages = cursor->getData();

			delete cursor;
		}
	}

	catch( VoxoxSQLiteException e )
	{
		int xxx = 1;		//TODO: On exception
	}

	return messages;
}

UmwMessageList VoxoxDbManager::getVmAndRecordedCallMessages()
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	UmwMessageList messages;

	if (!isDbValid() )
		return messages;

	std::string selection = DatabaseTables::Message::TYPE    + " IN ( ?, ? ) " + 
							" AND " +
							DbManagerConstants::getNoDeletedMessagesCondition();

	QueryParameters qp;
	qp.setTable       ( DatabaseTables::Message::TABLE );
	qp.setProjection  ( DbManagerConstants::getGetMessagesProjection() );
	qp.setSelection   ( selection        );
	qp.addSelectionArg( Message::Type::VOICEMAIL   );
	qp.addSelectionArg( Message::Type::RECORDCALLS );
	qp.setOrderBy     ( DatabaseTables::Message::SERVER_TIMESTAMP + " ASC" );

	try 
	{
		UmwMessageCursor* cursor = new UmwMessageCursor( getDb()->query( qp ) );

		if ( cursor ) 
		{
			messages = cursor->getData();

			delete cursor;
		}
	}

	catch( VoxoxSQLiteException e )
	{
		int xxx = 1;		//TODO: On exception
	}

	return messages;
}

UmwMessageList VoxoxDbManager::getFaxMessages()
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	UmwMessageList messages;

	if (!isDbValid() )
		return messages;

	std::string selection = DatabaseTables::Message::TYPE    + " = ? " + 
							" AND " +
							DbManagerConstants::getNoDeletedMessagesCondition();

	QueryParameters qp;
	qp.setTable       ( DatabaseTables::Message::TABLE );
	qp.setProjection  ( DbManagerConstants::getGetMessagesProjection() );
	qp.setSelection   ( selection        );
	qp.addSelectionArg( Message::Type::FAX   );
	qp.setOrderBy     ( DatabaseTables::Message::SERVER_TIMESTAMP + " ASC" );

	try 
	{
		UmwMessageCursor* cursor = new UmwMessageCursor( getDb()->query( qp ) );

		if ( cursor ) 
		{
			messages = cursor->getData();

			delete cursor;
		}
	}

	catch( VoxoxSQLiteException e )
	{
		int xxx = 1;		//TODO: On exception
	}

	return messages;
}

UmwMessageList VoxoxDbManager::getMessagesForPhoneNumber( const std::string& did )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	UmwMessageList messages;

	if (!isDbValid() )
		return messages;

	bool		didIsValid = true;
	std::string contactDid = DbUtils::getNormalizedNumberNoPrefix(did);

	if (contactDid.length() < 3 )	//DatabaseTables::MESSAGE_CONTACT_DID_MIN_LENGTH)  TODO
	{
		if (did.length() > 1) // contact_did must not be an empty string (search query may break)
			contactDid = did;
		else
			didIsValid = false;
	}

	if ( didIsValid )
	{
		std::string selection = DatabaseTables::Message::CONTACT_DID    + " = ? " + 
								" AND " +
								DbManagerConstants::getNoDeletedMessagesCondition();

		QueryParameters qp;
		qp.setTable       ( DatabaseTables::Message::TABLE );
		qp.setProjection  ( DbManagerConstants::getGetMessagesProjection() );
		qp.setSelection   ( selection        );
		qp.addSelectionArg( contactDid    );
		qp.setOrderBy     ( DatabaseTables::Message::SERVER_TIMESTAMP + " ASC" );

		try 
		{
//			BuildMessageCursor* cursor = new BuildMessageCursor( getDb()->query( qp ) );
			UmwMessageCursor* cursor = new UmwMessageCursor( getDb()->query( qp ) );

			if ( cursor ) 
			{
				messages = cursor->getData();

				delete cursor;
			}
		}

		catch( VoxoxSQLiteException e )
		{
			int xxx = 1;		//TODO: On exception
		}
	}

	return messages;
}

UmwMessageList VoxoxDbManager::getMessagesForCmGroup( int cmGroup )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	UmwMessageList messages;

	if (!isDbValid() )
		return messages;

	//NOTE: For field disambiguation, this DbManagerContants call expects table aliases:
	//	Message		as 'm'
	//	PhoneNumber	as 'pn'
	std::string projection = DbUtils::stringListToSelectClause( DbManagerConstants::getGetMessagesForCmGroupProjection() );
	StringList	selectionArgs;
	selectionArgs.push_back( std::to_string( cmGroup ) );

	std::string query = projection +
				"FROM " + DatabaseTables::Message::TABLE + " m " +
				"LEFT JOIN phone_number pn   ON ( m.contact_did IS NOT NULL AND m.contact_did = pn.number ) " +
											// Ensure we included any messages that had a jid match with a contact.
                                 		  	"OR ( m.contact_did IS NULL     AND m.direction = 0 AND m.from_jid LIKE '%' || pn.xmpp_jid || '%' ) " +
                                 		  	"OR ( m.contact_did IS NULL     AND m.direction = 1 AND m.to_jid   LIKE '%' || pn.xmpp_jid || '%' ) " +

				"WHERE pn.cm_group = ? " +
				" AND m." + DbManagerConstants::getNoDeletedMessagesCondition() + " " +
				"GROUP BY m._id " +
				"ORDER BY server_timestamp ASC ";


	try 
	{
		UmwMessageCursor* cursor = new UmwMessageCursor( getDb()->rawQuery( query, selectionArgs ) );;

		if ( cursor ) 
		{
			messages = cursor->getData();

			delete cursor;
		}
	}

	catch( VoxoxSQLiteException e )
	{
		int xxx = 1;		//TODO: On exception
	}

	return messages;
}

UmwMessageList VoxoxDbManager::getMessagesForGroupId( const std::string& groupId )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	UmwMessageList messages;

	if (!isDbValid() )
		return messages;

	//NOTE: For field disambiguation, this DbManagerContants call expects table aliases:
	//	Message			  as 'm'
	//	PhoneNumber		  as 'pn'
	//	GroupMessageGroup as 'gmg'
	std::string projection = DbUtils::stringListToSelectClause( DbManagerConstants::getGetMessagesForGroupIdProjection() );
	StringList	selectionArgs;
	selectionArgs.push_back( groupId );

	std::string query = projection +
						"FROM message m " +
						"LEFT JOIN group_message_group  gmg ON  ( m.to_group_id = gmg.group_id ) "     +										//Gets Group Name (for well messages)
						"LEFT JOIN group_message_member gmm ON  ( m.to_group_id = gmm.group_id AND  m.from_user_id = gmm.user_id  ) "     +		//Gets Group Member nickname
						"LEFT JOIN phone_number         pn  ON m.from_user_id = pn.user_id " +												//Gets contact Name
						"WHERE m.to_group_id = ? " +
						" AND m." + DbManagerConstants::getNoDeletedMessagesCondition() +
						"GROUP BY m._id " +							//This removes dupes that may be caused by same userId (Voxox number) in PN table.
						"ORDER BY m.server_timestamp ASC ";

	try 
	{
		UmwMessageCursor* cursor = new UmwMessageCursor( getDb()->rawQuery( query, selectionArgs ) );;

		if ( cursor ) 
		{
			messages = cursor->getData();

			delete cursor;
		}
	}

	catch( VoxoxSQLiteException e )
	{
		int xxx = 1;		//TODO: On exception
	}

	return messages;
}

void VoxoxDbManager::updateMessageRichData( __int64 timeStamp, const RichData& richData, bool broadcast )		  //TODO-DBTEST
{
	std::string newBody = richData.toJsonString();
	updateMessageRichData( timeStamp, richData, broadcast, newBody );
}

void VoxoxDbManager::updateGroupMessageRichData( __int64 timeStamp, const RichData& richData, bool broadcast, const std::string& origBody )		  //TODO-DBTEST
{
	Message message = getMessage( timeStamp);

	if ( message.isGroupMessage() )
	{
		std::string newBody = message.getUpdatedBody( richData );	//TODO-TEST Phase 2
		updateMessageRichData( timeStamp, richData, broadcast, newBody );
	}
}

void VoxoxDbManager::updateMessage( __int64 timestamp, Message::Status status)
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	if (!isDbValid() )
		return;

	QueryParameters qp;

	qp.setTable		  ( DatabaseTables::Message::TABLE				  );
	qp.addContentValue( DatabaseTables::Message::STATUS, (long)status );
	qp.setSelection   ( DatabaseTables::Message::LOCAL_TIMESTAMP + " = ? "  );
	qp.addSelectionArg( timestamp );

	getDb()->update( qp );

	broadcastMessagesDatasetChanged( status );
}

void VoxoxDbManager::updateMessage( __int64 timestamp, Message::Status status, const std::string& messageId )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	if (!isDbValid() )
		return;

	QueryParameters qp;

	qp.setTable		  ( DatabaseTables::Message::TABLE					  );
	qp.addContentValue( DatabaseTables::Message::STATUS,	 (long)status );
	qp.addContentValue( DatabaseTables::Message::MESSAGE_ID, messageId    );
	qp.setSelection   ( DatabaseTables::Message::LOCAL_TIMESTAMP + " = ? ");
	qp.addSelectionArg( timestamp );

	getDb()->update( qp );

	broadcastMessagesDatasetChanged( status );
}

void VoxoxDbManager::updateMessage( __int64 timestamp, __int64 serverTimestamp, Message::Status status )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	if (!isDbValid() )
		return;

	QueryParameters qp;

	qp.setTable		  ( DatabaseTables::Message::TABLE							   );
	qp.addContentValue( DatabaseTables::Message::STATUS,		   (long)status    );
	qp.addContentValue( DatabaseTables::Message::SERVER_TIMESTAMP, serverTimestamp );
	qp.setSelection   ( DatabaseTables::Message::LOCAL_TIMESTAMP + " = ? "		   );
	qp.addSelectionArg( timestamp );

	getDb()->update( qp );

	broadcastMessagesDatasetChanged();		//TODO: this is different from broadcasts in two previous methods.
}

void VoxoxDbManager::updateMessage( __int64 timestamp, __int64 serverTimestamp, Message::Status status, const std::string& messageId )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	if (!isDbValid() )
		return;

	QueryParameters qp;

	qp.setTable		  ( DatabaseTables::Message::TABLE								);
	qp.addContentValue( DatabaseTables::Message::STATUS,			(long)status    );
	qp.addContentValue( DatabaseTables::Message::SERVER_TIMESTAMP,	serverTimestamp );
	qp.addContentValue( DatabaseTables::Message::MESSAGE_ID,		messageId		);
	qp.setSelection   ( DatabaseTables::Message::LOCAL_TIMESTAMP + " = ? "			);
	qp.addSelectionArg( timestamp );

	getDb()->update( qp );

	broadcastMessagesDatasetChanged();		//TODO: this is different from broadcasts in two previous methods.
}

void VoxoxDbManager::updateTranslatedMessage( __int64 timestamp, const Message& message )		  //TODO-DBTEST
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	if (!isDbValid() )
		return;

	QueryParameters qp;

	qp.setTable		  ( DatabaseTables::Message::TABLE								    );
	qp.addContentValue( DatabaseTables::Message::TRANSLATED, message.getTranslateBody() );
	qp.setSelection   ( DatabaseTables::Message::LOCAL_TIMESTAMP + " = ? "			    );
	qp.addSelectionArg( timestamp );

	getDb()->update( qp );

	broadcastMessagesDatasetChanged();
}

void VoxoxDbManager::updateMessageContactInfo( const VoxoxContactList& vcs )
{
	for ( VoxoxContact vc : vcs) {
		updateMessageContactInfo( vc );
	}

	broadcastContactsDatasetChanged();
}

void VoxoxDbManager::markMessageDelivered( __int64 timestamp, bool broadcast )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	if (!isDbValid() )
		return;

	__int64 systemTime = DbUtils::getSystemTimeMillis();
	long	status	   = (long) Message::DELIVERED;

	QueryParameters qp;		
	qp.setTable		  ( DatabaseTables::Message::TABLE							 );
	qp.addContentValue( DatabaseTables::Message::STATUS,			  status	 );
	qp.addContentValue( DatabaseTables::Message::DELIVERED_TIMESTAMP, systemTime );
	qp.setSelection   ( DatabaseTables::Message::LOCAL_TIMESTAMP + " = ? "		 );
	qp.addSelectionArg( timestamp );

	getDb()->update( qp );

	if ( broadcast )
	{
		broadcastMessagesDatasetChanged();
	}
}

void VoxoxDbManager::markMessageRead( __int64 timestamp, bool broadcast )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	if (!isDbValid() )
		return;

	__int64 systemTime = DbUtils::getSystemTimeMillis();
	long	status	   = (long) Message::READ;

	QueryParameters qp;		
	qp.setTable		  ( DatabaseTables::Message::TABLE						);
	qp.addContentValue( DatabaseTables::Message::STATUS,		 status		);
	qp.addContentValue( DatabaseTables::Message::READ_TIMESTAMP, systemTime	);
	qp.setSelection   ( DatabaseTables::Message::LOCAL_TIMESTAMP + " = ? "	);
	qp.addSelectionArg( timestamp );

	getDb()->update( qp );

	if ( broadcast )
	{
		broadcastMessagesDatasetChanged();
	}
}

bool VoxoxDbManager::addMessage( const Message& message, bool broadcast )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	if (!isDbValid() )
		return false;

	bool messageAddedToDb = false;

	//TODO-RICHDATA: Short term handling of RichData which is not yet supported
	if ( message.getBody().find( "voxoxrichmessage") != std::string::npos )
	{
		//const_cast<Message&>(message).setBody( "Rich Message (Not Implemented)");
	}

	/* --- prepare the fields parameters to be saved in the database --- */
	std::string mediaFile;
//	std::string thumbnailFile;
	int			msgDuration = -1;
//	RichData	richData = message.getRichData();			//TODO-DB:

	if (  message.isVoicemailEx() )
	{
		mediaFile   = message.getFileLocal();
		msgDuration = message.getDuration();
	} 
//	else if (richData.getMediaFile() != null)		//TODO-RICHDATA
//	{
//		mediaFile = richData.getMediaFile().getAbsolutePath();
//		File file = richData.getThumbnailFile();
//		thumbnailFile = file!=null ? file.getAbsolutePath() : null;
//	}

	// We do this check because:
	// - server time-stamps may not match our existing local DB values :
	//  	* server use FULL seconds, not milliseconds.
	//		* server time-stamps may have a difference of a couple of minutes than local time-stamps that are used for CHAT messages (as for CHAT we create our own "server" time-stamp because we use the XMPP server not the REST API server).
	// - server identifies unique messages based on messageId and type
	if ( messageExists( message.getType(), message.getMsgId() ) )
	{
		//TODO: Do we need any more updates?
		QueryParameters qp;

		qp.setTable       ( DatabaseTables::Message::TABLE );
		qp.setSelection   ( DatabaseTables::Message::MESSAGE_ID + " = ? AND " + DatabaseTables::Message::TYPE + " = ? " );
		qp.addSelectionArg( message.getMsgId() );
		qp.addSelectionArg( message.getType()  );

		getDb()->update( qp );
	} 
	else 
	{
		/* --- initialize content values to be saved in the database --- */

		ContentValues values;

		values.put( DatabaseTables::Message::THREAD_ID, 			1);
		values.put( DatabaseTables::Message::MESSAGE_ID, 			message.getMsgId()	);
		values.put( DatabaseTables::Message::DIRECTION,	 			message.getDirection()	);
		values.put( DatabaseTables::Message::TYPE, 					message.getType()		);
		values.put( DatabaseTables::Message::FROM_DID, 				DbUtils::getNormalizedNumber( message.getFromDid() ));
		values.put( DatabaseTables::Message::TO_DID, 				DbUtils::getNormalizedNumber( message.getToDid() 	 ));
		values.put( DatabaseTables::Message::CONTACT_DID, 			DbUtils::getNormalizedNumberNoPrefix( message.getOriginatingDid())); // this contact number is used to optimize database queries
		values.put( DatabaseTables::Message::STATUS, 				message.getStatus()			);
//		values.put( DatabaseTables::Message::READ_TIMESTAMP, 		message.getLocalTimestamp()	);	//TODO: This is likely wrong
		values.put( DatabaseTables::Message::DELIVERED_TIMESTAMP, 	message.getLocalTimestamp()	);
		values.put( DatabaseTables::Message::BODY, 					message.getBody()			);
		values.put( DatabaseTables::Message::TRANSLATED, 			message.getTranslateBody()	);
		values.put( DatabaseTables::Message::LOCAL_TIMESTAMP, 		message.getLocalTimestamp()	);
		values.put( DatabaseTables::Message::SERVER_TIMESTAMP, 		message.getServerTimestamp(	));
		values.put( DatabaseTables::Message::INBOUND_LANG,  		DbUtils::getTranslatorDefaultLocale()	);	// TODO: enter actual language
		values.put( DatabaseTables::Message::OUTBOUND_LANG, 		DbUtils::getTranslatorDefaultLocale()	);	// TODO: enter actual outbound language code

		//RichData - TODO
		values.put( DatabaseTables::Message::FILE_LOCAL, 			mediaFile			);	//VM, Fax
//		values.put( DatabaseTables::Message::THUMBNAIL_LOCAL, 		thumbnailFile		);
		values.put( DatabaseTables::Message::DURATION, 				msgDuration			);	//VM, Fax

		//Group message specific
		if ( message.isGroupMessage() )
		{
			values.put( DatabaseTables::Message::TO_GROUP_ID,		message.getToGroupId()  );
			values.put( DatabaseTables::Message::FROM_USER_ID,		message.getFromUserId() );
		}

		//Chat message specific items
		if ( message.isChat() )
		{
			values.put( DatabaseTables::Message::FROM_JID,			message.getFromJid() );
			values.put( DatabaseTables::Message::TO_JID,			message.getToJid()   );
		}

		//Voice mail message specific items
		if ( message.isVoicemail() )
		{
			values.put( DatabaseTables::Message::CNAME,				message.getCName()    );
			values.put( DatabaseTables::Message::IACCOUNT,			message.getIAccount() );
			values.put( DatabaseTables::Message::MODIFIED,			message.getModified() );
		}

		/* --- insert the new raw --- */
		int count = getDb()->insert(  DatabaseTables::Message::TABLE, values, false );

		messageAddedToDb = (count > 0 );

		//We need to set GM Member chat bubble color, for incoming messages.		//TODO-GM
//		if ( message.isGroupMessage() ) {
//			final Context context = VoxoxApp.getInstance();
//			final GroupMessageMessage gmMsg = (GroupMessageMessage) message;
//			GroupMessagingManager.INSTANCE.setGroupMemberColor(gmMsg, context);
//		}
	}

//	// save the latest server time-stamps of in-bound messages ONLY.
//	// we DO NOT handle server time-stamps for out-bound message here. It is done upon successful delivery where the messages are sent.
//	if ( message.isInbound() ) 
//	{
//		SessionManager.INSTANCE.setLastMessageTimestamp( VoxoxApp.getInstance(), message.getServerTimestamp() );
//	}

	/* --- broadcast database-set change  --- */

	if ( broadcast ) 
	{
		broadcastMessagesAdded( 1 );
	}

	return messageAddedToDb;
}

void VoxoxDbManager::addMessages( const MessageList&  messages )
{
	// a flag to indicate whether it is a new message or just updating an existing message with server time stamp.
	bool messageAddedToDbFlag = false;

	size_t size = messages.size();

	for (Message msg : messages) 
	{
		if ( addMessage( msg, size == 1 ) )	//On size == 1, this broadcasts dataset changed.
		{
			messageAddedToDbFlag = true;
		}
	}

	// NOTE: We only broadcast if size != 1.  That case was handled above.
	// 		In case where size == 0, we broadcast dataset-change so UI can update status (loading -> no messages)
	if ( size == 0) 
	{
		broadcastMessagesAdded( messages.size() );
	} 
	else if ( size > 1 ) 
	{
		broadcastMessagesAdded( messages.size() );
	}
}

int VoxoxDbManager::deleteMessages( const MessageList& messages )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	int result = 0;

	if (!isDbValid() )
		return result;


	if ( messages.size() > 0 ) 
	{
		//Put all timestamps in a IN list.
		std::string selection = makeInClauseFromLocalTimestamp( messages );

		//We simply set the STATUS == DELETED.  A separate REST API is made to delete them from SERVER, and then we will delete from local DB.
		//	Queries have been modified to ignore message.status == DELETED (9)
		QueryParameters qp;

		qp.setTable		  ( DatabaseTables::Message::TABLE );
		qp.addContentValue( DatabaseTables::Message::STATUS, (long)Message::DELETED );
		qp.setSelection   ( selection );

		result = getDb()->update( qp );

		// broadcast changes to the database
		broadcastMessagesDatasetChanged();
	}

	return result;
}

int VoxoxDbManager::removeDeletedMessages( const MessageList& messages )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	int result = 0;

	if (!isDbValid() )
		return result;

	if ( messages.size() > 0 ) 
	{
		//Put all timestamps in a IN list.
//		std::string whereClause = makeInClauseFromLocalTimestamp( messages );
		std::string whereClause = makeInClauseFromMsgId( messages );
		StringList args;

		result = getDb()->deleteRecords( DatabaseTables::Message::TABLE, whereClause, args );

		if ( result > 0 )
		{
			// broadcast changes to the database
			broadcastMessagesDatasetChanged();
		}
	}

	return result;
}

DeleteMessageDataList VoxoxDbManager::getMessageDataForDeletedMessages()
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	DeleteMessageDataList result;

	if (!isDbValid() )
		return result;

	//Get a list of messages that are Status == DELETED using ad hoc query (No defined cursor class)
	QueryParameters qp;

	qp.setTable       ( DatabaseTables::Message::TABLE			  );
	qp.addProjection  ( DatabaseTables::Message::MESSAGE_ID		  );
	qp.addProjection  ( DatabaseTables::Message::TYPE			  );
	qp.addProjection  ( DatabaseTables::Message::STATUS			  );
	qp.setSelection   ( DatabaseTables::Message::STATUS + " = ? " );
	qp.addSelectionArg( Message::DELETED );

	VoxoxSQLiteCursor* cursor = getDb()->query( qp );

	if ( cursor != NULL ) 
	{
		int idIndex     = cursor->fieldIndex( DatabaseTables::Message::MESSAGE_ID.c_str() );
		int typeIndex   = cursor->fieldIndex( DatabaseTables::Message::TYPE.c_str()		  );
		int statusIndex = cursor->fieldIndex( DatabaseTables::Message::STATUS.c_str()	  );

		std::string		msgId;
		Message::Type   msgType;
		Message::Status msgStatus;

		while ( !cursor->eof() ) 
		{
			msgId     = cursor->getStringEx( idIndex );
			msgType   = (Message::Type  )( cursor->getIntEx( typeIndex,   0 ) );
			msgStatus = (Message::Status)( cursor->getIntEx( statusIndex, 0 ) );

			if ( !msgId.empty() ) 
			{
				DeleteMessageData msgData( msgId.c_str(), msgType, msgStatus );

				result.push_back( msgData );
			}

			cursor->nextRow();
		}

		delete cursor;
	}

	return result;
}

bool VoxoxDbManager::hasDeletedMessages()	//TODO-TEST
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	bool result = false;

	if (!isDbValid() )
		return result;

	QueryParameters qp;
	qp.setTable		  ( DatabaseTables::Message::TABLE );
	qp.addProjection  ( "COUNT()" );
	qp.setSelection	  ( DatabaseTables::Message::STATUS + " = ? " );
	qp.addSelectionArg( Message::DELETED );

	int count = getDb()->getLongForQuery( qp );

	result = count > 0;

	return result;
}

bool VoxoxDbManager::hasMessageId( const std::string& msgId )	//TODO-TEST
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	bool result = false;

	if (!isDbValid() )
		return result;

	if ( !msgId.empty() )
	{
		QueryParameters qp;
		qp.setTable		  ( DatabaseTables::Message::TABLE );
		qp.addProjection  ( "COUNT()" );
		qp.setSelection	  ( DatabaseTables::Message::MESSAGE_ID + " = ? " );
		qp.addSelectionArg( msgId   );

		int count = getDb()->getLongForQuery( qp );

		result = count > 0;
	}

	return result;
}

__int64 VoxoxDbManager::getLocalTimestampForMsgId( const std::string& msgId )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	__int64 result = 0;

	if (!isDbValid() )
		return result;

	if ( !msgId.empty() )
	{
		QueryParameters qp;
		qp.setTable		  ( DatabaseTables::Message::TABLE );
		qp.addProjection  ( DatabaseTables::Message::LOCAL_TIMESTAMP );
		qp.setSelection	  ( DatabaseTables::Message::MESSAGE_ID + " = ? " );
		qp.addSelectionArg( msgId   );

		result = getDb()->getInt64ForQuery( qp );
	}

	return result;
}

RichData VoxoxDbManager::richDataFromJsonText( const std::string& jsonText )
{
	RichData result = RichData::fromJsonText( jsonText );
	return result;
}

UnreadCounts VoxoxDbManager::getUnreadItemCounts()
{
	return getUnreadItemCounts(true, true, true);
}

UnreadCounts VoxoxDbManager::getUnreadItemCounts( bool includeVoicemail, bool includeRecordedCalls, bool includeFaxes )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	UnreadCounts result;

	if (!isDbValid() )
		return result;

	StringList selectionArgs;

	std::string	query = "SELECT " + DatabaseTables::Message::TYPE + ", COUNT() as Cnt " +
						" FROM " + DatabaseTables::Message::TABLE +
						" WHERE " + DatabaseTables::Message::DIRECTION + " = ? AND " + DatabaseTables::Message::STATUS + " = ? ";
	
	if (includeVoicemail == false)
	{
		query = query + "   AND " + DatabaseTables::Message::TYPE + " !=  " + std::to_string(Message::VOICEMAIL);
	}

	if (includeRecordedCalls == false)
	{
		query = query + "   AND " + DatabaseTables::Message::TYPE + " !=  " + std::to_string(Message::RECORDCALLS);
	}

	if (includeFaxes == false)
	{
		query = query + "   AND " + DatabaseTables::Message::TYPE + " !=  " + std::to_string(Message::FAX);
	}

	query = query + " GROUP BY " + DatabaseTables::Message::TYPE;

	selectionArgs.push_back( std::to_string( Message::INBOUND) );
	selectionArgs.push_back( std::to_string( Message::NEW ) );

	VoxoxSQLiteCursor* cursor = getDb()->rawQuery( query, selectionArgs );

	if ( cursor != NULL ) 
	{
		int typeIndex  = cursor->fieldIndex( DatabaseTables::Message::TYPE.c_str() );
		int countIndex = cursor->fieldIndex( "Cnt"	  );

		Message::Type   msgType;
		int				count = 0;

		while ( !cursor->eof() ) 
		{
			msgType = (Message::Type  )( cursor->getIntEx( typeIndex,   0 ) );
			count   = cursor->getIntEx( countIndex, 0 );

			switch( msgType )
			{
				case Message::Type::CHAT:
					result.setChatCount( count );
					break;

				case Message::Type::SMS:
					result.setSmsCount( count );
					break;

				case Message::Type::GROUP_MESSAGE:
					result.setGmCount( count );
					break;

				case Message::Type::FAX:
					result.setFaxCount( count );
					break;

				case Message::Type::RECORDCALLS:
					result.setRecordedCallCount( count );
					break;

				case Message::Type::VOICEMAIL:
					result.setVoicemailCount( count );
					break;

				default:
					assert( false );			//New message type?
			}

			cursor->nextRow();
		}

		delete cursor;
	}

	//Missed calls.
	result.setMissedCallCount( getUnseenCallCount() );

	return result;
}

long VoxoxDbManager::getUnseenCallCount()
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	long result = 0;

	if (!isDbValid() )
		return result;

	QueryParameters qp;
	qp.setTable		  ( DatabaseTables::RecentCall::TABLE );
	qp.addProjection  ( "COUNT() as Cnt" );
	qp.setSelection	  ( DatabaseTables::RecentCall::INCOMING + " = ? AND " + DatabaseTables::RecentCall::SEEN + " = ? " );
	qp.addSelectionArg( 1 );	//Incoming
	qp.addSelectionArg( RecentCall::NOT_SEEN );

	result = getDb()->getLongForQuery( qp );

	return result;
}

//-----------------------------------------------------------------------------
// PRIVATE Message API method
//	- Approx. 6 methods
//-----------------------------------------------------------------------------

void VoxoxDbManager::updateMessageRichData( __int64 timestamp, const RichData& richData, bool broadcast, const std::string& newBody )		  //TODO-DBTEST
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	if (!isDbValid() )
		return;

	QueryParameters qp;

	qp.setTable       ( DatabaseTables::Message::TABLE );
//	qp.addContentValue( DatabaseTables::Message::FILE_LOCAL,	  (richData.hasMediaFile()	   ? richData.getMediaFile().getAbsolutePath()     : "" ) );	//TODO
//	qp.addContentValue( DatabaseTables::Message::THUMBNAIL_LOCAL, (richData.hasThumbnailFile() ? richData.getThumbnailFile().getAbsolutePath() : "" ) );	//TODO
	qp.addContentValue( DatabaseTables::Message::BODY, newBody );
	qp.setSelection   ( DatabaseTables::Message::LOCAL_TIMESTAMP + " = ? " );
	qp.addSelectionArg( timestamp );

	getDb()->update( qp );

	if (broadcast)
		broadcastMessagesDatasetChanged();
}

bool VoxoxDbManager::messageExists( Message::Type msgType, const std::string& msgId )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	bool result = false;

	if (!isDbValid() )
		return result;

	if ( !msgId.empty() )
	{
		QueryParameters qp;
		qp.setTable		  ( DatabaseTables::Message::TABLE );
		qp.addProjection  ( "COUNT()" );
		qp.setSelection	  ( DatabaseTables::Message::MESSAGE_ID + " = ? AND " + DatabaseTables::Message::TYPE  + " = ? " );
		qp.addSelectionArg( msgType );
		qp.addSelectionArg( msgId   );

		int count = getDb()->getLongForQuery( qp );

		result = count > 0;
	}

	return result;
}

void VoxoxDbManager::updateMessageContactInfo( const VoxoxContact& vc )
{
	updateMessageContactInfo( vc, true  );	//In-bound
	updateMessageContactInfo( vc, false );	//Out-bound
}

void VoxoxDbManager::updateMessageContactInfo( const VoxoxContact& vc, bool doInbound )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	if (!isDbValid() )
		return;

	std::string contactJid		= DbUtils::ensureFullJid			  ( vc.getUserName() );
	std::string contactDid		= DbUtils::getNormalizedNumberNoPrefix( vc.getDid()		 );
	std::string did				= DbUtils::getNormalizedNumber        ( vc.getDid()		 );
	std::string queryJid		= "%" + contactJid + "%";		//Removed single-quotes from this string.  It made LIKE not work.

	if ( !contactDid.empty() && !contactJid.empty() ) 
	{
		QueryParameters qp;
		qp.setTable		  ( DatabaseTables::Message::TABLE					   );
		qp.addContentValue( DatabaseTables::Message::CONTACT_DID,	contactDid );

		if ( doInbound )
			qp.addContentValue( DatabaseTables::Message::FROM_DID,	did );
		else
			qp.addContentValue( DatabaseTables::Message::TO_DID,	did );

		std::string selection =	DatabaseTables::Message::TYPE      + " = ? AND " + 
								DatabaseTables::Message::DIRECTION + " = ? AND " + 
								DatabaseTables::Message::FROM_JID  + " LIKE ? ";

		qp.setSelection   ( selection		);
		qp.addSelectionArg( Message::CHAT	);
		qp.addSelectionArg( doInbound ?  Message::INBOUND :  Message::OUTBOUND );
		qp.addSelectionArg( queryJid		);

		int count = getDb()->update( qp );
	}
}

std::string VoxoxDbManager::makeInClauseFromLocalTimestamp( const MessageList& messages )
{
	std::string result = "";

	if ( messages.size() > 0 )
	{
		//Put all timestamps in a IN list.
		int			item = 0;

		result.append( DatabaseTables::Message::LOCAL_TIMESTAMP + " IN ( " );

		for ( Message msg : messages )
		{
			item++;
			result.append( std::to_string( msg.getLocalTimestamp() ) );

			if ( messages.size() != item )
			{
				result.append( ", " );	//Add an "," after each selection (except the last)
			}
		}

		result.append( " ) " );				//Close the list.
	}

	return result;
}

std::string VoxoxDbManager::makeInClauseFromMsgId( const MessageList& messages )
{
	std::string result = "";

	if ( messages.size() > 0 )
	{
		//Put all timestamps in a IN list.
		int			item = 0;

		result.append( DatabaseTables::Message::MESSAGE_ID + " IN ( " );

		for ( Message msg : messages )
		{
			item++;
			result.append( "'" + msg.getMsgId() + "'" );

			if ( messages.size() != item )
			{
				result.append( ", " );	//Add an "," after each selection (except the last)
			}
		}

		result.append( " ) " );				//Close the list.
	}

	return result;
}


//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
// PUBLIC Contact/PhoneNumber API methods
//	- Approx 27 methods
//-----------------------------------------------------------------------------

void VoxoxDbManager::addAddressBookContacts( const ContactImportList& contacts )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	if (!isDbValid() )
		return;

	addAllContacts( contacts );
	assignContactMessageGroup( true );	//Assign a common group for same phone number on multiple contacts.
	broadcastContactsDatasetChanged();
}

void VoxoxDbManager::addOrUpdateVoxoxContacts( const VoxoxContactList& vcs )
{
	for ( VoxoxContact vc : vcs) 
	{
		addOrUpdateVoxoxContact(vc);
	}

	broadcastContactsDatasetChanged();
}

void VoxoxDbManager::addNakedNumbers( const VoxoxContactList& vcs )
{
	for ( VoxoxContact vc : vcs) 
	{
		addNakedNumber(vc);
	}

	assignContactMessageGroup( false );

	broadcastContactsDatasetChanged();
}

//void VoxoxDbManager::addOrUpdateXmppContacts( const VoxoxPhoneNumberList& voxoxPhoneNumbers )
//{
//	if ( ! voxoxPhoneNumbers.empty() ) 
//	{
//		for ( VoxoxPhoneNumber voxoxNumber : voxoxPhoneNumbers )
//		{
//			std::string jid			= voxoxNumber.getJid();
//			std::string did			= voxoxNumber.getDid();
//			std::string contactKey	= voxoxNumber.getContactKey();
//			std::string source		= voxoxNumber.getSource();
//
//			addOrUpdateXmppContact( jid, did, contactKey, source );
//		}
//
//		broadcastContactsDatasetChanged();
//	}
//}

void VoxoxDbManager::addOrUpdateXmppContact( const std::string& jid, const std::string& phoneNumber, const std::string& contactKey, const std::string& source )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	if (!isDbValid() )
		return;

	std::string xmppAddress = DbUtils::ensureFullJid( jid );

	ContentValues phoneValues;

	//Add/update phone_number table
	phoneValues.put( DatabaseTables::PhoneNumber::XMPP_JID,			xmppAddress	);
	phoneValues.put( DatabaseTables::PhoneNumber::DISPLAY_NUMBER, 	phoneNumber	);
	phoneValues.put( DatabaseTables::PhoneNumber::NUMBER,			DbUtils::getNormalizedNumberNoPrefix( phoneNumber ));
	phoneValues.put( DatabaseTables::PhoneNumber::IS_VOXOX,       	PhoneNumber::DID_NUMBER );

	if ( isXmppContactInLocalAddressBook( xmppAddress ) ) 
	{ 
		// update
		QueryParameters qp;

		qp.setTable		   ( DatabaseTables::PhoneNumber::TABLE );
		qp.setContentValues( phoneValues );
		qp.setSelection	   ( DatabaseTables::PhoneNumber::XMPP_JID + " = ? COLLATE NOCASE" );
		qp.addSelectionArg ( xmppAddress );

		getDb()->update( qp );
	}
	else
	{ 
		// add
		//  - as we are adding a new contact that is not the in local address book, its display name is the phone number
		//	- add a common group to the sole entry. Will help us running queries on the database (for now used by get messages summary query).
//		phoneValues.put( DatabaseTables::PhoneNumber::NAME,		phoneNumber);		//We now let UI logic handle this.
		phoneValues.put( DatabaseTables::PhoneNumber::CM_GROUP,		getNextCmGroup() );
		phoneValues.put( DatabaseTables::PhoneNumber::CONTACT_KEY,	contactKey		 );
		phoneValues.put( DatabaseTables::PhoneNumber::SOURCE,		source			 );

		getDb()->insert( DatabaseTables::PhoneNumber::TABLE, phoneValues, true );
	} 
}

void VoxoxDbManager::updateXmppPresence( const XmppContact& xc )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	if (!isDbValid() )
		return;

	if ( !xc.getJid().empty() )
	{
		QueryParameters qp;

		qp.setTable       ( DatabaseTables::PhoneNumber::TABLE );
		qp.addContentValue( DatabaseTables::PhoneNumber::XMPP_STATUS,   xc.getStatus()    );
		qp.addContentValue( DatabaseTables::PhoneNumber::XMPP_PRESENCE, xc.getMsgState()  );
		qp.setSelection   ( DatabaseTables::PhoneNumber::XMPP_JID + " = ? COLLATE NOCASE" );
//		qp.addSelectionArg( xc.getJid() );
		qp.addSelectionArg( DbUtils::ensureFullJid(xc.getJid()) );

		getDb()->update( qp );
	}
}

void VoxoxDbManager::updatePhoneNumberFavorite( const std::string& phoneNumber, bool newValue )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	if (!isDbValid() )
		return;

	QueryParameters qp;
	qp.setTable       ( DatabaseTables::PhoneNumber::TABLE );
	qp.addContentValue( DatabaseTables::PhoneNumber::IS_FAVORITE, newValue );
	qp.setSelection   ( DatabaseTables::PhoneNumber::NUMBER + " = ? " );
	qp.addSelectionArg( phoneNumber );

	int count = getDb()->update( qp );

	broadcastContactsDatasetChanged();
}

Contact VoxoxDbManager::getContactByKey( const std::string& contactKey )
{
	Contact result;
	StringList  selectionArgs;

	std::string query = DbManagerConstants::getPhoneNumberAsContactSelectLong()	+
					" WHERE " + DatabaseTables::PhoneNumber::CONTACT_KEY + " = ? ";

	selectionArgs.push_back( contactKey );

	ContactListCursor* cursor = new ContactListCursor( getDb()->rawQuery( query, selectionArgs ) );

	if ( cursor )
	{
		ContactList list = cursor->getData();

		if ( list.size() > 0 )
		{
			result = list.front();
		}

		delete cursor;
	}

	return result;
}

ContactList VoxoxDbManager::getContacts( const std::string& searchFilter, PhoneNumber::ContactFilter contactsFilter )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	ContactList result;
	StringList  selectionArgs;

	if (!isDbValid() )
		return result;


	// display address book contacts AND/OR Extensions from CloudPhone.
	std::string contactHaving = "HAVING  (" + DbManagerConstants::getSourceInClause() +
								 " OR SUM(" + DatabaseTables::PhoneNumber::IS_EXT + ") > 0 ) ";
	std::string groupBy 	  = "GROUP BY " + DatabaseTables::PhoneNumber::CONTACT_KEY + " ";
//	std::string groupWhereClause;

	switch( contactsFilter )
	{
	case PhoneNumber::CONTACT_FILTER_VOXOX:
		contactHaving += " AND " + DatabaseTables::Contact::VOXOX_COUNT + " > 0 ";
		break;

	case  PhoneNumber::CONTACT_FILTER_FAVORITE:
		// We may have VoxOx DID numbers from various sources that could be favorited.
		//	So we can select the contacts only by the favorite filter
		//	As such, we cannot group by ContactKey so we use CmGroup.
		contactHaving	 	= "HAVING " + DatabaseTables::Contact::FAVORITE_COUNT + " > 0 ";
		break;

	case PhoneNumber::CONTACT_FILTER_COMPANY:
		//ONLY contacts that have extentions.
		contactHaving = "HAVING SUM(" + DatabaseTables::PhoneNumber::IS_EXT + " > 0 ) ";
		break;

	default:
		break;
	}

	if ( !searchFilter.empty() ) 
	{
//		contactSelection += " AND ( lower(name) LIKE ? ";
//		contactSelection += "  OR EXISTS( SELECT null FROM phone_number pn2 WHERE pn2.number LIKE ? AND pn.number = pn2.number ) ) ";
		contactHaving += " AND ( name LIKE ? ";
		contactHaving += "  OR EXISTS( SELECT null FROM phone_number pn2 WHERE pn2.number LIKE ? AND pn.contact_key = pn2.contact_key ) ) ";
//		groupWhereClause = " WHERE  lower(name) LIKE ? ";

		std::string likeArg = "%" + searchFilter + "%";
		selectionArgs.push_back( likeArg );
		selectionArgs.push_back( likeArg );
//		selectionArgs.push_back( likeArg );		//Do once for each clause.  This is correct
	}

	std::string contactsQuery = //"SELECT * FROM ( " +
									DbManagerConstants::getPhoneNumberAsContactSelectLong()	+
//									"WHERE "    + contactSelection + " " +
									groupBy + " " +
									contactHaving + " " +
									"ORDER BY " + DatabaseTables::PhoneNumber::NAME + " COLLATE NOCASE " + 	//So names are alphabetic if more than one contact has them.
								//")"
								"";

	//We want to exclude inactive GM Groups.
	//if ( groupWhereClause.empty() )
	//	groupWhereClause =  "WHERE ";
	//else
	//	groupWhereClause += " AND ";

	//groupWhereClause += DbManagerConstants::getGmGroupActiveClause();

	//JRT - 2015.10.08 - We do not support Group Messaging
	//std::string groupIdQuery = 	"SELECT * FROM ( " +
	//								DbManagerConstants::getGroupMessageGroupAsContactSelectLong() +
	//								groupWhereClause +
	//							") " +
	//							"ORDER BY " + DatabaseTables::PhoneNumber::NAME + " COLLATE NOCASE " 	//So names are alphabetic in ContactList.
	//							;

	std::string query = contactsQuery;

	//We do not need this for now, but will likely need it when we implement Group Messaging.
	//switch( contactsFilter )
	//{
	//case PhoneNumber::CONTACT_FILTER_GROUPS:
	////	query = groupIdQuery;
	//	assert( false );	//Should not get here.
	//	break;

	//case PhoneNumber::CONTACT_FILTER_FAVORITE:
	//	query =	contactsQuery;	//WHERE clause has been modified for this.
	//	break;

	//case PhoneNumber::CONTACT_FILTER_COMPANY:
	//	query =	contactsQuery;	//WHERE clause has been modified for this.
	//	break;

	//default:
	//	query =	contactsQuery;	// + " UNION " +	groupIdQuery;
	//	break;
	//}

	ContactListCursor* cursor = new ContactListCursor( getDb()->rawQuery( query, selectionArgs ) );

	if ( cursor )
	{
		result = cursor->getData();

		delete cursor;
	}

	return result;
}


//NOTE: We want ALL phoneNumbers for a given contact if ANY phone number matches the filter.
//Because this returns Contacts based on PhoneNumber attributes (favorite, in-net, etc.), we use a SubQuery.
PhoneNumberList VoxoxDbManager::getContactPhoneNumbers( const std::string& searchFilter, PhoneNumber::ContactFilter contactsFilter )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	PhoneNumberList result;
	StringList		selectionArgs;
	std::string		subQueryAlias = "pn2.";

	if (!isDbValid() )
		return result;

	// display address book contacts only
	std::string contactWhereClause = subQueryAlias + DbManagerConstants::getSourceInClause();
	std::string groupWhereClause;

	if ( PhoneNumber::CONTACT_FILTER_VOXOX == contactsFilter) 
	{
		contactWhereClause += " AND " + subQueryAlias + DatabaseTables::PhoneNumber::IS_VOXOX + " > 0 ";
	} 
	else if ( PhoneNumber::CONTACT_FILTER_FAVORITE == contactsFilter) 
	{
		// We may have numbers from outside address book. 
		//	So we can select the contacts only by the favorite filter.
		//	As such, we cannot group by ContactKey so we use CmGroup.
		contactWhereClause 	= subQueryAlias + DatabaseTables::PhoneNumber::IS_FAVORITE + " = 1 ";
		//groupBy 			= "GROUP BY " + DatabaseTables::PhoneNumber::CM_GROUP + " ";	//TODO-CM_GROUP: Do we need NUMBER here?  I think not since we do not need to distinguish IS_VOXOX == 1 vs. 2
	}
	else if ( PhoneNumber::CONTACT_FILTER_BLOCKED == contactsFilter) 
	{
		// We may have numbers from outside address book. 
		//	So we can select the contacts only by the blocked filter.
		//	As such, we cannot group by ContactKey so we use CmGroup.
		contactWhereClause 	= subQueryAlias + DatabaseTables::PhoneNumber::IS_BLOCKED + " = 1 ";
		//groupBy 			= "GROUP BY " + DatabaseTables::PhoneNumber::CM_GROUP + " ";	//TODO-CM_GROUP: Do we need NUMBER here?  I think not since we do not need to distinguish IS_VOXOX == 1 vs. 2
	}

	if ( !searchFilter.empty() ) 
	{
		contactWhereClause += " AND lower(name) LIKE ? ";
		groupWhereClause = " WHERE  lower(name) LIKE ? ";

		std::string likeArg = "%" + searchFilter + "%";
		selectionArgs.push_back( likeArg );
		selectionArgs.push_back( likeArg );		//Do once for each clause.  This is correct
	}

	std::string subQuery =	"SELECT " + DatabaseTables::PhoneNumber::CONTACT_KEY + " " +
							"FROM "   + DatabaseTables::PhoneNumber::TABLE + " pn2 " +
							"WHERE "  + contactWhereClause + " ";
	std::string contactsQuery = //"SELECT * FROM ( " +
									DbManagerConstants::getPhoneNumberAsContactSelectLong()	+
									"WHERE " + DatabaseTables::PhoneNumber::CONTACT_KEY + " IN ( " + subQuery + " ) " +
									//groupBy +
									"ORDER BY " + DatabaseTables::PhoneNumber::NAME + " COLLATE NOCASE " + 	//So names are alphabetic if more than one contact has them.
								//")";
								"";

	//We want to exclude inactive GM Groups.
	if ( groupWhereClause.empty() )
		groupWhereClause =  "WHERE ";
	else
		groupWhereClause += " AND ";

	groupWhereClause += DbManagerConstants::getGmGroupActiveClause();

	//JRT - 2015.10.08 - We do not support Group Messaging
	//std::string groupIdQuery = 	"SELECT * FROM ( " +
	//								DbManagerConstants::getGroupMessageGroupAsContactSelectLong() +
	//								groupWhereClause +
	//							") " +
	//							"ORDER BY " + DatabaseTables::PhoneNumber::NAME + " COLLATE NOCASE " 	//So names are alphabetic in ContactList.
	//							;

	std::string query;

	if ( PhoneNumber::CONTACT_FILTER_GROUPS == contactsFilter) 
	{
//		query = groupIdQuery;
		assert( false );	//Should not get here.
	} 
	else if ( PhoneNumber::CONTACT_FILTER_FAVORITE == contactsFilter) 
	{
		query =	contactsQuery;	//WHERE clause has been modified for this.
	} 
	else 
	{
		query =	contactsQuery;	// + " UNION " +	groupIdQuery;
	}

	PhoneNumberCursor* cursor = new PhoneNumberCursor( getDb()->rawQuery( query, selectionArgs ) );

	if ( cursor )
	{
		result = cursor->getData();

		delete cursor;
	}

	return result;
}

PhoneNumberList VoxoxDbManager::getContactDetailsPhoneNumbersByKey( const std::string& contactKey )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	PhoneNumberList result;

	if (!isDbValid() )
		return result;

	std::string contactWhereClause = DatabaseTables::PhoneNumber::CONTACT_KEY + " = \"" + contactKey + "\" ";

	std::string query = "SELECT * FROM ("
							"SELECT * FROM "
							"(" +
								DbManagerConstants::getPhoneNumberAsContactSelectShort() +
								"WHERE " + contactWhereClause + " " +
							")" +

							//JRT - 2015.10.08 - We do not have GroupMessaging yet.
							//" UNION " +

							//"SELECT * FROM " +
							//"(" +
							//	DbManagerConstants::getGroupMessageGroupAsContactSelectShort() +
							//	"JOIN group_message_member gmm ON gmg.group_id = gmm.group_id " +
							//	"WHERE gmm.user_id " + groupWhereClause +
							//" ) " +

							"ORDER BY " + DatabaseTables::PhoneNumber::NAME  + " COLLATE NOCASE ) " +  	//So names are alphabetic in PhoneNumberList.
						"ORDER BY " + DatabaseTables::Contact::TYPE
						;

	//The real work starts here.
	PhoneNumberCursor* cursor = new PhoneNumberCursor( getDb()->rawQuery( query, StringList() ) );

	if ( cursor != NULL )
	{
		result = cursor->getData();
		delete cursor;
	}

	return result;
}

PhoneNumberList VoxoxDbManager::getContactDetailsPhoneNumbersByCmGroup( long cmGroup )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	PhoneNumberList result;

	if (!isDbValid() )
		return result;

	std::string contactWhereClause = DatabaseTables::PhoneNumber::CM_GROUP + " = " + std::to_string( cmGroup );

	std::string query = "SELECT * FROM ("
							"SELECT * FROM "
							"(" +
								DbManagerConstants::getPhoneNumberAsContactSelectShort() +
								"WHERE " + contactWhereClause + " " +
								"GROUP BY " + DatabaseTables::PhoneNumber::NUMBER	+ " " +		//Remove duplicate numbers from CmGroup.
							")" +

							//JRT - 2015.10.08 - We do not have GroupMessaging yet.
							//" UNION " +

							//"SELECT * FROM " +
							//"(" +
							//	DbManagerConstants::getGroupMessageGroupAsContactSelectShort() +
							//	"JOIN group_message_member gmm ON gmg.group_id = gmm.group_id " +
							//	"WHERE gmm.user_id " + groupWhereClause +
							//" ) " +

							"ORDER BY " + DatabaseTables::PhoneNumber::NAME  + " COLLATE NOCASE ) " +  	//So names are alphabetic in PhoneNumberList.
						"ORDER BY " + DatabaseTables::Contact::TYPE
						;

	//The real work starts here.
	PhoneNumberCursor* cursor = new PhoneNumberCursor( getDb()->rawQuery( query, StringList() ) );

	if ( cursor != NULL )
	{
		result = cursor->getData();
		delete cursor;
	}

	return result;
}

PhoneNumberList VoxoxDbManager::getAllContactDetailsPhoneNumbers()
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	PhoneNumberList result;

	if (!isDbValid() )
		return result;

	//If/When we add Group Messaging, we will need to add UNION to SELECT.
	std::string query = "SELECT * FROM ("
							"SELECT * FROM "
							"(" +
								DbManagerConstants::getPhoneNumberAsContactSelectShort() +
							")" +
							"ORDER BY " + DatabaseTables::PhoneNumber::NAME + ", " + DatabaseTables::PhoneNumber::CM_GROUP  + " COLLATE NOCASE ) " +  	//So names are alphabetic in PhoneNumberList.
						"ORDER BY " + DatabaseTables::Contact::TYPE
						;

	//The real work starts here.
	ContactDetailCursor* cursor = new ContactDetailCursor( getDb()->rawQuery( query, StringList() ) );	//TODO: May be wrong cursor

	if ( cursor != NULL )
	{
		result = cursor->getData();
		delete cursor;
	}

	return result;
}

PhoneNumberList VoxoxDbManager::getContactPhoneNumbersListForCmGroup( long cmGroup, bool isVoxox )
{
	return getContactPhoneNumbersList( cmGroup, isVoxox );
}

PhoneNumber VoxoxDbManager::getPhoneNumber( const std::string& phoneNumberString, int cmGroup )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	PhoneNumber result;

	if (!isDbValid() )
		return result;

	std::string normalizedNoPrefix = DbUtils::getNormalizedNumberNoPrefix( phoneNumberString );

	// verify that the phone number string is still valid after normalizing it ...
//	if ( !normalizedNoPrefix.empty() ) 
	{
		int			voxoxUserId			 = getVoxoxUserId( normalizedNoPrefix );
		std::string voxoxUserIdSelection = voxoxUserId > 0 ? " OR " +	DatabaseTables::PhoneNumber::USER_ID + " = ? "/* + voxoxUserId*/ : " ";

		std::string selection = DatabaseTables::PhoneNumber::NUMBER + " = ? " +		//normalizedNoPrefix +
								" OR " +
								DatabaseTables::PhoneNumber::CM_GROUP + " = ? " + //cmGroup +
								voxoxUserIdSelection;

		QueryParameters qp;

		qp.setTable       ( DatabaseTables::PhoneNumber::TABLE );
		qp.setProjection  ( DbManagerConstants::getGetPhoneNumberProjection() );
		qp.setSelection   ( selection );
		qp.addSelectionArg( normalizedNoPrefix );
		qp.addSelectionArg( cmGroup   );

		if ( voxoxUserId > 0 )
			qp.addSelectionArg( voxoxUserId );

		PhoneNumberCursor* cursor = new PhoneNumberCursor( getDb()->query( qp ) );

		if ( cursor != NULL )
		{
			PhoneNumberList pnLlist = cursor->getData();
			delete cursor;

			result = DbUtils::buildPhoneNumber( phoneNumberString, pnLlist );	//Mostly handles compound name, if more than one number (CMGROUP)
		}

		if ( !result.isValid() ) 
		{
			result = DbUtils::buildPhoneNumber( phoneNumberString, PhoneNumberList() );
		}
	}

	return result;
}

int VoxoxDbManager::getVoxoxUserId( const std::string& phoneNumberString )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	int voxoxUserId = -999999;

	if (!isDbValid() )
		return voxoxUserId;

	std::string phoneNumberNormalizedNoPrefix = DbUtils::getNormalizedNumberNoPrefix( phoneNumberString );

	// verify that the phone number string is still valid after normalizing it ...
	if ( ! phoneNumberNormalizedNoPrefix.empty() ) 
	{
		QueryParameters qp;

		qp.setTable       ( DatabaseTables::PhoneNumber::TABLE			  );
		qp.addProjection  ( DatabaseTables::PhoneNumber::USER_ID		  );
		qp.setSelection	  ( DatabaseTables::PhoneNumber::NUMBER + " = ? " );
		qp.addSelectionArg( phoneNumberNormalizedNoPrefix				  );
		qp.setLimit		  ( 1 );

		voxoxUserId = getDb()->getLongForQuery( qp );	//TODO: This will return zero by default.  Do we nee the -999999 above?
	}

	return voxoxUserId;
}

int VoxoxDbManager::getVoxoxUserId( int cmGroup )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	int voxoxUserId = -999999;

	if (!isDbValid() )
		return voxoxUserId;

	// verify that the phone number string is still valid after normalizing it ...
	if ( cmGroupIsValid( cmGroup ) ) 
	{
		QueryParameters qp;

		qp.setTable       ( DatabaseTables::PhoneNumber::TABLE			    );
		qp.addProjection  ( DatabaseTables::PhoneNumber::USER_ID		    );
		qp.setSelection	  ( DatabaseTables::PhoneNumber::CM_GROUP + " = ? " );
		qp.addSelectionArg( cmGroup	);
		qp.setLimit		  ( 1 );

		voxoxUserId = getDb()->getLongForQuery( qp );	//TODO: This will return zero by default.  Do we nee the -999999 above?
	}

	return voxoxUserId;
}

int VoxoxDbManager::getCmGroup( const std::string& phoneNumberString )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	int result = -1;

	if (!isDbValid() )
		return result;

	std::string phoneNumberNormalizedNoPrefix = DbUtils::getNormalizedNumberNoPrefix( phoneNumberString );

	// verify that the phone number string is still valid after normalizing it ...
	if ( ! phoneNumberNormalizedNoPrefix.empty() ) 
	{
		QueryParameters	qp;

		qp.setTable       ( DatabaseTables::PhoneNumber::TABLE    );
		qp.addProjection  ( DatabaseTables::PhoneNumber::CM_GROUP );
		qp.setSelection	  ( DatabaseTables::PhoneNumber::NUMBER + " = ? " );
		qp.addSelectionArg( phoneNumberNormalizedNoPrefix		  );
		qp.setLimit		  ( 1 );

		result = getDb()->getLongForQuery( qp );
	}

	return result;
}

StringList VoxoxDbManager::getVoxoxUserNamesWithNoAssociatedPhoneNumbers()		  //TODO-DBTEST
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	StringList result;

	if (!isDbValid() )
		return result;

	VoxoxSQLiteCursor* cursor = getDb()->rawQuery( DbManagerConstants::getXmppContactsMessagesOnlyRawQuery(), StringList() );

	if ( cursor != NULL )
	{
		// The raw query returns with only one column that is the XMPP address (as it appears in the message)
		int			index = 0;
		std::string jid;
		std::string voxoxName;

		while ( !cursor->eof() ) 
		{
			jid = cursor->getStringEx( index, "");

			//Due to issue in parsing getMessageHistory(), I am seeing some from_jids that are UUIDs and msg type = 2 (IM).
			//	I fixed that issue, but felt it best to add this check here because the result is empty strings in array which
			//	cause bad JSON returned from API, making it look like the API call failed.
			voxoxName = DbUtils::parseName( jid );

			if ( !voxoxName.empty() )
			{
				result.push_back( voxoxName );
			}
		}

		delete cursor;
	}

	return result;
}

StringList VoxoxDbManager::getPhoneNumbersWithInvalidCmGroup()
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	StringList result;

	if (!isDbValid() )
		return result;

	RecentCallList callList = getRecentCalls( -1 );

	result = callList.getUniquePhoneNumbers();

	return result;
}

bool VoxoxDbManager::isXmppContactInLocalAddressBook( const std::string& jid )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	bool result = false;

	if (!isDbValid() )
		return result;

	std::string cleanJid = DbUtils::parseBareAddress( jid );
	cleanJid = DbUtils::ensureFullJid( cleanJid );

	QueryParameters qp;
	
	qp.setTable     ( DatabaseTables::PhoneNumber::TABLE     );
	qp.addProjection( "COUNT()" );
	qp.setSelection ( DatabaseTables::PhoneNumber::IS_VOXOX + " > 0 AND " + DatabaseTables::PhoneNumber::XMPP_JID + " = ? COLLATE NOCASE " );
	qp.addSelectionArg( cleanJid );

	int count = getDb()->getLongForQuery( qp );

	result = (count > 0);

	return result;
}

bool VoxoxDbManager::phoneNumberExists( const std::string& phoneNumber )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	bool result = false;

	if (!isDbValid() )
		return result;

	std::string normalizedNumberNoPrefix = DbUtils::getNormalizedNumberNoPrefix( phoneNumber );

	QueryParameters qp;
	
	qp.setTable     ( DatabaseTables::PhoneNumber::TABLE     );
	qp.addProjection( "COUNT()" );
	qp.setSelection (  DatabaseTables::PhoneNumber::NUMBER + " = ? COLLATE NOCASE " );
	qp.addSelectionArg( normalizedNumberNoPrefix );

	int count = getDb()->getLongForQuery( qp );

	result = (count > 0);

	return result;
}

bool VoxoxDbManager::isNumberAnExtension( const std::string& phoneNumber )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	bool result = false;

	if (!isDbValid() )
		return result;

	std::string normalizedNumberNoPrefix = DbUtils::getNormalizedNumberNoPrefix( phoneNumber );

	QueryParameters qp;
	
	qp.setTable     ( DatabaseTables::PhoneNumber::TABLE     );
	qp.addProjection( "COUNT()" );
	qp.setSelection (  DatabaseTables::PhoneNumber::NUMBER + " = ? AND " +  
					   DatabaseTables::PhoneNumber::IS_EXT + " = 1 " + 
					   " COLLATE NOCASE " );
	qp.addSelectionArg( normalizedNumberNoPrefix );

	int count = getDb()->getLongForQuery( qp );

	result = (count > 0);

	return result;
}

int VoxoxDbManager::getNewInboundMessageCountForCmGroup( int cmGroup )
{
	return getNewInboundMessageCountForCmGroup(cmGroup, true, true, true);
}

int VoxoxDbManager::getNewInboundMessageCountForCmGroup( int cmGroup, bool includeVoicemail, bool includeRecordedCalls, bool includeFaxes )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	if (!isDbValid() )
		return 0;

	std::string query = "SELECT COUNT(DISTINCT m." + DatabaseTables::Message::ID + ") " +		//Use of DISTINCT ensure we do not double count messages that may apply to more than one phone number.
						 DbManagerConstants::getMessageToPhoneNumberJoin()    +					//Standard join of message (m) and phone_number (pn) tables
						" WHERE " + DatabaseTables::PhoneNumber::CM_GROUP	  + " = ? " +
						"   AND " + "m." + DatabaseTables::Message::DIRECTION + " = ? " + 
						"   AND " +	"m." + DatabaseTables::Message::STATUS    + " = ? ";		//Disambiguate with new GM Group status

	if (includeVoicemail == false)
	{
		query = query + "   AND " + "m." + DatabaseTables::Message::TYPE + " !=  " + std::to_string(Message::VOICEMAIL);
	}

//	if (includeRecordedCalls == false)			//RecordedCalls are never considered 'new' since user initiated them
	{
		query = query + "   AND " + "m." + DatabaseTables::Message::TYPE + " !=  " + std::to_string(Message::RECORDCALLS);
	}

	if (includeFaxes == false)
	{
		query = query + "   AND " + "m." + DatabaseTables::Message::TYPE + " !=  " + std::to_string(Message::FAX);
	}

	StringList args;
	args.push_back( std::to_string( cmGroup			 ) );
	args.push_back( std::to_string( Message::INBOUND ) );
	args.push_back( std::to_string( Message::NEW	 ) );

	return getDb()->getLongForQuery( query, args );
}

int VoxoxDbManager::getNewInboundMessageCount( const std::string& phoneNumber, const std::string& xmppAddress )
{
	return getNewInboundMessageCount(phoneNumber, xmppAddress, true, true, true);
}

int VoxoxDbManager::getNewInboundMessageCount( const std::string& phoneNumber, const std::string& xmppAddress, bool includeVoicemail, bool includeRecordedCalls, bool includeFaxes )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	int result = 0;

	if (!isDbValid() )
		return result;

	std::string normalizedNumberNoPrefix = DbUtils::getNormalizedNumberNoPrefix( phoneNumber );
	std::string userName				 = DbUtils::parseName( xmppAddress );
	bool		validPhoneNumber		 = ( ! normalizedNumberNoPrefix.empty() );
	bool		validXmppAddress		 = ( ! xmppAddress.empty() );
	bool		doQuery					 = true;

	std::string query;
	StringList  args;

	if (validPhoneNumber && validXmppAddress) 
	{
		// We have both phone number AND an XMPP address
		query = "SELECT COUNT(*) "
				" FROM "  + DatabaseTables::Message::TABLE  +
				" WHERE " +	"( " + DatabaseTables::Message::CONTACT_DID + " = ? OR " + DatabaseTables::Message::FROM_JID + " LIKE ? ) " + 
				" AND "   + DatabaseTables::Message::DIRECTION	+ " = ? " +
				" AND "   + DatabaseTables::Message::STATUS		+ " = ? ";

		args.push_back( normalizedNumberNoPrefix );
		args.push_back( "%" + userName + "%"     );
		args.push_back( std::to_string( Message::INBOUND ) );
		args.push_back( std::to_string( Message::NEW	 ) );
	} 
	else if ( validPhoneNumber ) 
	{ 
		// phone number only (xmppAddress string is null or 0-length)
		query = "SELECT COUNT(*) "
				" FROM "  + DatabaseTables::Message::TABLE  +
				" WHERE " + DatabaseTables::Message::CONTACT_DID + " = ? " +
				"   AND " + DatabaseTables::Message::DIRECTION   + " = ? " +
				"   AND " + DatabaseTables::Message::STATUS 	 + " = ? ";

		args.push_back( normalizedNumberNoPrefix );
		args.push_back( std::to_string( Message::INBOUND ) );
		args.push_back( std::to_string( Message::NEW	 ) );
	} 
	else if ( validXmppAddress ) 
	{ 
		// XMPP address only (phone number string is null or 0-length)
		query = "SELECT COUNT(*) "
				" FROM "  + DatabaseTables::Message::TABLE  +
				" WHERE " + DatabaseTables::Message::FROM_JID   + " LIKE ? " + 
				"   AND " + DatabaseTables::Message::DIRECTION  + " = ? "	 +
				"   AND " + DatabaseTables::Message::STATUS 	+ " = ? ";

		args.push_back( "%" + userName + "%" );
		args.push_back( std::to_string( Message::INBOUND ) );
		args.push_back( std::to_string( Message::NEW	 ) );
	}
	else
	{
		doQuery = false;
	}

	if ( doQuery )
	{
		if (includeVoicemail == false)
		{
			query = query + "   AND " + DatabaseTables::Message::TYPE + " !=  " + std::to_string(Message::VOICEMAIL);
		}

//		if (includeRecordedCalls == false)		//RecordedCalls are never considered 'new' since user initiated them
		{
			query = query + "   AND " + DatabaseTables::Message::TYPE + " !=  " + std::to_string(Message::RECORDCALLS);
		}

		if (includeFaxes == false)
		{
			query = query + "   AND " + DatabaseTables::Message::TYPE + " !=  " + std::to_string(Message::FAX);
		}

		result = getDb()->getLongForQuery( query, args );
	}

	return result;
}

ContactMessageSummaryList VoxoxDbManager::getContactsMessageSummary()		  //TODO-DBTEST
{
	return getContactsMessageSummary(true, true, true);
}

ContactMessageSummaryList VoxoxDbManager::getContactsMessageSummary( bool includeVoicemail, bool includeRecordedCalls, bool includeFaxes )		  //TODO-DBTEST
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	ContactMessageSummaryList result;

	if (!isDbValid() )
		return result;

	std::string query = DbManagerConstants::getAllContactsMessagesSummaryRawQuery(includeVoicemail, includeRecordedCalls, includeFaxes);

	ContactMessageSummaryCursor* cursor = new ContactMessageSummaryCursor( getDb()->rawQuery( query, StringList() ) );

	if ( cursor != NULL )
	{
		result = cursor->getData();
		delete cursor;
	}

	return result;
}

int VoxoxDbManager::getNewInboundMessageCountForGroupId( const std::string& groupId )
{
	return getNewInboundMessageCountForGroupId(groupId, true, true, true);
}

int VoxoxDbManager::getNewInboundMessageCountForGroupId( const std::string& groupId, bool includeVoicemail, bool includeRecordedCalls, bool includeFaxes )		  //TODO-DBTEST
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	if (!isDbValid() )
		return 0;

	std::string query = "SELECT COUNT(DISTINCT m." + DatabaseTables::Message::ID + " ) " +	//Use of DISTINCT ensures we do not double count messages that may apply to more than on phone number.
				DbManagerConstants::getMessageToPhoneNumberJoin()						 +	//Standard join of message (m) and phone_number (pn) tables
				" WHERE m." + DatabaseTables::Message::TO_DID    + " = ? " + 
				"   AND m." + DatabaseTables::Message::DIRECTION + " = ? " +
				"   AND m." + DatabaseTables::Message::STATUS    + " = ? ";

	if (includeVoicemail == false)
	{
		query = query + "   AND " + "m." + DatabaseTables::Message::TYPE + " !=  " + std::to_string(Message::VOICEMAIL);
	}

	if (includeRecordedCalls == false)
	{
		query = query + "   AND " + "m." + DatabaseTables::Message::TYPE + " !=  " + std::to_string(Message::RECORDCALLS);
	}

	if (includeFaxes == false)
	{
		query = query + "   AND " + "m." + DatabaseTables::Message::TYPE + " !=  " + std::to_string(Message::FAX);
	}

	StringList args;
	args.push_back( groupId );
	args.push_back( std::to_string( Message::INBOUND ) );
	args.push_back( std::to_string( Message::NEW	 ) );

	return getDb()->getLongForQuery( query, args );
}

MessageTranslationProperties VoxoxDbManager::getTranslationModeProps( const std::string& phoneNumber, const std::string& jid )		  //TODO-DBTEST
{
	MessageTranslationProperties result = getTranslationMode( phoneNumber, jid );	//Returns at most ONE MTP.

	return result;
}

bool VoxoxDbManager::addContactMessageTranslation( const MessageTranslationProperties& mt )		  //TODO-DBTEST
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	if (!isDbValid() )
		return false;

	std::string phoneNumber = normalizePhoneNumber( mt.getPhoneNumber() );
	std::string jid			= normalizeJid        ( mt.getJid()		  );
	std::string table		= DatabaseTables::Translation::TABLE;

	ContentValues values;

	values.put( DatabaseTables::Translation::CONTACT_ID, 					mt.getContactId()		);
	values.put( DatabaseTables::Translation::ENABLED, 						mt.isEnabled()			);
	values.put( DatabaseTables::Translation::USER_LANGUAGE, 				mt.getUserLanguage()	);
	values.put( DatabaseTables::Translation::CONTACT_LANGUAGE, 				mt.getContactLanguage()	);
	values.put( DatabaseTables::Translation::PHONE_NUMBER, 					phoneNumber				);
	values.put( DatabaseTables::Translation::JID, 							jid						);
	values.put( DatabaseTables::Translation::TRANSLATE_MESSAGE_DIRECTION, 	mt.getDirection()		);

	int count = 0;

	if (mt.getRecordId() > 0) 
	{
		QueryParameters qp;
		qp.setTable		   ( table );
		qp.setContentValues( values );
		qp.setSelection	   ( DatabaseTables::Translation::ID + " = ? " );
		qp.addSelectionArg ( mt.getRecordId() );

		count = getDb()->update( qp );
	} 
	else 
	{
		count = getDb()->insert( table, values, false );
	}

	return count > 0;
}

//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
// PRIVATE Contact/PhoneNumber API methods
//	- Approx 10 methods
//-----------------------------------------------------------------------------

void VoxoxDbManager::addOrUpdateVoxoxContact( const VoxoxContact& vc )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	if (!isDbValid() )
		return;

	ContentValues phoneValues;
	std::string   mobileRegistrationNumber 			= vc.getRegistrationNumber();
	std::string   voxoxDidNumber	 				= vc.getDid();
	std::string   mobileRegistrationNumberSelection = DatabaseTables::PhoneNumber::NUMBER + " = " + DbUtils::getNormalizedNumberNoPrefix( mobileRegistrationNumber );
	std::string   voxoxDidSelection  				= DatabaseTables::PhoneNumber::NUMBER + " = " + DbUtils::getNormalizedNumberNoPrefix( voxoxDidNumber );
	int			  mobileRegistrationCmGroup 		= getCmGroup( mobileRegistrationNumber );
	int			  voxoxDidCmGroup 					= getCmGroup( voxoxDidNumber );

	// get the user name, may be a compound name if the mobile registration number is part of multiple entries.
	std::string  voxoxUserGivenName					= getCompoundName( DbUtils::getNormalizedNumberNoPrefix( mobileRegistrationNumber ) );
	std::string	 jid								= DbUtils::ensureFullJid( std::to_string( vc.getUserId() ) );
	int			 userId 							= vc.getUserId();
	int			 newCmGroup							= cmGroupIsValid(voxoxDidCmGroup) ? voxoxDidCmGroup : mobileRegistrationCmGroup;

	// We have special case for the 'VoxOx Team' entry.  Here we have DID and Mobile registration number the same.
	// No need to add it again to the database. EVER.
	// but we MUST update its user id, is VoxOx etc ... as it is a VoxOx number ... (known as Oliebird).
	bool voxoxTeamEntry	= ( voxoxDidNumber == mobileRegistrationNumber );
	bool addVoxoxDid    = false;

	if ( !cmGroupIsValid( mobileRegistrationCmGroup ) && !cmGroupIsValid( voxoxDidCmGroup ) )
	{
		addVoxoxDid = true;
	}
	else if ( !mobileRegistrationNumber.empty()  && ! voxoxTeamEntry )	// registration number may be null in the server (if it was reset).
	{
		// A COMMON SCENARIO
		// - we have the contact's mobile registration number in the local database.
		if ( cmGroupIsValid( mobileRegistrationCmGroup ) ) 
		{
			//TODO-LOG

			//Update existing instance of Mobile Registration number, with IsVoxox and UserId
			phoneValues.put( DatabaseTables::PhoneNumber::IS_VOXOX, PhoneNumber::MOBILE_REGISTRATION_NUMBER );
			phoneValues.put( DatabaseTables::PhoneNumber::USER_ID,	userId );

			QueryParameters qp;
			qp.setTable		   ( DatabaseTables::PhoneNumber::TABLE );
			qp.setContentValues( phoneValues );
			qp.setSelection	   ( mobileRegistrationNumberSelection );

//			getDb()->update( DatabaseConstants.PhoneNumbers.TABLE, phoneValues, mobileRegistrationNumberSelection, null );
			getDb()->update( qp );

			//TODO-LOG

			//We may already have the Voxox DID in table, so check for it and then add/update as needed.  (Update is done later in code flow)
			addVoxoxDid = !cmGroupIsValid( voxoxDidCmGroup );
		} 
		else 
		{
			// the mobile registration is not in the database, we got here by sending the VoxOx did number. the REST API should not return the mobile registration. anyhow, we ignore it.
//			if (BuildConstants.ENABLE_LOGGING) Log.d(TAG, "addOrUpdateVoxoxContact() DO NOT ADD MOBILE (mobile is not in address book) : contact = " + vc.getUserName() + ", did = " + vc.getDid() + ", mobile = " + vc.getMobile());
			//TODO-LOG
		}
	}

	if ( addVoxoxDid )
	{
		if ( !cmGroupIsValid( newCmGroup ) )
			newCmGroup = getNextCmGroup();

		//Add Voxox DID.
		//	- Why is name a compound name?  This messes up other places we create compound name
		//	- Why does DidCmGroup take precedence over MobileRegistrationCmGroup?
		//	- If MobReg number exists on more than one contact, this DID should also be added to more than one contact, or
		//		the common group for ALL of them be the same.
		phoneValues.clear();

		phoneValues.put( DatabaseTables::PhoneNumber::CONTACT_KEY,		vc.getContactKey()	);
		phoneValues.put( DatabaseTables::PhoneNumber::SOURCE,			vc.getSource()		);
		phoneValues.put( DatabaseTables::PhoneNumber::IS_VOXOX,			PhoneNumber::DID_NUMBER );
		phoneValues.put( DatabaseTables::PhoneNumber::XMPP_JID,  		jid					);
		phoneValues.put( DatabaseTables::PhoneNumber::USER_ID,			userId				);
		phoneValues.put( DatabaseTables::PhoneNumber::CM_GROUP,			newCmGroup			);
		phoneValues.put( DatabaseTables::PhoneNumber::NUMBER,			DbUtils::getNormalizedNumberNoPrefix(voxoxDidNumber) );	// use for database comparison, normalized with no prefix!
		phoneValues.put( DatabaseTables::PhoneNumber::DISPLAY_NUMBER,	voxoxDidNumber		);
		phoneValues.put( DatabaseTables::PhoneNumber::NAME,				voxoxUserGivenName	);
			
		getDb()->insert( DatabaseTables::PhoneNumber::TABLE, phoneValues, false ); // insert the new VoxOx phone number to the local database.
	}

	// NOT A COMMON SCENARIO
	// - We have the actual VoxOx did number in the local address book. In that case we just update the did number with its VoxOx properties.
	if ( cmGroupIsValid( voxoxDidCmGroup )) 
	{
		phoneValues.clear();

		phoneValues.put( DatabaseTables::PhoneNumber::IS_VOXOX,	PhoneNumber::DID_NUMBER	);
		phoneValues.put( DatabaseTables::PhoneNumber::XMPP_JID, jid						);
		phoneValues.put( DatabaseTables::PhoneNumber::USER_ID,	vc.getUserId()			);

		QueryParameters qp;
		qp.setTable( DatabaseTables::PhoneNumber::TABLE );
		qp.setContentValues( phoneValues );
		qp.setSelection( voxoxDidSelection );

		// the UPDATE affects only those rows for which the result of evaluating the WHERE clause expression as a boolean expression is true.
//		int rowsAffected = getDb()->update( DatabaseTables::PhoneNumber::TABLE, phoneValues, voxoxDidSelection/*WHERE*/, StringList() );
		int rowsAffected = getDb()->update( qp );

//		if (BuildConstants.ENABLE_LOGGING) Log.d(TAG, "addOrUpdateVoxoxContact() UPDATE DID (" + rowsAffected + ") : contact = " + vc.getUserName() + ", did = " + voxoxDidNumber + ", mobile = " + mobileRegistrationNumber);
		//TODO-LOG
	}


}

void VoxoxDbManager::addNakedNumber( const VoxoxContact& vc )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	if (!isDbValid() )
		return;

	std::string	number		  = normalizePhoneNumber( vc.getDid() );
	std::string displayNumber = vc.getDid();

	if ( phoneNumberExists( number ) )
		return;

	ContentValues phoneValues;

	phoneValues.clear();
	phoneValues.put( DatabaseTables::PhoneNumber::CONTACT_KEY,		vc.getContactKey()	);	// used for database comparison, normalized with no prefix!
	phoneValues.put( DatabaseTables::PhoneNumber::NUMBER,			number				);	// used for database comparison, normalized with no prefix!
	phoneValues.put( DatabaseTables::PhoneNumber::DISPLAY_NUMBER,	displayNumber		);
//	phoneValues.put( DatabaseTables::PhoneNumber::NAME,				displayNumber		);	//Not sure about this.
	phoneValues.put( DatabaseTables::PhoneNumber::SOURCE,			vc.getSource()		);
	phoneValues.put( DatabaseTables::PhoneNumber::IS_BLOCKED,		vc.isBlocked()		);
	phoneValues.put( DatabaseTables::PhoneNumber::IS_FAVORITE,		vc.isFavorite()		);
			
	int count = getDb()->insert( DatabaseTables::PhoneNumber::TABLE, phoneValues, false );
	int xxx = 1;
}

PhoneNumberList VoxoxDbManager::getContactPhoneNumbersList( long cmGroup, bool isVoxox )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	PhoneNumberList result;

	if (!isDbValid() )
		return result;

	std::string contactWhereClause = DatabaseTables::PhoneNumber::CM_GROUP + " = " + std::to_string( cmGroup ) + " ";

	if (isVoxox)
		contactWhereClause += " AND " + DatabaseTables::PhoneNumber::IS_VOXOX + " = 1 ";

	std::string query = "SELECT * FROM ("
							"SELECT * FROM "
							"(" +
								DbManagerConstants::getPhoneNumberAsContactSelectShort() +
								"WHERE " + contactWhereClause + " " +
								"GROUP BY " + DatabaseTables::PhoneNumber::NUMBER + " " +				//To remove duplicates from CmGroup.
								"ORDER BY " + DatabaseTables::PhoneNumber::NAME	+ " COLLATE NOCASE " + 	//So names are alphabetic if more than one contact has them.
							")" +

							//JRT - 2015.10.08 - We do not have GroupMessaging yet.
							//" UNION " +

							//"SELECT * FROM " +
							//"(" +
							//	DbManagerConstants::getGroupMessageGroupAsContactSelectShort() +
							//	"JOIN group_message_member gmm ON gmg.group_id = gmm.group_id " +
							//	"WHERE gmm.user_id " + groupWhereClause +
							//" ) " +
			
							"ORDER BY " + DatabaseTables::PhoneNumber::NAME  + " COLLATE NOCASE ) " +  	//So names are alphabetic in ContactList.
						"ORDER BY " + DatabaseTables::Contact::TYPE
						;


	PhoneNumberCursor* cursor = new PhoneNumberCursor( getDb()->rawQuery( query, StringList() ) );

	if ( cursor != NULL )
	{
		result = cursor->getData();
		delete cursor;
	}

	return result;
}

std::string VoxoxDbManager::getCompoundName( const std::string& number )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	std::string result;

	if ( number.empty() )
		return result;

	if (!isDbValid() )
		return result;

	std::string query =	"SELECT "
							"  (SELECT replace(group_concat(distinct name) , \",\", \"" +
								DbUtils::getNameSep() + "\") " +
								"FROM ( " +
										 "SELECT number, name FROM phone_number pn2 " +		//Gets names in alpha order
										 "WHERE pn.number = pn2.number " +
										 "ORDER BY name ASC " +
									 " ) " +
							 " ) AS " + DatabaseTables::Contact::DISPLAY_NAME + " " +
						" FROM phone_number pn " +
						" WHERE number = ? " +
						" LIMIT 1";

	StringList args;
	args.push_back( number );

	result = getDb()->getStringForQuery( query, args );

	return result;
}

std::string VoxoxDbManager::normalizePhoneNumber( const std::string& phoneNumber )
{
	return DbUtils::getNormalizedNumberNoPrefix( phoneNumber );
}

std::string VoxoxDbManager::normalizeJid( const std::string& jid )
{
	return DbUtils::parseBareAddress( jid);
}

MessageTranslationProperties VoxoxDbManager::getTranslationMode( const std::string& phoneNumber, const std::string& jid )		  //TODO-DBTEST
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	MessageTranslationProperties result;

	if (!isDbValid() )
		return result;

	if ( !phoneNumber.empty() || !jid.empty() )
	{
		std::string selection;
		StringList  args;
		int 		index 	= 0;

		if ( !phoneNumber.empty() ) 
		{
			std::string tempNumber = normalizePhoneNumber( phoneNumber );
			selection = DatabaseTables::Translation::PHONE_NUMBER + " = ? ";
			args.push_back( tempNumber );
		}

		if ( !jid.empty() ) 
		{
			if ( args.size() > 0 )
				selection += " OR ";

			std::string tempJid = normalizeJid( jid );
			selection += DatabaseTables::Translation::JID + " = ? ";
			args.push_back( tempJid );
		}

		QueryParameters qp;
		qp.setTable		( DatabaseTables::Translation::TABLE );
		qp.setProjection( DbManagerConstants::getMessageTranslationPropertyProjection() );
		qp.setSelection ( selection );
		qp.setSelectionArgs( args );

		MessageTranslationPropertyCursor* cursor = new MessageTranslationPropertyCursor( getDb()->query( qp ) );

		if ( cursor != NULL )
		{
			MessageTranslationPropertiesList mtpList = cursor->getData();

			if ( mtpList.size() > 0 )
			{
				result = mtpList.front();	//Take the first one.  TODO: Do we care if there are more than one here?
			}

			delete cursor;
		}
	}

	return result;
}

std::string VoxoxDbManager::getDidByJid( const std::string& jidIn )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	std::string result;

	if (!isDbValid() )
		return result;

	if ( !jidIn.empty() ) 
	{
		std::string		jid = DbUtils::ensureFullJid( jidIn );
		QueryParameters qp;

		qp.setTable		  ( DatabaseTables::PhoneNumber::TABLE				);
		qp.addProjection  ( DatabaseTables::PhoneNumber::DISPLAY_NUMBER		);
		qp.setSelection	  ( DatabaseTables::PhoneNumber::XMPP_JID + " = ? COLLATE NOCASE " );
		qp.addSelectionArg( DbUtils::parseBareAddress( jid )				);
		qp.setLimit		  ( 1 );

		result = getDb()->getStringForQuery( qp );
	}

	return result;
}

//-----------------------------------------------------------------



//-----------------------------------------------------------------
// PUBLIC GroupMessaging methods
//	- Approx 24 methods
//-----------------------------------------------------------------

//void VoxoxDbManager::addOrUpdateGroupMessageGroup( const std::string& groupId, const std::string& groupName, GroupMessageGroup::Status status, int inviteType, bool announceMembers, const std::string& confirmationCode )
//{
//}
//
//void VoxoxDbManager::addOrUpdateGroupMessageGroup( const GroupMessageGroup& group )
//{
//}
//
//void VoxoxDbManager::deleteGroupMessageGroup( const std::string& groupId, bool zombify )
//{
//}
//
//void VoxoxDbManager::activateGroupMessageGroup( const std::string& groupId )
//{
//}
//
//void VoxoxDbManager::deactivateGroupMessageGroup( const std::string& groupId )
//{
//}
//
//void VoxoxDbManager::renameGroupMessageGroup( const std::string& groupId, const std::string& newGroupName )
//{
//}
//
//bool VoxoxDbManager::groupMessageGroupHasUser( const std::string& groupId, int userId )
//{
//}
//
//void VoxoxDbManager::addOrUpdateGroupMessageMember( const std::string& groupId, int userId, const std::string& nickname, bool isAdmin, bool isInvited,
//													bool hasConfirmed, bool optOut, bool notify, bool doBroadcast )
//{
//}
//
//void VoxoxDbManager::addOrUpdateGroupMessageMember( const GroupMessageMember& member, bool doBroadcast )
//{
//}
//
//void VoxoxDbManager::removeGroupMessageMember( const std::string& groupId, int userId )
//{
//}
//
//void VoxoxDbManager::setGroupMessageMemberNotify( const std::string& groupId, int userId, bool notify )
//{
//}
//
//GroupMessageMemberOfList VoxoxDbManager::getContactsInGroupMessageGroup( const std::string& groupId, const std::string& userId )
//{
//}
//
//ContactList VoxoxDbManager::getContactsNotInGroupMessageGroup( const std::string& searchFilter, const std::string& groupId )
//{
//}
//
//int VoxoxDbManager::getGroupAdminUserId( const std::string& groupId )
//{
//}
//
//GroupMessageGroup VoxoxDbManager::getGroupMessageGroup( const std::string& groupId )
//{
//}
//
//
////Some methods related to GM Chat Bubble colors
//void VoxoxDbManager::setGroupLastColorIfNeeded( const std::string& colorName /*, SQLiteDatabase db*/ )
//{
//}
//
//void VoxoxDbManager::setGroupMemberColorsIfNeeded( /*SQLiteDatabase db*/ )
//{
//}
//
//std::string VoxoxDbManager::getGroupLastColor( const std::string& groupId /*, SQLiteDatabase db*/ )
//{
//}
//
//void VoxoxDbManager::setGroupLastColor( const std::string& groupId, const std::string& colorName /*, SQLiteDatabase db*/ )
//{
//}
//
//std::string VoxoxDbManager::getGroupMemberColor( const std::string& groupId,  int userId /*, SQLiteDatabase db*/ )
//{
//}
//
//void VoxoxDbManager::setGroupMemberColor( const std::string& groupId, int userId, const std::string& colorName /*, SQLiteDatabase db*/ )
//{
//}


//-----------------------------------------------------------------
// PRIVATE GroupMessaging methods
//	- Approx. 3 methods
//-----------------------------------------------------------------

//bool VoxoxDbManager::groupMessageGroupExists( const std::string& groupId )
//{
//}
//
//void VoxoxDbManager::updateGroupMessageGroupStatus( const std::string& groupId, GroupMessageGroup::Status status )
//{
//}

//-----------------------------------------------------------------


//-----------------------------------------------------------------------------
// PUBLIC RecentCall API methods
//	- Approx xx methods
//-----------------------------------------------------------------------------

bool VoxoxDbManager::addOrUpdateCall( const RecentCall& call, bool broadcast )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	bool addedOrUpdated = false;

	if (!isDbValid() )
		return addedOrUpdated;

	// We do this check because:
	if ( callExists( call.getUid() ) )		//TODO-CALL: Add update logic outside this method (UID, Status, etc.)
	{
		//Update these elements:
		//	- Seen
		if ( call.isIncoming() )
		{
			QueryParameters qp;

			qp.setTable		  ( DatabaseTables::RecentCall::TABLE					  );
			qp.addContentValue( DatabaseTables::RecentCall::SEEN, (long)call.getSeen() );

			qp.setSelection   ( DatabaseTables::RecentCall::UID + " = ? "		  );
			qp.addSelectionArg( call.getUid() );

			int count = getDb()->update( qp );

			addedOrUpdated = (count > 0 );
		}
	} 
	else 
	{
		//Initialize content values to be saved in the database
		ContentValues values;

		values.put( DatabaseTables::RecentCall::UID,				call.getUid()		 	 );
		values.put( DatabaseTables::RecentCall::STATUS, 			call.getStatus()		 );
		values.put( DatabaseTables::RecentCall::INCOMING,	 		call.isIncoming()		 );
		values.put( DatabaseTables::RecentCall::TIMESTAMP, 			call.getTimestamp() 	 );
		values.put( DatabaseTables::RecentCall::START_TIMESTAMP, 	call.getStartTimestamp() );
		values.put( DatabaseTables::RecentCall::DURATION, 			call.getDuration()		 );
		values.put( DatabaseTables::RecentCall::IS_LOCAL,           call.getIsLocal()        );
		values.put( DatabaseTables::RecentCall::SEEN,               call.getSeen()	         );

		values.put( DatabaseTables::RecentCall::PHONE_NUMBER, 		DbUtils::getNormalizedNumberNoPrefix( call.getNumber() ) ); // this contact number is used to optimize database queries
		values.put( DatabaseTables::RecentCall::ORIG_PHONE_NUMBER, 	call.getOrigNumber()			);

		// Insert the new raw
		int count = getDb()->insert(  DatabaseTables::RecentCall::TABLE, values, false );

		addedOrUpdated = (count > 0 );
	}

	// broadcast database-set change
	if ( broadcast ) 
	{
		broadcastMessagesAdded( 1 );

		/* --- notification message --- */

		if ( call.isIncoming() && addedOrUpdated ) 
		{
//			notifyUserCallReceived(call);	//TODO-DB
		}
	}

	return addedOrUpdated;
}

void VoxoxDbManager::addOrUpdateCalls( const RecentCallList& calls )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	if (!isDbValid() )
		return;

	// a flag to indicate whether it is a new call or just updating an existing call with server time stamp.
	bool addedToDbFlag = false;

	size_t size = calls.size();

	for (RecentCall call : calls) 
	{
		if ( addOrUpdateCall( call, size == 1 ) )	//On size == 1, this broadcasts dataset changed.
		{
			addedToDbFlag = true;
		}
	}

	// NOTE: We only broadcast if size != 1.  That case was handled above.
	// 		In case where size == 0, we broadcast dataset-change so UI can update status (loading -> no calls)
	if ( size == 0) 
	{
		broadcastMessagesAdded( calls.size() );
	} 
	else if ( size > 1 ) 
	{
		broadcastMessagesAdded( calls.size() );
	}
}

bool VoxoxDbManager::callExists( const std::string& uid )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	bool result = false;

	if (!isDbValid() )
		return result;

	if ( !uid.empty() )
	{
		QueryParameters qp;
		qp.setTable		  ( DatabaseTables::RecentCall::TABLE );
		qp.addProjection  ( "COUNT()" );
		qp.setSelection	  ( DatabaseTables::RecentCall::UID + " = ? " );
		qp.addSelectionArg( uid );

		int count = getDb()->getLongForQuery( qp );

		result = count > 0;
	}

	return result;
}

__int64 VoxoxDbManager::getLatestCallServerTimestamp()
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	__int64 result = 0;

	if (!isDbValid() )
		return result;

	QueryParameters qp;
	qp.setTable( DatabaseTables::RecentCall::TABLE );
	qp.addProjection( DatabaseTables::RecentCall::START_TIMESTAMP );
	qp.setOrderBy   ( DatabaseTables::RecentCall::START_TIMESTAMP + " DESC" );
	qp.setLimit		( 1 );

	try 
	{
		result = getDb()->getInt64ForQuery( qp );
	} 
	
	catch( VoxoxSQLiteException e )
	{
		int xxx = 1;		//TODO: On exception
	}

	return result;
}

int VoxoxDbManager::addRecentCall( const std::string& phoneNumber, __int64 startTime, bool isIncoming )
{
	return addRecentCall(phoneNumber, startTime, isIncoming, false);
}

int VoxoxDbManager::addRecentCall( const std::string& phoneNumber, __int64 startTime, bool isIncoming, bool isLocal)
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	int			  result = 0;
	ContentValues values;

	if (!isDbValid() )
		return result;

	values.put( DatabaseTables::RecentCall::ORIG_PHONE_NUMBER,	phoneNumber );
	values.put( DatabaseTables::RecentCall::PHONE_NUMBER,		DbUtils::getNormalizedNumberNoPrefix( phoneNumber ) );
	values.put( DatabaseTables::RecentCall::TIMESTAMP, 			startTime   );
	values.put( DatabaseTables::RecentCall::INCOMING, 			isIncoming  );
	values.put( DatabaseTables::RecentCall::STATUS, 			RecentCall::STATUS_NEW );
	values.put( DatabaseTables::RecentCall::IS_LOCAL,           isLocal );

	result = getDb()->insert( DatabaseTables::RecentCall::TABLE, values, true );

	return result;
}

//For unit tests only.  DO NOT CALL THIS IN APP.
int VoxoxDbManager::addRecentCall( const std::string& phoneNumber,bool isIncoming, RecentCall::Status status, __int64 startTime )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	int			  result = 0;
	ContentValues values;

	if (!isDbValid() )
		return result;

	values.put( DatabaseTables::RecentCall::PHONE_NUMBER, DbUtils::getNormalizedNumberNoPrefix( phoneNumber ) );
	values.put( DatabaseTables::RecentCall::TIMESTAMP, 	  startTime  );
	values.put( DatabaseTables::RecentCall::INCOMING, 	  isIncoming );
	values.put( DatabaseTables::RecentCall::STATUS, 	  status     );

	if ( status == RecentCall::STATUS_ANSWERED )
	{
		values.put( DatabaseTables::RecentCall::START_TIMESTAMP, startTime );
	}

	result = getDb()->insert( DatabaseTables::RecentCall::TABLE, values, true );

	return result;
}

void VoxoxDbManager::updateRecentCallAnswered( int callId )
{
	updateRecentCall( callId, RecentCall::STATUS_ANSWERED );
}

void VoxoxDbManager::updateRecentCallDeclined( int callId )
{
	updateRecentCall( callId, RecentCall::STATUS_DECLINED );
}

void VoxoxDbManager::updateRecentCallTerminated( int callId )	//TODO: Have caller provide duration?  We will calculate for now.
{
	RecentCall recentCall = getRecentCall( callId );

	int xxx = 1;

	if ( recentCall.isValid() )
	{
		if ( recentCall.isNew() )	//Call was not answered
		{
			updateRecentCall( callId, RecentCall::STATUS_MISSED );
		}
		else if ( recentCall.isAnswered() )
		{
			int duration = (int) (( DbUtils::getSystemTimeMillis() - recentCall.getStartTimestamp() ) / 1000 );
			updateRecentCall( callId, duration );
		}
		else if ( recentCall.isDeclined() )
		{
			//Do nothing?  Is this same as a missed call?
			xxx = 2;
		}
	}
	//Check current status to determine what to do.
}

bool VoxoxDbManager::updateRecentCallSeen( int callId )
{
	return updateRecentCallSeen( callId, RecentCall::SEEN );
}


int VoxoxDbManager::deleteRecentCall( int callId )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	if (!isDbValid() )
		return 0;

	std::string whereClause = DatabaseTables::RecentCall::ID + " = ? ";
	StringList args;
	args.push_back( std::to_string( callId ) );

	return getDb()->deleteRecords( DatabaseTables::RecentCall::TABLE, whereClause, args );
}

RecentCall VoxoxDbManager::getRecentCall( int callId )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	RecentCall	result;

	if (!isDbValid() )
		return result;

	StringList	selectionArgs;
	std::string whereClause = " WHERE rc." + DatabaseTables::RecentCall::ID + " = ?";	//Disambiguate 
	selectionArgs.push_back( std::to_string( callId ) );

	RecentCallList list = getRecentCalls( whereClause, selectionArgs );

	if ( list.size() > 0 )
	{
		result = list.front();		//Should be only one, but just in case, take the first one.
	}

	return result;
}

RecentCall VoxoxDbManager::getRecentCallByUid( const std::string& uid )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	RecentCall	result;

	if (!isDbValid() )
		return result;

	StringList	selectionArgs;
	std::string whereClause = " WHERE rc." + DatabaseTables::RecentCall::UID + " = ?";	//Disambiguate 
	selectionArgs.push_back( uid );

	RecentCallList list = getRecentCalls( whereClause, selectionArgs );

	if ( list.size() > 0 )
	{
		result = list.front();		//Should be only one, but just in case, take the first one.
	}

	return result;
}

RecentCallList VoxoxDbManager::getRecentCalls( const std::string& phoneNumber )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	RecentCallList result;

	if (!isDbValid() )
		return result;

	if (!phoneNumber.empty()) 
	{
		StringList selectionArgs;
		std::string whereClause = " WHERE rc." + DatabaseTables::RecentCall::PHONE_NUMBER + " = ? ";
		selectionArgs.push_back(DbUtils::getNormalizedNumberNoPrefix(phoneNumber));

		result = getRecentCalls(whereClause, selectionArgs);
		return result;
	}
	else
	{
		StringList selectionArgs;
		std::string whereClause;

		result = getRecentCalls(whereClause, selectionArgs);
		return result;
	}
}

RecentCallList VoxoxDbManager::getRecentCalls( int cmGroup )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	RecentCallList result;

	if (!isDbValid() )
		return result;

	StringList selectionArgs;
	std::string whereClause;

	if ( !cmGroupIsValid( cmGroup ) )	//Invalid group
	{
		whereClause = " WHERE " + DatabaseTables::PhoneNumber::CM_GROUP + " ISNULL ";
	}
	else
	{
		whereClause = " WHERE " + DatabaseTables::PhoneNumber::CM_GROUP + " = ?";
		selectionArgs.push_back( std::to_string( cmGroup ) );
	}

	result = getRecentCalls( whereClause, selectionArgs );

	return result;
}

int VoxoxDbManager::clearLocalCalls()
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	if (!isDbValid() )
		return 0;

	std::string whereClause = DatabaseTables::RecentCall::IS_LOCAL + " = ? ";
	StringList args;
	args.push_back(std::to_string(1));

	return getDb()->deleteRecords(DatabaseTables::RecentCall::TABLE, whereClause, args);
}


//-----------------------------------------------------------------
// PRIVATE RecentCall Methods
//-----------------------------------------------------------------

void VoxoxDbManager::updateRecentCall( int callId, RecentCall::Status status )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	if (!isDbValid() )
		return;

	QueryParameters qp;

	qp.setTable		  ( DatabaseTables::RecentCall::TABLE				 );
	qp.addContentValue( DatabaseTables::RecentCall::STATUS, (long)status );

	if ( status == RecentCall::STATUS_ANSWERED )
	{
		qp.addContentValue( DatabaseTables::RecentCall::START_TIMESTAMP, DbUtils::getSystemTimeMillis() );
	}

	qp.setSelection   ( DatabaseTables::Message::ID + " = ? "		  );
	qp.addSelectionArg( callId );

	int count = getDb()->update( qp );

	if ( count > 0 )
		broadcastRecentCallDatasetChanged();
}

void VoxoxDbManager::updateRecentCall( int callId, int duration )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	if (!isDbValid() )
		return;

	QueryParameters qp;

	qp.setTable		  ( DatabaseTables::RecentCall::TABLE			         );
	qp.addContentValue( DatabaseTables::RecentCall::DURATION, (long)duration );
	 
	qp.setSelection   ( DatabaseTables::Message::ID + " = ? "		   );
	qp.addSelectionArg( callId );

	int count = getDb()->update( qp );

	if ( count > 0 )
		broadcastRecentCallDatasetChanged();
}

bool VoxoxDbManager::updateRecentCallSeen( int callId, RecentCall::Seen seen )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	if (!isDbValid() )
		return false;

	QueryParameters qp;

	qp.setTable		  ( DatabaseTables::RecentCall::TABLE			 );
	qp.addContentValue( DatabaseTables::RecentCall::SEEN, (long)seen );

	qp.setSelection   ( DatabaseTables::Message::ID + " = ? "		 );
	qp.addSelectionArg( callId );

	int count = getDb()->update( qp );

	if ( count > 0 )
		broadcastRecentCallDatasetChanged();

	return (count > 0);
}

RecentCallList VoxoxDbManager::getRecentCalls( const std::string& whereClause, const StringList& selectionArgs )
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	RecentCallList	result;

	if (!isDbValid() )
		return result;

	std::string query = DbManagerConstants::getRecentCallRawQuery( whereClause );

	try 
	{
		RecentCallCursor* cursor = new RecentCallCursor( getDb()->rawQuery( query, selectionArgs ) );

		if ( cursor ) 
		{
			result = cursor->getData();

			delete cursor;
		}
	} 
	catch( VoxoxSQLiteException e )
	{
		int xxx = 1;		//TODO: On exception
	}

	return result;
}

//-----------------------------------------------------------------
// PUBLIC Settings Methods
//-----------------------------------------------------------------

UserSettings VoxoxDbManager::getUserSettings()
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	UserSettings result;

	if (!isDbValid() )
		return result;

	QueryParameters qp;
	qp.setTable     ( DatabaseTables::Settings::TABLE );
	qp.setProjection( DbManagerConstants::getGetSettingsProjection() );
	qp.setLimit( 1 );	

	try 
	{
		UserSettingsCursor* cursor = new UserSettingsCursor( getDb()->query( qp ) );

		if ( cursor ) 
		{
			result = cursor->getData();

			delete cursor;
		}
	} 
	catch( VoxoxSQLiteException e )
	{
		int xxx = 1;		//TODO: On exception
	}

	return result;
}

bool VoxoxDbManager::updateUserSettings( UserSettings& src )
{
	bool result = false;

	//We do a complete replacement of data, and Tables is set up to REPLACE ON CONFLICT, so can just do an INSERT.

	ContentValues values;

	values.put( DatabaseTables::Settings::USER_ID,						src.getUserId()	);

	values.put( DatabaseTables::Settings::AUDIO_INPUT_DEVICE,			src.getAudioInputDevice()	);
	values.put( DatabaseTables::Settings::AUDIO_OUTPUT_DEVICE,			src.getAudioOutputDevice()	);
	values.put( DatabaseTables::Settings::AUDIO_INPUT_VOLUME,			src.getAudioInputVolume()	);
	values.put( DatabaseTables::Settings::AUDIO_OUTPUT_VOLUME,			src.getAudioOutputVolume()	);

	values.put( DatabaseTables::Settings::SHOW_FAX_IN_MSG_THREAD,		src.getShowFaxInMsgThread()	);
	values.put( DatabaseTables::Settings::SHOW_VM_IN_MSG_THREAD,		src.getShowVmInMsgThread()	);
	values.put( DatabaseTables::Settings::SHOW_RC_IN_MSG_THREAD,		src.getShowRcInMsgThread()	);

	values.put( DatabaseTables::Settings::ENABLE_SOUND_INCOMING_MSG,	src.getEnableSoundIncomingMsg()		);
	values.put( DatabaseTables::Settings::ENABLE_SOUND_INCOMING_CALL,	src.getEnableSoundIncomingCall()	);
//	values.put( DatabaseTables::Settings::ENABLE_SOUND_OUTGOING_MSG,	src.getEnableSoundOutgoingMsg()		);

	values.put( DatabaseTables::Settings::MY_CHAT_BUBBLE_COLOR,			src.getMyChatBubbleColor()			);
	values.put( DatabaseTables::Settings::CONVERSANT_CHAT_BUBBLE_COLOR,	src.getConversantChatBubbleColor()	);

	values.put( DatabaseTables::Settings::COUNTRY_CODE,					src.getCountryCode()	);
	values.put( DatabaseTables::Settings::COUNTRY_ABBREV,				src.getCountryAbbrev()	);

//	values.put( DatabaseTables::Settings::OUTGOING_CALL_SOUND_FILE,		src.getOutgoingCallSoundFile()	);
	values.put( DatabaseTables::Settings::INCOMING_CALL_SOUND_FILE,		src.getIncomingCallSoundFile()	);
	values.put( DatabaseTables::Settings::CALL_CLOSED_SOUND_FILE,		src.getCallClosedSoundFile()	);

	values.put( DatabaseTables::Settings::WINDOW_LEFT,					src.getWindowLeft()		);
	values.put( DatabaseTables::Settings::WINDOW_TOP,					src.getWindowTop()		);
	values.put( DatabaseTables::Settings::WINDOW_HEIGHT,				src.getWindowHeight()	);
	values.put( DatabaseTables::Settings::WINDOW_WIDTH,					src.getWindowWidth()	);
	values.put( DatabaseTables::Settings::WINDOW_STATE,					src.getWindowState()	);

	values.put( DatabaseTables::Settings::LAST_MSG_TIMESTAMP,			src.getLastMsgTimestamp()		);
	values.put( DatabaseTables::Settings::LAST_CALL_TIMESTAMP,			src.getLastCallTimestamp()		);
	values.put( DatabaseTables::Settings::LAST_CONTACT_SOURCE_KEY,		src.getLastContactSourceKey()	);

	values.put( DatabaseTables::Settings::SIP_UUID,						src.getSipUuid()		);

	// Insert the new row
	int count = getDb()->insert(  DatabaseTables::Settings::TABLE, values, false );

	result  = (count > 0 );

	return result;
}


//-----------------------------------------------------------------
// PUBLIC APP Settings Methods - Not user level
//-----------------------------------------------------------------

AppSettings VoxoxDbManager::getAppSettings()
{
	std::lock_guard<std::recursive_mutex> lock( s_dbMutex );

	AppSettings result;

	if (!isDbValid() )
		return result;

	QueryParameters qp;
	qp.setTable     ( DatabaseTables::AppSettings::TABLE );
	qp.setProjection( DbManagerConstants::getGetAppSettingsProjection() );
	qp.setLimit( 1 );	

	try 
	{
		AppSettingsCursor* cursor = new AppSettingsCursor( getDb()->query( qp ) );

		if ( cursor ) 
		{
			result = cursor->getData();

			delete cursor;
		}
	} 
	catch( VoxoxSQLiteException e )
	{
		int xxx = 1;		//TODO: On exception
	}

	return result;
}

bool VoxoxDbManager::updateAppSettings( AppSettings& src )
{
	bool result = false;

	//We do a complete replacement of data, and Tables is set up to REPLACE ON CONFLICT, so can just do an INSERT.
	ContentValues values;

	values.put( DatabaseTables::AppSettings::ID,						1 );		//Always use same ID, to keep a single record.

	values.put( DatabaseTables::AppSettings::AUTO_LOGIN,				src.getAutoLogin()			);
	values.put( DatabaseTables::AppSettings::REMEMBER_PASSWORD,			src.getRememberPassword()	);
	values.put( DatabaseTables::AppSettings::LAST_LOGIN_USERNAME,		src.getLastLoginUsername()	);
	values.put( DatabaseTables::AppSettings::LAST_LOGIN_PASSWORD,		src.getLastLoginPassword()	);
	values.put( DatabaseTables::AppSettings::LAST_LOGIN_COUNTRY_CODE,	src.getLastLoginCountryCode()	);
	values.put( DatabaseTables::AppSettings::LAST_LOGIN_PHONE_NUMBER,	src.getLastLoginPhoneNumber()	);

	values.put( DatabaseTables::AppSettings::DEBUG_MENU,				src.getDebugMenu()				);
	values.put( DatabaseTables::AppSettings::IGNORE_LOGGERS,			src.getIgnoreLoggers()			);
	values.put( DatabaseTables::AppSettings::MAX_SIP_LOGGING,			src.getMaxSipLogging()			);
	values.put( DatabaseTables::AppSettings::GET_INIT_OPTIONS_HANDLING,	src.getGetInitOptionsHandling()	);

	// Insert the new row
	int count = getDb()->insert(  DatabaseTables::AppSettings::TABLE, values, false );

	result  = (count > 0 );

	return result;
}

//-----------------------------------------------------------------
//TODO: PUBLIC/PRIVATE DB Change broadcast message broadcast
//	- All private save one for GM.
//	- Approx. 8 methods
//-----------------------------------------------------------------

void VoxoxDbManager::broadcastMessagesDatasetChanged()
{
	mBroadcastMgr.msgChange();
}

void VoxoxDbManager::broadcastMessagesDatasetChanged( Message::Status newStatus )
{
	mBroadcastMgr.msgStatusChange( newStatus );
}

void VoxoxDbManager::broadcastMessagesAdded( size_t msgCount )
{
	mBroadcastMgr.msgAdded( msgCount );
}

void VoxoxDbManager::broadcastContactsDatasetChanged()
{
	mBroadcastMgr.contactChange( -1 );
}

void VoxoxDbManager::broadcastContactsDatasetChanged( int voxoxId )
{
	mBroadcastMgr.contactChange( voxoxId );
}

void VoxoxDbManager::broadcastGroupMessagingDatasetChanged( const std::string& groupId, DbChangeData::Action action )
{
	mBroadcastMgr.gmChange( groupId, action );
}

void VoxoxDbManager::broadcastRecentCallDatasetChanged()
{
	mBroadcastMgr.recentCallChange();
}

//public
void VoxoxDbManager::broadcastGroupMessagingDatasetChanged()
{
	mBroadcastMgr.gmChange();
}

//-----------------------------------------------------------------
