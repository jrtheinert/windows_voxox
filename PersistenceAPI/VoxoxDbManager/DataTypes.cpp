/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

#include "DataTypes.h"
#include "DbUtils.h"					//For Server Message Types.  This may move to a Constants class.

//For JSON support
#include "rapidjson/writer.h"			// for stringify JSON
#include "rapidjson/stringbuffer.h"

#include <assert.h>

//GM Message JSON keys/values
const std::string Message::s_GmJsonKeyGroupMsg	= "voxoxgroupmessage";

const std::string Message::s_GmJsonKeyBody		= "body";
const std::string Message::s_GmJsonKeyFrom		= "from";
const std::string Message::s_GmJsonKeyGroupId	= "groupId";
const std::string Message::s_GmJsonKeyMessageId	= "messageId";
const std::string Message::s_GmJsonKeyMsgType	= "messageType";
const std::string Message::s_GmJsonKeyNickname	= "nickname";
const std::string Message::s_GmJsonKeyTimestamp	= "timestamp";
const std::string Message::s_GmJsonKeyUserId	= "userId";

const std::string Message::s_GmJsonMsgType_GroupDissolved	= "groupdissolved";
const std::string Message::s_GmJsonMsgType_MemberAdded		= "newmember";
const std::string Message::s_GmJsonMsgType_MemberRemoved	= "memberremoved";
const std::string Message::s_GmJsonMsgType_MemberLeft		= "memberleft";
const std::string Message::s_GmJsonMsgType_Msg				= "im";

//RichData JSON keys/values
const std::string RichData::s_RichDataJsonKey			= "voxoxrichmessage";
const std::string RichData::s_RichDataJsonKeyType		= "type";
const std::string RichData::s_RichDataJsonKeyBody		= "body";
const std::string RichData::s_RichDataJsonKeyData		= "data";

//JSON type values
const std::string RichData::s_RichDataJsonTypeLocation	= "location";
const std::string RichData::s_RichDataJsonTypeContact	= "contact";
const std::string RichData::s_RichDataJsonTypeJpeg		= "jpeg";	//Image 
const std::string RichData::s_RichDataJsonTypeMpeg		= "mpeg";	//Video
		
// location JSON keys
const std::string RichData::s_RichDataJsonKeyLongitude	= "longitude";
const std::string RichData::s_RichDataJsonKeyLatitude	= "latitude";	
	
// media sharing JSON Keys
const std::string RichData::s_RichDataJsonKeyFile		= "file";
const std::string RichData::s_RichDataJsonKeyThumbnail	= "thumbnail";


//See DataTypes.h for details
//=============================================================================
// Contact class
//	- Used to populate CONTACT LIST tab
//=============================================================================

Contact::Contact() : mRecordId( 0 ), mCmGroup( Contact::INVALID_CMGROUP ), 
					 mVoxoxCount( 0 ), mCmGroupVoxoxCount( 0 ), mType( Contact::TYPE_UNKNOWN ), 
					 mBlockedCount( 0 ), mFavoriteCount( 0 ), mPhoneNumberCount( 0 ), mExtensionCount( 0 )
{
}

Contact::~Contact()
{
}

Contact& Contact::operator=( const Contact& src )
{
	if ( this != &src )
	{
		setRecordId			( src.getRecordId()			);
		setCmGroup			( src.getCmGroup()			);
		setSource			( src.getSource()			);
		setPhoneNumber		( src.getPhoneNumber()		);
		setDisplayNumber	( src.getDisplayNumber()	);
		setPhoneLabel		( src.getPhoneLabel()		);
		setExtension		( src.getExtension()		);
		setPreferredNumber	( src.getPreferredNumber()	);

		setName				( src.getName()				);
		setCmGroupName		( src.getCmGroupName()		);
		setDisplayName		( src.getDisplayName()		);
		setVoxoxCount		( src.getVoxoxCount()		);
		setCmGroupVoxoxCount( src.getCmGroupVoxoxCount());

		setGroupId			( src.getGroupId()			);
		setType				( src.getType()				);

		//New Contact-Sync
		setContactKey		( src.getContactKey()	);
		setFirstName		( src.getFirstName()	);
		setLastName			( src.getLastName()		);
		setCompany			( src.getCompany()		);

		setBlockedCount		( src.getBlockedCount()		);
		setFavoriteCount	( src.getFavoriteCount()	);
		setPhoneNumberCount ( src.getPhoneNumberCount() );
		setExtensionCount   ( src.getExtensionCount()   );
	}

	return *this;
}

//=============================================================================



//=============================================================================
// ContactImport class
//	- Used to import contact and phone numbers into DB
//	- This is expected to be a one-way data transger INTO DB.
//=============================================================================

ContactImport::ContactImport()
{
}

ContactImport::ContactImport( const std::string& contactKey, const std::string& name, const std::string& phoneNumber, const std::string& phoneLabel )
{
	setContactKey ( contactKey  );
	setName       ( name        );
	setPhoneNumber( phoneNumber );
	setPhoneLabel ( phoneLabel  );
}

ContactImport::~ContactImport()
{
}

ContactImport& ContactImport::operator=( const ContactImport& src )
{
	if ( this != &src )
	{
		setName	      ( src.getName()		 );
		setPhoneNumber( src.getPhoneNumber() );
		setPhoneLabel ( src.getPhoneLabel()	 ); 
		setJid        ( src.getJid()         );

		setIsFavorite ( src.getIsFavorite()  );

		//Added with new Contact Syncing
		setContactKey ( src.getContactKey()  );
		setSource	  ( src.getSource()		 );
		setFirstName  ( src.getFirstName()   );
		setLastName	  ( src.getLastName()    );
		setCompany	  ( src.getCompany()     );
		setUserId     ( src.getUserId()      );
		setUserName   ( src.getUserName()    );
		setIsBlocked  ( src.getIsBlocked()   );
		setIsExtension( src.getIsExtension() );
	}

	return *this;
}

//=============================================================================



//=============================================================================
// ContactMessageSummary class
//	- Provide Summary of each message thread for Message Thread window
//=============================================================================

ContactMessageSummary::ContactMessageSummary() : mCmGroup( Contact::INVALID_CMGROUP ), mGroupStatus( GroupMessageGroup::ACTIVE ), 
												 mMsgDir( Message::INBOUND ), mMsgType( Message::UNKNOWN_TYPE ), mMsgServerTimestamp( 0 )
{
}

ContactMessageSummary::~ContactMessageSummary()
{
}

ContactMessageSummary& ContactMessageSummary::operator=( const ContactMessageSummary& src )
{
	if ( this != &src )
	{
		setContactKey		  ( src.getContactKey()			);
		setCmGroup			  ( src.getCmGroup()			);
		setCmGroupName		  ( src.getCmGroupName()		);
		setDisplayName		  ( src.getDisplayName()		);
		setPhoneNumber		  ( src.getPhoneNumber()		);
		setDisplayNumber	  ( src.getDisplayNumber()		);
		setXmppJid			  ( src.getXmppJid()			);

		//For GM
		setToGroupId		  ( src.getToGroupId()			);
		setGroupName		  ( src.getGroupName()			);
		setGroupStatus		  ( src.getGroupStatus()		);

		//Newest Message Info
		setMsgDir			 ( src.getMsgDir()				);
		setMsgType			 ( src.getMsgType()				);
		setMsgServerTimestamp( src.getMsgServerTimestamp()	);
		setMsgBody			 ( src.getMsgBody()				);
		setMsgTranslated	 ( src.getMsgTranslated()		);
		setMsgToDid			 ( src.getMsgToDid()			);
		setMsgFromDid		 ( src.getMsgFromDid()			);
		setMsgToJid			 ( src.getMsgToJid()			);
		setMsgFromJid		 ( src.getMsgFromJid()			);
	}

	return *this;
}

//=============================================================================



//=============================================================================
// GroupMessageGroup class
//=============================================================================

GroupMessageGroup::GroupMessageGroup() : mRecordId( 0 ), mStatus( GroupMessageGroup::ACTIVE ), mInviteType( 0 ), mAnnounceMembers( false )
{
}

GroupMessageGroup::~GroupMessageGroup()
{
}

GroupMessageGroup& GroupMessageGroup::operator=( const GroupMessageGroup& src )
{
	if ( this != &src )
	{
		setRecordId		   ( src.getRecordId()			);
		setStatus		   ( src.getStatus()			);
		setGroupId		   ( src.getGroupId()			);
		setName			   ( src.getName()				);
		setInviteType	   ( src.getInviteType()		);
		setAnnounceMembers ( src.getAnnounceMembers()	);
		setConfirmationCode( src.getConfirmationCode()	);
		setAdminUserId	   ( src.getAdminUserId()		);
//		setAvatar		   ( src.getAvatar()			);
	}

	return *this;
}

//=============================================================================


//=============================================================================
// GroupMessageMember class
//=============================================================================

GroupMessageMember::GroupMessageMember() : mMemberId( 0 ), mUserId( 0 ), mReferrerId( 0 ), mIsAdmin( false ), mIsInvited( false ),
											mHasConfirmed( false ), mOptOut( false ), mNotify( false )
{
}

GroupMessageMember::~GroupMessageMember()
{
}

GroupMessageMember& GroupMessageMember::operator=( const GroupMessageMember& src )
{
	if ( this != &src )
	{
		setMemberId	   ( src.getMemberId()	 );
		setUserId	   ( src.getUserId()	 );
		setReferrerId  ( src.getReferrerId() );
		setGroupId	   ( src.getGroupId()	 );
		setTypeId	   ( src.getTypeId()	 );
		setNickName	   ( src.getNickName()	 );
		setIsAdmin	   ( src.isAdmin()		 );
		setIsInvited   ( src.isInvited()	 );
		setHasConfirmed( src.hasConfirmed()	 );
		setHasOptedOut ( src.hasOptedOut()	 );
		setShouldNotify( src.shouldNotify()	 );
	}

	return *this;
}

//=============================================================================


//=============================================================================
// GroupMessageMemberOf class
//	- Info for Contacts that are part of a given GM group.
//=============================================================================

GroupMessageMemberOf::GroupMessageMemberOf()  : mRecordId( 0 ), mUserId( 0 ), mIsAdmin( false )
{
}

GroupMessageMemberOf::~GroupMessageMemberOf()
{
}

GroupMessageMemberOf& GroupMessageMemberOf::operator=( const GroupMessageMemberOf& src )
{
	if ( this != &src )
	{
		setRecordId   ( src.getRecordId()	 );
		setGroupId    ( src.getGroupId()	 );
		setGroupName  ( src.getGroupName()	 );
		
		setUserId     ( src.getUserId()		 );
		setIsAdmin    ( src.isAdmin()		 );
		setNickName   ( src.getNickName()	 );

		setDisplayName( src.getDisplayName() );
	}

	return *this;
}

//=============================================================================

	
	
//=============================================================================
// Message class - TODO: Expand this to include ALL data elements
//=============================================================================

Message::Message() : mRecordId( 0 ), mThreadId( 0 ), mType( Message::UNKNOWN_TYPE ), mDirection( Message::INBOUND ), mStatus( Message::UNKNOWN_STATUS ),
					 mLocalTimestamp( 0 ), mServerTimestamp( 0 ), mReadTimestamp( 0 ), mDeliveredTimestamp( 0 ), 
					 mFromUserId( 0 ), mDuration( 0 )
//					 , mContactId( 0 ), mInboundLogId( 0 ),
{
}

Message::~Message()
{
}

Message& Message::operator=( const Message& src )
{
	if ( this != &src )
	{
		setRecordId		  ( src.getRecordId()		 );
		setThreadId		  ( src.getThreadId()		 );
		setType			  ( src.getType()			 ); 
		setDirection	  ( src.getDirection()		 );
		setStatus		  ( src.getStatus()			 );
		setMsgId		  ( src.getMsgId()			 );
		setBody			  ( src.getBody()			 );

		setTranslateBody  ( src.getTranslateBody()	 );
		setInboundLang    ( src.getInboundLang()	 );
		setOutboundLang   ( src.getOutboundLang()	 );

		setLocalTimestamp ( src.getLocalTimestamp()	 );
		setServerTimestamp( src.getServerTimestamp() );
		setReadTimestamp  ( src.getReadTimestamp() );
		setDeliveredTimestamp( src.getDeliveredTimestamp() );

		setContactDid	  ( src.getContactDid()		 );

		setFromDid		  ( src.getFromDid()		 );
		setToDid		  ( src.getToDid()			 );

		setFromJid		  ( src.getFromJid()		 );
		setToJid		  ( src.getToJid()			 );

		setToGroupId	  ( src.getToGroupId()		 );
		setFromUserId	  ( src.getFromUserId()		 );

		setFileLocal	  ( src.getFileLocal()		 );
		setThumbnailLocal ( src.getThumbnailLocal()	 );
		setDuration		  ( src.getDuration()		 );

		setModified		  ( src.getModified()		 );
		setIAccount		  ( src.getIAccount()		 );
		setCName		  ( src.getCName()			 );

//		setContactId      ( src.getContactId()		 );
//		setInboundLogId   ( src.getInboundLogId()	 );
//		setRichData       ( src.getRichData()		 );
	}

	return *this;
}


bool Message::isVoicemailEx() const
{ 
	return ( mType == Message::VOICEMAIL   ||
			 mType == Message::RECORDCALLS ||
			 mType == Message::FAX  );
}

//static
std::string Message::typeToServerType( Message::Type type )
{
	std::string result = DbUtils::mSvrMsgType_Xmpp;

	switch ( type )
	{
	case Message::CHAT:
	case Message::GROUP_MESSAGE:
		result = DbUtils::mSvrMsgType_Xmpp;		//Includes Group Messages.
		break;

	case Message::SMS:
		result = DbUtils::mSvrMsgType_Sms;
		break;

	case Message::VOICEMAIL:
		result = DbUtils::mSvrMsgType_Voicemail;
		break;

	case Message::FAX:
		result = DbUtils::mSvrMsgType_Fax;
		break;

	case Message::RECORDCALLS:
		result = DbUtils::mSvrMsgType_RecordedCall;
		break;

	default:
		assert( false );			//New type

	}

	return result;
}

RichData Message::getRichData() const
{
	RichData result = RichData::fromJsonText( getBody() );
	return result;
}

//For GroupMessage Message which may have RichData JSON embedded within its own JSON.
std::string Message::getUpdatedBody( const RichData& richData ) const
{
	std::string result = getBody();		//If not GM, just return existing body.

	if ( isGroupMessage() )
	{
		result = toJsonString( richData );

		//	result = unescape( obj.toString() );		//TODO-GM

		//	// remove extra char on GM where is has n appended at the end.
		//	// TODO please read http://www.ietf.org/rfc/rfc4627.txt section 2.5.
		//	// for now we escape only the newline characters but we might need to add more characters.
		//	//Moved this from RichData.  Still need to properly address this issue.
		//	result = result.replace( "\\n", "\n" );
		//}
	}

	return result;
}
	
std::string	Message::toJsonString( const RichData& richData ) const
{
	std::string result;

	rapidjson::Document* doc = toJsonObject( richData );

	if ( doc )
	{
		rapidjson::StringBuffer strbuf;
		rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
		doc->Accept(writer);
		result = strbuf.GetString();

//		//TODO: confirm if we need this or not.
//		//-Android- JSON object escapes forward slash.  This fouls up our RichData URLs, especially for Group Messaging.
//		//Replace escaped forward slash:  "\/" -> "/".  Crude, but effective.  It would be an edge case that user actually wants to send "\/".
//		result = result.replace( "\\/", "/" );

//		// TODO please read http://www.ietf.org/rfc/rfc4627.txt section 2.5.

		delete doc;
	}

	return result;
}

rapidjson::Document* Message::toJsonObject( const RichData& richData ) const	//NOT TESTED
{
	rapidjson::Document* doc = new rapidjson::Document;	//TODO: Wrap this rapidjson code in a class.

	if ( doc )
	{
		// Parse an empty JSON objext to initialize the document.
		char json[20] = "{  }";
		if (doc->Parse<0>(json).HasParseError())	
		{
			// Oops, something went wrong
			int xxx = 1;
		}

		std::string body = (richData.isTypeNone() ? getBody() : richData.toJsonString() );
		std::string tsText = DbUtils::convertMillisToServerTimeStamp( getServerTimestamp() );	//TODO: This DbUtils method is not implemented

		rapidjson::Value tempVal;

		//MESSAGE object
		rapidjson::Value message( rapidjson::Type::kObjectType);

		tempVal.SetString( getToGroupId().c_str(), doc->GetAllocator() );
		message.AddMember( s_GmJsonKeyGroupId.c_str(),	  tempVal,			doc->GetAllocator() );	//To GroupId

		tempVal.SetString( s_GmJsonMsgType_Msg.c_str(), doc->GetAllocator() );
		message.AddMember( s_GmJsonKeyMsgType.c_str(),	  tempVal,			doc->GetAllocator() );	//Msg Type is IM, because other types are not modifiable.


		tempVal.SetString( body.c_str(), doc->GetAllocator() );
		message.AddMember( s_GmJsonKeyBody.c_str(),		  tempVal,			doc->GetAllocator() );	//Body

		tempVal.SetString( getMsgId().c_str(), doc->GetAllocator() );
		message.AddMember( s_GmJsonKeyMessageId.c_str(),  tempVal,			doc->GetAllocator() );	//MsgId

		message.AddMember( s_GmJsonKeyFrom.c_str(),		  getFromUserId(),	doc->GetAllocator() );	//From UserID
		message.AddMember( s_GmJsonKeyTimestamp.c_str(),  tsText.c_str(),	doc->GetAllocator() );	//ServerTimestamp

		//Add to doc
		doc->AddMember( s_GmJsonKeyGroupMsg.c_str(), message, doc->GetAllocator() );
	}

	return doc;
}

//=============================================================================


//=============================================================================
// MessageTranslationProperties class
//=============================================================================

MessageTranslationProperties::MessageTranslationProperties() : mRecordId( 0 ), mContactId( 0 ), mEnabled( false ), mDirection( Message::INBOUND )
{
}

MessageTranslationProperties::~MessageTranslationProperties()
{
}

MessageTranslationProperties& MessageTranslationProperties::operator=( const MessageTranslationProperties& src )
{
	if ( this != &src )
	{
		setRecordId		  ( src.getRecordId()		 );
		setContactId	  ( src.getContactId()		 );
		setEnabled		  ( src.isEnabled()			 ); 
		setPhoneNumber	  ( src.getPhoneNumber()	 );
		setJid			  ( src.getJid()			 );
		setUserLanguage   ( src.getUserLanguage()	 );
		setContactLanguage( src.getContactLanguage() );
		setDirection	  ( src.getDirection()		 );
	}

	return *this;
}

//=============================================================================


//=============================================================================
// PhoneNumber class
//=============================================================================

PhoneNumber::PhoneNumber() : mRecordId( 0 ), mCmGroup( Contact::INVALID_CMGROUP ), mNumberType( PhoneNumber::DID_NUMBER ), 
							 mVoxoxUserId( 0 ), mIsFavorite( false ), mIsBlocked( false ), mXmppStatus( -1 ), mContactType( Contact::TYPE_PHONE_NUMBER )
{
}

PhoneNumber::~PhoneNumber()
{
}

PhoneNumber& PhoneNumber::operator=( const PhoneNumber& src )
{
	if ( this != &src )
	{
		setRecordId		( src.getRecordId()		);
		setName			( src.getName()			);
		setSource		( src.getSource()		);
		setDisplayName	( src.getDisplayName()	);
		setCmGroupName	( src.getCmGroupName()	);
		setContactType	( src.getContactType()	);

		//Phone Number related
		setNumber		( src.getNumber()		);
		setDisplayNumber( src.getDisplayNumber());
		setLabel		( src.getLabel()		);
		setCmGroup		( src.getCmGroup()		);
		setNumberType	( src.getNumberType()	);
		setIsFavorite	( src.isFavorite()		);

		//XMPP related
		setXmppJid		( src.getXmppJid()		);
		setXmppAlias	( src.getXmppAlias()	);
		setXmppPresence ( src.getXmppPresence()	);
		setXmppStatus	( src.getXmppStatus()	);
		setXmppGroup	( src.getXmppGroup()	);

		//New Contact-Sync
		setContactKey	( src.getContactKey()	);
		setFirstName	( src.getFirstName()	);
		setLastName		( src.getLastName()		);
		setCompany		( src.getCompany()		);
		setUserId		( src.getUserId()		);
		setUserName		( src.getUserName()		);
		setIsBlocked	( src.isBlocked()		);
		setIsExtension	( src.isExtension()		);
	}

	return *this;
}

bool PhoneNumber::isValid() const 	//TODO: Review this.  OK for now.
{
	bool result = false;

	if ( !getDisplayNumber().empty() &&
		 !getNumber().empty() )
	{
		result = true;
	}

	return result;
}

//=============================================================================


//=============================================================================
// RecentCall class
//=============================================================================

RecentCall::RecentCall() : mRecordId( -1 ), mStatus( STATUS_NONE ), mIsIncoming( false ), mTimestamp( 0 ), mStartTimestamp( 0 ), mDuration( 0 ), mIsLocal ( false ), mSeen( NOT_SEEN )
{
}

RecentCall::~RecentCall()
{
}

RecentCall& RecentCall::operator=( const RecentCall& src )
{
	if ( this != &src )
	{
		setRecordId		 ( src.getRecordId()		);
		setUid			 ( src.getUid()				);
		setStatus		 ( src.getStatus()			);
		setIsIncoming	 ( src.isIncoming()			);
		setTimestamp	 ( src.getTimestamp()		);
		setStartTimestamp( src.getStartTimestamp()	);
		setDuration		 ( src.getDuration()		);
		setIsLocal       ( src.getIsLocal()         );
		setSeen			 ( src.getSeen()			);

		setNumber		( src.getNumber()			);
		setOrigNumber	( src.getOrigNumber()		);
		setDisplayNumber( src.getDisplayNumber()	);

		setName			( src.getName()				);
		setContactKey	( src.getContactKey()		);
		setCmGroup		( src.getCmGroup()			);
		setCmGroupName	( src.getCmGroupName()		);
	}

	return *this;
}

bool RecentCall::isValid() const
{
	return getStatus() != STATUS_NONE;
}


//=============================================================================
// RecentCall class
//=============================================================================

StringList RecentCallList::getUniquePhoneNumbers()
{
	StringList result;

	for ( RecentCallList::const_iterator it = this->begin(); it != this->end(); it++ )
	{
		std::string contactNumber = ( (*it).isIncoming() ? (*it).getOrigNumber() : (*it).getNumber() );
		bool found = std::find( result.begin(), result.end(), contactNumber ) != result.end();

		if ( !found )
		{
			result.push_back( contactNumber );
		}
	}

	return result;
}

//=============================================================================
// RichData class
//=============================================================================

RichData::RichData() : mType( RichData::NONE ), mLatitude( 0.0 ), mLongitude( 0.0 )
{
}

RichData::~RichData()
{
}

RichData& RichData::operator=( const RichData& src )
{
	if ( this != &src )
	{
		setType		   ( src.getType()			);

		setBody		   ( src.getBody()			);
		setThumbnailUrl( src.getThumbnailUrl()	);
		setMediaUrl	   ( src.getMediaUrl()		);

		setLatitude	   ( src.getLatitude()		);
		setLongitude   ( src.getLongitude()		);
		setAddress	   ( src.getAddress()		);
	}

	return *this;
}

bool RichData::usesMedia() const
{
	bool result = false;

	switch ( getType() )
	{
	case Type::CONTACT:
	case Type::IMAGE:
	case Type::VIDEO:
		result = true;
		break;

	case Type::NONE:
	case Type::LOCATION:
	case Type::PDF:				//This may be 'true', but we do not yet support it.
		result = false;
		break;

	default:
		assert(false);				//New Type?
	
	}

	return result;
}

std::string RichData::typeToJsonKey() const
{
	std::string result;

	switch ( getType() )
	{
	case Type::CONTACT:
		result = s_RichDataJsonTypeContact;
		break;

	case Type::LOCATION:
		result = s_RichDataJsonTypeLocation;
		break;

	case Type::IMAGE:
		result = s_RichDataJsonTypeJpeg;
		break;

	case Type::VIDEO:
		result = s_RichDataJsonTypeMpeg;
		break;

	case Type::PDF:					//We do not yet support it.
	case Type::NONE:				//Should not be calling for this case
	default:
		assert(false);				//New Type?
	
	}

	return result;
}


//Some convenience methods
//static
RichData RichData::makeContact ( const std::string& mediaUrl, const std::string& thumbnailUrl, const std::string& body )
{
	RichData result;

	result.setType        ( Type::CONTACT );
	result.setBody        ( body          );
	result.setMediaUrl    ( mediaUrl      );
	result.setThumbnailUrl( thumbnailUrl  );

	return result;
}

//static
RichData RichData::makeLocation( const std::string& body, double latitude, double longitude )
{
	RichData result;

	result.setType	   ( Type::LOCATION );
	result.setBody     ( body      );	//Should/May contain address
	result.setLatitude ( latitude  );
	result.setLongitude( longitude );

	return result;
}

//static
 RichData RichData::makeImage( const std::string& mediaUrl, const std::string& thumbnailUrl, const std::string& body )
{
	RichData result;

	result.setType        ( Type::IMAGE   );
	result.setBody        ( body          );
	result.setMediaUrl    ( mediaUrl      );
	result.setThumbnailUrl( thumbnailUrl  );

	return result;
}

//static
RichData RichData::makeVideo( const std::string& mediaUrl, const std::string& thumbnailUrl, const std::string& body )
{
	RichData result;

	result.setType        ( Type::VIDEO   );
	result.setBody        ( body          );
	result.setMediaUrl    ( mediaUrl      );
	result.setThumbnailUrl( thumbnailUrl  );

	return result;
}


//Convert to JSON format.
//static
std::string	RichData::toJsonString() const
{
	std::string result;

	rapidjson::Document* doc = toJsonObject();

	if ( doc )
	{
		rapidjson::StringBuffer strbuf;
		rapidjson::Writer<rapidjson::StringBuffer> writer(strbuf);
		doc->Accept(writer);
		result = strbuf.GetString();

//		//TODO: confirm if we need this or not.
//		//-Android- JSON object escapes forward slash.  This fouls up our RichData URLs, especially for Group Messaging.
//		//Replace escaped forward slash:  "\/" -> "/".  Crude, but effective.  It would be an edge case that user actually wants to send "\/".
//		result = result.replace( "\\/", "/" );

//		// TODO please read http://www.ietf.org/rfc/rfc4627.txt section 2.5.

		delete doc;
	}

	return result;
}

//Caller must delete this pointer.
rapidjson::Document* RichData::toJsonObject() const
{
	rapidjson::Document* doc = new rapidjson::Document;

	if ( doc )
	{
		// Parse an empty JSON objext to initialize the document.
		char json[20] = "{  }";
		if (doc->Parse<0>(json).HasParseError())	
		{
			// Oops, something went wrong
			int xxx = 1;
		}

		rapidjson::Value tempVal;	//Used to properly allocate string values.

		if ( usesMedia() )
		{
			//DATA node
			rapidjson::Value data( rapidjson::Type::kObjectType );
			tempVal.SetString( getMediaUrl().c_str(), doc->GetAllocator() );
			data.AddMember( s_RichDataJsonKeyFile.c_str(), tempVal, doc->GetAllocator() );

			if ( !getThumbnailUrl().empty() )
			{
				tempVal.SetString( getThumbnailUrl().c_str(), doc->GetAllocator() );
				data.AddMember( s_RichDataJsonKeyThumbnail.c_str(), tempVal, doc->GetAllocator() );
			}

			//MESSAGE object
			rapidjson::Value message( rapidjson::Type::kObjectType );

			tempVal.SetString( typeToJsonKey().c_str(), doc->GetAllocator() );
			message.AddMember( s_RichDataJsonKeyType.c_str(), tempVal,	doc->GetAllocator() );	//TYPE

			tempVal.SetString(  getBody().c_str(), doc->GetAllocator() );
			message.AddMember( s_RichDataJsonKeyBody.c_str(), tempVal,  doc->GetAllocator() );	//BODY

			message.AddMember( s_RichDataJsonKeyData.c_str(), data,		doc->GetAllocator() );	//DATA Node

			//Add to doc
			doc->AddMember( s_RichDataJsonKey.c_str(), message, doc->GetAllocator() );
		}
		else
		{
			//LOCATION node.
			rapidjson::Value data( rapidjson::Type::kObjectType);
			data.AddMember( s_RichDataJsonKeyLatitude.c_str(),  getLatitude(),  doc->GetAllocator() );
			data.AddMember( s_RichDataJsonKeyLongitude.c_str(), getLongitude(), doc->GetAllocator() );

			//MESSAGE object
			rapidjson::Value message( rapidjson::Type::kObjectType);

			tempVal.SetString( typeToJsonKey().c_str(), doc->GetAllocator() );
			message.AddMember( s_RichDataJsonKeyType.c_str(), tempVal, doc->GetAllocator() );	//TYPE

			tempVal.SetString( getAddress().c_str(), doc->GetAllocator() );
			message.AddMember( s_RichDataJsonKeyBody.c_str(), tempVal, doc->GetAllocator() );	//BODY/Address

			tempVal.SetString( typeToJsonKey().c_str(), doc->GetAllocator() );
			message.AddMember( s_RichDataJsonKeyData.c_str(), data,	   doc->GetAllocator() );	//DATA Node

			//Add to doc
			doc->AddMember( s_RichDataJsonKey.c_str(), message, doc->GetAllocator() );
		}
	}

	return doc;
}

//static
//This does all the parsing.  All other methods should call this to get a RichData object from JSON string.
RichData RichData::fromJsonText( const std::string& json )
{
	RichData result;

	try
	{
		if ( DbUtils::contains( json, RichData::s_RichDataJsonKey, true )  )
		{
			rapidjson::Document doc;	// Default template parameter uses UTF8 and MemoryPoolAllocator.

			// "normal" parsing, decode strings to new buffers. Can use other input stream via ParseStream().
			if (doc.Parse<0>(json.c_str()).HasParseError() != 1 )	// TODO: Equal zero?
			{
				//Is it a RichData JSON string?
				if ( doc.HasMember(s_RichDataJsonKey.c_str()) )
				{
					const rapidjson::Value& richDataObj = doc[s_RichDataJsonKey.c_str()];

					//Check for 'type'
					if ( richDataObj.HasMember(s_RichDataJsonKeyType.c_str()) )
					{
						//In DATA node
						std::string mediaUrl;
						std::string thumbnailUrl;
						double		latitude  = 0.0;
						double		longitude = 0.0;

						//In main node.
						std::string body;
						std::string type = richDataObj[s_RichDataJsonKeyType.c_str()].GetString();

						//Body is used in most/all types, so let's get it once.
						if ( richDataObj.HasMember(s_RichDataJsonKeyBody.c_str()) )
							body = richDataObj[s_RichDataJsonKeyBody.c_str()].GetString();

						//Determine if DATA node exists and parse it.
						if ( richDataObj.HasMember( s_RichDataJsonKeyData.c_str() ) )
						{
							const rapidjson::Value& dataObj = richDataObj[s_RichDataJsonKeyData.c_str()];	//Get Data object from RichData Object

							if ( dataObj.HasMember( s_RichDataJsonKeyFile.c_str() ) )
								mediaUrl = dataObj[s_RichDataJsonKeyFile.c_str()].GetString();

							if ( dataObj.HasMember( s_RichDataJsonKeyThumbnail.c_str() ) )
								thumbnailUrl = dataObj[s_RichDataJsonKeyThumbnail.c_str()].GetString();

							if ( dataObj.HasMember( s_RichDataJsonKeyLatitude.c_str() ) )
								latitude = dataObj[s_RichDataJsonKeyLatitude.c_str()].GetDouble();

							if ( dataObj.HasMember( s_RichDataJsonKeyLongitude.c_str() ) )
								longitude = dataObj[s_RichDataJsonKeyLongitude.c_str()].GetDouble();
						}


						if ( type == s_RichDataJsonTypeLocation )
						{
							result = makeLocation( body, latitude, longitude );			//Body should/may contain address
						}
						else if ( type == s_RichDataJsonTypeContact )
						{
							result = makeContact( mediaUrl, thumbnailUrl, body );
						}
						else if ( type == s_RichDataJsonTypeJpeg )
						{
							result = makeImage( mediaUrl, thumbnailUrl, body );
						}
						else if ( type == s_RichDataJsonTypeMpeg )
						{
							result = makeVideo( mediaUrl, thumbnailUrl, body );
						}
					}
				}
			}
		}
	}

	catch ( ... )
	{
		//rapidjson does not have a specific exception, so I add this just in case.
		//TODO: log error?
		int xxx = 1;
		xxx++;
	}

	return result;
}

//=============================================================================


//=============================================================================
// UmwMessage class
//=============================================================================
UmwMessage::UmwMessage() :  mRecordId( 0 ), mCmGroup( Contact::INVALID_CMGROUP ), mIsVoxox( PhoneNumber::DID_NUMBER ),
							mType( Message::UNKNOWN_TYPE ), mDir( Message::INBOUND ), mStatus( Message::UNKNOWN_STATUS ),
							mLocalTimestamp( 0 ), mServerTimestamp( 0 ), mReadTimestamp( 0 ), mDeliveredTimestamp( 0 ),
							mDuration( 0 ), mFromUserId( 0 )
{
}

UmwMessage::~UmwMessage()
{
}

UmwMessage& UmwMessage::operator=( const UmwMessage& src )
{
	if ( this != &src )
	{
		setRecordId			  ( src.getRecordId()			);
		setContactKey		  ( src.getContactKey()			);
		setPhoneNumber		  ( src.getPhoneNumber()		);
		setCmGroup			  ( src.getCmGroup()			);
		setName				  ( src.getName()				);
		setCmGroupName		  ( src.getCmGroupName()		);
		setIsVoxox			  ( src.getIsVoxox()			);

		//From GM table
		setGmNickName		  ( src.getGmNickName()			);
		setGmColor			  ( src.getGmColor()			);
		setGmGroupName		  ( src.getGmGroupName()		);

		//Main message info
		setType				  ( src.getType()				);
		setDirection		  ( src.getDirection()			);
		setStatus			  ( src.getStatus()				);
		setBody				  ( src.getBody()				);
		setTranslated		  ( src.getTranslated()			);
		setMsgId			  ( src.getMsgId()				);

		//Time stamps
		setLocalTimestamp    ( src.getLocalTimestamp()		);
		setServerTimestamp   ( src.getServerTimestamp()		);
		setReadTimestamp     ( src.getReadTimestamp()		);
		setDeliveredTimestamp( src.getDeliveredTimestamp()	);

		//Rich data
		setFileLocal		  ( src.getFileLocal()			);
		setThumbnailLocal	  ( src.getThumbnailLocal()		);
		setDuration			  ( src.getDuration()			);

		//SMS
		setFromDid			  ( src.getFromDid()			);
		setToDid			  ( src.getToDid()				);

		//Chat
		setFromJid			  ( src.getFromJid()			);
		setToJid			  ( src.getToJid()				);

		//Group Messaging
		setToGroupId		  ( src.getToGroupId()			);
		setFromUserId		  ( src.getFromUserId()			);
	}

	return *this;
}

//=============================================================================

	
//=============================================================================
// VoxoxContact class
//=============================================================================

VoxoxContact::VoxoxContact() : mUserId( 0 )
{
}

VoxoxContact::VoxoxContact( const std::string& contactKey, const std::string& regNum, const std::string& userName, const std::string& did, 
							const std::string& phoneLabel, int userId, bool isBlocked, bool isFavorite, const std::string& source )
{
	setContactKey( contactKey		);
	setRegistrationNumber( regNum	);
	setUserName  ( userName			);
	setDid	     ( did				);
	setPhoneLabel( phoneLabel		);
	setUserId    ( userId			);
	setSource    ( source			);
	setIsBlocked ( isBlocked		);
	setIsFavorite( isFavorite		);
}

VoxoxContact::~VoxoxContact()
{
}

VoxoxContact& VoxoxContact::operator=( const VoxoxContact& src )
{
	if ( this != &src )
	{
		setContactKey		 ( src.getContactKey()			);
		setRegistrationNumber( src.getRegistrationNumber()	);
		setUserName		     ( src.getUserName()			);
		setDid				 ( src.getDid()					);
		setPhoneLabel		 ( src.getPhoneLabel()			);
		setUserId			 ( src.getUserId()				);
		setSource			 ( src.getSource()				);
		setIsBlocked		 ( src.isBlocked()				);
		setIsFavorite		 ( src.isFavorite()				);
	}

	return *this;
}

//=============================================================================

	
//=============================================================================
// VoxoxPhoneNumber class
//=============================================================================

VoxoxPhoneNumber::VoxoxPhoneNumber()
{
}

VoxoxPhoneNumber::VoxoxPhoneNumber( const std::string& did, const std::string& jid )
{
	setDid( did );
	setJid( jid );
}

VoxoxPhoneNumber::~VoxoxPhoneNumber()
{
}

VoxoxPhoneNumber& VoxoxPhoneNumber::operator=( const VoxoxPhoneNumber& src )
{
	if ( this != &src )
	{
		setDid( src.getDid() );
		setJid( src.getJid() );
	}

	return *this;
}

//=============================================================================


//=============================================================================
// XmppContact class
//=============================================================================

XmppContact::XmppContact()
{
}

XmppContact::~XmppContact()
{
}

XmppContact& XmppContact::operator=( const XmppContact& src )
{
	if ( this != &src )
	{
		setJid	   ( src.getJid()	   );
		setName	   ( src.getName()	   );
		setStatus  ( src.getStatus()   );
		setMsgState( src.getMsgState() );
	}

	return *this;
}

//=============================================================================


	
//=============================================================================
// DeleteMessageData class
//=============================================================================

DeleteMessageData::DeleteMessageData() : mMsgType( Message::UNKNOWN_TYPE ), mMsgStatus( Message::UNKNOWN_STATUS ), mSuccess( false )
{
}

DeleteMessageData::DeleteMessageData( const std::string msgId, Message::Type type, Message::Status status ) : mSuccess( false )
{
	setMsgId    ( msgId  );
	setMsgType  ( type   );
	setMsgStatus( status );

	setSvrMsgType( Message::typeToServerType( type ) );		//TODO: Fill this in here, or allow the REST API to handle it, since the values are SERVER controlled?
}

DeleteMessageData::~DeleteMessageData()
{
}

DeleteMessageData DeleteMessageData::operator=( const DeleteMessageData& src )
{
	if ( this != &src )
	{
		setMsgId     ( src.getMsgId()		);
		setMsgType   ( src.getMsgType()		);
		setSvrMsgType( src.getSvrMsgType()	);
		setMsgStatus ( src.getMsgStatus()	);
		setSuccess   ( src.getSuccess()		);
	}

	return *this;
}

//=============================================================================


//=============================================================================

UnreadCounts::UnreadCounts() : mChat( 0 ), mSms( 0 ), mGm( 0 ), mFax( 0 ), mVoicemail( 0 ), mRecordedCall( 0 ), mMissedCall( 0 )
{
}

UnreadCounts::~UnreadCounts()
{
}

UnreadCounts UnreadCounts::operator=( const UnreadCounts& src )
{
	if ( this != &src  )
	{
		setChatCount  ( src.getChatCount()	 );
		setSmsCount	  ( src.getSmsCount()	 );
		setGmCount	  ( src.getGmCount()	 );

		setFaxCount			( src.getFaxCount()			 );
		setVoicemailCount   ( src.getVoicemailCount()    );
		setRecordedCallCount( src.getRecordedCallCount() );

		setMissedCallCount( src.getMissedCallCount() );
	}

	return *this;
}

//=============================================================================



//=============================================================================
//UserSettings
//=============================================================================
UserSettings::UserSettings() : 
	  mAudioInputVolume  ( 0 )
	, mAudioOutputVolume ( 0 )
	, mShowFaxInMsgThread( true )
	, mShowVmInMsgThread ( true )
	, mShowRcInMsgThread ( true )
	, mEnableSoundIncomingMsg( true )
	, mEnableSoundIncomingCall( true )
//	, mEnableSoundOutgoingMsg ( true )
	, mWindowLeft  ( 0 )
	, mWindowTop   ( 0 )
	, mWindowHeight( 0 )
	, mWindowWidth ( 0 )
	, mWindowState ( 0 )
	, mLastMsgTimestamp ( 0 )
	, mLastCallTimestamp( 0 )
{
}

UserSettings::~UserSettings()
{
}

UserSettings UserSettings::operator=( const UserSettings& src )
{
	if ( this != &src  )
	{
		setUserId					( src.getUserId()						);

		setAudioInputDevice			( src.getAudioInputDevice()				);
		setAudioOutputDevice		( src.getAudioOutputDevice()			);
		setAudioInputVolume			( src.getAudioInputVolume()				);
		setAudioOutputVolume		( src.getAudioOutputVolume()			);

		setShowFaxInMsgThread		( src.getShowFaxInMsgThread()			);
		setShowVmInMsgThread		( src.getShowVmInMsgThread()			);
		setShowRcInMsgThread		( src.getShowRcInMsgThread()			);

		setEnableSoundIncomingMsg	( src.getEnableSoundIncomingMsg()		);
		setEnableSoundIncomingCall	( src.getEnableSoundIncomingCall()		);
//		setEnableSoundOutgoingMsg	( src.getEnableSoundOutgoingMsg()		);

		setMyChatBubbleColor		( src.getMyChatBubbleColor()			);
		setConversantChatBubbleColor( src.getConversantChatBubbleColor()	);

		setCountryCode				( src.getCountryCode()					);
		setCountryAbbrev			( src.getCountryAbbrev()				);

//		setOutgoingCallSoundFile	( src.getOutgoingCallSoundFile()		);
		setIncomingCallSoundFile	( src.getIncomingCallSoundFile()		);
		setCallClosedSoundFile		( src.getCallClosedSoundFile()			);

		setWindowLeft				( src.getWindowLeft()					);
		setWindowTop				( src.getWindowTop()					);
		setWindowHeight				( src.getWindowHeight()					);
		setWindowWidth				( src.getWindowWidth()					);

		setLastMsgTimestamp			( src.getLastMsgTimestamp()				);
		setLastCallTimestamp		( src.getLastCallTimestamp()			);
		setLastContactSourceKey		( src.getLastContactSourceKey()			);

		setSipUuid					( src.getSipUuid()						);
	}

	return *this;
}

//=============================================================================


//=============================================================================
//AppSettings - NOT PART OF USER-DB
//=============================================================================

AppSettings::AppSettings() : 
	  mRecordId ( -1 )
	, mAutoLogin       ( true )
	, mRememberPassword( true )
	, mMaxSipLogging   ( true )
{
}

AppSettings::~AppSettings()
{
}

AppSettings AppSettings::operator=( const AppSettings& src )
{
	if ( this != &src  )
	{
		setRecordId			   ( src.getRecordId()				);

		setAutoLogin		   ( src.getAutoLogin()				);
		setRememberPassword    ( src.getRememberPassword()		);
		setLastLoginUsername   ( src.getLastLoginUsername()		);
		setLastLoginPassword   ( src.getLastLoginPassword()		);
		setLastLoginCountryCode( src.getLastLoginCountryCode()	);
		setLastLoginPhoneNumber( src.getLastLoginPhoneNumber()	);

		//Debug items
		setDebugMenu		     ( src.getDebugMenu()				);
		setIgnoreLoggers	     ( src.getIgnoreLoggers()			);
		setMaxSipLogging	     ( src.getMaxSipLogging()			);
		setGetInitOptionsHandling( src.getGetInitOptionsHandling()	);
	}

	return *this;
}



//=============================================================================


//=============================================================================
//DbChangeData class
//	Reports what table and, in some cases, specific data changed.
//=============================================================================

DbChangeData::DbChangeData() : mTable( Table::ANY ), mMsgStatus( Message::UNKNOWN_STATUS ), mMsgCount( 0 ), mVoxoxId( 0 ), mGmAction( Action::NO_ACTION )
{
}

DbChangeData::DbChangeData( Table table ) : mTable( table ), mMsgStatus( Message::UNKNOWN_STATUS ), mMsgCount( 0 ), mVoxoxId( 0 ), mGmAction( Action::NO_ACTION )
{
}

DbChangeData::~DbChangeData()
{
}

DbChangeData& DbChangeData::operator=( const DbChangeData& src )
{
	if ( this != &src )
	{
		mTable		= src.mTable;

		mMsgStatus	= src.mMsgStatus;
		mMsgCount	= src.mMsgCount;

		mVoxoxId	= src.mVoxoxId;

		mGroupId	= src.mGroupId;
		mGmAction	= src.mGmAction;
	}
	
	return *this;
}

//Table
DbChangeData::Table DbChangeData::getTable() const
{
	return mTable;
}

bool DbChangeData::isMsgTable() const
{
	return mTable == Table::MESSAGE;
}

bool DbChangeData::isContactTable() const
{
	return mTable == Table::CONTACT;
}

bool DbChangeData::isGmTable() const
{
	return mTable == Table::GM;
}

bool DbChangeData::isRecentCallTable() const
{
	return mTable == Table::RECENT_CALL;
}

bool DbChangeData::isSettingsTable() const
{
	return mTable == Table::SETTINGS;
}

bool DbChangeData::isAppSettingsTable() const
{
	return mTable == Table::APP_SETTINGS;
}

//Message table
Message::Status	DbChangeData::getMsgStatus() const
{
	Message::Status result = Message::UNKNOWN_STATUS;

	if ( isMsgTable() )
	{
		result = mMsgStatus;
	}

	return result;
}

size_t DbChangeData::getMsgCount() const
{
	size_t result = 0;

	if ( isMsgTable() )
	{
		result = mMsgCount;
	}

	return result;
}

//Contact
int DbChangeData::getVoxoxId()	const
{
	int result = 0;

	if ( isContactTable() )
	{
		result = mVoxoxId;
	}

	return result;
}

//GM
std::string DbChangeData::getGroupId()	const
{
	std::string result;

	if ( isGmTable() )
	{
		result = mGroupId;
	}

	return result;
}

DbChangeData::Action DbChangeData::getGmAction() const
{
	return mGmAction;
}

bool DbChangeData::isGmAdd() const
{
	return isGmAction( Action::ADD );
}

bool DbChangeData::isGmEdit() const
{
	return isGmAction( Action::EDIT );
}

bool DbChangeData::isGmDelete() const
{
	return isGmAction( Action::DELETE );
}

bool DbChangeData::isGmAction( Action tgtAction ) const
{
	bool result = false;
	
	if ( isGmTable() )
	{
		result = ( mGmAction == tgtAction );
	}

	return result;
}

//=============================================================================

