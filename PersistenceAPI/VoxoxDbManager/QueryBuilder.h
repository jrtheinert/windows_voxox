/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

//=====================================================================================================================
//This class will consist of mostly static methods used to build SQL queries
//	- Based on -Android- SQLiteQueryBuilder class.
//=====================================================================================================================
#pragma once

#include "VoxoxSQLite.h"
#include "DllExport.h"

class DLLEXPORT QueryBuilder
{
public:
    /**
     * Build an SQL query string from the given clauses.
     *
     * @param distinct true if you want each row to be unique, false otherwise.
     * @param tables The table names to compile the query against.
     * @param columns A list of which columns to return. Passing null will
     *            return all columns, which is discouraged to prevent reading
     *            data from storage that isn't going to be used.
     * @param where A filter declaring which rows to return, formatted as an SQL
     *            WHERE clause (excluding the WHERE itself). Passing null will
     *            return all rows for the given URL.
     * @param groupBy A filter declaring how to group rows, formatted as an SQL
     *            GROUP BY clause (excluding the GROUP BY itself). Passing null
     *            will cause the rows to not be grouped.
     * @param having A filter declare which row groups to include in the cursor,
     *            if row grouping is being used, formatted as an SQL HAVING
     *            clause (excluding the HAVING itself). Passing null will cause
     *            all row groups to be included, and is required when row
     *            grouping is not being used.
     * @param orderBy How to order the rows, formatted as an SQL ORDER BY clause
     *            (excluding the ORDER BY itself). Passing null will use the
     *            default sort order, which may be unordered.
     * @param limit Limits the number of rows returned by the query,
     *            formatted as LIMIT clause. Passing null denotes no LIMIT clause.
     * @return the SQL query string
     */
    static std::string buildQueryString( bool distinct, const std::string& tables, StringList columns, 
										 const std::string& where,  const std::string& groupBy, 
										 const std::string& having, const std::string& orderBy, 
										 const std::string& limit );
    
    /**
     * Add the names that are non-null in columns to s, separating them with commas.
     */
	static void appendColumns( std::string& sql, StringList columns);

private:
	static void appendClause( std::string& sql, const std::string& name, const std::string& clause);

private:
	QueryBuilder();
	~QueryBuilder();
};

