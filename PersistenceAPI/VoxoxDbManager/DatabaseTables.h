/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

//=====================================================================================================================
//This class will contain all the table definitions, which include
//	- SQL to CREATE table
//	- SQL to DROP table
//	- Defined constants for standard SQL phrases
//	- Table-specific field names.  These will be public so they can be re-used in DatabaseManger class
//	- Misc 'names' used in subqueries, SELECT AS, etc. (This will be likely be replaced by Contact table re-implementation.
//
//	Most methods/memvars will be static so they are easily assessible.
//=====================================================================================================================

//=========================================================================================================
//NOTE: We use DLLEXPORT so the defined values are available to UNIT TESTING
//	If you don't care about UNIT TESTING, then you can drop DLLEXPORT, but I (JRT) recommend against it.
//=========================================================================================================

#pragma once

#include "DllExport.h"
#include <string>

class DatabaseTables
{
private:
	DatabaseTables()		{}		//Private so it cannot be instantiated
	~DatabaseTables()		{}		//Nothing to destroy

private:
	//=============================================================================
	//SQL snippets useable in all tables.  These should likely be private/protected.
	//=============================================================================
	static const std::string CREATE_TABLE;
	static const std::string DELETE_TABLE;

	static const std::string INTEGER;
	static const std::string INTEGER_DEFAULT_INVALID;
	static const std::string INTEGER_DEFAULT_1;
	static const std::string INTEGER_DEFAULT_0;

	static const std::string REAL;
	static const std::string REAL_DEFAULT_0;

	static const std::string TEXT;
	static const std::string PRIMARY_KEY;

	static const std::string OPEN_PARENS;
	static const std::string CLOSE_PARENS;

	static const int MESSAGE_CONTACT_DID_MIN_LENGTH = 3;

public:
	//=============================================================================
	//Contact Table (To be re-implemented in Desktop) - These values are currently contact-level aggregates.
	//=============================================================================
	class Contact
	{
	private:
		Contact()		{}					//Private so it cannot be instantiated

	public:
	//	static const std::string TABLE;
	//	static const std::string ID;
	//	static const std::string NAME;

	//	static const std::string CREATE_TABLE;

		DLLEXPORT static const std::string TYPE;	//For Contact list that has Contacts and Groups.

		//These will likely move to Contact table, once we re-implement that table.
		DLLEXPORT static const std::string DISPLAY_NAME;

		DLLEXPORT static const std::string VOXOX_COUNT;
		DLLEXPORT static const std::string BLOCKED_COUNT;
		DLLEXPORT static const std::string FAVORITE_COUNT;
		DLLEXPORT static const std::string PHONE_NUMBER_COUNT;
		DLLEXPORT static const std::string EXTENSION_COUNT;

		DLLEXPORT static const std::string EXTENSION;			//Used with 'AS' in ContactList for CloudPhone
		DLLEXPORT static const std::string PREFERRED_NUMBER;	//Used with 'AS' in ContactList for CloudPhone
	};

	//=============================================================================
	//Phone Number table
	//=============================================================================
	class PhoneNumber
	{
	private:
		PhoneNumber()		{}			//Private so it cannot be instantiated

	public:
		//SQL
		DLLEXPORT static const std::string CREATE_TABLE;
		DLLEXPORT static const std::string DELETE_TABLE;

		//Table name
		DLLEXPORT static const std::string TABLE;

		//Fields
		DLLEXPORT static const std::string ID;
		DLLEXPORT static const std::string SOURCE;
		DLLEXPORT static const std::string CM_GROUP;		//Used to properly group messages in ContactsMessagesActivity.
		DLLEXPORT static const std::string GROUP_ID;
		DLLEXPORT static const std::string NAME;			//Constructed FULL NAME
		DLLEXPORT static const std::string FIRST_NAME;		//New Contact Syncing
		DLLEXPORT static const std::string LAST_NAME;		//New Contact Syncing
		DLLEXPORT static const std::string COMPANY;			//New Contact Syncing
		DLLEXPORT static const std::string CM_GROUP_NAME;	//Possbily a compound name

		DLLEXPORT static const std::string NUMBER;			// number used to optimize database queries
		DLLEXPORT static const std::string DISPLAY_NUMBER;	// display_number (as appears in the native database)
		DLLEXPORT static const std::string LABEL;
		DLLEXPORT static const std::string IS_VOXOX;
		DLLEXPORT static const std::string IS_FAVORITE;
		DLLEXPORT static const std::string IS_BLOCKED;		//New Contact Syncing
		DLLEXPORT static const std::string IS_EXT;			//New Contact Syncing	Denotes phoneNumber is an Extension

		DLLEXPORT static const std::string CONTACT_KEY;		//New Contact Syncing
		DLLEXPORT static const std::string USER_ID;			//New Contact Syncing
		DLLEXPORT static const std::string USER_NAME;		//New Contact Syncing

		DLLEXPORT static const std::string XMPP_JID;
		DLLEXPORT static const std::string XMPP_GROUP;
		DLLEXPORT static const std::string XMPP_STATUS;
		DLLEXPORT static const std::string XMPP_PRESENCE;
		DLLEXPORT static const std::string XMPP_SUBSCRIPTION;
	};

	//=============================================================================
	//Message table
	//=============================================================================
	class Message
	{
	private:
		Message()		{}			//Private so it cannot be instantiated

	public:
		//Table name
		DLLEXPORT static const std::string TABLE;

		//Base message
		static const std::string ID;
		static const std::string MESSAGE_ID;
		static const std::string THREAD_ID;
		static const std::string DIRECTION;
		static const std::string TYPE;
		static const std::string STATUS;
		static const std::string BODY;

		//Rich message
		static const std::string FILE_LOCAL;
		static const std::string THUMBNAIL_LOCAL;
		static const std::string DURATION;

		//Translation
		static const std::string TRANSLATED;;
		static const std::string INBOUND_LANG;
		static const std::string OUTBOUND_LANG;

		//Time stamps
		static const std::string LOCAL_TIMESTAMP;
		static const std::string SERVER_TIMESTAMP;
		static const std::string READ_TIMESTAMP;
		static const std::string DELIVERED_TIMESTAMP;

		//Not sure if these are used.
		static const std::string IACCOUNT;
		static const std::string CNAME;
		static const std::string MODIFIED;

		//SMS and Chat - but may not be totally correct.
		static const std::string CONTACT_DID ;

		//SMS Message
		static const std::string FROM_DID;
		static const std::string TO_DID;

		//Chat Message
		static const std::string FROM_JID;
		static const std::string TO_JID;

		//Group Messaging
		static const std::string TO_GROUP_ID;
		static const std::string FROM_USER_ID;

		//Aggregate values - NOT FIELDS IN TABLE, BUT USED IN QUERIES.
		static const std::string MSG_COUNT;

		//DML
		static const std::string CREATE_TABLE;
		static const std::string DELETE_TABLE;
	};


	//=============================================================================
	//Group Message Group table
	//=============================================================================
	class GroupMessageGroup
	{
	private:
		GroupMessageGroup()		{}			//Private so it cannot be instantiated

	public:
		//Table Name
        DLLEXPORT static const std::string  TABLE;

		//Fields
        static const std::string  ID;
        static const std::string  GROUP_ID;
        static const std::string  NAME;
        static const std::string  STATUS;
        static const std::string  LAST_COLOR;			//Last color name assigned.  Used to determine NEXT color to assign a group member

        //These are crossed out in API doc.  May be Phase 2, so leave in DB for now.  Check with Curtis
        static const std::string  INVITE_TYPE; 			// type of invite to send (0=none, 1=welcome message, 2=opt-in)
        static const std::string  ANNOUNCE_MEMBERS; 	// send new member announcements to the group
        static const std::string  CONFIRMATION_CODE;	// code required for opting in "Yes"

        //TODO-GM: we do not typically store avatar in DB.  How will we be handling this?  Phase 2, per Curtis.
        static const std::string  AVATAR;

		//Aggregate values - NOT FIELDS IN TABLE, BUT USED IN QUERIES.
        static const std::string  MEMBER_COUNT;
        static const std::string  ADMIN_USER_ID;

        //SELECT AS field names to disambiguate PN and GMG 'name'
        static const std::string  GROUP_NAME;

		//DML
		static const std::string CREATE_TABLE;
		static const std::string DELETE_TABLE;
	};


	//=============================================================================
	//Group Message Member table
	//=============================================================================

	class GroupMessageMember
	{
	private:
		GroupMessageMember()		{}			//Private so it cannot be instantiated

	public:
		DLLEXPORT static const std::string TABLE;

		static const std::string ID;
        static const std::string GROUP_ID;
        static const std::string USER_ID;
        static const std::string COLOR;				//Color name used for chat bubbles

        static const std::string MEMBER_ID;
        static const std::string REFERRER_ID;
        static const std::string TYPE_ID;
        static const std::string NICKNAME;
        static const std::string IS_ADMIN;
        static const std::string IS_INVITED;
        static const std::string HAS_CONFIRMED;
        static const std::string OPT_OUT;
        static const std::string NOTIFY;			//Per discussion in API doc, this will be used in later phases.  Put in DB now, to avoid schema changes.

        //Aggregate values - NOT FIELDS IN TABLE, BUT USED IN QUERIES.
        static const std::string DISPLAY_NAME;

		//DML
		static const std::string CREATE_TABLE;
		static const std::string DELETE_TABLE;
	};


	//=============================================================================
	//Translation table
	//=============================================================================

	class Translation
	{
	private:
		Translation()		{}			//Private so it cannot be instantiated

	public:
		DLLEXPORT static const std::string TABLE;

		static const std::string ID;
		static const std::string ENABLED;
		static const std::string USER_LANGUAGE;
		static const std::string CONTACT_LANGUAGE;
		static const std::string TRANSLATE_MESSAGE_DIRECTION;
		static const std::string CONTACT_ID;
		static const std::string PHONE_NUMBER;
		static const std::string JID;

		//DML
		static const std::string CREATE_TABLE;
		static const std::string DELETE_TABLE;
	};

	//=============================================================================
	//RecentCall table
	//=============================================================================
	class RecentCall
	{
	private:
		RecentCall()		{}			//Private so it cannot be instantiated

	public:
		DLLEXPORT static const std::string TABLE;

		DLLEXPORT static const std::string ID;
		DLLEXPORT static const std::string UID;
		DLLEXPORT static const std::string DURATION;
		DLLEXPORT static const std::string TIMESTAMP;
		DLLEXPORT static const std::string START_TIMESTAMP;
		DLLEXPORT static const std::string INCOMING;
		DLLEXPORT static const std::string STATUS;
		DLLEXPORT static const std::string PHONE_NUMBER;		//Normalized
		DLLEXPORT static const std::string ORIG_PHONE_NUMBER;	//As we get from user
		DLLEXPORT static const std::string IS_LOCAL;
		DLLEXPORT static const std::string SEEN;

		//DML
		static const std::string CREATE_TABLE;
		static const std::string DELETE_TABLE;
	};

	//=============================================================================
	//Settings table
	//=============================================================================
	class Settings
	{
	private:
		Settings()		{}			//Private so it cannot be instantiated

	public:
		DLLEXPORT static const std::string TABLE;

		DLLEXPORT static const std::string USER_ID;

		DLLEXPORT static const std::string AUDIO_INPUT_DEVICE;				//String
		DLLEXPORT static const std::string AUDIO_OUTPUT_DEVICE;				//String
		DLLEXPORT static const std::string AUDIO_INPUT_VOLUME;				//int
		DLLEXPORT static const std::string AUDIO_OUTPUT_VOLUME;				//Int

		DLLEXPORT static const std::string SHOW_FAX_IN_MSG_THREAD;			//Bool
		DLLEXPORT static const std::string SHOW_VM_IN_MSG_THREAD;			//Bool
		DLLEXPORT static const std::string SHOW_RC_IN_MSG_THREAD;			//Bool

		DLLEXPORT static const std::string ENABLE_SOUND_INCOMING_MSG;		//Bool
		DLLEXPORT static const std::string ENABLE_SOUND_INCOMING_CALL;		//Bool
//		DLLEXPORT static const std::string ENABLE_SOUND_OUTGOING_MSG;		//Bool

		DLLEXPORT static const std::string MY_CHAT_BUBBLE_COLOR;			//String
		DLLEXPORT static const std::string CONVERSANT_CHAT_BUBBLE_COLOR;	//String

		DLLEXPORT static const std::string COUNTRY_CODE;					//String
		DLLEXPORT static const std::string COUNTRY_ABBREV;					//String

		DLLEXPORT static const std::string INCOMING_CALL_SOUND_FILE;		//String
//		DLLEXPORT static const std::string OUTGOING_CALL_SOUND_FILE;		//String	- This is controlled by server.
		DLLEXPORT static const std::string CALL_CLOSED_SOUND_FILE;			//String

		DLLEXPORT static const std::string WINDOW_LEFT;						//Double
		DLLEXPORT static const std::string WINDOW_TOP;						//Double
		DLLEXPORT static const std::string WINDOW_HEIGHT;					//Double
		DLLEXPORT static const std::string WINDOW_WIDTH;					//Double
		DLLEXPORT static const std::string WINDOW_STATE;					//Int

		//From API/DB calls
		DLLEXPORT static const std::string LAST_MSG_TIMESTAMP;				//Long
		DLLEXPORT static const std::string LAST_CALL_TIMESTAMP;				//Long
		DLLEXPORT static const std::string LAST_CONTACT_SOURCE_KEY;			//String

		//For SIP
		DLLEXPORT static const std::string SIP_UUID;						//String

		//DML
		static const std::string CREATE_TABLE;
		static const std::string DELETE_TABLE;
	};


	//=============================================================================
	//AppSettings table - this is NOT part of user DBs.
	//	This is APP level info and table will contain a single row of data
	//=============================================================================
	class AppSettings
	{
	private:
		AppSettings()		{}			//Private so it cannot be instantiated

	public:
		DLLEXPORT static const std::string TABLE;

		DLLEXPORT static const std::string ID;							//Unique key, used to keep a single record

		DLLEXPORT static const std::string AUTO_LOGIN;					//Boolean
		DLLEXPORT static const std::string REMEMBER_PASSWORD;			//Boolean
		DLLEXPORT static const std::string LAST_LOGIN_USERNAME;			//String
		DLLEXPORT static const std::string LAST_LOGIN_PASSWORD;			//String
		DLLEXPORT static const std::string LAST_LOGIN_COUNTRY_CODE;		//String
		DLLEXPORT static const std::string LAST_LOGIN_PHONE_NUMBER;		//String

		DLLEXPORT static const std::string DEBUG_MENU;					//String - so we can use a secret string to enable it.
		DLLEXPORT static const std::string IGNORE_LOGGERS;				//String
		DLLEXPORT static const std::string MAX_SIP_LOGGING;				//Boolean
		DLLEXPORT static const std::string GET_INIT_OPTIONS_HANDLING;	//Int
		

		//DML
		static const std::string CREATE_TABLE;
		static const std::string DELETE_TABLE;
	};

};

