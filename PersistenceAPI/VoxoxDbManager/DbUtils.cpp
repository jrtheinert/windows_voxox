/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

#include "DbUtils.h"
#include "windows.h"			//For SystemTime	//TODO: causes C4005 '__userHeader' : macro redefinition and '__on_failure' : macro redefinition on Win32 builds
#include <algorithm>			//transform()

const std::string DbUtils::mXmppServer				= "voxox.com";		//TODO: What is proper value and how to set from main app.
const size_t	  DbUtils::mPhoneNumberKeyLen		= 10;
const std::string DbUtils::mTranslatorDefaultLocale = "en";
const std::string DbUtils::mNameSep					= "|||";

//TODO: These *may* be moved to REST API, but will define here for now
// NOTE: These values are from the VoxOx servers, so do NOT change them.  Feel free to add new ones as needed.
const std::string DbUtils::mSvrMsgType_Voicemail	= "1";
const std::string DbUtils::mSvrMsgType_Fax			= "2";
const std::string DbUtils::mSvrMsgType_RecordedCall	= "3";
const std::string DbUtils::mSvrMsgType_Sms			= "4";
const std::string DbUtils::mSvrMsgType_Xmpp			= "5";

// NOTE: These values are from the VoxOx servers, so do NOT change them.  Feel free to add new ones as needed.
const std::string DbUtils::mSvrVoicemailStatus_New     = "1";
const std::string DbUtils::mSvrVoicemailStatus_Heard   = "2";
const std::string DbUtils::mSvrVoicemailStatus_Deleted = "4";

//Extension
const int	mMinExtensionLength = 3;
const int	mMaxExtensionLength = 6;

//-------------------------------------------------------------------------
// New utility methods
//-------------------------------------------------------------------------
//static
std::string	DbUtils::stringListToSelectClause( const StringList& list )
{
	const std::string SEP = ", ";
	std::string result;

	if ( list.size() > 0 )
	{
		result += " SELECT " + stringListToString( list, SEP );
	}

	return result;
}

//static 
std::string	DbUtils::stringListToString( const StringList& list, const std::string& separator )
{
	std::string result;

	if ( list.size() > 0 )
	{
		for ( StringList::const_iterator it = list.begin(); it != list.end();	it++ )
		{
			if ( it != list.begin() )
			{
				result.append( separator );
			}

			result.append( *it );
		}

		result.append( " " );
	}

	return result;
}


//static
std::string	DbUtils::intArrayToInClause( const std::list<int>& values )
{
	//If list is empty, we still return the IN ( ) clause
	std::string result = " IN ( " ;

	if ( !values.empty() )
	{
		result += intArrayToString( values );
	}

	result += " ) ";

	return result;
}

//static
bool DbUtils::contains( const std::list<int>& list, int tgtValue )
{
	bool result = false;

	for ( int value : list )
	{
		if ( value == tgtValue )
		{
			result = true;
			break;
		}
	}

	return result;
}


//static
bool DbUtils::contains( const std::list<std::string>& list, const std::string& tgtValue )
{
	bool result = false;

	for ( std::string value : list )
	{
		if ( value == tgtValue )
		{
			result = true;
			break;
		}
	}

	return result;
}


//-----------------------------------------------------------------------------
//From VoxoxUtils
//-----------------------------------------------------------------------------

//static 
std::string	DbUtils::ensureFullJid( const std::string& jid )
{
	std::string result = jid;
	std::string sep    = "@";

	size_t pos = jid.find( sep );

	if ( pos == std::string::npos )
		result =  jid + sep + mXmppServer;

	return result;
}

//static 
std::string	DbUtils::getNormalizedNumberNoPrefix( const std::string& number )
{
	if ( number.empty() )
	{
		return number;
	}
	else 
	{
		std::string normalizedNumber = digitsOnly( number );

		if ( normalizedNumber.size() > mPhoneNumberKeyLen )
		{
			//Get right-most digits.
			size_t startPos = normalizedNumber.size() - mPhoneNumberKeyLen;
			normalizedNumber = normalizedNumber.substr( startPos, mPhoneNumberKeyLen );
		}

		return normalizedNumber;
	}
}

//static 
std::string	DbUtils::getNormalizedNumber( const std::string& number )
{
	if ( number.empty() ) 
	{
		return number;
	} 
	else 
	{
		return digitsOnly( number );	
	}
}

//static
std::string DbUtils::digitsOnly( const std::string& valueIn )
{
	std::string result;
	char c;

	for ( size_t x = 0; x < valueIn.size(); x++ )
	{
		c = valueIn.at(x);

		if ( c >= '0' && c <= '9' )
			result += c;
	}

	return result;
}

//static
//This is equivalent to ManagedDataTypes::VxTimeStamp::GetNowTimestamp()
__int64 DbUtils::getSystemTimeMillis()
{
	FILETIME fileTime;		//100-nanosecond intervals, so divide by 10,000 to get millis
	::GetSystemTimeAsFileTime( &fileTime );

	//Method 1
	__int64 nanos;
	memcpy( &nanos, &fileTime, 8);

	return nanos / 10000;

	return 0;
}

//static
std::string DbUtils::convertMillisToServerTimeStamp( __int64 timestamp )	//TODO and TODO-Mac
{
	std::string result;

	//Here is code from -Android-
//	private final static String REST_SERVER_TIMESTAMP_FORMAT = "yyyy-MM-dd HH:mm:ss";
//	private static final String DEFAULT_TIMEZONE = "America/Los_Angeles";
//	TimeZone tz = TimeZone.getTimeZone( DEFAULT_TIMEZONE );
//	Date date = new Date( timestamp );
//	DateFormat formatter = new SimpleDateFormat(REST_SERVER_TIMESTAMP_FORMAT, Locale.US);
//	formatter.setTimeZone(tz);
//	return formatter.format(date);

	return result;
}

std::string DbUtils::getNameSep()
{
	return mNameSep;
}


//-----------------------------------------------------------------------------
//From Database
//-----------------------------------------------------------------------------
	
//static 
std::string DbUtils::escapeParameter( const std::string& paramIn )
{
	std::string result;

	if ( !paramIn.empty() )
	{
		result = replace( paramIn, "'",  "''", false );		//Simplistic.  Just replace one single quote with two single quotes (not double quotes).
	}

	return result;
}

//-----------------------------------------------------------------------------
//From DatabaseManagerHelper
//-----------------------------------------------------------------------------

//static
std::string DbUtils::intArrayToString( const int values[], int size ) 
{
	std::string result;

	for ( int x = 0; x < size; x++ ) 
	{
		if ( x > 0 )
			result.append( ", ");
	
		result.append( std::to_string( values[x] ) );
	}

	return result;
}

//static 
std::string DbUtils::intArrayToString( const std::list<int> values )
{
	std::string result;
	int			item = 0;

	for ( int value : values )
	{
		if ( item > 0 )
			result.append( ", ");
	
		result.append( std::to_string( value ) );

		item++;
	}

	return result;
}

//static
PhoneNumber DbUtils::buildPhoneNumber( const std::string& requestedPhoneNumberString, const PhoneNumberList& pnList )
{
	PhoneNumber result;
	PhoneNumber tempFound;
	std::string contactName = requestedPhoneNumberString; // may be overridden if we have an address book contact (most common scenario)

	if ( !pnList.empty() ) 
	{
		// find the requested number in the list as we may have several entries that match the database query.
		if ( !requestedPhoneNumberString.empty() ) 
		{
			bool		numberFound = false;
			std::string requestedPhoneNumberNormalized = DbUtils::getNormalizedNumberNoPrefix( requestedPhoneNumberString );

			for ( PhoneNumber number : pnList )
			{
				if ( number.getNumber() == requestedPhoneNumberNormalized )
				{
					tempFound = number;
					numberFound = true;
					break;
				}
			}
		}

		std::string phoneNumberString        = requestedPhoneNumberString.empty() ? tempFound.getNumber()        : DbUtils::getNormalizedNumberNoPrefix(requestedPhoneNumberString);
		std::string displayPhoneNumberString = requestedPhoneNumberString.empty() ? tempFound.getDisplayNumber() : requestedPhoneNumberString;

		if ( !tempFound.getName().empty() )
			contactName = tempFound.getName();

		// build the display name as it may be compound of two or more names.
		// it may happen if the same phone number is associated with more than one contact.
		std::string				displayName;
		std::list<std::string>	contactKeys;
		std::list<int[2]>		androidAndCmGroupIds;

		int	cmGroup = tempFound.getCmGroup();

		// go over all contacts id's in the cursor's list and build the display name.
		for ( PhoneNumber number : pnList )		//TODO-DB: Not getting compound name.  (I think)
		{
//			int currentAndroidId = number.getAndroidId();
			std::string currentContactKey = number.getContactKey();

//			if ( ! DbUtils::contains( androidIds, currentAndroidId ) ) 
			if ( ! DbUtils::contains( contactKeys, currentContactKey ) ) 
			{
				std::string nameTemp = tempFound.getName();

				//Do not include if exact name already exists.
				if ( !nameTemp.empty() && !DbUtils::contains( displayName, nameTemp, true ) && number.getCmGroup() == cmGroup) 
				{
					if ( !displayName.empty() )
					{
						displayName += DbUtils::getNameSep();		//This will be replaced with proper text when we have context.
					}

//					displayName += number.getName();
					displayName += nameTemp;
				}

				// add only valid android id's to the list.
				// - we may have JID contacts with only a phone number. Their android Id will be -1 (if we received a CHAT message from a contact not in address book)
				// - VoxOx did numbers may not have an android Id (common case). But they will have android Id's associated with then through the mobile registration number.
				// so it is a valid case to have phone number instance with android id = -1, and android Id's list associated with it.
				//TODO: 2015.10.29 - Revisit this quickly
//				if (currentAndroidId > 0) 
//				{
////					androidIds.push_back( currentAndroidId );
//					contactKeys.push_back( currentContactKey );
//
////					int temp[] = {currentAndroidId, number.getCmGroup() };
////					androidAndCmGroupIds.push_back( temp );
//				}
			}
		}

		if ( displayName.empty() )
		{
			displayName = contactName;
		}

		//We have all the data we need, lets build the PhoneNumber instance.
		//	- Copy number we matched on, then update a couple fields.
		result = tempFound;

		result.setDisplayName( displayName );
		result.setDisplayNumber( displayPhoneNumberString );
		result.setName( contactName );
//		result.setContactKeyList( androidAndCmGroupIds );		//TODO: Add this to PhoneNumber class, but it CANNOT be a simple std::list<int[2]> memvar.
	}

	if ( !result.isValid() ) 
	{
		std::string normalizedPhoneNumber = DbUtils::getNormalizedNumberNoPrefix( requestedPhoneNumberString );

		result.setNumber       ( normalizedPhoneNumber      );
		result.setDisplayNumber( requestedPhoneNumberString );
		result.setName		   ( contactName				);
		result.setDisplayName  ( contactName				);
	}

	return result;

}

//-----------------------------------------------------------------------------
//From String Util or String/StringList related
//-----------------------------------------------------------------------------
//static
std::string DbUtils::replace( const std::string& text, const std::string& before, const std::string& after, bool caseSensitive )
{
	//Copy this + before to tmp + before2
	std::string result  = text;
	std::string temp    = text;
	std::string before2 = before;

	if (!caseSensitive) 
	{
		//Converts tmp + before2 to lower case
		temp    = toLowerCase( temp    );
		before2 = toLowerCase( before2 );
	}

	//Searches on tmp + before2 rather than this + before
	std::string::size_type pos = 0;

	while ((pos = temp.find(before2, pos)) != std::string::npos) 
	{
		//Replaces in 'result' based on position found in 'temp'.
		result.replace(pos, before2.length(), after);
		temp.replace  (pos, before2.length(), after);	//Replace in temp, so we don't find() it again.
		pos = pos + after.length();
	}

	return result;
}

//static 
std::string DbUtils::toLowerCase( const std::string& text )
{
	std::string result = text;
	std::transform( result.begin(), result.end(), result.begin(), (int(*)(int)) tolower);
	return result;
}
//static
bool DbUtils::contains( const std::string& text, const std::string& str, bool caseSensitive )
{
	bool		result = false;
	std::string tmp    = text;
	std::string str2   = str;

	if (!caseSensitive) 
	{
		//Converts tmp + str2 to lower case
		tmp  = toLowerCase(tmp);
		str2 = toLowerCase(str2);
	}

	result = (tmp.find(str2, 0) != std::string::npos) ;

	return result;
}

//static
std::string DbUtils::splitLeft( const std::string& textIn, const std::string& delim )
{
	std::string result = textIn;	//Default is delim is not found.

    if ( !textIn.empty() && !delim.empty() ) 
	{
	    size_t pos = textIn.find( delim );

		if ( pos != std::string::npos )
		{
			result = textIn.substr( 0, pos );
		}
    }

	return result;
}

//static
std::string DbUtils::parseName( const std::string& xmppAddress )
{
	return splitLeft( xmppAddress, "@" );
}

//static
std::string DbUtils::parseBareAddress( const std::string& xmppAddress )
{
	return splitLeft( xmppAddress, "/" );
}

//static
std::string DbUtils::getNthListEntry(const StringList& list, size_t tgtItem)
{
	std::string result;

	if  (tgtItem < list.size())
	{
		int item = 0;

		for (StringList::const_iterator it = list.begin(); it != list.end(); it++)
		{
			if (item == tgtItem)
			{
				result = (*it);
				break;
			}

			item++;
		}
	}

	return result;

}

bool DbUtils::isValidExtensionLength( int len )
{
	return len >= mMinExtensionLength && len <= mMaxExtensionLength;
}
