
/* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

/*
* Small class to broadcast DB change events to registered listeners.
*/

#pragma once

#include "DataTypes.h"	//For DbChangeData and Message
#include "DllExport.h"

#include <string>

//=============================================================================

class BroadcastManager
{
public:
	BroadcastManager();
	~BroadcastManager();

	void setCallback( DbChangeCallbackFunc func );

	DbChangeData operator()()	{}

	//Convenience methods to trigger events.
	void msgChange      ();
	void msgAdded       ( size_t count );
	void msgStatusChange( Message::Status status );

	void contactChange( int voxoxUserId );

	void gmChange();
	void gmChange( const std::string& groupId, DbChangeData::Action action );

	void recentCallChange();


private:
	 void broadcast( const DbChangeData& data );

private:
	DbChangeCallbackFunc mCallback;
};

