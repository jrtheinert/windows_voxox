/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

#include "ContentValues.h"
#include <assert.h>

//=============================================================================
//Various classes to support different and supported data types
//=============================================================================

DataType::DataType()
{
	initVars();

	//No values to override.  This is Type_Null.
}

DataType::DataType( const std::string& value )
{
	initVars();

	mType   = Type_String;
	mString = value;
}

DataType::DataType( int value )
{
	initVars();

	mType = Type_Long;
	mLong = value;
}

DataType::DataType( long value )
{
	initVars();

	mType = Type_Long;
	mLong = value;
}

DataType::DataType( __int64 value )
{
	initVars();

	mType = Type_Long;
	mLong = value;
}

DataType::DataType( float value )
{
	initVars();

	mType  = Type_Float;
	mFloat = value;
}

DataType::DataType( double value )
{
	initVars();

	mType   = Type_Double;
	mDouble = value;
}

DataType::DataType( bool value )
{
	initVars();

	mType = Type_Bool;
	mBool = value;
}

DataType::~DataType()
{
}

void DataType::initVars()
{
	mType = Type_Null;

	mString.clear();
	mLong   = 0;
	mFloat  = 0.0f;
	mDouble = 0.0;
	mBool   = false;
}

//Currently, I am using strict type checking.  This may change, especially when we want value AS STRING
//	TODO: Throw exception if we cannot convert data type.
std::string DataType::getValueAsString()
{
	std::string result;

	switch ( mType )
	{
	case Type_Null:
		break;

	case Type_String:
		result = mString;
		break;

	case Type_Long:
		result = std::to_string( mLong );		//C++11.	Does Mac support this?
		break;

	case Type_Float:
		result = std::to_string( mFloat );		//C++11.	Does Mac support this?
		break;

	case Type_Double:
		result = std::to_string( mDouble );		//C++11.	Does Mac support this?
		break;

	case Type_Bool:
		result = (mBool ? "1" : "0");			//SQLite stores bool as int
		break;

	default:
		assert( false );						//New type?
	}

	return result;
}

__int64 DataType::getValueAsLong()
{
	__int64 result = 0;

	switch ( mType )
	{
	case Type_Null:		//For NULL, we use the default value of mLong, in case we want to have a different default.
	case Type_Long:
		result = mLong;
		break;
		break;

	case Type_String:
		result = std::stoi( mString );	//C++11.	Does Mac support this?	
		break;

	case Type_Float:
		result = (long)mFloat;			//May lose data
		break;

	case Type_Double:
		result = (long)mDouble;			//May lose data
		break;

	case Type_Bool:
		result = (mBool ? 1 : 0);		//SQLite stores bool as int
		break;

	default:
		assert( false );				//New type?
	}

	return result;
}

float DataType::getValueAsFloat()
{
	float result = 0.0f;

	switch ( mType )
	{
	case Type_Null:		//For NULL, we use the default value of mLong, in case we want to have a different default.
	case Type_Float:
		result = mFloat;
		break;
		break;

	case Type_String:
		result = std::stof( mString );	//C++11.	Does Mac support this?	
		break;

	case Type_Long:
		result = (float)mLong;
		break;

	case Type_Double:
		result = (float)mDouble;		//May lose precision
		break;

	case Type_Bool:
		result = (mBool ? 1.0f : 0.0f);	//SQLite stores bool as int
		break;

	default:
		assert( false );				//New type?
	}

	return result;
}

double DataType::getValueAsDouble()
{
	double result = 0.0f;

	switch ( mType )
	{
	case Type_Null:						//For NULL, we use the default value of mLong, in case we want to have a different default.
	case Type_Double:
		result = mDouble;
		break;
		break;

	case Type_String:
		result = std::stod( mString );	//C++11.	Does Mac support this?	
		break;

	case Type_Long:
		result = (double)mLong;
		break;

	case Type_Float:
		result = (double)mFloat;
		break;

	case Type_Bool:
		result = (mBool ? 1.0 : 0.0);	//SQLite stores bool as int
		break;

	default:
		assert( false );				//New type?
	}

	return result;
}

bool DataType::getValueAsBool()
{
	bool result = false;

	switch ( mType )
	{
	case Type_Null:		//For NULL, we use the default value of mLong, in case we want to have a different default.
	case Type_Bool:
		result = mBool;
		break;

	case Type_String:
		result = (std::stoi( mString ) == 1);	//C++11.	Does Mac support this?		//Or maybe != 0?
		break;

	case Type_Long:
		result = (int)mLong != 0;
		break;

	case Type_Float:
		result = (int)mFloat != 0;
		break;

	case Type_Double:
		result = (int)mDouble != 0;
		break;

	default:
		assert( false );		//New type?
	}

	return result;
}


//=============================================================================


//=============================================================================

ContentValues::ContentValues()
{
}

ContentValues::~ContentValues()
{
}

ContentValues::ContentValues( const ContentValues& from) {
        mValues = from.mValues;
}


//----------------------------------------------------
//Group of methods to PUT key/values into map
//----------------------------------------------------

void ContentValues::add( const std::string& key, DataType data )
{
	std::pair<std::string, DataType> pair( key, data );
	mValues.insert( pair );
}

void ContentValues::putAll( ContentValues other ) 
{
	mValues = other.mValues;
}

void ContentValues::put( const std::string& key, const std::string& value )	
{
	add( key, DataType( value ) );
}

void ContentValues::put( const std::string& key, const char* value )	
{
	put( key, std::string( value ) );
}

void ContentValues::put( const std::string& key, bool value) 
{
	add( key, DataType( value ) );
}

void ContentValues::put( const std::string& key, int value) 
{
	add( key, DataType( value ) );
}

void ContentValues::put( const std::string& key, long value) 
{
	add( key, DataType( value ) );
}

void ContentValues::put( const std::string& key, __int64 value) 
{
	add( key, DataType( value ) );
}

void ContentValues::put( const std::string& key, float value) 
{
	add( key, DataType( value ) );
}

void ContentValues::put( const std::string& key, double value) 
{
	add( key, DataType( value ) );
}

void ContentValues::putNull( const std::string& key ) 
{
	add( key, DataType() );
}

size_t ContentValues::size() const
{ 
	return mValues.size();	
}

void ContentValues::remove( const std::string& key )
{ 
	mValues.erase( key );
}

void ContentValues::clear()
{ 
	mValues.clear();			
}

bool ContentValues::containsKey( const std::string& key) 
{
	KeyValueMap::iterator it = mValues.find( key );
	return it != mValues.end();
}

//Object ContentValues::get(String key) {
//        return mValues.get(key);
//    }

std::string ContentValues::getAsString( const std::string& key ) 
{
	std::string result;

	KeyValueMap::iterator it = mValues.find( key );

	if ( it != mValues.end() )
	{
		result = (*it).second.getValueAsString();
	}

	return result;
}

__int64 ContentValues::getAsLong( const std::string& key ) 
{
	__int64 result = 0;

	KeyValueMap::iterator it = mValues.find( key );

	if ( it != mValues.end() )
	{
		result = (*it).second.getValueAsLong();
	}

	return result;
}

float ContentValues::getAsFloat( const std::string& key) 
{
	float result = 0.0f;

	KeyValueMap::iterator it = mValues.find( key );

	if ( it != mValues.end() )
	{
		result = (*it).second.getValueAsFloat();
	}

	return result;
}

double ContentValues::getAsDouble( const std::string& key) 
{
	double result = 0.0;

	KeyValueMap::iterator it = mValues.find( key );

	if ( it != mValues.end() )
	{
		result = (*it).second.getValueAsDouble();
	}

	return result;
}

bool ContentValues::getAsBoolean( const std::string& key) 
{
	bool result = false;

	KeyValueMap::iterator it = mValues.find( key );

	if ( it != mValues.end() )
	{
		result = (*it).second.getValueAsBool();
	}

	return result;
}

//    Set<Map.Entry<String, Object>> valueSet() {
//        return mValues.entrySet();
//}

//    Set<String> keySet() {
//        return mValues.keySet();
//    }

//	public String toString() {
//		StringBuilder sb = new StringBuilder();
//		for (String name : mValues.keySet()) {
//			String value = getAsString(name);
//			if (sb.length() > 0) sb.append(" ");
//			sb.append(name + "=" + value);
//		}
//		return sb.toString();
//	}

