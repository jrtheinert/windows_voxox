/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

#include "VoxoxSQLite.h"
#include "sqlite3.h"

namespace VoxoxSQLite
{

const char* VoxoxSQLiteDb::mBeginTransactionSql		= "BEGIN TRANSACTION";
const char* VoxoxSQLiteDb::mCommitTransactionSql	= "COMMIT TRANSACTION";
const char* VoxoxSQLiteDb::mRollbackTransactionSql	= "ROLLBACK TRANSACTION";
const char* VoxoxSQLiteDb::mSynchronousOff			= "PRAGMA synchronous = OFF";
const char* VoxoxSQLiteDb::mSynchronousOn			= "PRAGMA synchronous = ON";

static const char* s_columnType[] = { "UNKNOWN", "INTEGER", "FLOAT", "TEXT", "BLOB", "NULL" };

static const char* getColumnTypeDesc( int type )
{
	if ( type < 0 || type > 5 )
		type = 0;

	return s_columnType[type];
}

//=============================================================================
//	VoxoxSQLiteError class - Collection of static methods that work for other classes
//=============================================================================
//static
bool VoxoxSQLiteError::isOk( int value )
{
	return value == SQLITE_OK;
}

//static
bool VoxoxSQLiteError::isError( int value )
{
	return value != SQLITE_OK;
}

//static
bool VoxoxSQLiteError::isDone( int value )
{
	return value == SQLITE_DONE;
}

//static
bool VoxoxSQLiteError::hasRow( int value )
{
	return value == SQLITE_ROW;
}

void VoxoxSQLiteError::checkDB( sqlite3* db )
{
	if ( !db )
	{
		throw VoxoxSQLiteException( VOXOX_SQLITE_ERROR, "Database not open" );
	}
}

void VoxoxSQLiteError::checkStatement( sqlite3_stmt* statement )
{
	if ( statement == NULL )
	{
		throw VoxoxSQLiteException( VOXOX_SQLITE_ERROR, "Null Virtual Machine pointer" );
	}
}

//static
const char* VoxoxSQLiteError::getDbErrorMsg( sqlite3* db )
{
	return sqlite3_errmsg( db );
}

//static
void VoxoxSQLiteError::throwVoxoxSQLiteException( const char* errMsg )
{
	throw VoxoxSQLiteException( VOXOX_SQLITE_ERROR, errMsg );
}

//static
bool VoxoxSQLiteError::throwExceptionOnError(  int errorCode, sqlite3* db )
{
	return throwExceptionOnError( errorCode, db, NULL );
}

//static 
bool VoxoxSQLiteError::throwExceptionOnError( int errorCode, sqlite3* db, const char* errMsg )
{
	bool thrown = false;

	if ( isError( errorCode ) )
	{
		std::string msg = ( db == NULL ? "No msg because DB is NULL" : getDbErrorMsg( db ) );

		if ( errMsg != NULL )
		{
			msg += " - Spec: ";
			msg += errMsg;
		}

		throw VoxoxSQLiteException( errorCode, msg.c_str() );
	}

	return thrown;
}

//=============================================================================


	
//=============================================================================
//	VoxoxSQLiteException class
//=============================================================================
VoxoxSQLiteException::VoxoxSQLiteException( const int errCode, const char* errMsg ) : mErrorCode( errCode ), mErrorMsg( errMsg )
{
}

VoxoxSQLiteException::VoxoxSQLiteException( const VoxoxSQLiteException& e ) : mErrorCode( e.mErrorCode ), mErrorMsg( e.mErrorMsg )
{
}

VoxoxSQLiteException::~VoxoxSQLiteException()
{
}

//static
const char* VoxoxSQLiteException::errorCodeToString( int nErrCode )
{
	switch (nErrCode)
	{
		case SQLITE_OK          : return "SQLITE_OK";
		case SQLITE_ERROR       : return "SQLITE_ERROR";
		case SQLITE_INTERNAL    : return "SQLITE_INTERNAL";
		case SQLITE_PERM        : return "SQLITE_PERM";
		case SQLITE_ABORT       : return "SQLITE_ABORT";
		case SQLITE_BUSY        : return "SQLITE_BUSY";
		case SQLITE_LOCKED      : return "SQLITE_LOCKED";
		case SQLITE_NOMEM       : return "SQLITE_NOMEM";
		case SQLITE_READONLY    : return "SQLITE_READONLY";
		case SQLITE_INTERRUPT   : return "SQLITE_INTERRUPT";
		case SQLITE_IOERR       : return "SQLITE_IOERR";
		case SQLITE_CORRUPT     : return "SQLITE_CORRUPT";
		case SQLITE_NOTFOUND    : return "SQLITE_NOTFOUND";
		case SQLITE_FULL        : return "SQLITE_FULL";
		case SQLITE_CANTOPEN    : return "SQLITE_CANTOPEN";
		case SQLITE_PROTOCOL    : return "SQLITE_PROTOCOL";
		case SQLITE_EMPTY       : return "SQLITE_EMPTY";
		case SQLITE_SCHEMA      : return "SQLITE_SCHEMA";
		case SQLITE_TOOBIG      : return "SQLITE_TOOBIG";
		case SQLITE_CONSTRAINT  : return "SQLITE_CONSTRAINT";
		case SQLITE_MISMATCH    : return "SQLITE_MISMATCH";
		case SQLITE_MISUSE      : return "SQLITE_MISUSE";
		case SQLITE_NOLFS       : return "SQLITE_NOLFS";
		case SQLITE_AUTH        : return "SQLITE_AUTH";
		case SQLITE_FORMAT      : return "SQLITE_FORMAT";
		case SQLITE_RANGE       : return "SQLITE_RANGE";
		case SQLITE_ROW         : return "SQLITE_ROW";
		case SQLITE_DONE        : return "SQLITE_DONE";

		case VOXOX_SQLITE_ERROR    : return "VOXOXDB_SQLITE_ERROR";

		default: return "UNKNOWN_ERROR";
	}
}

//=============================================================================

//TODO: Specific DB exceptions in addition to the generic one above.

//=============================================================================
//	VoxoxSQLiteBuffer class
//=============================================================================

VoxoxSQLiteBuffer::VoxoxSQLiteBuffer() : mBuf( NULL )
{
}

VoxoxSQLiteBuffer::~VoxoxSQLiteBuffer()
{
	clear();
}

void VoxoxSQLiteBuffer::clear()
{
	if ( mBuf )
	{
		sqlite3_free( mBuf );
		mBuf = NULL;
	}
}

const char* VoxoxSQLiteBuffer::format( const char* szFormat, ... )
{
	clear();
	va_list va;
	va_start(va, szFormat);
	mBuf = sqlite3_vmprintf(szFormat, va);
	va_end(va);

	return mBuf;
}

//=============================================================================


//=============================================================================
//	VoxoxSQLiteCursor class
//=============================================================================

VoxoxSQLiteCursor::VoxoxSQLiteCursor()
{
	mDb			  = NULL;
	mStatement    = NULL;
	mEof	      = true;
	mCols		  = 0;
	mOwnStatement = false;
}

VoxoxSQLiteCursor::VoxoxSQLiteCursor( const VoxoxSQLiteCursor& src )
{
	mDb			  = src.mDb;
	mStatement	  = src.mStatement;
	mEof		  = src.mEof;
	mCols		  = src.mCols;
	mOwnStatement = src.mOwnStatement;

	// Only one object can own the VM
	const_cast<VoxoxSQLiteCursor&>(src).mStatement = NULL;
}

VoxoxSQLiteCursor::VoxoxSQLiteCursor( sqlite3* db, sqlite3_stmt* statement, bool eof, bool ownStatement )
{
	mDb			  = db;
	mStatement	  = statement;
	mEof		  = eof;
	mCols		  = sqlite3_column_count( mStatement );
	mOwnStatement = ownStatement;
}


VoxoxSQLiteCursor::~VoxoxSQLiteCursor()
{
	try
	{
		finalize();
	}
	catch (...)
	{
		//TODO: Throw exception?
	}
}

VoxoxSQLiteCursor& VoxoxSQLiteCursor::operator=( const VoxoxSQLiteCursor& src )
{
	if ( this != &src )
	{
		try
		{
			finalize();
		}
		catch (...)
		{
		}

		mDb		      = src.mDb;
		mStatement    = src.mStatement;
		mEof          = src.mEof;
		mCols         = src.mCols;
		mOwnStatement = src.mOwnStatement;

		// Only one object can own the VM
		const_cast<VoxoxSQLiteCursor&>(src).mStatement = NULL;
	}

	return *this;
}

void VoxoxSQLiteCursor::checkStatement()
{ 
	VoxoxSQLiteError::checkStatement( mStatement );	
}

void VoxoxSQLiteCursor::checkFieldIndex( int index )
{
	if ( !isFieldIndexValid( index ) )
	{
		VoxoxSQLiteError::throwVoxoxSQLiteException( "Invalid field index requested" );
	}
}

bool VoxoxSQLiteCursor::isValid()
{
	return mStatement != NULL;
}

int VoxoxSQLiteCursor::numFields()
{
	checkStatement();
	return mCols;
}

bool VoxoxSQLiteCursor::isFieldIndexValid( int index )
{
	return ( index >= 0 && index <= mCols - 1);
}

const char* VoxoxSQLiteCursor::fieldValue( int index )
{
	checkStatement();

	if ( !isFieldIndexValid( index ) )
	{
		VoxoxSQLiteError::throwVoxoxSQLiteException( "Invalid field index requested" );
	}

	return (const char*)sqlite3_column_text( mStatement, index );
}

const char* VoxoxSQLiteCursor::fieldValue( const char* name )
{
	int index = fieldIndex( name );
	return (const char*)sqlite3_column_text( mStatement, index );
}

//-----------------------------------------------------------------------------
// Methods to get field value by Field Index
//-----------------------------------------------------------------------------

int VoxoxSQLiteCursor::getInt( int index, int nullValue /*=0*/ )
{
	if ( fieldIsNull( index ) )
	{
		return nullValue;
	}
	else
	{
		return sqlite3_column_int( mStatement, index );
	}
}

sqlite_int64 VoxoxSQLiteCursor::getInt64( int index, sqlite_int64 nullValue /*=0*/ )
{
	if ( fieldIsNull( index ) )
	{
		return nullValue;
	}
	else
	{
		return sqlite3_column_int64( mStatement, index );
	}
}

double VoxoxSQLiteCursor::getFloat( int index, double nullValue /*=0.0*/ )
{
	if ( fieldIsNull( index ) )
	{
		return nullValue;
	}
	else
	{
		return sqlite3_column_double( mStatement, index );
	}
}

const char* VoxoxSQLiteCursor::getString( int index, const char* nullValue /*=""*/ )
{
	if ( fieldIsNull( index ) )
	{
		return nullValue;
	}
	else
	{
		return (const char*)sqlite3_column_text( mStatement, index );
	}
}

const unsigned char* VoxoxSQLiteCursor::getBlob( int index, int& len)
{
	checkStatement();
	checkFieldIndex( index );

	len = sqlite3_column_bytes( mStatement, index );
	return (const unsigned char*)sqlite3_column_blob( mStatement, index );
}

//-----------------------------------------------------------------------------
// Methods to get field value by Field index, but return nullValue if index == -1
//	- These are convenience methods
//-----------------------------------------------------------------------------

int	VoxoxSQLiteCursor::getIntEx( int index, int nullValue /*= 0*/ )
{
	if ( index < 0 )
		return nullValue;
	else
		return getInt( index, nullValue );
}

double VoxoxSQLiteCursor::getFloatEx( int index, double nullValue /*= 0.0*/ )
{
	if ( index < 0 )
		return nullValue;
	else
		return getFloat( index, nullValue );
}

const char* VoxoxSQLiteCursor::getStringEx( int index, const char* nullValue /*= ""*/  )
{
	if ( index < 0 )
		return nullValue;
	else
		return getString( index, nullValue );
}

sqlite_int64 VoxoxSQLiteCursor::getInt64Ex( int index, sqlite_int64 nullValue /*= 0*/ )
{
	if ( index < 0 )
		return nullValue;
	else
		return getInt64( index, nullValue );
}

const unsigned char* VoxoxSQLiteCursor::getBlobEx( int index, int& len )
{
	return getBlob( index, len );
}

//-----------------------------------------------------------------------------
// Methods to get field value by Field name
//-----------------------------------------------------------------------------

int VoxoxSQLiteCursor::getInt(const char* szField, int nNullValue/*=0*/)
{
	int nField = fieldIndex(szField);
	return getInt(nField, nNullValue);
}

sqlite_int64 VoxoxSQLiteCursor::getInt64(const char* szField, sqlite_int64 nNullValue/*=0*/)
{
	int nField = fieldIndex(szField);
	return getInt64(nField, nNullValue);
}

double VoxoxSQLiteCursor::getFloat(const char* szField, double fNullValue/*=0.0*/)
{
	int nField = fieldIndex(szField);
	return getFloat(nField, fNullValue);
}

const char* VoxoxSQLiteCursor::getString(const char* szField, const char* szNullValue/*=""*/)
{
	int nField = fieldIndex(szField);
	return getString(nField, szNullValue);
}

const unsigned char* VoxoxSQLiteCursor::getBlob(const char* szField, int& nLen)
{
	int nField = fieldIndex(szField);
	return getBlob(nField, nLen);
}

bool VoxoxSQLiteCursor::fieldIsNull( int index )
{
	return ( fieldDataType( index ) == SQLITE_NULL );
}

bool VoxoxSQLiteCursor::fieldIsNull( const char* name )
{
	int index = fieldIndex( name );
	return (fieldDataType( index ) == SQLITE_NULL);
}

int VoxoxSQLiteCursor::fieldIndexOrThrowException( const char* name )
{
	return fieldIndex( name, true );
}

int VoxoxSQLiteCursor::fieldIndex( const char* name )
{
	return fieldIndex ( name, false );		//Do NOT throw on error.
}

int VoxoxSQLiteCursor::fieldIndex( const char* name, bool throwOnError )
{
	int result = -1;

	checkStatement();

	if ( name )
	{
		for (int index = 0; index < mCols; index++)
		{
			const char* szTemp = sqlite3_column_name( mStatement, index );

			if (strcmp( name, szTemp) == 0)
			{
				result = index;
				break;
			}
		}
	}

	if ( result < 0 && throwOnError )
		VoxoxSQLiteError::throwVoxoxSQLiteException( "Invalid field name requested" );

	return result;
}

void VoxoxSQLiteCursor::throwErrorOnInvalidIndex( int index )
{
	if ( !isFieldIndexValid( index ) )
	{
		VoxoxSQLiteError::throwVoxoxSQLiteException( "Invalid field index requested" );
	}
}

const char* VoxoxSQLiteCursor::fieldName( int index )
{
	checkStatement();

	throwErrorOnInvalidIndex( index );

	return sqlite3_column_name( mStatement, index );
}

//const char* VoxoxSQLiteCursor::fieldDeclType( int index )
//{
//	checkStatement();
//
//	throwErrorOnInvalidIndex( index );
//
//	return sqlite3_column_decltype( mStatement, index );
//}

const char* VoxoxSQLiteCursor::fieldType( int index )
{
	return getColumnTypeDesc( fieldDataType( index ) );
}

int VoxoxSQLiteCursor::fieldDataType( int index )
{
	checkStatement();

	throwErrorOnInvalidIndex( index );

	return sqlite3_column_type( mStatement, index );
}

bool VoxoxSQLiteCursor::eof()
{
	checkStatement();
	return mEof;
}

void VoxoxSQLiteCursor::nextRow()
{
	checkStatement();

	int result = sqlite3_step( mStatement );

	if ( result == SQLITE_DONE )
	{
		// no rows
		mEof = true;
	}
	else if ( result == SQLITE_ROW )
	{
		// more rows, nothing to do
	}
	else
	{
		result = sqlite3_finalize( mStatement );
		mStatement = NULL;

		VoxoxSQLiteError::throwExceptionOnError( result, mDb );
	}
}

void VoxoxSQLiteCursor::finalize()
{
	if ( mStatement && mOwnStatement )
	{
		int result = sqlite3_finalize( mStatement );
		mStatement = NULL;

		VoxoxSQLiteError::throwExceptionOnError( result, mDb );
	}
}

std::string VoxoxSQLiteCursor::toString()
{
	std::string result;

	if ( numFields() == 0 )
	{
		result = "Cursor has ZERO fields.";
	}
	else
	{
		//Get field names/types
		std::string hdr;
		for ( int fld = 0; fld < numFields(); fld++)
		{
			if ( fld > 0 )
				hdr += " | ";

			hdr += fieldName(fld);
			hdr += "(";
			hdr += fieldType(fld);
			hdr += ")";
		}

		hdr += "\n";

		result += hdr;

		//Get field values.
		if ( eof() )
		{
			result += "  >>> No rows.\n";
		}
		else
		{
			while ( !eof() )
			{
				std::string values;
				for ( int fld = 0; fld < numFields(); fld++)
				{
					if ( fld > 0 )
						values += " | ";

					values += fieldValue(fld);
				}

				values += "\n";

				result += values;

				nextRow();
			}
		}
	}

	return result;
}

//=============================================================================


//=============================================================================
//	VoxoxSQLiteStatement class
//=============================================================================

VoxoxSQLiteStatement::VoxoxSQLiteStatement() : mDb( NULL ), mStatement( NULL )
{
}

//VoxoxSQLiteStatement::VoxoxSQLiteStatement( const VoxoxSQLiteStatement& src )
//{
//	mDb		   = src.mDb;
//	mStatement = src.mStatement;
//
//	// Only one object can own VM
//	const_cast<VoxoxSQLiteStatement&>(src).mStatement = NULL;
//}

VoxoxSQLiteStatement::VoxoxSQLiteStatement( sqlite3* db, sqlite3_stmt* statement ) : mDb( db ), mStatement( statement )
{
}

VoxoxSQLiteStatement::VoxoxSQLiteStatement( sqlite3* db, const char* sql, const StringList& args ) : mDb( db ), mStatement( NULL )
{
	checkDB();

	const char*   tail      = NULL;

	int result = sqlite3_prepare_v2( mDb, sql, -1, &mStatement, &tail);

	//If we have SQL parsing error, let's capture the SQL in the exception
	if ( VoxoxSQLiteError::isError( result ) )
	{
		std::string errMsg;

		if ( result == SQLITE_ERROR )		//SQL Parse error
		{
			errMsg += "  SQL: ";
			errMsg += sql;
		}

		VoxoxSQLiteError::throwExceptionOnError( result, mDb, errMsg.c_str() );
	}

	if ( args.size() > 0 )
	{
		//For our purposes all args are 'string'.
		int paramIndex = 1; //One-based
		for ( StringList::const_iterator it = args.begin(); it != args.end(); it++ )
		{
			bind( paramIndex, (*it).c_str() );
			paramIndex++;
		}
	}

	VoxoxSQLiteError::throwExceptionOnError( result, mDb );
}

VoxoxSQLiteStatement::~VoxoxSQLiteStatement()
{
	try
	{
		finalize();		//Destroys mStatement
	}
	catch (...)
	{
		//TODO: throw exception
	}
}

VoxoxSQLiteStatement& VoxoxSQLiteStatement::operator=( const VoxoxSQLiteStatement& src )
{
	mDb        = src.mDb;
	mStatement = src.mStatement;

	// Only one object can own VM
	const_cast<VoxoxSQLiteStatement&>(src).mStatement = NULL;
	return *this;
}

void VoxoxSQLiteStatement::checkDB()
{
	VoxoxSQLiteError::checkDB( mDb );
}

void VoxoxSQLiteStatement::checkStatement()
{
	VoxoxSQLiteError::checkStatement( mStatement );
}

int VoxoxSQLiteStatement::execDml()
{
	int rowsChanged = 0;

	checkDB();
	checkStatement();

	int result = sqlite3_step( mStatement );

	if ( VoxoxSQLiteError::isOk ( result ) || VoxoxSQLiteError::isDone ( result ) )
	{
		rowsChanged = sqlite3_changes( mDb );

		result = sqlite3_clear_bindings( mStatement );	//JRT - 2015.11.11
		result = sqlite3_reset( mStatement );

		VoxoxSQLiteError::throwExceptionOnError( result, mDb );
	}
	else
	{
		reset();
	}

	return rowsChanged;
}

VoxoxSQLiteCursor* VoxoxSQLiteStatement::execQuery()
{
	VoxoxSQLiteCursor* result = NULL;

	checkDB();
	checkStatement();

	int nRet = sqlite3_step( mStatement );

	if (nRet == SQLITE_DONE)
	{
		// no rows
		result     =  new VoxoxSQLiteCursor( mDb, mStatement, true/*eof*/, true );
		mStatement = NULL;		//Transferred to Cursor
	}
	else if (nRet == SQLITE_ROW)
	{
		// at least 1 row
		result     = new VoxoxSQLiteCursor( mDb, mStatement, false/*eof*/, true);
		mStatement = NULL;		//Transferred to Cursor
	}
	else
	{
		reset();
	}

	return result;
}


//-----------------------------------------------------------------------------
//Bind methods
//-----------------------------------------------------------------------------

void VoxoxSQLiteStatement::bind( int paramIndex, const char* value )
{
	checkStatement();

	int result = sqlite3_bind_text( mStatement, paramIndex, value, -1, SQLITE_TRANSIENT);

	VoxoxSQLiteError::throwExceptionOnError( result, mDb, "Error binding string param" );
}


void VoxoxSQLiteStatement::bind( int paramIndex, const int value )
{
	checkStatement();

	int result = sqlite3_bind_int( mStatement, paramIndex, value );
	
	VoxoxSQLiteError::throwExceptionOnError( result, mDb, "Error binding int param" );
}

void VoxoxSQLiteStatement::bind( int paramIndex, const double value )
{
	checkStatement();
	
	int result = sqlite3_bind_double( mStatement, paramIndex, value );

	VoxoxSQLiteError::throwExceptionOnError( result, mDb, "Error binding double param" );
}

void VoxoxSQLiteStatement::bind( int paramIndex, const unsigned char* value, int len )
{
	checkStatement();

	int result = sqlite3_bind_blob( mStatement, paramIndex, (const void*)value, len, SQLITE_TRANSIENT);

	VoxoxSQLiteError::throwExceptionOnError( result, mDb, "Error binding blob param" );
}

void VoxoxSQLiteStatement::bindNull( int paramIndex )
{
	checkStatement();

	int result = sqlite3_bind_null( mStatement, paramIndex );

	VoxoxSQLiteError::throwExceptionOnError( result, mDb, "Error binding NULL param" );
}

int VoxoxSQLiteStatement::bindParameterIndex( const char* paramName )
{
	checkStatement();

	int paramIndex = sqlite3_bind_parameter_index( mStatement, paramName );

	int         nn  = sqlite3_bind_parameter_count( mStatement );
	const char* sz1 = sqlite3_bind_parameter_name ( mStatement, 1);
	const char* sz2 = sqlite3_bind_parameter_name ( mStatement, 2);

	if ( paramIndex <= 0 )
	{
		std::string msg;
		msg += "Parameter '";
		msg + paramName;
		msg + "' is not valid for this statement";
		throw VoxoxSQLiteException( VOXOX_SQLITE_ERROR, msg.c_str() );
	}

	return paramIndex;
}

void VoxoxSQLiteStatement::bind( const char* paramName, const char* value )
{
	int paramIndex = bindParameterIndex( paramName );
	bind( paramIndex, value );
}

void VoxoxSQLiteStatement::bind( const char* paramName, const int value)
{
	int paramIndex = bindParameterIndex( paramName );
	bind( paramIndex, value );
}

void VoxoxSQLiteStatement::bind( const char* paramName, const double value)
{
	int paramIndex = bindParameterIndex( paramName );
	bind( paramIndex, value);
}

void VoxoxSQLiteStatement::bind( const char* paramName, const unsigned char* value, int len)
{
	int paramIndex = bindParameterIndex( paramName );
	bind( paramIndex, value, len);
}

void VoxoxSQLiteStatement::bindNull( const char* paramName )
{
	int paramIndex = bindParameterIndex( paramName );
	bindNull( paramIndex );
}

void VoxoxSQLiteStatement::reset()
{
	if ( mStatement )
	{
		int result = sqlite3_reset( mStatement );

		VoxoxSQLiteError::throwExceptionOnError( result, mDb );
	}
}

//void VoxoxSQLiteStatement::reset()
//{
//	int	result = sqlite3_reset( mStatement );
//	VoxoxSQLiteError::throwExceptionOnError( result, mDb );
//}

void VoxoxSQLiteStatement::finalize()
{
	if ( mStatement )
	{
		int result = sqlite3_finalize( mStatement );

		mStatement = NULL;

		VoxoxSQLiteError::throwExceptionOnError( result, mDb );
	}
}

void VoxoxSQLiteStatement::close()
{
	finalize();
}

//=============================================================================


//=============================================================================
//	VoxoxSQLiteDb class
//=============================================================================

static int s_defaultBusyTimeout = 15 * 1000;

VoxoxSQLiteDb::VoxoxSQLiteDb()
{
	mDb			   = NULL;
	mIsOpen		   = false;
	mOpenFlags	   = 0;
	mBusyTimeoutMs = s_defaultBusyTimeout;
}

//VoxoxSQLiteDb::VoxoxSQLiteDb( const VoxoxSQLite& db )
//{
//}

VoxoxSQLiteDb::~VoxoxSQLiteDb()
{
	try
	{
		close();
	}
	catch (...)
	{
		throw VoxoxSQLiteException( 1, "Cannot close" );		//TODO:
	}
}

void VoxoxSQLiteDb::checkDB()
{
	VoxoxSQLiteError::checkDB( mDb );
}

//TODO: Copy operator?

void VoxoxSQLiteDb::openOrCreateReadOnly( const char* fileName )
{
	open( fileName, SQLITE_OPEN_READONLY | SQLITE_OPEN_CREATE );
}

void VoxoxSQLiteDb::openOrCreateWriteable( const char* fileName )
{
	open( fileName, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE );
}

//TODO: Should open() check for existence of db file before trying to open?  
//	If not we get an exception, when all we *may* need is a 'false' return value.
//	The exception only indicates 'unable to open database', without a reason.
void VoxoxSQLiteDb::open( const char* fileName, int flags )
{
	int result = sqlite3_open_v2( fileName, &mDb, flags, NULL );

	if ( VoxoxSQLiteError::isError( result ) )
	{
		const char* szError = sqlite3_errmsg( mDb );
		throw VoxoxSQLiteException( result, VoxoxSQLiteError::getDbErrorMsg( mDb ) );
	}

	mIsOpen    = true;
	mOpenFlags = flags;
	setBusyTimeout( mBusyTimeoutMs );
}

void VoxoxSQLiteDb::close()
{
	if ( mDb )
	{
		int result = sqlite3_close( mDb );

		VoxoxSQLiteError::throwExceptionOnError( result, mDb );

		mDb        = NULL;
		mOpenFlags = 0;
		mIsOpen    = false;
	}
}

//User version for DB upgrade
void VoxoxSQLiteDb::setUserVersion( int version )
{
	char sql[256];
	sprintf_s( sql, "PRAGMA user_version = '%d'", version );
	int result = execDml( sql );
}

int	VoxoxSQLiteDb::getUserVersion()
{
	char sql[] = "PRAGMA user_version";
	int result = simpleQueryForLong( sql, 0 );
	return result;
}

//Convenience method - We should be able to do this with DML
bool VoxoxSQLiteDb::tableExists( const char* tableName )
{
	char sql[256];
	sprintf_s( sql, "SELECT COUNT(*) FROM sqlite_master WHERE type = 'table' AND name = '%s'", tableName);
	int result = simpleQueryForLong( sql, 0 );
	return (result > 0);
}

//Statement preparation and execution
VoxoxSQLiteStatement VoxoxSQLiteDb::compileStatement( const char* sql )
{
	return VoxoxSQLiteStatement( mDb, sql, StringList() );	//Empty arg list.
}

int VoxoxSQLiteDb::execDml( const char* sql )
{
	VoxoxSQLiteStatement stmt( mDb, sql, StringList() );
	return stmt.execDml();
}

int VoxoxSQLiteDb::execDml( const char* sql, const StringList& args )
{
	VoxoxSQLiteStatement stmt( mDb, sql, args );
	return stmt.execDml();
}

VoxoxSQLiteCursor* VoxoxSQLiteDb::execQuery( const char* sql )
{
VoxoxSQLiteStatement stmt( mDb, sql, StringList() );	//Empty Arg List
	return stmt.execQuery();
}

VoxoxSQLiteCursor* VoxoxSQLiteDb::execQuery( const char* sql, const StringList& args )
{
	VoxoxSQLiteStatement stmt( mDb, sql, args );
	return stmt.execQuery();
}

VoxoxSQLiteCursor* VoxoxSQLiteDb::execStatement( sqlite3_stmt* stmt )
{
	VoxoxSQLiteCursor* cursor = NULL;
	int result = sqlite3_step( stmt );

	if ( VoxoxSQLiteError::isDone( result  ) )
	{
		// no rows
		cursor = new VoxoxSQLiteCursor( mDb, stmt, true/*eof*/);
	}
	else if ( VoxoxSQLiteError::hasRow( result  ) )
	{
		// at least 1 row
		cursor = new VoxoxSQLiteCursor( mDb, stmt, false/*eof*/);
	}
	else
	{
		result = sqlite3_finalize( stmt );
		VoxoxSQLiteError::throwExceptionOnError( result, mDb );		//TODO: Should we be forcing the 'throw' here?
	}

	return cursor;
}


long VoxoxSQLiteDb::simpleQueryForLong( const char* sql, long nullValue /*=0*/ )
{
	return simpleQueryForLong( sql, nullValue, StringList() );
}

long VoxoxSQLiteDb::simpleQueryForLong( const char* sql, long nullValue, const StringList& argList )
{
	long result = nullValue;

	VoxoxSQLiteCursor* cursor = execQuery( sql, argList );

	result = getLongFromCursor( cursor, nullValue );

	return result;
}

long VoxoxSQLiteDb::getLongFromCursor( VoxoxSQLiteCursor* cursor, long nullValue )
{
	long result = nullValue;

	if ( cursor == NULL || cursor->numFields() < 1 )
	{
		VoxoxSQLiteError::throwVoxoxSQLiteException( "Invalid scalar query" );
	}

	result = cursor->getInt( 0, nullValue );

	delete cursor;

	return result;
}


__int64 VoxoxSQLiteDb::simpleQueryForInt64( const char* sql, _int64 nullValue /*=0*/ )
{
	return simpleQueryForInt64( sql, nullValue, StringList() );
}

__int64 VoxoxSQLiteDb::simpleQueryForInt64( const char* sql, __int64 nullValue, const StringList& argList )
{
	__int64 result = nullValue;

	VoxoxSQLiteCursor* cursor = execQuery( sql, argList );

	result = getInt64FromCursor( cursor, nullValue );

	return result;
}

__int64 VoxoxSQLiteDb::getInt64FromCursor( VoxoxSQLiteCursor* cursor, __int64 nullValue )
{
	__int64 result = nullValue;

	if ( cursor == NULL || cursor->numFields() < 1 )
	{
		VoxoxSQLiteError::throwVoxoxSQLiteException( "Invalid scalar query" );
	}

	result = cursor->getInt64( 0, nullValue );

	delete cursor;

	return result;
}


std::string VoxoxSQLiteDb::simpleQueryForString( const char* sql, const char* nullValue /*=0*/ )
{
	return simpleQueryForString( sql, nullValue, StringList() );
}

std::string VoxoxSQLiteDb::simpleQueryForString( const char* sql, const char* nullValue, const StringList& argList )
{
	std::string result = nullValue;

	VoxoxSQLiteCursor* cursor = execQuery( sql, argList );

	result = getStringFromCursor( cursor, nullValue );

	return result;
}

std::string VoxoxSQLiteDb::getStringFromCursor( VoxoxSQLiteCursor* cursor, const char* nullValue )
{
	std::string result = ( nullValue == NULL ) ? "" : nullValue;

	if ( cursor == NULL || cursor->numFields() < 1 )
	{
		VoxoxSQLiteError::throwVoxoxSQLiteException( "Invalid scalar query" );
	}

	result = cursor->getString( 0, nullValue );

	delete cursor;

	return result;
}

int VoxoxSQLiteDb::deleteRecords( const std::string& table, const std::string& whereClause, const StringList& whereArgs)
{
	std::string sql = "DELETE FROM " + table;

	if ( !whereClause.empty() )
	{
		sql += " WHERE " + whereClause;
	}

	VoxoxSQLiteStatement stmt( mDb, sql.c_str(), whereArgs );
	return stmt.execDml();
}

//Configuration/Settings methods
sqlite_int64 VoxoxSQLiteDb::getLastRowId()
{
	return sqlite3_last_insert_rowid( mDb );
}

void VoxoxSQLiteDb::interrupt()	
{ 
	sqlite3_interrupt( mDb );		
}

void VoxoxSQLiteDb::setBusyTimeout( int value )
{
	mBusyTimeoutMs = value;
	sqlite3_busy_timeout( mDb, mBusyTimeoutMs );
}

bool VoxoxSQLiteDb::isAutoCommitOn()
{
	checkDB();
	return sqlite3_get_autocommit( mDb ) ? true : false;
}


//Convenience methods
//static 
bool VoxoxSQLiteDb::doesDbExist( const char* dbName )
{
	VoxoxSQLiteDb checkDB;
	bool result = false;

	try
	{
		checkDB.open( dbName, SQLITE_OPEN_READONLY );
		checkDB.close();
		result = true;
	}
	catch ( VoxoxSQLiteException e )
	{
		result = false;
		//database does not exist yet.
	}

	return result;
}

bool VoxoxSQLiteDb::isOpen()
{
	return mIsOpen;
}

bool VoxoxSQLiteDb::isReadOnly()
{
	return isOpen() && ((mOpenFlags & SQLITE_OPEN_READONLY) == SQLITE_OPEN_READONLY);
}

bool VoxoxSQLiteDb::isWriteable()
{
	return isOpen() && ((mOpenFlags & SQLITE_OPEN_READWRITE) == SQLITE_OPEN_READWRITE);
}


int VoxoxSQLiteDb::beginTransaction()
{
	return execDml( mBeginTransactionSql );
}

int VoxoxSQLiteDb::commitTransaction()
{
	return execDml( mCommitTransactionSql );
}

int VoxoxSQLiteDb::rollbackTransaction()
{
	return execDml( mRollbackTransactionSql );
}

int VoxoxSQLiteDb::setSynchronous( bool value )
{
	const char* sql = (value ? mSynchronousOn : mSynchronousOff);
	return execDml( sql );
}


//Helper/Info methods
//static 
const char* VoxoxSQLiteDb::SQLiteVersion()
{ 
	return SQLITE_VERSION;				
}

//static 
const char* VoxoxSQLiteDb::SQLiteHeaderVersion()
{ 
	return SQLITE_VERSION;
}

//static 
const char* VoxoxSQLiteDb::SQLiteLibraryVersion()
{ 
	return sqlite3_libversion();			
}

//static 
int	VoxoxSQLiteDb::SQLiteLibraryVersionNumber()
{ 
	return sqlite3_libversion_number();	
}


};	//namespace VoxoxSQLite