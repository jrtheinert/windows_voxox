/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

#include "MessageTranslationPropertyCursor.h"

MessageTranslationPropertyCursor::MessageTranslationPropertyCursor( VoxoxSQLiteCursor* cursor ) : WrappedCursor( cursor )
{
	initIndexes();
}

MessageTranslationPropertyCursor::~MessageTranslationPropertyCursor()
{
}

void MessageTranslationPropertyCursor::initIndexes()
{
	mRecordIdIndex		  = fieldIndex( DatabaseTables::Translation::ID.c_str()					);
	mEnabledIndex		  = fieldIndex( DatabaseTables::Translation::ENABLED.c_str()			);
	mContactIdIndex		  = fieldIndex( DatabaseTables::Translation::CONTACT_ID.c_str()			);
	mUserLanguageIndex	  = fieldIndex( DatabaseTables::Translation::USER_LANGUAGE.c_str()		);
	mContactLanguageIndex = fieldIndex( DatabaseTables::Translation::CONTACT_LANGUAGE.c_str()	);
	mPhoneNumberIndex	  = fieldIndex( DatabaseTables::Translation::PHONE_NUMBER.c_str()		);
	mJidIndex			  = fieldIndex( DatabaseTables::Translation::JID.c_str()				);
	mDirIndex			  = fieldIndex( DatabaseTables::Translation::TRANSLATE_MESSAGE_DIRECTION.c_str()	);
}

MessageTranslationPropertiesList MessageTranslationPropertyCursor::getData()
{
	mData.clear();

	//We always start at first row, if there is any data.
	while ( !eof() )
	{
		MessageTranslationProperties data;

		Message::Direction dir = (Message::Direction)getDirection();

		data.setRecordId	   ( getRecordId()		  );
		data.setDirection	   ( dir				  );
		data.setEnabled		   ( isEnabled()		  );
		data.setContactId	   ( getContactId()		  );
		data.setUserLanguage   ( getUserLanguage()	  );
		data.setContactLanguage( getContactLanguage() );
		data.setPhoneNumber    ( getPhoneNumber()	  );
		data.setJid			   ( getJid()			  );

		mData.push_back( data );

		nextRow();
	}

	return mData;
}
