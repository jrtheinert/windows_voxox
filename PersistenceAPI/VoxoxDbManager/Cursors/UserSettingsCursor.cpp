/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

#include "UserSettingsCursor.h"

UserSettingsCursor::UserSettingsCursor( VoxoxSQLiteCursor* cursor ) : WrappedCursor( cursor )
{
	initIndexes();
}

UserSettingsCursor::~UserSettingsCursor()
{
}

void UserSettingsCursor::initIndexes()
{
	mUserIdIndex					= fieldIndex( DatabaseTables::Settings::USER_ID.c_str()							);

	mAudioInputDeviceIndex			= fieldIndex( DatabaseTables::Settings::AUDIO_INPUT_DEVICE.c_str()				);
	mAudioOutputDeviceIndex			= fieldIndex( DatabaseTables::Settings::AUDIO_OUTPUT_DEVICE.c_str()				);
	mAudioInputVolumeIndex			= fieldIndex( DatabaseTables::Settings::AUDIO_INPUT_VOLUME.c_str()				);
	mAudioOutputVolumeIndex			= fieldIndex( DatabaseTables::Settings::AUDIO_OUTPUT_VOLUME.c_str()				);

	mShowFaxInMsgThreadIndex		= fieldIndex( DatabaseTables::Settings::SHOW_FAX_IN_MSG_THREAD.c_str()			);
	mShowVmInMsgThreadIndex			= fieldIndex( DatabaseTables::Settings::SHOW_VM_IN_MSG_THREAD.c_str()			);
	mShowRcInMsgThreadIndex			= fieldIndex( DatabaseTables::Settings::SHOW_RC_IN_MSG_THREAD.c_str()			);

	mEnableSoundIncomingMsgIndex	= fieldIndex( DatabaseTables::Settings::ENABLE_SOUND_INCOMING_MSG.c_str()		);
	mEnableSoundIncomingCallIndex	= fieldIndex( DatabaseTables::Settings::ENABLE_SOUND_INCOMING_CALL.c_str()		);
//	mEnableSoundOutgoingMsgIndex	= fieldIndex( DatabaseTables::Settings::ENABLE_SOUND_OUTGOING_MSG.c_str()		);

	mMyChatBubbleColorIndex			= fieldIndex( DatabaseTables::Settings::MY_CHAT_BUBBLE_COLOR.c_str()			);
	mConversantChatBubbleColorIndex	= fieldIndex( DatabaseTables::Settings::CONVERSANT_CHAT_BUBBLE_COLOR.c_str()	);

	mCountryCodeIndex				= fieldIndex( DatabaseTables::Settings::COUNTRY_CODE.c_str()					);
	mCountryAbbrevIndex				= fieldIndex( DatabaseTables::Settings::COUNTRY_ABBREV.c_str()					);

//	mOutgoingCallSoundFileIndex		= fieldIndex( DatabaseTables::Settings::OUTGOING_CALL_SOUND_FILE.c_str()		);
	mIncomingCallSoundFileIndex		= fieldIndex( DatabaseTables::Settings::INCOMING_CALL_SOUND_FILE.c_str()		);
	mCallClosedSoundFileIndex		= fieldIndex( DatabaseTables::Settings::CALL_CLOSED_SOUND_FILE.c_str()			);

	mWindowLeftIndex				= fieldIndex( DatabaseTables::Settings::WINDOW_LEFT.c_str()						);
	mWindowTopIndex					= fieldIndex( DatabaseTables::Settings::WINDOW_TOP.c_str()						);
	mWindowHeightIndex				= fieldIndex( DatabaseTables::Settings::WINDOW_HEIGHT.c_str()					);
	mWindowWidthIndex				= fieldIndex( DatabaseTables::Settings::WINDOW_WIDTH.c_str()					);
	mWindowStateIndex				= fieldIndex( DatabaseTables::Settings::WINDOW_STATE.c_str()					);

	mLastMsgTimestampIndex			= fieldIndex( DatabaseTables::Settings::LAST_MSG_TIMESTAMP.c_str()				);
	mLastCallTimestampIndex			= fieldIndex( DatabaseTables::Settings::LAST_CALL_TIMESTAMP.c_str()				);
	mLastContactSourceKeyIndex		= fieldIndex( DatabaseTables::Settings::LAST_CONTACT_SOURCE_KEY.c_str()			);

	mSipUuidIndex					= fieldIndex( DatabaseTables::Settings::SIP_UUID.c_str()						);
}

UserSettings UserSettingsCursor::getData()
{
	//TODO: We need/expect a single row here.  Check for that and log warning if we have more.

	//We always start at first row, if there is any data.

	mData.setUserId					  ( getUserId()						);

	mData.setAudioInputDevice		  ( getAudioInputDevice()			);
	mData.setAudioOutputDevice		  ( getAudioOutputDevice()			);
	mData.setAudioInputVolume		  ( getAudioInputVolume()			);
	mData.setAudioOutputVolume		  ( getAudioOutputVolume()			);

	mData.setShowFaxInMsgThread		  ( getShowFaxInMsgThread()			);
	mData.setShowVmInMsgThread		  ( getShowVmInMsgThread()			);
	mData.setShowRcInMsgThread		  ( getShowRcInMsgThread()			);

	mData.setEnableSoundIncomingMsg	  ( getEnableSoundIncomingMsg()		);
	mData.setEnableSoundIncomingCall  ( getEnableSoundIncomingCall()	);
//	mData.setEnableSoundOutgoingMsg	  ( getEnableSoundOutgoingMsg()		);

	mData.setMyChatBubbleColor		  ( getMyChatBubbleColor()			);
	mData.setConversantChatBubbleColor( getConversnatChatBubbleColor()	);

	mData.setCountryCode			  ( getCountryCode()				);
	mData.setCountryAbbrev			  ( getCountryAbbrev()				);

//	mData.setOutgoingCallSoundFile	  ( getOutgoingCallSoundFile()		);
	mData.setIncomingCallSoundFile	  ( getIncomingCallSoundFile()		);
	mData.setCallClosedSoundFile	  ( getCallClosedSoundFile()		);

	mData.setWindowLeft				  ( getWindowLeft()					);
	mData.setWindowTop				  ( getWindowTop()					);
	mData.setWindowHeight			  ( getWindowHeight()				);
	mData.setWindowWidth			  ( getWindowWidth()				);

	mData.setLastMsgTimestamp		  ( getLastMsgTimestamp()			);
	mData.setLastCallTimestamp		  ( getLastCallTimestamp()			);
	mData.setLastContactSourceKey	  ( getLastContactSourceKey()		);

	mData.setSipUuid				  ( getSipUuid()					);

	return mData;
}
