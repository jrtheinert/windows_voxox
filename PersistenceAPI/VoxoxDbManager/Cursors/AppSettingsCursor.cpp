/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

#include "AppSettingsCursor.h"

AppSettingsCursor::AppSettingsCursor( VoxoxSQLiteCursor* cursor ) : WrappedCursor( cursor )
{
	initIndexes();
}

AppSettingsCursor::~AppSettingsCursor()
{
}

void AppSettingsCursor::initIndexes()
{
	mRecordIdIndex					= fieldIndex( DatabaseTables::AppSettings::ID.c_str()						);

	mAutoLoginIndex					= fieldIndex( DatabaseTables::AppSettings::AUTO_LOGIN.c_str()				);
	mRememberPasswordIndex			= fieldIndex( DatabaseTables::AppSettings::REMEMBER_PASSWORD.c_str()		);
	mLastLoginUsernameIndex			= fieldIndex( DatabaseTables::AppSettings::LAST_LOGIN_USERNAME.c_str()		);
	mLastLoginPasswordIndex			= fieldIndex( DatabaseTables::AppSettings::LAST_LOGIN_PASSWORD.c_str()		);
	mLastLoginCountryCodeIndex		= fieldIndex( DatabaseTables::AppSettings::LAST_LOGIN_COUNTRY_CODE.c_str()	);
	mLastLoginPhoneNumberIndex		= fieldIndex( DatabaseTables::AppSettings::LAST_LOGIN_PHONE_NUMBER.c_str()	);

	//Debug items
	mDebugMenuIndex					= fieldIndex( DatabaseTables::AppSettings::DEBUG_MENU.c_str()				);
	mIgnoreLoggersIndex				= fieldIndex( DatabaseTables::AppSettings::IGNORE_LOGGERS.c_str()			);
	mMaxSipLoggingIndex				= fieldIndex( DatabaseTables::AppSettings::MAX_SIP_LOGGING.c_str()			);
	mGetInitOptionsHandlingIndex	= fieldIndex( DatabaseTables::AppSettings::GET_INIT_OPTIONS_HANDLING.c_str());
}

AppSettings AppSettingsCursor::getData()
{
	//TODO: We need/expect a single row here.  Check for that and log warning if we have more.

	//We always start at first row, if there is any data.

	mData.setRecordId			 ( getRecordId()				);

	mData.setAutoLogin			 ( getAutoLogin()				);
	mData.setRememberPassword	 ( getRememberPassword()		);
	mData.setLastLoginUsername	 ( getLastLoginUsername()		);
	mData.setLastLoginPassword	 ( getLastLoginPassword()		);
	mData.setLastLoginCountryCode( getLastLoginCountryCode()	);
	mData.setLastLoginPhoneNumber( getLastLoginPhoneNumber()	);

	mData.setDebugMenu			   ( getDebugMenu()				);
	mData.setIgnoreLoggers		   ( getIgnoreLoggers()			);
	mData.setMaxSipLogging		   ( getMaxSipLogging()			);
	mData.setGetInitOptionsHandling( getGetInitOptionsHandling());

	return mData;
}
