/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

/**
 *	This is a GET-ONLY class.
 */

#pragma once

#include "WrappedCursor.h"

using namespace VoxoxSQLite;

class GroupMessageGroupCursor :	public WrappedCursor
{
public:
	GroupMessageGroupCursor( VoxoxSQLiteCursor* cursor );
	~GroupMessageGroupCursor();

	GroupMessageGroupList	getData();

private:
	int getRecordId()					{ return getIntEx( mRecordIdIndex,				0 );	}
	int getStatus()						{ return getIntEx( mStatusIndex,				0 );	}
	int getInviteType()					{ return getIntEx( mInviteTypeIndex,			0 );	}
	int getMemberCount()				{ return getIntEx( mMemberCountIndex,			0 );	}
	int getAdminUserId()				{ return getIntEx( mAdminUserIdIndex,			0 );	}

	bool getAnnounceMembers()			{ return getAnnounceMembersRaw() > 0;	}
	
	std::string getGroupId()			{ return getStringEx( mGroupIdIndex,			"" );	}
	std::string getName()				{ return getStringEx( mNameIndex,				"" );	}
	std::string getConfirmationCode()	{ return getStringEx( mConfirmationCodeIndex,	"" );	}
	std::string getAvatar()				{ return getStringEx( mAvatarIndex,				"" );	}


private:
	//Get field indexes from field names, for greater efficiency
	void initIndexes();
	int getAnnounceMembersRaw()			{ return getIntEx( mAnnounceMembersIndex,		0 );	}

private:
	GroupMessageGroupList mData;

	int mRecordIdIndex;
	int mGroupIdIndex;
	int mNameIndex;
	int mStatusIndex;
	int mInviteTypeIndex;
	int mAnnounceMembersIndex;
	int mConfirmationCodeIndex;
	int mAvatarIndex;

	int mMemberCountIndex;
	int mAdminUserIdIndex;
};

