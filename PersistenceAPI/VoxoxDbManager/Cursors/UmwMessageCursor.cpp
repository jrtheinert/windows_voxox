/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

#include "UmwMessageCursor.h"

UmwMessageCursor::UmwMessageCursor( VoxoxSQLiteCursor* cursor ) : WrappedCursor( cursor )
{
	initIndexes();
}

UmwMessageCursor::~UmwMessageCursor()
{
}

void UmwMessageCursor::initIndexes()
{
	mContactKeyIndex		 = fieldIndex( DatabaseTables::PhoneNumber::CONTACT_KEY.c_str()		);
	mPhoneNumberIndex		 = fieldIndex( DatabaseTables::PhoneNumber::NUMBER.c_str()			);	
	mCmGroupIndex			 = fieldIndex( DatabaseTables::PhoneNumber::CM_GROUP.c_str()		);
	mIsVoxoxIndex			 = fieldIndex( DatabaseTables::PhoneNumber::IS_VOXOX.c_str()		);
	mNameIndex				 = fieldIndex( DatabaseTables::PhoneNumber::NAME.c_str()			);
	mCmGroupNameIndex		 = fieldIndex( DatabaseTables::PhoneNumber::CM_GROUP_NAME.c_str()	);

	mGmGroupNameIndex		 = fieldIndex( DatabaseTables::GroupMessageGroup::GROUP_NAME.c_str());
	mGmNickNameIndex		 = fieldIndex( DatabaseTables::GroupMessageMember::NICKNAME.c_str()	);
	mGmColorIndex			 = fieldIndex( DatabaseTables::GroupMessageMember::COLOR.c_str()	);

	//Main message info
	mRecordIdIndex			 = fieldIndex( DatabaseTables::Message::ID.c_str()					);
	mMsgIdIndex				 = fieldIndex( DatabaseTables::Message::MESSAGE_ID.c_str()			);
	mTypeIndex				 = fieldIndex( DatabaseTables::Message::TYPE.c_str()				);
	mDirIndex				 = fieldIndex( DatabaseTables::Message::DIRECTION.c_str()			);
	mStatusIndex			 = fieldIndex( DatabaseTables::Message::STATUS.c_str()				);
	mBodyIndex				 = fieldIndex( DatabaseTables::Message::BODY.c_str()				);
	mTranslatedIndex		 = fieldIndex( DatabaseTables::Message::TRANSLATED.c_str()			);

	//Time stamps
	mTimestampIndex			 = fieldIndex( DatabaseTables::Message::LOCAL_TIMESTAMP.c_str()		);
	mServerTimestampIndex	 = fieldIndex( DatabaseTables::Message::SERVER_TIMESTAMP.c_str()	);
	mReadTimestampIndex		 = fieldIndex( DatabaseTables::Message::READ_TIMESTAMP.c_str()		);
	mDeliveredTimestampIndex = fieldIndex( DatabaseTables::Message::DELIVERED_TIMESTAMP.c_str()	);

	//Rich data
	mFileLocalIndex			 = fieldIndex( DatabaseTables::Message::FILE_LOCAL.c_str()			);
	mThumbnailLocalIndex	 = fieldIndex( DatabaseTables::Message::THUMBNAIL_LOCAL.c_str()		);
	mDurationIndex			 = fieldIndex( DatabaseTables::Message::DURATION.c_str()			);

	//SMS
	mFromDidIndex			 = fieldIndex( DatabaseTables::Message::FROM_DID.c_str()			);
	mToDidIndex				 = fieldIndex( DatabaseTables::Message::TO_DID.c_str()				);

	//Chat
	mFromJidIndex			 = fieldIndex( DatabaseTables::Message::FROM_JID.c_str()			);
	mToJidIndex				 = fieldIndex( DatabaseTables::Message::TO_JID.c_str()				);

	//Group Messaging
	mToGroupIdIndex			 = fieldIndex( DatabaseTables::Message::TO_GROUP_ID.c_str()			);
	mFromUserIdIndex		 = fieldIndex( DatabaseTables::Message::FROM_USER_ID.c_str()		);
}

UmwMessageList UmwMessageCursor::getData()
{
	mData.clear();

	//We always start at first row, if there is any data.
	while ( !eof() )
	{
		UmwMessage data;
	
		Message::Type      type   = (Message::Type)      getType();
		Message::Direction dir    = (Message::Direction) getDir();
		Message::Status    status = (Message::Status)    getStatus();

		data.setRecordId	( getRecordId()		);
		data.setContactKey	( getContactKey()	);
		data.setCmGroup		( getCmGroup()		);
		data.setPhoneNumber	( getPhoneNumber()	);
		data.setName		( getName()			);
		data.setCmGroupName	( getCmGroupName()	);
		data.setIsVoxox		( getIsVoxox()		);
		data.setGmNickName	( getGmNickname()	);
		data.setGmGroupName ( getGmGroupName()	);
		data.setGmColor		( getGmColor()		);

		//Main message info
		data.setType	  ( type			);
		data.setDirection ( dir				);
		data.setStatus	  ( status			);
		data.setMsgId	  ( getMsgId()		);
		data.setBody	  ( getBody()		);
		data.setTranslated( getTranslated() );

		//Time stamps
		data.setLocalTimestamp	  ( getLocalTimestamp()		);
		data.setServerTimestamp	  ( getServerTimestamp()	);
		data.setReadTimestamp	  ( getReadTimestamp()		);
		data.setDeliveredTimestamp( getDeliveredTimestamp()	);

		//Rich data
		data.setFileLocal	  (	getFileLocal()		);
		data.setThumbnailLocal( getThumbnailLocal()	);
		data.setDuration	  ( getDuration()		);

		//SMS
		data.setFromDid( getFromDid()	);
		data.setToDid  ( getToDid()		);

		//Chat
		data.setFromJid( getFromJid()	);
		data.setToJid  ( getToJid()		);

		//Group Messaging
		data.setToGroupId ( getToGroupId()	);
		data.setFromUserId( getFromUserId()	);

		mData.push_back( data );

		nextRow();
	}

	return mData;
}
