/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

/**
 *	This is a GET-ONLY class.
 */

#pragma once

#include "WrappedCursor.h"

using namespace VoxoxSQLite;

class UserSettingsCursor : public WrappedCursor
{
public:
	UserSettingsCursor( VoxoxSQLiteCursor* cursor );
	~UserSettingsCursor();

	UserSettings	getData();

private:
	std::string	getUserId()						{ return getStringEx( mUserIdIndex,				"" );	}

	std::string getAudioInputDevice()			{ return getStringEx( mAudioInputDeviceIndex,	"" );	}
	std::string getAudioOutputDevice()			{ return getStringEx( mAudioOutputDeviceIndex,	"" );	}
	int			getAudioInputVolume()			{ return getIntEx   ( mAudioInputVolumeIndex,	 0 );	}
	int			getAudioOutputVolume()			{ return getIntEx   ( mAudioOutputVolumeIndex,	 0 );	}

	bool		getShowFaxInMsgThread()			{ return getShowFaxInMsgThreadRaw()			!= 0;		}
	bool		getShowVmInMsgThread()			{ return getShowVmInMsgThreadRaw()			!= 0;		}
	bool		getShowRcInMsgThread()			{ return getShowRcInMsgThreadRaw()			!= 0;		}

	bool		getEnableSoundIncomingMsg()		{ return getEnableSoundIncomingMsgRaw()		!= 0;		}
	bool		getEnableSoundIncomingCall()	{ return getEnableSoundIncomingCallRaw()	!= 0;		}
//	bool		getEnableSoundOutgoingMsg()		{ return getEnableSoundOutgoingMsgRaw()		!= 0;		}

	std::string	getMyChatBubbleColor()			{ return getStringEx( mMyChatBubbleColorIndex,			"" );	}
	std::string	getConversnatChatBubbleColor()	{ return getStringEx( mConversantChatBubbleColorIndex,	"" );	}

	std::string	getCountryCode()				{ return getStringEx( mCountryCodeIndex,				"" );	}
	std::string getCountryAbbrev()				{ return getStringEx( mCountryAbbrevIndex,				"" );	}

//	std::string getOutgoingCallSoundFile()		{ return getStringEx( mOutgoingCallSoundFileIndex,		"" );	}
	std::string getIncomingCallSoundFile()		{ return getStringEx( mIncomingCallSoundFileIndex,		"" );	}
	std::string getCallClosedSoundFile()		{ return getStringEx( mCallClosedSoundFileIndex,		"" );	}

	double		getWindowLeft()					{ return getDoubleEx( mWindowLeftIndex,				0  );	}
	double		getWindowTop()					{ return getDoubleEx( mWindowTopIndex,				0  );	}
	double		getWindowHeight()				{ return getDoubleEx( mWindowHeightIndex,			0  );	}
	double		getWindowWidth()				{ return getDoubleEx( mWindowWidthIndex,			0  );	}

	__int64		getLastMsgTimestamp()			{ return getInt64Ex( mLastMsgTimestampIndex,		0  );	}
	__int64		getLastCallTimestamp()			{ return getInt64Ex( mLastCallTimestampIndex,		0  );	}
	std::string	getLastContactSourceKey()		{ return getStringEx( mLastContactSourceKeyIndex,	"" );	}

	std::string	getSipUuid()					{ return getStringEx( mSipUuidIndex,				"" );	}
				
private:
	//Get field indexes from field names, for greater efficiency
	void initIndexes();

	int  getShowFaxInMsgThreadRaw()			{ return getIntEx( mShowFaxInMsgThreadIndex,		0 );	}
	int  getShowVmInMsgThreadRaw()			{ return getIntEx( mShowVmInMsgThreadIndex,			0 );	}
	int  getShowRcInMsgThreadRaw()			{ return getIntEx( mShowRcInMsgThreadIndex,			0 );	}

	int  getEnableSoundIncomingMsgRaw()		{ return getIntEx( mEnableSoundIncomingMsgIndex,	0 );	}
	int  getEnableSoundIncomingCallRaw()	{ return getIntEx( mEnableSoundIncomingCallIndex,	0 );	}
//	int  getEnableSoundOutgoingMsgRaw()		{ return getIntEx( mEnableSoundOutgoingMsgIndex,	0 );	}

private:
	UserSettings mData;

	int mUserIdIndex;

	int mAudioInputDeviceIndex;
	int mAudioOutputDeviceIndex;
	int mAudioInputVolumeIndex;
	int mAudioOutputVolumeIndex;

	int mShowFaxInMsgThreadIndex;
	int mShowVmInMsgThreadIndex;
	int mShowRcInMsgThreadIndex;

	int mEnableSoundIncomingMsgIndex;
	int mEnableSoundIncomingCallIndex;
//	int mEnableSoundOutgoingMsgIndex;

	int mMyChatBubbleColorIndex;
	int mConversantChatBubbleColorIndex;

	int mCountryCodeIndex;
	int mCountryAbbrevIndex;

//	int mOutgoingCallSoundFileIndex;
	int mIncomingCallSoundFileIndex;
	int mCallClosedSoundFileIndex;

	int mWindowLeftIndex;
	int mWindowTopIndex;
	int mWindowHeightIndex;
	int mWindowWidthIndex;
	int mWindowStateIndex;

	int mLastMsgTimestampIndex;
	int mLastCallTimestampIndex;
	int mLastContactSourceKeyIndex;

	int mSipUuidIndex;
};
