/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

#include "PhoneNumberCursor.h"

PhoneNumberCursor::PhoneNumberCursor( VoxoxSQLiteCursor* cursor ) : WrappedCursor( cursor )
{
	initIndexes();
}

PhoneNumberCursor::~PhoneNumberCursor()
{
}

void PhoneNumberCursor::initIndexes()
{
	mRecordIdIndex		= fieldIndex( DatabaseTables::PhoneNumber::ID.c_str()				);
	mSourceIndex		= fieldIndex( DatabaseTables::PhoneNumber::SOURCE.c_str()			);
	mCmGroupIndex		= fieldIndex( DatabaseTables::PhoneNumber::CM_GROUP.c_str()			);
	mIsVoxoxIndex		= fieldIndex( DatabaseTables::PhoneNumber::IS_VOXOX.c_str()			);
	mIsFavoriteIndex	= fieldIndex( DatabaseTables::PhoneNumber::IS_FAVORITE.c_str()		);
	
	mNameIndex			= fieldIndex( DatabaseTables::PhoneNumber::NAME.c_str()				);
	mNumberIndex		= fieldIndex( DatabaseTables::PhoneNumber::NUMBER.c_str()			);
	mDisplayNumberIndex = fieldIndex( DatabaseTables::PhoneNumber::DISPLAY_NUMBER.c_str()	);
	mLabelIndex			= fieldIndex( DatabaseTables::PhoneNumber::LABEL.c_str()			);
	mCmGroupNameIndex	= fieldIndex( DatabaseTables::PhoneNumber::CM_GROUP_NAME.c_str()	);
	
	mXmppJidIndex		= fieldIndex( DatabaseTables::PhoneNumber::XMPP_JID.c_str()			);
	mXmppPresenceIndex	= fieldIndex( DatabaseTables::PhoneNumber::XMPP_PRESENCE.c_str()	);
	mXmppStatusIndex	= fieldIndex( DatabaseTables::PhoneNumber::XMPP_STATUS.c_str()		);
	mXmppGroupIndex		= fieldIndex( DatabaseTables::PhoneNumber::XMPP_GROUP.c_str()		);

	//New Contact Syncing
	mContactKeyIndex	= fieldIndex( DatabaseTables::PhoneNumber::CONTACT_KEY.c_str()		  );
	mCompanyIndex		= fieldIndex( DatabaseTables::PhoneNumber::COMPANY.c_str()			  );
	mFirstNameIndex		= fieldIndex( DatabaseTables::PhoneNumber::FIRST_NAME.c_str()		  );
	mLastNameIndex		= fieldIndex( DatabaseTables::PhoneNumber::LAST_NAME.c_str()		  );
	mUserIdIndex		= fieldIndex( DatabaseTables::PhoneNumber::USER_ID.c_str()			  );
	mUserNameIndex		= fieldIndex( DatabaseTables::PhoneNumber::USER_NAME.c_str()		  );
	mIsBlockedIndex		= fieldIndex( DatabaseTables::PhoneNumber::IS_BLOCKED.c_str()		  );
	mIsExtensionIndex	= fieldIndex( DatabaseTables::PhoneNumber::IS_EXT.c_str()			  );
}

PhoneNumberList PhoneNumberCursor::getData()
{
	mData.clear();

	//We always start at first row, if there is any data.
	while ( !eof() )
	{
		PhoneNumber data;

		data.setRecordId	 ( getRecordId()		);
		data.setSource		 ( getSource()			);
		data.setCmGroup		 ( getCmGroup()			);
		data.setNumberType	 ( getIsVoxox()			);
		data.setIsFavorite	 ( getIsFavorite()		);

		data.setName		 ( getName()			);
		data.setNumber		 ( getPhoneNumber()		);
		data.setDisplayNumber( getDisplayNumber()	);
		data.setLabel		 ( getLabel()			);
		data.setCmGroupName	 ( getCmGroupName()		);

		data.setXmppJid		 ( getXmppJid()			);
		data.setXmppPresence ( getXmppPresence()	);
		data.setXmppStatus	 ( getXmppStatus()		);
		data.setXmppGroup	 ( getXmppGroup()		);

		//New contact syncing
		data.setContactKey ( getContactKey()  );
		data.setCompany    ( getCompany()	  );
		data.setFirstName  ( getFirstName()	  );
		data.setLastName   ( getLastName()	  );
		data.setUserId	   ( getUserId()	  );
		data.setUserName   ( getUserName()	  );
		data.setIsBlocked  ( getIsBlocked()	  );
		data.setIsExtension( getIsExtension() );

		mData.push_back( data );

		nextRow();
	}

	return mData;
}
