/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

#include "WrappedCursor.h"

WrappedCursor::WrappedCursor() : mCursor( NULL )
{
	initIndexes();
}

WrappedCursor::WrappedCursor( VoxoxSQLiteCursor* cursor ) : mCursor( cursor )
{
	initIndexes();
}

WrappedCursor::~WrappedCursor()
{
	delete mCursor;
	mCursor = NULL;
}


//Wrapped methods

int WrappedCursor::fieldIndex( const std::string& fieldName )
{
	return ( mCursor == NULL ? -1 : mCursor->fieldIndex( fieldName.c_str() ) );
}

bool WrappedCursor::eof()
{
	return ( mCursor == NULL ? true : mCursor->eof() );
}

void WrappedCursor::nextRow()
{
	if ( mCursor )
		 mCursor->nextRow();
}


int WrappedCursor::getIntEx( int index, int defaultValue )
{
	return (mCursor == NULL ? defaultValue : mCursor->getIntEx( index, defaultValue ) );
}

__int64 WrappedCursor::getInt64Ex( int index, __int64 defaultValue )
{
	return (mCursor == NULL ? defaultValue : mCursor->getInt64Ex( index, defaultValue ) );
}

double WrappedCursor::getDoubleEx( int index, double defaultValue )
{
	return (mCursor == NULL ? defaultValue : mCursor->getFloatEx( index, defaultValue ) );
}

std::string WrappedCursor::getStringEx( int index, const std::string& defaultValue )
{
	return (mCursor == NULL ? defaultValue : mCursor->getStringEx( index, defaultValue.c_str() ) );
}

//Helpers
//static
bool WrappedCursor::toBool( int value )
{
	return value != 0;
}
