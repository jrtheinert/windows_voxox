/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

/**
 *	This is a GET-ONLY class.
 */

#pragma once

#include "WrappedCursor.h"

using namespace VoxoxSQLite;

class GroupMessageMemberOfCursor : public WrappedCursor
{
public:
	GroupMessageMemberOfCursor( VoxoxSQLiteCursor* cursor );
	~GroupMessageMemberOfCursor();

	GroupMessageMemberOfList	getData();

private:
	int getRecordId()					{ return getIntEx( mRecordIdIndex,			0 );	}
	int getUserId()						{ return getIntEx( mUserIdIndex,			0 );	}
	
	std::string getGroupId()			{ return getStringEx( mGroupIdIndex,		"" );	}
	std::string getGroupName()			{ return getStringEx( mGroupNameIndex,		"" );	}
	std::string getNickname()			{ return getStringEx( mNicknameIndex,		"" );	}
	std::string getDisplayName()		{ return getStringEx( mDisplayNameIndex,	"" );	}

	bool isAdmin()						{ return ( getIsAdminRaw() > 0 );	}

private:
	//Get field indexes from field names, for greater efficiency
	void initIndexes();
	int getIsAdminRaw()					{ return getIntEx( mIsAdminIndex,			0 );	}

private:
	GroupMessageMemberOfList mData;

	int mRecordIdIndex;
	int mGroupIdIndex;
	int mGroupNameIndex;

	int mUserIdIndex;
	int mIsAdminIndex;
	int mNicknameIndex;

	int mDisplayNameIndex;
};

