/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

/**
 *	This is a GET-ONLY class.
 */

#pragma once

#include "WrappedCursor.h"

using namespace VoxoxSQLite;

class PhoneNumberCursor : public WrappedCursor
{
public:
	PhoneNumberCursor( VoxoxSQLiteCursor* cursor );
	~PhoneNumberCursor();

	PhoneNumberList	getData();

private:
	int getRecordId()					{ return getIntEx( mRecordIdIndex,			 0 );	}
	int getCmGroup()					{ return getIntEx( mCmGroupIndex,			 Contact::CmGroup::INVALID_CMGROUP );	}
	int getIsVoxox()					{ return getIntEx( mIsVoxoxIndex,			 0 );	}
	bool getIsFavorite()				{ return getIsFavoriteRaw()  != 0;					}
	bool getIsBlocked()					{ return getIsBlockedRaw()   != 0;					}	//Contact-Sync
	bool getIsExtension()				{ return getIsExtensionRaw() != 0;					}	//Contact-Sync
	
	std::string getContactKey()			{ return getStringEx( mContactKeyIndex,		"" );	}	//Contact-Sync
	std::string getSource()				{ return getStringEx( mSourceIndex,			"" );	}
	std::string getName()				{ return getStringEx( mNameIndex,			"" );	}
	std::string getFirstName()			{ return getStringEx( mFirstNameIndex,		"" );	}	//Contact-Sync
	std::string getLastName()			{ return getStringEx( mLastNameIndex,		"" );	}	//Contact-Sync
	std::string getCompany()			{ return getStringEx( mCompanyIndex,		"" );	}	//Contact-Sync
	std::string getPhoneNumber()		{ return getStringEx( mNumberIndex,			"" );	}
	std::string getDisplayNumber()		{ return getStringEx( mDisplayNumberIndex,	"" );	}
	std::string getLabel()				{ return getStringEx( mLabelIndex,			"" );	}
	std::string getCmGroupName()		{ return getStringEx( mCmGroupNameIndex,	"" );	}

	int			getUserId()				{ return getIntEx	( mUserIdIndex,			-1 );	}	//Contact-Sync
	std::string getUserName()			{ return getStringEx( mUserNameIndex,		"" );	}	//Contact-Sync
	std::string getXmppJid()			{ return getStringEx( mXmppJidIndex,		"" );	}
	std::string getXmppPresence()		{ return getStringEx( mXmppPresenceIndex,	"" );	}
	std::string getXmppGroup()			{ return getStringEx( mXmppGroupIndex,		"" );	}
//	std::string getXmppStatus()			{ return getStringEx( mXmppStatusIndex,		"" );	}
	int getXmppStatus()					{ return getIntEx( mXmppStatusIndex,		 0 );	}
				
private:
	//Get field indexes from field names, for greater efficiency
	void initIndexes();
	int  getIsFavoriteRaw()				{ return getIntEx( mIsFavoriteIndex,		 0 );	}
	int  getIsBlockedRaw()				{ return getIntEx( mIsBlockedIndex,			 0 );	}
	int  getIsExtensionRaw()			{ return getIntEx( mIsExtensionIndex,		 0 );	}

private:
	PhoneNumberList mData;

	int mRecordIdIndex;
	int mSourceIndex;
	int mCmGroupIndex;

	int mNameIndex;
	int mNumberIndex;
	int mDisplayNumberIndex;
	int mLabelIndex;
	int mCmGroupNameIndex;

	int mIsVoxoxIndex;
	int mIsFavoriteIndex;

	int mXmppJidIndex;
	int mXmppPresenceIndex;
	int mXmppStatusIndex;
	int mXmppGroupIndex;

	//New Contact-Sync
	int mContactKeyIndex;
	int mCompanyIndex;
	int mFirstNameIndex;
	int mLastNameIndex;
	int mUserIdIndex;
	int mUserNameIndex;
	int mIsBlockedIndex;
	int mIsExtensionIndex;
};

