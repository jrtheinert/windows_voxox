
/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

/**
 *	This is a GET-ONLY class.
 */

#pragma once

#include "WrappedCursor.h"

using namespace VoxoxSQLite;

class ContactListCursor : public WrappedCursor
{
public:
	ContactListCursor( VoxoxSQLiteCursor* cursor );
	~ContactListCursor();

	ContactList	getData();

private:
	int getRecordId()					{ return getIntEx( mRecordIdIndex,			 0 );	}
	int getCmGroup()					{ return getIntEx( mCmGroupIndex,			 Contact::CmGroup::INVALID_CMGROUP );	}
	int getType()						{ return getIntEx( mTypeIndex,				 1 );	}	//Default to Contact = 1.

	//Some useful counts
	int getVoxoxCount()					{ return getIntEx( mVoxoxCountIndex,		 0 );	}
	int getBlockedCount()				{ return getIntEx( mBlockedCountIndex,		 0 );	}
	int getFavoriteCount()				{ return getIntEx( mFavoriteCountIndex,		 0 );	}
	int getPhoneNumberCount()			{ return getIntEx( mPhoneNumberCountIndex,	 0 );	}
	int getExtensionCount()				{ return getIntEx( mExtensionCountIndex,	 0 );	}
	
	std::string getContactKey()			{ return getStringEx( mContactKeyIndex,		"" );	}	//Contact-Sync
	std::string getSource()				{ return getStringEx( mSourceIndex,			"" );	}
	std::string getCmGroupName()		{ return getStringEx( mCmGroupNameIndex,	"" );	}
	std::string getDisplayName()		{ return getStringEx( mDisplayNameIndex,	"" );	}
	std::string getGroupId()			{ return getStringEx( mGroupIdIndex,		"" );	}
	std::string getName()				{ return getStringEx( mNameIndex,			"" );	}
	std::string getFirstName()			{ return getStringEx( mFirstNameIndex,		"" );	}	//Contact-Sync
	std::string getLastName()			{ return getStringEx( mLastNameIndex,		"" );	}	//Contact-Sync
	std::string getCompany()			{ return getStringEx( mCompanyIndex,		"" );	}	//Contact-Sync

	std::string getExtension()			{ return getStringEx( mExtensionIndex,		"" );	}	//Get extension, if one exists.  For Company Directory on CloudPhone
	std::string getPreferredNumber()	{ return getStringEx( mPreferredNumberIndex, "" );	}	//Get preferred Number

private:
	//Get field indexes from field names, for greater efficiency
	void initIndexes();

private:
	ContactList mData;

	int mRecordIdIndex;
	int mCmGroupIndex;
	int mSourceIndex;
	int mNameIndex;
	int mDisplayNameIndex;
	int mCmGroupNameIndex;
	int mPreferredNumberIndex;

	int mTypeIndex;
	int mGroupIdIndex;

	//New Contact-Sync
	int mContactKeyIndex;
	int mCompanyIndex;
	int mFirstNameIndex;
	int mLastNameIndex;

	//Some useful counts
	int mVoxoxCountIndex;
	int mPhoneNumberCountIndex;
	int mBlockedCountIndex;
	int mFavoriteCountIndex;
	int mExtensionCountIndex;

	//For CloudPhone
	int mExtensionIndex;
};

