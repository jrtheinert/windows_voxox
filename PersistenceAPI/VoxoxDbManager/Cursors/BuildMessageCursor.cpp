/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

#include "BuildMessageCursor.h"

BuildMessageCursor::BuildMessageCursor( VoxoxSQLiteCursor* cursor ) : WrappedCursor( cursor )
{
	initIndexes();
}

BuildMessageCursor::~BuildMessageCursor()
{
}

void BuildMessageCursor::initIndexes()
{
	mRecordIdIndex			= fieldIndex( DatabaseTables::Message::ID.c_str()				);

	mMessageIdIndex			= fieldIndex( DatabaseTables::Message::MESSAGE_ID.c_str()		);
	mTypeIndex				= fieldIndex( DatabaseTables::Message::TYPE.c_str()				);
	mDirIndex				= fieldIndex( DatabaseTables::Message::DIRECTION.c_str()		);
	mStatusIndex			= fieldIndex( DatabaseTables::Message::STATUS.c_str()			);

	mBodyIndex				= fieldIndex( DatabaseTables::Message::BODY.c_str()				);
	mTranslatedIndex		= fieldIndex( DatabaseTables::Message::TRANSLATED.c_str()		);

	mFileLocalIndex			= fieldIndex( DatabaseTables::Message::FILE_LOCAL.c_str()		);
	mThumbnailLocalIndex	= fieldIndex( DatabaseTables::Message::THREAD_ID.c_str()		);
	mDurationIndex			= fieldIndex( DatabaseTables::Message::DURATION.c_str()			);

	mFromDidIndex			= fieldIndex( DatabaseTables::Message::FROM_DID.c_str()			);
	mToDidIndex				= fieldIndex( DatabaseTables::Message::TO_DID.c_str()			);

	mFromJidIndex			= fieldIndex( DatabaseTables::Message::FROM_JID.c_str()			);
	mToJidIndex				= fieldIndex( DatabaseTables::Message::TO_JID.c_str()			);

	mFromUserIdIndex		= fieldIndex( DatabaseTables::Message::FROM_USER_ID.c_str()		);
	mToGroupIdIndex			= fieldIndex( DatabaseTables::Message::TO_GROUP_ID.c_str()		);

	mTimestampIndex			= fieldIndex( DatabaseTables::Message::LOCAL_TIMESTAMP.c_str()	);
	mServerTimestampIndex	= fieldIndex( DatabaseTables::Message::SERVER_TIMESTAMP.c_str()	);

	mModifiedIndex			= fieldIndex( DatabaseTables::Message::MODIFIED.c_str()			);
	mIAccountIndex			= fieldIndex( DatabaseTables::Message::IACCOUNT.c_str()			);
	mCNameIndex				= fieldIndex( DatabaseTables::Message::CNAME.c_str()			);
}

MessageList BuildMessageCursor::getData()
{
	mData.clear();

	//We always start at first row, if there is any data.
	while ( !eof() )
	{
		Message data;

		Message::Type      type   = (Message::Type)      getType();
		Message::Direction dir    = (Message::Direction) getDir();
		Message::Status    status = (Message::Status)    getStatus();

		data.setRecordId       ( getRecordId()			);

		data.setMsgId		   ( getMessageId()			);
		data.setType		   ( type					);
		data.setDirection	   ( dir					);
		data.setStatus		   ( status					);
		data.setBody		   ( getBody()				);
		data.setTranslateBody  ( getTranslated()		);
		data.setFileLocal	   ( getFileLocal()			);
		data.setThumbnailLocal ( getThumbnailLocal()	);
		data.setDuration	   ( getDuration()			);
		data.setFromDid		   ( getFromDid()			);
		data.setToDid		   ( getToDid()				);
		data.setFromJid		   ( getFromJid()			);
		data.setToJid		   ( getToJid()				);

		data.setFromUserId     ( getFromUserId()		);
		data.setToGroupId	   ( getToGroupId()			);

		data.setLocalTimestamp ( getLocalTimestamp()	);
		data.setServerTimestamp( getServerTimestamp()	);

		data.setModified	   ( getModified()			);
		data.setIAccount	   ( getIAccount()			);
		data.setCName		   ( getCName()				);

		mData.push_back( data );

		nextRow();
	}

	return mData;
}
