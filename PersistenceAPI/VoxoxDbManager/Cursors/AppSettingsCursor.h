/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

/**
 *	This is a GET-ONLY class.
 */

#pragma once

#include "WrappedCursor.h"

using namespace VoxoxSQLite;

class AppSettingsCursor : public WrappedCursor
{
public:
	AppSettingsCursor( VoxoxSQLiteCursor* cursor );
	~AppSettingsCursor();

	AppSettings	getData();

private:
	int			getRecordId()					{ return getIntEx( mRecordIdIndex,	-1 );					}

	bool		getAutoLogin()					{ return getAutoLoginRaw()			!= 0;					}
	bool		getRememberPassword()			{ return getRememberPasswordRaw()	!= 0;					}
	std::string getLastLoginUsername()			{ return getStringEx( mLastLoginUsernameIndex,		"" );	}
	std::string getLastLoginPassword()			{ return getStringEx( mLastLoginPasswordIndex,		"" );	}
	std::string getLastLoginCountryCode()		{ return getStringEx( mLastLoginCountryCodeIndex,	"" );	}
	std::string getLastLoginPhoneNumber()		{ return getStringEx( mLastLoginPhoneNumberIndex,	"" );	}

	//Debug items
	std::string getDebugMenu()					{ return getStringEx( mDebugMenuIndex,			"" );	}
	std::string getIgnoreLoggers()				{ return getStringEx( mIgnoreLoggersIndex,		"" );	}
	bool		getMaxSipLogging()				{ return getMaxSipLoggingRaw()		!= 0;				}
	int			getGetInitOptionsHandling()		{ return getIntEx( mGetInitOptionsHandlingIndex, 0 );	}
				
private:
	//Get field indexes from field names, for greater efficiency
	void initIndexes();

	int  getAutoLoginRaw()					{ return getIntEx( mAutoLoginIndex,			0 );	}
	int  getRememberPasswordRaw()			{ return getIntEx( mRememberPasswordIndex,	0 );	}

	int  getMaxSipLoggingRaw()				{ return getIntEx( mMaxSipLoggingIndex,		0 );	}

private:
	AppSettings mData;

	int mRecordIdIndex;

	int mAutoLoginIndex;
	int mRememberPasswordIndex;
	int mLastLoginUsernameIndex;
	int mLastLoginPasswordIndex;
	int mLastLoginCountryCodeIndex;
	int mLastLoginPhoneNumberIndex;

	//Debug items
	int mDebugMenuIndex;
	int mIgnoreLoggersIndex;
	int mMaxSipLoggingIndex;
	int mGetInitOptionsHandlingIndex;
};

