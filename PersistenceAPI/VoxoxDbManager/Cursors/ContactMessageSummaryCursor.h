/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

/**
 *	This is a GET-ONLY class.
 */

#pragma once

#include "WrappedCursor.h"

using namespace VoxoxSQLite;

class ContactMessageSummaryCursor : public WrappedCursor
{
public:
	ContactMessageSummaryCursor( VoxoxSQLiteCursor* cursor );
	~ContactMessageSummaryCursor();

	ContactMessageSummaryList	getData();

private:
	int							getCmGroup()						{ return getIntEx( mCmGroupIndex,			Contact::CmGroup::INVALID_CMGROUP );	}
	GroupMessageGroup::Status	getGroupStatus()					{ return (GroupMessageGroup::Status)getIntEx( mGroupStatusIndex,		0 );	}
	
	std::string					getContactKey()						{ return getStringEx( mContactKeyIndex,		"" );	}
	std::string					getSource()							{ return getStringEx( mSourceIndex,			"" );	}
	std::string					getCmGroupName()					{ return getStringEx( mCmGroupNameIndex,	"" );	}
	std::string					getDisplayName()					{ return getStringEx( mDisplayNameIndex,	"" );	}
	std::string					getDisplayNumber()					{ return getStringEx( mDisplayNumberIndex,	"" );	}
	std::string					getPhoneNumber()					{ return getStringEx( mPhoneNumberIndex,	"" );	}

	std::string					getToGroupId()						{ return getStringEx( mToGroupIdIndex,		"" );	}
	std::string					getGroupName()						{ return getStringEx( mGroupNameIndex,		"" );	}
	std::string					getXmppJid()						{ return getStringEx( mXmppJidIndex,		"" );	}

	//Msg summary
	Message::Direction			getMsgDir()							{ return (Message::Direction)getIntEx( mMsgDirIndex,  0 );	}
	Message::Type				getMsgType()						{ return (Message::Type)	 getIntEx( mMsgTypeIndex, 0 );	}
	__int64						getMsgServerTimestamp()				{ return getInt64Ex( mMsgServerTimestampIndex, 0 );	}

	std::string					getMsgBody()						{ return getStringEx( mMsgBodyIndex,		"" );	}
	std::string					getMsgTranslated()					{ return getStringEx( mMsgTranslatedIndex,	"" );	}
	std::string					getMsgToDid()						{ return getStringEx( mMsgToDidIndex,		"" );	}
	std::string					getMsgFromDid()						{ return getStringEx( mMsgFromDidIndex,		"" );	}
	std::string					getMsgToJid()						{ return getStringEx( mMsgToJidIndex,		"" );	}
	std::string					getMsgFromJid()						{ return getStringEx( mMsgFromJidIndex,		"" );	}

private:
	//Get field indexes from field names, for greater efficiency
	void initIndexes();

private:
	ContactMessageSummaryList mData;

	int mContactKeyIndex;
	int mSourceIndex;
	int mCmGroupIndex;
	int mCmGroupNameIndex;
	int mDisplayNameIndex;
	int mPhoneNumberIndex;
	int mDisplayNumberIndex;
	int mXmppJidIndex;

	int mToGroupIdIndex;
	int mGroupNameIndex;
	int mGroupStatusIndex;

	//Newest message info
	int mMsgBodyIndex;
	int mMsgTranslatedIndex;
	int mMsgServerTimestampIndex;
	int mMsgDirIndex;
	int mMsgTypeIndex;
	int mMsgToDidIndex;
	int mMsgFromDidIndex;
	int mMsgToJidIndex;
	int mMsgFromJidIndex;
};

