/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

#include "GroupMessageMemberOfCursor.h"

GroupMessageMemberOfCursor::GroupMessageMemberOfCursor( VoxoxSQLiteCursor* cursor ) : WrappedCursor( cursor )
{
	initIndexes();
}

GroupMessageMemberOfCursor::~GroupMessageMemberOfCursor()
{
}

void GroupMessageMemberOfCursor::initIndexes()
{
	mRecordIdIndex			= fieldIndex( DatabaseTables::GroupMessageGroup::ID.c_str()				);
	mGroupIdIndex			= fieldIndex( DatabaseTables::GroupMessageGroup::GROUP_ID.c_str()		);
	mGroupNameIndex			= fieldIndex( DatabaseTables::GroupMessageGroup::NAME.c_str()			);
		
	mUserIdIndex			= fieldIndex( DatabaseTables::GroupMessageMember::USER_ID.c_str()		);
	mIsAdminIndex			= fieldIndex( DatabaseTables::GroupMessageMember::IS_ADMIN.c_str()		);
	mNicknameIndex			= fieldIndex( DatabaseTables::GroupMessageMember::NICKNAME.c_str()		);

	mDisplayNameIndex		= fieldIndex( DatabaseTables::GroupMessageMember::DISPLAY_NAME.c_str()	);
}

GroupMessageMemberOfList GroupMessageMemberOfCursor::getData()
{
	mData.clear();

	//We always start at first row, if there is any data.
	while ( !eof() )
	{
		GroupMessageMemberOf data;

		data.setRecordId ( getRecordId()	);
		data.setGroupId  ( getGroupId()		);
		data.setGroupName( getGroupName()	);

		data.setUserId	 ( getUserId()		);
		data.setIsAdmin	 ( isAdmin()		);
		data.setNickName ( getNickname()	);

		data.setDisplayName( getDisplayName() );

		mData.push_back( data );

		nextRow();
	}

	return mData;
}
