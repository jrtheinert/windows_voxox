/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

#include "GroupMessageGroupCursor.h"

GroupMessageGroupCursor::GroupMessageGroupCursor( VoxoxSQLiteCursor* cursor ) : WrappedCursor( cursor )
{
	initIndexes();
}

GroupMessageGroupCursor::~GroupMessageGroupCursor()
{
}

void GroupMessageGroupCursor::initIndexes()
{
	mRecordIdIndex			= fieldIndex( DatabaseTables::GroupMessageGroup::ID.c_str()					);
	mGroupIdIndex			= fieldIndex( DatabaseTables::GroupMessageGroup::GROUP_ID.c_str()			);
	mNameIndex				= fieldIndex( DatabaseTables::GroupMessageGroup::NAME.c_str()				);
	mStatusIndex			= fieldIndex( DatabaseTables::GroupMessageGroup::STATUS.c_str()				);
	mInviteTypeIndex		= fieldIndex( DatabaseTables::GroupMessageGroup::INVITE_TYPE.c_str()		);
	mAnnounceMembersIndex	= fieldIndex( DatabaseTables::GroupMessageGroup::ANNOUNCE_MEMBERS.c_str()	);
	mConfirmationCodeIndex	= fieldIndex( DatabaseTables::GroupMessageGroup::CONFIRMATION_CODE.c_str()	);
	mAvatarIndex			= fieldIndex( DatabaseTables::GroupMessageGroup::AVATAR.c_str()				);
																			 
	mMemberCountIndex		= fieldIndex( DatabaseTables::GroupMessageGroup::MEMBER_COUNT.c_str()		);
	mAdminUserIdIndex		= fieldIndex( DatabaseTables::GroupMessageGroup::ADMIN_USER_ID.c_str()		);
}

GroupMessageGroupList GroupMessageGroupCursor::getData()
{
	mData.clear();

	//We always start at first row, if there is any data.
	while ( !eof() )
	{
		GroupMessageGroup data;

		GroupMessageGroup::Status status = (GroupMessageGroup::Status)getStatus();

		data.setAdminUserId		( getAdminUserId()		);
		data.setAnnounceMembers ( getAnnounceMembers()	);
		data.setConfirmationCode( getConfirmationCode() );
		data.setGroupId			( getGroupId()			);
		data.setInviteType		( getInviteType()		);
		data.setName			( getName()				);
		data.setRecordId		( getRecordId()			);
		data.setStatus			( status				);

		mData.push_back( data );

		nextRow();
	}

	return mData;
}
