/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

/**
 *	This is a GET-ONLY class.
 */

#pragma once

#include "WrappedCursor.h"

using namespace VoxoxSQLite;

class MessageTranslationPropertyCursor : public WrappedCursor
{
public:
	MessageTranslationPropertyCursor( VoxoxSQLiteCursor* cursor );
	~MessageTranslationPropertyCursor();

	MessageTranslationPropertiesList	getData();

private:
	int getRecordId()					{ return getIntEx( mRecordIdIndex,				 0 );	}
	int getDirection()					{ return getIntEx( mDirIndex,					 0 );	}
	int	getContactId()					{ return getIntEx( mContactIdIndex,				 0 );	}
	bool isEnabled()					{ return getEnabledRaw() > 0;						}
	
	std::string getUserLanguage()		{ return getStringEx( mUserLanguageIndex,		"" );	}
	std::string getContactLanguage()	{ return getStringEx( mContactLanguageIndex,	"" );	}
	std::string getPhoneNumber()		{ return getStringEx( mPhoneNumberIndex,		"" );	}
	std::string getJid()				{ return getStringEx( mJidIndex,				"" );	}
				
private:
	//Get field indexes from field names, for greater efficiency
	void initIndexes();
	int getEnabledRaw()					{ return getIntEx( mEnabledIndex,				 0 );	}

private:
	MessageTranslationPropertiesList mData;

	int mRecordIdIndex;
	int mDirIndex;
	int mEnabledIndex;

	int mContactIdIndex;
	int mUserLanguageIndex;
	int mContactLanguageIndex;
	int mPhoneNumberIndex;
	int mJidIndex;
};

