/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

#include "ContactMessageSummaryCursor.h"

ContactMessageSummaryCursor::ContactMessageSummaryCursor( VoxoxSQLiteCursor* cursor ) : WrappedCursor( cursor )
{
	initIndexes();
}

ContactMessageSummaryCursor::~ContactMessageSummaryCursor(void)
{
}

void ContactMessageSummaryCursor::initIndexes()
{
	mContactKeyIndex		 = fieldIndex( DatabaseTables::PhoneNumber::CONTACT_KEY.c_str()		);
	mSourceIndex			 = fieldIndex( DatabaseTables::PhoneNumber::SOURCE.c_str()			);
	mCmGroupIndex			 = fieldIndex( DatabaseTables::PhoneNumber::CM_GROUP.c_str()		);
	mDisplayNameIndex		 = fieldIndex( DatabaseTables::Contact::DISPLAY_NAME.c_str()		);
	mCmGroupNameIndex		 = fieldIndex( DatabaseTables::PhoneNumber::CM_GROUP_NAME.c_str()	);
	mPhoneNumberIndex		 = fieldIndex( DatabaseTables::PhoneNumber::NUMBER.c_str()			);
	mDisplayNumberIndex		 = fieldIndex( DatabaseTables::PhoneNumber::DISPLAY_NUMBER.c_str()	);
	mXmppJidIndex			 = fieldIndex( DatabaseTables::PhoneNumber::XMPP_JID.c_str()		);

	mGroupNameIndex			 = fieldIndex( DatabaseTables::GroupMessageGroup::NAME.c_str()		);
	mGroupStatusIndex		 = fieldIndex( DatabaseTables::GroupMessageGroup::STATUS.c_str()	);
	mToGroupIdIndex			 = fieldIndex( DatabaseTables::Message::TO_GROUP_ID.c_str()			);

	//Newest message info
	mMsgBodyIndex			 = fieldIndex( DatabaseTables::Message::BODY.c_str()				);
	mMsgTranslatedIndex		 = fieldIndex( DatabaseTables::Message::TRANSLATED.c_str()			);
	mMsgServerTimestampIndex = fieldIndex( DatabaseTables::Message::SERVER_TIMESTAMP.c_str()	);
	mMsgDirIndex			 = fieldIndex( DatabaseTables::Message::DIRECTION.c_str()			);
	mMsgTypeIndex			 = fieldIndex( DatabaseTables::Message::TYPE.c_str()				);
	mMsgToDidIndex			 = fieldIndex( DatabaseTables::Message::TO_DID.c_str()				);
	mMsgFromDidIndex		 = fieldIndex( DatabaseTables::Message::FROM_DID.c_str()			);
	mMsgToJidIndex			 = fieldIndex( DatabaseTables::Message::TO_JID.c_str()				);
	mMsgFromJidIndex		 = fieldIndex( DatabaseTables::Message::FROM_JID.c_str()			);
}

ContactMessageSummaryList ContactMessageSummaryCursor::getData()
{
	mData.clear();

	//We always start at first row, if there is any data.
	while ( !eof() )
	{
		ContactMessageSummary data;

		data.setContactKey	 ( getContactKey()    );
		data.setSource		 ( getSource()		  );
		data.setCmGroup		 ( getCmGroup()		  );
		data.setCmGroupName	 ( getCmGroupName()   );
		data.setDisplayName	 ( getDisplayName()   );
		data.setDisplayNumber( getDisplayNumber() );
		data.setPhoneNumber	 ( getPhoneNumber()   );
		data.setXmppJid		 ( getXmppJid()		  );

		data.setToGroupId	 ( getToGroupId()	  );
		data.setGroupName	 ( getGroupName()	  );
		data.setGroupStatus	 ( getGroupStatus()   );

		//Message summary
		data.setMsgType			  ( getMsgType()			);
		data.setMsgDir			  ( getMsgDir()				);
		data.setMsgBody			  ( getMsgBody()			);
		data.setMsgFromDid		  ( getMsgFromDid()			);
		data.setMsgFromJid		  ( getMsgFromJid()			);
		data.setMsgServerTimestamp( getMsgServerTimestamp() );
		data.setMsgToDid		  ( getMsgToDid()			);
		data.setMsgToJid		  ( getMsgToJid()			);
		data.setMsgTranslated	  ( getMsgTranslated()		);
	
		mData.push_back( data );

		nextRow();
	}

	return mData;
}
