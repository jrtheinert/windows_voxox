
/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

//NOTE: This should NOT contain any PhoneNumber specific data.
//	It SHOULD contain PhoneNumber summary data such as FavoriteCount, BlockedCount, PhoneNumberCount, etc.
#include "ContactListCursor.h"

ContactListCursor::ContactListCursor( VoxoxSQLiteCursor* cursor ) : WrappedCursor( cursor )
{
	initIndexes();
}

ContactListCursor::~ContactListCursor()
{
}

void ContactListCursor::initIndexes()
{
	mRecordIdIndex			= fieldIndex( DatabaseTables::PhoneNumber::ID.c_str()				);
	mCmGroupIndex			= fieldIndex( DatabaseTables::PhoneNumber::CM_GROUP.c_str()			);
	mSourceIndex			= fieldIndex( DatabaseTables::PhoneNumber::SOURCE.c_str()			);
	mNameIndex				= fieldIndex( DatabaseTables::PhoneNumber::NAME.c_str()				);
	mDisplayNameIndex		= fieldIndex( DatabaseTables::Contact::DISPLAY_NAME.c_str()			);
	mCmGroupNameIndex		= fieldIndex( DatabaseTables::PhoneNumber::CM_GROUP_NAME.c_str()	);

	mTypeIndex				= fieldIndex( DatabaseTables::Contact::TYPE.c_str()					);
	mGroupIdIndex			= fieldIndex( DatabaseTables::GroupMessageGroup::GROUP_ID.c_str()	);

	//New Contact Syncing
	mContactKeyIndex		= fieldIndex( DatabaseTables::PhoneNumber::CONTACT_KEY.c_str()		);
	mCompanyIndex			= fieldIndex( DatabaseTables::PhoneNumber::COMPANY.c_str()			);
	mFirstNameIndex			= fieldIndex( DatabaseTables::PhoneNumber::FIRST_NAME.c_str()		);
	mLastNameIndex			= fieldIndex( DatabaseTables::PhoneNumber::LAST_NAME.c_str()		);

	mVoxoxCountIndex		= fieldIndex( DatabaseTables::Contact::VOXOX_COUNT.c_str()			);
	mBlockedCountIndex		= fieldIndex( DatabaseTables::Contact::BLOCKED_COUNT.c_str()		);
	mFavoriteCountIndex		= fieldIndex( DatabaseTables::Contact::FAVORITE_COUNT.c_str()		);
	mPhoneNumberCountIndex	= fieldIndex( DatabaseTables::Contact::PHONE_NUMBER_COUNT.c_str()	);
	mExtensionCountIndex    = fieldIndex( DatabaseTables::Contact::EXTENSION_COUNT.c_str()		);

	mExtensionIndex		   = fieldIndex( DatabaseTables::Contact::EXTENSION.c_str()				);
	mPreferredNumberIndex  = fieldIndex( DatabaseTables::Contact::PREFERRED_NUMBER.c_str()		);
}

ContactList ContactListCursor::getData()
{
	mData.clear();

	//We always start at first row, if there is any data.
	while ( !eof() )
	{
		Contact data;

		data.setCmGroup			 ( getCmGroup()           );
		data.setSource			 ( getSource()            );
		data.setDisplayName		 ( getDisplayName()		  );
		data.setGroupId			 ( getGroupId()			  );
		data.setName			 ( getName()			  );
		data.setCmGroupName		 ( getCmGroupName()		  );
		data.setRecordId		 ( getRecordId()		  );
		data.setType			 ( getType()			  );

		//New contact syncing
		data.setContactKey( getContactKey()	);
		data.setCompany   ( getCompany()	);
		data.setFirstName ( getFirstName()	);
		data.setLastName  ( getLastName()	);

		data.setVoxoxCount		( getVoxoxCount()		);
		data.setBlockedCount	( getBlockedCount()		);
		data.setFavoriteCount	( getFavoriteCount()	);
		data.setPhoneNumberCount( getPhoneNumberCount() );
		data.setExtensionCount  ( getExtensionCount()	);

		data.setExtension		( getExtension()		);
		data.setPreferredNumber	( getPreferredNumber()	);

		mData.push_back( data );

		nextRow();
	}

	return mData;
}
