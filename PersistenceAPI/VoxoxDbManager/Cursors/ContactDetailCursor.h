/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

/**
 *	This is a GET-ONLY class.
 */

#pragma once

#include "WrappedCursor.h"

using namespace VoxoxSQLite;

class ContactDetailCursor : public WrappedCursor
{
public:
	ContactDetailCursor( VoxoxSQLiteCursor* cursor );
	~ContactDetailCursor();

	PhoneNumberList	getData();

private:
	int getRecordId()				{ return getIntEx( mRecordIdIndex,		 0 );	}
	int getCmGroup()				{ return getIntEx( mCmGroupIndex,		 Contact::CmGroup::INVALID_CMGROUP );	}
	int getIsVoxox()				{ return getIntEx( mIsVoxoxIndex,		-1 );	}

	Contact::Type getType()			{ return (Contact::Type)getIntEx( mTypeIndex,  1 );	}	//Default to Contact = 1.

	bool isFavorite()				{ return getIsFavoriteRaw() > 0;	}
	
	std::string getSource()			{ return getStringEx( mSourceIndex,				"" );	}
	std::string getDisplayNumber()	{ return getStringEx( mDisplayNumberIndex,		"" );	}
	std::string getLabel()			{ return getStringEx( mLabelIndex,				"" );	}
	std::string getGroupId()		{ return getStringEx( mGroupPhoneNumberIndex,	"" );	}
	std::string getName()			{ return getStringEx( mGroupPhoneNameIndex,		"" );	}
	std::string getCmGroupName()	{ return getStringEx( mCmGroupNameIndex,		"" );	}

	//New contact sycning
	std::string	getContactKey()		{ return getStringEx( mContactKeyIndex,			"" );	}
	std::string getCompany()		{ return getStringEx( mCompanyIndex,			"" );	}
	std::string getFirstName()		{ return getStringEx( mFirstNameIndex,			"" );	}
	std::string getLastName()		{ return getStringEx( mLastNameIndex,			"" );	}
	int			getUserId()			{ return getIntEx   ( mUserIdIndex,				-1 );	}
	std::string getUserName()		{ return getStringEx( mUserNameIndex,			"" );	}
	bool		isBlocked()			{ return getIsBlockedRaw()   > 0;	}
	bool		isExtension()		{ return getIsExtensionRaw() > 0;	}

private:
	//Get field indexes from field names, for greater efficiency
	void initIndexes();
	int getIsFavoriteRaw()			{ return getIntEx( mIsFavoriteIndex,	-1 );	}
	int getIsBlockedRaw()			{ return getIntEx( mIsBlockedIndex,		-1 );	}
	int getIsExtensionRaw()			{ return getIntEx( mIsExtensionIndex,	-1 );	}

private:
//	ContactDetailPhoneNumberList mData;
	PhoneNumberList mData;

	int mRecordIdIndex;
	int mSourceIndex;
	int mCmGroupIndex;
	int mDisplayNumberIndex;
	int mLabelIndex;
	int mIsVoxoxIndex;
	int mIsFavoriteIndex;
	int mTypeIndex;
	int mGroupPhoneNumberIndex;
	int mGroupPhoneNameIndex;
	int mCmGroupNameIndex;

	//New Contact Syncing
	int mContactKeyIndex;
	int mCompanyIndex;
	int mFirstNameIndex;
	int mLastNameIndex;
	int mUserIdIndex;
	int mUserNameIndex;
	int mIsBlockedIndex;
	int mIsExtensionIndex;
};

