#pragma once
/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

/**
 *	This is a GET-ONLY class.
 */

#pragma once

#include "WrappedCursor.h"

using namespace VoxoxSQLite;

class RecentCallCursor : public WrappedCursor
{
public:
	RecentCallCursor( VoxoxSQLiteCursor* cursor );
	~RecentCallCursor();

	RecentCallList	getData();

private:
	int					getRecordId()				{ return getIntEx( mRecordIdIndex,			0 );	}
	__int64				getTimestamp()				{ return getInt64Ex( mTimestampIndex,		0 );	}
	__int64				getStartTimestamp()			{ return getInt64Ex( mStartTimestampIndex,	0 );	}
	int					getDuration()				{ return getIntEx( mDurationIndex,			0 );	}	
	std::string			getUid()					{ return getStringEx( mUidIndex,			"");	}
	bool				isLocal()	     			{ return getIsLocalRaw() > 0;                       }
	bool				isIncoming()				{ return getIsIncomingRaw() > 0;					}
	RecentCall::Status	getStatus()					{ return (RecentCall::Status) getIntEx( mStatusIndex, 0 );	}
	RecentCall::Seen	getSeen()					{ return (RecentCall::Seen  ) getIntEx( mSeenIndex,   0 );	}

	std::string getNumber()							{ return getStringEx( mNumberIndex,			"" );	}
	std::string getOrigNumber()						{ return getStringEx( mOrigNumberIndex,		"" );	}
	std::string getDisplayNumber()					{ return getStringEx( mDisplayNumberIndex,	"" );	}
	std::string getName()							{ return getStringEx( mNameIndex,			"" );	}
	std::string getContactKey()						{ return getStringEx( mContactKeyIndex,		"" );	}
	int			getCmGroup()						{ return getIntEx   ( mCmGroupIndex,		Contact::CmGroup::INVALID_CMGROUP );	}
	std::string getCmGroupName()					{ return getStringEx( mCmGroupNameIndex,	"" );	}

private:
	//Get field indexes from field names, for greater efficiency
	void initIndexes();
	int getIsIncomingRaw()							{ return getIntEx( mIsIncomingIndex, 0 );	}
	int getIsLocalRaw()						    	{ return getIntEx( mIsLocalIndex,    0 );	}

private:
	RecentCallList mData;

	int mRecordIdIndex;
	int mStatusIndex;
	int mIsIncomingIndex;
	int mTimestampIndex;
	int mStartTimestampIndex;
	int mDurationIndex;
	int mIsLocalIndex;
	int mSeenIndex;
	int mUidIndex;

	int mNumberIndex;
	int mOrigNumberIndex;
	int mDisplayNumberIndex;
	int mNameIndex;
	int mContactKeyIndex;
	int mCmGroupIndex;
	int mCmGroupNameIndex;
};

