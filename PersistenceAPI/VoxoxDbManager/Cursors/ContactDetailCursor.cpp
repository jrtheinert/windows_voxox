/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

#include "ContactDetailCursor.h"

ContactDetailCursor::ContactDetailCursor( VoxoxSQLiteCursor* cursor ) : WrappedCursor( cursor )
{
	initIndexes();
}

ContactDetailCursor::~ContactDetailCursor()
{
}

void ContactDetailCursor::initIndexes()
{
	mRecordIdIndex			= fieldIndex( DatabaseTables::PhoneNumber::ID.c_str()			  );
	mSourceIndex			= fieldIndex( DatabaseTables::PhoneNumber::SOURCE.c_str()		  );
	mCmGroupIndex			= fieldIndex( DatabaseTables::PhoneNumber::CM_GROUP.c_str()		  );
	mDisplayNumberIndex		= fieldIndex( DatabaseTables::PhoneNumber::DISPLAY_NUMBER.c_str() );
	mLabelIndex				= fieldIndex( DatabaseTables::PhoneNumber::LABEL.c_str()		  );
	mIsVoxoxIndex			= fieldIndex( DatabaseTables::PhoneNumber::IS_VOXOX.c_str()		  );
	mIsFavoriteIndex		= fieldIndex( DatabaseTables::PhoneNumber::IS_FAVORITE.c_str()	  );
	mTypeIndex				= fieldIndex( DatabaseTables::Contact::TYPE.c_str()				  );
	mGroupPhoneNumberIndex	= fieldIndex( DatabaseTables::PhoneNumber::NUMBER.c_str()		  );
	mGroupPhoneNameIndex	= fieldIndex( DatabaseTables::PhoneNumber::NAME.c_str()			  );
	mCmGroupNameIndex		= fieldIndex( DatabaseTables::PhoneNumber::CM_GROUP_NAME.c_str()  );

	//New Contact Syncing
	mContactKeyIndex	= fieldIndex( DatabaseTables::PhoneNumber::CONTACT_KEY.c_str()		  );
	mCompanyIndex		= fieldIndex( DatabaseTables::PhoneNumber::COMPANY.c_str()			  );
	mFirstNameIndex		= fieldIndex( DatabaseTables::PhoneNumber::FIRST_NAME.c_str()		  );
	mLastNameIndex		= fieldIndex( DatabaseTables::PhoneNumber::LAST_NAME.c_str()		  );
	mUserIdIndex		= fieldIndex( DatabaseTables::PhoneNumber::USER_ID.c_str()			  );
	mUserNameIndex		= fieldIndex( DatabaseTables::PhoneNumber::USER_NAME.c_str()		  );
	mIsBlockedIndex		= fieldIndex( DatabaseTables::PhoneNumber::IS_BLOCKED.c_str()		  );
	mIsExtensionIndex	= fieldIndex( DatabaseTables::PhoneNumber::IS_EXT.c_str()			  );
}

PhoneNumberList ContactDetailCursor::getData()
{
	mData.clear();

	//We always start at first row, if there is any data.
	while ( !eof() )
	{
		PhoneNumber data;

		data.setSource			( getSource()           );
		data.setCmGroup			( getCmGroup()          );
		data.setDisplayNumber	( getDisplayNumber()    );
		data.setName			( getName()				);
		data.setCmGroupName		( getCmGroupName()		);
		data.setNumber			( getGroupId()			);
		data.setIsFavorite      ( isFavorite()			);
		data.setNumberType	    ( getIsVoxox()			);
		data.setLabel			( getLabel()			);
		data.setRecordId		( getRecordId()			);
		data.setContactType		( getType()				);	//Contact vs. Group

		//New contact syncing
		data.setContactKey ( getContactKey() );
		data.setCompany    ( getCompany()	 );
		data.setFirstName  ( getFirstName()	 );
		data.setLastName   ( getLastName()	 );
		data.setUserId	   ( getUserId()	 );
		data.setUserName   ( getUserName()	 );
		data.setIsBlocked  ( isBlocked()	 );
		data.setIsExtension( isExtension()	 );

		mData.push_back( data );

		nextRow();
	}

	return mData;
}


