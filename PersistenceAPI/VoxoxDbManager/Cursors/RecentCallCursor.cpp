
/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

#include "RecentCallCursor.h"

RecentCallCursor::RecentCallCursor( VoxoxSQLiteCursor* cursor ) : WrappedCursor( cursor )
{
	initIndexes();
}

RecentCallCursor::~RecentCallCursor()
{
}

void RecentCallCursor::initIndexes()
{
	mRecordIdIndex			= fieldIndex( DatabaseTables::RecentCall::ID.c_str()				);
	mStatusIndex			= fieldIndex( DatabaseTables::RecentCall::STATUS.c_str()			);
	mIsIncomingIndex		= fieldIndex( DatabaseTables::RecentCall::INCOMING.c_str()			);
	mTimestampIndex			= fieldIndex( DatabaseTables::RecentCall::TIMESTAMP.c_str()			);
	mStartTimestampIndex	= fieldIndex( DatabaseTables::RecentCall::START_TIMESTAMP.c_str()	);
	mDurationIndex			= fieldIndex( DatabaseTables::RecentCall::DURATION.c_str()			);
	mIsLocalIndex           = fieldIndex( DatabaseTables::RecentCall::IS_LOCAL.c_str()          );
	mSeenIndex              = fieldIndex( DatabaseTables::RecentCall::SEEN.c_str()				);
	mUidIndex				= fieldIndex( DatabaseTables::RecentCall::UID.c_str()				);

	mNumberIndex			= fieldIndex( DatabaseTables::RecentCall::PHONE_NUMBER.c_str()		);
	mOrigNumberIndex		= fieldIndex( DatabaseTables::RecentCall::ORIG_PHONE_NUMBER.c_str()	);

	mDisplayNumberIndex		= fieldIndex( DatabaseTables::PhoneNumber::NAME.c_str()				);
	mNameIndex				= fieldIndex( DatabaseTables::PhoneNumber::DISPLAY_NUMBER.c_str()	);
	mContactKeyIndex		= fieldIndex( DatabaseTables::PhoneNumber::CONTACT_KEY.c_str()		);
	mCmGroupIndex			= fieldIndex( DatabaseTables::PhoneNumber::CM_GROUP.c_str()			);
	mCmGroupNameIndex		= fieldIndex( DatabaseTables::PhoneNumber::CM_GROUP_NAME.c_str()	);
}

RecentCallList RecentCallCursor::getData()
{
	mData.clear();

	//We always start at first row, if there is any data.
	while ( !eof() )
	{
		RecentCall data;

		data.setRecordId	  ( getRecordId()		);
		data.setStatus		  ( getStatus()			);
		data.setIsIncoming	  ( isIncoming()		);
		data.setTimestamp	  ( getTimestamp()		);
		data.setStartTimestamp( getStartTimestamp() );
		data.setDuration	  ( getDuration()		);
		data.setIsLocal       ( isLocal()           );
		data.setSeen		  ( getSeen()			);
		data.setUid			  ( getUid()			);

		data.setNumber		 ( getNumber()			);
		data.setOrigNumber	 ( getOrigNumber()		);
		data.setDisplayNumber( getDisplayNumber()	);
		data.setName		 ( getName()			);
		data.setContactKey	 ( getContactKey()		);

		data.setCmGroup		 ( getCmGroup()         );
		data.setCmGroupName	 ( getCmGroupName()     );

		mData.push_back( data );

		nextRow();
	}

	return mData;
}


