/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

/**
 *	This is a GET-ONLY class.
 */

#pragma once

#include "WrappedCursor.h"

using namespace VoxoxSQLite;

class BuildMessageCursor : public WrappedCursor
{
public:
	BuildMessageCursor( VoxoxSQLiteCursor* cursor );
	~BuildMessageCursor();

	MessageList	getData();

private:
	int getRecordId()					{ return getIntEx( mRecordIdIndex,			0	);	}

	int getType()						{ return getIntEx( mTypeIndex,				0	);	}
	int getDir()						{ return getIntEx( mDirIndex,				0	);	}
	int getStatus()						{ return getIntEx( mStatusIndex,			0	);	}
	int getDuration()					{ return getIntEx( mDurationIndex,			0	);	}
	int getFromUserId()					{ return getIntEx( mFromUserIdIndex,		0	);	}

	__int64 getLocalTimestamp()			{ return getInt64Ex( mTimestampIndex,		0	);	}
	__int64 getServerTimestamp()		{ return getInt64Ex( mServerTimestampIndex,	0	);	}
	
	std::string getMessageId()			{ return getStringEx( mMessageIdIndex,		""	);	}
	std::string getBody()				{ return getStringEx( mBodyIndex,			""	);	}
	std::string getTranslated()			{ return getStringEx( mTranslatedIndex,		""	);	}

	std::string getFileLocal()			{ return getStringEx( mFileLocalIndex,		""	);	}
	std::string getThumbnailLocal()		{ return getStringEx( mThumbnailLocalIndex,	""	);	}
		
	std::string getFromDid()			{ return getStringEx( mFromDidIndex,		""	);	}
	std::string getToDid()				{ return getStringEx( mToDidIndex,			""	);	}

	std::string getFromJid()			{ return getStringEx( mFromJidIndex,		""	);	}
	std::string getToJid()				{ return getStringEx( mToJidIndex,			""	);	}

	std::string getToGroupId()			{ return getStringEx( mToGroupIdIndex,		""	);	}

	std::string getModified()			{ return getStringEx( mModifiedIndex,		""  );	}
	std::string getIAccount()			{ return getStringEx( mIAccountIndex,		""	);	}
	std::string getCName()				{ return getStringEx( mCNameIndex,			""	);	}

private:
	//Get field indexes from field names, for greater efficiency
	void initIndexes();

private:
	MessageList mData;

	int mRecordIdIndex;

	int mMessageIdIndex;
	int mTypeIndex;
	int mDirIndex;
	int mStatusIndex;

	int mBodyIndex;
	int mTranslatedIndex;

	int mFileLocalIndex;
	int mThumbnailLocalIndex;
	int mDurationIndex;

	int mFromDidIndex;
	int mToDidIndex;

	int mFromJidIndex;
	int mToJidIndex;

	int mFromUserIdIndex;
	int mToGroupIdIndex;

	int mTimestampIndex;
	int mServerTimestampIndex;

	int mModifiedIndex;
	int mIAccountIndex;
	int mCNameIndex;
};

