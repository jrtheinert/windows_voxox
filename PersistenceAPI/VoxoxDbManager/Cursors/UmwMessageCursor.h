/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

/**
 *	This is a GET-ONLY class.
 */

#pragma once

#include "WrappedCursor.h"

using namespace VoxoxSQLite;

class UmwMessageCursor : public WrappedCursor
{
public:
	UmwMessageCursor( VoxoxSQLiteCursor* cursor );
	~UmwMessageCursor();

	UmwMessageList	getData();

private:
	int getRecordId()							{ return getIntEx( mRecordIdIndex,			 0 );	}
	int getCmGroup()							{ return getIntEx( mCmGroupIndex,			 Contact::CmGroup::INVALID_CMGROUP );	}
	int getIsVoxox()							{ return getIntEx( mIsVoxoxIndex,			 0 );	}

	std::string getContactKey()					{ return getStringEx( mContactKeyIndex,		"" );	}
	std::string getPhoneNumber()				{ return getStringEx( mPhoneNumberIndex,	"" );	}
	std::string getName()						{ return getStringEx( mNameIndex,			"" );	}
	std::string getCmGroupName()				{ return getStringEx( mCmGroupNameIndex,	"" );	}
	std::string getGmNickname()					{ return getStringEx( mGmNickNameIndex,		"" );	}
	std::string getGmColor()					{ return getStringEx( mGmColorIndex,		"" );	}
	std::string getGmGroupName()				{ return getStringEx( mGmGroupNameIndex,	"" );	}

	//Main Message Info
	int			getType()						{ return getIntEx( mTypeIndex,				 0 );	}
	int			getDir()						{ return getIntEx( mDirIndex,				 0 );	}
	int			getStatus()						{ return getIntEx( mStatusIndex,			 0 );	}
	int			getDuration()					{ return getIntEx( mDurationIndex,			 0 );	}

	__int64		getLocalTimestamp()				{ return getInt64Ex( mTimestampIndex,		   0 );	}
	__int64		getServerTimestamp()			{ return getInt64Ex( mServerTimestampIndex,	   0 );	}
	__int64		getReadTimestamp()				{ return getInt64Ex( mReadTimestampIndex,	   0 );	}
	__int64		getDeliveredTimestamp()			{ return getInt64Ex( mDeliveredTimestampIndex, 0 );	}

	int			getFromUserId()					{ return getIntEx( mFromUserIdIndex,		 0 );	}

	std::string getMsgId()						{ return getStringEx( mMsgIdIndex,			"" );	}
	std::string getBody()						{ return getStringEx( mBodyIndex,			"" );	}
	std::string getTranslated()					{ return getStringEx( mTranslatedIndex,		"" );	}

	std::string getFileLocal()					{ return getStringEx( mFileLocalIndex,		"" );	}
	std::string getThumbnailLocal()				{ return getStringEx( mThumbnailLocalIndex,	"" );	}

	std::string getFromDid()					{ return getStringEx( mFromDidIndex,		"" );	}
	std::string getToDid()						{ return getStringEx( mToDidIndex,			"" );	}

	std::string getFromJid()					{ return getStringEx( mFromJidIndex,		"" );	}
	std::string getToJid()						{ return getStringEx( mToJidIndex,			"" );	}

	std::string getToGroupId()					{ return getStringEx( mToGroupIdIndex,		"" );	}

private:
	//Get field indexes from field names, for greater efficiency
	void initIndexes();

private:
	UmwMessageList mData;

	int mRecordIdIndex;
	int mContactKeyIndex;
	int mPhoneNumberIndex;	
	int mCmGroupIndex;
	int mNameIndex;
	int mCmGroupNameIndex;
	int mIsVoxoxIndex;
	int mGmNickNameIndex;
	int mGmColorIndex;
	int mGmGroupNameIndex;

	//Main message info
	int mTypeIndex;
	int mDirIndex;
	int mStatusIndex;
	int mMsgIdIndex;
	int mBodyIndex;
	int mTranslatedIndex;

	//Time stamps
	int mTimestampIndex;
	int mServerTimestampIndex;
	int mReadTimestampIndex;
	int mDeliveredTimestampIndex;

	//Rich data
	int mFileLocalIndex;
	int mThumbnailLocalIndex;
	int mDurationIndex;

	//SMS
	int mFromDidIndex;
	int mToDidIndex;

	//Chat
	int mFromJidIndex;
	int mToJidIndex;

	//Group Messaging
	int mToGroupIdIndex;
	int mFromUserIdIndex;
};

