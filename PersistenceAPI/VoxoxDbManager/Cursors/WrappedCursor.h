/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

//A simple, abstract class to wrap a VoxoxSQLiteCursor to simplify extracting standard data
//	We take a VoxoxSQLiteCursor make the underlying methods accessible

#pragma once

#include "..\VoxoxSQLite.h"
#include "..\DataTypes.h"		//Not needed here, but ALL derived classes will need it.
#include "..\DatabaseTables.h"	//Not needed here, but ALL derived classes will need it.
#include "..\DllExport.h"

using namespace VoxoxSQLite;

class DLLEXPORT WrappedCursor
{
public:
	WrappedCursor();
	WrappedCursor( VoxoxSQLiteCursor* cursor );
	~WrappedCursor();

//	virtual <T> getData() = 0;		//TODO: Template would be nice, but each derived class should implement getData().

protected:
	virtual void initIndexes() {};		//TODO: Should be pure virtual, but have link issue with DLL.

	//Wrapped methods:
	int  fieldIndex( const std::string& fieldName );
	bool eof();
	void nextRow();

	int			getIntEx   ( int index, int defaultValue );
	__int64		getInt64Ex ( int index, __int64 defaultValue );
	double		getDoubleEx( int index, double  defaultValue );
	std::string getStringEx( int index, const std::string& defaultValue );

	//Some helper methods
	static bool toBool( int value );

protected:
	VoxoxSQLiteCursor*	mCursor;
};

