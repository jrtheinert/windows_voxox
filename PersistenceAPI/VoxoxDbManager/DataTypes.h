/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

//-----------------------------------------------------------------------------
// This file will contain all the data type definitions used in VoxoxDbManager.
//
// Most/All of these classes will have C# counterparts in the main app.
//	So we will have to match the C# / C++ classes during data merging.
//
// As such, there will be almost no logic in these classes.  Just set/get methods.
//
// Keeping all the definitions in a single class makes C# intergration easier,
//	but should not be considered an overriding concern.
//
// The logic in existing -Android- classes should be implemented in the C# classes, 
//	not here.  Except where it will improve readability of VoxoxDbManager code.
//
// Let's try to keep the classes in alphabetical order.  In some instances we cannot because
//	we use class types that need to be defined.
//		Example: DeleteMessageData uses Message::Type, so Message class must be defined first.
//		TODO: Consider moving all the enums into a single defined class.
//
// Many of these classes will also have 'collection' versions, typically a list.
//	I hope that in most cases, std::list<class> will suffice.
//	These may just become typedefs.
//
// Each class should have:
//	- setters/getters
//	- operator=
//	- copy ctor (maybe)
//
//-----------------------------------------------------------------------------

#pragma once

#include "StringList.h"
#include "DllExport.h"

#include <string>
#include <list>

#include "rapidjson/document.h"		//For JSON parsing.

class DbChangeData;
class RichData;

typedef void (*DbChangeCallbackFunc)(const DbChangeData&);

//=============================================================================
// Contact class
//	- Used to populate CONTACT LIST tab
//=============================================================================

class Contact
{
public:
	DLLEXPORT enum Type
	{
		TYPE_UNKNOWN	  = 0,
		TYPE_PHONE_NUMBER = 1,
		TYPE_GROUP		  = 2
	};

	DLLEXPORT enum CmGroup
	{
		INVALID_CMGROUP = -1
	};

	DLLEXPORT Contact();
	DLLEXPORT ~Contact();

	Contact& operator=( const Contact& src );

	//Sets ---------------------------------------------------------------------
	DLLEXPORT void setRecordId			( int                value )		{ mRecordId			 = value;	}
	DLLEXPORT void setCmGroup			( int                value )		{ mCmGroup			 = value;	}

	DLLEXPORT void setContactKey		( const std::string& value )		{ mContactKey		 = value;	}	//Contact-Sync
	DLLEXPORT void setSource			( const std::string& value )		{ mSource			 = value;	}
	DLLEXPORT void setName				( const std::string& value )		{ mName				 = value;	}
	DLLEXPORT void setCmGroupName		( const std::string& value )		{ mCmGroupName		 = value;	}
	DLLEXPORT void setDisplayName		( const std::string& value )		{ mDisplayName		 = value;	}
	DLLEXPORT void setFirstName			( const std::string& value )		{ mFirstName		 = value;	}	//Contact-Sync
	DLLEXPORT void setLastName			( const std::string& value )		{ mLastName			 = value;	}	//Contact-Sync
	DLLEXPORT void setCompany			( const std::string& value )		{ mCompany			 = value;	}	//Contact-Sync

	DLLEXPORT void setPhoneNumber		( const std::string& value )		{ mPhoneNumber		 = value;	}
	DLLEXPORT void setDisplayNumber		( const std::string& value )		{ mDisplayNumber	 = value;	}
	DLLEXPORT void setPhoneLabel		( const std::string& value )		{ mPhoneLabel		 = value;	}
	DLLEXPORT void setExtension			( const std::string& value )		{ mExtension		 = value;	}
	DLLEXPORT void setPreferredNumber	( const std::string& value )		{ mPreferredNumber	 = value;	}

	DLLEXPORT void setVoxoxCount		( int                value )		{ mVoxoxCount		 = value;	}
	DLLEXPORT void setCmGroupVoxoxCount	( int                value )		{ mCmGroupVoxoxCount = value;	}
	DLLEXPORT void setBlockedCount		( int                value )		{ mBlockedCount		 = value;   }
	DLLEXPORT void setFavoriteCount		( int                value )		{ mFavoriteCount	 = value;   }
	DLLEXPORT void setPhoneNumberCount	( int	             value )		{ mPhoneNumberCount	 = value;   }
	DLLEXPORT void setExtensionCount	( int	             value )		{ mExtensionCount	 = value;   }

	DLLEXPORT void setGroupId			( const std::string& value )		{ mGroupId			 = value;	}
	DLLEXPORT void setType				( int                value )		{ mType				 = value;	}

	//Gets ---------------------------------------------------------------------
	DLLEXPORT int			getRecordId()			const					{ return mRecordId;			}
	DLLEXPORT int			getCmGroup()			const					{ return mCmGroup;			}

	DLLEXPORT std::string	getContactKey()			const					{ return mContactKey;		}	//Contact-Sync
	DLLEXPORT std::string	getSource()				const					{ return mSource;			}
	DLLEXPORT std::string	getName()				const					{ return mName;				}
	DLLEXPORT std::string	getCmGroupName()		const					{ return mCmGroupName;		}
	DLLEXPORT std::string	getDisplayName()		const					{ return mDisplayName;		}
	DLLEXPORT std::string	getFirstName()			const					{ return mFirstName;		}	//Contact-Sync
	DLLEXPORT std::string	getLastName()			const					{ return mLastName;			}	//Contact-Sync
	DLLEXPORT std::string	getCompany()			const					{ return mCompany;			}	//Contact-Sync

	DLLEXPORT std::string	getPhoneNumber()		const					{ return mPhoneNumber;		}
	DLLEXPORT std::string	getDisplayNumber()		const					{ return mDisplayNumber;	}
	DLLEXPORT std::string	getPhoneLabel()			const					{ return mPhoneLabel;		}
	DLLEXPORT std::string	getExtension()			const					{ return mExtension;		}
	DLLEXPORT std::string	getPreferredNumber()	const					{ return mPreferredNumber;	}

	DLLEXPORT int			getVoxoxCount()			const					{ return mVoxoxCount;		}
	DLLEXPORT int			getCmGroupVoxoxCount()	const					{ return mCmGroupVoxoxCount;}
	DLLEXPORT int			getBlockedCount()		const					{ return mBlockedCount;		}
	DLLEXPORT int			getFavoriteCount()		const					{ return mFavoriteCount;	}
	DLLEXPORT int			getPhoneNumberCount()	const					{ return mPhoneNumberCount;	}
	DLLEXPORT int			getExtensionCount()		const					{ return mExtensionCount;	}

	DLLEXPORT std::string	getGroupId()			const					{ return mGroupId;			}
	DLLEXPORT int			getType()				const					{ return mType;				}

private:
	//From PN table.
	int			mRecordId;
	int			mCmGroup;
	std::string mSource;
	std::string	mPhoneNumber;		//Normalized version of Phone number
	std::string	mDisplayNumber;		//Phone number as it was in user's AB
	std::string	mPhoneLabel;
	std::string	mExtension;
	std::string	mPreferredNumber;

	std::string	mName;
	std::string	mCmGroupName;
	std::string	mDisplayName;
	int			mVoxoxCount;		//Count of Voxox numbers this contact has (obtained via a subquery)
	int			mCmGroupVoxoxCount;	//Count of Voxox numbers this CMGroup has (obtained via a subquery)

	//From GMG table
	std::string	mGroupId;

	//Misc defined field name
	int			mType;		//PhoneNumber or Group.  Needed in merged contact List. 1 = pn, 2 = GM group.

	//New Contact-Sync
	std::string	mContactKey;
	std::string	mFirstName;
	std::string	mLastName;
	std::string	mCompany;

	int			mBlockedCount;
	int			mFavoriteCount;
	int			mPhoneNumberCount;
	int			mExtensionCount;

};	//class Contact

//=============================================================================

class ContactList : public std::list<Contact>
{
public:
	ContactList()		{}
	~ContactList()		{}
};	//class ContactList

//=============================================================================



//=============================================================================
// ContactImport class
//	- Used to import contact and phone numbers into DB
//	- This is expected to be a one-way data transger INTO DB.
//=============================================================================

class ContactImport
{
public:
	DLLEXPORT ContactImport();
	DLLEXPORT ContactImport( const std::string& contactKey, const std::string& name, const std::string& phoneNumber, const std::string& phoneLabel );
	DLLEXPORT ~ContactImport();

	DLLEXPORT ContactImport& operator=( const ContactImport& src );

	//Sets ---------------------------------------------------------------------
	DLLEXPORT void setName	     ( const std::string&	value )				{ mName		   = value;	}
	DLLEXPORT void setPhoneNumber( const std::string&	value )				{ mPhoneNumber = value;	}
	DLLEXPORT void setPhoneLabel ( const std::string&	value )				{ mPhoneLabel  = value;	}
	DLLEXPORT void setJid		 ( const std::string&	value )				{ mJid		   = value;	}
	DLLEXPORT void setIsFavorite ( int					value )				{ mIsFavorite  = value; }

	//Added with new Contact Syncing
	DLLEXPORT void setContactKey ( const std::string&	value )				{ mContactKey  = value;	}
	DLLEXPORT void setSource	 ( const std::string&	value )				{ mSource	   = value;	}
	DLLEXPORT void setFirstName	 ( const std::string&	value )				{ mFirstName   = value;	}
	DLLEXPORT void setLastName	 ( const std::string&	value )				{ mLastName    = value;	}
	DLLEXPORT void setCompany	 ( const std::string&	value )				{ mCompany     = value;	}
	DLLEXPORT void setUserId     ( int					value )				{ mUserId	   = value;	}
	DLLEXPORT void setUserName   ( const std::string&	value )				{ mUserName	   = value;	}
	DLLEXPORT void setIsBlocked  ( int					value )				{ mIsBlocked   = value;	}
	DLLEXPORT void setIsExtension( int					value )				{ mIsExtension = value;	}

	//Gets ---------------------------------------------------------------------
	DLLEXPORT std::string	getName()			const						{ return mName;			}
	DLLEXPORT std::string	getPhoneNumber()	const						{ return mPhoneNumber;	}
	DLLEXPORT std::string	getPhoneLabel()		const						{ return mPhoneLabel;	}
	DLLEXPORT std::string	getJid()			const						{ return mJid;			}
	DLLEXPORT int			getIsFavorite()		const						{ return mIsFavorite;   }

	//Added with new Contact Syncing
	DLLEXPORT std::string	getContactKey()		const						{ return mContactKey;	}
	DLLEXPORT std::string	getSource()			const						{ return mSource;		}
	DLLEXPORT std::string	getFirstName()		const						{ return mFirstName;	}
	DLLEXPORT std::string	getLastName()		const						{ return mLastName;		}
	DLLEXPORT std::string	getCompany()		const						{ return mCompany;		}
	DLLEXPORT int			getUserId()			const						{ return mUserId;		}
	DLLEXPORT std::string	getUserName()		const						{ return mUserName;		}
	DLLEXPORT int			getIsBlocked()		const						{ return mIsBlocked;	}
	DLLEXPORT int			getIsExtension()	const						{ return mIsExtension;	}

private:
	std::string	mName;
	std::string	mPhoneNumber;
	std::string	mPhoneLabel;		//Work, mobile, etc.  No standard values
	std::string mJid;
	int			mIsFavorite;

	//Added with new Contact Syncing
	std::string	mContactKey;
	std::string	mSource;
	std::string	mFirstName;
	std::string	mLastName;
	std::string mCompany;
	int			mUserId;
	std::string	mUserName;
	int			mIsBlocked;
	int			mIsExtension;
};	//class ContactImport

//=============================================================================

class ContactImportList : public std::list<ContactImport>
{
public:
	ContactImportList()			{}
	~ContactImportList()		{}
};	//class ContactImportList

//=============================================================================


//=============================================================================
// GroupMessageGroup class
//=============================================================================

class GroupMessageGroup 
{
public:
	//NOTE: Do not re-order this because it will affect DB message entries.  Feel free to APPEND new values
	DLLEXPORT enum Status 
	{	
		INACTIVE = 0, 			// User has been removed from group, but still has message thread.
		ACTIVE	 = 1, 			// Normal state
	};

	GroupMessageGroup();
	~GroupMessageGroup();

	GroupMessageGroup& operator=( const GroupMessageGroup& src );

	//Sets ---------------------------------------------------------------------
	void setRecordId		( int				 value )			{ mRecordId			= value;	}
	void setStatus			( Status			 value )			{ mStatus			= value;	}
	void setGroupId			( const std::string& value )			{ mGroupId			= value;	}
	void setName			( const std::string& value )			{ mName				= value;	}
	void setInviteType		( int				 value )			{ mInviteType		= value;	}
	void setAnnounceMembers	( bool				 value )			{ mAnnounceMembers	= value;	}
	void setConfirmationCode( const std::string& value )			{ mConfirmationCode	= value;	}
	void setAdminUserId		( int				 value )			{ mAdminUserId		= value;	}
//	void setAvatar			( const std::string& value )			{ mAvatar			= value;	}	//Not currently used.

	//Gets ---------------------------------------------------------------------
	int			getRecordId()			const						{ return mRecordId;				}
	Status		getStatus()				const						{ return mStatus;			}
	std::string	getGroupId()			const						{ return mGroupId;			}
	std::string getName()				const						{ return mName;				}
	int			getInviteType()			const						{ return mInviteType;		}
	bool		getAnnounceMembers()	const						{ return mAnnounceMembers;	}
	std::string getConfirmationCode()	const						{ return mConfirmationCode;	}
	int			getAdminUserId()		const						{ return mAdminUserId;		}
//	std::string getAvatar()				const						{ return mAvatar;			}	//Not currently used.

private:
	int			mRecordId;					//DB _id field.
	Status		mStatus;					//Group may be 'inactive' if user has been removed from group.
//	int			mStatus;					//Group may be 'inactive' if user has been removed from group.
	std::string	mGroupId;
	std::string mName;
	int			mInviteType;				//Should have ENUM for this.
	bool		mAnnounceMembers;
	std::string mConfirmationCode;
	int			mAdminUserId;
//	std::string mAvatar;					//Base-64 encoded string	//NOT CURRENTLY USED
};	//class GroupMessageGroup 

//=============================================================================

class GroupMessageGroupList : public std::list<GroupMessageGroup>
{
public:
	GroupMessageGroupList()			{}
	~GroupMessageGroupList()		{}
};	//class GroupMessageGroupList

//=============================================================================



//=============================================================================
// GroupMessageMember class
//=============================================================================

class GroupMessageMember 
{
public:
	GroupMessageMember();
	~GroupMessageMember();

	GroupMessageMember& operator=( const GroupMessageMember& src );

	//Sets ---------------------------------------------------------------------
	void setMemberId	( int				 value )			{ mMemberId		= value;	}
	void setUserId		( int				 value )			{ mUserId		= value;	}
	void setReferrerId	( int				 value )			{ mReferrerId	= value;	}
	void setGroupId		( const std::string& value )			{ mGroupId		= value;	}
	void setTypeId		( const std::string& value )			{ mTypeId		= value;	}
	void setNickName	( const std::string& value )			{ mNickname		= value;	}
	void setIsAdmin		( bool				 value )			{ mIsAdmin		= value;	}
	void setIsInvited	( bool				 value )			{ mIsInvited	= value;	}
	void setHasConfirmed( bool				 value )			{ mHasConfirmed = value;	}
	void setHasOptedOut ( bool				 value )			{ mOptOut		= value;	}
	void setShouldNotify( bool				 value )			{ mNotify		= value;	}


	//Gets ---------------------------------------------------------------------
	int			getMemberId()		const						{ return mMemberId;		}
	int			getUserId()			const						{ return mUserId;		}
	int			getReferrerId()		const						{ return mReferrerId;	}
	std::string getGroupId()		const						{ return mGroupId;		}
	std::string getTypeId()			const						{ return mTypeId;		}
	std::string getNickName()		const						{ return mNickname;		}
	bool		isAdmin()			const						{ return mIsAdmin;		}
	bool		isInvited()			const						{ return mIsInvited;	}
	bool		hasConfirmed()		const						{ return mHasConfirmed;	}
	bool		hasOptedOut()		const						{ return mOptOut;		}
	bool		shouldNotify()		const						{ return mNotify;		}


private:
	int			mMemberId;
	int			mUserId;
	int			mReferrerId;
	std::string mGroupId;
	std::string mTypeId;		//1=SMS, 2=IM	//TODO-GM: Why is this string in API?
	std::string mNickname;
	bool		mIsAdmin;
	bool		mIsInvited;
	bool		mHasConfirmed;
	bool		mOptOut;		//TODO: Not sure if this is HAS or CAN OptOut
	bool		mNotify;		//TODO: Not sure if this is SHOULD or HAS notified
};	//class GroupMessageMember

//=============================================================================

class GroupMessageMemberList : public std::list<GroupMessageMember>
{
public:
	GroupMessageMemberList()		{}
	~GroupMessageMemberList()		{}
};

//=============================================================================



//=============================================================================
// GroupMessageMemberOf class
//	- Info for Contacts that are part of a given GM group.
//=============================================================================

class GroupMessageMemberOf
{
public:
	GroupMessageMemberOf();
	~GroupMessageMemberOf();

	GroupMessageMemberOf& operator=( const GroupMessageMemberOf& src );

	//Sets ---------------------------------------------------------------------
	void setRecordId   ( int				value )				{ mRecordId		= value;	}
	void setGroupId    ( const std::string&	value )				{ mGroupId		= value;	}
	void setGroupName  ( const std::string& value )				{ mGroupName	= value;	}

	void setUserId     ( int				value )				{ mUserId		= value;	}
	void setIsAdmin    ( bool				value )				{ mIsAdmin		= value;	}
	void setNickName   ( const std::string& value )				{ mNickname		= value;	}

	void setDisplayName( const std::string& value )				{ mDisplayName	= value;	}


	//Gets ---------------------------------------------------------------------
	int			getRecordId()		const						{ return mRecordId;		}
	std::string getGroupId()		const						{ return mGroupId;		}
	std::string getGroupName()		const						{ return mGroupName;	}

	int			getUserId()			const						{ return mUserId;		}
	bool		isAdmin()			const						{ return mIsAdmin;		}
	std::string getNickName()		const						{ return mNickname;		}

	std::string getDisplayName()	const						{ return mDisplayName;	}

private:
	//GM Group
	int			mRecordId;
	std::string mGroupId;
	std::string mGroupName;

	//GM Member
	int			mUserId;
	bool		mIsAdmin;
	std::string mNickname;

	//PN/Contact
	std::string	mDisplayName;
};	//class GroupMessageMemberOf

//=============================================================================

class GroupMessageMemberOfList : public std::list<GroupMessageMemberOf>
{
public:
	GroupMessageMemberOfList()		{}
	~GroupMessageMemberOfList()		{}
};

//=============================================================================



//=============================================================================
// Message class
//=============================================================================

class Message
{
public:
	//NOTE: Do not re-order this because it will affect DB message entries.  Feel free to APPEND new values
	DLLEXPORT enum Direction	
	{
		INBOUND  = 0,
		OUTBOUND = 1,
		ALL_DIR	 = 2,			//Had to change from ALL since we get compile 'redefinition' errors							
	};

	//NOTE: Do not re-order this because it will affect DB message entries.  Feel free to APPEND new values
	DLLEXPORT enum Type
	{
		ALL_TYPES	  = 0,		//Had to change from ALL since we get compile 'redefinition' errors		
		SMS			  = 1, 
		CHAT		  = 2, 
		VOICEMAIL	  = 3, 
		FAX			  = 4, 
		RECORDCALLS	  = 5, 
		UNKNOWN_TYPE  = 6,		//Had to change from ALL since we get compile 'redefinition' errors		 
		GROUP_MESSAGE = 7,		
	};

	//NOTE: Do not re-order this because it will affect DB message entries.  Feel free to APPEND new values
	DLLEXPORT enum Status 
	{										
		NEW				=  0, 	// construct a new message (in-bound or out-bound)
		READ			=  1, 	// in-bound message already read or outbound XMPP message has been read by receiver (person)
		PENDING			=  2, 	// message is waiting to be sent
		PENDING_WIFI	=  3, 	// message is waiting be sent only on a WIFI
		INTRANSITION	=  4, 	// sending ..., message is in a transition phase
		SENT			=  5, 	// message sent successfully
		FAILED			=  6, 	// failed to send message (network failure?)
		RETRY			=  7, 	// user asks to re-send message that was failed to deliver
		UPLOADING		=  8,	// for RichData media
		DELETED			=  9,	// deleted locally, but not yet deleted on server
		DELIVERED		= 10, 	// message successfully received at remote end
		TRANSLATING		= 11,	// async API call should be active to retrieve translated value
		SCHEDULED		= 12,
		ALL_STATUS		= 13,	//Had to change from ALL since we get compile 'redefinition' errors		
		UNKNOWN_STATUS	= 14	//Catch-all in case we have invalid data in DB.	//Had to change from UNKNOWN since we get compile 'redefinition' errors		
	};

	DLLEXPORT Message();
	DLLEXPORT ~Message();

	DLLEXPORT Message& operator=( const Message& src );

	DLLEXPORT static std::string typeToServerType( Message::Type type );

	DLLEXPORT std::string	getUpdatedBody( const RichData& richData ) const;
	DLLEXPORT RichData		getRichData() const;		//This parses BODY on each call.


	//Sets ---------------------------------------------------------------------
	DLLEXPORT void setRecordId	 ( int				  value )				{ mRecordId			= value;	}
	DLLEXPORT void setThreadId   ( int				  value )				{ mThreadId			= value;	}
	DLLEXPORT void setType		 ( Type				  value )				{ mType				= value;	}
	DLLEXPORT void setDirection	 ( Direction		  value )				{ mDirection		= value;	}
	DLLEXPORT void setStatus	 ( Status             value )				{ mStatus			= value;	}
	DLLEXPORT void setMsgId		 ( const std::string& value )				{ mMsgId			= value;	}
	DLLEXPORT void setBody		 ( const std::string& value )				{ mBody				= value;	}

	DLLEXPORT void setTranslateBody( const std::string& value )				{ mTranslateBody	= value;	}
	DLLEXPORT void setInboundLang  ( const std::string& value )				{ mInboundLang		= value;	}
	DLLEXPORT void setOutboundLang ( const std::string& value )				{ mOutboundLang		= value;	}

	DLLEXPORT void setLocalTimestamp	( __int64		 value )			{ mLocalTimestamp	= value;	}
	DLLEXPORT void setServerTimestamp	( __int64		 value )			{ mServerTimestamp	= value;	}
	DLLEXPORT void setReadTimestamp		( __int64		 value )			{ mReadTimestamp	= value;	}
	DLLEXPORT void setDeliveredTimestamp( __int64		 value )			{ mDeliveredTimestamp = value;	}

	DLLEXPORT void setContactDid	( const std::string& value )			{ mContactDid			= value;	}

	DLLEXPORT void setFromDid		( const std::string& value )			{ mFromDid			= value;	}
	DLLEXPORT void setToDid			( const std::string& value )			{ mToDid			= value;	}

	DLLEXPORT void setFromJid		( const std::string& value )			{ mFromJid			= value;	}
	DLLEXPORT void setToJid			( const std::string& value )			{ mToJid			= value;	}

	DLLEXPORT void setToGroupId		( const std::string& value )			{ mToGroupId		= value;	}
	DLLEXPORT void setFromUserId	( int				 value )			{ mFromUserId		= value;	}

	DLLEXPORT void setFileLocal	  ( const std::string& value )				{ mFileLocal		= value;	}
	DLLEXPORT void setThumbnailLocal( const std::string& value )				{ mThumbnailLocal	= value;	}
	DLLEXPORT void setDuration	  ( int				   value )				{ mDuration			= value;	}

	DLLEXPORT void setModified	( const std::string& value )				{ mModified			= value;	}
	DLLEXPORT void setIAccount	( const std::string& value )				{ mIAccount			= value;	}
	DLLEXPORT void setCName		( const std::string& value )				{ mCName			= value;	}

//	DLLEXPORT void setContactId      ( long            value )				{ mContactId		= value;	}
//	DLLEXPORT void setInboundLogId   ( int             value )				{ mInboundLogId		= value;	}
//	DLLEXPORT void setRichData       ( const RichData& value )				{ mRichData			= value;	}

	//Gets ---------------------------------------------------------------------
	DLLEXPORT int			getRecordId()			const						{ return mRecordId;			}
	DLLEXPORT int			getThreadId()			const						{ return mThreadId;			}
	DLLEXPORT Type			getType()				const						{ return mType;				}
	DLLEXPORT Direction		getDirection()			const						{ return mDirection;		}
	DLLEXPORT Status		getStatus()				const						{ return mStatus;			}
	DLLEXPORT std::string	getMsgId()				const						{ return mMsgId;			}
	DLLEXPORT std::string	getBody()				const						{ return mBody;				}

	DLLEXPORT std::string	getTranslateBody()		const						{ return mTranslateBody;	}
	DLLEXPORT std::string	getInboundLang()		const						{ return mInboundLang;		}
	DLLEXPORT std::string	getOutboundLang()		const						{ return mOutboundLang;		}

	DLLEXPORT __int64		getLocalTimestamp()		const						{ return mLocalTimestamp;	}
	DLLEXPORT __int64		getServerTimestamp()	const						{ return mServerTimestamp;	}
	DLLEXPORT __int64		getReadTimestamp()		const						{ return mReadTimestamp;	}
	DLLEXPORT __int64		getDeliveredTimestamp()	const						{ return mDeliveredTimestamp;	}

	DLLEXPORT std::string	getContactDid()			const						{ return mContactDid;		}

	DLLEXPORT std::string	getFromDid()			const						{ return mFromDid;			}
	DLLEXPORT std::string	getToDid()				const						{ return mToDid;			}

	DLLEXPORT std::string	getFromJid()			const						{ return mFromJid;			}
	DLLEXPORT std::string	getToJid()				const						{ return mToJid;			}

	DLLEXPORT std::string	getToGroupId()			const						{ return mToGroupId;		}
	DLLEXPORT int			getFromUserId()			const						{ return mFromUserId;		}

	DLLEXPORT std::string	getFileLocal()			const						{ return mFileLocal;		}
	DLLEXPORT std::string	getThumbnailLocal()		const						{ return mThumbnailLocal;	}
	DLLEXPORT int			getDuration()			const						{ return mDuration;			}

	DLLEXPORT std::string	getModified()			const						{ return mModified;			}
	DLLEXPORT std::string	getIAccount()			const						{ return mIAccount;			}
	DLLEXPORT std::string	getCName()				const						{ return mCName;			}


//	DLLEXPORT long		getContactId()				const						{ return mContactId;		}
//	DLLEXPORT int		getInboundLogId()			const						{ return mInboundLogId;		}
//	DLLEXPORT RichData	getRichData()				const						{ return mRichData;			}

	//Convenience methods
	DLLEXPORT bool isInbound()						const						{ return (mDirection == Message::INBOUND  );	}
	DLLEXPORT bool isOutbound()						const						{ return (mDirection == Message::OUTBOUND );	}

	DLLEXPORT bool isChat()							const						{ return (mType      == Message::CHAT);			}
	DLLEXPORT bool isSms()							const						{ return (mType      == Message::SMS);			}
	DLLEXPORT bool isFax()							const						{ return (mType      == Message::FAX);			}
	DLLEXPORT bool isGroupMessage()					const						{ return (mType      == Message::GROUP_MESSAGE);}
	DLLEXPORT bool isVoicemail()					const						{ return (mType      == Message::VOICEMAIL	  );}
	DLLEXPORT bool isVoicemailEx()					const;

	DLLEXPORT std::string	getOriginatingDid()		const						{ return (isInbound() ? getFromDid() : getToDid());	}

private:
	rapidjson::Document*	toJsonObject( const RichData& richData )	const;		//Caller must delete this pointer.
//	std::string				typeToJsonKey() const;
	std::string				toJsonString( const RichData& richData )	const;

	static RichData			fromJsonText( const std::string& input );

private:
	//Base message
	int			mRecordId;
	std::string	mMsgId;
	int			mThreadId;	//Not currently used, but in DB
	Type		mType;
	Direction	mDirection;
	Status		mStatus;
	std::string	mBody;

	//Translation
	std::string	mTranslateBody;
	std::string	mInboundLang;
	std::string	mOutboundLang;

	//Timestamps
	__int64		mLocalTimestamp;
	__int64		mServerTimestamp;
	__int64		mReadTimestamp;
	__int64		mDeliveredTimestamp;

	//SMS and Chat (not sure)
	std::string	mContactDid;

	//SMS
	std::string	mFromDid;
	std::string	mToDid;

	//Chat
	std::string	mFromJid;
	std::string	mToJid;

	//GroupMessaging
	std::string	mToGroupId;
	int			mFromUserId;

	//RichData
	std::string	mFileLocal;
	std::string	mThumbnailLocal;
	int			mDuration;

	//VM related?
	std::string	mModified;
	std::string	mIAccount;
	std::string	mCName;

	//What are these? Derived data elements?  Commenting for now
//	long		mContactId;			//TODO: Do we use this?
//	int			mInboundLogId;		//What is this?

//	RichData	mRichData;			//This is populated when Body is parsed and contains RichData JSON code.

	//JSON keys/values
	static const std::string s_GmJsonKeyGroupMsg;

	static const std::string s_GmJsonKeyBody;
	static const std::string s_GmJsonKeyFrom;
	static const std::string s_GmJsonKeyGroupId;
	static const std::string s_GmJsonKeyMessageId;
	static const std::string s_GmJsonKeyMsgType;
	static const std::string s_GmJsonKeyNickname;
	static const std::string s_GmJsonKeyTimestamp;
	static const std::string s_GmJsonKeyUserId;

	static const std::string s_GmJsonMsgType_GroupDissolved;	//No need for GROUP_CREATE, since only Admin creating it cares.
	static const std::string s_GmJsonMsgType_MemberAdded;
	static const std::string s_GmJsonMsgType_MemberRemoved;
	static const std::string s_GmJsonMsgType_MemberLeft;
	static const std::string s_GmJsonMsgType_Msg;
};	//class Message

//=============================================================================

class MessageList : public std::list<Message>
{
public:
	MessageList()		{}
	~MessageList()		{}
};

//=============================================================================


//=============================================================================
// MessageTranslationProperties class
//=============================================================================

class MessageTranslationProperties
{
public:
//	public static final String MESSAGE_TRANSLATION_ON = "ON";
//	public static final String MESSAGE_TRANSLATION_OFF = "OFF";

	DLLEXPORT MessageTranslationProperties();
	DLLEXPORT ~MessageTranslationProperties();
	
	MessageTranslationProperties& operator=( const MessageTranslationProperties& src );

	//Sets ---------------------------------------------------------------------
	DLLEXPORT void setRecordId	     ( int				  value )				{ mRecordId			= value;	}
	DLLEXPORT void setContactId	     ( int				  value )				{ mContactId		= value;	}
	DLLEXPORT void setEnabled		 ( bool			      value )				{ mEnabled			= value;	}
	DLLEXPORT void setPhoneNumber	 ( const std::string& value )				{ mPhoneNumber		= value;	}
	DLLEXPORT void setJid			 ( const std::string& value )				{ mJid				= value;	}
	DLLEXPORT void setUserLanguage   ( const std::string& value )				{ mUserLanguage		= value;	}
	DLLEXPORT void setContactLanguage( const std::string& value )				{ mContactLanguage	= value;	}
	DLLEXPORT void setDirection	     ( Message::Direction value )				{ mDirection		= value;	}

	//Gets ---------------------------------------------------------------------
	DLLEXPORT int					getRecordId()			const				{ return mRecordId;			}
	DLLEXPORT int					getContactId()			const				{ return mContactId;		}
	DLLEXPORT bool					isEnabled()				const				{ return mEnabled;			}
	DLLEXPORT std::string			getPhoneNumber()		const				{ return mPhoneNumber;		}
	DLLEXPORT std::string			getJid()				const				{ return mJid;				}
	DLLEXPORT std::string			getUserLanguage()		const				{ return mUserLanguage;		}
	DLLEXPORT std::string			getContactLanguage()	const				{ return mContactLanguage;	}
	DLLEXPORT Message::Direction	getDirection()			const				{ return mDirection;		}

private:
	int					mRecordId;
    int					mContactId;
    bool				mEnabled;
	std::string			mPhoneNumber;
	std::string			mJid;
	std::string			mUserLanguage;
	std::string			mContactLanguage;
	Message::Direction	mDirection;
};	//class MessageTranslationProperties

//=============================================================================

class MessageTranslationPropertiesList : public std::list<MessageTranslationProperties>		//TODO: verify this is acceptable, at least for now.
{
public:
	MessageTranslationPropertiesList()		{}
	~MessageTranslationPropertiesList()		{}
};

//=============================================================================



//=============================================================================
// PhoneNumber class
//
//	I have merged ContactDetailsPhoneNumber class with this class.
//	Other than a couple more data elements, this class now:
//	 - May contain Group info (Name, ID).  These will be stored in Name and Number
//	 - A Type member to distinguish Phone/contact vs. Group.
//=============================================================================

class PhoneNumber 
{
public:	
	DLLEXPORT enum VoxoxNumberType 
	{
		NOT_VOXOX_RELATED			= 0,	// any phone number (in the address book or not)
		DID_NUMBER					= 1,	// VoxOx assigned number 
		MOBILE_REGISTRATION_NUMBER	= 2,	// the mobile registration number used when the user signed up for VoxOx
	};

	DLLEXPORT enum ContactFilter 
	{
    	CONTACT_FILTER_ALL		= 0,
    	CONTACT_FILTER_FAVORITE = 1,
    	CONTACT_FILTER_VOXOX	= 2,
    	CONTACT_FILTER_GROUPS	= 3,
    	CONTACT_FILTER_BLOCKED	= 4,
    	CONTACT_FILTER_COMPANY	= 5,
    };

	DLLEXPORT PhoneNumber();
	DLLEXPORT ~PhoneNumber();

	DLLEXPORT PhoneNumber& operator=( const PhoneNumber& src );

	DLLEXPORT bool isValid() const;
	DLLEXPORT bool numberIsVoxox()	const		{ return getNumberType() == DID_NUMBER;					}
	DLLEXPORT bool numberIsMobReg()	const		{ return getNumberType() == MOBILE_REGISTRATION_NUMBER;	}

	//Sets ---------------------------------------------------------------------
	DLLEXPORT void setRecordId	 ( int				  value )				{ mRecordId		 = value;	}
	DLLEXPORT void setContactKey ( const std::string  value )				{ mContactKey	 = value;	}
	DLLEXPORT void setSource	 ( const std::string& value )				{ mSource		 = value;	}
	DLLEXPORT void setName		 ( const std::string& value )				{ mName			 = value;	}
	DLLEXPORT void setFirstName	 ( const std::string& value )				{ mFirstName	 = value;	}	//Contact-Sync
	DLLEXPORT void setLastName	 ( const std::string& value )				{ mLastName		 = value;	}	//Contact-Sync
	DLLEXPORT void setCompany	 ( const std::string& value )				{ mCompany		 = value;	}	//Contact-Sync
	DLLEXPORT void setDisplayName( const std::string& value )				{ mDisplayName	 = value;	}
	DLLEXPORT void setCmGroupName( const std::string& value )				{ mCmGroupName	 = value;	}
	DLLEXPORT void setContactType( Contact::Type	  value )				{ mContactType	 = value;	}

	//Phone Number related
	DLLEXPORT void setCmGroup		( int				 value )			{ mCmGroup		 = value;	}
	DLLEXPORT void setNumber		( const std::string& value )			{ mNumber		 = value;	}
	DLLEXPORT void setDisplayNumber	( const std::string& value )			{ mDisplayNumber = value;	}
	DLLEXPORT void setLabel			( const std::string& value )			{ mLabel		 = value;	}
	DLLEXPORT void setNumberType	( int				 value )			{ mNumberType	 = value;	}
	DLLEXPORT void setIsFavorite	( bool				 value )			{ mIsFavorite	 = value;	}
	DLLEXPORT void setIsBlocked		( bool				 value )			{ mIsBlocked	 = value;	}	//Contact-Sync
	DLLEXPORT void setIsExtension	( bool				 value )			{ mIsExtension	 = value;	}	//Contact-Sync

	//XMPP related
	DLLEXPORT void setUserId		( int				 value )			{ mUserId		 = value;	}	//Contact-Sync
	DLLEXPORT void setUserName		( const std::string& value )			{ mUserName		 = value;	}	//Contact-Sync	- May have same value as XmppJid
	DLLEXPORT void setXmppJid		( const std::string& value )			{ mXmppJid		 = value;	}
	DLLEXPORT void setXmppAlias		( const std::string& value )			{ mXmppAlias	 = value;	}
	DLLEXPORT void setXmppPresence	( const std::string& value )			{ mXmppPresence  = value;	}
	DLLEXPORT void setXmppStatus	( int				 value )			{ mXmppStatus	 = value;	}
	DLLEXPORT void setXmppGroup		( const std::string& value )			{ mXmppGroup	 = value;	}


	//Gets ---------------------------------------------------------------------
	DLLEXPORT int			getRecordId()		const							{ return mRecordId;		}
	DLLEXPORT std::string	getContactKey()		const							{ return mContactKey;	}	//Contact-Sync
	DLLEXPORT std::string	getSource()			const							{ return mSource;		}
	DLLEXPORT std::string	getName()			const							{ return mName;			}
	DLLEXPORT std::string	getFirstName()		const							{ return mFirstName;	}	//Contact-Sync
	DLLEXPORT std::string	getLastName()		const							{ return mLastName;		}	//Contact-Sync
	DLLEXPORT std::string	getCompany()		const							{ return mCompany;		}	//Contact-Sync
	DLLEXPORT std::string	getDisplayName()	const							{ return mDisplayName;	}
	DLLEXPORT std::string	getCmGroupName()	const							{ return mCmGroupName;	}

	DLLEXPORT Contact::Type getContactType()	const							{ return mContactType;	}

	//Phone Number related
	DLLEXPORT std::string	getNumber()			const							{ return mNumber;		}
	DLLEXPORT std::string	getDisplayNumber()	const							{ return mDisplayNumber;}
	DLLEXPORT std::string	getLabel()			const							{ return mLabel;		}
	DLLEXPORT int 			getCmGroup()		const							{ return mCmGroup;		}
	DLLEXPORT int			getNumberType()		const							{ return mNumberType;	}
	DLLEXPORT bool			isFavorite()		const							{ return mIsFavorite;	}
	DLLEXPORT bool			isBlocked()			const							{ return mIsBlocked;	}	//Contact-Sync
	DLLEXPORT bool			isExtension()		const							{ return mIsExtension;	}	//Contact-Sync
	
	//XMPP related
	DLLEXPORT int			getUserId()			const							{ return mUserId;		}	//Contact-Sync
	DLLEXPORT std::string	getUserName()		const							{ return mUserName;		}	//Contact-Sync
	DLLEXPORT std::string	getXmppJid()		const							{ return mXmppJid;		}
	DLLEXPORT std::string	getXmppAlias()		const							{ return mXmppAlias;	}
	DLLEXPORT std::string	getXmppPresence()	const							{ return mXmppPresence;	}
	DLLEXPORT int			getXmppStatus()		const							{ return mXmppStatus;	}
	DLLEXPORT std::string	getXmppGroup()		const							{ return mXmppGroup;	}


private:
	//Using -Android- data model, we only kept Contact Name and ContactKey.  The decision was made to just duplicate these in PhoneNumber table
	int			mRecordId;
	std::string	mContactKey;		//Contact-Sync
	std::string	mSource;
	std::string	mName;				//Should be from Mobile Device AB.
	std::string	mFirstName;			//Should be from Mobile Device AB.
	std::string	mLastName;			//Should be from Mobile Device AB.
	std::string	mCompany;			//Should be from Mobile Device AB.
	std::string mDisplayName;		//Composite of first/last
	std::string mCmGroupName;
	Contact::Type	mContactType;	//Group vs. Phone/Contact

	//Phone Number related
	std::string mNumber;			//Normalized (last 10 digits) of PhoneNumber used for DB access.
	std::string mDisplayNumber;		//Phone number as it is in users AB
	std::string mLabel;
	int 		mCmGroup;
	int			mNumberType;		//Voxox = 1, MobReg = 2, other = 0
	int			mVoxoxUserId;		//From Voxox lookup of PhoneNumber.
	bool		mIsFavorite;
	bool		mIsBlocked;			//Contact-Sync
	bool		mIsExtension;		//Contact-Sync
	
	//XMPP related
	int			mUserId;			//Contact-Sync
	std::string	mUserName;			//Contact-Sync
	std::string	mXmppJid;
	std::string	mXmppAlias;
	std::string mXmppPresence;
	int			mXmppStatus;
	std::string mXmppGroup;			//This should really be an array of strings, but since we are not really using it, this is OK for now.
};	//class PhoneNumber 

//=============================================================================

class PhoneNumberList : public std::list<PhoneNumber>
{
public:
	PhoneNumberList()		{}
	~PhoneNumberList()		{}
};

//=============================================================================


//=============================================================================
// RecentCall class
//	- Duration will be zero until call is answered AND hung up.
//	  This will allow us to separate ACTIVE calls from COMPLETED calls.
//=============================================================================

class RecentCall 
{
public:
	DLLEXPORT enum Status 
	{
		STATUS_NONE		= 0,	//Invalid status
		STATUS_NEW		= 1,	//Inbound/Outbound
		STATUS_ANSWERED = 2,	//Inbound/Outbound
		STATUS_DECLINED = 3,	//Inbound/Outbound
		STATUS_MISSED	= 4,	//Inbound
	};

	DLLEXPORT enum Seen
	{
		NOT_SEEN = 0,		//These match the server values.
		SEEN	 = 1,
	};

	DLLEXPORT RecentCall();
	DLLEXPORT ~RecentCall();

	DLLEXPORT RecentCall& operator=( const RecentCall& src );

	DLLEXPORT bool isValid() const;

	DLLEXPORT bool isNew()		const										{ return getStatus() == STATUS_NEW;			}
	DLLEXPORT bool isAnswered()	const										{ return getStatus() == STATUS_ANSWERED;	}
	DLLEXPORT bool isDeclined()	const										{ return getStatus() == STATUS_DECLINED;	}
	DLLEXPORT bool isMissed()	const										{ return getStatus() == STATUS_MISSED;		}
	DLLEXPORT bool isCompleted() const										{ return isAnswered() && getDuration() > 0;	}

	DLLEXPORT bool hasBeenSeen() const										{ return getSeen() == SEEN;	}

	//Sets --------------------------------------------------------------------
	DLLEXPORT void setRecordId		( int					value )			{ mRecordId			= value;	}
	DLLEXPORT void setUid			( const std::string&	value )			{ mUid				= value;	}
	DLLEXPORT void setStatus		( Status				value )			{ mStatus			= value;	}
	DLLEXPORT void setIsIncoming	( bool					value )			{ mIsIncoming		= value;	}
	DLLEXPORT void setTimestamp		( __int64				value )			{ mTimestamp		= value;	}
	DLLEXPORT void setStartTimestamp( __int64				value )			{ mStartTimestamp	= value;	}
	DLLEXPORT void setDuration		( int					value )			{ mDuration			= value;	}
	DLLEXPORT void setIsLocal       ( bool					value )			{ mIsLocal          = value;    }
	DLLEXPORT void setSeen			( Seen					value )			{ mSeen				= value;	}

	DLLEXPORT void setNumber		( const std::string&	value )			{ mNumber			= value;	}
	DLLEXPORT void setOrigNumber	( const std::string&	value )			{ mOrigNumber		= value;	}
	DLLEXPORT void setDisplayNumber ( const std::string&	value )			{ mDisplayNumber	= value;	}	
	DLLEXPORT void setName			( const std::string&	value )			{ mName				= value;	}	
	DLLEXPORT void setContactKey	( const std::string&	value )			{ mContactKey		= value;	}	
	DLLEXPORT void setCmGroup		( int					value )			{ mCmGroup			= value;	}	
	DLLEXPORT void setCmGroupName	( const std::string&	value )			{ mCmGroupName		= value;	}	

	//Gets --------------------------------------------------------------------
	DLLEXPORT int			getRecordId		 ()	const						{ return mRecordId;			}
	DLLEXPORT std::string	getUid			 ()	const						{ return mUid;				}
	DLLEXPORT Status		getStatus		 ()	const						{ return mStatus;			}
	DLLEXPORT bool			isIncoming		 ()	const						{ return mIsIncoming;		}
	DLLEXPORT __int64		getTimestamp	 ()	const						{ return mTimestamp;		}
	DLLEXPORT __int64		getStartTimestamp()	const						{ return mStartTimestamp;	}
	DLLEXPORT int			getDuration		 ()	const						{ return mDuration;			}
	DLLEXPORT bool			getIsLocal       ()	const						{ return mIsLocal;          }
	DLLEXPORT Seen			getSeen			 ()	const						{ return mSeen;				}

	DLLEXPORT std::string	getNumber		 ()	const						{ return mNumber;			}
	DLLEXPORT std::string	getOrigNumber	 ()	const						{ return mOrigNumber;		}
	DLLEXPORT std::string	getDisplayNumber ()	const						{ return mDisplayNumber;	}	
	DLLEXPORT std::string	getName			 ()	const						{ return mName;				}	
	DLLEXPORT std::string	getContactKey	 ()	const						{ return mContactKey;		}	
	DLLEXPORT int			getCmGroup		 ()	const						{ return mCmGroup;			}	
	DLLEXPORT std::string	getCmGroupName	 ()	const						{ return mCmGroupName;		}	

private:
	int			mRecordId;
	std::string	mUid;
	Status		mStatus;
	bool		mIsIncoming;
	__int64		mTimestamp;
	__int64		mStartTimestamp;
	int			mDuration;
	bool		mIsLocal;
	Seen		mSeen;

	std::string	mOrigNumber;		//Number as we get from API caller
	std::string	mNumber;			//normalized, 10-digit so we can lookup Contact name

	std::string mDisplayNumber;		//Display number from DB lookup
	std::string	mName;				//Contact Name from DB lookup
	std::string	mContactKey;		//Contact Key from DB lookup
	int			mCmGroup;			//From PN lookup
	std::string	mCmGroupName;		//Compound name from DB lookup

};	// class RecentCall

//=============================================================================

class RecentCallList : public std::list<RecentCall>
{
public:
	RecentCallList()		{}
	~RecentCallList()		{}

	StringList getUniquePhoneNumbers();
};

//=============================================================================



//=============================================================================
// RichData class
//	- This does not directly map to a DB class, so maybe we don't need it
//	- If we do, then it most likely needs to/from string logic so we can update DB.
//	  Hopefully, we can avoid this duplicate logic in C++ / C# classes.
//=============================================================================
class RichData 
{
public:
	DLLEXPORT enum Type 
	{
		NONE	 = 0, 
		IMAGE	 = 1, 
		VIDEO	 = 2,	
		LOCATION = 3, 
		CONTACT	 = 4, 
		PDF		 = 5,
	};

	DLLEXPORT RichData();
	DLLEXPORT ~RichData();

	DLLEXPORT RichData& operator=( const RichData& src );

	bool		usesMedia()		const;

	bool	isTypeNone()		const			{ return mType == Type::NONE;		}
	bool	isTypeImage()		const			{ return mType == Type::IMAGE;		}
	bool	isTypeVideo()		const			{ return mType == Type::VIDEO;		}
	bool	isTypeLocation()	const			{ return mType == Type::LOCATION;	}
	bool	isTypeContact()		const			{ return mType == Type::CONTACT;	}
	bool	isTypePdf()			const			{ return mType == Type::PDF;		}

	DLLEXPORT std::string	toJsonString()	const;							//Accessible to DbManger
	static RichData			fromJsonText( const std::string& input );		//Accessible to Message

	//Sets ---------------------------------------------------------------------
	DLLEXPORT void setType		 ( Type				 value )			{ mType			 = value;	}

	DLLEXPORT void setThumbnailUrl ( const std::string& value )			{ mThumbnailUrl  = value;	}
	DLLEXPORT void setMediaUrl	 ( const std::string& value )			{ mMediaUrl		 = value;	}
	DLLEXPORT void setBody		 ( const std::string& value )			{ mBody			 = value;	}

	DLLEXPORT void setLatitude	 ( double			 value )			{ mLatitude		 = value;	}
	DLLEXPORT void setLongitude	 ( double			 value )			{ mLongitude	 = value;	}
	DLLEXPORT void setAddress	 ( const std::string& value )			{ mAddress		 = value;	}

	DLLEXPORT void setMediaFile	 ( const std::string& value )			{ mMediaFile	 = value;	}
	DLLEXPORT void setThumbnailFile( const std::string& value )			{ mThumbnailFile = value;	}

	//Gets ---------------------------------------------------------------------
	DLLEXPORT Type			getType()			const						{ return mType;				}

	DLLEXPORT std::string	getThumbnailUrl()	const						{ return mThumbnailUrl;		}
	DLLEXPORT std::string	getMediaUrl()		const						{ return mMediaUrl;			}
	DLLEXPORT std::string	getBody()			const						{ return mBody;				}

	DLLEXPORT double		getLatitude()		const						{ return mLatitude;			}
	DLLEXPORT double		getLongitude()		const						{ return mLongitude;		}
	DLLEXPORT std::string	getAddress()		const						{ return mAddress;			}

	DLLEXPORT std::string	getMediaFile()		const						{ return mMediaFile;		}
	DLLEXPORT std::string	getThumbnailFile()	const						{ return mThumbnailFile;	}

	//Convenience methods
	DLLEXPORT bool hasMediaFile()				const						{ return !mMediaFile.empty();		}
	DLLEXPORT bool hasThumbnailFile()			const						{ return !mThumbnailFile.empty();	}

private:
	rapidjson::Document*	toJsonObject()	const;		//Caller must delete this pointer.
	std::string				typeToJsonKey() const;

	static RichData makeLocation( const std::string& body,     double latitude, double longitude );		//Body should/may contain address.
	static RichData makeContact ( const std::string& mediaUrl, const std::string& thumbnailUrl, const std::string& body );
	static RichData makeImage   ( const std::string& mediaUrl, const std::string& thumbnailUrl, const std::string& body );
	static RichData makeVideo   ( const std::string& mediaUrl, const std::string& thumbnailUrl, const std::string& body );

private:
	//Members that are -USED- as part of the JSON object to support sending & receiving rich messages
	Type		mType;

	//Media
	std::string	mThumbnailUrl;
	std::string	mMediaUrl;
	std::string	mBody;

	//Location
	double		mLatitude;
	double		mLongitude;
	std::string	mAddress;

	//Hold files properties locally, -NOT USED- as part of the JSON object
	std::string	mMediaFile;				//TODO: revert back to File type?  Need cross-platform definition.  Need File for absolutePath parsing anyway.
	std::string	mThumbnailFile;

	//JSON parsing keys
	static const std::string s_RichDataJsonKey;;
	static const std::string s_RichDataJsonKeyType;
	static const std::string s_RichDataJsonKeyData;
	static const std::string s_RichDataJsonKeyBody;

	//JSON type values
	static const std::string s_RichDataJsonTypeLocation;
	static const std::string s_RichDataJsonTypeContact;
	static const std::string s_RichDataJsonTypeJpeg;
	static const std::string s_RichDataJsonTypeMpeg;
		
	// location JSON keys
	static const std::string s_RichDataJsonKeyLongitude;
	static const std::string s_RichDataJsonKeyLatitude;

	// media sharing JSON Keys
	static const std::string s_RichDataJsonKeyFile;
	static const std::string s_RichDataJsonKeyThumbnail;

	//TODO?: JSON Contact.
};	//RichData

//=============================================================================


//=============================================================================
// UmwMessage class - TODO: Derive this from Message Class.
//	- Returns data to be used in UMW
//	- Outgoing from model layer only from a query.
//=============================================================================

class UmwMessage
{
public:
	DLLEXPORT UmwMessage();
	DLLEXPORT ~UmwMessage();

	DLLEXPORT UmwMessage& operator=( const UmwMessage& src );

	//Sets ---------------------------------------------------------------------
	DLLEXPORT void setContactKey	  ( const std::string&	value )				{ mContactKey		  = value;	}
	DLLEXPORT void setPhoneNumber	  ( const std::string&	value )				{ mPhoneNumber		  = value;	}
	DLLEXPORT void setCmGroup		  ( int					value )				{ mCmGroup			  = value;	}
	DLLEXPORT void setName			  ( const std::string&	value )				{ mName				  = value;	}
	DLLEXPORT void setCmGroupName	  ( const std::string&	value )				{ mCmGroupName		  = value;	}
	DLLEXPORT void setIsVoxox		  ( int					value )				{ mIsVoxox			  = value;	}	//Misnomer

	//From GM table
	DLLEXPORT void setGmNickName	  ( const std::string&	value )				{ mGmNickName		  = value;	}
	DLLEXPORT void setGmColor		  ( const std::string&	value )				{ mGmColor			  = value;	}
	DLLEXPORT void setGmGroupName	  ( const std::string&	value )				{ mGmGroupName		  = value;	}

	//Main message info
	DLLEXPORT void setRecordId		  ( int					value )				{ mRecordId			  = value;	}
	DLLEXPORT void setType			  ( Message::Type		value )				{ mType				  = value;	}
	DLLEXPORT void setDirection		  ( Message::Direction  value )				{ mDir				  = value;	}
	DLLEXPORT void setStatus		  ( Message::Status		value )				{ mStatus			  = value;	}
	DLLEXPORT void setBody			  ( const std::string&	value )				{ mBody				  = value;	}
	DLLEXPORT void setTranslated	  ( const std::string&	value )				{ mTranslated		  = value;	}
	DLLEXPORT void setMsgId			  ( const std::string&	value )				{ mMsgId			  = value;	}

	//Time stamps
	DLLEXPORT void setLocalTimestamp    ( __int64			value )				{ mLocalTimestamp	  = value;	}
	DLLEXPORT void setServerTimestamp   ( __int64			value )				{ mServerTimestamp	  = value;	}
	DLLEXPORT void setReadTimestamp     ( __int64			value )				{ mReadTimestamp	  = value;	}
	DLLEXPORT void setDeliveredTimestamp( __int64			value )				{ mDeliveredTimestamp = value;	}

	//Rich data
	DLLEXPORT void setFileLocal		  ( const std::string&	value )				{ mFileLocal		  = value;	}
	DLLEXPORT void setThumbnailLocal  ( const std::string&	value )				{ mThumbnailLocal	  = value;	}
	DLLEXPORT void setDuration		  ( int					value )				{ mDuration			  = value;	}

	//SMS
	DLLEXPORT void setFromDid		  ( const std::string&	value )				{ mFromDid			  = value;	}
	DLLEXPORT void setToDid			  ( const std::string&	value )				{ mToDid			  = value;	}

	//Chat
	DLLEXPORT void setFromJid		  ( const std::string&	value )				{ mFromJid			  = value;	}
	DLLEXPORT void setToJid			  ( const std::string&	value )				{ mToJid			  = value;	}

	//Group Messaging
	DLLEXPORT void setToGroupId		  ( const std::string&	value )				{ mToGroupId		  = value;	}
	DLLEXPORT void setFromUserId	  ( int					value )				{ mFromUserId		  = value;	}

	//Gets ---------------------------------------------------------------------
	DLLEXPORT std::string			getContactKey()			const					{ return mContactKey;	}
	DLLEXPORT std::string			getPhoneNumber()		const					{ return mPhoneNumber;	}
	DLLEXPORT int					getCmGroup()			const					{ return mCmGroup;		}
	DLLEXPORT std::string			getName()				const					{ return mName;			}
	DLLEXPORT std::string			getCmGroupName()		const					{ return mCmGroupName;	}
	DLLEXPORT int					getIsVoxox()			const					{ return mIsVoxox;		}	//Misnomer

	//From GM table
	DLLEXPORT std::string			getGmNickName()			const					{ return mGmNickName;	}
	DLLEXPORT std::string			getGmColor()			const					{ return mGmColor;		}
	DLLEXPORT std::string			getGmGroupName()		const					{ return mGmGroupName;	}

	//Main message info
	DLLEXPORT int					getRecordId()			const					{ return mRecordId;		}
	DLLEXPORT Message::Type			getType()				const					{ return mType;			}
	DLLEXPORT Message::Direction	getDirection()			const					{ return mDir;			}
	DLLEXPORT Message::Status		getStatus()				const					{ return mStatus;		}
	DLLEXPORT std::string			getBody()				const					{ return mBody;			}
	DLLEXPORT std::string			getTranslated()			const					{ return mTranslated;	}
	DLLEXPORT std::string			getMsgId()				const					{ return mMsgId;		}

	//Time stamps
	DLLEXPORT __int64				getLocalTimestamp()		const					{ return mLocalTimestamp;	}
	DLLEXPORT __int64				getServerTimestamp()	const					{ return mServerTimestamp;	}
	DLLEXPORT __int64				getReadTimestamp()		const					{ return mReadTimestamp;	}
	DLLEXPORT __int64				getDeliveredTimestamp()	const					{ return mDeliveredTimestamp; }

	//Rich data
	DLLEXPORT std::string			getFileLocal()			const					{ return mFileLocal;		}
	DLLEXPORT std::string			getThumbnailLocal()		const					{ return mThumbnailLocal;	}
	DLLEXPORT int					getDuration()			const					{ return mDuration;			}

	//SMS
	DLLEXPORT std::string			getFromDid()			const					{ return mFromDid;			}
	DLLEXPORT std::string			getToDid()				const					{ return mToDid;			}

	//Chat
	DLLEXPORT std::string			getFromJid()			const					{ return mFromJid;			}
	DLLEXPORT std::string			getToJid()				const					{ return mToJid;			}

	//Group Messaging
	DLLEXPORT std::string			getToGroupId()			const					{ return mToGroupId;		}
	DLLEXPORT int					getFromUserId()			const					{ return mFromUserId;		}


private:
	//From PN table
	std::string			mContactKey;
	std::string			mPhoneNumber;
	int					mCmGroup;
	std::string			mName;
	std::string			mCmGroupName;
	int					mIsVoxox;	//Misnomer

	//From GM table
	std::string			mGmNickName;
	std::string			mGmColor;
	std::string			mGmGroupName;

	//Main message info
	int					mRecordId;	//_id in Message table
	Message::Type		mType;
	Message::Direction	mDir;
	Message::Status		mStatus;
	std::string			mBody;
	std::string			mTranslated;
	std::string			mMsgId;

	//Time stamps
	__int64				mLocalTimestamp;
	__int64				mServerTimestamp;
	__int64				mReadTimestamp;
	__int64				mDeliveredTimestamp;

	//Rich data
	std::string			mFileLocal;
	std::string			mThumbnailLocal;
	int					mDuration;

	//SMS
	std::string			mFromDid;
	std::string			mToDid;

	//Chat
	std::string			mFromJid;
	std::string			mToJid;

	//Group Messaging
	std::string			mToGroupId;
	int					mFromUserId;
};	//class UmwMessage

//=============================================================================

class UmwMessageList : public std::list<UmwMessage>
{
public:
	UmwMessageList()		{}
	~UmwMessageList()		{}
};

//=============================================================================



//=============================================================================
// VoxoxContact class
//	- This class does not have direct DB table corollary
//	- This data is received from main app and used to update DB.
//=============================================================================

class VoxoxContact 
{
public:
	DLLEXPORT VoxoxContact();
	DLLEXPORT VoxoxContact( const std::string& contactKey, const std::string& regNum, const std::string& userName, const std::string& did, 
							const std::string& phoneLabel, int userId, bool isBlocked, bool isFavorite, const std::string& source );
	DLLEXPORT ~VoxoxContact();

	DLLEXPORT VoxoxContact& operator=( const VoxoxContact& src );

	//Sets ---------------------------------------------------------------------
	DLLEXPORT void setContactKey		( const std::string& value )				{ mContactKey		  = value;	}
	DLLEXPORT void setRegistrationNumber( const std::string& value )				{ mRegistrationNumber = value;	}
	DLLEXPORT void setUserName			( const std::string& value )				{ mUserName			  = value;	}
	DLLEXPORT void setDid				( const std::string& value )				{ mDid				  = value;	}
	DLLEXPORT void setPhoneLabel		( const std::string& value )				{ mPhoneLabel		  = value;	}
	DLLEXPORT void setSource			( const std::string& value )				{ mSource			  = value;	}
	DLLEXPORT void setUserId			( int				 value )				{ mUserId			  = value;	}
	DLLEXPORT void setIsBlocked			( bool				 value )				{ mIsBlocked		  = value;	}
	DLLEXPORT void setIsFavorite		( bool				 value )				{ mIsFavorite		  = value;	}

	//Gets ---------------------------------------------------------------------
	DLLEXPORT std::string getContactKey()				const						{ return mContactKey;			}
	DLLEXPORT std::string getRegistrationNumber()		const						{ return mRegistrationNumber;	}
	DLLEXPORT std::string getUserName()					const						{ return mUserName;				}
	DLLEXPORT std::string getDid()						const						{ return mDid;					}
	DLLEXPORT std::string getPhoneLabel()				const						{ return mPhoneLabel;			}
	DLLEXPORT std::string getSource()					const						{ return mSource;				}
	DLLEXPORT int		  getUserId()					const						{ return mUserId;				}
	DLLEXPORT bool		  isBlocked()					const						{ return mIsBlocked;			}
	DLLEXPORT bool		  isFavorite()					const						{ return mIsFavorite;			}

private:
	std::string mContactKey;				// May or may not be used when adding/updating DB
	std::string mRegistrationNumber;		// mobile number used by the user to activate the Voxox account 
	std::string mUserName;					// Voxox user name (also prefix of the JID) 
	std::string mDid;						// Voxox user Did
	std::string mPhoneLabel;
	std::string mSource;					// Means by which we got this contact
	int			mUserId;					// Voxox user id.
	bool		mIsBlocked;
	bool		mIsFavorite;
};	//class VoxoxContact 

//=============================================================================

class VoxoxContactList : public std::list<VoxoxContact>		//TODO: verify this is acceptable, at least for now.
{
public:
	VoxoxContactList()		{}
	~VoxoxContactList()		{}
};

//=============================================================================



//=============================================================================
// VoxoxPhoneNumber class
//	- This class does not have direct DB table corollary
//	- This data is received from main app and used to update DB.
// Currently a simple association of UseName (XMPP JID) and Voxox DID
//=============================================================================

class VoxoxPhoneNumber 
{
public:
	DLLEXPORT VoxoxPhoneNumber();
	DLLEXPORT VoxoxPhoneNumber( const std::string& did, const std::string& jid );
	DLLEXPORT ~VoxoxPhoneNumber();

	DLLEXPORT VoxoxPhoneNumber& operator=( const VoxoxPhoneNumber& src );

	//Sets ---------------------------------------------------------------------
	DLLEXPORT void setDid( const std::string& value )					{ mDid = value;	}
	DLLEXPORT void setJid( const std::string& value )					{ mJid = value;	}

	//Gets ---------------------------------------------------------------------
	DLLEXPORT std::string getDid()					const			{ return mDid;	}
	DLLEXPORT std::string getJid()					const			{ return mJid;	}

private:
	std::string mDid;
	std::string mJid;
};	//class VoxoxPhoneNumber 

//=============================================================================

class VoxoxPhoneNumberList : public std::list<VoxoxPhoneNumber>
{
public:
	VoxoxPhoneNumberList()		{}
	~VoxoxPhoneNumberList()		{}
};

//=============================================================================


//=============================================================================
// XmppContact class
//	- This class does not have direct DB table corollary
//	- This data is received from main app and used to update DB.
//=============================================================================

class XmppContact 
{
public:
	DLLEXPORT XmppContact();
	DLLEXPORT ~XmppContact();

	DLLEXPORT XmppContact& operator=( const XmppContact& src );

	//Sets ---------------------------------------------------------------------
	DLLEXPORT void setJid	    ( const std::string& value )			{ mJID		= value;	}
	DLLEXPORT void setName	( const std::string& value )				{ mName		= value;	}
	DLLEXPORT void setStatus  ( const std::string& value )				{ mStatus	= value;	}
	DLLEXPORT void setMsgState( const std::string& value )				{ mMsgState = value;	}

	//Gets ---------------------------------------------------------------------
	DLLEXPORT std::string getJid()		const							{ return mJID;		}
	DLLEXPORT std::string getName()		const							{ return mName;		}
	DLLEXPORT std::string getStatus()		const						{ return mStatus;	}
	DLLEXPORT std::string getMsgState()	const							{ return mMsgState;	}
	
private:
	std::string mJID;			// username@voxox.com
	std::string mName;			// user chosen name (alias), if null use name portion of JID
	std::string mStatus; 		// available/unavailable/chat/away/dnd/xa	//TODO: This is normally an int and translated to text.
	std::string mMsgState;		// contact-set status message

//	private ArrayList<String> mRes;			//TODO: Verify these are not needed
//	private ArrayList<String> mGroups;
};	//class XmppContact 

//=============================================================================


//=============================================================================
// ContactMessageSummary class
//	- Provide Summary of each message thread for Message Thread window
//=============================================================================

class ContactMessageSummary
{
public:
	DLLEXPORT ContactMessageSummary();
	DLLEXPORT ~ContactMessageSummary();

	DLLEXPORT ContactMessageSummary& operator=( const ContactMessageSummary& src );

	//Sets ---------------------------------------------------------------------
	DLLEXPORT void setContactKey		( const std::string&	value )					{ mContactKey		  = value;	}
	DLLEXPORT void setSource			( const std::string&	value )					{ mSource			  = value;	}
	DLLEXPORT void setCmGroup			( int					value )					{ mCmGroup			  = value;	}
	DLLEXPORT void setCmGroupName		( const std::string&	value )					{ mCmGroupName		  = value;	}
	DLLEXPORT void setDisplayName		( const std::string&	value )					{ mDisplayName		  = value;	}
	DLLEXPORT void setPhoneNumber		( const std::string&	value )					{ mPhoneNumber		  = value;	}
	DLLEXPORT void setDisplayNumber		( const std::string&	value )					{ mDisplayNumber	  = value;	}
	DLLEXPORT void setXmppJid			( const std::string&	value )					{ mXmppJid			  = value;	}

	//For GM
	DLLEXPORT void setToGroupId			( const std::string&	value )					{ mToGroupId		  = value;	}
	DLLEXPORT void setGroupName			( const std::string&	value )					{ mGroupName		  = value;	}
	DLLEXPORT void setGroupStatus		( GroupMessageGroup::Status value )				{ mGroupStatus		  = value;	}

	//Newest Message Info
	DLLEXPORT void setMsgDir			( Message::Direction	value )					{ mMsgDir			  = value;	}
	DLLEXPORT void setMsgType			( Message::Type			value )					{ mMsgType			  = value;	}
	DLLEXPORT void setMsgServerTimestamp( __int64				value )					{ mMsgServerTimestamp = value;	}
	DLLEXPORT void setMsgBody			( const std::string&	value )					{ mMsgBody			  = value;	}
	DLLEXPORT void setMsgTranslated		( const std::string&	value )					{ mMsgTranslated	  = value;	}
	DLLEXPORT void setMsgToDid			( const std::string&	value )					{ mMsgToDid			  = value;	}
	DLLEXPORT void setMsgFromDid		( const std::string&	value )					{ mMsgFromDid		  = value;	}
	DLLEXPORT void setMsgToJid			( const std::string&	value )					{ mMsgToJid			  = value;	}
	DLLEXPORT void setMsgFromJid		( const std::string&	value )					{ mMsgFromJid		  = value;	}


	//Gets ---------------------------------------------------------------------
	DLLEXPORT std::string				getContactKey()			const					{ return mContactKey;		}
	DLLEXPORT std::string				getSource()				const					{ return mSource;			}
	DLLEXPORT int						getCmGroup()			const					{ return mCmGroup;			}
	DLLEXPORT std::string				getCmGroupName()		const					{ return mCmGroupName;		}
	DLLEXPORT std::string				getDisplayName()		const					{ return mDisplayName;		}
	DLLEXPORT std::string				getPhoneNumber()		const					{ return mPhoneNumber;		}
	DLLEXPORT std::string				getDisplayNumber()		const					{ return mDisplayNumber;	}
	DLLEXPORT std::string				getXmppJid()			const					{ return mXmppJid;			}

	//For GM
	DLLEXPORT std::string				getToGroupId()			const					{ return mToGroupId;		}
	DLLEXPORT std::string				getGroupName()			const					{ return mGroupName;		}
	DLLEXPORT GroupMessageGroup::Status getGroupStatus()		const					{ return mGroupStatus;		}

	//Newest Message Info
	DLLEXPORT Message::Direction		getMsgDir()				const					{ return mMsgDir;			}
	DLLEXPORT Message::Type				getMsgType()			const					{ return mMsgType;			}
	DLLEXPORT _int64					getMsgServerTimestamp()	const					{ return mMsgServerTimestamp; }
	DLLEXPORT std::string				getMsgBody()			const					{ return mMsgBody;			}
	DLLEXPORT std::string				getMsgTranslated()		const					{ return mMsgTranslated;	}
	DLLEXPORT std::string				getMsgToDid()			const					{ return mMsgToDid;			}
	DLLEXPORT std::string				getMsgFromDid()			const					{ return mMsgFromDid;		}
	DLLEXPORT std::string				getMsgToJid()			const					{ return mMsgToJid;			}
	DLLEXPORT std::string				getMsgFromJid()			const					{ return mMsgFromJid;		}


private:
	//Contact/PhoneNumber info
	std::string			mContactKey;
	std::string			mSource;
	int					mCmGroup;
	std::string			mCmGroupName;
	std::string			mDisplayName;
	std::string			mPhoneNumber;		//Normalized 10-digit
	std::string			mDisplayNumber;
	std::string			mXmppJid;

	//For GM
	std::string					mToGroupId;			//From Message table
	std::string					mGroupName;			//From GMG table
	GroupMessageGroup::Status	mGroupStatus;	//From GMG table

	//Newest Message Info
	Message::Direction	mMsgDir;
	Message::Type		mMsgType;
	__int64				mMsgServerTimestamp;
	std::string			mMsgBody;
	std::string			mMsgTranslated;
	std::string			mMsgToDid;
	std::string			mMsgFromDid;
	std::string			mMsgToJid;
	std::string			mMsgFromJid;
};	//class ContactMessageSummary

//=============================================================================

class ContactMessageSummaryList : public std::list<ContactMessageSummary>
{
public:
	ContactMessageSummaryList()			{}
	~ContactMessageSummaryList()		{}
};

//=============================================================================



//=============================================================================
// DeleteMessageData class
//	- Data needed to delete messages on server
//	- This class must be defined AFTER Messge class since it uses Message::Type, etc.
//=============================================================================

class DeleteMessageData
{
public:
	DLLEXPORT DeleteMessageData();
	DLLEXPORT DeleteMessageData( const std::string msgId, Message::Type type, Message::Status status );
	DLLEXPORT ~DeleteMessageData();

	DLLEXPORT DeleteMessageData operator=( const DeleteMessageData& src );

	//Sets ---------------------------------------------------------------------
	DLLEXPORT void setMsgId     ( const std::string&	value )			{ mMsgId	  = value;	}
	DLLEXPORT void setSvrMsgType( const std::string&	value )			{ mSvrMsgType = value;	}
	DLLEXPORT void setMsgType   ( Message::Type			value )			{ mMsgType    = value;	}
	DLLEXPORT void setMsgStatus ( Message::Status		value )			{ mMsgStatus  = value;	}
	DLLEXPORT void setSuccess   ( bool					value )			{ mSuccess	  = value;	}


	//Gets ---------------------------------------------------------------------
	DLLEXPORT std::string		getMsgId()		const					{ return mMsgId;		}
	DLLEXPORT std::string		getSvrMsgType()	const					{ return mSvrMsgType;	}
	DLLEXPORT Message::Type		getMsgType()	const					{ return mMsgType;		}
	DLLEXPORT Message::Status	getMsgStatus()	const					{ return mMsgStatus;	}
	DLLEXPORT bool				getSuccess()	const					{ return mSuccess;		}

private:
	std::string		mMsgId;
	std::string		mSvrMsgType;
	Message::Type	mMsgType;
	Message::Status mMsgStatus;		//May not be needed.
	bool			mSuccess;		//This may become false after we handle REST API response.
};	//class DeleteMessageData

//=============================================================================

class DeleteMessageDataList : public std::list<DeleteMessageData>
{
public:
	DeleteMessageDataList()		{}
	~DeleteMessageDataList()	{}
};

//=============================================================================


//=============================================================================

class UnreadCounts
{
public:
	DLLEXPORT UnreadCounts();
	DLLEXPORT ~UnreadCounts();

	DLLEXPORT UnreadCounts operator=( const UnreadCounts& src );

	//Sets ---------------------------------------------------------------------
	DLLEXPORT void setChatCount		   ( int value )			{ mChat			  = value;	}
	DLLEXPORT void setSmsCount		   ( int value )			{ mSms			  = value;	}
	DLLEXPORT void setGmCount		   ( int value )			{ mGm			  = value;	}
	DLLEXPORT void setFaxCount		   ( int value )			{ mFax			  = value;	}
	DLLEXPORT void setVoicemailCount   ( int value )			{ mVoicemail	  = value;	}
	DLLEXPORT void setRecordedCallCount( int value )			{ mRecordedCall	  = value;	}
	DLLEXPORT void setMissedCallCount  ( int value )			{ mMissedCall	  = value;	}

	//Gets ---------------------------------------------------------------------
	DLLEXPORT int	getChatCount()			const				{ return mChat;			}
	DLLEXPORT int	getSmsCount()			const				{ return mSms;			}
	DLLEXPORT int	getGmCount()			const				{ return mGm;			}
	DLLEXPORT int	getFaxCount()			const				{ return mFax;			}
	DLLEXPORT int	getVoicemailCount()		const				{ return mVoicemail;	}
	DLLEXPORT int	getRecordedCallCount()	const				{ return mRecordedCall;	}
	DLLEXPORT int	getMissedCallCount()	const				{ return mMissedCall;	}

private:
	int mChat;
	int mSms;
	int mGm;
	int mFax;
	int mVoicemail;
	int mRecordedCall;
	int	mMissedCall;
};

//=============================================================================


//=============================================================================

class UserSettings
{
public:
	DLLEXPORT UserSettings();
	DLLEXPORT ~UserSettings();

	DLLEXPORT UserSettings operator=( const UserSettings& src );

	//Sets ---------------------------------------------------------------------
	DLLEXPORT void setUserId				  ( const std::string&	value )		{ mUserId					= value;	}

	DLLEXPORT void setAudioInputDevice		  ( const std::string&	value )		{ mAudioInputDevice			= value;	}
	DLLEXPORT void setAudioOutputDevice		  ( const std::string&	value )		{ mAudioOutputDevice		= value;	}
	DLLEXPORT void setAudioInputVolume		  ( int					value )		{ mAudioInputVolume			= value;	}
	DLLEXPORT void setAudioOutputVolume		  ( int					value )		{ mAudioOutputVolume		= value;	}

	DLLEXPORT void setShowFaxInMsgThread	  ( bool				value )		{ mShowFaxInMsgThread		= value;	}
	DLLEXPORT void setShowVmInMsgThread		  ( bool				value )		{ mShowVmInMsgThread		= value;	}
	DLLEXPORT void setShowRcInMsgThread		  ( bool				value )		{ mShowRcInMsgThread		= value;	}

	DLLEXPORT void setEnableSoundIncomingMsg  ( bool				value )		{ mEnableSoundIncomingMsg	= value;	}
	DLLEXPORT void setEnableSoundIncomingCall ( bool				value )		{ mEnableSoundIncomingCall	= value;	}
//	DLLEXPORT void setEnableSoundOutgoingMsg  ( bool				value )		{ mEnableSoundOutgoingMsg	= value;	}

	DLLEXPORT void setMyChatBubbleColor		   ( const std::string& value )		{ mMyChatBubbleColor		 = value;	}
	DLLEXPORT void setConversantChatBubbleColor( const std::string& value )		{ mConversantChatBubbleColor = value;	}

	DLLEXPORT void setCountryCode			  ( const std::string&	value )		{ mCountryCode				= value;	}
	DLLEXPORT void setCountryAbbrev			  ( const std::string&	value )		{ mCountryAbbrev			= value;	}

//	DLLEXPORT void setOutgoingCallSoundFile	  ( const std::string&	value )		{ mOutgoingCallSoundFile	= value;	}
	DLLEXPORT void setIncomingCallSoundFile	  ( const std::string&	value )		{ mIncomingCallSoundFile	= value;	}
	DLLEXPORT void setCallClosedSoundFile	  ( const std::string&	value )		{ mCallClosedSoundFile		= value;	}

	DLLEXPORT void setWindowLeft			  ( double				value )		{ mWindowLeft				= value;	}
	DLLEXPORT void setWindowTop				  ( double				value )		{ mWindowTop				= value;	}
	DLLEXPORT void setWindowHeight			  ( double				value )		{ mWindowHeight				= value;	}
	DLLEXPORT void setWindowWidth			  ( double				value )		{ mWindowWidth				= value;	}
	DLLEXPORT void setWindowState			  ( int					value )		{ mWindowState				= value;	}

	DLLEXPORT void setLastMsgTimestamp		  ( __int64				value )		{ mLastMsgTimestamp			= value;	}
	DLLEXPORT void setLastCallTimestamp		  ( __int64				value )		{ mLastCallTimestamp		= value;	}
	DLLEXPORT void setLastContactSourceKey	  ( const std::string&	value )		{ mLastContactSourceKey		= value;	}

	DLLEXPORT void setSipUuid				  (  const std::string& value)		{ mSipUuid					= value;	}

	//Gets ---------------------------------------------------------------------
	DLLEXPORT std::string	getUserId					()	const  { return mUserId;					}

	DLLEXPORT std::string	getAudioInputDevice			()	const  { return mAudioInputDevice;			}
	DLLEXPORT std::string	getAudioOutputDevice		()	const  { return mAudioOutputDevice;			}
	DLLEXPORT int			getAudioInputVolume			()	const  { return mAudioInputVolume;			}
	DLLEXPORT int			getAudioOutputVolume		()	const  { return mAudioOutputVolume;			}

	DLLEXPORT bool			getShowFaxInMsgThread		()	const  { return mShowFaxInMsgThread;		}
	DLLEXPORT bool			getShowVmInMsgThread		()	const  { return mShowVmInMsgThread;			}
	DLLEXPORT bool			getShowRcInMsgThread		()	const  { return mShowRcInMsgThread;			}

	DLLEXPORT bool			getEnableSoundIncomingMsg	()	const  { return mEnableSoundIncomingMsg;	}
	DLLEXPORT bool			getEnableSoundIncomingCall  ()	const  { return mEnableSoundIncomingCall;	}
//	DLLEXPORT bool			getEnableSoundOutgoingMsg	()	const  { return mEnableSoundOutgoingCall;	}

	DLLEXPORT std::string	getMyChatBubbleColor		()	const  { return mMyChatBubbleColor;			}
	DLLEXPORT std::string	getConversantChatBubbleColor()	const  { return mConversantChatBubbleColor;	}

	DLLEXPORT std::string	getCountryCode				()	const  { return mCountryCode;				}
	DLLEXPORT std::string	getCountryAbbrev			()	const  { return mCountryAbbrev;				}

//	DLLEXPORT std::string	getOutgoingCallSoundFile	()	const  { return mOutgoinCallSoundFile;		}
	DLLEXPORT std::string	getIncomingCallSoundFile	()	const  { return mIncomingCallSoundFile;		}
	DLLEXPORT std::string	getCallClosedSoundFile		()	const  { return mCallClosedSoundFile;		}

	DLLEXPORT double		getWindowLeft				()	const  { return mWindowLeft;				}
	DLLEXPORT double		getWindowTop				()	const  { return mWindowTop;					}
	DLLEXPORT double		getWindowHeight				()	const  { return mWindowHeight;				}
	DLLEXPORT double		getWindowWidth				()	const  { return mWindowWidth;				}
	DLLEXPORT int			getWindowState				()	const  { return mWindowState;				}

	DLLEXPORT __int64		getLastMsgTimestamp			()	const  { return mLastMsgTimestamp;			}
	DLLEXPORT __int64		getLastCallTimestamp		()	const  { return mLastCallTimestamp;			}
	DLLEXPORT std::string	getLastContactSourceKey		()	const  { return mLastContactSourceKey;		}

	DLLEXPORT std::string	getSipUuid					()	const  { return mSipUuid;					}


private:
	std::string	mUserId;

	std::string	mAudioInputDevice;
	std::string mAudioOutputDevice;
	int			mAudioInputVolume;
	int			mAudioOutputVolume;

	bool		mShowFaxInMsgThread;
	bool		mShowVmInMsgThread;
	bool		mShowRcInMsgThread;

	bool		mEnableSoundIncomingMsg;
	bool		mEnableSoundIncomingCall;
//	bool		mEnableSoundOutgoingMsg;

	std::string	mMyChatBubbleColor;
	std::string	mConversantChatBubbleColor;

	std::string	mCountryCode;
	std::string mCountryAbbrev;

//	std::string	mOutgoingCallSoundFile;
	std::string mIncomingCallSoundFile;
	std::string mCallClosedSoundFile;

	double		mWindowLeft;
	double		mWindowTop;	 
	double		mWindowHeight;
	double		mWindowWidth;
	int			mWindowState;

	__int64		mLastMsgTimestamp;
	__int64		mLastCallTimestamp;
	std::string mLastContactSourceKey;

	std::string	mSipUuid;
};

//=============================================================================




//=============================================================================
//AppSettings - NOT PART OF USER DB
//=============================================================================

class AppSettings
{
public:
	DLLEXPORT AppSettings();
	DLLEXPORT ~AppSettings();

	DLLEXPORT AppSettings operator=( const AppSettings& src );

	//Sets ---------------------------------------------------------------------
	DLLEXPORT void setRecordId			  ( int					value )			{ mRecordId				= value;	}

	DLLEXPORT void setAutoLogin			  ( bool				value )			{ mAutoLogin			= value;	}
	DLLEXPORT void setRememberPassword	  ( bool				value )			{ mRememberPassword		= value;	}
	DLLEXPORT void setLastLoginUsername	  ( const std::string&	value )			{ mLastLoginUsername	= value;	}
	DLLEXPORT void setLastLoginPassword	  ( const std::string&	value )			{ mLastLoginPassword	= value;	}
	DLLEXPORT void setLastLoginCountryCode( const std::string&	value )			{ mLastLoginCountryCode = value;	}
	DLLEXPORT void setLastLoginPhoneNumber( const std::string&	value )			{ mLastLoginPhoneNumber = value;	}

	//Debug items
	DLLEXPORT void setDebugMenu			    ( const std::string&	value )		{ mDebugMenu			  = value;	}
	DLLEXPORT void setIgnoreLoggers		    ( const std::string&	value )		{ mIgnoreLoggers		  = value;	}
	DLLEXPORT void setMaxSipLogging		    ( bool					value )		{ mMaxSipLogging		  = value;	}
	DLLEXPORT void setGetInitOptionsHandling( int					value )		{ mGetInitOptionsHandling = value;	}

	//Gets ---------------------------------------------------------------------
	DLLEXPORT int			getRecordId		   ()		const  { return mRecordId;				}

	DLLEXPORT bool			getAutoLogin	   ()		const  { return mAutoLogin;				}
	DLLEXPORT bool			getRememberPassword()		const  { return mRememberPassword;		}
	DLLEXPORT std::string	getLastLoginUsername()		const  { return mLastLoginUsername;		}
	DLLEXPORT std::string	getLastLoginPassword()		const  { return mLastLoginPassword;		}
	DLLEXPORT std::string	getLastLoginCountryCode()	const  { return mLastLoginCountryCode;	}
	DLLEXPORT std::string	getLastLoginPhoneNumber()	const  { return mLastLoginPhoneNumber;	}

	//Debug items
	DLLEXPORT std::string	getDebugMenu()				const  { return mDebugMenu;				 }
	DLLEXPORT std::string	getIgnoreLoggers()			const  { return mIgnoreLoggers;			 }
	DLLEXPORT bool			getMaxSipLogging()			const  { return mMaxSipLogging;			 }
	DLLEXPORT int			getGetInitOptionsHandling()	const  { return mGetInitOptionsHandling; }

private:
	int			mRecordId;

	bool		mAutoLogin;
	bool		mRememberPassword;
	std::string	mLastLoginUsername;
	std::string mLastLoginPassword;
	std::string mLastLoginCountryCode;
	std::string mLastLoginPhoneNumber;

	//Debug items
	std::string	mDebugMenu;
	std::string	mIgnoreLoggers;
	bool		mMaxSipLogging;
	int			mGetInitOptionsHandling;
};

//=============================================================================


//=============================================================================

class DbChangeData
{
public:
	enum Table	//Which table has been changed.  Allow listeners to select which tables they care about.
	{
		ANY          = 0,
		MESSAGE      = 1,
		CONTACT      = 2,
		GM		     = 3,
		RECENT_CALL  = 4,
		SETTINGS	 = 5,
		APP_SETTINGS = 6
	};

	enum Action
	{
		NO_ACTION = 0,
		ADD		  = 1,
		EDIT	  = 2,
		DELETE	  = 3
	};

			  DbChangeData();
	DLLEXPORT DbChangeData( Table table );
	DLLEXPORT ~DbChangeData();

	DLLEXPORT DbChangeData& operator=( const DbChangeData& src );

	//General
	DLLEXPORT Table				getTable()		const;

	//Message table
	DLLEXPORT Message::Status	getMsgStatus()	const;
	DLLEXPORT size_t			getMsgCount()	const;

	//Contact
	DLLEXPORT int				getVoxoxId()	const;

	//GM
	DLLEXPORT std::string		getGroupId()	const;	//GM NameActivity ONLY on ADD.
	DLLEXPORT Action			getGmAction()	const;

	DLLEXPORT bool				isGmAdd()		const;
	DLLEXPORT bool				isGmEdit()		const;
	DLLEXPORT bool				isGmDelete()	const;

	//RecentCall - None

	//Sets -------------------------------------------------------------------------
	// Do NOT export these
	void setMsgStatus( Message::Status value )			{ mMsgStatus = value;	}
	void setMsgCount ( size_t		   value )			{ mMsgCount  = value;	}	

	//Contact
	void setVoxoxId  ( int			   value )			{ mVoxoxId   = value;	}

	//GM
	void setGroupId( const std::string& value )			{ mGroupId	 = value;	}
	void setGmAction( Action 			value )			{ mGmAction  = value;   }

private:
	bool isMsgTable()		 const;
	bool isContactTable()	 const;
	bool isGmTable()		 const;
	bool isRecentCallTable() const;
	bool isSettingsTable()	 const;

	bool isAppSettingsTable() const;

	bool isGmAction( Action action ) const;

private:
	Table			mTable;

	//Message extras
	Message::Status	mMsgStatus;		//For msg STATUS change typically tracked in UMW, e.g. NEW -> READ
	size_t			mMsgCount;		//Used in StateMachine on initial load to display 'No Data Available' if no messages are received from server.

	//Contact extras
	int				mVoxoxId;		//Do we need this in broadcast? Used in ContactDetailsActivity to update GM Groups

	//GM
	std::string		mGroupId;		//GM NameActivity ONLY on ADD.
	Action			mGmAction;		//ADD, EDIT, DELETE.  Used in GM DetailsActivity, UMW
};	//class DbChangeData

//=============================================================================
