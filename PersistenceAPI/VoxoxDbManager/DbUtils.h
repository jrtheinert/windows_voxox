/**
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/


//=====================================================================================================================
// This class will contain some static methods to HELP VoxoxDbManager
//	- All methods are static
//	- This helps keep the VoxoxDbManager code clearer.
//=====================================================================================================================

#pragma once

#include "DataTypes.h"
#include "StringList.h"
#include "DllExport.h"

#include <string>
#include <list>


class DbUtils
{
public:
	//New
	DLLEXPORT static std::string	stringListToSelectClause( const StringList& list );
	DLLEXPORT static std::string	stringListToString      ( const StringList& list, const std::string& separator );
	DLLEXPORT static std::string	intArrayToInClause		( const std::list<int>& values );
	DLLEXPORT static bool			contains ( const std::list<int>& list, int tgtValue );
	DLLEXPORT static bool			contains ( const std::list<std::string>& list, const std::string& tgtValue );
	DLLEXPORT static std::string	splitLeft( const std::string& text, const std::string& delim );

	//From VoxoxUtils
	DLLEXPORT static std::string	ensureFullJid			   ( const std::string& jid );
	DLLEXPORT static std::string	getNormalizedNumberNoPrefix( const std::string& did	);
	DLLEXPORT static std::string	getNormalizedNumber        ( const std::string& did	);

	//From Misc.Contants
	DLLEXPORT static std::string	getTranslatorDefaultLocale()		{ return mTranslatorDefaultLocale;	}

	//From Database
	static std::string escapeParameter( const std::string& paramIn );

	//From DatabaseManagerHelper
	DLLEXPORT static std::string intArrayToString( const int values[], int size ) ;
	DLLEXPORT static std::string intArrayToString( const std::list<int> values  ) ;
	DLLEXPORT static PhoneNumber buildPhoneNumber( const std::string& requestedPhoneNumberString, const PhoneNumberList& list );

	//From String or String/StringList related
	DLLEXPORT static std::string replace	( const std::string& text, const std::string& before, const std::string& after, bool caseSensitive );
	DLLEXPORT static std::string toLowerCase( const std::string& text );
	DLLEXPORT static bool		 contains	( const std::string& text, const std::string& str, bool caseSensitive);
	DLLEXPORT static std::string getNthListEntry(const StringList& list, size_t tgtItem);

	/**
		* Returns the name portion of a XMPP address. For example, for the
		* address "matt@jivesoftware.com/Smack", "matt" would be returned. If no
		* username is present in the address, the empty string will be returned.
	*/
	DLLEXPORT static std::string parseName( const std::string& xmppAddress);

	/**
     * Returns the XMPP address with any resource information removed. For example,
     * for the address "matt@jivesoftware.com/Smack", "matt@jivesoftware.com" would
     * be returned.
     *
     * @param XMPPAddress the XMPP address.
     * @return the bare XMPP address without resource information.
     */
    DLLEXPORT static std::string parseBareAddress( const std::string& xmppAddress);

	//Helpers
	DLLEXPORT static std::string	digitsOnly( const std::string& valueIn );
	DLLEXPORT static std::string	getNameSep();
	DLLEXPORT static __int64	    getSystemTimeMillis();
	DLLEXPORT static std::string	convertMillisToServerTimeStamp( __int64 timestamp ) ;
	DLLEXPORT static bool			isValidExtensionLength( int len );

private:
	DbUtils()			{}	//Prevents instantiation
	~DbUtils()			{}

private:
	//Do NOT make these 'const' since we may have a need to set them externally.
	static const std::string	mTranslatorDefaultLocale;
	static const std::string	mXmppServer;
	static const std::string	mNameSep;
	static const size_t			mPhoneNumberKeyLen;

public: 
	//TODO: These *may* be moved to REST API, but will define here for now
	static const std::string mSvrMsgType_Voicemail;
	static const std::string mSvrMsgType_Fax;
	static const std::string mSvrMsgType_RecordedCall;
	static const std::string mSvrMsgType_Sms;
	static const std::string mSvrMsgType_Xmpp;

	static const std::string mSvrVoicemailStatus_New;
	static const std::string mSvrVoicemailStatus_Heard;
	static const std::string mSvrVoicemailStatus_Deleted;

};

