/**
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

#include "DbManagerConstants.h"
#include "DatabaseTables.h"
#include "DataTypes.h"				//Mostly for ENUMS
#include "DbUtils.h"				//For NameSep

StringList  DbManagerConstants::mGetMessagesProjection;
StringList  DbManagerConstants::mGetMessagesForGroupIdProjection;
StringList  DbManagerConstants::mGetMessagesForCmGroupProjection;
StringList	DbManagerConstants::mGetPhoneNumberProjection;
StringList  DbManagerConstants::mMessageTranslationPropertyProjection;
StringList  DbManagerConstants::mGetRecentCallProjection;
StringList  DbManagerConstants::mGetSettingsProjection;
StringList  DbManagerConstants::mGetAppSettingsProjection;

std::string DbManagerConstants::mNoDeletedMessagesCondition;
std::string	DbManagerConstants::mContactSelect;
std::string	DbManagerConstants::mPhoneNumberAsContactSelectLong;
std::string	DbManagerConstants::mPhoneNumberAsContactSelectShort;
std::string DbManagerConstants::mGroupMessageGroupAsContactSelectLong;
std::string DbManagerConstants::mGroupMessageGroupAsContactSelectShort;

std::string	DbManagerConstants::mContactsMessagesDisplayNameSubquery;
std::string	DbManagerConstants::mRecentCallRawQuery;

std::string DbManagerConstants::mVoxoxCountSubquery;
std::string DbManagerConstants::mBlockedCountSubquery;
std::string DbManagerConstants::mFavoriteCountSubquery;
std::string DbManagerConstants::mPhoneNumberCountSubquery;
std::string DbManagerConstants::mExtensionCountSubquery;
std::string DbManagerConstants::mExtensionSubquery;
std::string DbManagerConstants::mBestPhoneNumberSubquery;

std::string DbManagerConstants::mGmGroupActiveClause;
std::string DbManagerConstants::mXmppContactsMessagesOnlyRawQuery;
std::string DbManagerConstants::mAllContactsMessagesSummaryRawQuery;
std::string DbManagerConstants::mMessageToPhoneNumberJoin;

std::string DbManagerConstants::mSourceInClause;

//-----------------------------------------------------------------------------

//static
std::string DbManagerConstants::getContactsMessagesDisplayNameSubquery()
{
	if ( mContactsMessagesDisplayNameSubquery.empty() )
	{
		mContactsMessagesDisplayNameSubquery = 
					"  (SELECT replace(group_concat(distinct " + DatabaseTables::PhoneNumber::NAME + " ) , \",\", \"" +
					DbUtils::getNameSep() + "\") " +
					"FROM " + DatabaseTables::PhoneNumber::TABLE + " pn2 " +
					"WHERE pn." +  DatabaseTables::PhoneNumber::CM_GROUP + " = pn2." +  DatabaseTables::PhoneNumber::CM_GROUP + " ) AS " + DatabaseTables::Contact::DISPLAY_NAME + " ";
	}

	return mContactsMessagesDisplayNameSubquery;
}


//This relies on query using 'pn' for table parameter in query().
//static
std::string DbManagerConstants::formatPhoneNumberBasedCountSubquery( const std::string& criteria, const std::string& asParameter )
{
	const std::string pnAlias1 = "pn";
	const std::string pnAlias2 = "pn2";
	const std::string keyField = "contact_key";

	const std::string table2		 = DatabaseTables::PhoneNumber::TABLE + " " + pnAlias2 + " ";
	const std::string whereSubClause = pnAlias1 + "." + keyField + " = " + pnAlias2 + "." + keyField + " ";

	std::string result;
	std::string additionalCriteria = ( criteria.empty() ? "" : " AND " + pnAlias2 + "." + criteria );

	result = "	(SELECT COUNT() FROM " + table2 +
			 " WHERE " + whereSubClause + additionalCriteria + " ) " +
			 " AS " + asParameter;

	return result;
}

//This relies on query using 'pn' for table parameter in query().  Should not be a problem since this projection is only used once.
//static
std::string DbManagerConstants::getVoxoxCountSubquery()
{
	if ( mVoxoxCountSubquery.empty() )
	{
		std::string criteria = DatabaseTables::PhoneNumber::IS_VOXOX + " > 0";
		mVoxoxCountSubquery = formatPhoneNumberBasedCountSubquery( criteria, DatabaseTables::Contact::VOXOX_COUNT );
	}

	return mVoxoxCountSubquery;
}

//This relies on query using 'pn' for table parameter in query().  Should not be a problem since this projection is only used once.
//static
std::string DbManagerConstants::getBlockedCountSubquery()
{
	if ( mBlockedCountSubquery.empty() )
	{
		std::string criteria = DatabaseTables::PhoneNumber::IS_BLOCKED + " > 0";
		mBlockedCountSubquery = formatPhoneNumberBasedCountSubquery( criteria, DatabaseTables::Contact::BLOCKED_COUNT );
	}

	return mBlockedCountSubquery;
}

//This relies on query using 'pn' for table parameter in query().  Should not be a problem since this projection is only used once.
//static
std::string DbManagerConstants::getFavoriteCountSubquery()
{
	if ( mFavoriteCountSubquery.empty() )
	{
		std::string criteria = DatabaseTables::PhoneNumber::IS_FAVORITE + " > 0";
		mFavoriteCountSubquery = formatPhoneNumberBasedCountSubquery( criteria, DatabaseTables::Contact::FAVORITE_COUNT );
	}

	return mFavoriteCountSubquery;
}

//This relies on query using 'pn' for table parameter in query().  Should not be a problem since this projection is only used once.
//static
std::string DbManagerConstants::getPhoneNumberCountSubquery()
{
	if ( mPhoneNumberCountSubquery.empty() )
	{
		std::string criteria = "";
		mPhoneNumberCountSubquery = formatPhoneNumberBasedCountSubquery( criteria, DatabaseTables::Contact::PHONE_NUMBER_COUNT );
	}

	return mPhoneNumberCountSubquery;
}

//This relies on query using 'pn' for table parameter in query().  Should not be a problem since this projection is only used once.
//static
std::string DbManagerConstants::getExtensionCountSubquery()
{
	if ( mExtensionCountSubquery.empty() )
	{
		std::string criteria = DatabaseTables::PhoneNumber::IS_EXT + " > 0 ";
//							   "AND LEN( " + DatabaseTables::PhoneNumber::NUMBER + ") >= 3 " +
//							   "AND LEN( " + DatabaseTables::PhoneNumber::NUMBER + ") <= 7 ";

		mExtensionCountSubquery = formatPhoneNumberBasedCountSubquery( criteria, DatabaseTables::Contact::EXTENSION_COUNT );
	}

	return mExtensionCountSubquery;
}

//This relies on query using 'pn' for table parameter in query().  Should not be a problem since this projection is only used once.
//static
std::string DbManagerConstants::getExtensionSubquery()
{
	if ( mExtensionSubquery.empty() )
	{
		const std::string pnAlias1 = "pn";
		const std::string pnAlias2 = "pn2";
		const std::string keyField = "contact_key";

		const std::string table2		 = DatabaseTables::PhoneNumber::TABLE + " " + pnAlias2 + " ";
		const std::string whereSubClause = pnAlias1 + "." + keyField + " = " + pnAlias2 + "." + keyField + 
											" AND " + DatabaseTables::PhoneNumber::IS_EXT + " = 1 ";

		mExtensionCountSubquery = "	(SELECT " + DatabaseTables::PhoneNumber::DISPLAY_NUMBER + " FROM " + table2 +
								  " WHERE " + whereSubClause + " LIMIT 1 ) " +
								  " AS " + DatabaseTables::Contact::EXTENSION + " ";
	}

	return mExtensionCountSubquery;
}

//This relies on query using 'pn' for table parameter in query().  Should not be a problem since this projection is only used once.
//static
std::string DbManagerConstants::getBestPhoneNumberSubquery()
{
	//We want phone number in this order
	//	- In-Net (Voxox)
	//	- Off-net
	//	- NEVER choose Extension, we have a separate subquery for that.
	if ( mBestPhoneNumberSubquery.empty() )
	{
		const std::string pnAlias1 = "pn";
		const std::string pnAlias2 = "pn2";
		const std::string keyField = "contact_key";

		const std::string table2		 = DatabaseTables::PhoneNumber::TABLE + " " + pnAlias2 + " ";
		const std::string whereSubClause = pnAlias1 + "." + keyField + " = " + pnAlias2 + "." + keyField + 
											" AND " + DatabaseTables::PhoneNumber::IS_EXT + " = 0 ";	//Omit extensions

		mBestPhoneNumberSubquery = " (SELECT " + DatabaseTables::PhoneNumber::DISPLAY_NUMBER + " FROM " + table2 +
								   " WHERE " + whereSubClause + 
								   " ORDER BY " +DatabaseTables::PhoneNumber::IS_VOXOX + " DESC " +		//Force in-net to top of list
								   " LIMIT 1 ) " +														//Take only top item
								   " AS " + DatabaseTables::Contact::PREFERRED_NUMBER + " ";
	}

	return mBestPhoneNumberSubquery;
}

//static
std::string DbManagerConstants::getGmGroupActiveClause()
{
	if ( mGmGroupActiveClause.empty() )
	{
		mGmGroupActiveClause = " " + DatabaseTables::GroupMessageGroup::STATUS + " = " + std::to_string( GroupMessageGroup::ACTIVE ) +  " ";
	}

	return mGmGroupActiveClause;
}

//static

std::string DbManagerConstants::getXmppContactsMessagesOnlyRawQuery()
{
	if ( mXmppContactsMessagesOnlyRawQuery.empty() )
	{
		//FROM_JID and INBOUND messages
		std::string subquery1  = "SELECT " + DatabaseTables::PhoneNumber::XMPP_JID + " FROM " + DatabaseTables::PhoneNumber::TABLE +
								 " WHERE " + DatabaseTables::PhoneNumber::XMPP_JID + " NOT NULL ";

		std::string selection1 =  "SELECT " + DatabaseTables::Message::FROM_JID    + " AS JID FROM " + DatabaseTables::Message::TABLE +
								  " WHERE " + DatabaseTables::Message::FROM_JID    + " NOT NULL " +
								  "   AND " + DatabaseTables::Message::CONTACT_DID + " IS NULL "
								  "   AND " + DatabaseTables::Message::FROM_JID    + " NOT IN ( " + subquery1 + " ) " +
								  "   AND " + DatabaseTables::Message::DIRECTION   + " = 0 ";

		//TO_JID and OUTBOUND messages
		std::string subquery2  = "SELECT " + DatabaseTables::PhoneNumber::XMPP_JID + " FROM " + DatabaseTables::PhoneNumber::TABLE +
								 " WHERE " + DatabaseTables::PhoneNumber::XMPP_JID + " NOT NULL ";

		std::string selection2 =  "SELECT " + DatabaseTables::Message::TO_JID      + " AS JID FROM " + DatabaseTables::Message::TABLE +
								  " WHERE " + DatabaseTables::Message::TO_JID      + " NOT NULL " +
								  "   AND " + DatabaseTables::Message::CONTACT_DID + " IS NULL "
								  "   AND " + DatabaseTables::Message::TO_JID      + " NOT IN ( " + subquery2 + " ) " +
								  "   AND " + DatabaseTables::Message::DIRECTION   + " = 1 ";

		mXmppContactsMessagesOnlyRawQuery = selection1 + " UNION " + selection2;
	}

	return mXmppContactsMessagesOnlyRawQuery;
}


std::string DbManagerConstants::getAllContactsMessagesSummaryRawQuery()
{
	return getAllContactsMessagesSummaryRawQuery(true, true, true);
}

//static
//This is expected to handle normal, contact messages AND Group Messaging messages. Used only in Messages activity.
std::string DbManagerConstants::getAllContactsMessagesSummaryRawQuery( bool includeVoicemail, bool includeRecordedCalls, bool includeFaxes )
{
//	if ( mAllContactsMessagesSummaryRawQuery.empty() )
	{
		//Let's just create a StringList of fields we want and then generate the SELECT clause for easier maintenance and readability.
		//	The aliases used here are REQUIRED by DbManagerConstants::getMessageToPhoneNumberJoin()
		StringList projection;

		projection.push_back( "pn." + DatabaseTables::PhoneNumber::CONTACT_KEY	  );
		projection.push_back( "pn." + DatabaseTables::PhoneNumber::ID			  );
		projection.push_back( "pn." + DatabaseTables::PhoneNumber::CM_GROUP		  );
		projection.push_back( "pn." + DatabaseTables::PhoneNumber::CM_GROUP_NAME  );
		projection.push_back( "pn." + DatabaseTables::PhoneNumber::SOURCE		  );
		projection.push_back( "pn." + DatabaseTables::PhoneNumber::NUMBER		  );
		projection.push_back( "pn." + DatabaseTables::PhoneNumber::XMPP_JID		  );
//		projection.push_back( "pn." + DatabaseTables::PhoneNumber::NAME			  );	//pn.name is NOT used in wrappedCursor.  Instead, we use DISPLAY_NAME, which is in CONTACTS_MESSAGES_DISPLAY_NAME_SUB_QUERY subquery below.
		projection.push_back( "pn." + DatabaseTables::PhoneNumber::DISPLAY_NUMBER );

		projection.push_back( DbManagerConstants::getContactsMessagesDisplayNameSubquery() );

		projection.push_back( "m." + DatabaseTables::Message::SERVER_TIMESTAMP	);
		projection.push_back( "m." + DatabaseTables::Message::BODY				);
		projection.push_back( "m." + DatabaseTables::Message::TRANSLATED		);
		projection.push_back( "m." + DatabaseTables::Message::DIRECTION			);
		projection.push_back( "m." + DatabaseTables::Message::TYPE				);
		projection.push_back( "m." + DatabaseTables::Message::FROM_DID			);
		projection.push_back( "m." + DatabaseTables::Message::TO_DID			);
		projection.push_back( "m." + DatabaseTables::Message::FROM_JID			);
		projection.push_back( "m." + DatabaseTables::Message::TO_JID			);
		projection.push_back( "m." + DatabaseTables::Message::FROM_USER_ID		);
		projection.push_back( "m." + DatabaseTables::Message::TO_GROUP_ID		);

		projection.push_back( "g." + DatabaseTables::GroupMessageGroup::NAME	);
		projection.push_back( "g." + DatabaseTables::GroupMessageGroup::STATUS	);

		std::string projectionString = DbUtils::stringListToSelectClause( projection );

		//Here we define the WHERE, GROUP BY and ORDER BY clauses
		//	as it is a left join, we need to include only:
		//	- messages with a phone number (SMS or CHAT) where the message contact_did is NOT NULL
		//	OR
		//	- CHAT messages that don't have a phone number (message contact_did is NULL) but they have an entry in the phone number table so cm_group > 0.
		//	OR
		//	- Group Messages
		//	we do -NOT- display chat messages that don't have a valid phone number and that don't have an entry in the phone number table.
		std::string whereClause = " WHERE ( m." + DatabaseTables::Message::CONTACT_DID + " IS NOT NULL " + 
										  " OR pn." + DatabaseTables::PhoneNumber::CM_GROUP + " > 0 "    + 
										  " OR m."  + DatabaseTables::Message::FROM_USER_ID + " > 0 ) "  +
 								  " AND m." + DbManagerConstants::getNoDeletedMessagesCondition();		//Do not include DELETED messages

		if (includeVoicemail == false)
		{
			whereClause = whereClause + " AND m." + DatabaseTables::Message::TYPE + " !=  " + std::to_string(Message::VOICEMAIL) + " ";
		}

		if (includeRecordedCalls == false)
		{
			whereClause = whereClause + " AND m." + DatabaseTables::Message::TYPE + " !=  " + std::to_string(Message::RECORDCALLS) + " ";
		}

		if (includeFaxes == false)
		{
			whereClause = whereClause + " AND m." + DatabaseTables::Message::TYPE + " !=  " + std::to_string(Message::FAX) + " ";
		}

		std::string groupByClause = "GROUP BY CASE "
									"   WHEN m."  + DatabaseTables::Message::TO_GROUP_ID  + " IS NOT NULL THEN m."  + DatabaseTables::Message::TO_GROUP_ID  + " " +	//GM Groups do not have CM_GROUP.
									"   WHEN pn." + DatabaseTables::PhoneNumber::CM_GROUP + " IS NOT NULL THEN pn." + DatabaseTables::PhoneNumber::CM_GROUP + " " + //TODO-CM_GROUP: Do we need NUMBER? I think not since we do not need to distinguish IS_VOXOX == 1 vs. 2
									"   WHEN pn." + DatabaseTables::PhoneNumber::CM_GROUP + " IS     NULL THEN m."  + DatabaseTables::Message::CONTACT_DID  + "  " +
 									"END ";

		std::string orderByClause = "ORDER BY MAX(m." + DatabaseTables::Message::SERVER_TIMESTAMP + " ) DESC ";

		mAllContactsMessagesSummaryRawQuery = projectionString + DbManagerConstants::getMessageToPhoneNumberJoin() + whereClause + groupByClause + orderByClause;
	}

	return mAllContactsMessagesSummaryRawQuery;
}

std::string DbManagerConstants::getMessageToPhoneNumberJoin()
{
	if ( mMessageToPhoneNumberJoin.empty() )
	{
		mMessageToPhoneNumberJoin = "FROM " + DatabaseTables::Message::TABLE + " m " +
	  								"LEFT JOIN " + DatabaseTables::PhoneNumber::TABLE + " pn " + 
									"  ON ( m."  + DatabaseTables::Message::CONTACT_DID + " = pn." + DatabaseTables::PhoneNumber::NUMBER + " ) " +

								    //Join on Group Table
									 "LEFT JOIN " + DatabaseTables::GroupMessageGroup::TABLE + " g " +
									 "  ON   m." + DatabaseTables::Message::TO_GROUP_ID + " = g." + DatabaseTables::GroupMessageGroup::GROUP_ID + " ";
	}

	return mMessageToPhoneNumberJoin;
}

//static
std::string DbManagerConstants::getNoDeletedMessagesCondition()
{
	if ( mNoDeletedMessagesCondition.empty() )
	{
		mNoDeletedMessagesCondition = DatabaseTables::Message::STATUS + " != " + std::to_string( Message::DELETED ) + " ";
	}

	return mNoDeletedMessagesCondition;
}

//static
StringList DbManagerConstants::getGetMessagesProjection()
{
	if ( mGetMessagesProjection.size() == 0 )
	{
		mGetMessagesProjection.push_back( DatabaseTables::Message::ID				);

		mGetMessagesProjection.push_back( DatabaseTables::Message::DIRECTION		);
		mGetMessagesProjection.push_back( DatabaseTables::Message::TYPE				);
		mGetMessagesProjection.push_back( DatabaseTables::Message::STATUS			);
		mGetMessagesProjection.push_back( DatabaseTables::Message::MESSAGE_ID		);

		//SMS message
		mGetMessagesProjection.push_back( DatabaseTables::Message::FROM_DID			);
		mGetMessagesProjection.push_back( DatabaseTables::Message::TO_DID			);

		//Chat message
		mGetMessagesProjection.push_back( DatabaseTables::Message::FROM_JID			);
		mGetMessagesProjection.push_back( DatabaseTables::Message::TO_JID			);
		
		//Group Message
		mGetMessagesProjection.push_back( DatabaseTables::Message::TO_GROUP_ID		);
		mGetMessagesProjection.push_back( DatabaseTables::Message::FROM_USER_ID		);
		mGetMessagesProjection.push_back( DatabaseTables::Message::BODY				);
		mGetMessagesProjection.push_back( DatabaseTables::Message::TRANSLATED		);
		mGetMessagesProjection.push_back( DatabaseTables::Message::FILE_LOCAL		);
		mGetMessagesProjection.push_back( DatabaseTables::Message::THUMBNAIL_LOCAL	);
		mGetMessagesProjection.push_back( DatabaseTables::Message::DURATION			);

		mGetMessagesProjection.push_back( DatabaseTables::Message::LOCAL_TIMESTAMP		);
		mGetMessagesProjection.push_back( DatabaseTables::Message::SERVER_TIMESTAMP		);
		mGetMessagesProjection.push_back( DatabaseTables::Message::READ_TIMESTAMP		);
		mGetMessagesProjection.push_back( DatabaseTables::Message::DELIVERED_TIMESTAMP	);

		//These do not appear to be used (May be related to Voicemail				)
//		mGetMessagesProjection.push_back( DatabaseTables::Message::MODIFIED			);
//		mGetMessagesProjection.push_back( DatabaseTables::Message::IACCOUNT			);
//		mGetMessagesProjection.push_back( DatabaseTables::Message::CNAME			);
	}

	return mGetMessagesProjection;
}

//static
StringList DbManagerConstants::getGetMessagesForCmGroupProjection()
{
	if ( mGetMessagesForCmGroupProjection.size() == 0 )
	{
		//PhoneNumber table
		mGetMessagesForCmGroupProjection.push_back( "pn." + DatabaseTables::PhoneNumber::SOURCE	);
		mGetMessagesForCmGroupProjection.push_back( DatabaseTables::PhoneNumber::CM_GROUP	);
		mGetMessagesForCmGroupProjection.push_back( DatabaseTables::PhoneNumber::NUMBER 	);
		mGetMessagesForCmGroupProjection.push_back( DatabaseTables::PhoneNumber::IS_VOXOX 	);

		//Message table
		mGetMessagesForCmGroupProjection.push_back( "m." + DatabaseTables::Message::ID		);		//m. is to disambiguate _id

		mGetMessagesForCmGroupProjection.push_back( DatabaseTables::Message::DIRECTION		);
		mGetMessagesForCmGroupProjection.push_back( DatabaseTables::Message::TYPE			);
		mGetMessagesForCmGroupProjection.push_back( "m." + DatabaseTables::Message::STATUS	);		//m. to disambiguate Message and GMG.

		mGetMessagesForCmGroupProjection.push_back( DatabaseTables::Message::BODY			);
		mGetMessagesForCmGroupProjection.push_back( DatabaseTables::Message::TRANSLATED		);

		mGetMessagesForCmGroupProjection.push_back( DatabaseTables::Message::FROM_DID		);
		mGetMessagesForCmGroupProjection.push_back( DatabaseTables::Message::TO_DID			);

		mGetMessagesForCmGroupProjection.push_back( DatabaseTables::Message::FROM_JID		);
		mGetMessagesForCmGroupProjection.push_back( DatabaseTables::Message::TO_JID			);

		mGetMessagesForCmGroupProjection.push_back( DatabaseTables::Message::TO_GROUP_ID	);
		mGetMessagesForCmGroupProjection.push_back( DatabaseTables::Message::FROM_USER_ID	);

		mGetMessagesForCmGroupProjection.push_back( DatabaseTables::Message::DURATION		);
		mGetMessagesForCmGroupProjection.push_back( DatabaseTables::Message::FILE_LOCAL		);
		mGetMessagesForCmGroupProjection.push_back( DatabaseTables::Message::THUMBNAIL_LOCAL);

		mGetMessagesForCmGroupProjection.push_back( DatabaseTables::Message::LOCAL_TIMESTAMP		);
		mGetMessagesForCmGroupProjection.push_back( DatabaseTables::Message::SERVER_TIMESTAMP		);
		mGetMessagesForCmGroupProjection.push_back( DatabaseTables::Message::READ_TIMESTAMP		);
		mGetMessagesForCmGroupProjection.push_back( DatabaseTables::Message::DELIVERED_TIMESTAMP  );
		mGetMessagesForCmGroupProjection.push_back( DatabaseTables::Message::MESSAGE_ID			);
	}

	return mGetMessagesForCmGroupProjection;
}

//static
StringList DbManagerConstants::getGetMessagesForGroupIdProjection()
{
	if ( mGetMessagesForGroupIdProjection.size() == 0 )
	{
		//From DatabaseManagerConstants.GET_MESSAGES_PROJECTION2
		mGetMessagesForGroupIdProjection.push_back( "m." + DatabaseTables::Message::ID		);		//m. is to disambiguate _id

		mGetMessagesForGroupIdProjection.push_back( DatabaseTables::Message::DIRECTION		);
		mGetMessagesForGroupIdProjection.push_back( DatabaseTables::Message::TYPE				);
		mGetMessagesForGroupIdProjection.push_back( "m." + DatabaseTables::Message::STATUS	);		//m. to disambiguate Message and GMG.

		mGetMessagesForGroupIdProjection.push_back( DatabaseTables::Message::BODY				);
		mGetMessagesForGroupIdProjection.push_back( DatabaseTables::Message::TRANSLATED		);

		mGetMessagesForGroupIdProjection.push_back( DatabaseTables::Message::FROM_DID			);
		mGetMessagesForGroupIdProjection.push_back( DatabaseTables::Message::TO_DID			);

		mGetMessagesForGroupIdProjection.push_back( DatabaseTables::Message::FROM_JID			);
		mGetMessagesForGroupIdProjection.push_back( DatabaseTables::Message::TO_JID			);

		mGetMessagesForGroupIdProjection.push_back( DatabaseTables::Message::TO_GROUP_ID		);
		mGetMessagesForGroupIdProjection.push_back( DatabaseTables::Message::FROM_USER_ID		);

		mGetMessagesForGroupIdProjection.push_back( DatabaseTables::Message::DURATION			);
		mGetMessagesForGroupIdProjection.push_back( DatabaseTables::Message::FILE_LOCAL		);
		mGetMessagesForGroupIdProjection.push_back( DatabaseTables::Message::THUMBNAIL_LOCAL	);

		mGetMessagesForGroupIdProjection.push_back( DatabaseTables::Message::LOCAL_TIMESTAMP		);
		mGetMessagesForGroupIdProjection.push_back( DatabaseTables::Message::SERVER_TIMESTAMP		);
		mGetMessagesForGroupIdProjection.push_back( DatabaseTables::Message::READ_TIMESTAMP		);
		mGetMessagesForGroupIdProjection.push_back( DatabaseTables::Message::DELIVERED_TIMESTAMP  );
		mGetMessagesForGroupIdProjection.push_back( DatabaseTables::Message::MESSAGE_ID			);

		//From PN table
		mGetMessagesForGroupIdProjection.push_back( "pn." + DatabaseTables::PhoneNumber::NAME		);		//pn. to disambiguate PhoneNumber and GMG.

		//GroupMessageMember table
		mGetMessagesForGroupIdProjection.push_back( DatabaseTables::GroupMessageMember::NICKNAME  );
		mGetMessagesForGroupIdProjection.push_back( DatabaseTables::GroupMessageMember::COLOR     );
	}

	return mGetMessagesForGroupIdProjection;
}

//static
StringList DbManagerConstants::getGetPhoneNumberProjection()
{
	if ( mGetPhoneNumberProjection.empty() )
	{
		mGetPhoneNumberProjection.push_back( DatabaseTables::PhoneNumber::ID			);
		mGetPhoneNumberProjection.push_back( DatabaseTables::PhoneNumber::SOURCE		);
		mGetPhoneNumberProjection.push_back( DatabaseTables::PhoneNumber::CM_GROUP		);
		mGetPhoneNumberProjection.push_back( DatabaseTables::PhoneNumber::NAME			);
		mGetPhoneNumberProjection.push_back( DatabaseTables::PhoneNumber::NUMBER		);
		mGetPhoneNumberProjection.push_back( DatabaseTables::PhoneNumber::DISPLAY_NUMBER);
		mGetPhoneNumberProjection.push_back( DatabaseTables::PhoneNumber::LABEL			);
		mGetPhoneNumberProjection.push_back( DatabaseTables::PhoneNumber::CM_GROUP_NAME );
		mGetPhoneNumberProjection.push_back( DatabaseTables::PhoneNumber::XMPP_JID		);
		mGetPhoneNumberProjection.push_back( DatabaseTables::PhoneNumber::XMPP_STATUS	);
		mGetPhoneNumberProjection.push_back( DatabaseTables::PhoneNumber::XMPP_PRESENCE	);
		mGetPhoneNumberProjection.push_back( DatabaseTables::PhoneNumber::XMPP_GROUP	);
		mGetPhoneNumberProjection.push_back( DatabaseTables::PhoneNumber::IS_VOXOX		);
		mGetPhoneNumberProjection.push_back( DatabaseTables::PhoneNumber::IS_FAVORITE	);

		//New Contact-Sync
		mGetPhoneNumberProjection.push_back( DatabaseTables::PhoneNumber::CONTACT_KEY	);
		mGetPhoneNumberProjection.push_back( DatabaseTables::PhoneNumber::FIRST_NAME	);
		mGetPhoneNumberProjection.push_back( DatabaseTables::PhoneNumber::LAST_NAME		);
		mGetPhoneNumberProjection.push_back( DatabaseTables::PhoneNumber::COMPANY		);
		mGetPhoneNumberProjection.push_back( DatabaseTables::PhoneNumber::USER_ID		);
		mGetPhoneNumberProjection.push_back( DatabaseTables::PhoneNumber::USER_NAME		);
		mGetPhoneNumberProjection.push_back( DatabaseTables::PhoneNumber::IS_BLOCKED	);
		mGetPhoneNumberProjection.push_back( DatabaseTables::PhoneNumber::IS_EXT		);
	}

	return mGetPhoneNumberProjection;
}

//static
StringList DbManagerConstants::getMessageTranslationPropertyProjection()
{
	if ( mMessageTranslationPropertyProjection.empty() )
	{
		mMessageTranslationPropertyProjection.push_back( DatabaseTables::Translation::ID				);
		mMessageTranslationPropertyProjection.push_back( DatabaseTables::Translation::ENABLED			);
		mMessageTranslationPropertyProjection.push_back( DatabaseTables::Translation::USER_LANGUAGE		);
		mMessageTranslationPropertyProjection.push_back( DatabaseTables::Translation::CONTACT_LANGUAGE	);
		mMessageTranslationPropertyProjection.push_back( DatabaseTables::Translation::PHONE_NUMBER		);
		mMessageTranslationPropertyProjection.push_back( DatabaseTables::Translation::JID				);
		mMessageTranslationPropertyProjection.push_back( DatabaseTables::Translation::CONTACT_ID		);
		mMessageTranslationPropertyProjection.push_back( DatabaseTables::Translation::TRANSLATE_MESSAGE_DIRECTION	);
	}

	return mMessageTranslationPropertyProjection;
}

//Contact summary info - This contains on SUMMARY PhoneNumber info, such as counts.
std::string DbManagerConstants::getContactSelect()
{
	if ( mContactSelect.empty() )
	{
		mContactSelect += "SELECT ";
		mContactSelect += "1 AS " + DatabaseTables::Contact::TYPE 		+ ", ";
		mContactSelect += DatabaseTables::PhoneNumber::ID 				+ ", ";
		mContactSelect += DatabaseTables::PhoneNumber::SOURCE	 		+ ", ";
		mContactSelect += DatabaseTables::PhoneNumber::CM_GROUP			+ ", ";
		mContactSelect += DatabaseTables::PhoneNumber::DISPLAY_NUMBER 	+ ", ";
		mContactSelect += DatabaseTables::PhoneNumber::NAME 			+ ", ";
		mContactSelect += DatabaseTables::PhoneNumber::CM_GROUP_NAME	+ ", ";

		mContactSelect += "NULL AS " + DatabaseTables::GroupMessageGroup::GROUP_ID + ", ";

		mContactSelect += DbManagerConstants::getVoxoxCountSubquery() 			+ ", ";
		mContactSelect += DbManagerConstants::getBlockedCountSubquery() 		+ ", ";
		mContactSelect += DbManagerConstants::getFavoriteCountSubquery() 		+ ", ";
		mContactSelect += DbManagerConstants::getPhoneNumberCountSubquery() 	+ ", ";
		mContactSelect += DbManagerConstants::getExtensionCountSubquery() 		+ ", ";

		//New Contact Syncing
		mContactSelect += DatabaseTables::PhoneNumber::CONTACT_KEY		+ ", ";
		mContactSelect += DatabaseTables::PhoneNumber::FIRST_NAME		+ ", ";
		mContactSelect += DatabaseTables::PhoneNumber::LAST_NAME		+ ", ";
		mContactSelect += DatabaseTables::PhoneNumber::COMPANY			+ ", ";

		mContactSelect += "FROM " + DatabaseTables::PhoneNumber::TABLE + " pn ";
	}

	return mContactSelect;
}

//NOTE: This should have same structure as GROUP_MESSAGE_GROUP_AS_CONTACT_SELECT_LONG.
//	You must change both.
//static 
std::string DbManagerConstants::getPhoneNumberAsContactSelectLong()
{
	if ( mPhoneNumberAsContactSelectLong.empty() )
	{
		mPhoneNumberAsContactSelectLong += "SELECT ";
		mPhoneNumberAsContactSelectLong += "1 AS " + DatabaseTables::Contact::TYPE 		+ ", ";
		mPhoneNumberAsContactSelectLong += DatabaseTables::PhoneNumber::ID 				+ ", ";
		mPhoneNumberAsContactSelectLong += DatabaseTables::PhoneNumber::SOURCE	 		+ ", ";
		mPhoneNumberAsContactSelectLong += DatabaseTables::PhoneNumber::CM_GROUP		+ ", ";

		//TODO: These are used in getContactPhoneNumbers() PhoneNumberList and we need to normalize this so that ContactList does NOT need them
		mPhoneNumberAsContactSelectLong += DatabaseTables::PhoneNumber::DISPLAY_NUMBER 	+ ", ";		
		mPhoneNumberAsContactSelectLong += DatabaseTables::PhoneNumber::IS_FAVORITE 	+ ", ";
		mPhoneNumberAsContactSelectLong += DatabaseTables::PhoneNumber::IS_VOXOX 		+ ", ";
		mPhoneNumberAsContactSelectLong += DatabaseTables::PhoneNumber::NUMBER 			+ ", ";
		mPhoneNumberAsContactSelectLong += DatabaseTables::PhoneNumber::LABEL			+ ", ";

		mPhoneNumberAsContactSelectLong += DatabaseTables::PhoneNumber::NAME 			+ ", ";
		mPhoneNumberAsContactSelectLong += DatabaseTables::PhoneNumber::CM_GROUP_NAME 	+ ", ";

		mPhoneNumberAsContactSelectLong += "NULL AS " + DatabaseTables::GroupMessageGroup::GROUP_ID + ", ";

		mPhoneNumberAsContactSelectLong += DatabaseTables::PhoneNumber::XMPP_JID 		+ ", ";
		mPhoneNumberAsContactSelectLong += DatabaseTables::PhoneNumber::XMPP_STATUS 	+ ", ";
		mPhoneNumberAsContactSelectLong += DatabaseTables::PhoneNumber::XMPP_PRESENCE 	+ ", ";
		mPhoneNumberAsContactSelectLong += DatabaseTables::PhoneNumber::XMPP_GROUP	 	+ ", ";

		mPhoneNumberAsContactSelectLong += DbManagerConstants::getVoxoxCountSubquery() 			+ ", ";
		mPhoneNumberAsContactSelectLong += DbManagerConstants::getBlockedCountSubquery() 		+ ", ";
		mPhoneNumberAsContactSelectLong += DbManagerConstants::getFavoriteCountSubquery() 		+ ", ";
		mPhoneNumberAsContactSelectLong += DbManagerConstants::getPhoneNumberCountSubquery() 	+ ", ";
		mPhoneNumberAsContactSelectLong += DbManagerConstants::getExtensionCountSubquery() 		+ ", ";

		mPhoneNumberAsContactSelectLong += DbManagerConstants::getExtensionSubquery() 			+ ", ";
		mPhoneNumberAsContactSelectLong += DbManagerConstants::getBestPhoneNumberSubquery() 	+ ", ";

		//New Contact Syncing
		mPhoneNumberAsContactSelectLong += DatabaseTables::PhoneNumber::CONTACT_KEY		+ ", ";
		mPhoneNumberAsContactSelectLong += DatabaseTables::PhoneNumber::FIRST_NAME		+ ", ";
		mPhoneNumberAsContactSelectLong += DatabaseTables::PhoneNumber::LAST_NAME		+ ", ";
		mPhoneNumberAsContactSelectLong += DatabaseTables::PhoneNumber::COMPANY			+ ", ";
		mPhoneNumberAsContactSelectLong += DatabaseTables::PhoneNumber::USER_ID			+ ", ";
		mPhoneNumberAsContactSelectLong += DatabaseTables::PhoneNumber::USER_NAME		+ ", ";
		mPhoneNumberAsContactSelectLong += DatabaseTables::PhoneNumber::IS_BLOCKED		+ ", ";
		mPhoneNumberAsContactSelectLong += DatabaseTables::PhoneNumber::IS_EXT			+ " ";

		mPhoneNumberAsContactSelectLong += "FROM " + DatabaseTables::PhoneNumber::TABLE + " pn ";
	}

	return mPhoneNumberAsContactSelectLong;
}

//static
std::string	DbManagerConstants::getPhoneNumberAsContactSelectShort()
{
	if ( mPhoneNumberAsContactSelectShort.empty() )
	{
		mPhoneNumberAsContactSelectShort += "SELECT ";
		mPhoneNumberAsContactSelectShort += "1 AS " + DatabaseTables::Contact::TYPE 	+ ", ";
		mPhoneNumberAsContactSelectShort += DatabaseTables::PhoneNumber::ID 			+ ", ";
		mPhoneNumberAsContactSelectShort += DatabaseTables::PhoneNumber::SOURCE		 	+ ", ";
		mPhoneNumberAsContactSelectShort += DatabaseTables::PhoneNumber::DISPLAY_NUMBER + ", ";
		mPhoneNumberAsContactSelectShort += DatabaseTables::PhoneNumber::IS_FAVORITE 	+ ", ";
		mPhoneNumberAsContactSelectShort += DatabaseTables::PhoneNumber::IS_VOXOX 		+ ", ";
		mPhoneNumberAsContactSelectShort += DatabaseTables::PhoneNumber::NAME 			+ ", ";
		mPhoneNumberAsContactSelectShort += DatabaseTables::PhoneNumber::NUMBER 		+ ", "; 	//TODO: Does this work?  Different name in UNION with GMG?
		mPhoneNumberAsContactSelectShort += DatabaseTables::PhoneNumber::LABEL 			+ ", " ;
		mPhoneNumberAsContactSelectShort += DatabaseTables::PhoneNumber::XMPP_JID		+ ", " ;

		//New Contact Syncing
		mPhoneNumberAsContactSelectShort += DatabaseTables::PhoneNumber::CONTACT_KEY	+ ", ";
		mPhoneNumberAsContactSelectShort += DatabaseTables::PhoneNumber::CM_GROUP		+ ", ";
		mPhoneNumberAsContactSelectShort += DatabaseTables::PhoneNumber::FIRST_NAME		+ ", ";
		mPhoneNumberAsContactSelectShort += DatabaseTables::PhoneNumber::LAST_NAME		+ ", ";
		mPhoneNumberAsContactSelectShort += DatabaseTables::PhoneNumber::COMPANY		+ ", ";
		mPhoneNumberAsContactSelectShort += DatabaseTables::PhoneNumber::USER_ID		+ ", ";
		mPhoneNumberAsContactSelectShort += DatabaseTables::PhoneNumber::USER_NAME		+ ", ";
		mPhoneNumberAsContactSelectShort += DatabaseTables::PhoneNumber::IS_BLOCKED		+ ", ";
		mPhoneNumberAsContactSelectShort += DatabaseTables::PhoneNumber::IS_EXT			+ " ";

		mPhoneNumberAsContactSelectShort += "FROM " + DatabaseTables::PhoneNumber::TABLE + " ";
	}

	return mPhoneNumberAsContactSelectShort;
}

//NOTE: This should have same structure as PHONE_NUMBER_AS_CONTACT_SELECT_LONG.
//	You must change both.
//static 
//JRT - 2015.10.08 - We are not supporting GroupMessaging yet.  This will have to be updated when we do.
//std::string DbManagerConstants::getGroupMessageGroupAsContactSelectLong()
//{
//	if ( mGroupMessageGroupAsContactSelectLong.empty() )
//	{
//		mGroupMessageGroupAsContactSelectLong += "SELECT " +
//		mGroupMessageGroupAsContactSelectLong += "   2 AS " + DatabaseTables::Misc::TYPE 					+ ", ";
//		mGroupMessageGroupAsContactSelectLong += "  -1 AS " + DatabaseTables::PhoneNumber::ID 				+ ", ";
//		mGroupMessageGroupAsContactSelectLong += "  -1 AS " + DatabaseTables::PhoneNumber::CM_GROUP			+ ", ";
//		mGroupMessageGroupAsContactSelectLong += "NULL AS " + DatabaseTables::PhoneNumber::DISPLAY_NUMBER 	+ ", ";
//		mGroupMessageGroupAsContactSelectLong += "   0 AS " + DatabaseTables::PhoneNumber::IS_FAVORITE 		+ ", ";
//		mGroupMessageGroupAsContactSelectLong += "   0 AS " + DatabaseTables::PhoneNumber::IS_VOXOX 		+ ", ";
//		mGroupMessageGroupAsContactSelectLong += "NULL AS " + DatabaseTables::PhoneNumber::NUMBER 			+ ", ";			//Used in 1 of 3 places
//		mGroupMessageGroupAsContactSelectLong += "NULL AS " + DatabaseTables::PhoneNumber::LABEL			+ ", ";
//		mGroupMessageGroupAsContactSelectLong += "NULL AS " + DatabaseTables::PhoneNumber::XMPP_JID			+ ", ";
//
//		mGroupMessageGroupAsContactSelectLong += DatabaseTables::GroupMessageGroup::NAME					+ ", ";
//		mGroupMessageGroupAsContactSelectLong += DatabaseTables::PhoneNumber::GROUP_ID						+ ", ";			//This is 40-char string.
//
//		mGroupMessageGroupAsContactSelectLong += "NULL AS " + DatabaseTables::PhoneNumber::XMPP_JID 		+ ", ";
//		mGroupMessageGroupAsContactSelectLong += "NULL AS " + DatabaseTables::PhoneNumber::XMPP_STATUS 		+ ", ";
//		mGroupMessageGroupAsContactSelectLong += "NULL AS " + DatabaseTables::PhoneNumber::XMPP_PRESENCE 	+ ", ";
//		mGroupMessageGroupAsContactSelectLong += "NULL AS " + DatabaseTables::PhoneNumber::XMPP_GROUP	 	+ ", ";			//JRT- Added for consistency.
//
//		mGroupMessageGroupAsContactSelectLong += "   0 AS " + DatabaseTables::PhoneNumber::VOXOX_COUNT		+ " ";	 		//Used in 2 of 3 places
//
//		mGroupMessageGroupAsContactSelectLong += "FROM " + DatabaseTables::GroupMessageGroup::TABLE + " g ";
//	}
//
//	return mGroupMessageGroupAsContactSelectLong;
//}

//static
//JRT - 2015.10.08 - We do not have GroupMessaging yet.  This will have be updated when/if we do.
//std::string	DbManagerConstants::getGroupMessageGroupAsContactSelectShort()
//{
//	if ( mGroupMessageGroupAsContactSelectShort.empty() )
//	{
//		mGroupMessageGroupAsContactSelectShort += "SELECT ";
//		mGroupMessageGroupAsContactSelectShort += "   2 AS " + DatabaseTables::Misc::TYPE 					+ ", ";
//		mGroupMessageGroupAsContactSelectShort += "  -1 AS " + DatabaseTables::PhoneNumber::ID 				+ ", ";
//		mGroupMessageGroupAsContactSelectShort += "NULL AS " + DatabaseTables::PhoneNumber::DISPLAY_NUMBER 	+ ", ";
//		mGroupMessageGroupAsContactSelectShort += "   0 AS " + DatabaseTables::PhoneNumber::IS_FAVORITE 	+ ", ";
//		mGroupMessageGroupAsContactSelectShort += "   0 AS " + DatabaseTables::PhoneNumber::IS_VOXOX 		+ ", ";
//		mGroupMessageGroupAsContactSelectShort += " gmg."	 + DatabaseTables::GroupMessageGroup::NAME		+ ", ";
//		mGroupMessageGroupAsContactSelectShort += " gmg."	 + DatabaseTables::PhoneNumber::GROUP_ID		+ ", ";		//This is 40-char string.
//		mGroupMessageGroupAsContactSelectShort += "NULL AS " + DatabaseTables::PhoneNumber::LABEL 			+ " " ;
//
//		mGroupMessageGroupAsContactSelectShort += "FROM " + DatabaseTables::GroupMessageGroup::TABLE 		+ " gmg ";
//	}
//
//	return mGroupMessageGroupAsContactSelectShort;
//}

StringList DbManagerConstants::getGetRecentCallProjection()
{
	if ( mGetRecentCallProjection.size() == 0 )
	{
		mGetRecentCallProjection.push_back( "rc." + DatabaseTables::RecentCall::ID		);	//Disambiguate in main query.
		mGetRecentCallProjection.push_back( DatabaseTables::RecentCall::STATUS			);
		mGetRecentCallProjection.push_back( DatabaseTables::RecentCall::INCOMING		);
		mGetRecentCallProjection.push_back( DatabaseTables::RecentCall::TIMESTAMP		);
		mGetRecentCallProjection.push_back( DatabaseTables::RecentCall::START_TIMESTAMP	);
		mGetRecentCallProjection.push_back( DatabaseTables::RecentCall::DURATION		);
		mGetRecentCallProjection.push_back( DatabaseTables::RecentCall::IS_LOCAL        );
		mGetRecentCallProjection.push_back( DatabaseTables::RecentCall::ORIG_PHONE_NUMBER );
		mGetRecentCallProjection.push_back( DatabaseTables::RecentCall::PHONE_NUMBER	);		
		mGetRecentCallProjection.push_back( DatabaseTables::RecentCall::SEEN            );
		mGetRecentCallProjection.push_back( DatabaseTables::RecentCall::UID             );

		//PhoneNumber table
//		mGetRecentCallProjection.push_back( DatabaseTables::PhoneNumber::DISPLAY_NAME	);
		mGetRecentCallProjection.push_back( DatabaseTables::PhoneNumber::NAME			);
		mGetRecentCallProjection.push_back( DatabaseTables::PhoneNumber::DISPLAY_NUMBER	);
		mGetRecentCallProjection.push_back( DatabaseTables::PhoneNumber::CONTACT_KEY	);
		mGetRecentCallProjection.push_back( DatabaseTables::PhoneNumber::CM_GROUP		);
		mGetRecentCallProjection.push_back( DatabaseTables::PhoneNumber::CM_GROUP_NAME	);
	}

	return mGetRecentCallProjection;
}

std::string DbManagerConstants::getRecentCallRawQuery( const std::string& whereClauseIn )
{
//	if ( mRecentCallRawQuery.empty() )
	{
		std::string projectionString = DbUtils::stringListToSelectClause( getGetRecentCallProjection() );

		std::string fromClause  = " FROM "      + DatabaseTables::RecentCall::TABLE + " rc " +
								  " LEFT JOIN " + DatabaseTables::PhoneNumber::TABLE +" pn " + 
								  "        ON rc." + DatabaseTables::RecentCall::PHONE_NUMBER + " = pn." + DatabaseTables::PhoneNumber::NUMBER +
								  " " + whereClauseIn +
								  " GROUP BY rc." + DatabaseTables::RecentCall::ID;		//Removes dupes in case phone number exists more than one in PhoneNumber table.


		mRecentCallRawQuery = projectionString + fromClause;
	}

	return mRecentCallRawQuery;
}

StringList DbManagerConstants::getGetSettingsProjection()
{
	if ( mGetSettingsProjection.size() == 0 )
	{
		mGetSettingsProjection.push_back( DatabaseTables::Settings::USER_ID							);

		mGetSettingsProjection.push_back( DatabaseTables::Settings::AUDIO_INPUT_DEVICE				);
		mGetSettingsProjection.push_back( DatabaseTables::Settings::AUDIO_OUTPUT_DEVICE				);
		mGetSettingsProjection.push_back( DatabaseTables::Settings::AUDIO_INPUT_VOLUME				);
		mGetSettingsProjection.push_back( DatabaseTables::Settings::AUDIO_OUTPUT_VOLUME				);

		mGetSettingsProjection.push_back( DatabaseTables::Settings::SHOW_FAX_IN_MSG_THREAD			);
		mGetSettingsProjection.push_back( DatabaseTables::Settings::SHOW_VM_IN_MSG_THREAD			);
		mGetSettingsProjection.push_back( DatabaseTables::Settings::SHOW_RC_IN_MSG_THREAD			);

		mGetSettingsProjection.push_back( DatabaseTables::Settings::ENABLE_SOUND_INCOMING_MSG		);
		mGetSettingsProjection.push_back( DatabaseTables::Settings::ENABLE_SOUND_INCOMING_CALL		);
//		mGetSettingsProjection.push_back( DatabaseTables::Settings::ENABLE_SOUND_OUTGOING_MSG		);

		mGetSettingsProjection.push_back( DatabaseTables::Settings::MY_CHAT_BUBBLE_COLOR			);
		mGetSettingsProjection.push_back( DatabaseTables::Settings::CONVERSANT_CHAT_BUBBLE_COLOR	);

		mGetSettingsProjection.push_back( DatabaseTables::Settings::COUNTRY_CODE					);
		mGetSettingsProjection.push_back( DatabaseTables::Settings::COUNTRY_ABBREV					);

//		mGetSettingsProjection.push_back( DatabaseTables::Settings::OUTGOING_CALL_SOUND_FILE		);
		mGetSettingsProjection.push_back( DatabaseTables::Settings::INCOMING_CALL_SOUND_FILE		);
		mGetSettingsProjection.push_back( DatabaseTables::Settings::CALL_CLOSED_SOUND_FILE			);

		mGetSettingsProjection.push_back( DatabaseTables::Settings::WINDOW_LEFT						);
		mGetSettingsProjection.push_back( DatabaseTables::Settings::WINDOW_TOP						);
		mGetSettingsProjection.push_back( DatabaseTables::Settings::WINDOW_HEIGHT					);
		mGetSettingsProjection.push_back( DatabaseTables::Settings::WINDOW_WIDTH					);
		mGetSettingsProjection.push_back( DatabaseTables::Settings::WINDOW_STATE					);

		mGetSettingsProjection.push_back( DatabaseTables::Settings::LAST_MSG_TIMESTAMP				);
		mGetSettingsProjection.push_back( DatabaseTables::Settings::LAST_CALL_TIMESTAMP				);
		mGetSettingsProjection.push_back( DatabaseTables::Settings::LAST_CONTACT_SOURCE_KEY			);

		mGetSettingsProjection.push_back( DatabaseTables::Settings::SIP_UUID						);
	}

	return mGetSettingsProjection;
}

StringList DbManagerConstants::getGetAppSettingsProjection()
{
	if ( mGetAppSettingsProjection.size() == 0 )
	{
		mGetAppSettingsProjection.push_back( "*");	//Test if this works
	}

	return mGetAppSettingsProjection;
}

std::string DbManagerConstants::getSourceInClause()
{
	if ( mSourceInClause.empty() )
	{
		mSourceInClause = DatabaseTables::PhoneNumber::SOURCE + " IN ( 'AB', 'CD' ) ";
	}

	return mSourceInClause;
}