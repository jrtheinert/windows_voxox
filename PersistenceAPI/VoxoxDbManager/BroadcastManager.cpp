/* 
* Voxox 
* Copyright (C) 2004-2014 Voxox 
*/

/*
* Small class to broadcast DB change events to registered listeners.
*/

#include "BroadcastManager.h"

//=============================================================================

BroadcastManager::BroadcastManager() : mCallback( nullptr )
{
}

BroadcastManager::~BroadcastManager()
{
}

void BroadcastManager::setCallback( DbChangeCallbackFunc func )
{
	mCallback = func;
}

//Convenience methods to trigger events.
void BroadcastManager::msgChange()
{
	DbChangeData data( DbChangeData::Table::MESSAGE );

	broadcast( data );
}

void BroadcastManager::msgAdded( size_t count )
{
	DbChangeData data( DbChangeData::Table::MESSAGE );
	data.setMsgCount( count );

	broadcast( data );
}

void BroadcastManager::msgStatusChange( Message::Status status )
{
	DbChangeData data( DbChangeData::Table::MESSAGE );
	data.setMsgStatus( status );

	broadcast( data );
}

void BroadcastManager::contactChange( int voxoxUserId )
{
	DbChangeData data( DbChangeData::Table::CONTACT );
	data.setVoxoxId( voxoxUserId );

	broadcast( data );
}

void BroadcastManager::gmChange()
{
	DbChangeData data( DbChangeData::Table::GM );

	broadcast( data );
}

void BroadcastManager::gmChange( const std::string& groupId, DbChangeData::Action action )
{
	DbChangeData data( DbChangeData::Table::GM );
	data.setGroupId ( groupId );
	data.setGmAction( action  );

	broadcast( data );
}

void BroadcastManager::recentCallChange()
{
	DbChangeData data( DbChangeData::Table::RECENT_CALL );

	broadcast( data );
}

//-----------------------------------------------------------------------------
//The real work starts here
//-----------------------------------------------------------------------------
 void BroadcastManager::broadcast( const DbChangeData& data )
{
	if ( mCallback != nullptr )
	{
		mCallback(data);
	}
}
