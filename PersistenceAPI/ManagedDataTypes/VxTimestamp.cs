﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Mar 2016
 * 
 * The goal within the entire project is to use ONLY UTC internally.
 * UTC times will be converted to LocalTime, based on TimeZone,
 * for:
 *	- display to user
 *	- To PST for API call timestamps, at least until server goes to UTC.
 *	
 * Each 'timestamp' value will be FileTime in UTC.  It will be 'modified' to represent
 * milliseconds, rather than 10-nanosecond blocks.  (We may review this).
 */

using System;

namespace ManagedDataTypes
{
	public class VxTimestamp
	{
        static private readonly Int64  FILETIME_MODIFIER			= 10000;					//FileTime is in 10-nano blocks, so divide by 10,000 to get milliseconds.
        static private readonly String SERVER_TIMEZONE				= "Pacific Standard Time";	//TZ currently used by server.
        static private readonly String REST_SERVER_TIMESTAMP_FORMAT = "yyyy-MM-dd HH:mm:ss";
		
		static private Boolean mServerUsesUtc = false;			//TODO: Set this based on Desktop.Config value

		private Boolean  mFromUnix;
		private Int64	 mTimeStamp;			//UTC
		private Int64	 mUnixTimeStamp;
		private String	 mDateTimeString;

		private DateTime mUtcTime;
		private DateTime mLocalTime;
		private DateTime mServerTime;

		public VxTimestamp( Int64 ts  )
		{
			initFromLong( ts, false );
		}

		public VxTimestamp( Int64 ts, bool fromUnix )
		{
			initFromLong( ts, fromUnix );
		}

		public VxTimestamp( String dtString )
		{
			mDateTimeString = dtString;
			mTimeStamp		= ConvertServerDateTimeString( dtString );

			initVars();
		}
		
		public void initFromLong( Int64 ts, Boolean fromUnix )
		{
			mFromUnix = fromUnix;

			if ( fromUnix )
			{
				mUnixTimeStamp = ts;
				mTimeStamp     = ConvertUnixTimestampToServerTimestamp( ts );
			}
			else 
			{
				mTimeStamp = ts;
			}

			initVars();
		}

		private void initVars()
		{
			mUtcTime    = ToUtcTime   ( mTimeStamp );	//Must come before ToServerTime() call.
			mLocalTime  = ToLocalTime ( mTimeStamp );
			mServerTime = ToServerTime( mTimeStamp );
		}

		public Int64 AsLong()
		{
			return mTimeStamp;
		}

		public DateTime AsLocalTime()
		{
			return mLocalTime;
		}

		public DateTime AsUtcTime()
		{
			return mUtcTime;
		}

		public DateTime AsServerTime()
		{
			return mServerTime;
		}


		#region Private methods

		//This STRING comes from API call(s).
        private Int64 ConvertServerDateTimeString( String serverString )
        {
            Int64 result = 0;

            DateTime dtTemp = Convert.ToDateTime( serverString );
			DateTime dt;
			Int64 fileTime  = 0;

			if ( mServerUsesUtc )
			{
				dt		 = dtTemp;
				fileTime = dt.ToFileTime();
			}
			else
			{
				dt		  = TimeZoneInfo.ConvertTimeToUtc( dtTemp, getServerTimeZone() );
				fileTime  = dt.ToFileTime();
			}

            result =  modifyFileTime( fileTime );

            return result;
        }

		//Currently, used in one location to convert API response 'epochStart'.  Per discussion, this should ALREADY be UTC.
		//This 'unixTimestamp' comes from API calls.
        private Int64 ConvertUnixTimestampToServerTimestamp( Int64 unixTimestamp )
        {
            DateTime epochDate = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            DateTime unixDate  = epochDate.AddSeconds(unixTimestamp);

            DateTime utcDate   = DateTime.SpecifyKind(unixDate, DateTimeKind.Utc);
            Int64    result    = GetTimestamp( utcDate );

//			VxTimestamp ts = new VxTimestamp( result );	//Debug

			return result;
        }

		private DateTime ToUtcTime( Int64 timestamp )
        {
			DateTime result  = DateTime.FromFileTimeUtc( unmodifyFileTime( timestamp ) );
			return result;
        }

        private DateTime ToLocalTime( Int64 timestamp )
        {
			DateTime value  = DateTime.FromFileTimeUtc( unmodifyFileTime( timestamp ) );
			DateTime result = TimeZoneInfo.ConvertTime( value, TimeZoneInfo.Local );
			return result;
        }

        private DateTime ToServerTime( Int64 timestamp )
        {
			DateTime result = TimeZoneInfo.ConvertTimeFromUtc( mUtcTime, getServerTimeZone());
			return result;
        }
	
		private TimeZoneInfo getServerTimeZone()
		{ 
			TimeZoneInfo result = TimeZoneInfo.FindSystemTimeZoneById(SERVER_TIMEZONE);
			return result;
		}

		#endregion

		#region Static methods

        public static Int64 GetNowTimestamp()
        {
            DateTime dt		  = DateTime.UtcNow;
            Int64	 fileTime = dt.ToFileTime();
            Int64	 result   = modifyFileTime( fileTime );
			
//			VxTimestamp ts = new VxTimestamp( result );		//Debug

            return result;
        }

		//Used once in MessageManager, and internal to this class.
		public static Int64 GetTimestamp( DateTime dt )
        {
            Int64 fileTime = dt.ToFileTime();
            Int64 result   = modifyFileTime( fileTime );

            return result;
        }

		//getCallHistory() and getMessageHistory()
		public static Int64 GetServerTimestampSomeDaysBack( int daysBack )
		{
			//Ensure 'daysBack' is negative.
			daysBack = (daysBack > 0 ? daysBack * -1 : daysBack );

            DateTime dt1    = DateTime.UtcNow;
			DateTime dt2    = dt1.AddDays( daysBack );
			Int64    result = GetTimestamp( dt2 );

//			VxTimestamp ts = new VxTimestamp( result );		//Debug

			return result;
		}

		//getCallHistory() and getMessageHistory()
        public static string GetFormattedTimestampForREST( Int64 timestamp )
        {
			VxTimestamp ts = new VxTimestamp( timestamp );
			String result = ts.AsServerTime().ToString( REST_SERVER_TIMESTAMP_FORMAT );
			return result;
        }

		//OK
		private static Int64 modifyFileTime( Int64 ft )
		{ 
			return ft / FILETIME_MODIFIER;
		}

		//OK
		private static Int64 unmodifyFileTime( Int64 ft )
		{ 
			return ft * FILETIME_MODIFIER;
		}

		#endregion
	}
}
