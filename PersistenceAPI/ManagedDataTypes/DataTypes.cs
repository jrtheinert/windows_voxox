﻿
using System;
using System.Collections.Generic;	//List

namespace ManagedDataTypes
{
	//NOTE: Do not re-order this because it will affect DB message entries.  Feel free to APPEND new values
    //Defined in unmanaged class, so we must match it here.
	//We make these top-level because XAML does not support nested types, e.g. PhoneNumber.ContactFilter.
	public enum ContactFilter
	{
    	All		  = 0,
    	Favorite  = 1,
    	InNetwork = 2,
    	Groups	  = 3,
    };

public static class Localization
{
	//Some data elements (Name, DisplayName) may be compound (Bob or Tom).  From PAPI, the 'or' is represented by '|||'.
	//	Since 'or' may need to be localized, this small class will handle that.  
	//	It is expected that main app will call setOrPlaceholder() method upon initialization so we get localized 'or' text.
	private static readonly string mNameSep	      = "|||";		//This matches PAPI value	//TODO: Get value from PAPI
	private static string          mGenericOrText = " or ";		//This will be overridden by main app.

	public static void setOrText( string value )
	{
		 mGenericOrText = " " + value.Trim() + " ";
	}

	public static string replaceOrPlaceholder( string val )
	{
		string result = val.Replace( mNameSep, mGenericOrText );
		return result;
	}
}

//=============================================================================
// Generic StringList class - Essentially a typedef.
//=============================================================================

public class StringList : System.Collections.Generic.List<String>
{
//	StringList()		{}
//	~StringList()		{}
};

//=============================================================================


//=============================================================================
// Contact class
//	- Used to populate CONTACT LIST tab
//=============================================================================

public class Contact
{
	private string name;
	private string displayName;
	private string cmGroupName;

	//NOTE: Do not re-order this because it will affect DB message entries.  Feel free to APPEND new values
    //Defined in unmanaged class, so we must match it here.
	public enum ContactType
	{
		TYPE_UNKNOWN	  = 0,
		TYPE_PHONE_NUMBER = 1,
		TYPE_GROUP		  = 2
	};

	public enum ContactSource
	{
		TYPE_UNKNOWN			= 0,
		TYPE_ADDRESSBOOK		= 1,
		TYPE_COMPANY_DIRECTORY  = 2,
		TYPE_IN_NETWORK			= 3,
		TYPE_NAKED_NUMBER		= 4
	};

	//DB Contact Source values - For easier reading.
	public static readonly String DbSource_AddressBook		= "AB";
	public static readonly String DbSource_CompanyDirectory = "CD";
	public static readonly String DbSource_InNetwork		= "IN";
	public static readonly String DbSource_NakedNumber		= "NN";

	//NOTE: Do not re-order this because it will affect DB message entries.  Feel free to APPEND new values
    //Defined in unmanaged class, so we must match it here.
    public static readonly Int32 INVALID_CMGROUP = -1;

	public Contact()
    {
        Type       = ContactType.TYPE_UNKNOWN;
    }

	//Properties ---------------------------------------------------------------------
	//From PN table.
    public Int32    RecordId            { get; set;	   }	//Required for unmanaged class
	public Int32	CmGroup             { get; set;    }	//Required for unmanaged class
	public String	PhoneNumber         { get; set;    }	//Required for unmanaged class
	public String	DisplayNumber       { get; set;    }	//Required for unmanaged class
	public String	PhoneLabel          { get; set;    }	//Required for unmanaged class
	public String	Extension	        { get; set;    }	//Required for unmanaged class
	public String	PreferredNumber     { get; set;    }	//Required for unmanaged class
	public String	Name									//Required for unmanaged class
	{ 
		get { return name;  }
		set { name = Localization.replaceOrPlaceholder( value ); }
    }    

	public String	DisplayName								//Required for unmanaged class
	{ 
		get { return displayName;  }
		set { displayName = Localization.replaceOrPlaceholder( value ); }
    }    

	public String	CmGroupName								//Required for unmanaged class
	{ 
		get { return cmGroupName;  }
		set { cmGroupName = Localization.replaceOrPlaceholder( value ); }
    }    

	public Int32	VoxoxCount          { get;  set;    }    //Required for unmanaged class
	public Int32	CmGroupVoxoxCount   { get;  set;    }    //Required for unmanaged class
	public Int32	BlockedCount	    { get;  set;    }    //Required for unmanaged class
	public Int32	FavoriteCount		{ get;  set;    }    //Required for unmanaged class
	public Int32	PhoneNumberCount	{ get;  set;    }    //Required for unmanaged class
	public Int32	ExtensionCount		{ get;  set;    }    //Required for unmanaged class

	//From GMG table
	public String	GroupId             { get;  set;    }    //Required for unmanaged class

	//Misc defined field name
    //PhoneNumber or Group.  Needed in merged contact List.
	public ContactType	 Type           { get;  set;    }    //Required for unmanaged class		
	public ContactSource Source         { get;  set;    }    //Required for unmanaged class		

	//New Contact-Sync
	public String	ContactKey			{ get;  set;    }    //Required for unmanaged class
	public String	FirstName			{ get;  set;    }    //Required for unmanaged class
	public String	LastName			{ get;  set;    }    //Required for unmanaged class
	public String	Company				{ get;  set;    }    //Required for unmanaged class

	public Boolean isFromAddressBook()	{ return Source == ContactSource.TYPE_ADDRESSBOOK;	}

	public String bestDisplayName( out bool isPhoneNumber )
	{
		String result = Name;

		isPhoneNumber = false;

		if ( String.IsNullOrEmpty( result ) )
		{
			result = PreferredNumber;
			isPhoneNumber = true;
		}

		return result;
	}

	public String bestDisplayNameForCall()
	{
		String result = cmGroupName;

		if ( String.IsNullOrEmpty( result ) )
		{
			result = name;
		}

		return result;
	}

	public Boolean canBeMessaged( Boolean isSmsEnabled )
	{
		Boolean result = true;

		if ( !isSmsEnabled )
		{
			//Must have on-net number.  Currently we *excluded* extensions.
			result = (VoxoxCount > 0);
		}

		return result;
	}

	public static ContactSource convertFromDb( String dbValue )
	{
		ContactSource result = ContactSource.TYPE_UNKNOWN;

		if ( dbValue == DbSource_AddressBook )
		{
			result = ContactSource.TYPE_ADDRESSBOOK;
		}
		else if ( dbValue == DbSource_CompanyDirectory )
		{
			result = ContactSource.TYPE_COMPANY_DIRECTORY;
		}
		else if ( dbValue == DbSource_InNetwork )
		{
			result = ContactSource.TYPE_IN_NETWORK;
		}
		else if ( dbValue == DbSource_NakedNumber )
		{
			result = ContactSource.TYPE_NAKED_NUMBER;
		}
		else 
		{ 
			result = ContactSource.TYPE_UNKNOWN;			//We get here if number is not in DB.
		}

		return result;
	}

};

//=============================================================================

public class ContactList : System.Collections.Generic.List<ManagedDataTypes.Contact>
{
//	ContactList()		{}
//	~ContactList()		{}
};

//=============================================================================



//=============================================================================
// ContactImport class
//	- Used to import contact and phone numbers into DB
//	- This is expected to be a one-way data transger INTO DB.
//=============================================================================

//TODO-Contact-Sync: Rework this so that we import Contacts and PhoneNumbers into separate tables.
public class ContactImport
{
	public ContactImport()
    {
    }

	//JRT - 2015.10.07 - This is currently used ONLY in Unit Test, so it is effectively useless.
    public ContactImport( String contactKey, String name, String phoneNumber, String phoneLabel, String jid, bool isFavorite )
    {
        Name        = name;
        PhoneNumber = phoneNumber;
        PhoneLabel  = phoneLabel;
		Jid			= jid;
        IsFavorite  = isFavorite;

		ContactKey  = contactKey;
		FirstName   = "";
		LastName    = "";
		Company		= "";
		IsBlocked   = false;

		UserId		= 0;
		UserName	= "";
    }

    public ContactImport( String source, String contactKey, String name, String firstName, String lastName, String phoneNumber, String phoneLabel, 
						  String jid, bool isFavorite, bool isBlocked, bool isExtension, Int32 userId, String userName, String company )
    {
        Name        = name;
        PhoneNumber = phoneNumber;
        PhoneLabel  = phoneLabel;
		Jid			= jid;
        IsFavorite  = isFavorite;
		IsBlocked	= isBlocked;
		IsExtension = isExtension;

		ContactKey  = contactKey;
		Source		= source;
		FirstName   = firstName;
		LastName    = lastName;
		Company		= company;

		UserId		= userId;
		UserName    = userName;
    }
	
	public ContactImport clone()
	{
		ContactImport result = new ContactImport();

        result.Name			= Name;
        result.PhoneNumber	= PhoneNumber;
        result.PhoneLabel	= PhoneLabel;
		result.Jid			= Jid;
        result.IsFavorite	= IsFavorite;
		result.IsBlocked	= IsBlocked;
		result.IsExtension	= IsExtension;

		result.ContactKey	= ContactKey;
		result.Source		= Source;
		result.FirstName	= FirstName;
		result.LastName		= LastName;
		result.Company		= Company;
		
		result.UserId		= UserId;
		result.UserName		= UserName;

		return result;
	}

	//Properties ---------------------------------------------------------------------
    public String  Name					{ get; set; }     //Required for unmanaged class
    public String  PhoneNumber			{ get; set; }     //Required for unmanaged class
    public String  PhoneLabel			{ get; set; }     //Required for unmanaged class    //Work, mobile, etc.  No standard values

	public String  ContactKey			{ get; set;	}		//Added for new Contact Syncing
	public String  Source				{ get; set;	}
	public String  FirstName			{ get; set;	}
	public String  LastName				{ get; set;	}
	public String  Company				{ get; set;	}

	public String  Jid					{ get; set;	}
    public Boolean IsFavorite			{ get; set;	}
    public Boolean IsBlocked			{ get; set;	}
    public Boolean IsExtension			{ get; set;	}

	public Int32   UserId				{ get; set;	}	//No real use, right now, but since server is sending, let's save it.
	public String  UserName				{ get; set;	}
};

//=============================================================================

public class ContactImportList : System.Collections.Generic.List<ManagedDataTypes.ContactImport>
{
//	ContactImportList()			{}
//	~ContactImportList()		{}

	public ContactImport findByName( String tgtName )
	{
		ContactImport result = null;

		foreach( ContactImport ci in this )
		{
			if ( ci.Name == tgtName )
			{
				result = ci;
				break;
			}
		}

		return result;
	}

	public ContactImportList getUniqueContactKeys()
	{
		ContactImportList result = new ContactImportList();

		foreach( ContactImport ci in this )
		{
			if ( !result.containsContactKey( ci.ContactKey) )
			{
				result.Add( ci );
			}
		}

		return result;
	}

	public Boolean containsContactKey( String tgtContactKey )
	{
		Boolean result = false;

		foreach( ContactImport ci in this )
		{
			if ( ci.ContactKey == tgtContactKey )
			{
				result = true;
				break;
			}
		}

		return result;
	}
};

//=============================================================================


//=============================================================================
// GroupMessageGroup class
//=============================================================================

public class GroupMessageGroup 
{
	//NOTE: Do not re-order this because it will affect DB message entries.  Feel free to APPEND new values
	public enum GmgStatus 
	{	
		INACTIVE = 0, 			// User has been removed from group, but still has message thread.
		ACTIVE	 = 1, 			// Normal state
	};

	public GroupMessageGroup()  
    {
        Status = GmgStatus.ACTIVE;
    }

	//Properties ---------------------------------------------------------------------
	Int32	    RecordId                { get;  set;    }     //Required for unmanaged class
	GmgStatus	Status                  { get;  set;    }     //Required for unmanaged class
	String	    GroupId                 { get;  set;    }     //Required for unmanaged class
	String      Name                    { get;  set;    }     //Required for unmanaged class
	Int32	    InviteType              { get;  set;    }     //Required for unmanaged class
	Boolean	    AnnounceMembers         { get;  set;    }     //Required for unmanaged class
	String      ConfirmationCode        { get;  set;    }     //Required for unmanaged class
	Int32       AdminUserId             { get;  set;    }     //Required for unmanaged class
//	String      Avatar                  { get;  set;    }     //Required for unmanaged class	//Base-64 encoded string	//Not currently used.
};

//=============================================================================

public class GroupMessageGroupList : System.Collections.Generic.List<ManagedDataTypes.GroupMessageGroup>
{
//	GroupMessageGroupList()			{}
//	~GroupMessageGroupList()		{}
};

//=============================================================================



//=============================================================================
// GroupMessageMember class
//=============================================================================

public class GroupMessageMember 
{
	public GroupMessageMember()
    {
    }

	//Properties ---------------------------------------------------------------------
	Int32		MemberId            { get;  set;    }     //Required for unmanaged class
	Int32		UserId              { get;  set;    }     //Required for unmanaged class
	Int32		ReferrerId          { get;  set;    }     //Required for unmanaged class
	String      GroupId             { get;  set;    }     //Required for unmanaged class
	String      TypeId              { get;  set;    }     //Required for unmanaged class    //1=SMS, 2=IM	//TODO-GM: Why is this string in REST API?
	String      NickName            { get;  set;    }     //Required for unmanaged class
	Boolean		IsAdmin             { get;  set;    }     //Required for unmanaged class
	Boolean		IsInvited           { get;  set;    }     //Required for unmanaged class
	Boolean		HasConfirmed        { get;  set;    }     //Required for unmanaged class
	Boolean		HasOptedOut         { get;  set;    }     //Required for unmanaged class    //TODO: Not sure if this is HAS or CAN OptOut
	Boolean		ShouldNotify        { get;  set;    }     //Required for unmanaged class    //TODO: Not sure if this is SHOULD or HAS notified
};

//=============================================================================

public class GroupMessageMemberList : System.Collections.Generic.List<ManagedDataTypes.GroupMessageMember>
{
//	GroupMessageMemberList()		{}
//	~GroupMessageMemberList()		{}
};

//=============================================================================



//=============================================================================
// GroupMessageMemberOf class
//	- Info for Contacts that are part of a given GM group.
//=============================================================================

public class GroupMessageMemberOf
{
	public GroupMessageMemberOf()
    {
        UserId  = 0;
        IsAdmin = false;;
    }

	//Properties ---------------------------------------------------------------------
    //From GM Group
	Int32	RecordId            { get;  set;    }     //Required for unmanaged class
	String  GroupId             { get;  set;    }     //Required for unmanaged class
	String  GroupName           { get;  set;    }     //Required for unmanaged class

    //From GM Member
	Int32	UserId              { get;  set;    }     //Required for unmanaged class
	Boolean IsAdmin             { get;  set;    }     //Required for unmanaged class
	String  NickName            { get;  set;    }     //Required for unmanaged class

    //From Pn/Contact
	String  DisplayName         { get;  set;    }     //Required for unmanaged class
};

//=============================================================================

public class GroupMessageMemberOfList : System.Collections.Generic.List<ManagedDataTypes.GroupMessageMemberOf>
{
//	GroupMessageMemberOfList()		{}
//	~GroupMessageMemberOfList()		{}
};

//=============================================================================


//=============================================================================
// Message class
//=============================================================================

public class Message	//TODO: abstract? with Chat, SMS, GM as derived classes?
{
	public static string SVR_VOICEMAIL_STATUS_NEW		= "1";
	public static string SVR_VOICEMAIL_STATUS_HEARD		= "2";
	public static string SVR_VOICEMAIL_STATUS_DELETED	= "4";

	//NOTE: Do not re-order this because it will affect DB message entries.  Feel free to APPEND new values
    //Defined in unmanaged class, so we must match it here.
	public enum MsgDirection	
	{
		INBOUND  = 0,
		OUTBOUND = 1,
		ALL		 = 2,			//Had to change from ALL since we get compile 'redefinition' errors							
	};

	//NOTE: Do not re-order this because it will affect DB message entries.  Feel free to APPEND new values
    //Defined in unmanaged class, so we must match it here.
	public enum MsgStatus 
	{										
		NEW				=  0, 	// construct a new message (in-bound or out-bound)
		READ			=  1, 	// in-bound message already read or outbound XMPP message has been read by receiver (person)
		PENDING			=  2, 	// message is waiting to be sent
		PENDING_WIFI	=  3, 	// message is waiting be sent only on a WIFI
		INTRANSITION	=  4, 	// sending ..., message is in a transition phase
		SENT			=  5, 	// message sent successfully
		FAILED			=  6, 	// failed to send message (network failure?)
		RETRY			=  7, 	// user asks to re-send message that was failed to deliver
		UPLOADING		=  8,	// for RichData media
		DELETED			=  9,	// deleted locally, but not yet deleted on server
		DELIVERED		= 10, 	// message successfully received at remote end
		TRANSLATING		= 11,	// async API call should be active to retrieve translated value
		SCHEDULED		= 12,
		ALL				= 13,	//Had to change from ALL since we get compile 'redefinition' errors		
		UNKNOWN			= 14	//Catch-all in case we have invalid data in DB.	//Had to change from UNKNOWN since we get compile 'redefinition' errors		
	};

	//NOTE: Do not re-order this because it will affect DB message entries.  Feel free to APPEND new values
    //Defined in unmanaged class, so we must match it here.
	public enum MsgType
	{
		ALL			  = 0,		//Had to change from ALL since we get compile 'redefinition' errors		
		SMS			  = 1, 
		CHAT		  = 2, 
		VOICEMAIL	  = 3, 
		FAX			  = 4, 
		RECORDED_CALL = 5, 
		UNKNOWN		  = 6,		//Had to change from ALL since we get compile 'redefinition' errors	    //TODO	 
		GROUP_MESSAGE = 7,		
	};

	//These have been strings in the recent past, so pay attention.
	public enum ServerMsgType
	{ 
		VOICEMAIL		= 1,
		FAX				= 2,
		RECORDED_CALL	= 3,
		SMS				= 4,
		XMPP			= 5,	//Includes GM
	};

	public enum ServerMsgStatus
	{
		UNKNOWN	 = 0,		//Not an official value, but we get it in getMessageHistory()
		NEW		 = 1,
		READ	 = 2,
		ARCHIVED = 3,
		DELETED  = 4
	}

    public Message()
    {
        Direction = MsgDirection.INBOUND;
        Status    = MsgStatus.UNKNOWN;
        Type      = MsgType.UNKNOWN;
        RichData = new RichData();
    }

    //Properties ---------------------------------------------------------------

	//Sets ---------------------------------------------------------------------
    public Int32        RecordId            { get;  set;    }     //Required for unmanaged class
    public Int32        ThreadId            { get;  set;    }     //Required for unmanaged class
    public MsgType      Type                { get;  set;    }     //Required for unmanaged class
    public MsgDirection Direction           { get;  set;    }     //Required for unmanaged class
    public MsgStatus    Status              { get;  set;    }     //Required for unmanaged class
    public String       MsgId               { get;  set;    }     //Required for unmanaged class

    public String       Body                { get;  set;    }     //Required for unmanaged class
    public String       TranslateBody       { get;  set;    }     //Required for unmanaged class
    public String       InboundLang         { get;  set;    }     //Required for unmanaged class
    public String       OutboundLang        { get;  set;    }     //Required for unmanaged class

    public VxTimestamp	ServerTimestamp     { get;  set;    }     //Required for unmanaged class
    public Int64        LocalTimestamp      { get;  set;    }     //Required for unmanaged class
    public Int64        ReadTimestamp       { get;  set;    }     //Required for unmanaged class
    public Int64        DeliveredTimestamp  { get;  set;    }     //Required for unmanaged class

    public String       ContactDid          { get;  set;    }     //Required for unmanaged class

    public String       FromDid             { get;  set;    }     //Required for unmanaged class
    public String       ToDid               { get;  set;    }     //Required for unmanaged class

    public String       FromJid             { get;  set;    }     //Required for unmanaged class
    public String       ToJid               { get;  set;    }     //Required for unmanaged class

    public String       ToGroupId           { get;  set;    }     //Required for unmanaged class
    public Int32        FromUserId          { get;  set;    }     //Required for unmanaged class

    public String       FileLocal           { get;  set;    }     //Required for unmanaged class
    public String       ThumbnailLocal      { get;  set;    }     //Required for unmanaged class
    public Int32        Duration            { get;  set;    }     //Required for unmanaged class

    public String       Modified            { get;  set;    }     //Required for unmanaged class
    public String       IAccount            { get;  set;    }     //Required for unmanaged class
    public String       CName               { get;  set;    }     //Required for unmanaged class

//    public Int32        ContactId         { get;  set;    }
//    public Int32        InBoundLogId      { get;  set;    }
    public RichData     RichData			{ get;  set;    }	  //Required for unmanaged class

	//For Carbons handling.  Not in DB
    public String       FromResource        { get;  set;    }     //Required for unmanaged class
    public String       MyResource          { get;  set;    }     //Required for unmanaged class
    public String       ReceiptMsgId        { get;  set;    }     //Required for unmanaged class
	public Int32		ChatMsgType			{ get; set;		}

	public String		ClientUid			{ get; set;		}		//To resolve issue where getMessageHistory returns before sendSMSVerbose and we still have temp msgId


#region Public Methods
	public Boolean isInbound()					{ return (Direction == Message.MsgDirection.INBOUND  ); }
	public Boolean isOutbound()					{ return (Direction == Message.MsgDirection.OUTBOUND ); }

	public Boolean isChat()						{ return (MsgType.CHAT			== Type);	}
	public Boolean isSms()						{ return (MsgType.SMS			== Type);	}
	public Boolean isGroupMessage()				{ return (MsgType.GROUP_MESSAGE == Type);	}
	public Boolean isVoicemail()				{ return (MsgType.VOICEMAIL		== Type);	}
	public Boolean isFax()						{ return (MsgType.FAX			== Type);	}
	public Boolean isRecordedCall()				{ return (MsgType.RECORDED_CALL	== Type);	}

	public Boolean isVoicemailEx()				{ return (MsgType.VOICEMAIL== Type || MsgType.RECORDED_CALL == Type);	}

	//Status
	public Boolean isStatusUnknown()			{ return (Status == MsgStatus.UNKNOWN	  );	}
	public Boolean isStatusAll()				{ return (Status == MsgStatus.ALL		  );	}
	public Boolean isStatusNew()				{ return (Status == MsgStatus.NEW		  );	}
	public Boolean isStatusRead()				{ return (Status == MsgStatus.READ		  );	}
	public Boolean isStatusPending()			{ return (Status == MsgStatus.PENDING	  );	}
	public Boolean isStatusPendingWifi()		{ return (Status == MsgStatus.PENDING_WIFI);	}
	public Boolean isStatusInTransition()		{ return (Status == MsgStatus.INTRANSITION);	}
	public Boolean isStatusSent()				{ return (Status == MsgStatus.SENT		  );	}
	public Boolean isStatusFailed()				{ return (Status == MsgStatus.FAILED	  );	}
	public Boolean isStatusRetry()				{ return (Status == MsgStatus.RETRY		  );	}
	public Boolean isStatusUploading()			{ return (Status == MsgStatus.UPLOADING	  );	}
	public Boolean isStatusDeleted()			{ return (Status == MsgStatus.DELETED	  );	}
	public Boolean isStatusDelivered()			{ return (Status == MsgStatus.DELIVERED	  );	}
	public Boolean isStatusTranslating()		{ return (Status == MsgStatus.TRANSLATING );	}
	public Boolean isStatusScheduled()			{ return (Status == MsgStatus.SCHEDULED   );	}

	//RichData
	public Boolean isRichDataPicture()			{ return ( RichData.isTypeImage()  );		}
	public Boolean isRichDataVideo()			{ return ( RichData.isTypeVideo()    );		}
	public Boolean isRichDataLocation()			{ return ( RichData.isTypeLocation() );		}
	public Boolean isRichDataContact()			{ return ( RichData.isTypeContact()  );		}
	public Boolean isRichDataFile()				{ return ( RichData.isTypeMedia()	  );		}

	public String getOriginatingDid()			{ return  isInbound() ? FromDid : ToDid;		}
	public String getOriginatingJid()			{ return  isInbound() ? FromJid : ToJid;		}

	public void setOriginatingDid( String value )
	{
		if ( isInbound() )
			FromDid = value;
		else
			ToDid   = value;
	}

	public void setDestinationDid( String value )
	{
		if ( isInbound() )
			ToDid = value;
		else
			FromDid = value;
	}

	//We have caller provide the RichData object because we use the native DBM call to parse the JSON,
	//	and we want to avoid dependency.  So maybe this method should not be here?
	public Boolean isTranslatable( RichData richData )
	{
		return ( isChat() || isSms() || isVoicemail() )	&& richData.isTypeNone();
	}


	//Some static convenience methods so we don't have to expose implementation
	//	and to make it easier to deal with DB fields (mostly 'int' vs. 'enum')
	public static Boolean typeIsPhoneNumberBased( Message.MsgType type )
	{
		return type == MsgType.SMS 	   	 ||
			   type == MsgType.VOICEMAIL ||
			   type == MsgType.FAX 		 ||
			   type == MsgType.RECORDED_CALL;
	}

	public static Boolean typeIsJidBased( Message.MsgType type )
	{
		return type == MsgType.CHAT;
	}

	public static Boolean typeIsGroupMessageBased( Message.MsgType type )
	{
		return type == MsgType.GROUP_MESSAGE;
	}

	public static Boolean dirIsOutbound( Message.MsgDirection dir ) 
	{
		return dir == Message.MsgDirection.OUTBOUND;
	}

	public static Boolean dirIsInbound( Message.MsgDirection dir ) 
	{
		return dir == Message.MsgDirection.INBOUND;
	}

	//Some 'int' based methods, useful with values from DB.
	public static Boolean typeIsPhoneNumberBased( int type )
	{
		//TODO: Validate 'type' values
		return typeIsPhoneNumberBased( (MsgType) type );
	}

	public static Boolean typeIsJidBased( int type )
	{
		//TODO: Validate 'type' values
		return typeIsJidBased( (MsgType) type );
	}

	public static Boolean dirIsOutbound( int dir ) 
	{
		//TODO: Validate 'dir' values
		return dirIsOutbound( (Message.MsgDirection) dir );
	}

	public static Boolean dirIsInbound( int dir ) 
	{
		//TODO: Validate 'dir' values
		return dirIsInbound( (Message.MsgDirection) dir );
	}

	/**
	 * Convert server message type to app message type
	 *
	 * @param serverMessageType
	 * @return - Message.MsgType
	 */
	public static MsgType typeFromServerType( Int32 serverMessageTypeIn ) 
	{
		ServerMsgType svrType = (ServerMsgType)serverMessageTypeIn;
		MsgType type = MsgType.UNKNOWN;

		switch ( svrType )
		{
		case ServerMsgType.FAX:
			type = MsgType.FAX;
			break;
		case ServerMsgType.RECORDED_CALL:
			type = MsgType.RECORDED_CALL;
			break;
		case ServerMsgType.VOICEMAIL:
			type = MsgType.VOICEMAIL;
			break;
		case ServerMsgType.SMS:
			type = MsgType.SMS;
			break;
		case ServerMsgType.XMPP:
			type = MsgType.CHAT;	//Includes Group Message
			break;
		}

		return type;
	}

	/**
	 * Convert App message type to server message type
	 *
	 * @param MsgType
	 * @return - serverMessageType
	 */
	public static ServerMsgType typeToServerType( MsgType msgType )
	{
		ServerMsgType serverType = ServerMsgType.XMPP;

		switch ( msgType )
		{ 
		case Message.MsgType.CHAT:
		case Message.MsgType.GROUP_MESSAGE:
			serverType = ServerMsgType.XMPP;
			break;
		case Message.MsgType.SMS:
			serverType = ServerMsgType.SMS;
			break;
		case Message.MsgType.VOICEMAIL:
			serverType = ServerMsgType.VOICEMAIL;
			break;
		case Message.MsgType.FAX:
			serverType = ServerMsgType.RECORDED_CALL;
			break;
		case Message.MsgType.RECORDED_CALL:
			serverType = ServerMsgType.RECORDED_CALL;
			break;
		default:
			break;
		}

		return serverType;
	}

	//Convert server status to client status
	public static MsgStatus statusFromServerStatus( Int32 serverMessageStatusIn ) 
	{
		ServerMsgStatus svrStatus = (ServerMsgStatus)serverMessageStatusIn;
		MsgStatus status = MsgStatus.UNKNOWN;

		switch ( svrStatus )
		{
		case ServerMsgStatus.UNKNOWN:			//Not an official value, but we get it in getMessageHistory()
			status = MsgStatus.UNKNOWN;
			break;
		case ServerMsgStatus.NEW:
			status = MsgStatus.NEW;
			break;
		case ServerMsgStatus.READ:
			status = MsgStatus.READ;
			break;
		case ServerMsgStatus.ARCHIVED:
			status = MsgStatus.DELETED;
			break;
		case ServerMsgStatus.DELETED:
			status = MsgStatus.DELETED;
			break;
		}

		return status;
	}

	public static MsgStatus statusFromServerStatusEx( Int32 serverMessageStatusIn, MsgDirection dir ) 
	{
		MsgStatus status = statusFromServerStatus( serverMessageStatusIn );

		if ( status == MsgStatus.UNKNOWN )
		{
			//This is a best guess, if server does not provide proper status.			
			status = (dir == MsgDirection.INBOUND ? MsgStatus.NEW : MsgStatus.SENT );
		}

		return status;
	}

	//Convert server status from client status
	public static ServerMsgStatus statusToServerStatus( MsgStatus status ) 
	{
		ServerMsgStatus svrStatus = ServerMsgStatus.UNKNOWN;

		switch ( status )
		{
		case MsgStatus.UNKNOWN:			//Not an official value, but we get it in getMessageHistory()
			svrStatus = ServerMsgStatus.UNKNOWN;
			break;
		case MsgStatus.NEW:
			svrStatus = ServerMsgStatus.NEW;
			break;
		case MsgStatus.READ:
			svrStatus = ServerMsgStatus.READ;
			break;
//		case MsgStatus.ARCHIVED:
//			svrStatus = ServerMsgStatus.DELETED;
//			break;
		case MsgStatus.DELETED:
			svrStatus = ServerMsgStatus.DELETED;
			break;
		}

		return svrStatus;
	}

	public Boolean isOkToSend()
	{
		return ( isStatusNew() || isStatusRetry() || isStatusScheduled() );
	}

	public bool canBeMarkedRead()
	{
		bool result = false;

		if ( !isStatusDeleted() )
		{
            if (isInbound())
            {
                result = isStatusNew();
            }
            else if (isOutbound())
            {
                result = isStatusSent() || isStatusDelivered();
            }
		}

		return result;
	}

	public bool canBeResent()
	{
		bool result = false;

		if ( !isStatusDeleted() )
		{
			if ( isOutbound() )
            {
                result = isStatusFailed() ||  isStatusPending();
            }
		}

		return result;
	}

	static public Message makeChatMessage( Message.MsgDirection dir, Message.MsgStatus status, String fromJid, String toJid, String fromResource, String myResource, String msgId, String body,
											String receiptMsgId, int type, Int64 localTimestamp )
	{
		Message result = new Message();

		result.Direction		= dir;
		result.Status			= status;
		result.Type				= MsgType.CHAT;

		result.FromJid			= fromJid;	//TODO: Split between JID and resource?
		result.ToJid			= toJid;
		result.LocalTimestamp	= localTimestamp;
		result.MsgId			= msgId;		//TODO: Append timestamp ?  Or let caller do that?
		result.Body				= body;

		//TODO: not in DB.  Was this added for Carbons?
		result.FromResource     = fromResource;
		result.MyResource		= myResource;
		result.ReceiptMsgId     = receiptMsgId;
		result.ChatMsgType      = type;
//		.subject(message.getSubject())
//		.msgThread(chat.getThreadID())

		return result;
	}

#endregion
};  //Message

//=============================================================================

public class MessageList : System.Collections.Generic.List<ManagedDataTypes.Message>
{
//public:
//    MessageList()		{}
//    ~MessageList()		{}
};  //MessageList

//=============================================================================


//=============================================================================
// MessageTranslationProperties class
//  NOTE: This is taken directly from -Android- implementation which is TOO granular
//           in that it allows translation to be set at the contact/phone number level
//        If contact has a single number, then this is NOT an issue.  However, if
//        contact has multiple numbers, the translation setting should apply to
//        all phone numbers.  IOW, the translation is at CONTACT level, not Phone Number level
//=============================================================================

public class MessageTranslationProperties
{
	public MessageTranslationProperties()
    {
        IsEnabled = false;
        Direction = Message.MsgDirection.INBOUND;
    }

	//Properties ---------------------------------------------------------------------
	public Int32                    RecordId            { get;  set;    }     //Required for unmanaged class
	public Int32                    ContactId           { get;  set;    }     //Required for unmanaged class
	public Boolean		            IsEnabled           { get;  set;    }     //Required for unmanaged class
//	public String                   PhoneNumber         { get;  set;    }     //Required for unmanaged class
//	public String			        Jid                 { get;  set;    }     //Required for unmanaged class
	public String			        UserLanguage        { get;  set;    }     //Required for unmanaged class
	public String  			        ContactLanguage     { get;  set;    }     //Required for unmanaged class
	public Message.MsgDirection 	Direction           { get;  set;    }     //Required for unmanaged class
};

//=============================================================================

public class MessageTranslationPropertiesList : System.Collections.Generic.List<ManagedDataTypes.MessageTranslationProperties>
{
//	MessageTranslationPropertiesList()		{}
//	~MessageTranslationPropertiesList()		{}
};

//=============================================================================



//=============================================================================
// PhoneNumber class
//
//	I have merged ContactDetailsPhoneNumber class with this class.
//	Other than a couple more data elements, this class now:
//	 - May contain Group info (Name, ID).  These will be stored in Name and Number
//	 - A Type member to distinguish Phone/contact vs. Group.
//=============================================================================

public class PhoneNumber 
{
	private string name;
	private string displayName;
	private string cmGroupName;

	//NOTE: Do not re-order this because it will affect DB message entries.  Feel free to APPEND new values
    //Defined in unmanaged class, so we must match it here.
	public enum PnType 
	{
		NOT_VOXOX_RELATED			= 0,	// any phone number (in the address book or not)
		DID_NUMBER					= 1,	// VoxOx assigned number 
		MOBILE_REGISTRATION_NUMBER	= 2,	// the mobile registration number used when the user signed up for VoxOx
	};

	public PhoneNumber()
    {
        CmGroup       = Contact.INVALID_CMGROUP;
        ContactType	  = Contact.ContactType.TYPE_PHONE_NUMBER;
        ContactSource = Contact.ContactSource.TYPE_UNKNOWN;
        NumberType    = PhoneNumber.PnType.DID_NUMBER;   
        IsFavorite    = false;
        IsBlocked     = false;
		IsExtension   = false;
        XmppStatus    = -1;               //Define this default?
    }

	public Boolean isValid()
    {
	    Boolean result = false;

	    if ( !String.IsNullOrEmpty( DisplayName) &&
		     !String.IsNullOrEmpty( Number) )
	    {
		    result = true;
        }

    	return result;
    }

	public Boolean numberIsVoxox()	            { return NumberType == PnType.DID_NUMBER;	                }
    public Boolean numberIsMobReg()             { return NumberType == PnType.MOBILE_REGISTRATION_NUMBER;	}

	//Properties ---------------------------------------------------------------------
	public Int32	        RecordId            { get;  set;    }   //Required for unmanaged class
	public String			Name									//Required for unmanaged class
	{ 
		get { return name;  }
		set { name = Localization.replaceOrPlaceholder( value ); }
    }    

	public String	DisplayName										//Required for unmanaged class
	{ 
		get { return displayName;  }
		set { displayName = Localization.replaceOrPlaceholder( value ); }
    }    

	public String	CmGroupName										//Required for unmanaged class
	{ 
		get { return cmGroupName;  }
		set { cmGroupName = Localization.replaceOrPlaceholder( value ); }
    }    

	public Contact.ContactType   ContactType      { get;  set;    }     //Required for unmanaged class
	public Contact.ContactSource ContactSource    { get;  set;    }     //Required for unmanaged class

	//Phone Number related
	public String           Number              { get;  set;    }     //Required for unmanaged class
	public String           DisplayNumber       { get;  set;    }     //Required for unmanaged class
	public String           Label               { get;  set;    }     //Required for unmanaged class
	public Int32 	        CmGroup             { get;  set;    }     //Required for unmanaged class
	public PnType	        NumberType          { get;  set;    }     //Required for unmanaged class
	public Boolean	        IsFavorite          { get;  set;    }     //Required for unmanaged class	
	
	//XMPP related
	public String	        XmppJid             { get;  set;    }     //Required for unmanaged class
	public String	        XmppAlias           { get;  set;    }     //Required for unmanaged class
	public String           XmppPresence        { get;  set;    }     //Required for unmanaged class
	public Int32	        XmppStatus          { get;  set;    }     //Required for unmanaged class
	public String           XmppGroup           { get;  set;    }     //Required for unmanaged class
	
	public Boolean isVoxox()							{  return !String.IsNullOrEmpty( XmppJid ); }	//VoxoxUserId is now part JID.

	public Boolean isFromDb()							{ return !String.IsNullOrEmpty(ContactKey);	}	//This indicates phoneNumber is NOT in DB

	//New Contact-Sync
	public String	ContactKey			{ get;  set;    }    //Required for unmanaged class
	public String	FirstName			{ get;  set;    }    //Required for unmanaged class
	public String	LastName			{ get;  set;    }    //Required for unmanaged class
	public String	Company				{ get;  set;    }    //Required for unmanaged class
	public Int32	UserId				{ get;  set;    }    //Required for unmanaged class
	public String	UserName			{ get;  set;    }    //Required for unmanaged class
	public Boolean	IsBlocked			{ get;  set;    }    //Required for unmanaged class
	public Boolean	IsExtension			{ get;  set;    }    //Required for unmanaged class

	//We have a separate method here because:
	//	- logic may change
	//	- we may need to include Extensions, which *may* not have XMPP JID.
	public Boolean  isInNetwork()
	{
		return isVoxox();
	}

	public String bestDisplayName( out bool isPhoneNumber )
	{
		String result = CmGroupName;

		isPhoneNumber = false;

		if ( String.IsNullOrEmpty( result ) )
		{ 
			result = DisplayName;
		}

		if ( String.IsNullOrEmpty( result ) )
		{ 
			result = Name;
		}

		if ( String.IsNullOrEmpty( result ) )
		{ 
			result = DisplayNumber;
			isPhoneNumber = true;
		}

		return result;
	}
};

//=============================================================================

public class PhoneNumberList : System.Collections.Generic.List<ManagedDataTypes.PhoneNumber>
{
//	PhoneNumberList()		{}
//	~PhoneNumberList()		{}

	public int getVoxoxCount()
	{
		int result = 0;

		foreach ( PhoneNumber pn in this )
		{
			if ( pn.numberIsVoxox() )
				result++;
		}

		return result;

	}
};

//=============================================================================


//=============================================================================

public class RecentCall
{
	private string cmGroupName;

	public enum CallStatus 
	{
		None		= 0,	//Invalid status
		New			= 1,	//Inbound/Outbound
		Answered	= 2,	//Inbound/Outbound
		Declined	= 3,	//Inbound/Outbound
		Missed		= 4,	//Inbound
	};

	public enum SeenFlag
	{
		NotSeen = 0,
		Seen    = 1
	}

	public RecentCall()
	{
		RecordId		= -1;
		Status			= CallStatus.None;
		Incoming		= false;
		Duration		= 0;
	}

	public Boolean isValid()				{ return Status != CallStatus.None;		}

	public Boolean isNew()					{ return Status == CallStatus.New;		}
	public Boolean isAnswered()				{ return Status == CallStatus.Answered;	}
	public Boolean isDeclined()				{ return Status == CallStatus.Declined;	}
//	public Boolean isMissed()				{ return Status == CallStatus.Missed;	}
	public Boolean isMissedEx()				{ return Status == CallStatus.Missed && Incoming == true;	}

	public Boolean isCompleted() 			{ return isAnswered() && Duration > 0;	}

	public Boolean hasBeenSeen()			{  return Seen == SeenFlag.Seen; }

	public Boolean canBeMarkedSeen()
	{
		Boolean result = false;
						
		//We only care about incoming calls, for now.
		if ( Incoming )
		{
			result = !hasBeenSeen();
		}

		return result;
	}

	//Properties ---------------------------------------------------------------------
	public Int32		RecordId            { get;  set;    }     //Required for unmanaged class    
	public String		Uid		            { get; set;     }     //Required for unmanaged class
	public CallStatus	Status              { get;  set;    }     //Required for unmanaged class		//Should Missed be a separate flag? As from CallHistory
	public Boolean		Incoming            { get;  set;    }     //Required for unmanaged class
	public VxTimestamp	StartTime		    { get;  set;    }     //Required for unmanaged class    
	public Int32		Duration            { get;  set;    }     //Required for unmanaged class    
    public Boolean      IsLocal             { get; set;     }     //Required for unmanaged class
    public SeenFlag     Seen                { get; set;     }     //Required for unmanaged class

	public String		Number              { get;  set;    }     //Required for unmanaged class    
	public String		OrigNumber          { get;  set;    }     //Required for unmanaged class    
	public String		DisplayNumber       { get;  set;    }     //Required for unmanaged class    	
	public String		Name                { get;  set;    }     //Required for unmanaged class    	
	public Int32		CmGroup	            { get;  set;    }     //Required for unmanaged class    
	public String		ContactKey          { get;  set;    }     //Required for unmanaged class    

	public String	CmGroupName										//Required for unmanaged class
	{ 
		get { return cmGroupName;  }
		set { cmGroupName = Localization.replaceOrPlaceholder( value ); }
    }    

	public String bestDisplayNumber()
	{
		return ( DisplayNumber.Length > 0 ? DisplayNumber : OrigNumber);
	}
};

//=============================================================================

public class RecentCallList : System.Collections.Generic.List<ManagedDataTypes.RecentCall>
{
//	RecentCallList()		{}
//	~RecentCallList()		{}

	public Int64 getMostRecentStartTimestamp()
	{
		Int64 result = 0;

		foreach ( RecentCall call in this )
		{
			if ( call.StartTime.AsLong() > result )
				result = call.StartTime.AsLong();
		}

		return result;
	}
};

//=============================================================================


//=============================================================================
// RichData class
//	- This does not directly map to a DB class, so maybe we don't need it
//	- If we do, then it most likely needs to/from string logic so we can update DB.
//	  Hopefully, we can avoid this duplicate logic in C++ / C# classes.
//=============================================================================
public class RichData 
{
	//Default/Invalid values
	private static readonly Double LATITUDE_INVALID  = -1;
    private static readonly Double LONGITUDE_INVALID = -1;

	//RichData type strings
	private static readonly String TYPE_JPEG     = "jpeg";
	private static readonly String TYPE_MPEG     = "mpeg";			
	private static readonly String TYPE_LOCATION = "location";
	private static readonly String TYPE_CONTACT  = "contact";
	private static readonly String TYPE_PDF      = "pdf";		//TODO: future?

	public enum RichDataType       //TODO?: Keep this private.  Define 'isType' and 'setType' methods.
	{
		NONE	 = 0, 
		IMAGE	 = 1, 
		VIDEO	 = 2,	
		LOCATION = 3, 
		CONTACT	 = 4, 
		PDF		 = 5,
	};

	public RichData()
    {
        Type      = RichDataType.NONE;
        Latitude  = LATITUDE_INVALID;
        Longitude = LONGITUDE_INVALID;
    }

    //Convenience methods
	public bool hasMediaFile()	                { return ! System.String.IsNullOrEmpty( MediaFile );		}	
	public bool hasThumbnailFile()	            { return ! System.String.IsNullOrEmpty( ThumbnailFile );    }	

	//Properties ---------------------------------------------------------------------
	public RichDataType	Type                    { get;  set;    }     //Required for unmanaged class    

	public String	ThumbnailUrl                { get;  set;    }     //Required for unmanaged class
	public String	MediaUrl                    { get;  set;    }     //Required for unmanaged class
	public String	Body                        { get;  set;    }     //Required for unmanaged class

	public double	Latitude                    { get;  set;    }     //Required for unmanaged class
	public double	Longitude                   { get;  set;    }     //Required for unmanaged class
	public String	Address                     { get;  set;    }     //Required for unmanaged class

	//Hold files properties locally, -NOT USED- as part of the JSON object
    //TODO: NOTE that in original code, these were FILE types so we could parse the file path name.
	public String	MediaFile                   { get;  set;    }     //Required for unmanaged class
	public String	ThumbnailFile               { get;  set;    }     //Required for unmanaged class

	//Convenience methods
	public Boolean isTypeNone()					{  return Type == RichDataType.NONE;	 }
	public Boolean isTypeImage()				{  return Type == RichDataType.IMAGE;	 }
	public Boolean isTypeVideo()				{  return Type == RichDataType.VIDEO;	 }
	public Boolean isTypeLocation()				{  return Type == RichDataType.LOCATION; }
	public Boolean isTypeContact()				{  return Type == RichDataType.CONTACT;	 }
	public Boolean isTypePdf()					{  return Type == RichDataType.PDF;		 }

	public Boolean isTypeMedia()				{  return isTypeImage() || isTypeVideo();	}


	//Typically called from JSON deserializer
	public void setType( string rawType )
	{
		RichDataType result = RichDataType.NONE;

		if ( rawType == TYPE_CONTACT )
		{
			result = RichDataType.CONTACT;
		}
		else if ( rawType == TYPE_LOCATION )
		{
			result = RichDataType.LOCATION;
		}
		else if ( rawType == TYPE_JPEG )
		{
			result = RichDataType.IMAGE;
		}
		else if ( rawType == TYPE_MPEG )
		{
			result = RichDataType.VIDEO;
		}
		else if ( rawType == TYPE_PDF )
		{
			result = RichDataType.PDF;
		}

		Type = result;
	}

	public String getTypeAsText()
	{
		String result = "";

		switch ( Type )
		{
		case RichDataType.NONE:
			result = "";
			break;

		case RichDataType.CONTACT:
			result = TYPE_CONTACT;
			break;

		case RichDataType.LOCATION:
			result = TYPE_LOCATION;
			break;

		case RichDataType.IMAGE:
			result = TYPE_JPEG;
			break;

		case RichDataType.VIDEO:
			result = TYPE_MPEG;
			break;

		case RichDataType.PDF:
			result = TYPE_PDF;
			break;

		}

		return result;
	}

	static public RichData fromJsonText( String jsonText )
	{
        RichData result = new RichData();
		
		if ( jsonText.Contains( "voxoxrichmessage" ) )
		{
			try 
			{ 
				RestSharp.RestResponse response = new RestSharp.RestResponse();
				response.Content = jsonText;

				var deserializer = new RestSharp.Deserializers.JsonDeserializer();

                RichDataJsonBase temp = deserializer.Deserialize<RichDataJsonBase>(response);
                result.setType(temp.voxoxrichmessage.type);

                if (temp.voxoxrichmessage.type == TYPE_JPEG || temp.voxoxrichmessage.type == TYPE_MPEG)
                {
                    var media = deserializer.Deserialize<RichDataJsonMedia>(response);
                    result.Body = media.voxoxrichmessage.body;
                    result.MediaUrl = media.voxoxrichmessage.data.file;
                    result.ThumbnailUrl = media.voxoxrichmessage.data.thumbnail;
                }
                else if (temp.voxoxrichmessage.type == TYPE_LOCATION)
                {
                    var location = deserializer.Deserialize<RichDataJsonLocation>(response);
                    result.Body = location.voxoxrichmessage.body;
                    result.Latitude = location.voxoxrichmessage.data.latitude;
                    result.Longitude = location.voxoxrichmessage.data.longitude;
                }
				
				//result.MediaUrl		= temp.voxoxrichmessage.data.file;
				//result.ThumbnailUrl = temp.voxoxrichmessage.data.thumbnail;

				//result.setType( temp.voxoxrichmessage.type );

				//var serializer = new RestSharp.Serializers.JsonSerializer();
				//String tempOut = serializer.Serialize( temp );
				//int xxx = 1;

			}

			catch ( Exception /*e*/ )
			{
                throw;	// e;	//throw without explicit value with re-throw original exception and maintain stack location.
				//String temp = e.ToString();	//TODO-LOG
			}
		}

		return result;
	}

	static public String toJsonText( RichData richData )
	{
		String result = "";
		var serializer = new RestSharp.Serializers.JsonSerializer();

		if ( richData.isTypeNone() )
		{
			//TODO-LOG
		}
		else if ( richData.isTypeContact() )
		{
			RichDataJsonContact temp = new RichDataJsonContact( richData );
			result = serializer.Serialize( temp );
		}
		else if ( richData.isTypeLocation() )
		{
            RichDataJsonLocation temp = new RichDataJsonLocation(richData);
            result = serializer.Serialize(temp);
		}
		else if ( richData.isTypeMedia() )
		{
            RichDataJsonMedia temp = new RichDataJsonMedia(richData);
            result = serializer.Serialize(temp);
		}

		return result;
	}

};  //RichData

//-----------------------------------------------------------------------------
//Because RichData JSON has different payloads for each 'type', and I cannot find
//	a way to manually create a JSON object based on 'type' (which I find utterly
//	frustrating and unbelievable), I am forced to create these 4 related RichData classes.
//-----------------------------------------------------------------------------
public class RichMessageBase
{
	public RichMessageBase()
	{
	}

	//Which contains:
	public string type				{ get; set; }	//location, contact, jpeg, mpeg
	public string body				{ get; set; }

	//'data' will be added by derived classes.
};

public class RichMessageContact : RichMessageBase
{
	public RichMessageContact()
	{
		data = new Data();
	}

	public Data data { get; set; }

	public class Data
	{ 
		public Data()
		{
		}

		//I think this contains VCard info
		public string file				{ get; set; }
	}
}

public class RichMessageLocation : RichMessageBase
{
	public RichMessageLocation()
	{
		data = new Data();
	}

	public Data data { get; set; }

	public class Data
	{ 
		public Data()
		{
			latitude  = -1;
			longitude = -1;
		}

		//Location - in 'data' if type == location,
		//	'body' will contain street address, if available
		public double latitude				{ get; set; }
		public double longitude				{ get; set; }
	}
}

public class RichMessageMedia : RichMessageBase
{
	public RichMessageMedia()
	{
		data = new Data();
	}

	public Data data { get; set; }

	public class Data
	{ 
		public Data()
		{
		}

		//Media (jpeg, mpeg) - in 'data' if type == jpeg or mpeg
		//	'body' will be accompanying text message, if any
		public string file				{ get; set; }
		public string thumbnail			{ get; set; }
	}
}

public class RichDataJsonBase
{
	public RichDataJsonBase()
	{
		voxoxrichmessage = new RichMessageBase();
	}

	//Main JSON object
    public RichMessageBase voxoxrichmessage	{ get; set; }

};

public class RichDataJsonContact : RichDataJsonBase
{
	public RichDataJsonContact()
	{
		voxoxrichmessage = new RichMessageContact();
	}

	public RichDataJsonContact( RichData richData )
	{
		voxoxrichmessage = new RichMessageContact();

		voxoxrichmessage.body = richData.Body;
		voxoxrichmessage.type = richData.getTypeAsText();

		voxoxrichmessage.data.file = richData.MediaUrl;
	}

	//Main JSON object
    public new RichMessageContact voxoxrichmessage	{ get; set; }
};

public class RichDataJsonLocation : RichDataJsonBase
{
	public RichDataJsonLocation()
	{
		voxoxrichmessage = new RichMessageLocation();
	}

	public RichDataJsonLocation( RichData richData )
	{
		voxoxrichmessage = new RichMessageLocation();

		voxoxrichmessage.body = richData.Body;
		voxoxrichmessage.type = richData.getTypeAsText();

		voxoxrichmessage.data.latitude  = richData.Latitude;
		voxoxrichmessage.data.longitude = richData.Longitude;
	}

	//Main JSON object
    public new RichMessageLocation voxoxrichmessage { get; set; }
};

public class RichDataJsonMedia : RichDataJsonBase
{
	public RichDataJsonMedia()
	{
		voxoxrichmessage = new RichMessageMedia();
	}

	public RichDataJsonMedia( RichData richData )
	{
		voxoxrichmessage = new RichMessageMedia();

		voxoxrichmessage.body = richData.Body;
		voxoxrichmessage.type = richData.getTypeAsText();

		voxoxrichmessage.data.file      = richData.MediaUrl;
		voxoxrichmessage.data.thumbnail = richData.ThumbnailUrl;
	}

	//Main JSON object
    public new RichMessageMedia voxoxrichmessage { get; set; }
};
//-------------------------------------------------------------------------------------
//End RichData special JSON classes
//-------------------------------------------------------------------------------------


//=============================================================================


//=============================================================================
// UmwMessage class - TODO: Derive this from Message Class.
//	- Returns data to be used in UMW
//	- Outgoing from model layer only from a query.
//=============================================================================

public class UmwMessage
{
	private string cmGroupName;

	public UmwMessage()
    {
        CmGroup	    = Contact.INVALID_CMGROUP;
        NumberType  = PhoneNumber.PnType.DID_NUMBER;
        Type        = Message.MsgType.UNKNOWN;
        Direction   = Message.MsgDirection.INBOUND;
        Status      = Message.MsgStatus.UNKNOWN;

		ServerTimestamp = new VxTimestamp( 0 );
    }

	//Properties ---------------------------------------------------------------------
	public String			    ContactKey	                { get;  set;    }     //Required for unmanaged class
	public Int32			    CmGroup                     { get;  set;    }     //Required for unmanaged class
	public PhoneNumber.PnType   NumberType                  { get;  set;    }     //Required for unmanaged class	//Was/Is 'is_voxox' in DB.
	public String			    Number                      { get;  set;    }     //Required for unmanaged class
	public String			    Name                        { get;  set;    }     //Required for unmanaged class

	public String				CmGroupName										//Required for unmanaged class
	{ 
		get { return cmGroupName;  }
		set { cmGroupName = Localization.replaceOrPlaceholder( value ); }
    }    

	//From GM table
	public String			    GmNickName                  { get;  set;    }     //Required for unmanaged class
	public String			    GmColor                     { get;  set;    }     //Required for unmanaged class
	public String			    GmGroupName                 { get;  set;    }     //Required for unmanaged class

	//Main message info
	public Int32				RecordId                    { get;  set;    }     //Required for unmanaged class
	public Message.MsgType		Type                        { get;  set;    }     //Required for unmanaged class
	public Message.MsgDirection Direction                   { get;  set;    }     //Required for unmanaged class
	public Message.MsgStatus	Status                      { get;  set;    }     //Required for unmanaged class
	public String			    Body                        { get;  set;    }     //Required for unmanaged class
	public String			    Translated                  { get;  set;    }     //Required for unmanaged class
	public String			    MsgId                       { get;  set;    }     //Required for unmanaged class

	//Time stamps
	public Int64				LocalTimestamp              { get;  set;    }     //Required for unmanaged class
	public VxTimestamp			ServerTimestamp             { get;  set;    }     //Required for unmanaged class
	public Int64				ReadTimestamp               { get;  set;    }     //Required for unmanaged class
	public Int64				DeliveredTimestamp          { get;  set;    }     //Required for unmanaged class

	//Rich data
	public String			    FileLocal                   { get;  set;    }     //Required for unmanaged class
	public String			    ThumbnailLocal              { get;  set;    }     //Required for unmanaged class
	public Int32				Duration                    { get;  set;    }     //Required for unmanaged class

	//SMS
	public String			    FromDid                     { get;  set;    }     //Required for unmanaged class
	public String			    ToDid                       { get;  set;    }     //Required for unmanaged class

	//Chat
	public String			    FromJid                     { get;  set;    }     //Required for unmanaged class
	public String			    ToJid                       { get;  set;    }     //Required for unmanaged class

	//Group Messaging
	public String			    ToGroupId                   { get;  set;    }     //Required for unmanaged class
	public Int32			    FromUserId                  { get;  set;    }     //Required for unmanaged class

#region Public Methods
	public Boolean isInbound()					{ return (Direction == Message.MsgDirection.INBOUND  ); }
	public Boolean isOutbound()					{ return (Direction == Message.MsgDirection.OUTBOUND ); }

	public Boolean isChat()						{ return (Message.MsgType.CHAT			== Type);	}
	public Boolean isSms()						{ return (Message.MsgType.SMS			== Type);	}
	public Boolean isGroupMessage()				{ return (Message.MsgType.GROUP_MESSAGE == Type);	}
	public Boolean isVoicemail()				{ return (Message.MsgType.VOICEMAIL		== Type);	}
	public Boolean isFax()						{ return (Message.MsgType.FAX			== Type);	}
	public Boolean isRecordedCall()				{ return (Message.MsgType.RECORDED_CALL	== Type);	}

	public Boolean isVoicemailEx()				{ return (Message.MsgType.VOICEMAIL== Type || Message.MsgType.RECORDED_CALL == Type);	}

	//Status
	public Boolean isStatusUnknown()			{ return (Status == Message.MsgStatus.UNKNOWN	  );	}
	public Boolean isStatusAll()				{ return (Status == Message.MsgStatus.ALL		  );	}
	public Boolean isStatusNew()				{ return (Status == Message.MsgStatus.NEW		  );	}
	public Boolean isStatusRead()				{ return (Status == Message.MsgStatus.READ		  );	}
	public Boolean isStatusPending()			{ return (Status == Message.MsgStatus.PENDING	  );	}
	public Boolean isStatusPendingWifi()		{ return (Status == Message.MsgStatus.PENDING_WIFI);	}
	public Boolean isStatusInTransition()		{ return (Status == Message.MsgStatus.INTRANSITION);	}
	public Boolean isStatusSent()				{ return (Status == Message.MsgStatus.SENT		  );	}
	public Boolean isStatusFailed()				{ return (Status == Message.MsgStatus.FAILED	  );	}
	public Boolean isStatusRetry()				{ return (Status == Message.MsgStatus.RETRY		  );	}
	public Boolean isStatusUploading()			{ return (Status == Message.MsgStatus.UPLOADING	  );	}
	public Boolean isStatusDeleted()			{ return (Status == Message.MsgStatus.DELETED	  );	}
	public Boolean isStatusDelivered()			{ return (Status == Message.MsgStatus.DELIVERED	  );	}
	public Boolean isStatusTranslating()		{ return (Status == Message.MsgStatus.TRANSLATING );	}
	public Boolean isStatusScheduled()			{ return (Status == Message.MsgStatus.SCHEDULED   );	}

	//RichData
//	public Boolean isRichDataPicture()			{ return ( getRichData().isPicture()  );		}
//	public Boolean isRichDataVideo()			{ return ( getRichData().isVideo()    );		}
//	public Boolean isRichDataLocation()			{ return ( getRichData().isLocation() );		}
//	public Boolean isRichDataContact()			{ return ( getRichData().isContact()  );		}
//	public Boolean isRichDataFile()				{ return ( getRichData().isFile()	  );		}

		//Used to provide best Display Name.
	//	If we have messages from unknown contacts, normal options are not sufficient.
	public String bestDisplayName()
	{
		//TODO-GM: Change logic for GM
		String result = Name;

		if ( String.IsNullOrEmpty( result ) )
		{
			result = Number;
		}

		if ( String.IsNullOrEmpty( result ) )
		{
			result = (Message.dirIsInbound( Direction ) ? FromDid : ToDid);
		}

		if ( String.IsNullOrEmpty( result ) )
		{
			result = "Unknown";				//TODO: Localize
		}

		return result;
	}

	//Used to provide best query for messages based on phoneNumbers.
	//	If we have messages from unknown contacts, the PhoneNumber member is not sufficient.
	public String bestPhoneNumber()
	{
		String result = Number;

		if ( String.IsNullOrEmpty( result ) )
		{
			result = (Message.dirIsInbound( Direction ) ? FromDid : ToDid);
		}

		return result;
	}

	public bool canBeMarkedRead()
	{
		bool result = false;

		if ( !isStatusDeleted() )
		{
			if ( isInbound() )
			{
				result = isStatusNew();
			}
			else
			{
				//outbound
//				result = isStatusSent() || isStatusDelivered();	//JRT - I think was due to mismarked direction on VM/RC.  Am commenting to test that.
			}
		}

		return result;
	}

	public bool isUnreadVoicemail()
	{
		bool result = false;

		if ( isVoicemail() )
		{
			result = !isStatusRead();
		}

		return result;
	}

#endregion

};

//=============================================================================

public class UmwMessageList : System.Collections.Generic.List<ManagedDataTypes.UmwMessage>
{
	public UmwMessage getLastReadMessage()
	{
		UmwMessage result = new UmwMessage();

		foreach ( UmwMessage msg in this )
		{
			//We only care about CHAT, since that is only type that has/supports READ status
			//	This is so, even though we mark other types (Recorded Call, Fax, etc. as read)
			//	We can easily extend this if need be.
			if ( msg.isChat() )
			{
				if ( msg.isOutbound() )
				{ 
					if ( msg.Status == Message.MsgStatus.READ )
					{ 
						//We use ServerTimestamp because getMessageHistory() does not provide other timestamps.
						if ( msg.ServerTimestamp.AsLong() > result.ServerTimestamp.AsLong() )
						{
							result = msg;
						}
					}
				}
			}
		}

		return result;
	}
};

//=============================================================================



//=============================================================================
// VoxoxContact class
//	- This class does not have direct DB table corollary
//	- This data is received from main app and used to update DB.
//=============================================================================

public class VoxoxContact 
{
	public VoxoxContact()
    {
        UserId = 0;
    }

	public VoxoxContact( String contactKey, String regNum, String userName, String did, Int32 userId, String phoneLabel, Boolean isBlocked, Boolean isFavorite, String source )
    {
		ContactKey			= contactKey;
        RegistrationNumber  = regNum;
        UserName            = userName;
        Did                 = did;
        UserId              = userId;
		PhoneLabel			= phoneLabel;
		IsBlocked			= isBlocked;
		IsFavorite			= isFavorite;
		Source				= source;
    }

	public VoxoxContact( String contactKey, String number, bool isBlocked, bool isFavorite, String source )
    {
		ContactKey			= contactKey;
        Did                 = number;
		Source				= source;
		IsBlocked			= isBlocked;
		IsFavorite			= isFavorite;
    }

	//Properties ---------------------------------------------------------------------
	public String  ContactKey		            { get;  set;    }     //Required for unmanaged class
	public String  RegistrationNumber           { get;  set;    }     //Required for unmanaged class
	public String  UserName                     { get;  set;    }     //Required for unmanaged class
	public String  Did                          { get;  set;    }     //Required for unmanaged class
	public Int32   UserId                       { get;  set;    }     //Required for unmanaged class
	public String  PhoneLabel                   { get;  set;    }     //Required for unmanaged class
	public Boolean IsBlocked                    { get;  set;    }     //Required for unmanaged class
	public Boolean IsFavorite                   { get;  set;    }     //Required for unmanaged class
	public String  Source                       { get;  set;    }     //Required for unmanaged class
};

//=============================================================================

public class VoxoxContactList : System.Collections.Generic.List<ManagedDataTypes.VoxoxContact>
{
//	VoxoxContactList()		{}
//	~VoxoxContactList()		{}
};

//=============================================================================



//=============================================================================
// VoxoxPhoneNumber class
//	- This class does not have direct DB table corollary
//	- This data is received from main app and used to update DB.
// Currently a simple association of UseName (XMPP JID) and Voxox DID
//=============================================================================

public class VoxoxPhoneNumber 
{
//    public VoxoxPhoneNumber() { }
	public VoxoxPhoneNumber( String did, String jid )
    {
        Did = did;
        Jid = jid;
    }

	//Properties ---------------------------------------------------------------------
	public String Did               { get;  set;    }     //Required for unmanaged class
	public String Jid               { get;  set;    }     //Required for unmanaged class
};

//=============================================================================

public class VoxoxPhoneNumberList : System.Collections.Generic.List<ManagedDataTypes.VoxoxPhoneNumber>
{
//	VoxoxPhoneNumberList()		{}
//	~VoxoxPhoneNumberList()		{}
};

//=============================================================================


//=============================================================================
// XmppContact class
//	- This class does not have direct DB table corollary
//	- This data is received from main app and used to update DB.
//=============================================================================

public class XmppContact 
{
	public XmppContact()    {}

	//Properties ---------------------------------------------------------------------
	public String Jid                   { get;  set;    }     //Required for unmanaged class    // username@voxox.com
	public String Name                  { get;  set;    }     //Required for unmanaged class    // user chosen name (alias), if null use name portion of JID
	public String Status                { get;  set;    }     //Required for unmanaged class    // available/unavailable/chat/away/dnd/xa	//TODO: This is normally an int and translated to text.
	public String MsgState              { get;  set;    }     //Required for unmanaged class    // contact-set status message
};

//=============================================================================


//=============================================================================
// ContactMessageSummary class
//	- Provide Summary of each message thread for Message Thread window
//=============================================================================

public class ContactMessageSummary
{
	private string cmGroupName;

	public ContactMessageSummary()
    {
        CmGroup     = Contact.INVALID_CMGROUP;
        GroupStatus = GroupMessageGroup.GmgStatus.ACTIVE;
		MsgDir      = Message.MsgDirection.INBOUND;
        MsgType     = Message.MsgType.UNKNOWN;
    }

	//Properties ---------------------------------------------------------------------
	public String					    ContactKey              { get;  set;    }     //Required for unmanaged class
	public Int32					    CmGroup                 { get;  set;    }     //Required for unmanaged class
	public String			            DisplayName             { get;  set;    }     //Required for unmanaged class
	public String			            PhoneNumber             { get;  set;    }     //Required for unmanaged class
	public String			            DisplayNumber           { get;  set;    }     //Required for unmanaged class
	public String			            XmppJid                 { get;  set;    }     //Required for unmanaged class

	public String						CmGroupName									  //Required for unmanaged class
	{ 
		get { return cmGroupName;  }
		set { cmGroupName = Localization.replaceOrPlaceholder( value ); }
    }    

	//For GM
	public String				        ToGroupId               { get;  set;    }     //Required for unmanaged class
	public String				        GroupName               { get;  set;    }     //Required for unmanaged class
	public GroupMessageGroup.GmgStatus  GroupStatus             { get;  set;    }     //Required for unmanaged class

	//Newest Message Info
	public Message.MsgDirection	        MsgDir                  { get;  set;    }     //Required for unmanaged class
	public Message.MsgType		        MsgType                 { get;  set;    }     //Required for unmanaged class
	public VxTimestamp			        MsgServerTimestamp      { get;  set;    }     //Required for unmanaged class
	public String			            MsgBody                 { get;  set;    }     //Required for unmanaged class
	public String			            MsgTranslated           { get;  set;    }     //Required for unmanaged class
	public String			            MsgToDid                { get;  set;    }     //Required for unmanaged class
	public String			            MsgFromDid              { get;  set;    }     //Required for unmanaged class
	public String			            MsgToJid                { get;  set;    }     //Required for unmanaged class
	public String			            MsgFromJid              { get;  set;    }     //Required for unmanaged class

	//Used to provide best Display Name.
	//	If we have messages from unknown contacts, normal options are not sufficient.
	public String bestDisplayNameForMessageSummary( out bool nameIsPhoneNumber )
	{
		String result = CmGroupName;

		nameIsPhoneNumber = false;

		if ( String.IsNullOrEmpty( result ) )
		{
			result = DisplayName;
		}

		if ( String.IsNullOrEmpty( result ) )
		{
			result = DisplayNumber;
			nameIsPhoneNumber = true;
		}

		if ( String.IsNullOrEmpty( result ) )
		{
			result = PhoneNumber;
			nameIsPhoneNumber = true;
		}

		if ( String.IsNullOrEmpty( result ) )
		{
			result = (Message.dirIsInbound( MsgDir ) ? MsgFromDid : MsgToDid);
			nameIsPhoneNumber = true;
		}

		if ( String.IsNullOrEmpty( result ) )
		{
			result = "Unknown";				//TODO: Localize
		}

		return result;
	}

	//Used to provide best query for messages based on phoneNumbers.
	//	If we have messages from unknown contacts, the PhoneNumber member is not sufficient.
	public String bestPhoneNumber()
	{
		String result = PhoneNumber;

		if ( String.IsNullOrEmpty( result ) )
		{
			result = (Message.dirIsInbound( MsgDir ) ? MsgFromDid : MsgToDid);
		}

		return result;
	}
};

//=============================================================================

public class ContactMessageSummaryList : System.Collections.Generic.List<ManagedDataTypes.ContactMessageSummary>
{
//	ContactMessageSummaryList()			{}
//	~ContactMessageSummaryList()		{}
};

//=============================================================================



//=============================================================================
// DeleteMessageData class
//	- Data needed to delete messages on server
//	- This class must be defined AFTER Messge class since it uses Message::Type, etc.
//=============================================================================

public class DeleteMessageData
{
	public DeleteMessageData()
    {
        MsgType   = Message.MsgType.UNKNOWN;
        MsgStatus = Message.MsgStatus.UNKNOWN;
        Success   = false;
    }

	public DeleteMessageData( String msgId, Message.MsgType type, Message.MsgStatus status )
    {
        MsgId     = msgId;
        MsgType   = type;
        MsgStatus = status;
    }

	//Properties ---------------------------------------------------------------------
	public String		        MsgId   	        { get;  set;    }     //Required for unmanaged class
	public String		        SvrMsgType          { get;  set;    }     //Required for unmanaged class
	public Message.MsgType	    MsgType	            { get;  set;    }     //Required for unmanaged class
	public Message.MsgStatus    MsgStatus	        { get;  set;    }     //Required for unmanaged class
	public Boolean				Success	            { get;  set;    }     //Required for unmanaged class
};

//=============================================================================

public class DeleteMessageDataList : System.Collections.Generic.List<ManagedDataTypes.DeleteMessageData>
{
//	DeleteMessageDataList()		{}
//	~DeleteMessageDataList()	{}
};

//=============================================================================


//=============================================================================

public class UnreadCounts
{
	public UnreadCounts()
	{
		ChatCount			= 0;
		SmsCount			= 0;
		GmCount				= 0;
		FaxCount			= 0;
		VoicemailCount		= 0;
		RecordedCallCount   = 0;
		MissedCallCount		= 0;
	}

	//Properties ---------------------------------------------------------------------
	public Int32 ChatCount  	        { get;  set;    }     //Required for unmanaged class
	public Int32 SmsCount	  	        { get;  set;    }     //Required for unmanaged class
	public Int32 GmCount	  	        { get;  set;    }     //Required for unmanaged class
	public Int32 FaxCount	  	        { get;  set;    }     //Required for unmanaged class
	public Int32 VoicemailCount    	    { get;  set;    }     //Required for unmanaged class
	public Int32 RecordedCallCount  	{ get;  set;    }     //Required for unmanaged class
	public Int32 MissedCallCount    	{ get;  set;    }     //Required for unmanaged class

	public Int32 getMessageCount()
	{
		return ChatCount + SmsCount + GmCount;
	}
};

//=============================================================================


//=============================================================================

public class UserSettings
{
	public UserSettings()
	{
		AudioInputVolume		= 0;
		AudioOutputVolume		= 0;

		ShowFaxInMsgThread		= true;
		ShowVmInMsgThread		= true;
		ShowRcInMsgThread		= true;

		EnableSoundIncomingMsg	= true;
		EnableSoundIncomingCall	= true;
//		EnableSoundOutgoingMsg	= true;

		WindowLeft				= 0;
		WindowTop				= 0;
		WindowHeight			= 0;
		WindowWidth				= 0;
		WindowState				= 0;		//TODO: Get WindowState Enum

		LastMsgTimestamp		= 0;
		LastCallTimestamp		= 0;
	}

	//Properties ---------------------------------------------------------------------
	public String	UserId						{ get; set;		}     //Required for unmanaged class

	public String	AudioInputDevice			{ get; set;		}     //Required for unmanaged class
	public String	AudioOutputDevice			{ get; set;		}     //Required for unmanaged class
	public Int32	AudioInputVolume			{ get; set;		}     //Required for unmanaged class
	public Int32	AudioOutputVolume			{ get; set;		}     //Required for unmanaged class

	public Boolean	ShowFaxInMsgThread			{ get; set;		}     //Required for unmanaged class
	public Boolean	ShowVmInMsgThread			{ get; set;		}     //Required for unmanaged class
	public Boolean	ShowRcInMsgThread			{ get; set;		}     //Required for unmanaged class

	public Boolean	EnableSoundIncomingMsg		{ get; set;		}     //Required for unmanaged class
	public Boolean	EnableSoundIncomingCall		{ get; set;		}     //Required for unmanaged class
//	public Boolean	EnableSoundOutgoingMsg		{ get; set;		}     //Required for unmanaged class

	public String	MyChatBubbleColor			{ get; set;		}     //Required for unmanaged class
	public String	ConversantChatBubbleColor	{ get; set;		}     //Required for unmanaged class
	
	public String	CountryCode					{ get; set;		}     //Required for unmanaged class
	public String	CountryAbbrev				{ get; set;		}     //Required for unmanaged class

//	public String	OutgoindCallSoundFile		{ get; set;		}     //Required for unmanaged class
	public String	IncomingCallSoundFile		{ get; set;		}     //Required for unmanaged class
	public String 	CallClosedSoundFile			{ get; set;		}     //Required for unmanaged class

	public Double	WindowLeft					{ get; set;		}     //Required for unmanaged class
	public Double	WindowTop					{ get; set;		}     //Required for unmanaged class
	public Double	WindowHeight				{ get; set;		}     //Required for unmanaged class
	public Double	WindowWidth					{ get; set;		}     //Required for unmanaged class
	public Int32	WindowState					{ get; set;		}     //Required for unmanaged class		//TODO: Get WindowState Enum

	public Int64	LastMsgTimestamp			{ get; set;		}     //Required for unmanaged class
	public Int64	LastCallTimestamp			{ get; set;		}     //Required for unmanaged class
	public String	LastContactSourceKey		{ get; set;		}     //Required for unmanaged class

	public String	SipUuid						{ get; set;		}     //Required for unmanaged class

	public Boolean isValid()
	{
		return !String.IsNullOrEmpty( UserId );
	}
};

//=============================================================================



//=============================================================================
//NOTE: This class is NOT at user-level, but at Application-level
//=============================================================================

public class AppSettings
{
	public AppSettings()
	{
		RecordId				= -1;
		AutoLogin				= false;
		RememberPassword		= false;
		GetInitOptionsHandling	= 0;
	}

	//Properties ---------------------------------------------------------------------
	public Int32	RecordId					{ get; set;		}     //Required for unmanaged class

	public Boolean	AutoLogin					{ get; set;		}     //Required for unmanaged class
	public Boolean	RememberPassword			{ get; set;		}     //Required for unmanaged class
	public String	LastLoginUsername			{ get; set;		}     //Required for unmanaged class
	public String	LastLoginPassword			{ get; set;		}     //Required for unmanaged class
	public String	LastLoginCountryCode		{ get; set;		}     //Required for unmanaged class
	public String	LastLoginPhoneNumber		{ get; set;		}     //Required for unmanaged class

	//Debug items
	public String	DebugMenu					{ get; set;		}     //Required for unmanaged class
	public String	IgnoreLoggers				{ get; set;		}     //Required for unmanaged class
	public Boolean	MaxSipLogging				{ get; set;		}     //Required for unmanaged class
	public int		GetInitOptionsHandling		{ get; set;		}     //Required for unmanaged class
	
	public Boolean	isValid()
	{
		return (RecordId != -1);
	}
};

//=============================================================================


//=============================================================================
// DbChangeData class
//	- DB changes
//=============================================================================

public class DbChangeData
{
	public enum TableId	//Which table has been changed.  Allow listeners to select which tables they care about.
	{
		ANY          = 0,
		MESSAGE      = 1,
		CONTACT      = 2,
        GM           = 3,
        RECENT_CALL  = 4,
		SETTINGS	 = 5,
		APP_SETTINGS = 6
	};

	public enum Action
	{
		NO_ACTION = 0,
		ADD		  = 1,
		EDIT	  = 2,
		DELETE	  = 3
	};

	public DbChangeData()  {}

	//Properties ---------------------------------------------------------------------
	public TableId	            Table		                { get;  set;    }     //Required for unmanaged class

	//Message extras
	public Message.MsgStatus    MsgStatus                   { get;  set;    }     //Required for unmanaged class
	public Int64				MsgCount                    { get;  set;    }     //Required for unmanaged class

	//Contact extras
	public Int32				VoxoxId                     { get;  set;    }     //Required for unmanaged class

	//GM
	public String       		GroupId                     { get;  set;    }     //Required for unmanaged class
	public Action	    		GmAction                    { get;  set;    }     //Required for unmanaged class


    //Methods ---------------------------------------------------------------------------------------------
	public Boolean		    	isGmAdd()                   { return isGmAction( Action.ADD    );  }
	public Boolean	    		isGmEdit()                  { return isGmAction( Action.EDIT   );  }
	public Boolean				isGmDelete()                { return isGmAction( Action.DELETE );  }

	public Boolean              isMsgTable()                { return Table == TableId.MESSAGE;  }
	public Boolean              isContactTable()            { return Table == TableId.CONTACT;  }
	public Boolean              isGmTable()                 { return Table == TableId.GM;       }
    public Boolean              isRecentCallTable()         { return Table == TableId.RECENT_CALL;	}
    public Boolean              isSettingsTable()			{ return Table == TableId.SETTINGS;		}
    public Boolean              isAppSettingsable()         { return Table == TableId.APP_SETTINGS; }

	private Boolean isGmAction( Action tgtAction )
    {
        Boolean result = false;

        if ( isGmTable() )
        {
            result = GmAction == tgtAction;
        }

        return result;
    }
};

//=============================================================================

}   //namespace ManagedDataTypes
