/*
* Voxox
* Copyright (C) 2004-2014 Voxox
*/

#pragma once

#include "..\VoxoxDbManager\DataTypes.h"	//For DbChangeCallbackFunc.  Consider moving that somewhere else.

using System::String;
using System::Boolean;
using System::Int64;
using System::Int32;
using System::EventArgs;
using System::Object;
using System::Runtime::InteropServices::UnmanagedFunctionPointerAttribute;
using System::Runtime::InteropServices::CallingConvention;

//using ManagedDataTypes::Message;	//We INTENTIONALLY do not use the 'using ManagedDataTypes' so we are forced to distinguish between managed and unmanaged classes.

class VoxoxDbManager;	//Fwd declaration
class DbChangeData;

namespace ManagedDbManager
{

public ref class DbChangedEventArgs : EventArgs
{
public:
	DbChangedEventArgs( ManagedDataTypes::DbChangeData^ data )
	{
		mData = data;
	}

	property ManagedDataTypes::DbChangeData^ Data
	{
		ManagedDataTypes::DbChangeData^ get()	{ return mData; }
	}

private:
	ManagedDataTypes::DbChangeData^ mData;
};

//Set proper calling convention for delegate used as callback to avoid stack corruption.
[UnmanagedFunctionPointerAttribute( CallingConvention::Cdecl)]	
delegate void DbChangeDelegate( const DbChangeData& );

public delegate void DbChangedEventHandler( Object^ sender, DbChangedEventArgs^ e );

//=============================================================================

public ref class ManagedDbManager
{
public:
	//Do NOT re-order or change these values.  Feel free to append
	enum class DbType
	{
		User = 0,
		App  = 1
	};

	ManagedDbManager( String^ dbName, DbType dbType );

	~ManagedDbManager();

	//Admin methods
	Boolean	checkDatabase();
	Boolean	checkTableExists( String^ tableName);
	Boolean	create();
	String^ getDbName();

	//Callback to detect DB changes.
	void setDbChangeCallback   ();
	void setDbChangeCallback   ( DbChangeCallbackFunc callback );
	void dbChangeCallback      ( const DbChangeData& data );
	void addDbChangeListener   ( DbChangedEventHandler^ dbChangeDelegate );
	void removeDbChangeListener( DbChangedEventHandler^ dbChangeDelegate );

	static Boolean checkDatabase( String^ dbName );

	//These methods help support unit testing.
	void	close();
	void	clearMessageTable();
	void	clearPhoneNumberTable();
	String^	richDataToJsonString( ManagedDataTypes::RichData^ richDataIn );

	static String^ testStringConversion( String^ textIn );

public:
	//---------------------------------------
	// PUBLIC Utility Methods
	//---------------------------------------
	String^ normalizePhoneNumber( String^ phoneNumberIn );
	String^ normalizeJid        ( String^ jidIn			);

	//---------------------------------------
	// PUBLIC Messages Methods
	//---------------------------------------
	/**
	* returns the last inserted server_timestamp, so we can query server for newer messages.
	*/
	Int64 getLatestMessageServerTimestamp(ManagedDataTypes::Message::MsgType type, ManagedDataTypes::Message::MsgDirection direction, ManagedDataTypes::Message::MsgStatus status);

	/**
	* returns Message with given LOCAL timestamp.
	*/
	ManagedDataTypes::Message^ getMessage(Int64 timeStamp);

	/**
	* returns List of messages based on criteria.
	*/
	ManagedDataTypes::MessageList^ getMessages(ManagedDataTypes::Message::MsgStatus messageStatus, ManagedDataTypes::Message::MsgDirection messageDirection);

	/**
	 * returns List of ALL VM and RECORDED CALL messages for ALL CONTACTS, except deleted messages.
	 */
	ManagedDataTypes::UmwMessageList^  getVmAndRecordedCallMessages();

	/**
	 * returns List of ALL FAX messages for ALL CONTACTS, except deleted messages.
	 */
	ManagedDataTypes::UmwMessageList^  getFaxMessages();

	/**
	* returns a cursor to all the messages from a PhoneNumber
	*/
	ManagedDataTypes::UmwMessageList^ getMessagesForPhoneNumber( String^ phoneNumberIn );

		/**
	* returns a cursor to all the messages from an CM Group
	*/
	ManagedDataTypes::UmwMessageList^ getMessagesForCmGroup( Int32 cmGroup );

	/**
	* returns a cursor to all the messages from an GM GroupId
	*/
	ManagedDataTypes::UmwMessageList^ getMessagesForGroupId( String^ groupId );

	/**
	* Description:
	* 	Update Message in DB with RichData.
	*
	*/
	void updateMessageRichData(Int64 timeStamp, ManagedDataTypes::RichData^ richData, Boolean broadcast);

	/**
	* Description:
	* 	Update GM message in DB with RichData.
	* 	We need this separate method because RichData is embedded in body so we cannot simply replace BODY.
	* */
	void updateGroupMessageRichData(Int64 timeStamp, ManagedDataTypes::RichData^ richData, Boolean broadcast, String^ origBody);

	/**
	* Update message status based on LOCAL timestamp
	*/
	void updateMessage(Int64 timeStamp, ManagedDataTypes::Message::MsgStatus status);

	/**
	* Update message status and MessageId based on LOCAL timestamp
	*/
	void updateMessage(Int64 timeStamp, ManagedDataTypes::Message::MsgStatus status, String^ messageId);

	/**
	* Update message status and SERVER timestamp based on LOCAL timestamp
	*/
	void updateMessage(Int64 timeStamp, Int64 serverTimeStamp, ManagedDataTypes::Message::MsgStatus status);

	/**
	* Update message status, SERVER timestamp and MessageId based on LOCAL timestamp
	*/
	void updateMessage(Int64 timeStamp, Int64 serverTimeStamp, ManagedDataTypes::Message::MsgStatus status, String^ messageId);

	/**
	* Update message translation message based on LOCAL timestamp
	*/
	void updateTranslatedMessage(Int64 timeStamp, ManagedDataTypes::Message^ message);

	/**
	* Some times we receive message from contacts not in our address book.
	*
	* We log the original message, then use the phone_number to determine if it is a Voxox user.  This is done asynchrously.
	*
	* This method updates those messages, by calling the single instance version below.
	*/
	void updateMessageContactInfo(ManagedDataTypes::VoxoxContactList^ vcs);

	/**
	* Appears to be a convenience method to udpate message status as DELIVERED
	*/
	void markMessageDelivered( Int64 timeStamp, bool broadcast );

	/**
	* Appears to be a convenience method to udpate message status as READ
	*/
	void markMessageRead(Int64 timeStamp, Boolean broadcast );

	Boolean addMessage( ManagedDataTypes::Message^ message, Boolean broadcast );

	/**
	*	Add list of messages to DB.
	*	Typically done from getMessageHistory() API call.
	*/
	void addMessages( ManagedDataTypes::MessageList^  messages);

	Int32 deleteMessages( ManagedDataTypes::MessageList^ messages);		//TODO: We just need the LOCAL timestamp, so consider just an array of LONGs.

	/*
	* This REMOVES matches records from the DB.  This is used to keep DBs in sync
	*	among various platforms.  These messages come from getMessageHistory()
	*/
	Int32 removeDeletedMessages( ManagedDataTypes::MessageList^ messages);		//TODO: We just need the LOCAL timestamp, so consider just an array of LONGs.

	Boolean hasDeletedMessages();

	Boolean hasMessageId( String^ msgId );

	Int64 getLocalTimestampForMsgId( String^ msgId );


	/*
	*	Gathers data needed to delete messages from the server.
	*
	*	This looks for all local messages with status DELETED.
	*	Once this data is used in the API call to delete the messages on server,
	*	a separate method will delete these from the local DB.
	*/
	ManagedDataTypes::DeleteMessageDataList^ getMessageDataForDeletedMessages();

	/**
	* Utility method get RichData object from JSON string so managed code does not have to 
	*	duplicate the JSON parsing
	*/
	ManagedDataTypes::RichData^ richDataFromJsonText( String^ jsonText );

	/**
	* Retrieve unread item counts for:
	*	- Missed calls
	*	- Messages (IM, Chat, and GM)
	*	- Fax;
	*	- Voicemail
	*/
	ManagedDataTypes::UnreadCounts^ ManagedDbManager::getUnreadItemCounts();

	/**
	* Retrieve unread item counts for:
	*	- Missed calls
	*	- Messages (IM, Chat, and GM) ( with option to include VM, RC or Fax message counts )
	*	- Fax;
	*	- Voicemail
	*/
	ManagedDataTypes::UnreadCounts^ ManagedDbManager::getUnreadItemCounts( bool includeVoicemail, bool includeRecordedCalls, bool includeFaxes );

public:
	//---------------------------------------
	// PUBLIC Calls Methods
	//---------------------------------------

	/**
	* Add or updated Call to DB.
	*/
	Boolean addOrUpdateCall(ManagedDataTypes::RecentCall^ callInIn, Boolean broadcastIn );

	/**
	* Add or updated Calls to DB.
	*/
	void addOrUpdateCalls( ManagedDataTypes::RecentCallList^  callistIn );

	/**
	* Add or updated Calls to DB.
	*/
	bool callExists( String^ uid );


	//---------------------------------------
	// PUBLIC Contact/PhoneNumber Methods
	//---------------------------------------
public:
	void addAddressBookContacts( ManagedDataTypes::ContactImportList^ contacts );		//TODO: May be Contacts/PhoneNumbers

	/**
	* Add or updated DB based on VoxoxContact info.
	*/
	void addOrUpdateVoxoxContacts( ManagedDataTypes::VoxoxContactList^ vcList );

	/**
	* Add naked number to DB based on VoxoxContact info.
	*/
	void addNakedNumbers( ManagedDataTypes::VoxoxContactList^ vcList );

	/**
	* Update PhoneNumber table with XMPP contact info, based on Voxox PhoneNumber
	* Simply calls singular version below.
	*/
//	void addOrUpdateXmppContacts( ManagedDataTypes::VoxoxPhoneNumberList^ voxoxPhoneNumberList );

	/**
	 * Description:
	 * Add OR update an XMPP contact to the phone number table. an XMPP contact may not have an AB contactKey associated with it. it can happen if we
	 *	received a CHAT message from a VoxOx user that is not in our address book. The contact will be added to the phone number table with a unique common group (cm_group).
	 */
	void addOrUpdateXmppContact( String^ xmppAddress, String^ phoneNumber, String^ contactKey, String^ source );

	/**
	 * Description:
	 * update the presence and the status of an XMPP contact.
	 * status values may be: UNAVAILABLE, AVAILABLE, AVAILABLE_AWAY ...
	 * presence: a status message of the presence update, or null if there is not a status. The status is free-form text describing a user's presence (i.e., "gone to lunch").
	 * */
	void updateXmppPresence( ManagedDataTypes::XmppContact^ xc );

	/**
	 * Description:
	 * update the new favorite status of a phone number entry.
	 * */
	void updatePhoneNumberFavorite( String^ phoneNumber, Boolean newValue );

	/**
	 * Description:
	 * Returns a cursor that is UNION of phone numbers AND GM Groups that match the filter.
	 *
	 * As we group by the ContactKey (we want the contact), we may loss the VoxOx flag of one of the phone numbers
	 * so we added a little trick in the query to count if any VoxOx numbers and added a field to the table called VOXOX_COUNT.
	 *
	 * Note:
	 * it is the responsibility of the calling function to close the cursor.
	 * Used by the CONTACTS tab.
	 * */
	ManagedDataTypes::Contact^     getContactByKey( String^ contactKey );	

	ManagedDataTypes::ContactList^ getContacts( String^ searchFilter, ManagedDataTypes::ContactFilter contactFilter );	

	ManagedDataTypes::PhoneNumberList^ ManagedDbManager::getContactPhoneNumbers( String^ searchFilterIn, ManagedDataTypes::ContactFilter contactFilterIn )	;

	/**
	 * Description:
	 * */
	ManagedDataTypes::PhoneNumberList^ getAllContactDetailsPhoneNumbers();

	/**
	 * Description:
	 * */
	ManagedDataTypes::PhoneNumberList^ getContactDetailsPhoneNumbersByKey( String^ contactkey );

	/**
	 * Description:
	 * */
	ManagedDataTypes::PhoneNumberList^ getContactDetailsPhoneNumbersByCmGroup( Int32 cmGroup );

	/**
	 * Description:
	 * returns a list of PhoneNumber objects associated with the CmGroup.
	 * */
	ManagedDataTypes::PhoneNumberList^ getContactPhoneNumbersListForCmGroup( Int32 cmGroup, Boolean isVoxox );

	/**
	 * Description: Get phoneNumber object for given phoneNumbe string and/or CMGroup
	 * Used in UMW and notification
	 * */
	ManagedDataTypes::PhoneNumber^ getPhoneNumber( String^ phoneNumberString,  Int32 cmGroup );

	/**
	* Determine if PhoneNumbers exists in DB.
	*/
	bool phoneNumberExists( String^ phoneNumberStringIn );

	/**
	* Determine if PhoneNumber is an extension.
	*/
	bool isNumberAnExtension( String^ phoneNumberStringIn );

	/**
	* Get Voxox userId for given phoneNumber string.
	*/
	Int32 getVoxoxUserId( String^ phoneNumberString );

	/**
	* Get Voxox userId for given CMGroup.	//TODO: I don't think this logic holds since a CMGroup may have more than one VoxoxID (one contact with multiple Voxox numbers).
	*/
	Int32 getVoxoxUserId( Int32 cmGroup);

	/**
	* Get CmGroup for given phoneNumber string.
	*/
	Int32 getCmGroup( String^ phoneNumberString );

	/**
	* Get DID for given JID
	*/
	String^ getDidByJid( String^ jid );

	/**
	 * Description:
	 * returns a list of VoxOx (XMPP) names (from CHAT messages) that don't have an associated phone number in the phone number table.
	 * */
	ManagedDataTypes::StringList^ getVoxoxUserNamesWithNoAssociatedPhoneNumbers();

	/**
	* Determine if given XMPP JID is in our DB
	*/
	Boolean isXmppContactInLocalAddressBook( String^ jid );

	/**
	 * returns number of new in-bound messages for a CM Group
	 * This is called in for each Contact in ContactsMessages (or its array/cursor adapter).  So could be called a lot, especially when scrolling.
	 */
	Int32 getNewInboundMessageCountForCmGroup( Int32 cmGroup );

	/**
	* returns number of new in-bound messages for a CM Group with option to include VM, RC or Fax message counts
	*/
	Int32 getNewInboundMessageCountForCmGroup( Int32 cmGroup, bool includeVoicemail, bool includeRecordedCalls, bool includeFaxes );

	/**
	 * returns number of new in-bound messages for a PhoneNumber string or JID.
	 *
	 * (I am not sure but...)This is called in for each Contact in ContactsMessages (or its array/cursor adapter).  So could be called a lot, especially when scrolling.
	 */
	Int32 getNewInboundMessageCount( String^ phoneNumber, String^ xmppAddress );

	/**
	* returns number of new in-bound messages for a PhoneNumber string or JID with option to include VM, RC or Fax message counts.
	*/
	Int32 getNewInboundMessageCount( String^ phoneNumber, String^ xmppAddress, bool includeVoicemail, bool includeRecordedCalls, bool includeFaxes );

	/**
	* Return list of ContactMessage objects.
	* Used on main Messages window.
	*/
	ManagedDataTypes::ContactMessageSummaryList^ getContactsMessageSummary();

	/**
	* Return list of ContactMessage objects with option to include VM, RC or Fax messages.
	* Used on main Messages window.
	*/
	ManagedDataTypes::ContactMessageSummaryList^ getContactsMessageSummary( bool includeVoicemail, bool includeRecordedCalls, bool includeFaxes );

	/**
	 * returns number of new in-bound messages for a GM GroupId
	 *
	 * This is called in for each Contact in ContactsMessages (or its array/cursor adapter).  So could be called a lot, especially when scrolling.
	 */
	Int32 getNewInboundMessageCountForGroupId( String^ groupId );

	/**
	* returns number of new in-bound messages for a GM GroupId with option to include VM, RC or Fax message counts.
	*/
	Int32 getNewInboundMessageCountForGroupId( String^ groupId, bool includeVoicemail, bool includeRecordedCalls, bool includeFaxes );

	ManagedDataTypes::MessageTranslationProperties^ getTranslationModeProps( String^ phoneNumber, String^ jid );

	/**
	 * Add translation mode to a contact. What required is wither the phone number of jid.
	 * CONTACT_ID should -NOT- be used for detecting a contact as it may change when user refresh the contacts list.
	 * returns true for success.
	 * */
	Boolean addContactMessageTranslation( ManagedDataTypes::MessageTranslationProperties^ mtp );

	/**
	 * Get all phoneNumber that have an invalid CmGroup.  These indicates they are NOT in our AB
	 *	and we need to check if they are InNetwork or not (via separate API call)
	*/
	ManagedDataTypes::StringList^ getPhoneNumbersWithInvalidCmGroup();

	//---------------------------------------
	// PUBLIC RecentCall Methods
	//---------------------------------------

	/**
	* Add new call to DB. Return DB recordId.
	*	This will be called when an incoming call event is detected or 
	*	user makes an outgoing call.
	*/
	Int32 addRecentCall( String^ phoneNumber, Int64 startTime, Boolean isIncoming ); // Call for compatibility
	Int32 addRecentCall( String^ phoneNumber, Int64 startTime, Boolean isIncoming, Boolean isLocal );

	//For unit tests only.  DO NOT CALL THIS IN APP.
	Int32 addRecentCall( String^ phoneNumber, Boolean isIncoming, ManagedDataTypes::RecentCall::CallStatus status, Int64 startTime );

	/**
	* Update call status on Answer, Decline, or Hangup (even for unanswered call)
	*/
	void updateRecentCallAnswered  ( Int32 callId );
	void updateRecentCallDeclined  ( Int32 callId );
	void updateRecentCallTerminated( Int32 callId );

	//Update Seen Flag.
	bool updateRecentCallSeen( Int32 callId );


	/**
	* returns the last inserted server_timestamp, so we can query server for newer calls.
	*/
	Int64 getLatestCallServerTimestamp();

	/**
	* Delete call - TODO: this may have to be 2-phase, like deleting messages is. Depends on sending call history to server.
	*/
	Int32 deleteRecentCall( Int32 callId );

	/**
	* Retrieve a single recent call by callId
	*/
	ManagedDataTypes::RecentCall^ getRecentCall( Int32 callId );

	ManagedDataTypes::RecentCall^ getRecentCallByUid( String^ uid );

	/**
	* Retrieve a list of recent calls by phoneNumber
	*/
	ManagedDataTypes::RecentCallList^ getRecentCalls( String^ phoneNumber );

	/**
	* Retrieve a list of recent calls by cmGroup
	*/
	ManagedDataTypes::RecentCallList^ getRecentCalls( Int32 cmGroup );

	/**
	* Clears all calls marked with "local" flag
	*/
	Int32 clearLocalCalls();

	/**
	* DbChange event handler
	*/
	void onDbChanged( DbChangedEventArgs^ e );

	//----------------------------------------
	//PUBLIC Settings methods
	//----------------------------------------
	ManagedDataTypes::UserSettings^ getUserSettings();
	bool							updateUserSettings( ManagedDataTypes::UserSettings^ src );

	//---------------------------------------
	// PUBLIC Miscellaneous calls
	//---------------------------------------
	void broadcastMessagesDatasetChanged();
	void broadcastRecentCallDatasetChanged();

	//----------------------------------------
	//PUBLIC APP Settings methods - NOT PART of USER DB
	//----------------------------------------
	ManagedDataTypes::AppSettings^ getAppSettings();
	bool						   updateAppSettings( ManagedDataTypes::AppSettings^ src );


private:
	VoxoxDbManager*		mDbm;
	DbChangeDelegate^	mDbChangeDelegate;

    event DbChangedEventHandler^ mDbChangedEventHandler;
};

}	//namespace ManagedDbManager
