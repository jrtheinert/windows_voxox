/*
* Voxox
* Copyright (C) 2004-2014 Voxox
*/

#pragma once

#include "..\VoxoxDbManager\DataTypes.h"
#include "..\VoxoxDbManager\StringList.h"

#include <string>

using System::String;

ref class Conversion
{
public:
	//Helper methods to convert data.
	static std::string										M2U_String( String^            textIn );
	static String^											U2M_String( const std::string& textIn );

	static ManagedDataTypes::StringList^					U2M_StringList( const StringList& stdList );

	 
	//Class conversion methods.
	//Bi-directional: managed -> unmanaged and unmanaged -> managed
	static ManagedDataTypes::Message^						U2M_Message(const Message& msgIn);
	static Message											M2U_Message( ManagedDataTypes::Message^ msgIn);

	static ManagedDataTypes::MessageTranslationProperties^	U2M_MessageTranslationProperties( const MessageTranslationProperties& mtpIn );
	static MessageTranslationProperties						M2U_MessageTranslationProperties( ManagedDataTypes::MessageTranslationProperties^ mtpIn );

	static ManagedDataTypes::RichData^						U2M_RichData( const RichData&			  dataIn );
	static RichData											M2U_RichData( ManagedDataTypes::RichData^ dataIn );

	static ManagedDataTypes::RecentCall^					U2M_RecentCall    ( const RecentCall&             rcIn );
	static RecentCall										M2U_RecentCall    ( ManagedDataTypes::RecentCall^ rcIn );

	static ManagedDataTypes::RecentCallList^				U2M_RecentCallList( const RecentCallList&             rcListIn );
	static RecentCallList									M2U_RecentCallList( ManagedDataTypes::RecentCallList^ rcListIn );

	static ManagedDataTypes::UserSettings^					U2M_UserSettings( const UserSettings&			  settingsIn );
	static UserSettings										M2U_UserSettings( ManagedDataTypes::UserSettings^ settingsIn );

	//This is NOT part of USER DB
	static ManagedDataTypes::AppSettings^					U2M_AppSettings( const AppSettings&			    settingsIn );
	static AppSettings										M2U_AppSettings( ManagedDataTypes::AppSettings^ settingsIn );

	//From unmanaged -> managed
	static ManagedDataTypes::DbChangeData^					U2M_DbChangeData( const DbChangeData& dataIn );

	static ManagedDataTypes::Contact^						U2M_Contact    ( const Contact&     contact     );
	static ManagedDataTypes::ContactList^					U2M_ContactList( const ContactList& contactList );

	static ManagedDataTypes::ContactMessageSummary^			U2M_ContactMessageSummary    ( const ContactMessageSummary&     summary     );
	static ManagedDataTypes::ContactMessageSummaryList^		U2M_ContactMessageSummaryList( const ContactMessageSummaryList& summaryList );

	static ManagedDataTypes::DeleteMessageData^				U2M_DeleteMessageData    ( const DeleteMessageData&     dataIn );
	static ManagedDataTypes::DeleteMessageDataList^			U2M_DeleteMessageDataList( const DeleteMessageDataList& listIn );
	
	static ManagedDataTypes::PhoneNumber^					U2M_PhoneNumber    ( const PhoneNumber&     pnIn     );
	static ManagedDataTypes::PhoneNumberList^				U2M_PhoneNumberList( const PhoneNumberList& pnListIn );

	static ManagedDataTypes::UmwMessage^					U2M_UmwMessage    ( const UmwMessage&      msgIn     );
	static ManagedDataTypes::UmwMessageList^				U2M_UmwMessageList( const UmwMessageList&  msgListIn );

	static ManagedDataTypes::UnreadCounts^					U2M_UnreadCounts( const UnreadCounts&	dataIn );

	//From managed -> unmanaged
	static ContactImport									M2U_ContactImport    ( ManagedDataTypes::ContactImport^     contactIn  );
	static ContactImportList								M2U_ContactImportList( ManagedDataTypes::ContactImportList^ contactsIn );

	static MessageList										M2U_MessageList( ManagedDataTypes::MessageList^ messagesIn);

	static VoxoxContact										M2U_VoxoxContact    ( ManagedDataTypes::VoxoxContact^     vcIn     );
	static VoxoxContactList									M2U_VoxoxContactList( ManagedDataTypes::VoxoxContactList^ vcListIn );

	static VoxoxPhoneNumber									M2U_VoxoxPhoneNumber    ( ManagedDataTypes::VoxoxPhoneNumber^     voxoxPhoneNumberIn );
	static VoxoxPhoneNumberList								M2U_VoxoxPhoneNumberList( ManagedDataTypes::VoxoxPhoneNumberList^ voxoxPhoneNumberListIn );

	static XmppContact										M2U_XmppContact( ManagedDataTypes::XmppContact^ xcIn );

private:
		Conversion();
};

