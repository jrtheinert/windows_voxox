/*
* Voxox
* Copyright (C) 2004-2014 Voxox
*/

#include "stdafx.h"
#include "Conversion.h"

using namespace System;
using namespace System::Text;	//UTF8Encoding

Conversion::Conversion()
{
}

//-----------------------------------------------------------------------------
//Static helpers - These may be better in a utility class, but OK here for now.
//-----------------------------------------------------------------------------

std::string Conversion::M2U_String(String^ textIn)
{
	using namespace System::Runtime::InteropServices;

	std::string result;

	if ( textIn != nullptr )
	{
		array<Byte>^ encodedBytes;
		pin_ptr<Byte> pinnedBytes;

		try
		{
			// Encode the text as UTF8 which SQLite supports
			encodedBytes = Encoding::UTF8->GetBytes(textIn);		//No BOM in this encoding

			// prevent GC moving the bytes around while this variable is on the stack
			if ( encodedBytes->Length > 0 )
			{
				pinnedBytes = &encodedBytes[0];

				result = reinterpret_cast<char*>(pinnedBytes);
			}
		}
		catch( ... )
		{
			int xxx = 1;
		}
	}

	return result;
}

//static
String^ Conversion::U2M_String( const std::string& textIn )
{
	using namespace System::Runtime::InteropServices;

	String^ result = gcnew String( "" );

	if ( textIn.size() > 0 )
	{
		array<Byte>^ bytes1 = gcnew array<Byte>( textIn.size() );
		
		//convert native pointer to System::IntPtr with C-Style cast
		Marshal::Copy((IntPtr)(char*)textIn.c_str(), bytes1, 0, textIn.size() );

		result = Encoding::UTF8->GetString( bytes1 );

		if ( textIn.size() > 5 )
			int xxxx = 1;
	}

	return result;
}

//static
ManagedDataTypes::StringList^ Conversion::U2M_StringList( const StringList& listIn )	//TODO
{
	ManagedDataTypes::StringList^ listOut = gcnew ManagedDataTypes::StringList;

	for ( std::string strIn : listIn )
	{
		listOut->Add( U2M_String( strIn ) );
	}

	return listOut;
}

//TODO: Add conversion methods for enums in C# vs. unmananged code.
//	We should be OK for now, but this will insulate us from potential mismatches going forward.
//	- Message::Direction
//	- Message::Status
//	- Message::Type
// MORE TO COME

//static
ManagedDataTypes::Message^ Conversion::U2M_Message( const Message& msgIn )
{
	ManagedDataTypes::Message^ msgOut = gcnew ManagedDataTypes::Message;

	msgOut->RecordId  = msgIn.getRecordId();
	msgOut->ThreadId  = msgIn.getThreadId();
	msgOut->Type	  = (ManagedDataTypes::Message::MsgType)msgIn.getType();
	msgOut->Direction = (ManagedDataTypes::Message::MsgDirection)msgIn.getDirection();
	msgOut->Status	  = (ManagedDataTypes::Message::MsgStatus)msgIn.getStatus();
	msgOut->MsgId	  = U2M_String( msgIn.getMsgId() );

	msgOut->Body				= U2M_String(msgIn.getBody() );
	msgOut->TranslateBody		= U2M_String(msgIn.getTranslateBody() );
	msgOut->InboundLang			= U2M_String(msgIn.getInboundLang() );
	msgOut->OutboundLang		= U2M_String(msgIn.getOutboundLang() );

	msgOut->LocalTimestamp		= msgIn.getLocalTimestamp();
	msgOut->ServerTimestamp		= gcnew ManagedDataTypes::VxTimestamp( msgIn.getServerTimestamp(), false );
	msgOut->ReadTimestamp		= msgIn.getReadTimestamp();
	msgOut->DeliveredTimestamp	= msgIn.getDeliveredTimestamp();

	msgOut->ContactDid			= U2M_String(msgIn.getContactDid());

	msgOut->FromDid				= U2M_String(msgIn.getFromDid());
	msgOut->ToDid				= U2M_String(msgIn.getToDid());

	msgOut->FromJid				= U2M_String(msgIn.getFromJid());
	msgOut->ToJid				= U2M_String(msgIn.getToJid());

	msgOut->ToGroupId			= U2M_String(msgIn.getToGroupId());
	msgOut->FromUserId			= msgIn.getFromUserId();

	msgOut->FileLocal			= U2M_String(msgIn.getFileLocal());
	msgOut->ThumbnailLocal		= U2M_String(msgIn.getThumbnailLocal());
	msgOut->Duration			= msgIn.getDuration();

	msgOut->Modified			= U2M_String(msgIn.getModified());
	msgOut->IAccount			= U2M_String(msgIn.getIAccount());
	msgOut->CName				= U2M_String(msgIn.getCName());

	//RichData  //TODO?
	return msgOut;
}

//static
Message Conversion::M2U_Message( ManagedDataTypes::Message^ msgIn )
{
	Message msgOut;

	msgOut.setRecordId ( msgIn->RecordId					  );
	msgOut.setThreadId ( msgIn->ThreadId					  );
	msgOut.setType     ( (Message::Type)msgIn->Type			  );
	msgOut.setDirection( (Message::Direction)msgIn->Direction );
	msgOut.setStatus   ( (Message::Status)msgIn->Status		  );
	msgOut.setMsgId    ( M2U_String(msgIn->MsgId)	  );

	msgOut.setBody         ( M2U_String(msgIn->Body)			 );
	msgOut.setTranslateBody( M2U_String(msgIn->TranslateBody) );
	msgOut.setInboundLang  ( M2U_String(msgIn->InboundLang)	 );
	msgOut.setOutboundLang ( M2U_String(msgIn->OutboundLang)	 );

	msgOut.setLocalTimestamp	( msgIn->LocalTimestamp		);
	msgOut.setServerTimestamp	( msgIn->ServerTimestamp->AsLong() );
	msgOut.setReadTimestamp		( msgIn->ReadTimestamp		);
	msgOut.setDeliveredTimestamp( msgIn->DeliveredTimestamp );

	msgOut.setContactDid	( M2U_String(msgIn->ContactDid)	);

	msgOut.setFromDid		( M2U_String(msgIn->FromDid)		);
	msgOut.setToDid			( M2U_String(msgIn->ToDid)		);

	msgOut.setFromJid		( M2U_String(msgIn->FromJid)		);
	msgOut.setToJid			( M2U_String(msgIn->ToJid)		);

	msgOut.setToGroupId		( M2U_String(msgIn->ToGroupId)	);
	msgOut.setFromUserId	( msgIn->FromUserId					);

	msgOut.setFileLocal		( M2U_String(msgIn->FileLocal)		);
	msgOut.setThumbnailLocal( M2U_String(msgIn->ThumbnailLocal)	);
	msgOut.setDuration		( msgIn->Duration					);

	msgOut.setModified		( M2U_String(msgIn->Modified) );
	msgOut.setIAccount		( M2U_String(msgIn->IAccount) );
	msgOut.setCName			( M2U_String(msgIn->CName)	 );

	//RichData  //TODO?
	return msgOut;
}

MessageList Conversion::M2U_MessageList( ManagedDataTypes::MessageList^ messagesIn )
{
	MessageList msgListOut;

	for each ( ManagedDataTypes::Message^ msgIn in messagesIn )
	{
		Message msg = M2U_Message(msgIn);
		msgListOut.push_back(msg);
	};

	return msgListOut;
}

ManagedDataTypes::RecentCall^ Conversion::U2M_RecentCall(const RecentCall& rcIn )
{
	ManagedDataTypes::RecentCall^ rcOut = gcnew ManagedDataTypes::RecentCall;

	//A couple recasts.
	ManagedDataTypes::RecentCall::CallStatus status	  = ( ManagedDataTypes::RecentCall::CallStatus ) rcIn.getStatus();
	ManagedDataTypes::RecentCall::SeenFlag   seenFlag = ( ManagedDataTypes::RecentCall::SeenFlag   ) rcIn.getSeen();

	rcOut->RecordId			= rcIn.getRecordId();
	rcOut->Status			= status;
	rcOut->Incoming			= rcIn.isIncoming();
	rcOut->StartTime		= gcnew ManagedDataTypes::VxTimestamp( rcIn.getStartTimestamp(), false);
	rcOut->Duration			= rcIn.getDuration();
	rcOut->IsLocal          = rcIn.getIsLocal();
	rcOut->Seen				= seenFlag;

	rcOut->Uid				= U2M_String( rcIn.getUid()			 );
	rcOut->Number			= U2M_String( rcIn.getNumber()		 );
	rcOut->OrigNumber		= U2M_String( rcIn.getOrigNumber()	 );
	rcOut->DisplayNumber	= U2M_String( rcIn.getDisplayNumber() );
	rcOut->Name				= U2M_String( rcIn.getName()		 );
	rcOut->ContactKey		= U2M_String( rcIn.getContactKey()	 );
	rcOut->CmGroupName		= U2M_String( rcIn.getCmGroupName()	 );
	rcOut->CmGroup			= rcIn.getCmGroup();

	return rcOut;
}

ManagedDataTypes::RecentCallList^ Conversion::U2M_RecentCallList( const RecentCallList& rcListIn )
{
	ManagedDataTypes::RecentCallList^ rcListOut = gcnew ManagedDataTypes::RecentCallList;

	for ( RecentCall rcIn : rcListIn )
	{
		ManagedDataTypes::RecentCall^ rc = U2M_RecentCall( rcIn );
		rcListOut->Add( rc );
	}

	return rcListOut;
}


RecentCall Conversion::M2U_RecentCall( ManagedDataTypes::RecentCall^ rcIn )
{
	RecentCall rcOut;

	//A couple recasts.
	RecentCall::Status   status = ( RecentCall::Status ) rcIn->Status;
	RecentCall::Seen     seen   = ( RecentCall::Seen   ) rcIn->Seen;

	rcOut.setRecordId		( rcIn->RecordId		);
	rcOut.setStatus			( status				);
	rcOut.setIsIncoming		( rcIn->Incoming		);
	rcOut.setStartTimestamp	( rcIn->StartTime->AsLong() );
	rcOut.setDuration		( rcIn->Duration		);
	rcOut.setIsLocal        ( rcIn->IsLocal         );
	rcOut.setSeen			( seen   				);

	rcOut.setUid			( M2U_String( rcIn->Uid			  ) );
	rcOut.setNumber			( M2U_String( rcIn->Number		  ) );
	rcOut.setOrigNumber		( M2U_String( rcIn->OrigNumber	  ) );
	rcOut.setDisplayNumber	( M2U_String( rcIn->DisplayNumber ) );
	rcOut.setName			( M2U_String( rcIn->Name		  ) );
	rcOut.setContactKey		( M2U_String( rcIn->ContactKey	  ) );
	rcOut.setCmGroup		( rcIn->CmGroup );

	return rcOut;
}

RecentCallList Conversion::M2U_RecentCallList( ManagedDataTypes::RecentCallList^ rcListIn )
{
	RecentCallList rcListOut;

	for each ( ManagedDataTypes::RecentCall^ rcIn in rcListIn )
	{
		RecentCall rc = M2U_RecentCall( rcIn );
		rcListOut.push_back( rc );
	}

	return rcListOut;
}

ManagedDataTypes::UmwMessage^ Conversion::U2M_UmwMessage(const UmwMessage& msgIn)
{
	ManagedDataTypes::UmwMessage^ msgOut = gcnew ManagedDataTypes::UmwMessage;

	//A couple recasts.
	ManagedDataTypes::PhoneNumber::PnType   numberType	= ( ManagedDataTypes::PhoneNumber::PnType   ) msgIn.getIsVoxox();
	ManagedDataTypes::Message::MsgDirection msgDir		= ( ManagedDataTypes::Message::MsgDirection ) msgIn.getDirection();
	ManagedDataTypes::Message::MsgStatus    msgStatus	= ( ManagedDataTypes::Message::MsgStatus    ) msgIn.getStatus();
	ManagedDataTypes::Message::MsgType		msgType		= ( ManagedDataTypes::Message::MsgType      ) msgIn.getType();

	msgOut->ContactKey		= U2M_String( msgIn.getContactKey() );
	msgOut->CmGroup			= msgIn.getCmGroup();
	msgOut->NumberType		= numberType;
	msgOut->Number			= U2M_String( msgIn.getPhoneNumber() );
	msgOut->Name			= U2M_String( msgIn.getName()		 );
	msgOut->CmGroupName		= U2M_String( msgIn.getCmGroupName() );

	//From GM table
	msgOut->GmNickName		= U2M_String( msgIn.getGmNickName() );
	msgOut->GmColor			= U2M_String( msgIn.getGmColor() );
	msgOut->GmGroupName		= U2M_String( msgIn.getGmGroupName() );

	//Main message info
	msgOut->RecordId		= msgIn.getRecordId();
	msgOut->Type			= msgType;
	msgOut->Direction		= msgDir;
	msgOut->Status			= msgStatus;
	msgOut->Body			= U2M_String( msgIn.getBody() );
	msgOut->Translated		= U2M_String( msgIn.getTranslated() );
	msgOut->MsgId			= U2M_String( msgIn.getMsgId() );

	//Time stamps
	msgOut->LocalTimestamp		= msgIn.getLocalTimestamp();
	msgOut->ServerTimestamp		= gcnew ManagedDataTypes::VxTimestamp( msgIn.getServerTimestamp(), false );
	msgOut->ReadTimestamp		= msgIn.getReadTimestamp();
	msgOut->DeliveredTimestamp	= msgIn.getDeliveredTimestamp();

	//Rich data
	msgOut->FileLocal		= U2M_String( msgIn.getFileLocal() );
	msgOut->ThumbnailLocal	= U2M_String( msgIn.getThumbnailLocal() );
	msgOut->Duration		= msgIn.getDuration();

	//SMS
	msgOut->FromDid			= U2M_String( msgIn.getFromDid() );
	msgOut->ToDid			= U2M_String( msgIn.getToDid() );

	//Chat
	msgOut->FromJid			= U2M_String( msgIn.getFromJid() );
	msgOut->ToJid			= U2M_String( msgIn.getToJid() );

	//Group Messaging
	msgOut->ToGroupId		= U2M_String( msgIn.getToGroupId() );
	msgOut->FromUserId		= msgIn.getFromUserId();

	return msgOut;
}

ManagedDataTypes::UmwMessageList^ Conversion::U2M_UmwMessageList( const UmwMessageList& msgListIn )
{
	ManagedDataTypes::UmwMessageList^ msgListOut = gcnew ManagedDataTypes::UmwMessageList;

	for ( UmwMessage msgIn : msgListIn )
	{
		ManagedDataTypes::UmwMessage^ msg = U2M_UmwMessage( msgIn );
		msgListOut->Add( msg );
	}

	return msgListOut;
}


ManagedDataTypes::UnreadCounts^ Conversion::U2M_UnreadCounts( const UnreadCounts& dataIn )
{
	ManagedDataTypes::UnreadCounts^ dataOut = gcnew ManagedDataTypes::UnreadCounts;

	dataOut->ChatCount			= dataIn.getChatCount();
	dataOut->SmsCount			= dataIn.getSmsCount();
	dataOut->GmCount			= dataIn.getGmCount();

	dataOut->FaxCount			= dataIn.getFaxCount();
	dataOut->RecordedCallCount	= dataIn.getRecordedCallCount();
	dataOut->VoicemailCount		= dataIn.getVoicemailCount();

	dataOut->MissedCallCount	= dataIn.getMissedCallCount();

	return dataOut;
}

RichData Conversion::M2U_RichData( ManagedDataTypes::RichData^ richDataIn)
{
	RichData result;

	RichData::Type type = (RichData::Type) richDataIn->Type;

	result.setType		  ( type );

	result.setThumbnailUrl( M2U_String( richDataIn->ThumbnailUrl ) );
	result.setMediaUrl    ( M2U_String( richDataIn->MediaUrl     ) );
	result.setBody		  ( M2U_String( richDataIn->Body         ) );

	result.setLatitude( richDataIn->Latitude ) ;
	result.setLongitude( richDataIn->Longitude );
	result.setAddress  ( M2U_String( richDataIn->Address ) );

    //TODO: NOTE that in original code, these were FILE types so we could parse the file path name.
	//	That functionality can be added to the Managed class.
	result.setMediaFile	   ( M2U_String( richDataIn->MediaFile     ) );
	result.setThumbnailFile( M2U_String( richDataIn->ThumbnailFile ) );

	return result;
}


ManagedDataTypes::RichData^ Conversion::U2M_RichData( const RichData& dataIn)
{
	ManagedDataTypes::RichData^ result = gcnew ManagedDataTypes::RichData;

	ManagedDataTypes::RichData::RichDataType type = (ManagedDataTypes::RichData::RichDataType) dataIn.getType();

	result->Type		 = type;

	result->ThumbnailUrl = U2M_String( dataIn.getThumbnailUrl() );
	result->MediaUrl     = U2M_String( dataIn.getMediaUrl()     );
	result->Body		 = U2M_String( dataIn.getBody()         );

	result->Latitude	= dataIn.getLatitude();
	result->Longitude	= dataIn.getLongitude();
	result->Address		= U2M_String( dataIn.getAddress() );

    //TODO: NOTE that in original code, these were FILE types so we could parse the file path name.
	//	That functionality can be added to the Managed class.
	result->MediaFile	  = U2M_String( dataIn.getMediaFile()     );
	result->ThumbnailFile = U2M_String( dataIn.getThumbnailFile() );

	return result;
}

ContactImport Conversion::M2U_ContactImport( ManagedDataTypes::ContactImport^ contactIn )
{
	ContactImport contact;

	contact.setName       ( M2U_String( contactIn->Name			) );
	contact.setPhoneNumber( M2U_String( contactIn->PhoneNumber	) );	//Used as base for Number
	contact.setPhoneLabel ( M2U_String( contactIn->PhoneLabel	) );

	contact.setIsFavorite ( contactIn->IsFavorite );
	contact.setJid        ( M2U_String( contactIn->Jid			) );	//Used for IsVoxox.

	//New fields with Contact Syncing
	contact.setContactKey ( M2U_String( contactIn->ContactKey	) );
	contact.setSource	  ( M2U_String( contactIn->Source		) );
	contact.setFirstName  ( M2U_String( contactIn->FirstName	) );
	contact.setLastName   ( M2U_String( contactIn->LastName		) );
	contact.setCompany    ( M2U_String( contactIn->Company		) );
	contact.setUserId     ( contactIn->UserId );
	contact.setUserName   ( M2U_String( contactIn->UserName		) );
	contact.setIsBlocked  ( contactIn->IsBlocked				  );
	contact.setIsExtension( contactIn->IsExtension				  );

	return contact;
}

ContactImportList Conversion::M2U_ContactImportList( ManagedDataTypes::ContactImportList^ contactsIn )
{
	ContactImportList listOut;

	for each ( ManagedDataTypes::ContactImport^ contactIn in contactsIn )
	{
		ContactImport contact = M2U_ContactImport( contactIn );
		listOut.push_back(contact);
	};

	return listOut;
}

VoxoxContact Conversion::M2U_VoxoxContact( ManagedDataTypes::VoxoxContact^ vcIn )
{
	VoxoxContact vc;

	vc.setContactKey		( M2U_String( vcIn->ContactKey			) );
	vc.setRegistrationNumber( M2U_String( vcIn->RegistrationNumber	) );
	vc.setUserName			( M2U_String( vcIn->UserName			) );
	vc.setDid				( M2U_String( vcIn->Did					) );
	vc.setSource			( M2U_String( vcIn->Source				) );
	vc.setPhoneLabel		( M2U_String( vcIn->PhoneLabel			) );
	vc.setUserId			( vcIn->UserId		);
	vc.setIsBlocked			( vcIn->IsBlocked	);
	vc.setIsFavorite		( vcIn->IsFavorite	);

	return vc;
}

VoxoxContactList Conversion::M2U_VoxoxContactList( ManagedDataTypes::VoxoxContactList^ vcListIn )
{
	VoxoxContactList vcListOut;

	for each ( ManagedDataTypes::VoxoxContact^ vcIn in vcListIn )
	{
		VoxoxContact vc = M2U_VoxoxContact( vcIn);
		vcListOut.push_back(vc);
	};

	return vcListOut;
}

VoxoxPhoneNumber Conversion::M2U_VoxoxPhoneNumber( ManagedDataTypes::VoxoxPhoneNumber^ pnIn )
{
	VoxoxPhoneNumber pn;

	pn.setDid( M2U_String( pnIn->Did ) );
	pn.setJid( M2U_String( pnIn->Jid ) );

	return pn;
}

VoxoxPhoneNumberList Conversion::M2U_VoxoxPhoneNumberList( ManagedDataTypes::VoxoxPhoneNumberList^ listIn )
{
	VoxoxPhoneNumberList listOut;

	for each ( ManagedDataTypes::VoxoxPhoneNumber^ dataIn in listIn )
	{
		VoxoxPhoneNumber data = M2U_VoxoxPhoneNumber( dataIn );
		listOut.push_back(data);
	};

	return listOut;
}

XmppContact Conversion::M2U_XmppContact( ManagedDataTypes::XmppContact^ xcIn )
{
	XmppContact xc;

	xc.setJid     ( M2U_String( xcIn->Jid      ) );
	xc.setMsgState( M2U_String( xcIn->MsgState ) );
	xc.setName    ( M2U_String( xcIn->Name     ) );
	xc.setStatus  ( M2U_String( xcIn->Status   ) );

	return xc;
}

ManagedDataTypes::Contact^ Conversion::U2M_Contact( const Contact& contactIn )
{
	ManagedDataTypes::Contact^ dataOut = gcnew ManagedDataTypes::Contact;

	ManagedDataTypes::Contact::ContactType   type   = (ManagedDataTypes::Contact::ContactType)contactIn.getType();
	ManagedDataTypes::Contact::ContactSource source = ManagedDataTypes::Contact::convertFromDb( U2M_String( contactIn.getSource() ) );

	dataOut->CmGroup			= contactIn.getCmGroup();
	dataOut->CmGroupVoxoxCount	= contactIn.getCmGroupVoxoxCount();
	dataOut->RecordId			= contactIn.getRecordId();
	dataOut->Type				= type;
	dataOut->Source				= source;

	dataOut->VoxoxCount			= contactIn.getVoxoxCount();
	dataOut->BlockedCount		= contactIn.getBlockedCount();
	dataOut->FavoriteCount		= contactIn.getFavoriteCount();
	dataOut->PhoneNumberCount	= contactIn.getPhoneNumberCount();
	dataOut->ExtensionCount		= contactIn.getExtensionCount();

	dataOut->DisplayName		= U2M_String( contactIn.getDisplayName()	);
	dataOut->DisplayNumber		= U2M_String( contactIn.getDisplayNumber()	);
	dataOut->GroupId			= U2M_String( contactIn.getGroupId()		);
	dataOut->Name				= U2M_String( contactIn.getName()			);
	dataOut->PhoneLabel			= U2M_String( contactIn.getPhoneLabel()		);
	dataOut->PhoneNumber		= U2M_String( contactIn.getPhoneNumber()	);
	dataOut->Extension			= U2M_String( contactIn.getExtension()		);
	dataOut->PreferredNumber	= U2M_String( contactIn.getPreferredNumber());
	dataOut->CmGroupName		= U2M_String( contactIn.getCmGroupName()	);

	//New Contact-Sync
	dataOut->ContactKey			= U2M_String( contactIn.getContactKey()	);
	dataOut->FirstName			= U2M_String( contactIn.getFirstName()	);
	dataOut->LastName			= U2M_String( contactIn.getLastName()	);
	dataOut->Company			= U2M_String( contactIn.getCompany()	);
	
	return dataOut;
}

ManagedDataTypes::ContactList^ Conversion::U2M_ContactList( const ContactList& listIn )
{
	ManagedDataTypes::ContactList^ listOut = gcnew ManagedDataTypes::ContactList;

	for ( Contact dataIn : listIn )
	{
		ManagedDataTypes::Contact^ data = U2M_Contact( dataIn );
		listOut->Add( data );
	}

	return listOut;
}

ManagedDataTypes::PhoneNumber^ Conversion::U2M_PhoneNumber( const PhoneNumber& pnIn )
{
	ManagedDataTypes::PhoneNumber^ dataOut = gcnew ManagedDataTypes::PhoneNumber;

	ManagedDataTypes::Contact::ContactSource contactSource = ManagedDataTypes::Contact::convertFromDb( U2M_String( pnIn.getSource()	) );
	ManagedDataTypes::Contact::ContactType   contactType   = (ManagedDataTypes::Contact::ContactType)pnIn.getContactType();
	ManagedDataTypes::PhoneNumber::PnType    numberType    = (ManagedDataTypes::PhoneNumber::PnType )pnIn.getNumberType();

	dataOut->ContactSource	= contactSource;
	dataOut->ContactType	= contactType;
	dataOut->NumberType		= numberType;

	dataOut->CmGroup		= pnIn.getCmGroup();
	dataOut->IsFavorite		= pnIn.isFavorite();
	dataOut->RecordId		= pnIn.getRecordId();
	dataOut->XmppStatus		= pnIn.getXmppStatus();

	dataOut->DisplayName	= U2M_String( pnIn.getDisplayName()	);
	dataOut->DisplayNumber	= U2M_String( pnIn.getDisplayNumber());
	dataOut->Label			= U2M_String( pnIn.getLabel()		);
	dataOut->Name			= U2M_String( pnIn.getName()		);
	dataOut->CmGroupName	= U2M_String( pnIn.getCmGroupName()	);
	dataOut->Number			= U2M_String( pnIn.getNumber()		);
	dataOut->XmppAlias		= U2M_String( pnIn.getXmppAlias()	);
	dataOut->XmppGroup		= U2M_String( pnIn.getXmppGroup()	);
	dataOut->XmppJid		= U2M_String( pnIn.getXmppJid()		);
	dataOut->XmppPresence	= U2M_String( pnIn.getXmppPresence() );

	//New Contact-Sync
	dataOut->ContactKey		= U2M_String( pnIn.getContactKey()	);
	dataOut->FirstName		= U2M_String( pnIn.getFirstName()	);
	dataOut->LastName		= U2M_String( pnIn.getLastName()	);
	dataOut->Company		= U2M_String( pnIn.getCompany()		);
	dataOut->UserId			= pnIn.getUserId();
	dataOut->UserName		= U2M_String( pnIn.getUserName()	);
	dataOut->IsBlocked		= pnIn.isBlocked();
	dataOut->IsExtension	= pnIn.isExtension();
	
	return dataOut;
}

ManagedDataTypes::PhoneNumberList^ Conversion::U2M_PhoneNumberList( const PhoneNumberList& listIn )
{
	ManagedDataTypes::PhoneNumberList^ listOut = gcnew ManagedDataTypes::PhoneNumberList;

	for ( PhoneNumber dataIn : listIn )
	{
		ManagedDataTypes::PhoneNumber^ data = U2M_PhoneNumber( dataIn );
		listOut->Add( data );
	}

	return listOut;
}

ManagedDataTypes::DeleteMessageData^ Conversion::U2M_DeleteMessageData( const DeleteMessageData& dataIn )
{
	ManagedDataTypes::DeleteMessageData^ dataOut = gcnew ManagedDataTypes::DeleteMessageData;

	dataOut->MsgId      = U2M_String( dataIn.getMsgId() );
	dataOut->SvrMsgType = U2M_String( dataIn.getMsgId() );
	dataOut->MsgType	= ( ManagedDataTypes::Message::MsgType)   dataIn.getMsgType();
	dataOut->MsgStatus	= ( ManagedDataTypes::Message::MsgStatus) dataIn.getMsgStatus();
	dataOut->Success	= dataIn.getSuccess();

	return dataOut;
}

ManagedDataTypes::DeleteMessageDataList^ Conversion::U2M_DeleteMessageDataList( const DeleteMessageDataList& listIn )
{
	ManagedDataTypes::DeleteMessageDataList^ listOut = gcnew ManagedDataTypes::DeleteMessageDataList;

	for ( DeleteMessageData dataIn : listIn )
	{
		ManagedDataTypes::DeleteMessageData^ data = U2M_DeleteMessageData( dataIn );
		listOut->Add( data );
	}

	return listOut;
}

ManagedDataTypes::ContactMessageSummary^ Conversion::U2M_ContactMessageSummary( const ContactMessageSummary& summaryIn  )
{
	ManagedDataTypes::ContactMessageSummary^ dataOut = gcnew ManagedDataTypes::ContactMessageSummary;

	ManagedDataTypes::GroupMessageGroup::GmgStatus groupStatus = (ManagedDataTypes::GroupMessageGroup::GmgStatus) summaryIn.getGroupStatus();
	ManagedDataTypes::Message::MsgDirection		   msgDir      = (ManagedDataTypes::Message::MsgDirection)        summaryIn.getMsgDir();
	ManagedDataTypes::Message::MsgType			   msgType     = (ManagedDataTypes::Message::MsgType)             summaryIn.getMsgType();

	dataOut->GroupStatus		= groupStatus;
	dataOut->MsgDir				= msgDir;
	dataOut->MsgType			= msgType;

	dataOut->CmGroup			= summaryIn.getCmGroup();
	dataOut->MsgServerTimestamp = gcnew ManagedDataTypes::VxTimestamp( summaryIn.getMsgServerTimestamp(), false );

	dataOut->ContactKey			= U2M_String( summaryIn.getContactKey()		);
	dataOut->DisplayName		= U2M_String( summaryIn.getDisplayName()	);
	dataOut->DisplayNumber		= U2M_String( summaryIn.getDisplayNumber()	);
	dataOut->GroupName			= U2M_String( summaryIn.getGroupName()		);
	dataOut->CmGroupName		= U2M_String( summaryIn.getCmGroupName()	);
	dataOut->MsgBody			= U2M_String( summaryIn.getMsgBody()		);
	dataOut->MsgFromDid			= U2M_String( summaryIn.getMsgFromDid()		);
	dataOut->MsgFromJid			= U2M_String( summaryIn.getMsgFromJid()		);
	dataOut->MsgToDid			= U2M_String( summaryIn.getMsgToDid()		);
	dataOut->MsgToJid			= U2M_String( summaryIn.getMsgToJid()		);
	dataOut->MsgTranslated		= U2M_String( summaryIn.getMsgTranslated()	);
	dataOut->PhoneNumber		= U2M_String( summaryIn.getPhoneNumber()	);
	dataOut->ToGroupId			= U2M_String( summaryIn.getToGroupId()		);
	dataOut->XmppJid			= U2M_String( summaryIn.getXmppJid()		);

	return dataOut;
}

ManagedDataTypes::ContactMessageSummaryList^ Conversion::U2M_ContactMessageSummaryList( const ContactMessageSummaryList& listIn )
{
	ManagedDataTypes::ContactMessageSummaryList^ listOut = gcnew ManagedDataTypes::ContactMessageSummaryList;

	for ( ContactMessageSummary dataIn : listIn )
	{
		ManagedDataTypes::ContactMessageSummary^ data = U2M_ContactMessageSummary( dataIn );
		listOut->Add( data );
	}

	return listOut;
}

ManagedDataTypes::MessageTranslationProperties^ Conversion::U2M_MessageTranslationProperties( const MessageTranslationProperties& mtpIn )
{
	ManagedDataTypes::MessageTranslationProperties^ dataOut = gcnew ManagedDataTypes::MessageTranslationProperties;

	ManagedDataTypes::Message::MsgDirection	msgDir = (ManagedDataTypes::Message::MsgDirection) mtpIn.getDirection();

	dataOut->Direction		 = msgDir;

	dataOut->ContactId		 = mtpIn.getContactId();
	dataOut->IsEnabled		 = mtpIn.isEnabled();
	dataOut->RecordId		 = mtpIn.getRecordId();

	dataOut->ContactLanguage = U2M_String( mtpIn.getContactLanguage() );
	dataOut->UserLanguage	 = U2M_String( mtpIn.getUserLanguage() );

	return dataOut;
}

MessageTranslationProperties Conversion::M2U_MessageTranslationProperties( ManagedDataTypes::MessageTranslationProperties^ mtpIn )
{
	MessageTranslationProperties mtp;

	Message::Direction	msgDir = (Message::Direction) mtpIn->Direction;

	mtp.setDirection( msgDir );

	mtp.setContactId( mtpIn->ContactId );
	mtp.setEnabled  ( mtpIn->IsEnabled );
	mtp.setRecordId ( mtpIn->RecordId  );

	mtp.setContactLanguage( M2U_String( mtpIn->ContactLanguage ) );
	mtp.setUserLanguage   ( M2U_String( mtpIn->UserLanguage    ) );
//	mtp.setJid		  ( M2U_String( mtpIn->Jid         ) );	//Not used currently
//	mtp.setPhoneNumber( M2U_String( mtpIn->PhoneNumber ) );	//Not used currently

	return mtp;
}

ManagedDataTypes::DbChangeData^ Conversion::U2M_DbChangeData( const DbChangeData& dataIn )
{
	ManagedDataTypes::DbChangeData^ dataOut = gcnew ManagedDataTypes::DbChangeData;

	ManagedDataTypes::DbChangeData::TableId	tableId   = (ManagedDataTypes::DbChangeData::TableId) dataIn.getTable();
	ManagedDataTypes::DbChangeData::Action	gmAction  = (ManagedDataTypes::DbChangeData::Action ) dataIn.getGmAction();
	ManagedDataTypes::Message::MsgStatus	msgStatus = (ManagedDataTypes::Message::MsgStatus    ) dataIn.getMsgStatus();

	dataOut->Table     = tableId;
	dataOut->MsgStatus = msgStatus;
	dataOut->MsgCount  = dataIn.getMsgCount();
	dataOut->VoxoxId   = dataIn.getVoxoxId();
	dataOut->GroupId   = U2M_String( dataIn.getGroupId() );
	dataOut->GmAction  = gmAction;

	return dataOut;
}


ManagedDataTypes::UserSettings^ Conversion::U2M_UserSettings( const UserSettings& srcIn )
{
	ManagedDataTypes::UserSettings^ dataOut = gcnew ManagedDataTypes::UserSettings;

	dataOut->UserId						= U2M_String( srcIn.getUserId()						);

	dataOut->AudioInputDevice			= U2M_String( srcIn.getAudioInputDevice()			);
	dataOut->AudioOutputDevice			= U2M_String( srcIn.getAudioOutputDevice()			);
	dataOut->AudioInputVolume			= srcIn.getAudioInputVolume();
	dataOut->AudioOutputVolume			= srcIn.getAudioOutputVolume();

	dataOut->ShowFaxInMsgThread			= srcIn.getShowFaxInMsgThread();
	dataOut->ShowVmInMsgThread			= srcIn.getShowVmInMsgThread();
	dataOut->ShowRcInMsgThread			= srcIn.getShowRcInMsgThread();

	dataOut->EnableSoundIncomingMsg		= srcIn.getEnableSoundIncomingMsg();
	dataOut->EnableSoundIncomingCall	= srcIn.getEnableSoundIncomingCall();
//	dataOut->EnableSoundOutgoingMsg		= srcIn.getEnableSoundOutgoingMsg();

	dataOut->MyChatBubbleColor			= U2M_String( srcIn.getMyChatBubbleColor()			);
	dataOut->ConversantChatBubbleColor	= U2M_String( srcIn.getConversantChatBubbleColor()	);

	dataOut->CountryCode				= U2M_String( srcIn.getCountryCode()				);
	dataOut->CountryAbbrev				= U2M_String( srcIn.getCountryAbbrev()				);

//	dataOut->OutgoingCallSoundFile		= U2M_String( srcIn.getOutgoingCallSoundFile()		);
	dataOut->IncomingCallSoundFile		= U2M_String( srcIn.getIncomingCallSoundFile()		);
	dataOut->CallClosedSoundFile		= U2M_String( srcIn.getCallClosedSoundFile()		);

	dataOut->WindowLeft					= srcIn.getWindowLeft();
	dataOut->WindowTop					= srcIn.getWindowTop();
	dataOut->WindowHeight				= srcIn.getWindowHeight();
	dataOut->WindowWidth				= srcIn.getWindowWidth();
	dataOut->WindowState				= srcIn.getWindowState();

	dataOut->LastMsgTimestamp			= srcIn.getLastMsgTimestamp();
	dataOut->LastCallTimestamp			= srcIn.getLastCallTimestamp();
	dataOut->LastContactSourceKey		= U2M_String( srcIn.getLastContactSourceKey()	);

	dataOut->SipUuid					= U2M_String( srcIn.getSipUuid()					);

	return dataOut;
}


UserSettings Conversion::M2U_UserSettings( ManagedDataTypes::UserSettings^ srcIn )
{
	UserSettings dataOut;

	dataOut.setUserId					 ( M2U_String( srcIn->UserId					)	);

	dataOut.setAudioInputDevice			 ( M2U_String( srcIn->AudioInputDevice			)	);
	dataOut.setAudioOutputDevice		 ( M2U_String( srcIn->AudioOutputDevice			)	);
	dataOut.setAudioInputVolume			 ( srcIn->AudioInputVolume			);
	dataOut.setAudioOutputVolume		 ( srcIn->AudioOutputVolume			);

	dataOut.setShowFaxInMsgThread		 ( srcIn->ShowFaxInMsgThread		);
	dataOut.setShowVmInMsgThread		 ( srcIn->ShowVmInMsgThread			);
	dataOut.setShowRcInMsgThread		 ( srcIn->ShowRcInMsgThread			);

	dataOut.setEnableSoundIncomingMsg	 ( srcIn->EnableSoundIncomingMsg	);
	dataOut.setEnableSoundIncomingCall	 ( srcIn->EnableSoundIncomingCall	);
//	dataOut.setEnableSoundOutgoingMsg	 ( srcIn->EnableSoundOutgoingMsg	);

	dataOut.setMyChatBubbleColor		 ( M2U_String( srcIn->MyChatBubbleColor			)	);
	dataOut.setConversantChatBubbleColor( M2U_String( srcIn->ConversantChatBubbleColor	)	);

	dataOut.setCountryCode				 ( M2U_String( srcIn->CountryCode				)	);
	dataOut.setCountryAbbrev			 ( M2U_String( srcIn->CountryAbbrev				)	);

//	dataOut.setOutgoingCallSoundFile	 ( M2U_String( srcIn->OutgoingCallSoundFile		)	);
	dataOut.setIncomingCallSoundFile	 ( M2U_String( srcIn->IncomingCallSoundFile		)	);
	dataOut.setCallClosedSoundFile		 ( M2U_String( srcIn->CallClosedSoundFile		)	);

	dataOut.setWindowLeft				 ( srcIn->WindowLeft		);
	dataOut.setWindowTop				 ( srcIn->WindowTop			);
	dataOut.setWindowHeight				 ( srcIn->WindowHeight		);
	dataOut.setWindowWidth				 ( srcIn->WindowWidth		);
	dataOut.setWindowState				 ( srcIn->WindowState		);

	dataOut.setLastMsgTimestamp			 ( srcIn->LastMsgTimestamp  );
	dataOut.setLastCallTimestamp		 ( srcIn->LastCallTimestamp );
	dataOut.setLastContactSourceKey		 ( M2U_String( srcIn->LastContactSourceKey	) );

	dataOut.setSipUuid					 ( M2U_String( srcIn->SipUuid					)	);

	return dataOut;
}



//-----------------------------------------------------
// These methods use data that is NOT part of user DB.
//-----------------------------------------------------
ManagedDataTypes::AppSettings^ Conversion::U2M_AppSettings( const AppSettings& srcIn )
{
	ManagedDataTypes::AppSettings^ dataOut = gcnew ManagedDataTypes::AppSettings;

	dataOut->RecordId					= srcIn.getRecordId();

	dataOut->AutoLogin					= srcIn.getAutoLogin();
	dataOut->RememberPassword			= srcIn.getRememberPassword();
	dataOut->LastLoginUsername			= U2M_String( srcIn.getLastLoginUsername()		);
	dataOut->LastLoginPassword			= U2M_String( srcIn.getLastLoginPassword()		);
	dataOut->LastLoginCountryCode		= U2M_String( srcIn.getLastLoginCountryCode()	);
	dataOut->LastLoginPhoneNumber		= U2M_String( srcIn.getLastLoginPhoneNumber()	);

	//Debug items
	dataOut->DebugMenu					= U2M_String( srcIn.getDebugMenu()		);
	dataOut->IgnoreLoggers				= U2M_String( srcIn.getIgnoreLoggers()	);
	dataOut->MaxSipLogging				= srcIn.getMaxSipLogging();
	dataOut->GetInitOptionsHandling		= srcIn.getGetInitOptionsHandling();

	return dataOut;
}

 AppSettings Conversion::M2U_AppSettings( ManagedDataTypes::AppSettings^ srcIn )
{
	AppSettings dataOut;

	dataOut.setRecordId				( srcIn->RecordId		  );

	dataOut.setAutoLogin			( srcIn->AutoLogin		  );
	dataOut.setRememberPassword		( srcIn->RememberPassword );
	dataOut.setLastLoginUsername	( M2U_String( srcIn->LastLoginUsername		) );
	dataOut.setLastLoginPassword	( M2U_String( srcIn->LastLoginPassword		) );
	dataOut.setLastLoginCountryCode	( M2U_String( srcIn->LastLoginCountryCode	) );
	dataOut.setLastLoginPhoneNumber	( M2U_String( srcIn->LastLoginPhoneNumber	) );

	//Debug items
	dataOut.setDebugMenu			 ( M2U_String( srcIn->DebugMenu		) );
	dataOut.setIgnoreLoggers		 ( M2U_String( srcIn->IgnoreLoggers	) );
	dataOut.setMaxSipLogging		 ( srcIn->MaxSipLogging				  );
	dataOut.setGetInitOptionsHandling( srcIn->GetInitOptionsHandling	  );

	return dataOut;
}

//-----------------------------------------------------------------------------
//End conversion methods
//-----------------------------------------------------------------------------
