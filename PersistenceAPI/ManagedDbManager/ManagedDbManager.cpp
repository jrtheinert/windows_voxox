/*
* Voxox
* Copyright (C) 2004-2014 Voxox
*/

#include "stdafx.h"
#include "ManagedDbManager.h"
#include "Conversion.h"

#include "..\VoxoxDbManager\DbManager.h"
#include "..\VoxoxDbManager\DataTypes.h"

using namespace System;
using namespace System::Runtime::InteropServices;	//For Marshal of callback ptr.

static GCHandle gch;	//To lock the DbChange callback in place.  We may need to revisit this.

namespace ManagedDbManager
{

//-----------------------------------------------------------------------------
//DB Change event related methods
//-----------------------------------------------------------------------------
void ManagedDbManager::setDbChangeCallback()
{
	//Get Callback to DbManager
	mDbChangeDelegate = gcnew DbChangeDelegate(this, &ManagedDbManager::dbChangeCallback );
	gch = GCHandle::Alloc( mDbChangeDelegate );
	IntPtr ip = Marshal::GetFunctionPointerForDelegate( mDbChangeDelegate );
	DbChangeCallbackFunc cb = static_cast<DbChangeCallbackFunc>(ip.ToPointer());
	setDbChangeCallback( cb );
}

void ManagedDbManager::setDbChangeCallback( DbChangeCallbackFunc callback )
{
	mDbm->setDbChangeCallback( callback );
}

void ManagedDbManager::dbChangeCallback( const DbChangeData& dataIn )
{
	ManagedDataTypes::DbChangeData^ data = Conversion::U2M_DbChangeData( dataIn );

	DbChangedEventArgs^ args = gcnew DbChangedEventArgs( data );
	onDbChanged( args );
}

void ManagedDbManager::onDbChanged( DbChangedEventArgs^ e )
{
	mDbChangedEventHandler( this, e );
}

void ManagedDbManager::addDbChangeListener( DbChangedEventHandler^ dbChangeDelegate )
{
	mDbChangedEventHandler += dbChangeDelegate;
}

void ManagedDbManager::removeDbChangeListener( DbChangedEventHandler^ dbChangeDelegate )
{
	mDbChangedEventHandler -= dbChangeDelegate;
}

//static
String^ ManagedDbManager::testStringConversion( String^ textIn )
{
	std::string temp   = Conversion::M2U_String(textIn);
	String^     result = Conversion::U2M_String(temp);

	return result;
}

//-----------------------------------------------------------------------------
//DB Admin methods
//-----------------------------------------------------------------------------

ManagedDbManager::ManagedDbManager(String^ dbNameIn, DbType dbTypeIn )
{
	VoxoxDbManager::DbType dbType = (VoxoxDbManager::DbType)dbTypeIn;

	std::string dbName( Conversion::M2U_String( dbNameIn ) );
	mDbm = new VoxoxDbManager( dbName, dbType );
	
	setDbChangeCallback();
}

ManagedDbManager::~ManagedDbManager()
{
	delete mDbm;
	mDbm = nullptr;
}

Boolean	ManagedDbManager::checkDatabase()
{
	return mDbm->checkDatabase();
}

Boolean	ManagedDbManager::checkTableExists(String^ tableName)
{
	std::string temp = Conversion::M2U_String(tableName);
	return mDbm->checkTableExists(temp);
}

Boolean	ManagedDbManager::create()
{
	return mDbm->create();
}

String^ ManagedDbManager::getDbName()
{
	std::string temp = mDbm->getDbName();

	return Conversion::U2M_String(temp);
}

//static 
Boolean ManagedDbManager::checkDatabase(String^ dbName)
{
	std::string temp = Conversion::M2U_String(dbName);
	return VoxoxDbManager::checkDatabase(temp);
}

//These methods help support unit testing.
void ManagedDbManager::close()
{
	mDbm->close();
}

void ManagedDbManager::clearMessageTable()
{
	mDbm->clearMessageTable();
}

void ManagedDbManager::clearPhoneNumberTable()
{
	mDbm->clearPhoneNumberTable();
}

String^ ManagedDbManager::richDataToJsonString( ManagedDataTypes::RichData^ richDataIn )
{
	RichData richData = Conversion::M2U_RichData( richDataIn );

	std::string temp = richData.toJsonString();
	return Conversion::U2M_String( temp );
}

//---------------------------------------
// PUBLIC Utility Methods
//---------------------------------------
String^ ManagedDbManager::normalizePhoneNumber( String^ phoneNumberIn )
{
	std::string pn    = Conversion::M2U_String( phoneNumberIn );
	std::string pnOut = mDbm->normalizePhoneNumber( pn );

	return Conversion::U2M_String( pnOut );
}

String^ ManagedDbManager::normalizeJid( String^ jidIn )
{
	std::string jid    = Conversion::M2U_String( jidIn );
	std::string jidOut = mDbm->normalizePhoneNumber( jid );

	return Conversion::U2M_String( jidOut );
}

//---------------------------------------
// PUBLIC Messages Methods
//---------------------------------------
Int64 ManagedDbManager::getLatestMessageServerTimestamp(ManagedDataTypes::Message::MsgType typeIn, ManagedDataTypes::Message::MsgDirection dirIn, ManagedDataTypes::Message::MsgStatus statusIn)
{
	Message::Direction	dir		= (Message::Direction)dirIn;
	Message::Status		status	= (Message::Status   )statusIn;
	Message::Type		type	= (Message::Type     )typeIn;

	Int64 result = mDbm->getLatestMessageServerTimestamp(type, dir, status);
	return result;
}

ManagedDataTypes::Message^ ManagedDbManager::getMessage( Int64 timestamp )
{
	Message msg = mDbm->getMessage(timestamp);
	return Conversion::U2M_Message(msg);
}

ManagedDataTypes::MessageList^ ManagedDbManager::getMessages(ManagedDataTypes::Message::MsgStatus statusIn, ManagedDataTypes::Message::MsgDirection dirIn)
{
	ManagedDataTypes::MessageList^ result = gcnew ManagedDataTypes::MessageList;

	Message::Direction	dir = (Message::Direction)dirIn;
	Message::Status		status = (Message::Status)statusIn;

	MessageList msgList = mDbm->getMessages( status, dir );

	for ( Message msg : msgList )
	{
		result->Add( Conversion::U2M_Message(msg));
	}

	return result;
}

ManagedDataTypes::UmwMessageList^  ManagedDbManager::getVmAndRecordedCallMessages()
{
	ManagedDataTypes::UmwMessageList^ result  = gcnew ManagedDataTypes::UmwMessageList;

	UmwMessageList msgList = mDbm->getVmAndRecordedCallMessages();

	return Conversion::U2M_UmwMessageList( msgList );
}

ManagedDataTypes::UmwMessageList^  ManagedDbManager::getFaxMessages()
{
	ManagedDataTypes::UmwMessageList^ result  = gcnew ManagedDataTypes::UmwMessageList;

	UmwMessageList msgList = mDbm->getFaxMessages();

	return Conversion::U2M_UmwMessageList( msgList );
}

ManagedDataTypes::UmwMessageList^ ManagedDbManager::getMessagesForPhoneNumber( String^ phoneNumberIn )
{
	ManagedDataTypes::UmwMessageList^ result  = gcnew ManagedDataTypes::UmwMessageList;
	std::string						  phoneNumber = Conversion::M2U_String(phoneNumberIn);

	UmwMessageList msgList = mDbm->getMessagesForPhoneNumber(phoneNumber);

	for ( UmwMessage msg : msgList)
	{
		result->Add( Conversion::U2M_UmwMessage(msg) );
	}

	return result;
}

ManagedDataTypes::UmwMessageList^ ManagedDbManager::getMessagesForCmGroup( Int32 cmGroup )
{
	ManagedDataTypes::UmwMessageList^ result  = gcnew ManagedDataTypes::UmwMessageList;

	UmwMessageList msgList = mDbm->getMessagesForCmGroup( cmGroup );

	for ( UmwMessage msg : msgList)
	{
		result->Add( Conversion::U2M_UmwMessage(msg) );
	}

	return result;
}

ManagedDataTypes::UmwMessageList^ ManagedDbManager::getMessagesForGroupId( String^ groupIdIn )
{
	ManagedDataTypes::UmwMessageList^ result  = gcnew ManagedDataTypes::UmwMessageList;
	std::string						  groupId = Conversion::M2U_String(groupIdIn);

	UmwMessageList msgList = mDbm->getMessagesForGroupId(groupId);

	for ( UmwMessage msg : msgList)
	{
		result->Add( Conversion::U2M_UmwMessage(msg) );
	}

	return result;
}

void ManagedDbManager::updateMessageRichData(Int64 timestamp, ManagedDataTypes::RichData^ richDataIn, Boolean broadcast)
{
	RichData richData = Conversion::M2U_RichData(richDataIn);

	mDbm->updateMessageRichData( timestamp, richData, (bool)broadcast );
}

void ManagedDbManager::updateGroupMessageRichData(Int64 timestamp, ManagedDataTypes::RichData^ richDataIn, Boolean broadcast, String^ origBodyIn )
{
	RichData	richData = Conversion::M2U_RichData( richDataIn );
	std::string origBody = Conversion::M2U_String( origBodyIn );

	mDbm->updateGroupMessageRichData( timestamp, richData, (bool)broadcast, origBody );
}

void ManagedDbManager::updateMessage(Int64 timestamp, ManagedDataTypes::Message::MsgStatus statusIn)
{
	Message::Status status = (Message::Status)statusIn;

	mDbm->updateMessage(timestamp, status);
}

void ManagedDbManager::updateMessage( Int64 timestamp, ManagedDataTypes::Message::MsgStatus statusIn, String^ msgIdIn )
{
	Message::Status status = (Message::Status)statusIn;
	std::string	    msgId  = Conversion::M2U_String(msgIdIn);

	mDbm->updateMessage(timestamp, status, msgId);
}

void ManagedDbManager::updateMessage(Int64 timestamp, Int64 serverTimestamp, ManagedDataTypes::Message::MsgStatus statusIn)
{
	Message::Status status = (Message::Status)statusIn;

	mDbm->updateMessage(timestamp, serverTimestamp, status);
}

void ManagedDbManager::updateMessage(Int64 timestamp, Int64 serverTimestamp, ManagedDataTypes::Message::MsgStatus statusIn, String^ msgIdIn )
{
	Message::Status status = (Message::Status)statusIn;
	std::string	    msgId = Conversion::M2U_String(msgIdIn);

	mDbm->updateMessage( timestamp, serverTimestamp, status, msgId );
}

void ManagedDbManager::updateTranslatedMessage(Int64 timestamp, ManagedDataTypes::Message^ msgIn)
{
	Message msg = Conversion::M2U_Message(msgIn);
	mDbm->updateTranslatedMessage(timestamp, msg);
}

void ManagedDbManager::updateMessageContactInfo( ManagedDataTypes::VoxoxContactList^ vcListIn )
{
	VoxoxContactList vcList = Conversion::M2U_VoxoxContactList( vcListIn );

	mDbm->updateMessageContactInfo( vcList );
}

void ManagedDbManager::markMessageDelivered(Int64 timestamp, Boolean broadcast )
{
	mDbm->markMessageDelivered(timestamp, broadcast );
}

void ManagedDbManager::markMessageRead(Int64 timestamp, Boolean broadcast )
{
	mDbm->markMessageRead(timestamp, broadcast );
}

Boolean ManagedDbManager::addMessage(ManagedDataTypes::Message^ messageIn, Boolean broadcastIn )
{
	Message msg = Conversion::M2U_Message( messageIn );

	return mDbm->addMessage(msg, (bool)broadcastIn);
}

void ManagedDbManager::addMessages(ManagedDataTypes::MessageList^  msgListIn )
{
	MessageList msgList = Conversion::M2U_MessageList( msgListIn );
	mDbm->addMessages(msgList);
}

Int32 ManagedDbManager::deleteMessages(ManagedDataTypes::MessageList^ messagesIn )
{
	Int32 result = 0;

	MessageList msgList = Conversion::M2U_MessageList( messagesIn );

	result = (Int32)mDbm->deleteMessages(msgList);

	return result;
}

Int32 ManagedDbManager::removeDeletedMessages( ManagedDataTypes::MessageList^ messagesIn )
{
	Int32 result = 0;

	MessageList msgList = Conversion::M2U_MessageList( messagesIn );

	result = (Int32)mDbm->removeDeletedMessages( msgList );

	return result;
}

ManagedDataTypes::DeleteMessageDataList^ ManagedDbManager::getMessageDataForDeletedMessages()
{
	DeleteMessageDataList listIn = mDbm->getMessageDataForDeletedMessages();

	ManagedDataTypes::DeleteMessageDataList^ listOut = Conversion::U2M_DeleteMessageDataList( listIn );

	return listOut;
}


Boolean ManagedDbManager::hasDeletedMessages()
{
	return mDbm->hasDeletedMessages();
}

Boolean ManagedDbManager::hasMessageId( String^ msgId )
{
	return mDbm->hasMessageId( Conversion::M2U_String( msgId ) );
}

Int64 ManagedDbManager::getLocalTimestampForMsgId( String^ msgId )
{
	return mDbm->getLocalTimestampForMsgId( Conversion::M2U_String( msgId ) );
}

ManagedDataTypes::RichData^ ManagedDbManager::richDataFromJsonText( String^ jsonText )
{
	RichData richData = mDbm->richDataFromJsonText( Conversion::M2U_String( jsonText ) );

	return Conversion::U2M_RichData( richData );
}

ManagedDataTypes::UnreadCounts^ ManagedDbManager::getUnreadItemCounts()
{
	UnreadCounts temp = mDbm->getUnreadItemCounts();

	return Conversion::U2M_UnreadCounts( temp );
}

ManagedDataTypes::UnreadCounts^ ManagedDbManager::getUnreadItemCounts( bool includeVoicemail, bool includeRecordedCalls, bool includeFaxes )
{
	UnreadCounts temp = mDbm->getUnreadItemCounts(includeVoicemail, includeRecordedCalls, includeFaxes);

	return Conversion::U2M_UnreadCounts(temp);
}



//---------------------------------------
// PUBLIC Contact/PhoneNumber Methods
//---------------------------------------
void ManagedDbManager::addAddressBookContacts( ManagedDataTypes::ContactImportList^ contactsIn )
{
	ContactImportList contacts = Conversion::M2U_ContactImportList( contactsIn );
	mDbm->addAddressBookContacts( contacts );
}

void ManagedDbManager::addOrUpdateVoxoxContacts( ManagedDataTypes::VoxoxContactList^ vcListIn )
{
	VoxoxContactList vcList = Conversion::M2U_VoxoxContactList( vcListIn );
	mDbm->addOrUpdateVoxoxContacts( vcList );
}

void ManagedDbManager::addNakedNumbers( ManagedDataTypes::VoxoxContactList^ vcListIn )
{
	VoxoxContactList vcList = Conversion::M2U_VoxoxContactList( vcListIn );
	mDbm->addNakedNumbers( vcList );
}

//void ManagedDbManager::addOrUpdateXmppContacts( ManagedDataTypes::VoxoxPhoneNumberList^ voxoxPhoneNumberListIn )
//{
//	VoxoxPhoneNumberList pnList = Conversion::M2U_VoxoxPhoneNumberList( voxoxPhoneNumberListIn );
//	mDbm->addOrUpdateXmppContacts( pnList );
//}

void ManagedDbManager::addOrUpdateXmppContact( String^ xmppAddressIn, String^ phoneNumberIn, String^ contactKeyIn, String^ sourceIn )
{
	std::string xmppAddress = Conversion::M2U_String( xmppAddressIn );
	std::string phoneNumber = Conversion::M2U_String( phoneNumberIn );
	std::string contactKey  = Conversion::M2U_String( contactKeyIn  );
	std::string source      = Conversion::M2U_String( sourceIn		);

	mDbm->addOrUpdateXmppContact( xmppAddress, phoneNumber, contactKey, source );
}

void ManagedDbManager::updateXmppPresence( ManagedDataTypes::XmppContact^ xcIn )
{
	XmppContact xc = Conversion::M2U_XmppContact( xcIn );

	mDbm->updateXmppPresence( xc );
}

void ManagedDbManager::updatePhoneNumberFavorite( String^ phoneNumberIn, Boolean newValue )
{
	std::string phoneNumber = Conversion::M2U_String( phoneNumberIn );
	mDbm->updatePhoneNumberFavorite( phoneNumber, (bool)newValue );
}

ManagedDataTypes::Contact^ ManagedDbManager::getContactByKey( String^ contactKeyIn )
{
	std::string contactKey = Conversion::M2U_String( contactKeyIn );

	Contact contact = mDbm->getContactByKey( contactKey );

	return Conversion::U2M_Contact( contact );
}

ManagedDataTypes::ContactList^ ManagedDbManager::getContacts( String^ searchFilterIn, ManagedDataTypes::ContactFilter contactFilterIn )	
{
	std::string searchFilter = Conversion::M2U_String( searchFilterIn );
	PhoneNumber::ContactFilter contactFilter = (PhoneNumber::ContactFilter)contactFilterIn;

	ContactList contactList = mDbm->getContacts( searchFilter, contactFilter );

	return Conversion::U2M_ContactList( contactList );
}

ManagedDataTypes::PhoneNumberList^ ManagedDbManager::getContactPhoneNumbers( String^ searchFilterIn, ManagedDataTypes::ContactFilter contactFilterIn )	
{
	std::string searchFilter = Conversion::M2U_String( searchFilterIn );
	PhoneNumber::ContactFilter contactFilter = (PhoneNumber::ContactFilter)contactFilterIn;

	PhoneNumberList pnList = mDbm->getContactPhoneNumbers( searchFilter, contactFilter );

	return Conversion::U2M_PhoneNumberList( pnList );
}

ManagedDataTypes::PhoneNumberList^ ManagedDbManager::getAllContactDetailsPhoneNumbers()
{
	PhoneNumberList pnList = mDbm->getAllContactDetailsPhoneNumbers();

	return Conversion::U2M_PhoneNumberList( pnList );
}

ManagedDataTypes::PhoneNumberList^ ManagedDbManager::getContactDetailsPhoneNumbersByKey( String^ contactKeyIn )
{
	std::string contactKey = Conversion::M2U_String( contactKeyIn );
	PhoneNumberList pnList = mDbm->getContactDetailsPhoneNumbersByKey( contactKey );

	return Conversion::U2M_PhoneNumberList( pnList );
}

ManagedDataTypes::PhoneNumberList^ ManagedDbManager::getContactDetailsPhoneNumbersByCmGroup( Int32 cmGroup )
{
	PhoneNumberList pnList = mDbm->getContactDetailsPhoneNumbersByCmGroup( cmGroup );

	return Conversion::U2M_PhoneNumberList( pnList );
}

ManagedDataTypes::PhoneNumberList^ ManagedDbManager::getContactPhoneNumbersListForCmGroup( Int32 cmGroup, Boolean isVoxox )
{
	PhoneNumberList pnList = mDbm->getContactPhoneNumbersListForCmGroup( cmGroup, isVoxox );

	return Conversion::U2M_PhoneNumberList( pnList );
}

ManagedDataTypes::PhoneNumber^ ManagedDbManager::getPhoneNumber( String^ phoneNumberStringIn,  Int32 cmGroup )
{
	std::string phoneNumber = Conversion::M2U_String( phoneNumberStringIn );

	PhoneNumber pn = mDbm->getPhoneNumber( phoneNumber, cmGroup );

	return Conversion::U2M_PhoneNumber( pn );
}

bool ManagedDbManager::phoneNumberExists( String^ phoneNumberStringIn )
{
	std::string phoneNumber = Conversion::M2U_String( phoneNumberStringIn );

	return mDbm->phoneNumberExists( phoneNumber );
}

bool ManagedDbManager::isNumberAnExtension( String^ phoneNumberStringIn )
{
	std::string phoneNumber = Conversion::M2U_String( phoneNumberStringIn );

	return mDbm->isNumberAnExtension( phoneNumber );
}

Int32 ManagedDbManager::getVoxoxUserId( String^ phoneNumberStringIn )
{
	std::string phoneNumber = Conversion::M2U_String( phoneNumberStringIn );
	return mDbm->getVoxoxUserId( phoneNumber );
}

Int32 ManagedDbManager::getVoxoxUserId( Int32 cmGroup)
{
	return mDbm->getVoxoxUserId( cmGroup );
}

Int32 ManagedDbManager::getCmGroup( String^ phoneNumberStringIn )
{
	std::string phoneNumber = Conversion::M2U_String( phoneNumberStringIn );
	return mDbm->getCmGroup( phoneNumber );
}

String^ ManagedDbManager::getDidByJid( String^ jidIn )
{
	std::string jid = Conversion::M2U_String( jidIn );
	std::string did = mDbm->getDidByJid( jid );

	return Conversion::U2M_String( did );
}

ManagedDataTypes::StringList^ ManagedDbManager::getVoxoxUserNamesWithNoAssociatedPhoneNumbers()
{
	StringList stdList = mDbm->getVoxoxUserNamesWithNoAssociatedPhoneNumbers();

	return Conversion::U2M_StringList( stdList );
}

Boolean ManagedDbManager::isXmppContactInLocalAddressBook( String^ jidIn )
{
	std::string jid = Conversion::M2U_String( jidIn );
	
	return mDbm->isXmppContactInLocalAddressBook( jid );
}

Int32 ManagedDbManager::getNewInboundMessageCountForCmGroup( Int32 cmGroup )
{
	return mDbm->getNewInboundMessageCountForCmGroup( cmGroup );
}

Int32 ManagedDbManager::getNewInboundMessageCountForCmGroup( Int32 cmGroup, bool includeVoicemail, bool includeRecordedCalls, bool includeFaxes )
{
	return mDbm->getNewInboundMessageCountForCmGroup(cmGroup, includeVoicemail, includeRecordedCalls, includeFaxes);
}

Int32 ManagedDbManager::getNewInboundMessageCount( String^ phoneNumberIn, String^ xmppAddressIn )
{
	std::string phoneNumber = Conversion::M2U_String( phoneNumberIn );
	std::string xmppAddress = Conversion::M2U_String( xmppAddressIn );

	return mDbm->getNewInboundMessageCount( phoneNumber, xmppAddress );
}

Int32 ManagedDbManager::getNewInboundMessageCount( String^ phoneNumberIn, String^ xmppAddressIn, bool includeVoicemail, bool includeRecordedCalls, bool includeFaxes )
{
	std::string phoneNumber = Conversion::M2U_String(phoneNumberIn);
	std::string xmppAddress = Conversion::M2U_String(xmppAddressIn);

	return mDbm->getNewInboundMessageCount(phoneNumber, xmppAddress, includeVoicemail, includeRecordedCalls, includeFaxes);
}

ManagedDataTypes::ContactMessageSummaryList^ ManagedDbManager::getContactsMessageSummary()
{
	ContactMessageSummaryList summaryList = mDbm->getContactsMessageSummary();

	return Conversion::U2M_ContactMessageSummaryList( summaryList );
}

ManagedDataTypes::ContactMessageSummaryList^ ManagedDbManager::getContactsMessageSummary( bool includeVoicemail, bool includeRecordedCalls, bool includeFaxes )
{
	ContactMessageSummaryList summaryList = mDbm->getContactsMessageSummary(includeVoicemail, includeRecordedCalls, includeFaxes);

	return Conversion::U2M_ContactMessageSummaryList(summaryList);
}

Int32 ManagedDbManager::getNewInboundMessageCountForGroupId( String^ groupIdIn )
{
	std::string groupId = Conversion::M2U_String( groupIdIn );

	return mDbm->getNewInboundMessageCountForGroupId( groupId );
}

Int32 ManagedDbManager::getNewInboundMessageCountForGroupId( String^ groupIdIn, bool includeVoicemail, bool includeRecordedCalls, bool includeFaxes )
{
	std::string groupId = Conversion::M2U_String(groupIdIn);

	return mDbm->getNewInboundMessageCountForGroupId(groupId, includeVoicemail, includeRecordedCalls, includeFaxes);
}

ManagedDataTypes::StringList^ ManagedDbManager::getPhoneNumbersWithInvalidCmGroup()
{
	StringList stdList = mDbm->getPhoneNumbersWithInvalidCmGroup();

	return Conversion::U2M_StringList( stdList );
}

//---------------------------------------
// PUBLIC MessageTranslationProperty Methods
//---------------------------------------
ManagedDataTypes::MessageTranslationProperties^ ManagedDbManager::getTranslationModeProps( String^ phoneNumberIn, String^ jidIn )
{
	std::string phoneNumber = Conversion::M2U_String( phoneNumberIn );
	std::string jid         = Conversion::M2U_String( jidIn         );

	MessageTranslationProperties mtp = mDbm->getTranslationModeProps( phoneNumber, jid );

	return Conversion::U2M_MessageTranslationProperties( mtp );
}

Boolean ManagedDbManager::addContactMessageTranslation( ManagedDataTypes::MessageTranslationProperties^ mtpIn )
{
	MessageTranslationProperties mtp = Conversion::M2U_MessageTranslationProperties( mtpIn );

	return mDbm->addContactMessageTranslation( mtp );
}

//---------------------------------------
// PUBLIC RecentCall Methods
//---------------------------------------

Boolean ManagedDbManager::addOrUpdateCall(ManagedDataTypes::RecentCall^ callIn, Boolean broadcastIn )
{
	RecentCall call = Conversion::M2U_RecentCall( callIn );

	return mDbm->addOrUpdateCall( call, (bool)broadcastIn);
}

void ManagedDbManager::addOrUpdateCalls( ManagedDataTypes::RecentCallList^  callListIn )
{
	RecentCallList callList = Conversion::M2U_RecentCallList( callListIn );
	mDbm->addOrUpdateCalls( callList );
}

bool ManagedDbManager::callExists( String^ uid )
{
	return mDbm->callExists( Conversion::M2U_String( uid ) );
}

Int64 ManagedDbManager::getLatestCallServerTimestamp()
{
	Int64 result = mDbm->getLatestCallServerTimestamp();
	return result;
}

Int32 ManagedDbManager::addRecentCall( String^ phoneNumberIn, Int64 startTime, Boolean isIncoming )
{
	return addRecentCall( phoneNumberIn, startTime, isIncoming, false );
}

Int32 ManagedDbManager::addRecentCall( String^ phoneNumberIn, Int64 startTime, Boolean isIncoming, Boolean isLocal )
{
	std::string phoneNumber = Conversion::M2U_String( phoneNumberIn );
	return mDbm->addRecentCall( phoneNumber, startTime, isIncoming, isLocal );
}

//For unit tests only.  DO NOT CALL THIS IN APP.
Int32 ManagedDbManager::addRecentCall( String^ phoneNumberIn, Boolean isIncoming, ManagedDataTypes::RecentCall::CallStatus statusIn, Int64 startTime )
{
	std::string phoneNumber = Conversion::M2U_String( phoneNumberIn );
	RecentCall::Status status = (RecentCall::Status)statusIn;

	return mDbm->addRecentCall( phoneNumber, isIncoming, status, startTime );
}

void ManagedDbManager::updateRecentCallAnswered( Int32 callId )
{
	mDbm->updateRecentCallAnswered( callId );
}

void ManagedDbManager::updateRecentCallDeclined( Int32 callId )
{
	mDbm->updateRecentCallDeclined( callId );
}

void ManagedDbManager::updateRecentCallTerminated( Int32 callId )
{
	mDbm->updateRecentCallTerminated( callId );
}

bool ManagedDbManager::updateRecentCallSeen( Int32 callId )
{
	return mDbm->updateRecentCallSeen( callId );
}

Int32 ManagedDbManager::deleteRecentCall( Int32 callId )
{
	return mDbm->deleteRecentCall( callId );
}

ManagedDataTypes::RecentCall^ ManagedDbManager::getRecentCall( Int32 callId )
{
	RecentCall recentCall = mDbm->getRecentCall( callId );
	return Conversion::U2M_RecentCall( recentCall );
}

ManagedDataTypes::RecentCall^ ManagedDbManager::getRecentCallByUid( String^ uidIn )
{
	std::string uid = Conversion::M2U_String( uidIn );
	RecentCall recentCall = mDbm->getRecentCallByUid( uid );
	return Conversion::U2M_RecentCall( recentCall );
}

ManagedDataTypes::RecentCallList^ ManagedDbManager::getRecentCalls( String^ phoneNumberIn )
{
	std::string phoneNumber = Conversion::M2U_String( phoneNumberIn );
	RecentCallList list = mDbm->getRecentCalls( phoneNumber );
	return Conversion::U2M_RecentCallList( list);
}

ManagedDataTypes::RecentCallList^ ManagedDbManager::getRecentCalls( Int32 cmGroup )
{
	RecentCallList list = mDbm->getRecentCalls( cmGroup );
	return Conversion::U2M_RecentCallList( list);
}

Int32 ManagedDbManager::clearLocalCalls()
{
	return mDbm->clearLocalCalls();
}

//----------------------------------------
//PUBLIC Settings methods
//----------------------------------------
ManagedDataTypes::UserSettings^ ManagedDbManager::getUserSettings()
{
	UserSettings settings = mDbm->getUserSettings();
	return Conversion::U2M_UserSettings( settings );
}

bool ManagedDbManager::updateUserSettings( ManagedDataTypes::UserSettings^ src )
{
	UserSettings settings = Conversion::M2U_UserSettings( src );
	return mDbm->updateUserSettings( settings );
}

//----------------------------------------
//PUBLIC App Settings methods - NOT PART OF USER DB
//----------------------------------------
ManagedDataTypes::AppSettings^ ManagedDbManager::getAppSettings()
{
	AppSettings settings = mDbm->getAppSettings();
	return Conversion::U2M_AppSettings( settings );
}

bool ManagedDbManager::updateAppSettings( ManagedDataTypes::AppSettings^ src )
{
	AppSettings settings = Conversion::M2U_AppSettings( src );
	return mDbm->updateAppSettings( settings );
}

//---------------------------------------
// PUBLIC Miscellaneous calls
//---------------------------------------
void ManagedDbManager::broadcastMessagesDatasetChanged()
{
	mDbm->broadcastMessagesDatasetChanged();
}

void ManagedDbManager::broadcastRecentCallDatasetChanged()
{
	mDbm->broadcastRecentCallDatasetChanged();
}

} //namespace ManagedDbManager
