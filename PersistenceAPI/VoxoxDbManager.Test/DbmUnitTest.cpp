/**
* Voxox
* Copyright (C) 2004-2014 Voxox
*/

#include "stdafx.h"
#include "CppUnitTest.h"

#include "../VoxoxDbManager/DbManager.h"
#include "../VoxoxDbManager/DatabaseTables.h"
#include "../VoxoxDbManager/DbUtils.h"

//for string - wstring conversion
//#include <locale>
//#include <codecvt>
#include <string>

//TODO: causees C4005 '__userHeader' : macro redefinition and '__on_failure' : macro redefinition on Win32 builds
#include "Windows.h"		//For Sleep(). Keep last to avoid weird conflict with GROUP_NAME	

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace DbmUnitTest
{		

TEST_CLASS( VoxoxDbManagerTest1 )
{
	std::string		mDbName;
	VoxoxDbManager* mDbm;

	std::string		mTestToGroupId;
	int				mTestFromUserId;

	int				mABContactCount;
	int				mTotalPhoneNumberCount;

//	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> mConverter;

public:
		
	VoxoxDbManagerTest1()
	{
		mDbName = "D:\\Temp\\test.db";
		mDbm    = NULL;

		mTestToGroupId  = "GMG-ID-12345";
		mTestFromUserId = 123456;
		
		mABContactCount			= 0;
		mTotalPhoneNumberCount	= 0;
	}

//	std::wstring toWide( const std::string& textIn )
//	{
//		return mConverter.from_bytes( textIn.c_str() );
//	}

	VoxoxDbManager* getDbm()
	{
		return mDbm;
	}

	void openDb()
	{
		if ( mDbm == NULL )
		{
			mDbm = new VoxoxDbManager( mDbName, VoxoxDbManager::DbType::User );
			mDbm->create();
		}
	}

	void closeDb()
	{
		if ( mDbm )
		{
			mDbm->close();
		}
	}

	void clearMessageTable()
	{
		getDbm()->clearMessageTable();
	}

	void clearPhoneNumberTable()
	{
		getDbm()->clearPhoneNumberTable();
	}

	Message createTestMessage( Message::Type type, Message::Direction dir, Message::Status status, const std::string& text )
	{
		::Sleep( 2 );	//So we get different timestamps

		__int64 localTs = DbUtils::getSystemTimeMillis();
		std::string localTsString = std::to_string( localTs );
	
		Message msg;

		msg.setType	    ( type );
		msg.setDirection( dir  );
		msg.setStatus	( status );

		msg.setMsgId	( "UUID-" + localTsString );

		msg.setBody	    ( "Test Body " + text );

		if ( msg.isSms() )
		{
			if ( dir == Message::INBOUND )
			{
				msg.setFromDid( "17605551111" );
				msg.setToDid  ( "17602775395" );
			}
			else
			{
				msg.setFromDid( "17602775395" );
				msg.setToDid  ( "17605551111" );
			}
		}
		else if ( msg.isChat() )
		{
			if ( dir == Message::INBOUND )
			{
				msg.setFromJid  ( "jrttest4a" );
				msg.setToJid	( "jrttest7b" );
				msg.setContactDid( "17605551111" );	//Not sure this will work.
			}
			else
			{
				msg.setFromJid  ( "jrttest7b" );
				msg.setToJid	( "jrttest4a" );
				msg.setContactDid( "17602775395" );	//Not sure this will work.
			}
		}
		else if ( msg.isGroupMessage() )
		{
			msg.setToGroupId ( mTestToGroupId  );
			msg.setFromUserId( mTestFromUserId );
		}
		else if ( msg.isVoicemail() )
		{
			msg.setCName   ( "CName " + text     );
			msg.setIAccount( "IAcct " + text     );
		}
		else
		{
			int xxx = 1;
		}

		msg.setLocalTimestamp ( localTs );
		msg.setServerTimestamp( localTs );

		return msg;
	}

	//Initialize phoneNumber table for Contact/PhoneNumber based Unit Tests
	size_t initPhoneNumbers()
	{
		int result = 0;

		openDb();
		clearPhoneNumberTable();

		ContactImportList ciList;
		
		ciList.push_back( ContactImport( "1", "Contact 1", "17605551111", "Work"   ) );
		ciList.push_back( ContactImport( "1", "Contact 1", "17605552222", "Mobile" ) );
		ciList.push_back( ContactImport( "2", "Contact 2", "17605553333", "Home"   ) );
		ciList.push_back( ContactImport( "3", "Contact 3", "17605554444", "Mobile" ) );

		getDbm()->addAddressBookContacts( ciList );
		
		mABContactCount		   = 3;
		mTotalPhoneNumberCount = 4;

		return ciList.size();
	}

	//Initialize phoneNumber table for Contact/PhoneNumber based Unit Tests which need XMPP data.
	size_t initPhoneNumbersEx()
	{
		int result = 0;

		openDb();
		clearPhoneNumberTable();

		ContactImportList ciList;
		
		ciList.push_back( ContactImport( "1", "Contact 1", "17605551111", "Work"   ) );
		ciList.push_back( ContactImport( "1", "Contact 1", "17605552222", "Mobile" ) );
		ciList.push_back( ContactImport( "2", "Contact 2", "17605553333", "Home"   ) );
		ciList.push_back( ContactImport( "3", "Contact 3", "17605554444", "Mobile" ) );

		getDbm()->addAddressBookContacts( ciList );

		//Add some Voxox user info
		int			userId1		= 111111;
		int			userId2		= 222222;
		std::string userName1	= "jrttest4a";
		std::string userName2	= "jrttest7b";

		VoxoxContactList vcs;
		vcs.push_back( VoxoxContact( "abc1","17605552222", userName1, "17605551111", "Voxox", userId1, false, false, "Test" ) );
		vcs.push_back( VoxoxContact( "abc2", "",           userName2, "17605553333", "Voxox", userId2, false, false, "Test" ) );

		/**
		* Add or updated DB based on VoxoxContact info.
		*/
		getDbm()->addOrUpdateVoxoxContacts( vcs );

		mABContactCount			= 3;
		mTotalPhoneNumberCount	= 4;

		return ciList.size();
	}

	
	int addTestRecentCall( const std::string& number, bool incoming, RecentCall::Status status, __int64 timestamp )
	{
		int callId = getDbm()->addRecentCall( number, incoming, status, timestamp );
		return callId;
	}

	//-------------------------------------------------
	// DB Admin tests
	//-------------------------------------------------


	//Exists
	//Create
	TEST_METHOD( testDbCreate )
	{
		bool exists = false;

		//DB should be created on first call to DBM.getInstance
		mDbm = new VoxoxDbManager( mDbName, VoxoxDbManager::DbType::User );
		mDbm->create();

		//Verify DB now exists.
		exists = getDbm()->checkDatabase();

		Assert::IsTrue( exists, L"DB does NOT exists when it should.", LINE_INFO() );

		//Verify the tables exist.  
		//TODO: Ideally, we would get a list of tables names from the Database class.
		StringList tables;
		tables.push_back( DatabaseTables::PhoneNumber::TABLE		);
		tables.push_back( DatabaseTables::Message::TABLE			);
		tables.push_back( DatabaseTables::GroupMessageGroup::TABLE	);
		tables.push_back( DatabaseTables::GroupMessageMember::TABLE );
		tables.push_back( DatabaseTables::Translation::TABLE		);
		tables.push_back( DatabaseTables::RecentCall::TABLE			);

		for ( StringList::iterator it = tables.begin(); it != tables.end(); it++ )
		{
			std::string tableName = (*it);
		
			exists = getDbm()->checkTableExists( tableName );
			Assert::IsTrue( exists, L"Tables does NOT exist", LINE_INFO() );
		}
	}


	//Open - readonly, readwrite
	//Close
	//Remove



	//-------------------------------------------------
	// Message tests
	//-------------------------------------------------

	TEST_METHOD( testAddMessage )		//Also tests getMessage( timestamp )
	{
		openDb();
		clearMessageTable();
	
		Message msgIn = createTestMessage( Message::SMS,  Message::INBOUND, Message::NEW, "testAddMsg" );
		__int64 localTs = msgIn.getLocalTimestamp();

		bool added = getDbm()->addMessage( msgIn, false );

		if ( added )
		{
			Message msgOut = getDbm()->getMessage( localTs );

			bool match = (msgOut.getLocalTimestamp() == localTs);

			Assert::IsTrue( match, L"Local timestamps do not match", LINE_INFO() );
		}
		else
		{
			Assert::Fail( L"Message was not added", LINE_INFO() );
		}

		//Test latest time stamp - Assert does not seem to support __int64 for AreEqual, so convert to string.
		__int64 latestTs = getDbm()->getLatestMessageServerTimestamp( Message::SMS, Message::INBOUND, Message::NEW );

		std::wstring strLocalTs  = std::to_wstring( localTs  );
		std::wstring strLatestTs = std::to_wstring( latestTs );

		Assert::AreEqual( strLocalTs, strLatestTs, L"Latest timestamp does not match", LINE_INFO() );
	}

	//TEST_METHOD( testGetLatestMessageServerTimestamp )	//Tested in testAddMessage()
	//{
	//	openDb();
	//	/**
	//	 * returns the last inserted server_timestamp, so we can query server for newer messages.
	//	 */
	//	long getLatestMessageServerTimestamp( Message::Type type, Message::Direction direction, Message::Status status );
	//}

	//TEST_METHOD( testGetMessageByTimestamp )				//Tested in testAddMessage()
	//{
	//	/**
	//	 * returns Message with given LOCAL timestamp.
	//	 */
	//	Message getMessage( long timeStamp );
	//}


	TEST_METHOD( testGetMessages )
	{
		openDb();
		clearMessageTable();

		Message msg1 = createTestMessage( Message::SMS,  Message::INBOUND, Message::NEW, "testGetMessages 1" );
		Message msg2 = createTestMessage( Message::SMS,  Message::INBOUND, Message::NEW, "testGetMessages 2" );
		Message msg3 = createTestMessage( Message::SMS,  Message::INBOUND, Message::NEW, "testGetMessages 3" );

		bool added1 = getDbm()->addMessage( msg1, false );
		bool added2 = getDbm()->addMessage( msg2, false );
		bool added3 = getDbm()->addMessage( msg3, false );

		if ( added1 && added2 && added3 )
		{
			/**
			 * returns List of messages based on criteria.
			 */
			MessageList msgList1 = getDbm()->getMessages( Message::NEW, Message::INBOUND );

			Assert::IsTrue( msgList1.size() == 3, L"MsgList1 is wrong size", LINE_INFO() );

			MessageList msgList2 = getDbm()->getMessages( Message::NEW, Message::OUTBOUND );

			Assert::IsTrue( msgList2.size() == 0, L"MsgList2 is wrong size", LINE_INFO() );
		}
		else
		{
			Assert::Fail( L"Not all test records were added", LINE_INFO() );
		}
	}


	TEST_METHOD( testGetMessagesForGroupId )	//Not needed for phase 1
	{
		openDb();
		clearMessageTable();

		Message msg1 = createTestMessage( Message::SMS,            Message::INBOUND, Message::NEW, "testGetMessagesForGroupId" );
		Message msg2 = createTestMessage( Message::GROUP_MESSAGE,  Message::INBOUND, Message::NEW, "testGetMessagesForGroupId" );
		Message msg3 = createTestMessage( Message::GROUP_MESSAGE,  Message::OUTBOUND, Message::NEW, "testGetMessagesForGroupId" );

		bool added1 = getDbm()->addMessage( msg1, false );
		bool added2 = getDbm()->addMessage( msg2, false );
		bool added3 = getDbm()->addMessage( msg3, false );

		if ( added1 && added2 && added3 )
		{
			/**
			 * returns a cursor to all the messages from an GM GroupId
			 */
			UmwMessageList msgList1 = getDbm()->getMessagesForGroupId( mTestToGroupId );

			Assert::IsTrue( msgList1.size() == 2, L"MsgList1 is wrong size", LINE_INFO() );

			UmwMessageList msgList2 = getDbm()->getMessagesForGroupId( "xxxxx" );

			Assert::IsTrue( msgList2.size() == 0, L"MsgList2 is wrong size", LINE_INFO() );
		}
		else
		{
			Assert::Fail( L"Not all test records were added", LINE_INFO() );
		}
	}

	TEST_METHOD( testUpdateMessageStatus )
	{
		openDb();
		clearMessageTable();

		Message msg1 = createTestMessage( Message::SMS, Message::INBOUND, Message::NEW, "testUpdateMessage" );
		__int64 localTs = msg1.getLocalTimestamp();

		bool added1 = getDbm()->addMessage( msg1, false );

		if ( added1 )
		{
			/**
			* Update message status based on LOCAL timestamp
			*/
			getDbm()->updateMessage( localTs, Message::READ );

			Message msgOut = getDbm()->getMessage( localTs );

			bool match = (msgOut.getStatus() == Message::READ);

			Assert::IsTrue( match, L"Status was NOT updated", LINE_INFO() );
		}
		else
		{
			Assert::Fail( L"Not all test records were added", LINE_INFO() );
		}
	}

	TEST_METHOD( testUpdateMessageStatusAndMsgId )
	{
		openDb();
		clearMessageTable();

		Message msg1 = createTestMessage( Message::SMS, Message::INBOUND, Message::NEW, "testUpdateMessageStatusAndMsgId" );
		__int64 localTs = msg1.getLocalTimestamp();

		bool added1 = getDbm()->addMessage( msg1, false );

		if ( added1 )
		{
			std::string newMsgId = "Updated MsgId";
			/**
			* Update message status and MessageId based on LOCAL timestamp
			*/
			getDbm()->updateMessage( localTs, Message::READ, newMsgId );

			Message msgOut = getDbm()->getMessage( localTs );

			bool match1 = (msgOut.getStatus() == Message::READ);
			bool match2 = (msgOut.getMsgId()  == newMsgId     );

			Assert::IsTrue( match1, L"Status was NOT updated", LINE_INFO() );
			Assert::IsTrue( match2, L"MsgId  was NOT updated", LINE_INFO() );
		}
		else
		{
			Assert::Fail( L"Not all test records were added", LINE_INFO() );
		}
	}

	TEST_METHOD( testUpdateMessageStatusAndServerTimestamp )
	{
		openDb();
		clearMessageTable();

		Message msg1 = createTestMessage( Message::SMS, Message::INBOUND, Message::NEW, "testUpdateMessageStatusAndServerTimestamp" );
		__int64 localTs = msg1.getLocalTimestamp();

		bool added1 = getDbm()->addMessage( msg1, false );

		if ( added1 )
		{
			__int64 serverTs = localTs + 200;

			/**
			* Update message status and SERVER timestamp based on LOCAL timestamp
			*/
			getDbm()->updateMessage( localTs, serverTs, Message::READ );

			Message msgOut = getDbm()->getMessage( localTs );

			bool match1 = (msgOut.getStatus()			== Message::READ );
			bool match2 = (msgOut.getServerTimestamp() == serverTs		 );

			Assert::IsTrue( match1, L"Status was NOT updated", LINE_INFO() );
			Assert::IsTrue( match2, L"Server TS was NOT updated", LINE_INFO() );
		}
		else
		{
			Assert::Fail( L"Not all test records were added", LINE_INFO() );
		}
	}

	TEST_METHOD( testUpdateMessageStatusAndServerTsAndMsgId )
	{
		openDb();
		clearMessageTable();

		Message msg1 = createTestMessage( Message::SMS, Message::INBOUND, Message::NEW, "testUpdateMessageStatusAndServerTimestamp" );
		__int64 localTs = msg1.getLocalTimestamp();

		bool added1 = getDbm()->addMessage( msg1, false );

		if ( added1 )
		{
			__int64		serverTs = localTs + 200;
			std::string newMsgId = "Updated MsgId";

			/**
			* Update message status, SERVER timestamp and MessageId based on LOCAL timestamp
			*/
			getDbm()->updateMessage( localTs, serverTs, Message::READ, newMsgId );

			Message msgOut = getDbm()->getMessage( localTs );

			bool match1 = (msgOut.getStatus()		   == Message::READ  );
			bool match2 = (msgOut.getServerTimestamp() == serverTs		 );
			bool match3 = (msgOut.getMsgId()		   == newMsgId		 );

			Assert::IsTrue( match1, L"Status was NOT updated",    LINE_INFO() );
			Assert::IsTrue( match2, L"Server TS was NOT updated", LINE_INFO() );
			Assert::IsTrue( match3, L"MsgId was NOT updated",     LINE_INFO() );
		}
		else
		{
			Assert::Fail( L"Not all test records were added", LINE_INFO() );
		}
	}

	TEST_METHOD( testMarkMessageDelivered )
	{
		openDb();
		clearMessageTable();

		Message msg1 = createTestMessage( Message::SMS, Message::INBOUND, Message::NEW, "testUpdateMessage" );
		__int64 localTs = msg1.getLocalTimestamp();

		bool added1 = getDbm()->addMessage( msg1, false );

		if ( added1 )
		{
			/**
			* Appears to be a convenience method to udpate message status as DELIVERED
			*/
			getDbm()->markMessageDelivered( localTs, true );

			Message msgOut = getDbm()->getMessage( localTs );

			bool match = (msgOut.getStatus() == Message::DELIVERED);

			Assert::IsTrue( match, L"Status was NOT updated", LINE_INFO() );
		}
		else
		{
			Assert::Fail( L"Not all test records were added", LINE_INFO() );
		}
	}

	TEST_METHOD( testMarkMessageRead )
	{
		openDb();
		clearMessageTable();

		Message msg1 = createTestMessage( Message::SMS, Message::INBOUND, Message::NEW, "testUpdateMessage" );
		__int64 localTs = msg1.getLocalTimestamp();

		bool added1 = getDbm()->addMessage( msg1, false );

		if ( added1 )
		{
			/**
			* Appears to be a convenience method to udpate message status as READ
			*/
			getDbm()->markMessageRead( localTs, true );

			Message msgOut = getDbm()->getMessage( localTs );

			bool match = (msgOut.getStatus() == Message::READ);

			Assert::IsTrue( match, L"Status was NOT updated", LINE_INFO() );
		}
		else
		{
			Assert::Fail( L"Not all test records were added", LINE_INFO() );
		}
	}

	TEST_METHOD( testUpdateMessageContactInfo )
	{
		openDb();
		clearMessageTable();

		//NOTE: This DBM method only updates CHAT messages with matching FROM_JID.
		//	It also uses INBOUND and OUTBOUND, but it seems this would only be needed for INBOUND (outbound CHATs coming from us.)

		//Generate some test messages.
		Message msg1 = createTestMessage( Message::CHAT, Message::INBOUND,  Message::NEW, "testUpdateMessageContactInfo 1" );
		Message msg2 = createTestMessage( Message::CHAT, Message::OUTBOUND, Message::NEW, "testUpdateMessageContactInfo 1" );
		Message msg3 = createTestMessage( Message::CHAT, Message::INBOUND,  Message::NEW, "testUpdateMessageContactInfo 2" );
		Message msg4 = createTestMessage( Message::CHAT, Message::OUTBOUND, Message::NEW, "testUpdateMessageContactInfo 2" );

		//Tweak the message to match our test data below.
		msg1.setFromJid( "jrttest4a@voxox.com" );
		msg2.setToJid  ( "jrttest4a@voxox.com" );
		msg3.setFromJid( "jrttest4b@voxox.com" );
		msg4.setToJid  ( "jrttest4b@voxox.com" );

		msg1.setToJid  ( "jrttest7b@voxox.com" );
		msg2.setFromJid( "jrttest7b@voxox.com" );
		msg3.setToJid  ( "jrttest7b@voxox.com" );
		msg4.setFromJid( "jrttest7b@voxox.com" );

		bool added1 = getDbm()->addMessage( msg1, false );
		bool added2 = getDbm()->addMessage( msg2, false );
		bool added3 = getDbm()->addMessage( msg3, false );
		bool added4 = getDbm()->addMessage( msg4, false );

		if ( added1 && added2 && added3 && added4 )
		{
			//Verify To/From DID are empty
			MessageList msgList1 = getDbm()->getMessages( Message::NEW, Message::INBOUND );

			for ( Message msg : msgList1 )
			{
				if ( !msg.getToDid().empty() )
					Assert::Fail( L"ToDid is NOT empty.", LINE_INFO() );

				if ( !msg.getFromDid().empty() )
					Assert::Fail( L"FromDid is NOT empty.", LINE_INFO() );

				if ( !msg.getContactDid().empty() )
					Assert::Fail( L"ContactDid is NOT empty.", LINE_INFO() );
			}

			//Simulate data we would have retrieved via REST API call
			VoxoxContactList vcs;

			VoxoxContact vc1;
			vc1.setDid( "17602775395" );
			vc1.setRegistrationNumber( "" );
			vc1.setUserId( 111111 );
			vc1.setUserName( "jrttest4a" );

			VoxoxContact vc2;
			vc2.setDid( "17602779999" );
			vc2.setRegistrationNumber( "" );
			vc2.setUserId( 222222 );
			vc2.setUserName( "jrttest4b" );

			vcs.push_back( vc1 );
			vcs.push_back( vc2 );

			/**
			* Some times we receive message from contacts not in our address book.
			*
			* We log the original message, then use the phone_number to determine if it is a Voxox user.  This is done asynchrously.
			*
			* This method updates those messages, by calling the single instance version below.
			*/
			getDbm()->updateMessageContactInfo( vcs );


			////Verify To/From DID are NOT empty for INBOUND messages only.
			MessageList msgList2 = getDbm()->getMessages( Message::NEW, Message::INBOUND );

			//Should have 2 entries, one for each JID.  
			//	ASSUMING these are in same order we added them.
			for ( Message msg : msgList2 )
			{
				bool match = false;

				if ( msg.getFromJid() == "jrttest4a@voxox.com" )
				{
					match = msg.getFromDid() == vc1.getDid();
					Assert::IsTrue( match, L"FromDid is not correct", LINE_INFO() );
				}
				else if ( msg.getFromJid() == "jrttest4b@voxox.com" )
				{
					match = msg.getFromDid() == vc2.getDid();
					Assert::IsTrue( match, L"FromDid is not correct", LINE_INFO() );
				}
				else
				{
					Assert::Fail( L"Found unexpected FromJid", LINE_INFO() );
				}
			}

		}
		else
		{
			Assert::Fail( L"Not all test records were added", LINE_INFO() );
		}
	}

	TEST_METHOD( testAddMessages )
	{
		openDb();
		clearMessageTable();

		//Generate some test messages.
		Message msg1 = createTestMessage( Message::CHAT, Message::INBOUND, Message::NEW, "testAddMessages 1" );
		Message msg2 = createTestMessage( Message::CHAT, Message::INBOUND, Message::NEW, "testAddMessages 2" );
		Message msg3 = createTestMessage( Message::CHAT, Message::INBOUND, Message::NEW, "testAddMessages 3" );
		Message msg4 = createTestMessage( Message::CHAT, Message::INBOUND, Message::NEW, "testAddMessages 4" );

		//Add to list
		MessageList msgListIn;
		msgListIn.push_back( msg1 );
		msgListIn.push_back( msg2 );
		msgListIn.push_back( msg3 );
		msgListIn.push_back( msg4 );


		/**
		*	Add list of messages to DB.
		*	Typically done from getMessageHistory() API call.
		*/
		getDbm()->addMessages( msgListIn );

		MessageList msgListOut = getDbm()->getMessages( Message::NEW, Message::INBOUND );

		Assert::AreEqual( msgListOut.size(), msgListIn.size(), L"Msg counts do not match", LINE_INFO() );
	}

	TEST_METHOD( testDeleteMessages )
	{
		//The DBM.deleteMessages() method simply changes STATUS to DELETED in the local DB.  We have a separate REST API call to delete them
		//	from the server, and the results of that API call will then remove the records from the local DB.

		//For this test, we do same as testAddMessges, but then add a final step to DELETE and check for success.
		openDb();
		clearMessageTable();

		//Generate some test messages.
		Message msg1 = createTestMessage( Message::CHAT, Message::INBOUND, Message::NEW, "testDeleteMessages 1" );
		Message msg2 = createTestMessage( Message::CHAT, Message::INBOUND, Message::NEW, "testDeleteMessages 2" );
		Message msg3 = createTestMessage( Message::CHAT, Message::INBOUND, Message::NEW, "testDeleteMessages 3" );
		Message msg4 = createTestMessage( Message::CHAT, Message::INBOUND, Message::NEW, "testDeleteMessages 4" );

		//Add to list
		MessageList msgListIn;
		msgListIn.push_back( msg1 );
		msgListIn.push_back( msg2 );
		msgListIn.push_back( msg3 );
		msgListIn.push_back( msg4 );


		/**
		*	Add list of messages to DB.
		*	Typically done from getMessageHistory() API call.
		*/
		getDbm()->addMessages( msgListIn );

		MessageList msgListOut = getDbm()->getMessages( Message::NEW, Message::INBOUND );

		Assert::AreEqual( msgListOut.size(), msgListIn.size(), L"Msg counts do not match", LINE_INFO() );

		//Verify the status is NOT DELETED
		for ( Message msg : msgListOut )
		{
			Assert::IsTrue( msg.getStatus() != Message::DELETED, L"Message staus == DELETED when it should not be.", LINE_INFO() );
		}

		//Now delete the messages
		size_t deleted = getDbm()->deleteMessages( msgListOut );

		Assert::AreEqual( deleted, msgListOut.size(), L"Deleted count does not match number to be deleted.", LINE_INFO() );

		//Verify they all have DELETED status
		MessageList msgListOut2 = getDbm()->getMessages( Message::DELETED, Message::INBOUND );

		Assert::AreEqual( msgListOut2.size(), msgListOut.size(), L"Msg counts do not match", LINE_INFO() );

		//Verify the status is NOT DELETED
		for ( Message msg : msgListOut2 )
		{
			Assert::IsTrue( msg.getStatus() == Message::DELETED, L"Message staus NOT DELETED when it should not be.", LINE_INFO() );
		}
	}

	TEST_METHOD( testGetMessageDataForDeletedMessages )
	{
		//Follow same steps as testDeleteMessages() above, and add final call for desired data.	//TODO: refactor and re-use?
		openDb();
		clearMessageTable();

		//Generate some test messages.
		Message msg1 = createTestMessage( Message::CHAT, Message::INBOUND, Message::NEW, "testDeleteMessages 1" );
		Message msg2 = createTestMessage( Message::CHAT, Message::INBOUND, Message::NEW, "testDeleteMessages 2" );
		Message msg3 = createTestMessage( Message::CHAT, Message::INBOUND, Message::NEW, "testDeleteMessages 3" );
		Message msg4 = createTestMessage( Message::CHAT, Message::INBOUND, Message::NEW, "testDeleteMessages 4" );

		//Add to list
		MessageList msgListIn;
		msgListIn.push_back( msg1 );
		msgListIn.push_back( msg2 );
		msgListIn.push_back( msg3 );
		msgListIn.push_back( msg4 );


		/**
		*	Add list of messages to DB.
		*	Typically done from getMessageHistory() API call.
		*/
		getDbm()->addMessages( msgListIn );

		MessageList msgListOut = getDbm()->getMessages( Message::NEW, Message::INBOUND );

		Assert::AreEqual( msgListOut.size(), msgListIn.size(), L"Msg counts do not match", LINE_INFO() );

		//Verify the status is NOT DELETED
		for ( Message msg : msgListOut )
		{
			Assert::IsTrue( msg.getStatus() != Message::DELETED, L"Message staus == DELETED when it should not be.", LINE_INFO() );
		}

		//Now delete the messages
		size_t deleted = getDbm()->deleteMessages( msgListOut );

		Assert::AreEqual( deleted, msgListOut.size(), L"Deleted count does not match number to be deleted.", LINE_INFO() );

		//Verify they all have DELETED status
		MessageList msgListOut2 = getDbm()->getMessages( Message::DELETED, Message::INBOUND );

		Assert::AreEqual( msgListOut2.size(), msgListOut.size(), L"Msg counts do not match", LINE_INFO() );

		//Verify the status is NOT DELETED
		for ( Message msg : msgListOut2 )
		{
			Assert::IsTrue( msg.getStatus() == Message::DELETED, L"Message staus NOT DELETED when it should not be.", LINE_INFO() );
		}

		/*
		*	Gathers data needed to delete messages from the server.
		*
		*	This looks for all local messages with status DELETED.
		*	Once this data is used in the API call to delete the messages on server, 
		*	a separate method will delete these from the local DB.
		*/
		DeleteMessageDataList deleteList = getDbm()->getMessageDataForDeletedMessages();

		Assert::AreEqual( deleteList.size(), msgListOut2.size(), L"Msg counts do not match", LINE_INFO() );
	}


	TEST_METHOD( testUpdateMessageRichData )
	{
		//Follow same steps as testDeleteMessages() above, and add final call for desired data.	//TODO: refactor and re-use?
		openDb();
		clearMessageTable();

		//Generate some test messages.
		Message msg1 = createTestMessage( Message::CHAT, Message::OUTBOUND, Message::NEW, "testUpdateMessageRichData 1" );
		__int64 localTs = msg1.getLocalTimestamp();

		bool added1 = getDbm()->addMessage( msg1, false );

		if ( added1 )
		{
			RichData richData1;

			richData1.setType( RichData::Type::IMAGE );
			richData1.setMediaUrl    ( "http::/voxox.com/test/mediaUrl" );
			richData1.setThumbnailUrl( "http::/voxox.com/test/thumbnailUrl" );

			std::string jsonString1 = richData1.toJsonString();

			/**
			 * Description:
			 * 	Update Message in DB with RichData.  Typically, we save message in DB, upload files, then update msg with URLs, before message is sent.
			 * 
			*/
			getDbm()->updateMessageRichData( localTs, richData1, false );

			Message msgOut1 = getDbm()->getMessage( localTs );

			std::string body1 = msgOut1.getBody();	//Should have RichData JSON.

			Assert::AreEqual( jsonString1, body1, L"Image: JSON strings do not match.", LINE_INFO() );

			//Location
			RichData richData2;
			richData2.setType( RichData::Type::LOCATION );
			richData2.setLatitude ( 11.11 );
			richData2.setLongitude( 22.22 );
			richData2.setAddress  ( "My Address" );

			std::string jsonString2 = richData2.toJsonString();

			getDbm()->updateMessageRichData( localTs, richData2, false );

			Message msgOut2 = getDbm()->getMessage( localTs );

			std::string body2 = msgOut2.getBody();	//Should have RichData JSON.

			Assert::AreEqual( jsonString2, body2, L"Location: JSON strings do not match.", LINE_INFO() );
		}
		else
		{
			Assert::Fail( L"Not all test records were added", LINE_INFO() );
		}
	}

	//TEST_METHOD( testUpdateGroupMessageRichData )	
	//{
	//	/**
	//	 * Description:
	//	 * 	Update GM message in DB with RichData.
	//	 * 	We need this separate method because RichData is embedded in body so we cannot simply replace BODY.
	//	 * */
	//	void updateGroupMessageRichData( long timeStamp, const RichData& richData, bool broadcast, const std::string& origBody );
	//}

	//TEST_METHOD( testUpdateTranslatedMessage )		//TODO
	//{
	//	/**
	//	* Update message translation message based on LOCAL timestamp
	//	*/
	//	void updateTranslatedMessage( long timeStamp, const Message& message );
	//}

	//---------------------------------------
	// PUBLIC Contact/PhoneNumber Methods
	//---------------------------------------

	TEST_METHOD( testAddOrUpdateVoxoxContacts )
	{
		size_t contactCount = initPhoneNumbers();	//Contains actual call to addAddressBookContacts()

//		ciList.push_back( ContactImport( 1, "Contact 1", "17605551111", "Work"   ) );
//		ciList.push_back( ContactImport( 1, "Contact 1", "17605552222", "Mobile" ) );
//		ciList.push_back( ContactImport( 2, "Contact 2", "17605553333", "Home"   ) );
//		ciList.push_back( ContactImport( 3, "Contact 3", "17605554444", "Mobile" ) );

		int			userId1		= 111111;
		int			userId2		= 222222;
		std::string userName1	= "jrttest4a";
		std::string userName2	= "jrttest7b";

		VoxoxContactList vcs;
		vcs.push_back( VoxoxContact( "abc1","17605552222", userName1, "17605551111", "Voxox", userId1, false, false, "Test" ) );
		vcs.push_back( VoxoxContact( "abc2", "",           userName2, "17605553333", "Voxox", userId2, false, false, "Test" ) );

		/**
		* Add or updated DB based on VoxoxContact info.
		*/
		getDbm()->addOrUpdateVoxoxContacts( vcs );

		//Retrieve all contacts - TODO: This is not sufficient since it retrieves basic info and a LIST of phoneNumbers.  Not easy to test.
		//	I have manually verified this works, but the test below needs to be tweaked.  Moving on for now.  The main issue is contacts with more than one phone number.
	}

	TEST_METHOD( testAddOrUpdateXmppContacts )		//Includes test for addOrUpdateXmppContact, since first iteratively calls the second.
	{
		//We received a chat from someone NOT in our AB.  
		//	Expectation is that we add a PhoneNumber entry with just the chat info.
		//	This method updates that record.  In unusual circumstance, we may add the record.

		size_t contactCount = initPhoneNumbers();	//Contains actual call to addAddressBookContacts()

		std::string userName    = "jrttestxx";
		std::string phoneNumber = "17605559999";
		VoxoxPhoneNumberList voxoxPhoneNumbers;

		voxoxPhoneNumbers.push_back( VoxoxPhoneNumber( phoneNumber, userName ) );

		/**
		* Update PhoneNumber table with XMPP contact info, based on Voxox PhoneNumber
		*	Simply calls singular version below.
		*/
//		getDbm()->addOrUpdateXmppContacts( voxoxPhoneNumbers );

		ContactList contacts = getDbm()->getContacts( phoneNumber,  PhoneNumber::CONTACT_FILTER_ALL );

		if ( contacts.size() == 1 )
		{
			std::string fullJid = DbUtils::ensureFullJid( userName );	//DBM will ensure full JID.
			Contact contact = contacts.front();

			Assert::AreEqual( phoneNumber, contact.getName(), L"XMPP JIDs do not match.", LINE_INFO() );
		}
		else
		{
			//TODO: add appropriate getContacts() changes/methods
//			Assert::Fail( L"ContactList is wrong size.  Expected 1 entry.", LINE_INFO() );
		}

	}

	//TEST_METHOD( testAddOrUpdateXmppContact )		//Tested above
	//{
	//	/**
	//	 * Description:
	//	 * Add OR update an XMPP contact to the phone number table. an XMPP contact may not have an AB contactKey associated with it. it can happen if we
	//	 *	received a CHAT message from a VoxOx user that is not in our address book. The contact will be added to the phone number table with a unique common group (cm_group).
	//	 */
	//	void addOrUpdateXmppContact( const std::string& xmppAddress, const std::string& phoneNumber );
	//}

	TEST_METHOD( testUpdateXmppPresence )
	{
		size_t contactCount = initPhoneNumbersEx();		//Create contacts/phoneNumbers with Voxox user info

		std::string userName = "jrttest4a";

		XmppContact xc;
		xc.setJid	  ( userName );
		xc.setMsgState( "gone to lunch" );
		xc.setStatus  ( "UNAVAILABLE"   );
		/**
		 * Description:
		 * update the presence and the status of an XMPP contact.
		 * status values may be: UNAVAILABLE, AVAILABLE, AVAILABLE_AWAY ...
		 * presence: a status message of the presence update, or null if there is not a status. The status is free-form text describing a user's presence (i.e., "gone to lunch").
		 * */
		getDbm()->updateXmppPresence( xc );		//TODO: I have verified this works, but we have no method to retrieve the desired XMPP JID contact.  Moving on.

		//std::string contactName = "Contact 1";

		//ContactList contacts = getDbm()->getContacts( contactName,  PhoneNumber::CONTACT_FILTER_ALL );

		//if ( contacts.size() == 1 )
		//{
		//	std::string fullJid = DbUtils::ensureFullJid( userName );	//DBM will ensure full JID.
		//	Contact contact = contacts.front();

		//	Assert::AreEqual( contactName,      contact.getName(),         L"Names do not match.",         LINE_INFO() );
		//	Assert::AreEqual( xc.getStatus(),   contact.getXmppStatus(),   L"XMPP Status do not match.",   LINE_INFO() );
		//	Assert::AreEqual( xc.getMsgState(), contact.getXmppPresence(), L"XMPP Presence do not match.", LINE_INFO() );
		//}
		//else
		//{
		//	Assert::Fail( L"ContactList is wrong size.  Expected 1 entry.", LINE_INFO() );
		//}
	}

	//COMMENTED BECAUSE WE NOW SET FAVORITE BASED ON PHONE NUMBER.
//	TEST_METHOD( testUpdateContactPhoneNumberFavorite )
//	{
//		size_t contactCount = initPhoneNumbersEx();		//Create contacts/phoneNumbers with Voxox user info
//
////		std::string phoneNumber = "17605553333";
//
//		//We need the RecordId of the Contact/PhoneNumber
//		std::string contactName = "Contact 2";
//
//		ContactList contacts = getDbm()->getContacts( contactName,  PhoneNumber::CONTACT_FILTER_ALL );
//
//		if ( contacts.size() == 1 )
//		{
//			Contact contact  = contacts.front();
//			int     recordId = contact.getRecordId();
//			bool    isFavorite = true;	//contact.isFavorite();	//Not at contact level any longer.
//
//			/**
//			 * Description:
//			 * update the new favorite status of a phone number entry.
//			 * */
//			getDbm()->updatePhoneNumberFavorite( recordId,  !isFavorite );
//
//			ContactList contacts2 = getDbm()->getContacts( contactName,  PhoneNumber::CONTACT_FILTER_ALL );
//
//			if ( contacts2.size() == 1 )
//			{
//				Contact contact2 = contacts2.front();
//
////				Assert::AreEqual( !isFavorite, contact2.isFavorite(), L"IsFavorite values do not match.", LINE_INFO() );
//			}
//			else
//			{
//				Assert::Fail( L"ContactList is wrong size.  Expected 1 entry.", LINE_INFO() );
//			}
//		}
//		else
//		{
//			Assert::Fail( L"ContactList is wrong size.  Expected 1 entry.", LINE_INFO() );
//		}
//	}

	TEST_METHOD( testGetContactsWithSearchFilterAndContactFilter )
	{
		//This has already been informally tested in Unit Tests above, but let's do it officially and more completely here.
		//	NOTE: This returns ONLY entries are in AB, so any naked chat contacts will not show here.
		size_t contactCount = initPhoneNumbersEx();		//Create contacts/phoneNumbers with Voxox user info

		//Get all contacts
		ContactList contacts1 = getDbm()->getContacts( "", PhoneNumber::CONTACT_FILTER_ALL );			// <<<<<<< Actual test

		if ( contacts1.size() != mABContactCount )
		{
			Assert::Fail( L"Not all expected contacts were retrieved", LINE_INFO() );
		}

		//Select by search fileter
		//We need the RecordId of the Contact/PhoneNumber
		std::string contactName = "Contact 2";

		/**
		 * Description:
		 * Returns a cursor that is UNION of phone numbers AND GM Groups that match the filter.
		 *
		 * As we group by the ContactKey (we want the contact), we may lose the VoxOx flag of one of the phone numbers
		 * so we added a little trick in the query to count if any VoxOx numbers and added a field to the table called VOXOX_COUNT.
		 *
		 * Used by the CONTACTS tab.
		 * */
		ContactList contacts2 = getDbm()->getContacts( contactName, PhoneNumber::CONTACT_FILTER_ALL );	// <<<<<<< Actual test

		if ( contacts2.size() != 1 )
		{
			Assert::Fail( L"Incorrect contact count for Search Filter test.", LINE_INFO() );
		}

		//Now set a FAVORITE and test the ContactFilter

		//COMMENTED BECAUSE WE NOW SET FAVORITE BASED ON PHONE NUMBER.

		//Contact contact  = contacts2.front();
		//int     recordId = contact.getRecordId();

		//getDbm()->updatePhoneNumberFavorite( recordId, true );

		//ContactList contacts3 = getDbm()->getContacts( "", PhoneNumber::CONTACT_FILTER_FAVORITE );		// <<<<<<< Actual test

		//if ( contacts3.size() != 1 )
		//{
		//	Assert::Fail( L"Incorrect contact count for Contact Filter test.", LINE_INFO() );
		//}
	}

	TEST_METHOD( testGetContactDetailsPhoneNumbersByCmGroup )
	{
		size_t contactCount = initPhoneNumbersEx();		//Create contacts/phoneNumbers with Voxox user info

		int cmGroup = 1;

		/**
		 * Description:
		 * TODO currently may return duplicated numbers
		 * */
		PhoneNumberList contacts1 = getDbm()->getContactDetailsPhoneNumbersByCmGroup( cmGroup );

		if ( contacts1.size() != 2 )
		{
			Assert::Fail( L"Incorrect contact count for test1.", LINE_INFO() );
		}

		PhoneNumberList contacts2 = getDbm()->getContactDetailsPhoneNumbersByCmGroup( 2 );

		if ( contacts2.size() != 1 )
		{
			Assert::Fail( L"Incorrect contact count for test2.", LINE_INFO() );
		}
	}

	TEST_METHOD( testGetContactPhoneNumbersListForCmGroup )	//TODO-UNITTEST: need number on multiple contacts
	{
		size_t contactCount = initPhoneNumbersEx();		//Create contacts/phoneNumbers with Voxox user info

		/**
		 * Description:
		 * returns a list of PhoneNumber objects associated with the CmGroup.
		 * */
		PhoneNumberList pnList1 = getDbm()->getContactPhoneNumbersListForCmGroup( 1, false );	//CmGroup, isVoxox
		if ( pnList1.size() != 2 )	//Expect Mobile and Voxox
		{
			Assert::Fail( L"Incorrect contact count for test1.", LINE_INFO() );
		}

		PhoneNumberList pnList2 = getDbm()->getContactPhoneNumbersListForCmGroup( 1, true );
		if ( pnList2.size() != 1 )	//Expect Just Voxox
		{
			Assert::Fail( L"Incorrect contact count for test1.", LINE_INFO() );
		}

		PhoneNumberList pnList3 = getDbm()->getContactPhoneNumbersListForCmGroup( 2, false );
		if ( pnList3.size() != 1 )	//Expect Just Voxox (only number on CmGroup)
		{
			Assert::Fail( L"Incorrect contact count for test3.", LINE_INFO() );
		}

		PhoneNumberList pnList4 = getDbm()->getContactPhoneNumbersListForCmGroup( 2, true  );
		if ( pnList4.size() != 1 )	//Expect Just Voxox (only number on CmGroup)
		{
			Assert::Fail( L"Incorrect contact count for test4.", LINE_INFO() );
		}
	}

	TEST_METHOD( testGetPhoneNumber )
	{
		size_t contactCount = initPhoneNumbersEx();		//Create contacts/phoneNumbers with Voxox user info

		std::string pnString = "17605551111";
		int			cmGroup  = 1;

		/**
		 * Description: Get phoneNumber object for given phoneNumbe string and/or CMGroup
		 * Used in UMW and notification
		 * */
		PhoneNumber pn1 = getDbm()->getPhoneNumber( pnString, cmGroup );	//TODO-UNITTEST - PN looks OK, but missing compound name.
		Assert::IsTrue( pn1.isValid(), L"Test1 should return VALID PhoneNumber object.", LINE_INFO() );

		PhoneNumber pn2 = getDbm()->getPhoneNumber( pnString, 2 );	//Fake number in more than one Contact (Does not fake properly, or compound name not working.
		Assert::IsTrue( pn2.isValid(), L"Test2 should return VALID PhoneNumber object.", LINE_INFO() );

		PhoneNumber pn3 = getDbm()->getPhoneNumber( std::string(), cmGroup );
		Assert::IsFalse( pn3.isValid(), L"Test3 should return INVALID PhoneNumber object.", LINE_INFO() );

		PhoneNumber pn4 = getDbm()->getPhoneNumber( std::string(), 0 );		//This should fail.
		Assert::IsFalse( pn4.isValid(), L"Test4 should return INVALID PhoneNumber object.", LINE_INFO() );
	}

	TEST_METHOD( testGetVoxoxUserIdForPhoneNumber )
	{
		size_t contactCount = initPhoneNumbersEx();		//Create contacts/phoneNumbers with Voxox user info

		std::string pnString       = "17605551111";
		int			expectedUserId = 111111;

		/**
		* Get Voxox userId for given phoneNumber string.
		*/
		int userId1 = getDbm()->getVoxoxUserId( pnString );
		Assert::AreEqual( expectedUserId, userId1, L"UserId is not as expected.", LINE_INFO() );

		int userId2 = getDbm()->getVoxoxUserId( "777777777" );		//Fail
		Assert::AreEqual( 0, userId2, L"Test2: UserId is not zero as expected for non-existent PN.", LINE_INFO() );
	}

	
	TEST_METHOD( testGetDidByJid )
	{
		size_t contactCount = initPhoneNumbersEx();		//Create contacts/phoneNumbers with Voxox user info

		std::string expectedUserDid = "17605551111";
		std::string jid				= "jrttest4a";

		/**
		* Get DID for given JID
		*/
		std::string did1 = getDbm()->getDidByJid( jid );
		Assert::AreEqual( expectedUserDid, did1, L"DID is not as expected.", LINE_INFO() );

		std::string did2 = getDbm()->getDidByJid( "xxxx" );		//Expected to fail
		Assert::AreEqual( std::string(), did2, L"DID is not as expected.", LINE_INFO() );
	}

	TEST_METHOD( testGetVoxoxUserIdForCmGroup )
	{
		size_t contactCount = initPhoneNumbersEx();		//Create contacts/phoneNumbers with Voxox user info

		int expectedUserId = 111111;

		/**
		* Get Voxox userId for given CMGroup.	//TODO: I don't think this logic holds since a CMGroup may have more than one VoxoxID (one contact with multiple Voxox numbers).
		*/
		int userId1 = getDbm()->getVoxoxUserId( 1 );	//CmGroup
		Assert::AreEqual( expectedUserId, userId1, L"UserId is not as expected.", LINE_INFO() );

		int userId2 = getDbm()->getVoxoxUserId( 99 );	//CmGroup is invalid
		Assert::AreEqual( 0, userId2, L"Test2: UserId is not zero as expected for non-existent PN.", LINE_INFO() );
	}

	TEST_METHOD( testGetCmGroupForPhoneNumber )
	{
		size_t contactCount = initPhoneNumbersEx();		//Create contacts/phoneNumbers with Voxox user info

		std::string pnString  = "17605551111";
		int expectedCmGroup   = 1;

		/**
		* Get CmGroup for given phoneNumber string.
		*/
		int cmGroup1 = getDbm()->getCmGroup( pnString );
		Assert::AreEqual( expectedCmGroup, cmGroup1, L"CmGroup is not as expected.", LINE_INFO() );

		int cmGroup2 = getDbm()->getCmGroup( "77777777" );	//Should fail
		Assert::AreEqual( 0, cmGroup2, L"CmGroup should be zero.", LINE_INFO() );
	}

	TEST_METHOD( testIsXmppContactInLocalAddressBook )
	{
		size_t contactCount = initPhoneNumbersEx();		//Create contacts/phoneNumbers with Voxox user info

		std::string jid = "jrttest4a";

		/**
		* Determine if given XMPP JID is in our DB
		*/
		bool result1 = getDbm()->isXmppContactInLocalAddressBook( jid );
		Assert::IsTrue( result1, L"JID is not in DB when it should be.", LINE_INFO() );

		bool result2 = getDbm()->isXmppContactInLocalAddressBook( "xxxxx" );
		Assert::IsFalse( result2, L"JID IS in DB when it should NOT be.", LINE_INFO() );
	}

	TEST_METHOD( testGetNewInboundMessageCountForCmGroup )
	{
		size_t contactCount = initPhoneNumbersEx();		//Create contacts/phoneNumbers with Voxox user info
		
		clearMessageTable();

		Message msg1 = createTestMessage( Message::SMS,  Message::INBOUND, Message::NEW, "testGetNewInboundMessageCountForCmGroup 1" );
		Message msg2 = createTestMessage( Message::SMS,  Message::INBOUND, Message::NEW, "testGetNewInboundMessageCountForCmGroup 2" );
		Message msg3 = createTestMessage( Message::SMS,  Message::INBOUND, Message::READ, "testGetNewInboundMessageCountForCmGroup 3" );

		int newCount = 2;

		bool added1 = getDbm()->addMessage( msg1, false );
		bool added2 = getDbm()->addMessage( msg2, false );
		bool added3 = getDbm()->addMessage( msg3, false );

		if ( added1 && added2 && added3 )
		{
			/**
			 * returns number of new in-bound messages for a CM Group
			 * This is called in for each Contact in ContactsMessages (or its array/cursor adapter).  So could be called a lot, especially when scrolling.
			 */
			int count1 = getDbm()->getNewInboundMessageCountForCmGroup( 1 );	//CmGroup
			Assert::AreEqual( newCount, count1, L"New message Count is not correct.", LINE_INFO() );

			int count2 = getDbm()->getNewInboundMessageCountForCmGroup( 99 );	//Should fail with zero
			Assert::AreEqual( 0, count2, L"New message Count is not correct.", LINE_INFO() );
		}
		else
		{
			Assert::Fail( L"Not all messages added for test.", LINE_INFO() );
		}
	}

	TEST_METHOD( testGetNewInboundMessageCount )
	{
		size_t contactCount = initPhoneNumbersEx();		//Create contacts/phoneNumbers with Voxox user info
		
		clearMessageTable();

		Message msg1 = createTestMessage( Message::SMS,  Message::INBOUND, Message::NEW, "testGetNewInboundMessageCountForCmGroup 1" );
		Message msg2 = createTestMessage( Message::SMS,  Message::INBOUND, Message::NEW, "testGetNewInboundMessageCountForCmGroup 2" );
		Message msg3 = createTestMessage( Message::SMS,  Message::INBOUND, Message::READ, "testGetNewInboundMessageCountForCmGroup 3" );

		Message msg4 = createTestMessage( Message::CHAT,  Message::INBOUND, Message::NEW, "testGetNewInboundMessageCountForCmGroup 1" );
		Message msg5 = createTestMessage( Message::CHAT,  Message::INBOUND, Message::NEW, "testGetNewInboundMessageCountForCmGroup 2" );
		Message msg6 = createTestMessage( Message::CHAT,  Message::INBOUND, Message::READ, "testGetNewInboundMessageCountForCmGroup 3" );

		int newSmsCount  = 2;
		int newChatCount = 2;
		int newTotalCount = newSmsCount + newChatCount;

		bool added1 = getDbm()->addMessage( msg1, false );
		bool added2 = getDbm()->addMessage( msg2, false );
		bool added3 = getDbm()->addMessage( msg3, false );

		bool added4 = getDbm()->addMessage( msg4, false );
		bool added5 = getDbm()->addMessage( msg5, false );
		bool added6 = getDbm()->addMessage( msg6, false );

		if ( added1 && added2 && added3 && added4 && added5 && added6 )
		{
			std::string phoneNumber = "17605551111";
			std::string jid			= "jrttest4a";
			/**
			 * returns number of new in-bound messages for a PhoneNumber string or JID.
			 *
			 * (I am not sure but...)This is called in for each Contact in ContactsMessages (or its array/cursor adapter).  So could be called a lot, especially when scrolling.
			 */
			int count1 = getDbm()->getNewInboundMessageCount( phoneNumber, jid );					//Both PhoneNumber and JID
			Assert::AreEqual( newTotalCount, count1, L"New message Count is not correct.", LINE_INFO() );

			int count2 = getDbm()->getNewInboundMessageCount( phoneNumber, "" );					//Just PhoneNumber
			Assert::AreEqual( newSmsCount, count2, L"New message Count is not correct.", LINE_INFO() );
			
			int count3 = getDbm()->getNewInboundMessageCount( "", jid );							//Just JID
			Assert::AreEqual( newChatCount, count3, L"New message Count is not correct.", LINE_INFO() );

			int count4 = getDbm()->getNewInboundMessageCount( "7777777", "xxx" );					//Values not in DB
			Assert::AreEqual( 0, count4, L"New message Count is not correct.", LINE_INFO() );

			int count5 = getDbm()->getNewInboundMessageCount( "", "" );								//Both values invalid/empty
			Assert::AreEqual( 0, count5, L"New message Count is not correct.", LINE_INFO() );
		}
		else
		{
			Assert::Fail( L"Not all messages added for test.", LINE_INFO() );
		}
	}

	TEST_METHOD( testGetContactsMessageSummary )
	{
		size_t contactCount = initPhoneNumbersEx();		//Create contacts/phoneNumbers with Voxox user info
		
		clearMessageTable();

		Message msg1 = createTestMessage( Message::SMS,  Message::INBOUND, Message::NEW, "testGetContactsMessageSummary 1" );
		Message msg2 = createTestMessage( Message::SMS,  Message::INBOUND, Message::NEW, "testGetContactsMessageSummary 2" );
		Message msg3 = createTestMessage( Message::SMS,  Message::INBOUND, Message::READ, "testGetContactsMessageSummary 3" );

//		Message msg4 = createTestMessage( Message::CHAT,  Message::INBOUND, Message::NEW, "testGetContactsMessageSummary 4" );
//		Message msg5 = createTestMessage( Message::CHAT,  Message::INBOUND, Message::NEW, "testGetContactsMessageSummary 5" );
//		Message msg6 = createTestMessage( Message::CHAT,  Message::INBOUND, Message::READ, "testGetContactsMessageSummary 6" );

		int newSmsCount   = 2;
		int newChatCount  = 2;
		int newTotalCount = newSmsCount + newChatCount;

		bool added1 = getDbm()->addMessage( msg1, false );
		bool added2 = getDbm()->addMessage( msg2, false );
		bool added3 = getDbm()->addMessage( msg3, false );

//		bool added4 = getDbm()->addMessage( msg4, false );
//		bool added5 = getDbm()->addMessage( msg5, false );
//		bool added6 = getDbm()->addMessage( msg6, false );

		if ( added1 && added2 && added3 /*&& added4 && added5 && added6*/ )
		{
			/**
			* Return list of ContactMessage objects.
			* Used on main Messages window.
			*/
			ContactMessageSummaryList summaryList = getDbm()->getContactsMessageSummary();

			int xxx = 1;
		}
	}

	//TEST_METHOD( testGetNewInboundMessageCountForGroupId )		//TODO-PHASE2
	//{
	//	/**
	//	 * returns number of new in-bound messages for a GM GroupId
	//	 *
	//	 * This is called in for each Contact in ContactsMessages (or its array/cursor adapter).  So could be called a lot, especially when scrolling.
	//	 */
	//	int getNewInboundMessageCountForGroupId( const std::string& groupId );
	//}

	//TEST_METHOD( testGetTranslationModeProps )		//TODO-UNITTEST
	//{
	//	MessageTranslationProperties getTranslationModeProps( const std::string& phoneNumber, const std::string& jid );
	//}

	//TEST_METHOD( testAddContactMessageTranslation )	//TODO-UNITTEST
	//{
	//	/**
	//	 * Add translation mode to a contact. What required is wither the phone number of jid.
	//	 * CONTACT_ID should -NOT- be used for detecting a contact as it may change when user refresh the contacts list.
	//	 * returns true for success.
	//	 * */
	//	bool addContactMessageTranslation( const MessageTranslationProperties& mt );
	//}

	//----------------------------------------------------------------------------
	//PUBLIC RecentCall tests
	//----------------------------------------------------------------------------

	TEST_METHOD( testAddRecentCall )
	{
		size_t contactCount = initPhoneNumbersEx();

		std::string			origNumber		= "+1(760)555-1111";
		bool				incoming		= true;
		RecentCall::Status	expectedStatus	= RecentCall::STATUS_NEW;
		__int64				startTime		= 0;	//TODO

		/**
		* Add new call to DB. Return DB recordId.
		*	This will be called when an incoming call event is detected or 
		*	user makes an outgoing call.
		*/
		int callId = getDbm()->addRecentCall( origNumber, startTime, incoming );

		if ( callId > 0 )
		{
			RecentCall rc = getDbm()->getRecentCall( callId );
			std::string number = DbUtils::getNormalizedNumberNoPrefix( origNumber );

			Assert::IsTrue( rc.getStatus()     == expectedStatus,	L"Status mismatch.",        LINE_INFO() );
			Assert::IsTrue( rc.getRecordId()   == callId,			L"CallId mismatch.",        LINE_INFO() );
			Assert::IsTrue( rc.isIncoming()    == incoming,			L"Incoming flag mismatch.", LINE_INFO() );
			Assert::IsTrue( rc.getOrigNumber() == origNumber,		L"OrigNumber mismatch.",    LINE_INFO() );
			Assert::IsTrue( rc.getNumber()     == number,			L"Number mismatch.",        LINE_INFO() );
		}
		else
		{
			Assert::Fail( L"Unable to add test call.", LINE_INFO() );
		}
	}

	TEST_METHOD( testUpdateRecentCallAnswered )	
	{
		size_t contactCount = initPhoneNumbersEx();

		std::string			origNumber		= "+1(760)555-1111";
		bool				incoming		= true;
		RecentCall::Status	expectedStatus	= RecentCall::STATUS_ANSWERED;
		__int64				startTime		= 0;	//TODO

		int callId = getDbm()->addRecentCall( origNumber, startTime, incoming );

		if ( callId > 0 )
		{
			Sleep( 2 );		//To determine that startTimestamp differs from timestamp
			/**
			* Update call status on Answer, Decline, or Hangup (even for unanswered call)
			*/
			getDbm()->updateRecentCallAnswered( callId );

			RecentCall rc = getDbm()->getRecentCall( callId );
			std::string number = DbUtils::getNormalizedNumberNoPrefix( origNumber );

			Assert::IsTrue( rc.getStatus()     == expectedStatus,	L"Status mismatch.",        LINE_INFO() );
			Assert::IsTrue( rc.getRecordId()   == callId,			L"CallId mismatch.",        LINE_INFO() );
			Assert::IsTrue( rc.isIncoming()    == incoming,			L"Incoming flag mismatch.", LINE_INFO() );
			Assert::IsTrue( rc.getOrigNumber() == origNumber,		L"OrigNumber mismatch.",    LINE_INFO() );
			Assert::IsTrue( rc.getNumber()     == number,			L"Number mismatch.",        LINE_INFO() );

			Assert::IsTrue( rc.getStartTimestamp() > rc.getTimestamp(), L"Timestamp mismatch.",  LINE_INFO() );	//TODO:Need better test
		}
		else
		{
			Assert::Fail( L"Unable to add test call.", LINE_INFO() );
		}
	}

	TEST_METHOD( testUpdateRecentCallDeclined )
	{
		size_t contactCount = initPhoneNumbersEx();

		std::string			origNumber		= "+1(760)555-1111";
		bool				incoming		= true;
		RecentCall::Status	expectedStatus	= RecentCall::STATUS_DECLINED;
		__int64				startTime		= 0;	//TODO

		int callId = getDbm()->addRecentCall( origNumber, startTime, incoming );

		if ( callId > 0 )
		{
			Sleep( 2 );		//To determine that startTimestamp differs from timestamp
			/**
			* Update call status on Answer, Decline, or Hangup (even for unanswered call)
			*/
			getDbm()->updateRecentCallDeclined( callId );

			RecentCall rc = getDbm()->getRecentCall( callId );
			std::string number = DbUtils::getNormalizedNumberNoPrefix( origNumber );

			Assert::IsTrue( rc.getStatus()			== expectedStatus,	L"Status mismatch.",         LINE_INFO() );
			Assert::IsTrue( rc.getRecordId()		== callId,			L"CallId mismatch.",         LINE_INFO() );
			Assert::IsTrue( rc.isIncoming()			== incoming,		L"Incoming flag mismatch.",  LINE_INFO() );
			Assert::IsTrue( rc.getOrigNumber()		== origNumber,		L"OrigNumber mismatch.",     LINE_INFO() );
			Assert::IsTrue( rc.getNumber()			== number,			L"Number mismatch.",         LINE_INFO() );
			Assert::IsTrue( rc.getStartTimestamp()	== 0,				L"StartTimestamp mismatch.", LINE_INFO() );
		}
		else
		{
			Assert::Fail( L"Unable to add test call.", LINE_INFO() );
		}
	}

	TEST_METHOD( testUpdateRecentCallAnsweredAndTerminated )
	{
		size_t contactCount = initPhoneNumbersEx();

		std::string			origNumber		= "+1(760)555-1111";
		bool				incoming		= true;
		RecentCall::Status	expectedStatus	= RecentCall::STATUS_ANSWERED;
		int					expectedDur     = 2;
		__int64				startTime		= 0;	//TODO

		int callId = getDbm()->addRecentCall( origNumber, startTime, incoming );

		if ( callId > 0 )
		{
			Sleep( 2 );		//To determine that startTimestamp differs from timestamp

			getDbm()->updateRecentCallAnswered( callId );

			Sleep( expectedDur * 1000 );		//So we have some Duration
			
			getDbm()->updateRecentCallTerminated( callId );

			RecentCall rc = getDbm()->getRecentCall( callId );
			std::string number = DbUtils::getNormalizedNumberNoPrefix( origNumber );

			Assert::IsTrue( rc.getStatus()			== expectedStatus,	L"Status mismatch.",        LINE_INFO() );
			Assert::IsTrue( rc.getRecordId()		== callId,			L"CallId mismatch.",        LINE_INFO() );
			Assert::IsTrue( rc.isIncoming()			== incoming,		L"Incoming flag mismatch.", LINE_INFO() );
			Assert::IsTrue( rc.getOrigNumber()		== origNumber,		L"OrigNumber mismatch.",    LINE_INFO() );
			Assert::IsTrue( rc.getNumber()			== number,			L"Number mismatch.",        LINE_INFO() );

			Assert::IsTrue( rc.getStartTimestamp() > rc.getTimestamp(), L"Timestamp mismatch.",		LINE_INFO() );	//TODO:Need better test

			Assert::IsTrue( rc.getDuration() == expectedDur,			L"Timestamp mismatch.",		LINE_INFO() );	//Sufficient?
		}
		else
		{
			Assert::Fail( L"Unable to add test call.", LINE_INFO() );
		}
	}

	TEST_METHOD( testUpdateRecentCallMissedAndTerminated )
	{
		size_t contactCount = initPhoneNumbersEx();

		std::string			origNumber		= "+1(760)555-1111";
		bool				incoming		= true;
		RecentCall::Status	expectedStatus	= RecentCall::STATUS_MISSED;
		int					expectedDur     = 0;
		__int64				startTime		= 0;	//TODO

		int callId = getDbm()->addRecentCall( origNumber, startTime, incoming );

		if ( callId > 0 )
		{
			Sleep( 2 );		//To determine that startTimestamp differs from timestamp

//			getDbm()->updateRecentCallAnswered( callId );	//User just let's it ring until caller hangs up.

			Sleep( 2000 );		//So we have some time difference.
			
			getDbm()->updateRecentCallTerminated( callId );

			RecentCall rc = getDbm()->getRecentCall( callId );
			std::string number = DbUtils::getNormalizedNumberNoPrefix( origNumber );

			Assert::IsTrue( rc.getStatus()			== expectedStatus,	L"Status mismatch.",         LINE_INFO() );
			Assert::IsTrue( rc.getRecordId()		== callId,			L"CallId mismatch.",         LINE_INFO() );
			Assert::IsTrue( rc.isIncoming()			== incoming,		L"Incoming flag mismatch.",  LINE_INFO() );
			Assert::IsTrue( rc.getOrigNumber()		== origNumber,		L"OrigNumber mismatch.",     LINE_INFO() );
			Assert::IsTrue( rc.getNumber()			== number,			L"Number mismatch.",         LINE_INFO() );
			Assert::IsTrue( rc.getStartTimestamp()  == 0,				L"StartTimestamp mismatch.", LINE_INFO() );
			Assert::IsTrue( rc.getDuration()		== expectedDur,		L"Duration.",				 LINE_INFO() );
		}
		else
		{
			Assert::Fail( L"Unable to add test call.", LINE_INFO() );
		}
	}

	TEST_METHOD( testDeleteRecentCall )
	{
		size_t contactCount = initPhoneNumbersEx();

		std::string			origNumber		= "+1(760)555-1111";
		bool				incoming		= true;
		RecentCall::Status	expectedStatus	= RecentCall::STATUS_ANSWERED;
		int					expectedDur     = 2;
		__int64				startTime		= 0;	//TODO

		int callId = getDbm()->addRecentCall( origNumber, startTime, incoming );

		if ( callId > 0 )
		{
			Sleep( 2 );		//To determine that startTimestamp differs from timestamp

			getDbm()->updateRecentCallAnswered( callId );

			Sleep( expectedDur * 1000 );		//So we have some Duration
			
			getDbm()->updateRecentCallTerminated( callId );

			/**
			* Delete call - TODO: this may have to be 2-phase, like deleting messages is. Depends on sending call history to server.
			*/
			getDbm()->deleteRecentCall( callId );

			RecentCall rc = getDbm()->getRecentCall( callId );

			Assert::IsFalse( rc.isValid(),	L"RecentCall should be INVALID.", LINE_INFO() );
		}
		else
		{
			Assert::Fail( L"Unable to add test call.", LINE_INFO() );
		}
	}

	/**
	* Retrieve a single recent call by callId
	*/
	//NOTE: GetRecentCall() is already tested in other unit tests.
	//TEST_METHOD( testGetRecentCall )
	//{
	//	DLLEXPORT RecentCall getRecentCall( int callId );
	//}

	TEST_METHOD( testGetRecentCallsByPhoneNumber )
	{
		size_t contactCount = initPhoneNumbersEx();

		std::string	origNumber = "+1(760)555-1111";
		std::string phoneNumber = DbUtils::getNormalizedNumberNoPrefix( origNumber );
		/**
		* Retrieve a list of recent calls by phoneNumber
		*/
		RecentCallList calls = getDbm()->getRecentCalls( origNumber );

		if ( calls.size() > 0 )
		{
			for each( RecentCall call in calls )
			{
				//TODO: improve test.  Manually verified this works.
				Assert::AreEqual( call.getNumber(),     phoneNumber, L"Number mismatch.",     LINE_INFO() );
				Assert::AreEqual( call.getOrigNumber(), origNumber,  L"OrigNumber mismatch.", LINE_INFO() );
			}
		}
	}

	TEST_METHOD( testGetRecentCallsByCmGroup )
	{
		size_t contactCount = initPhoneNumbersEx();

		int	cmGroup = 1;

		/**
		* Retrieve a list of recent calls by cmGroup
		*/
		RecentCallList calls = getDbm()->getRecentCalls( cmGroup );

		if ( calls.size() > 0 )
		{
			for each( RecentCall call in calls )
			{
				//TODO: improve test.  Manually verified this works.
				Assert::AreEqual( call.getCmGroup(), cmGroup, L"CmGroup mismatch.", LINE_INFO() );
			}
		}
	}

	};
}	//namespace DbmUnitTest1