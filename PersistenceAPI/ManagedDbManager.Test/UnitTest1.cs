﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using ManagedDbManager;
using ManagedDataTypes;

namespace ManagedDbManager.Test
{

public class TestDbChange
{ 
	public TestDbChange( String name ) 
	{
		mName = name;
	}

	public void HandleDbChange( Object sender, DbChangedEventArgs e )	//Same signature as DbChangeEventHandler
	{
		Console.WriteLine( "In HandleDbChange() for instance name: " + mName );

        if ( e.Data.isContactTable() )
        {
    		Console.WriteLine( "  Table: contact" );
        }
        else if ( e.Data.isMsgTable() )
        {
    		Console.WriteLine( "  Table: message" );
        }
        else if ( e.Data.isGmTable() )
        {
    		Console.WriteLine( "  Table: GM (group or member)" );   //TODO: GM is in Phase 2.
        }
        else
        {
    		Console.WriteLine( "  Table: UNKNOWN" );
        }
	}

    public String getName()
    {
        return mName;
    }

    private String mName;
};

[TestClass]
public class ManagedDbManagerTest
{
	String  		    mDbName = "D:\\Temp\\test.db";
	ManagedDbManager    mDbm    = null;

	String		        mTestToGroupId  = "GMG-ID-12345";
	Int32		        mTestFromUserId = 123456;

	Int32		        mABContactCount        = 0;
	Int32		        mTotalPhoneNumberCount = 0;

	ManagedDbManager getDbm()
	{
		return mDbm;
	}

	void openDb()
	{
		if ( mDbm == null )
		{
			mDbm = new ManagedDbManager( mDbName, ManagedDbManager.DbType.User );
			mDbm.create();

            addDbEventHandlers();
		}
	}

	void closeDb()
	{
		if ( mDbm != null )
		{
			mDbm.close();
		}
	}

    void addDbEventHandlers()
    {
	    TestDbChange test1 = new TestDbChange( "Jeff" );
	    TestDbChange test2 = new TestDbChange( "Pam" );

	    DbChangedEventHandler dbDel1 = (DbChangedEventHandler)System.Delegate.CreateDelegate( typeof(DbChangedEventHandler), test1, "HandleDbChange" );
	    DbChangedEventHandler dbDel2 = (DbChangedEventHandler)System.Delegate.CreateDelegate( typeof(DbChangedEventHandler), test2, "HandleDbChange" );

	    getDbm().addDbChangeListener( dbDel1 );     //Test with multiple listeners to demonstrate it works.
	    getDbm().addDbChangeListener( dbDel2 );
    }

	void clearMessageTable()
	{
		getDbm().clearMessageTable();
	}

	void clearPhoneNumberTable()
	{
		getDbm().clearPhoneNumberTable();
	}

	Message createTestMessage( Message.MsgType type, Message.MsgDirection dir, Message.MsgStatus status, String text )
	{
		System.Threading.Thread.Sleep( 2 );	//So we get different timestamps

//		Int64 localTs = DbUtils::getSystemTimeMillis();
        //100-nanosecond intervals, so divide by 10,000 to get millis
		Int64 localTs = System.DateTime.Now.ToFileTime() / 10000;
		String localTsString = localTs.ToString();
	
		Message msg = new Message();

		msg.Type      = type;
		msg.Direction = dir;
		msg.Status    = status;

		msg.MsgId	  = "UUID-" + localTsString;

		msg.Body	  = "Test Body " + text;

		if ( msg.isSms() )
		{
			if ( Message.dirIsInbound( dir ) )
			{
				msg.FromDid = "17605551111";
				msg.ToDid   = "17602775395";
			}
			else
			{
				msg.FromDid = "17602775395";
				msg.ToDid   = "17605551111";
			}
		}
		else if ( msg.isChat() )
		{
			if ( Message.dirIsInbound( dir ) )
			{
				msg.FromJid    = "jrttest4a";
				msg.ToJid	   = "jrttest7b";
				msg.ContactDid = "17605551111";	//Not sure this will work.
			}
			else
			{
				msg.FromJid    = "jrttest7b";
				msg.ToJid	   = "jrttest4a";
				msg.ContactDid = "17602775395";	//Not sure this will work.
			}
		}
		else if ( msg.isGroupMessage() )
		{
			msg.ToGroupId  = mTestToGroupId;
			msg.FromUserId = mTestFromUserId;
		}
		else if ( msg.isVoicemail() )
		{
			msg.CName    = "CName " + text;
			msg.IAccount = "IAcct " + text;
		}
		else
		{
			int xxx = 1;
            xxx++;
		}

		msg.LocalTimestamp  = localTs;
		msg.ServerTimestamp = new VxTimestamp( localTs );

		return msg;
	}

	//Initialize phoneNumber table for Contact/PhoneNumber based Unit Tests
	Int64 initPhoneNumbers()
	{
		openDb();
		clearPhoneNumberTable();

		ContactImportList ciList = new ContactImportList();
		
		ciList.Add( new ContactImport( "1", "Contact 1", "17605551111", "Work",   "", false ) );
		ciList.Add( new ContactImport( "1", "Contact 1", "17605552222", "Mobile", "", false ) );
		ciList.Add( new ContactImport( "2", "Contact 2", "17605553333", "Home",   "", false ) );
		ciList.Add( new ContactImport( "3", "Contact 3", "17605554444", "Mobile", "", false ) );

		getDbm().addAddressBookContacts( ciList );
		
		mABContactCount		   = 3;
		mTotalPhoneNumberCount = 4;

		return ciList.Count;
	}

	//Initialize phoneNumber table for Contact/PhoneNumber based Unit Tests which need XMPP data.
	Int64 initPhoneNumbersEx()
	{
		openDb();
		clearPhoneNumberTable();

		ContactImportList ciList = new ContactImportList();
		
		ciList.Add( new ContactImport( "1", "Contact 1", "17605551111", "Work"  , "", false ) );
		ciList.Add( new ContactImport( "1", "Contact 1", "17605552222", "Mobile", "", false ) );
		ciList.Add( new ContactImport( "2", "Contact 2", "17605553333", "Home"  , "", false ) );
		ciList.Add( new ContactImport( "3", "Contact 3", "17605554444", "Mobile", "", false ) );

		getDbm().addAddressBookContacts( ciList );

		//Add some Voxox user info
		int		userId1		= 111111;
		int	    userId2		= 222222;
		String  userName1	= "jrttest4a";
		String  userName2	= "jrttest7b";

		VoxoxContactList vcs = new VoxoxContactList();
		vcs.Add( new VoxoxContact( "abc", "17605552222", userName1, "17605551111", userId1, "Voxox", false, false, "TestSrc" ) );
		vcs.Add( new VoxoxContact( "abd", "",            userName2, "17605553333", userId2, "Voxox", false, false, "TestSrc" ) );

		/**
		* Add or updated DB based on VoxoxContact info.
		*/
		getDbm().addOrUpdateVoxoxContacts( vcs );

		mABContactCount			= 3;
		mTotalPhoneNumberCount	= 4;

		return ciList.Count;
	}


	//-------------------------------------------------
	// DB Admin tests
	//-------------------------------------------------


	//Exists
	//Create
    [TestMethod]
    public void testDbCreate ()
	{
		bool exists = false;
		//String dbName = mDbName;
		//remove( dbName.c_str() );

		//exists = getDbm().checkDatabase( dbName.c_str() );

		//std::wstring msg = "DB ";
		//msg.append( toWide( dbName ) );
		//msg.append( " exists when it should not." );

		//Assert.IsFalse( exists, msg.c_str() );

		//DB should be created on first call to DBM.getInstance
		mDbm = new ManagedDbManager( mDbName, ManagedDbManager.DbType.User );
		mDbm.create();

		//Verify DB now exists.
		exists = getDbm().checkDatabase();

		Assert.IsTrue( exists, "DB does NOT exists when it should." );

		//Verify the tables exist.  
		//TODO: Ideally, we would get a list of tables names from the Database class.
		StringList tables = new StringList();
//		tables.Add( DatabaseTables::PhoneNumber::TABLE		);
//		tables.Add( DatabaseTables::Message::TABLE			);
//		tables.Add( DatabaseTables::GroupMessageGroup::TABLE	);
//		tables.Add( DatabaseTables::GroupMessageMember::TABLE );
//		tables.Add( DatabaseTables::Translation::TABLE		);
//		tables.Add( DatabaseTables::RecentCall::TABLE			);
		tables.Add( "phone_number"  		);      //We do NOT have access to the DatabaseTables.h file.  Should we?
		tables.Add( "message"   			);
		tables.Add( "group_message_group"   );
		tables.Add( "group_message_member"  );
		tables.Add( "translation"		    );
		tables.Add( "recent_call"		    );

		foreach ( String tableName in tables )
		{
			exists = getDbm().checkTableExists( tableName );
			Assert.IsTrue( exists, "Tables does NOT exist" );
		}
	}


	//Open - readonly, readwrite
	//Close
	//Remove



	//-------------------------------------------------
	// Message tests
	//-------------------------------------------------

    [TestMethod]
    public void testAddMessage()		//Also tests getMessage( timestamp )
	{
		openDb();
		clearMessageTable();
	
		Message msgIn = createTestMessage( Message.MsgType.SMS,  Message.MsgDirection.INBOUND, Message.MsgStatus.NEW, "testAddMsg" );
		Int64 localTs = msgIn.LocalTimestamp;

		bool added = getDbm().addMessage( msgIn, false );

		if ( added )
		{
			Message msgOut = getDbm().getMessage( localTs );

			bool match = (msgOut.LocalTimestamp == localTs);

			Assert.IsTrue( match, "Local timestamps do not match" );
		}
		else
		{
			Assert.Fail( "Message was not added" );
		}

		//Test latest time stamp - Assert does not seem to support Int64 for AreEqual, so convert to string.
		Int64 latestTs = getDbm().getLatestMessageServerTimestamp( Message.MsgType.SMS, Message.MsgDirection.INBOUND, Message.MsgStatus.NEW );

		Assert.AreEqual( localTs, latestTs, "Latest timestamp does not match" );
	}

	//TEST_METHOD( testGetLatestMessageServerTimestamp )	//Tested in testAddMessage()
	//{
	//	openDb();
	//	/**
	//	 * returns the last inserted server_timestamp, so we can query server for newer messages.
	//	 */
	//	long getLatestMessageServerTimestamp( Message::Type type, Message::Direction direction, Message::Status status );
	//}

	//TEST_METHOD( testGetMessageByTimestamp )				//Tested in testAddMessage()
	//{
	//	/**
	//	 * returns Message with given LOCAL timestamp.
	//	 */
	//	Message getMessage( long timeStamp );
	//}


    [TestMethod]
    public void testGetMessages ()
	{
		openDb();
		clearMessageTable();

		Message msg1 = createTestMessage( Message.MsgType.SMS,  Message.MsgDirection.INBOUND, Message.MsgStatus.NEW, "testGetMessages 1" );
		Message msg2 = createTestMessage( Message.MsgType.SMS,  Message.MsgDirection.INBOUND, Message.MsgStatus.NEW, "testGetMessages 2" );
		Message msg3 = createTestMessage( Message.MsgType.SMS,  Message.MsgDirection.INBOUND, Message.MsgStatus.NEW, "testGetMessages 3" );

		bool added1 = getDbm().addMessage( msg1, false );
		bool added2 = getDbm().addMessage( msg2, false );
		bool added3 = getDbm().addMessage( msg3, false );

		if ( added1 && added2 && added3 )
		{
			/**
			 * returns List of messages based on criteria.
			 */
			MessageList msgList1 = getDbm().getMessages( Message.MsgStatus.NEW, Message.MsgDirection.INBOUND );

			Assert.IsTrue( msgList1.Count == 3, "MsgList1 is wrong size" );

			MessageList msgList2 = getDbm().getMessages( Message.MsgStatus.NEW, Message.MsgDirection.OUTBOUND );

			Assert.IsTrue( msgList2.Count == 0, "MsgList2 is wrong size" );
		}
		else
		{
			Assert.Fail( "Not all test recordes were added" );
		}
	}


    [TestMethod]
    public void testGetMessagesForGroupId()	//Not needed for phase 1
	{
		openDb();
		clearMessageTable();

		Message msg1 = createTestMessage( Message.MsgType.SMS,            Message.MsgDirection.INBOUND, Message.MsgStatus.NEW, "testGetMessagesForGroupId" );
		Message msg2 = createTestMessage( Message.MsgType.GROUP_MESSAGE,  Message.MsgDirection.INBOUND, Message.MsgStatus.NEW, "testGetMessagesForGroupId" );
		Message msg3 = createTestMessage( Message.MsgType.GROUP_MESSAGE,  Message.MsgDirection.OUTBOUND, Message.MsgStatus.NEW, "testGetMessagesForGroupId" );

		bool added1 = getDbm().addMessage( msg1, false );
		bool added2 = getDbm().addMessage( msg2, false );
		bool added3 = getDbm().addMessage( msg3, false );

		if ( added1 && added2 && added3 )
		{
			/**
			 * returns a cursor to all the messages from an GM GroupId
			 */
			UmwMessageList msgList1 = getDbm().getMessagesForGroupId( mTestToGroupId );

			Assert.IsTrue( msgList1.Count == 2, "MsgList1 is wrong size" );

			UmwMessageList msgList2 = getDbm().getMessagesForGroupId( "xxxxx" );

			Assert.IsTrue( msgList2.Count == 0, "MsgList2 is wrong size" );
		}
		else
		{
			Assert.Fail( "Not all test recordes were added" );
		}
	}

    [TestMethod]
    public void testUpdateMessageStatus()
	{
		openDb();
		clearMessageTable();

		Message msg1 = createTestMessage( Message.MsgType.SMS, Message.MsgDirection.INBOUND, Message.MsgStatus.NEW, "testUpdateMessage" );
		Int64 localTs = msg1.LocalTimestamp;

		bool added1 = getDbm().addMessage( msg1, false );

		if ( added1 )
		{
			/**
			* Update message status based on LOCAL timestamp
			*/
			getDbm().updateMessage( localTs, Message.MsgStatus.READ );

			Message msgOut = getDbm().getMessage( localTs );

			bool match = (msgOut.Status == Message.MsgStatus.READ);

			Assert.IsTrue( match, "Status was NOT updated" );
		}
		else
		{
			Assert.Fail( "Not all test recordes were added" );
		}
	}

    [TestMethod]
    public void testUpdateMessageStatusAndMsgId()
	{
		openDb();
		clearMessageTable();

		Message msg1 = createTestMessage( Message.MsgType.SMS, Message.MsgDirection.INBOUND, Message.MsgStatus.NEW, "testUpdateMessageStatusAndMsgId" );
		Int64 localTs = msg1.LocalTimestamp;

		bool added1 = getDbm().addMessage( msg1, false );

		if ( added1 )
		{
			String newMsgId = "Updated MsgId";
			/**
			* Update message status and MessageId based on LOCAL timestamp
			*/
			getDbm().updateMessage( localTs, Message.MsgStatus.READ, newMsgId );

			Message msgOut = getDbm().getMessage( localTs );

			bool match1 = (msgOut.Status == Message.MsgStatus.READ);
			bool match2 = (msgOut.MsgId  == newMsgId     );

			Assert.IsTrue( match1, "Status was NOT updated" );
			Assert.IsTrue( match2, "MsgId  was NOT updated" );
		}
		else
		{
			Assert.Fail( "Not all test recordes were added" );
		}
	}

    [TestMethod]
    public void testUpdateMessageStatusAndServerTimestamp()
	{
		openDb();
		clearMessageTable();

		Message msg1 = createTestMessage( Message.MsgType.SMS, Message.MsgDirection.INBOUND, Message.MsgStatus.NEW, "testUpdateMessageStatusAndServerTimestamp" );
		Int64 localTs = msg1.LocalTimestamp;

		bool added1 = getDbm().addMessage( msg1, false );

		if ( added1 )
		{
			Int64 serverTs = localTs + 200;

			/**
			* Update message status and SERVER timestamp based on LOCAL timestamp
			*/
			getDbm().updateMessage( localTs, serverTs, Message.MsgStatus.READ );

			Message msgOut = getDbm().getMessage( localTs );

			bool match1 = (msgOut.Status			       == Message.MsgStatus.READ );
			bool match2 = (msgOut.ServerTimestamp.AsLong() == serverTs		 );

			Assert.IsTrue( match1, "Status was NOT updated" );
			Assert.IsTrue( match2, "Server TS was NOT updated" );
		}
		else
		{
			Assert.Fail( "Not all test recordes were added" );
		}
	}

    [TestMethod]
    public void testUpdateMessageStatusAndServerTsAndMsgId()
	{
		openDb();
		clearMessageTable();

		Message msg1 = createTestMessage( Message.MsgType.SMS, Message.MsgDirection.INBOUND, Message.MsgStatus.NEW, "testUpdateMessageStatusAndServerTimestamp" );
		Int64 localTs = msg1.LocalTimestamp;

		bool added1 = getDbm().addMessage( msg1, false );

		if ( added1 )
		{
			Int64		serverTs = localTs + 200;
			String newMsgId = "Updated MsgId";

			/**
			* Update message status, SERVER timestamp and MessageId based on LOCAL timestamp
			*/
			getDbm().updateMessage( localTs, serverTs, Message.MsgStatus.READ, newMsgId );

			Message msgOut = getDbm().getMessage( localTs );

			bool match1 = (msgOut.Status					== Message.MsgStatus.READ  );
			bool match2 = (msgOut.ServerTimestamp.AsLong()	== serverTs		 );
			bool match3 = (msgOut.MsgId						== newMsgId		 );

			Assert.IsTrue( match1, "Status was NOT updated"    );
			Assert.IsTrue( match2, "Server TS was NOT updated" );
			Assert.IsTrue( match3, "MsgId was NOT updated"     );
		}
		else
		{
			Assert.Fail( "Not all test recordes were added" );
		}
	}

    [TestMethod]
    public void testMarkMessageDelivered()
	{
		openDb();
		clearMessageTable();

		Message msg1 = createTestMessage( Message.MsgType.SMS, Message.MsgDirection.INBOUND, Message.MsgStatus.NEW, "testUpdateMessage" );
		Int64 localTs = msg1.LocalTimestamp;

		bool added1 = getDbm().addMessage( msg1, false );

		if ( added1 )
		{
			/**
			* Appears to be a convenience method to udpate message status as DELIVERED
			*/
			getDbm().markMessageDelivered( localTs, true );

			Message msgOut = getDbm().getMessage( localTs );

			bool match = (msgOut.Status == Message.MsgStatus.DELIVERED);

			Assert.IsTrue( match, "Status was NOT updated" );
		}
		else
		{
			Assert.Fail( "Not all test recordes were added" );
		}
	}

    [TestMethod]
    public void testMarkMessageRead( )
	{
		openDb();
		clearMessageTable();

		Message msg1 = createTestMessage( Message.MsgType.SMS, Message.MsgDirection.INBOUND, Message.MsgStatus.NEW, "testUpdateMessage" );
		Int64 localTs = msg1.LocalTimestamp;

		bool added1 = getDbm().addMessage( msg1, false );

		if ( added1 )
		{
			/**
			* Appears to be a convenience method to udpate message status as READ
			*/
			getDbm().markMessageRead( localTs, true );

			Message msgOut = getDbm().getMessage( localTs );

			bool match = (msgOut.Status == Message.MsgStatus.READ);

			Assert.IsTrue( match, "Status was NOT updated" );
		}
		else
		{
			Assert.Fail( "Not all test recordes were added" );
		}
	}

    [TestMethod]
    public void testUpdateMessageContactInfo()
	{
		openDb();
		clearMessageTable();

		//NOTE: This DBM method only updates CHAT messages with matching FROM_JID.
		//	It also uses INBOUND and OUTBOUND, but it seems this would only be needed for INBOUND (outbound CHATs coming from us.)

		//Generate some test messages.
		Message msg1 = createTestMessage( Message.MsgType.CHAT, Message.MsgDirection.INBOUND,  Message.MsgStatus.NEW, "testUpdateMessageContactInfo 1" );
		Message msg2 = createTestMessage( Message.MsgType.CHAT, Message.MsgDirection.OUTBOUND, Message.MsgStatus.NEW, "testUpdateMessageContactInfo 1" );
		Message msg3 = createTestMessage( Message.MsgType.CHAT, Message.MsgDirection.INBOUND,  Message.MsgStatus.NEW, "testUpdateMessageContactInfo 2" );
		Message msg4 = createTestMessage( Message.MsgType.CHAT, Message.MsgDirection.OUTBOUND, Message.MsgStatus.NEW, "testUpdateMessageContactInfo 2" );

		//Tweak the message to match our test data below.
		msg1.FromJid = "jrttest4a@voxox.com";
		msg2.ToJid   = "jrttest4a@voxox.com";
		msg3.FromJid = "jrttest4b@voxox.com";
		msg4.ToJid   = "jrttest4b@voxox.com";
                     
		msg1.ToJid   = "jrttest7b@voxox.com";
		msg2.FromJid = "jrttest7b@voxox.com";
		msg3.ToJid   = "jrttest7b@voxox.com";
		msg4.FromJid = "jrttest7b@voxox.com";

		bool added1 = getDbm().addMessage( msg1, false );
		bool added2 = getDbm().addMessage( msg2, false );
		bool added3 = getDbm().addMessage( msg3, false );
		bool added4 = getDbm().addMessage( msg4, false );

		if ( added1 && added2 && added3 && added4 )
		{
			//Verify To/From DID are empty
			MessageList msgList1 = getDbm().getMessages( Message.MsgStatus.NEW, Message.MsgDirection.INBOUND );

			foreach ( Message msg in msgList1 )
			{
				if ( !String.IsNullOrEmpty( msg.ToDid ) )
					Assert.Fail( "ToDid is NOT empty." );

				if ( !String.IsNullOrEmpty( msg.FromDid ) )
					Assert.Fail( "FromDid is NOT empty." );

				if ( !String.IsNullOrEmpty( msg.ContactDid ) )
					Assert.Fail( "ContactDid is NOT empty." );
			}

			//Simulate data we would have retrieved via REST API call
			VoxoxContactList vcs = new VoxoxContactList();

			VoxoxContact vc1 = new VoxoxContact();
			vc1.Did      = "17602775395";
			vc1.RegistrationNumber = "";
			vc1.UserId   = 111111;
			vc1.UserName = "jrttest4a";

			VoxoxContact vc2 = new VoxoxContact();;
			vc2.Did = "17602779999";
			vc2.RegistrationNumber = "";
			vc2.UserId = 222222;
			vc2.UserName = "jrttest4b";

			vcs.Add( vc1 );
			vcs.Add( vc2 );

			/**
			* Some times we receive message from contacts not in our address book.
			*
			* We log the original message, then use the phone_number to determine if it is a Voxox user.  This is done asynchrously.
			*
			* This method updates those messages, by calling the single instance version below.
			*/
			getDbm().updateMessageContactInfo( vcs );


			////Verify To/From DID are NOT empty for INBOUND messages only.
			MessageList msgList2 = getDbm().getMessages( Message.MsgStatus.NEW, Message.MsgDirection.INBOUND );

			//Should have 2 entries, one for each JID.  
			//	ASSUMING these are in same order we added them.
			foreach ( Message msg in msgList2 )
			{
				bool match = false;

				if ( msg.FromJid == "jrttest4a@voxox.com" )
				{
					match = msg.FromDid == vc1.Did;
					Assert.IsTrue( match, "FromDid is not correct" );
				}
				else if ( msg.FromJid == "jrttest4b@voxox.com" )
				{
					match = msg.FromDid == vc2.Did;
					Assert.IsTrue( match, "FromDid is not correct" );
				}
				else
				{
					Assert.Fail( "Found unexpected FromJid" );
				}
			}

		}
		else
		{
			Assert.Fail( "Not all test recordes were added" );
		}
	}

    [TestMethod]
    public void testAddMessages()
	{
		openDb();
		clearMessageTable();

		//Generate some test messages.
		Message msg1 = createTestMessage( Message.MsgType.CHAT, Message.MsgDirection.INBOUND, Message.MsgStatus.NEW, "testAddMessages 1" );
		Message msg2 = createTestMessage( Message.MsgType.CHAT, Message.MsgDirection.INBOUND, Message.MsgStatus.NEW, "testAddMessages 2" );
		Message msg3 = createTestMessage( Message.MsgType.CHAT, Message.MsgDirection.INBOUND, Message.MsgStatus.NEW, "testAddMessages 3" );
		Message msg4 = createTestMessage( Message.MsgType.CHAT, Message.MsgDirection.INBOUND, Message.MsgStatus.NEW, "testAddMessages 4" );

		//Add to list
		MessageList msgListIn = new MessageList();
		msgListIn.Add( msg1 );
		msgListIn.Add( msg2 );
		msgListIn.Add( msg3 );
		msgListIn.Add( msg4 );


		/**
		*	Add list of messages to DB.
		*	Typically done from getMessageHistory() API call.
		*/
		getDbm().addMessages( msgListIn );

		MessageList msgListOut = getDbm().getMessages( Message.MsgStatus.NEW, Message.MsgDirection.INBOUND );

		Assert.AreEqual( msgListOut.Count, msgListIn.Count, "Msg counts do not match" );
	}

    [TestMethod]
    public void testDeleteMessages()
	{
		//The DBM.deleteMessages() method simply changes STATUS to DELETED in the local DB.  We have a separate REST API call to delete them
		//	from the server, and the results of that API call will then remove the records from the local DB.

		//For this test, we do same as testAddMessges, but then add a final step to DELETE and check for success.
		openDb();
		clearMessageTable();

		//Generate some test messages.
		Message msg1 = createTestMessage( Message.MsgType.CHAT, Message.MsgDirection.INBOUND, Message.MsgStatus.NEW, "testDeleteMessages 1" );
		Message msg2 = createTestMessage( Message.MsgType.CHAT, Message.MsgDirection.INBOUND, Message.MsgStatus.NEW, "testDeleteMessages 2" );
		Message msg3 = createTestMessage( Message.MsgType.CHAT, Message.MsgDirection.INBOUND, Message.MsgStatus.NEW, "testDeleteMessages 3" );
		Message msg4 = createTestMessage( Message.MsgType.CHAT, Message.MsgDirection.INBOUND, Message.MsgStatus.NEW, "testDeleteMessages 4" );

		//Add to list
		MessageList msgListIn = new MessageList();
		msgListIn.Add( msg1 );
		msgListIn.Add( msg2 );
		msgListIn.Add( msg3 );
		msgListIn.Add( msg4 );


		/**
		*	Add list of messages to DB.
		*	Typically done from getMessageHistory() API call.
		*/
		getDbm().addMessages( msgListIn );

		MessageList msgListOut = getDbm().getMessages( Message.MsgStatus.NEW, Message.MsgDirection.INBOUND );

		Assert.AreEqual( msgListOut.Count, msgListIn.Count, "Msg counts do not match" );

		//Verify the status is NOT DELETED
		foreach ( Message msg in msgListOut )
		{
			Assert.IsTrue( msg.Status != Message.MsgStatus.DELETED, "Message staus == DELETED when it should not be." );
		}

		//Now delete the messages
		Int64 deleted = getDbm().deleteMessages( msgListOut );

		Assert.AreEqual( deleted, msgListOut.Count, "Deleted count does not match number to be deleted." );

		//Verify they all have DELETED status
		MessageList msgListOut2 = getDbm().getMessages( Message.MsgStatus.DELETED, Message.MsgDirection.INBOUND );

		Assert.AreEqual( msgListOut2.Count, msgListOut.Count, "Msg counts do not match" );

		//Verify the status is NOT DELETED
		foreach ( Message msg in msgListOut2 )
		{
			Assert.IsTrue( msg.Status == Message.MsgStatus.DELETED, "Message staus NOT DELETED when it should not be." );
		}
	}

    [TestMethod]
    public void testGetMessageDataForDeletedMessages()
	{
		//Follow same steps as testDeleteMessages() above, and add final call for desired data.	//TODO: refactor and re-use?
		openDb();
		clearMessageTable();

		//Generate some test messages.
		Message msg1 = createTestMessage( Message.MsgType.CHAT, Message.MsgDirection.INBOUND, Message.MsgStatus.NEW, "testDeleteMessages 1" );
		Message msg2 = createTestMessage( Message.MsgType.CHAT, Message.MsgDirection.INBOUND, Message.MsgStatus.NEW, "testDeleteMessages 2" );
		Message msg3 = createTestMessage( Message.MsgType.CHAT, Message.MsgDirection.INBOUND, Message.MsgStatus.NEW, "testDeleteMessages 3" );
		Message msg4 = createTestMessage( Message.MsgType.CHAT, Message.MsgDirection.INBOUND, Message.MsgStatus.NEW, "testDeleteMessages 4" );

		//Add to list
		MessageList msgListIn = new MessageList();
		msgListIn.Add( msg1 );
		msgListIn.Add( msg2 );
		msgListIn.Add( msg3 );
		msgListIn.Add( msg4 );


		/**
		*	Add list of messages to DB.
		*	Typically done from getMessageHistory() API call.
		*/
		getDbm().addMessages( msgListIn );

		MessageList msgListOut = getDbm().getMessages( Message.MsgStatus.NEW, Message.MsgDirection.INBOUND );

		Assert.AreEqual( msgListOut.Count, msgListIn.Count, "Msg counts do not match" );

		//Verify the status is NOT DELETED
		foreach ( Message msg in msgListOut )
		{
			Assert.IsTrue( msg.Status != Message.MsgStatus.DELETED, "Message staus == DELETED when it should not be." );
		}

		//Now delete the messages
		Int64 deleted = getDbm().deleteMessages( msgListOut );

		Assert.AreEqual( deleted, msgListOut.Count, "Deleted count does not match number to be deleted." );

		//Verify they all have DELETED status
		MessageList msgListOut2 = getDbm().getMessages( Message.MsgStatus.DELETED, Message.MsgDirection.INBOUND );

		Assert.AreEqual( msgListOut2.Count, msgListOut.Count, "Msg counts do not match" );

		//Verify the status is NOT DELETED
		foreach ( Message msg in msgListOut2 )
		{
			Assert.IsTrue( msg.Status == Message.MsgStatus.DELETED, "Message staus NOT DELETED when it should not be." );
		}

		/*
		*	Gathers data needed to delete messages from the server.
		*
		*	This looks for all local messages with status DELETED.
		*	Once this data is used in the API call to delete the messages on server, 
		*	a separate method will delete these from the local DB.
		*/
		DeleteMessageDataList deleteList = getDbm().getMessageDataForDeletedMessages();

		Assert.AreEqual( deleteList.Count, msgListOut2.Count, "Msg counts do not match" );
	}


    [TestMethod]
    public void testUpdateMessageRichData() //TODO-DBTEST - need method for toJsonString.  
	{
		//Follow same steps as testDeleteMessages() above, and add final call for desired data.	//TODO: refactor and re-use?
		openDb();
		clearMessageTable();

		//Generate some test messages.
		Message msg1 = createTestMessage( Message.MsgType.CHAT, Message.MsgDirection.OUTBOUND, Message.MsgStatus.NEW, "testUpdateMessageRichData 1" );
		Int64 localTs = msg1.LocalTimestamp;

		Boolean added1 = getDbm().addMessage( msg1, false );

		if ( added1 )
		{
			RichData richData1 = new RichData();

			richData1.Type         = RichData.RichDataType.IMAGE;
			richData1.MediaUrl     = "http://voxox.com/test/mediaUrl";
			richData1.ThumbnailUrl = "http://voxox.com/test/thumbnailUr;";

            String jsonString1 = getDbm().richDataToJsonString( richData1 );

			/**
			 * Description:
			 * 	Update Message in DB with RichData.  Typically, we save message in DB, upload files, then update msg with URLs, before message is sent.
			 * 
			*/
			getDbm().updateMessageRichData( localTs, richData1, false );

			Message msgOut1 = getDbm().getMessage( localTs );

			String body1 = msgOut1.Body;	    //Should have RichData JSON.

			Assert.AreEqual( jsonString1, body1, "Image: JSON strings do not match." );

			//Location
			RichData richData2 = new RichData();
			richData2.Type      = RichData.RichDataType.LOCATION;
			richData2.Latitude  = 11.11;
			richData2.Longitude = 22.22;
			richData2.Address   = "My Address";

            String jsonString2 = getDbm().richDataToJsonString( richData2 );

			getDbm().updateMessageRichData( localTs, richData2, false );

			Message msgOut2 = getDbm().getMessage( localTs );

			String body2 = msgOut2.Body;	//Should have RichData JSON.

			Assert.AreEqual( jsonString2, body2, "Location: JSON strings do not match." );
		}
		else
		{
			Assert.Fail( "Not all test records were added" );
		}
	}

    //[TestMethod]
    //public void testUpdateGroupMessageRichData()	//TODO
	//{
	//	/**
	//	 * Description:
	//	 * 	Update GM message in DB with RichData.
	//	 * 	We need this separate method because RichData is embedded in body so we cannot simply replace BODY.
	//	 * */
	//	void updateGroupMessageRichData( long timeStamp, const RichData& richData, bool broadcast, String origBody );
	//}

   // [TestMethod]
    //public void testUpdateTranslatedMessage( )		//TODO
	//{
	//	/**
	//	* Update message translation message based on LOCAL timestamp
	//	*/
	//	void updateTranslatedMessage( long timeStamp, const Message& message );
	//}

	//---------------------------------------
	// PUBLIC Contact/PhoneNumber Methods
	//---------------------------------------

    [TestMethod]
    public void testAddOrUpdateVoxoxContacts()
	{
		Int64 contactCount = initPhoneNumbers();	//Contains actual call to addAddressBookContacts()

//		ciList.push_back( ContactImport( 1, "Contact 1", "17605551111", "Work"   ) );
//		ciList.push_back( ContactImport( 1, "Contact 1", "17605552222", "Mobile" ) );
//		ciList.push_back( ContactImport( 2, "Contact 2", "17605553333", "Home"   ) );
//		ciList.push_back( ContactImport( 3, "Contact 3", "17605554444", "Mobile" ) );

		int			userId1		= 111111;
		int			userId2		= 222222;
		String userName1	= "jrttest4a";
		String userName2	= "jrttest7b";

		VoxoxContactList vcs = new VoxoxContactList();
		vcs.Add( new VoxoxContact( "abc", "17605552222", userName1, "17605551111", userId1, "Voxox", false, false, "TestSrc" ) );
		vcs.Add( new VoxoxContact( "abd", "",            userName2, "17605553333", userId2, "Voxox", false, false, "TestSrc" ) );

		/**
		* Add or updated DB based on VoxoxContact info.
		*/
		getDbm().addOrUpdateVoxoxContacts( vcs );

		//Retrieve all contacts - TODO: This is not sufficient since it retrieves basic info and a LIST of phoneNumbers.  Not easy to test.
		//	I have manually verified this works, but the test below needs to be tweaked.  Moving on for now.  The main issue is contacts with more than one phone number.
	}

    [TestMethod]
    public void testAddOrUpdateXmppContacts()		//Includes test for addOrUpdateXmppContact, since first iteratively calls the second.
	{
		//We received a chat from someone NOT in our AB.  
		//	Expectation is that we add a PhoneNumber entry with just the chat info.
		//	This method updates that record.  In unusual circumstance, we may add the record.

		Int64 contactCount = initPhoneNumbers();	//Contains actual call to addAddressBookContacts()

		String userName    = "jrttestxx";
		String phoneNumber = "17605559999";
		VoxoxPhoneNumberList voxoxPhoneNumbers = new VoxoxPhoneNumberList();

		voxoxPhoneNumbers.Add( new VoxoxPhoneNumber( phoneNumber, userName ) );

		/**
		* Update PhoneNumber table with XMPP contact info, based on Voxox PhoneNumber
		*	Simply calls singular version below.
		*/
//		getDbm().addOrUpdateXmppContacts( voxoxPhoneNumbers );

		ContactList contacts = getDbm().getContacts( phoneNumber, ContactFilter.All );

		if ( contacts.Count == 1 )
		{
//			String fullJid  = DbUtils::ensureFullJid( userName );	//DBM will ensure full JID.
			Contact contact = contacts.ToArray()[0];                //Get First

			Assert.AreEqual( phoneNumber, contact.Name, "XMPP JIDs do not match." );
		}
		else
		{
			//TODO: add appropriate getContacts() changes/methods
//			Assert.Fail( "ContactList is wrong size.  Expected 1 entry." );
		}

	}

    //[TestMethod]
    //public void testAddOrUpdateXmppContact()		//Tested above
	//{
	//	/**
	//	 * Description:
	//	 * Add OR update an XMPP contact to the phone number table. an XMPP contact may not have an AB contactKey associated with it. it can happen if we
	//	 *	received a CHAT message from a VoxOx user that is not in our address book. The contact will be added to the phone number table with a unique common group (cm_group).
	//	 */
	//	void addOrUpdateXmppContact( String xmppAddress, String phoneNumber );
	//}

    [TestMethod]
    public void testUpdateXmppPresence()
	{
		Int64 contactCount = initPhoneNumbersEx();		//Create contacts/phoneNumbers with Voxox user info

		String userName = "jrttest4a";

		XmppContact xc = new XmppContact();
		xc.Jid	    = userName;
		xc.MsgState = "gone to lunch";
		xc.Status   = "UNAVAILABLE";
		/**
		 * Description:
		 * update the presence and the status of an XMPP contact.
		 * status values may be: UNAVAILABLE, AVAILABLE, AVAILABLE_AWAY ...
		 * presence: a status message of the presence update, or null if there is not a status. The status is free-form text describing a user's presence (i.e., "gone to lunch").
		 * */
		getDbm().updateXmppPresence( xc );		//TODO: I have verified this works, but we have no method to retrieve the desired XMPP JID contact.  Moving on.

		//String contactName = "Contact 1";

		//ContactList contacts = getDbm().getContacts( contactName,  PhoneNumber.ContactFilter.CONTACT_FILTER_ALL );

		//if ( contacts.Count == 1 )
		//{
		//	String fullJid = DbUtils::ensureFullJid( userName );	//DBM will ensure full JID.
		//	Contact contact = contacts.front();

		//	Assert.AreEqual( contactName,      contact.getName(),         "Names do not match.",          );
		//	Assert.AreEqual( xc.Status,   contact.getXmppStatus(),   "XMPP Status do not match.",    );
		//	Assert.AreEqual( xc.getMsgState(), contact.getXmppPresence(), "XMPP Presence do not match." );
		//}
		//else
		//{
		//	Assert.Fail( "ContactList is wrong size.  Expected 1 entry." );
		//}
	}

	//COMMENTED BECAUSE WE NOW SET FAVORITE BASED ON PHONE NUMBER.
//	[TestMethod]
//	public void testUpdateContactPhoneNumberFavorite()
//	{
//		Int64 contactCount = initPhoneNumbersEx();		//Create contacts/phoneNumbers with Voxox user info

////		String phoneNumber = "17605553333";

//		//We need the RecordId of the Contact/PhoneNumber
//		String contactName = "Contact 2";

//		ContactList contacts = getDbm().getContacts( contactName,  ContactFilter.All );

//		if ( contacts.Count == 1 )
//		{
//			Contact contact      = contacts.ToArray()[0];
//			Int32     recordId   = contact.RecordId;
//			Boolean   isFavorite = true;	//contact.IsFavorite;	//No longer at contact level

//			/**
//			 * Description:
//			 * update the new favorite status of a phone number entry.
//			 * */
//			getDbm().updateContactPhoneNumberFavorite( recordId,  !isFavorite );

//			ContactList contacts2 = getDbm().getContacts( contactName,  ContactFilter.All );

//			if ( contacts2.Count == 1 )
//			{
//				Contact contact2 = contacts2.ToArray()[0];

////				Assert.AreEqual( !isFavorite, contact2.IsFavorite, "IsFavorite values do not match." );	//No longer at contact level
//			}
//			else
//			{
//				Assert.Fail( "ContactList is wrong size.  Expected 1 entry." );
//			}
//		}
//		else
//		{
//			Assert.Fail( "ContactList is wrong size.  Expected 1 entry." );
//		}
//	}

    [TestMethod]
    public void testGetContactsWithSearchFilterAndContactFilter()
	{
		//This has already been informally tested in Unit Tests above, but let's do it officially and more completely here.
		//	NOTE: This returns ONLY entries are in AB, so any naked chat contacts will not show here.
		Int64 contactCount = initPhoneNumbersEx();		//Create contacts/phoneNumbers with Voxox user info

		//Get all contacts
		ContactList contacts1 = getDbm().getContacts( "", ContactFilter.All );			// <<<<<<< Actual test

		if ( contacts1.Count != mABContactCount )
		{
			Assert.Fail( "Not all expected contacts were retrieved" );
		}

		//Select by search fileter
		//We need the RecordId of the Contact/PhoneNumber
		String contactName = "Contact 2";

		/**
		 * Description:
		 * Returns a cursor that is UNION of phone numbers AND GM Groups that match the filter.
		 *
		 * As we group by the ContactKey (we want the contact), we may lose the VoxOx flag of one of the phone numbers
		 * so we added a little trick in the query to count if any VoxOx numbers and added a field to the table called VOXOX_COUNT.
		 *
		 * Used by the CONTACTS tab.
		 * */
		ContactList contacts2 = getDbm().getContacts( contactName, ContactFilter.All );	// <<<<<<< Actual test

		if ( contacts2.Count != 1 )
		{
			Assert.Fail( "Incorrect contact count for Search Filter test." );
		}

		//COMMENTED BECAUSE WE NOW SET FAVORITE BASED ON PHONE NUMBER.
		////Now set a FAVORITE and test the ContactFilter
		//Contact contact  = contacts2.ToArray()[0];
		//int     recordId = contact.RecordId;

		//getDbm().updatePhoneNumberFavorite( recordId, true );

		//ContactList contacts3 = getDbm().getContacts( "", ContactFilter.All );		// <<<<<<< Actual test

		//if ( contacts3.Count != 1 )
		//{
		//	Assert.Fail( "Incorrect contact count for Contact Filter test." );
		//}
	}

    [TestMethod]
    public void testGetContactDetailsPhoneNumbersByCmGroup()
	{
		Int64 contactCount = initPhoneNumbersEx();		//Create contacts/phoneNumbers with Voxox user info

		int cmGroup = 1;

		/**
		 * Description:
		 * */
		PhoneNumberList contacts1 = getDbm().getContactDetailsPhoneNumbersByCmGroup( cmGroup );

		if ( contacts1.Count != 2 )
		{
			Assert.Fail( "Incorrect contact count for test1." );
		}

		PhoneNumberList contacts2 = getDbm().getContactDetailsPhoneNumbersByCmGroup( 2 );

		if ( contacts2.Count != 1 )
		{
			Assert.Fail( "Incorrect contact count for test2." );
		}
	}

    [TestMethod]
    public void testGetContactPhoneNumbersListForCmGroup()	//TODO-UNITTEST: need number on multiple contacts
	{
		Int64 contactCount = initPhoneNumbersEx();		//Create contacts/phoneNumbers with Voxox user info

		/**
		 * Description:
		 * returns a list of PhoneNumber objects associated with the CmGroup.
		 * */
		PhoneNumberList pnList1 = getDbm().getContactPhoneNumbersListForCmGroup( 1, false );	//CmGroup, isVoxox
		if ( pnList1.Count != 2 )	//Expect Mobile and Voxox
		{
			Assert.Fail( "Incorrect contact count for test1." );
		}

		PhoneNumberList pnList2 = getDbm().getContactPhoneNumbersListForCmGroup( 1, true );
		if ( pnList2.Count != 1 )	//Expect Just Voxox
		{
			Assert.Fail( "Incorrect contact count for test1." );
		}

		PhoneNumberList pnList3 = getDbm().getContactPhoneNumbersListForCmGroup( 2, false );
		if ( pnList3.Count != 1 )	//Expect Just Voxox (only number on CmGroup)
		{
			Assert.Fail( "Incorrect contact count for test3." );
		}

		PhoneNumberList pnList4 = getDbm().getContactPhoneNumbersListForCmGroup( 2, true  );
		if ( pnList4.Count != 1 )	//Expect Just Voxox (only number on CmGroup)
		{
			Assert.Fail( "Incorrect contact count for test4." );
		}
	}

    [TestMethod]
    public void testGetPhoneNumber()
	{
		Int64 contactCount = initPhoneNumbersEx();		//Create contacts/phoneNumbers with Voxox user info

		String pnString = "17605551111";
		int			cmGroup  = 1;
        

		/**
		 * Description: Get phoneNumber object for given phoneNumbe string and/or CMGroup
		 * Used in UMW and notification
		 * */
		PhoneNumber pn1 = getDbm().getPhoneNumber( pnString, cmGroup );	//TODO-UNITTEST - PN looks OK, but missing compound name.
		Assert.IsTrue( pn1.isValid(), "Test1 should return VALID PhoneNumber object." );

		PhoneNumber pn2 = getDbm().getPhoneNumber( pnString, 2 );	//Fake number in more than one Contact (Does not fake properly, or compound name not working.
		Assert.IsTrue( pn2.isValid(), "Test2 should return VALID PhoneNumber object." );

		PhoneNumber pn3 = getDbm().getPhoneNumber( String.Empty, cmGroup );
		Assert.IsFalse( pn3.isValid(), "Test3 should return INVALID PhoneNumber object." );

		PhoneNumber pn4 = getDbm().getPhoneNumber( String.Empty, 0 );		//This should fail.
		Assert.IsFalse( pn4.isValid(), "Test4 should return INVALID PhoneNumber object." );
	}

    [TestMethod]
    public void testGetVoxoxUserIdForPhoneNumber()
	{
		Int64 contactCount = initPhoneNumbersEx();		//Create contacts/phoneNumbers with Voxox user info

		String pnString       = "17605551111";
		int			expectedUserId = 111111;

		/**
		* Get Voxox userId for given phoneNumber string.
		*/
		int userId1 = getDbm().getVoxoxUserId( pnString );
		Assert.AreEqual( expectedUserId, userId1, "UserId is not as expected." );

		int userId2 = getDbm().getVoxoxUserId( "777777777" );		//Fail
		Assert.AreEqual( 0, userId2, "Test2: UserId is not zero as expected for non-existent PN." );
	}

	
    [TestMethod]
    public void testGetDidByJid()
	{
		Int64 contactCount = initPhoneNumbersEx();		//Create contacts/phoneNumbers with Voxox user info

		String expectedUserDid = "17605551111";
		String jid				= "jrttest4a";

		/**
		* Get DID for given JID
		*/
		String did1 = getDbm().getDidByJid( jid );
		Assert.AreEqual( expectedUserDid, did1, "DID is not as expected." );

		String did2 = getDbm().getDidByJid( "xxxx" );		//Expected to fail
		Assert.AreEqual( String.Empty, did2, "DID is not as expected." );
	}

    [TestMethod]
    public void testGetVoxoxUserIdForCmGroup()
	{
		Int64 contactCount = initPhoneNumbersEx();		//Create contacts/phoneNumbers with Voxox user info

		int expectedUserId = 111111;

		/**
		* Get Voxox userId for given CMGroup.	//TODO: I don't think this logic holds since a CMGroup may have more than one VoxoxID (one contact with multiple Voxox numbers).
		*/
		int userId1 = getDbm().getVoxoxUserId( 1 );	//CmGroup
		Assert.AreEqual( expectedUserId, userId1, "UserId is not as expected." );

		int userId2 = getDbm().getVoxoxUserId( 99 );	//CmGroup is invalid
		Assert.AreEqual( 0, userId2, "Test2: UserId is not zero as expected for non-existent PN." );
	}

    [TestMethod]
    public void testGetCmGroupForPhoneNumber()
	{
		Int64 contactCount = initPhoneNumbersEx();		//Create contacts/phoneNumbers with Voxox user info

		String pnString  = "17605551111";
		int expectedCmGroup   = 1;

		/**
		* Get CmGroup for given phoneNumber string.
		*/
		int cmGroup1 = getDbm().getCmGroup( pnString );
		Assert.AreEqual( expectedCmGroup, cmGroup1, "CmGroup is not as expected." );

		int cmGroup2 = getDbm().getCmGroup( "77777777" );	//Should fail
		Assert.AreEqual( 0, cmGroup2, "CmGroup should be zero." );
	}

    [TestMethod]
    public void testIsXmppContactInLocalAddressBook()
	{
		Int64 contactCount = initPhoneNumbersEx();		//Create contacts/phoneNumbers with Voxox user info

		String jid = "jrttest4a";

		/**
		* Determine if given XMPP JID is in our DB
		*/
		bool result1 = getDbm().isXmppContactInLocalAddressBook( jid );
		Assert.IsTrue( result1, "JID is not in DB when it should be." );

		bool result2 = getDbm().isXmppContactInLocalAddressBook( "xxxxx" );
		Assert.IsFalse( result2, "JID IS in DB when it should NOT be." );
	}

    [TestMethod]
    public void testGetNewInboundMessageCountForCmGroup()
	{
		Int64 contactCount = initPhoneNumbersEx();		//Create contacts/phoneNumbers with Voxox user info
		
		clearMessageTable();

		Message msg1 = createTestMessage( Message.MsgType.SMS,  Message.MsgDirection.INBOUND, Message.MsgStatus.NEW, "testGetNewInboundMessageCountForCmGroup 1" );
		Message msg2 = createTestMessage( Message.MsgType.SMS,  Message.MsgDirection.INBOUND, Message.MsgStatus.NEW, "testGetNewInboundMessageCountForCmGroup 2" );
		Message msg3 = createTestMessage( Message.MsgType.SMS,  Message.MsgDirection.INBOUND, Message.MsgStatus.READ, "testGetNewInboundMessageCountForCmGroup 3" );

		int newCount = 2;

		bool added1 = getDbm().addMessage( msg1, false );
		bool added2 = getDbm().addMessage( msg2, false );
		bool added3 = getDbm().addMessage( msg3, false );

		if ( added1 && added2 && added3 )
		{
			/**
			 * returns number of new in-bound messages for a CM Group
			 * This is called in for each Contact in ContactsMessages (or its array/cursor adapter).  So could be called a lot, especially when scrolling.
			 */
			int count1 = getDbm().getNewInboundMessageCountForCmGroup( 1 );	//CmGroup
			Assert.AreEqual( newCount, count1, "New message Count is not correct." );

			int count2 = getDbm().getNewInboundMessageCountForCmGroup( 99 );	//Should fail with zero
			Assert.AreEqual( 0, count2, "New message Count is not correct." );
		}
		else
		{
			Assert.Fail( "Not all messages added for test." );
		}
	}

    [TestMethod]
    public void testGetNewInboundMessageCount()
	{
		Int64 contactCount = initPhoneNumbersEx();		//Create contacts/phoneNumbers with Voxox user info
		
		clearMessageTable();

		Message msg1 = createTestMessage( Message.MsgType.SMS,  Message.MsgDirection.INBOUND, Message.MsgStatus.NEW, "testGetNewInboundMessageCountForCmGroup 1" );
		Message msg2 = createTestMessage( Message.MsgType.SMS,  Message.MsgDirection.INBOUND, Message.MsgStatus.NEW, "testGetNewInboundMessageCountForCmGroup 2" );
		Message msg3 = createTestMessage( Message.MsgType.SMS,  Message.MsgDirection.INBOUND, Message.MsgStatus.READ, "testGetNewInboundMessageCountForCmGroup 3" );

		Message msg4 = createTestMessage( Message.MsgType.CHAT,  Message.MsgDirection.INBOUND, Message.MsgStatus.NEW, "testGetNewInboundMessageCountForCmGroup 1" );
		Message msg5 = createTestMessage( Message.MsgType.CHAT,  Message.MsgDirection.INBOUND, Message.MsgStatus.NEW, "testGetNewInboundMessageCountForCmGroup 2" );
		Message msg6 = createTestMessage( Message.MsgType.CHAT,  Message.MsgDirection.INBOUND, Message.MsgStatus.READ, "testGetNewInboundMessageCountForCmGroup 3" );

		int newSmsCount  = 2;
		int newChatCount = 2;
		int newTotalCount = newSmsCount + newChatCount;

		bool added1 = getDbm().addMessage( msg1, false );
		bool added2 = getDbm().addMessage( msg2, false );
		bool added3 = getDbm().addMessage( msg3, false );

		bool added4 = getDbm().addMessage( msg4, false );
		bool added5 = getDbm().addMessage( msg5, false );
		bool added6 = getDbm().addMessage( msg6, false );

		if ( added1 && added2 && added3 && added4 && added5 && added6 )
		{
			String phoneNumber = "17605551111";
			String jid			= "jrttest4a";
			/**
			 * returns number of new in-bound messages for a PhoneNumber string or JID.
			 *
			 * (I am not sure but...)This is called in for each Contact in ContactsMessages (or its array/cursor adapter).  So could be called a lot, especially when scrolling.
			 */
			int count1 = getDbm().getNewInboundMessageCount( phoneNumber, jid );					//Both PhoneNumber and JID
			Assert.AreEqual( newTotalCount, count1, "New message Count is not correct." );

			int count2 = getDbm().getNewInboundMessageCount( phoneNumber, "" );					//Just PhoneNumber
			Assert.AreEqual( newSmsCount, count2, "New message Count is not correct." );
			
			int count3 = getDbm().getNewInboundMessageCount( "", jid );							//Just JID
			Assert.AreEqual( newChatCount, count3, "New message Count is not correct." );

			int count4 = getDbm().getNewInboundMessageCount( "7777777", "xxx" );					//Values not in DB
			Assert.AreEqual( 0, count4, "New message Count is not correct." );

			int count5 = getDbm().getNewInboundMessageCount( "", "" );								//Both values invalid/empty
			Assert.AreEqual( 0, count5, "New message Count is not correct." );
		}
		else
		{
			Assert.Fail( "Not all messages added for test." );
		}
	}

    [TestMethod]
    public void testGetContactsMessageSummary()
	{
		Int64 contactCount = initPhoneNumbersEx();		//Create contacts/phoneNumbers with Voxox user info
		
		clearMessageTable();

		Message msg1 = createTestMessage( Message.MsgType.SMS,  Message.MsgDirection.INBOUND, Message.MsgStatus.NEW, "testGetContactsMessageSummary 1" );
		Message msg2 = createTestMessage( Message.MsgType.SMS,  Message.MsgDirection.INBOUND, Message.MsgStatus.NEW, "testGetContactsMessageSummary 2" );
		Message msg3 = createTestMessage( Message.MsgType.SMS,  Message.MsgDirection.INBOUND, Message.MsgStatus.READ, "testGetContactsMessageSummary 3" );

//		Message msg4 = createTestMessage( Message.MsgType.CHAT,  Message.MsgDirection.INBOUND, Message.MsgStatus.NEW, "testGetContactsMessageSummary 4" );
//		Message msg5 = createTestMessage( Message.MsgType.CHAT,  Message.MsgDirection.INBOUND, Message.MsgStatus.NEW, "testGetContactsMessageSummary 5" );
//		Message msg6 = createTestMessage( Message.MsgType.CHAT,  Message.MsgDirection.INBOUND, Message.MsgStatus.READ, "testGetContactsMessageSummary 6" );

		int newSmsCount   = 2;
		int newChatCount  = 2;
		int newTotalCount = newSmsCount + newChatCount;

		bool added1 = getDbm().addMessage( msg1, false );
		bool added2 = getDbm().addMessage( msg2, false );
		bool added3 = getDbm().addMessage( msg3, false );

//		bool added4 = getDbm().addMessage( msg4, false );
//		bool added5 = getDbm().addMessage( msg5, false );
//		bool added6 = getDbm().addMessage( msg6, false );

		if ( added1 && added2 && added3 /*&& added4 && added5 && added6*/ )
		{
			/**
			* Return list of ContactMessage objects.
			* Used on main Messages window.
			*/
			ContactMessageSummaryList summaryList = getDbm().getContactsMessageSummary();

			int xxx = 1;
            xxx++;
		}
	}

    //[TestMethod]
    //public void testGetNewInboundMessageCountForGroupId()		//TODO-PHASE2
	//{
	//	/**
	//	 * returns number of new in-bound messages for a GM GroupId
	//	 *
	//	 * This is called in for each Contact in ContactsMessages (or its array/cursor adapter).  So could be called a lot, especially when scrolling.
	//	 */
	//	int getNewInboundMessageCountForGroupId( String groupId );
	//}

    //[TestMethod]
    //public void testGetTranslationModeProps()		//TODO-UNITTEST
	//{
	//	MessageTranslationProperties getTranslationModeProps( String phoneNumber, String jid );
	//}

    //[TestMethod]
    //public void testAddContactMessageTranslation()	//TODO-UNITTEST
	//{
	//	/**
	//	 * Add translation mode to a contact. What required is wither the phone number of jid.
	//	 * CONTACT_ID should -NOT- be used for detecting a contact as it may change when user refresh the contacts list.
	//	 * returns true for success.
	//	 * */
	//	bool addContactMessageTranslation( const MessageTranslationProperties& mt );
	//}

	//----------------------------------------------------------------------------
	//PUBLIC RecentCall tests
	//----------------------------------------------------------------------------

    [TestMethod]
    public void testAddRecentCall()
	{
		Int64 contactCount = initPhoneNumbersEx();

		String					origNumber		= "+1(760)555-1111";
		Boolean					incoming		= true;
		RecentCall.CallStatus	expectedStatus	= RecentCall.CallStatus.New;
		Int64					startTime		= VxTimestamp.GetNowTimestamp();

		/**
		* Add new call to DB. Return DB recordId.
		*	This will be called when an incoming call event is detected or 
		*	user makes an outgoing call.
		*/
		int callId = getDbm().addRecentCall( origNumber, startTime, incoming );

		if ( callId > 0 )
		{
			RecentCall rc = getDbm().getRecentCall( callId );
//			String number = DbUtils.getNormalizedNumberNoPrefix( origNumber );		//TODO: unit test works in native code.  Verify this later.

			Assert.IsTrue( rc.Status     == expectedStatus,	"Status mismatch."			);
			Assert.IsTrue( rc.RecordId   == callId,			"CallId mismatch."			);
			Assert.IsTrue( rc.Incoming   == incoming,		"Incoming flag mismatch."	);
			Assert.IsTrue( rc.OrigNumber == origNumber,		"OrigNumber mismatch."		);
		//TODO: unit test works in native code.  Verify this later.
		}
		else
		{
			Assert.Fail( "Unable to add test call." );
		}
	}

    [TestMethod]
    public void testUpdateRecentCallAnswered()
	{
		Int64 contactCount = initPhoneNumbersEx();

		String					origNumber		= "+1(760)555-1111";
		Boolean					incoming		= true;
		RecentCall.CallStatus	expectedStatus	= RecentCall.CallStatus.Answered;
		Int64					startTime		= VxTimestamp.GetNowTimestamp();

		Int32 callId = getDbm().addRecentCall( origNumber, startTime, incoming );

		if ( callId > 0 )
		{
			System.Threading.Thread.Sleep( 2 );		//To determine that startTimestamp differs from timestamp
			/**
			* Update call status on Answer, Decline, or Hangup (even for unanswered call)
			*/
			getDbm().updateRecentCallAnswered( callId );

			RecentCall	rc		= getDbm().getRecentCall( callId );
//			String		number	= DbUtils.getNormalizedNumberNoPrefix( origNumber );		//TODO: unit test works in native code.  Verify this later.

			Assert.IsTrue( rc.Status     == expectedStatus,	"Status mismatch."         );
			Assert.IsTrue( rc.RecordId   == callId,			"CallId mismatch."         );
			Assert.IsTrue( rc.Incoming   == incoming,		"Incoming flag mismatch."  );
			Assert.IsTrue( rc.OrigNumber == origNumber,		"OrigNumber mismatch."     );
//			Assert.IsTrue( rc.Number     == number,			"Number mismatch."         );	//TODO: unit test works in native code.  Verify this later.

//			Assert.IsTrue( rc.StartTimestamp > rc.StartTimestamp, "Timestamp mismatch."   );	//TODO:Need better test
		}
		else
		{
			Assert.Fail( "Unable to add test call."  );
		}
	}

    [TestMethod]
    public void testUpdateRecentCallDeclined()
	{
		Int64 contactCount = initPhoneNumbersEx();

		String					origNumber		= "+1(760)555-1111";
		Boolean					incoming		= true;
		RecentCall.CallStatus	expectedStatus	= RecentCall.CallStatus.Declined;
		Int64					startTime		= VxTimestamp.GetNowTimestamp();

		Int32 callId = getDbm().addRecentCall( origNumber, startTime, incoming );

		if ( callId > 0 )
		{
			System.Threading.Thread.Sleep( 2 );		//To determine that startTimestamp differs from timestamp
			/**
			* Update call status on Answer, Decline, or Hangup (even for unanswered call)
			*/
			getDbm().updateRecentCallDeclined( callId );

			RecentCall rc	  = getDbm().getRecentCall( callId );
//			String	   number = DbUtils.getNormalizedNumberNoPrefix( origNumber );		//TODO: unit test works in native code.  Verify this later.

			Assert.IsTrue( rc.Status			 == expectedStatus,	"Status mismatch."          );
			Assert.IsTrue( rc.RecordId			 == callId,			"CallId mismatch."          );
			Assert.IsTrue( rc.Incoming			 == incoming,		"Incoming flag mismatch."   );
			Assert.IsTrue( rc.OrigNumber		 == origNumber,		"OrigNumber mismatch."      );
//			Assert.IsTrue( rc.Number			 == number,			"Number mismatch."          );		//TODO: unit test works in native code.  Verify this later.
			Assert.IsTrue( rc.StartTime.AsLong() == 0,				"StartTimestamp mismatch."  );
		}
		else
		{
			Assert.Fail( "Unable to add test call."  );
		}
	}

    [TestMethod]
    public void testUpdateRecentCallAnsweredAndTerminated()
	{
		Int64 contactCount = initPhoneNumbersEx();

		String					origNumber		= "+1(760)555-1111";
		Boolean					incoming		= true;
		RecentCall.CallStatus	expectedStatus	= RecentCall.CallStatus.Answered;
		Int32					expectedDur     = 2;
		Int64					startTime		= VxTimestamp.GetNowTimestamp();

		Int32 callId = getDbm().addRecentCall( origNumber, startTime, incoming );

		if ( callId > 0 )
		{
			System.Threading.Thread.Sleep( 2 );		//To determine that startTimestamp differs from timestamp

			getDbm().updateRecentCallAnswered( callId );

			System.Threading.Thread.Sleep( expectedDur * 1000 );		//So we have some Duration
			
			getDbm().updateRecentCallTerminated( callId );

			RecentCall rc = getDbm().getRecentCall( callId );
//			String number = DbUtils.getNormalizedNumberNoPrefix( origNumber );		//TODO: unit test works in native code.  Verify this later.

			Assert.IsTrue( rc.Status		== expectedStatus,	"Status mismatch."         );
			Assert.IsTrue( rc.RecordId		== callId,			"CallId mismatch."         );
			Assert.IsTrue( rc.Incoming		== incoming,		"Incoming flag mismatch."  );
			Assert.IsTrue( rc.OrigNumber	== origNumber,		"OrigNumber mismatch."     );
//			Assert.IsTrue( rc.Number		== number,			"Number mismatch."         );		//TODO: unit test works in native code.  Verify this later.

//			Assert.IsTrue( rc.StartTimestamp > rc.Timestamp,	"Timestamp mismatch."      );	//TODO:Need better test

			Assert.IsTrue( rc.Duration == expectedDur,			"Duration mismatch."  	   );	//Sufficient?
		}
		else
		{
			Assert.Fail( "Unable to add test call."  );
		}
	}

    [TestMethod]
    public void testUpdateRecentCallMissedAndTerminated()
	{
		Int64 contactCount = initPhoneNumbersEx();

		String					origNumber		= "+1(760)555-1111";
		Boolean					incoming		= true;
		RecentCall.CallStatus	expectedStatus	= RecentCall.CallStatus.Missed;
		Int32					expectedDur     = 0;
		Int64					startTime		= VxTimestamp.GetNowTimestamp();

		Int32 callId = getDbm().addRecentCall( origNumber, startTime, incoming );

		if ( callId > 0 )
		{
			System.Threading.Thread.Sleep( 2 );		//To determine that startTimestamp differs from timestamp

//			getDbm().updateRecentCallAnswered( callId );	//User just let's it ring until caller hangs up.

			System.Threading.Thread.Sleep( 2000 );		//So we have some time difference.
			
			getDbm().updateRecentCallTerminated( callId );

			RecentCall rc = getDbm().getRecentCall( callId );
//			String number = DbUtils.getNormalizedNumberNoPrefix( origNumber );		//TODO: unit test works in native code.  Verify this later.

			Assert.IsTrue( rc.Status			 == expectedStatus,	"Status mismatch."          );
			Assert.IsTrue( rc.RecordId			 == callId,			"CallId mismatch."          );
			Assert.IsTrue( rc.Incoming			 == incoming,		"Incoming flag mismatch."   );
			Assert.IsTrue( rc.OrigNumber		 == origNumber,		"OrigNumber mismatch."      );
//			Assert.IsTrue( rc.Number			 == number,			"Number mismatch."          );		//TODO: unit test works in native code.  Verify this later.
			Assert.IsTrue( rc.StartTime.AsLong() == 0,			"StartTimestamp mismatch."  );
			Assert.IsTrue( rc.Duration			 == expectedDur,		"Duration." 			    );
		}
		else
		{
			Assert.Fail( "Unable to add test call."  );
		}
	}

    [TestMethod]
    public void testDeleteRecentCall()
	{
		Int64 contactCount = initPhoneNumbersEx();

		String					origNumber		= "+1(760)555-1111";
		Boolean					incoming		= true;
//		RecentCall.CallStatus	expectedStatus	= RecentCall.CallStatus.Answered;
		Int32					expectedDur     = 2;
		Int64					startTime		= VxTimestamp.GetNowTimestamp();

		Int32 callId = getDbm().addRecentCall( origNumber, startTime, incoming );

		if ( callId > 0 )
		{
			System.Threading.Thread.Sleep( 2 );		//To determine that startTimestamp differs from timestamp

			getDbm().updateRecentCallAnswered( callId );

			System.Threading.Thread.Sleep( expectedDur * 1000 );		//So we have some Duration
			
			getDbm().updateRecentCallTerminated( callId );

			/**
			* Delete call - TODO: this may have to be 2-phase, like deleting messages is. Depends on sending call history to server.
			*/
			getDbm().deleteRecentCall( callId );

			RecentCall rc = getDbm().getRecentCall( callId );

			Assert.IsFalse( rc.isValid(),	"RecentCall should be INVALID."  );
		}
		else
		{
			Assert.Fail( "Unable to add test call."  );
		}
	}

	/**
	* Retrieve a single recent call by callId
	*/
	//NOTE: GetRecentCall() is already tested in other unit tests.
	//TEST_METHOD( testGetRecentCall )
	//{
	//	DLLEXPORT RecentCall getRecentCall( int callId );
	//}

    [TestMethod]
    public void testGetRecentCallsByPhoneNumber()
	{
		Int64 contactCount = initPhoneNumbersEx();

		String	origNumber = "+1(760)555-1111";
//		String phoneNumber = DbUtils.getNormalizedNumberNoPrefix( origNumber );		//TODO: unit test works in native code.  Verify this later.

		/**
		* Retrieve a list of recent calls by phoneNumber
		*/
		RecentCallList calls = getDbm().getRecentCalls( origNumber );

		if ( calls.Count > 0 )
		{
			foreach( RecentCall call in calls )
			{
				//TODO: improve test.  Manually verified this works.
//				Assert.AreEqual( call.Number,     phoneNumber, "Number mismatch."      );		//TODO: unit test works in native code.  Verify this later.
				Assert.AreEqual( call.OrigNumber, origNumber,  "OrigNumber mismatch."  );
			}
		}
	}

    [TestMethod]
    public void testGetRecentCallsByCmGroup()
	{
		Int64 contactCount = initPhoneNumbersEx();

		Int32	cmGroup = 1;

		/**
		* Retrieve a list of recent calls by cmGroup
		*/
		RecentCallList calls = getDbm().getRecentCalls( cmGroup );

		if ( calls.Count > 0 )
		{
			foreach( RecentCall call in calls )
			{
				//TODO: improve test.  Manually verified this works.
				Assert.AreEqual( call.CmGroup, cmGroup, "CmGroup mismatch."  );
			}
		}
	}

	[TestMethod]
    public void testRichData()
	{
//		String jsonText = @"{"voxoxrichmessage”: {"type”: ”jpeg”, "data”: {"file”: ”http://pre.my.voxox.com/downloadvoxox/index/serve/?hash=70ba7dda7c33cb20de3fc6de5b83b5ee-1360043989”, "thumbnail”: "http://pre.my.voxox.com/downloadvoxox/index/serve/?hash=2099e7305270c43abe56801e0f0f1ac9-1360043990”}, "body”: "London!”}}";
//		String jsonText = @"{""voxoxrichmessage"": {""type"": ""jpeg"", ""data"": {""file"": ""abc"", ""thumbnail"": ""123""}, ""body"": ""London!""}}";
		String jsonText = "{\"voxoxrichmessage\": {\"type\": \"jpeg\", \"data\": {\"file\": \"abc\", \"thumbnail\": \"123\"}, \"body\": \"London!\"}}";
//		String jsonText = "{\"type\": \"jpeg\", \"data\": {\"file\": \"abc\", \"thumbnail\": \"123\"}, \"body\": \"London!\"}";

		RichData temp = RichData.fromJsonText( jsonText );

		int xxx = 1;
		xxx++;
	}

}   // class ManagedDbManagerTest

}   //namespace ManagedDbManager.Test
