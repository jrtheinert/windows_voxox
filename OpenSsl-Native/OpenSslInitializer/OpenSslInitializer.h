// OpenSslInitializer.h

#pragma once

#include "DllExport.h"
#include <string>

#ifdef __cplusplus
extern "C"
{
	DLLEXPORT int openssl_init( bool compression );
	DLLEXPORT int openssl_terminate();
}
#endif /*__cplusplus*/

namespace VoxoxOpenSsl 
{

	class OpenSslInitializer
	{
	public:
		static DLLEXPORT int init( bool compression );
		static DLLEXPORT int terminate();

	private:
		OpenSslInitializer()			{}

		~OpenSslInitializer();	// throw();

		int initPrivate( bool compression );
		void cleanUp();

		//Some helper methods to keep main code cleaner.
		void logSettings();
		void logCompressionInfo( bool compression );

		void logInfo   ( const std::string& msgIn );
		void logWarning( const std::string& msgIn );

	private:
		static bool					s_CRYPTO_set_mem_functions_done;
		static OpenSslInitializer*	s_instance;
	};

}	//VoxoxOpenSsl
