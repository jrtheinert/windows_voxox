/*****************************************************************************************************/
/* Based on:
/*
/* TLS.cpp                                                                                           */
/* Declaration of TLS Worker and TLS Factory function                                                */
/* (c) 2010-2011 Telurix LLC                                                                         */
/*                                                                                                   */
/*****************************************************************************************************/

#include <assert.h>

#ifdef _WIN32
#   define OPENSSL_NO_ENGINE
#endif

#include "OpenSslInitializer.h"

#include <string>

#include <openssl/err.h>
#include <openssl/ssl.h>

#ifndef OPENSSL_NO_ENGINE
#   include <openssl/engine.h>
#endif

#if OPENSSL_VERSION_NUMBER < 0x00907000L
#warning ""
#warning "=============================================================="
#warning "Your version of OpenSSL is < 0.9.7."
#warning " Upgrade for better compatibility, features and security fixes!"
#warning "============================================================="
#warning ""
#endif 

#ifndef OPENSSL_NO_COMP
#   define TLS_COMP_SUPPORT
#   define zlib_cleanup()
#else
#   undef TLS_COMP_SUPPORT
#   define zlib_cleanup() COMP_zlib_cleanup()
#endif

#ifndef OPENSSL_NO_KRB5
#   define TLS_KERBEROS_SUPPORT
#else
#   undef TLS_KERBEROS_SUPPORT
#endif

const static std::string g_thisClass = "VoxoxOpenSsl";

//-----------------------------------------------------------------------------
//	C interface 
int openssl_init( bool compression )
{
	return VoxoxOpenSsl::OpenSslInitializer::init( compression );
}

//-----------------------------------------------------------------------------

int openssl_terminate()
{
	return VoxoxOpenSsl::OpenSslInitializer::terminate();
}

//-----------------------------------------------------------------------------


//----------------------------------------------------------------------------------------------------------------
//Additionally, based on code from: https://www.safaribooksonline.com/library/view/network-security-with/059600270X/ch04.html
//----------------------------------------------------------------------------------------------------------------

#define MUTEX_TYPE		 HANDLE
#define MUTEX_SETUP(x)   (x) = CreateMutex( nullptr, FALSE, nullptr )
#define MUTEX_CLEANUP(x) CloseHandle(x)
#define MUTEX_LOCK(x)    WaitForSingleObject((x), INFINITE)
#define MUTEX_UNLOCK(x)  ReleaseMutex(x)
#define THREAD_ID        GetCurrentThreadId()

// "dynamic" locks
struct CRYPTO_dynlock_value
{
	MUTEX_TYPE mutex;
};

// --------------- Once per module init of class statics ------------------------
bool								VoxoxOpenSsl::OpenSslInitializer::s_CRYPTO_set_mem_functions_done = false;
VoxoxOpenSsl::OpenSslInitializer*	VoxoxOpenSsl::OpenSslInitializer::s_instance					  = nullptr;

namespace OpenSslInit
{

static MUTEX_TYPE*	s_locks		= nullptr;
static size_t		s_locksSize = 0;

// --------------- TLS Locks ------------------------------------------------------
static struct CRYPTO_dynlock_value* dynlockCreate( const char* , int ) throw()
{
	CRYPTO_dynlock_value* lock = static_cast<CRYPTO_dynlock_value*>(OPENSSL_malloc(sizeof(CRYPTO_dynlock_value)));

	if ( lock  )
	{
		try
		{
//			new (static_cast<void*>(lock)) CRYPTO_dynlock_value;
			MUTEX_SETUP( lock->mutex );
		}

		catch(... )		//TODO
		{
			OPENSSL_free(lock);
			return 0;
		}
	}

	return lock;
}

//-----------------------------------------------------------------------------

static void dynlockFunc( int mode, CRYPTO_dynlock_value* lock, const char* , int ) throw()
{
	if ( lock )
	{
		if ((mode & CRYPTO_LOCK) != 0)
		{
			MUTEX_LOCK( lock->mutex );
		}
		else
		{
			MUTEX_UNLOCK( lock->mutex );
		}
	}
}

//-----------------------------------------------------------------------------

static void dynlockDestroy( CRYPTO_dynlock_value* lock, const char* , int ) throw()
{
    if ( lock != nullptr )
	{
//	    lock->~CRYPTO_dynlock_value();	//TODO: research this.
		MUTEX_CLEANUP( lock->mutex );
		OPENSSL_free( lock );
	}
}

//-----------------------------------------------------------------------------
// normal (static?) locking callback
static void lockingFunc( int mode, int index, const char* , int ) throw()
{
    if ( index < 0 || (size_t)index >= s_locksSize)
        abort();
    
	if (mode & CRYPTO_LOCK)
	{
		MUTEX_LOCK( &s_locks[index] );
	}
    else
	{
		MUTEX_UNLOCK( &s_locks[index] );
	}
}

//-----------------------------------------------------------------------------

static unsigned long id_function(void)
{
    return ((unsigned long)THREAD_ID);
}

//-----------------------------------------------------------------------------

bool createLocks( int locksSize )
{
	bool result = false;

	s_locks = new MUTEX_TYPE[locksSize];

	if ( s_locks )
	{
		s_locksSize = (size_t)locksSize;
		
		for ( size_t x = 0 ; x < s_locksSize ; x++ )
		{
			MUTEX_SETUP( s_locks[ x ] );
		}

		result = true;
	}
	else
	{
		s_locksSize = 0;
	}

	return result;
}

//-----------------------------------------------------------------------------

void destroyLocks()
{
    if (s_locks)
    {
        delete[] s_locks;
        s_locks = 0;
    }

	s_locksSize = 0;
}

}	//namespace OpenSslInit



// --------------- Once per module init -------------------------------------

namespace VoxoxOpenSsl
{

OpenSslInitializer::~OpenSslInitializer()	// throw()
{
	cleanUp();
}

//-----------------------------------------------------------------------------

int OpenSslInitializer::init( bool compression )
{
	if ( s_instance == nullptr )
	{
		s_instance = new OpenSslInitializer;

		if ( s_instance )
		{
			s_instance->initPrivate( compression );
		}
	}

	return ( s_instance ? 0 : 1 );
}

//-----------------------------------------------------------------------------

void OpenSslInitializer::cleanUp()
{
    EVP_cleanup();

#ifndef OPENSSL_NO_ENGINE
    ENGINE_cleanup();
#endif
    CRYPTO_cleanup_all_ex_data();

#if OPENSSL_VERSION_NUMBER < 0x01000000L
    ERR_remove_state(0);
#else
    ERR_remove_thread_state(0);
#endif

    ERR_free_strings();
    zlib_cleanup();

	OpenSslInit::destroyLocks();

	//Clear static lock callbacks
	CRYPTO_set_id_callback     ( nullptr );
	CRYPTO_set_locking_callback( nullptr );

	//Cleear dynamic lock callbacks
	CRYPTO_set_dynlock_create_callback ( nullptr );
    CRYPTO_set_dynlock_lock_callback   ( nullptr );
    CRYPTO_set_dynlock_destroy_callback( nullptr );

    s_instance  = nullptr;
}

//-----------------------------------------------------------------------------

//VOXOX - JRT - 2013.05.13 - Since we need/want/have global initialization, we should have matching global termination.
//	Use of refCount() does not work well when multiple threads, potentially from multiple libraries are using the
//	various OpenSSL callback functions.
int OpenSslInitializer::terminate()
{
	if ( s_instance )
	{
		delete s_instance;
		s_instance = nullptr;
	}
	
	return 0;
}

//-----------------------------------------------------------------------------

int OpenSslInitializer::initPrivate( bool compression )
{
    long version = SSLeay();
	long version2 = OPENSSL_VERSION_NUMBER;

    //Check if version have the same major minor and fix level (e.g. 0.9.8a & 0.9.8c are ok, but 0.9.8 and 0.9.9x are not)
    if ((version>>8) != (OPENSSL_VERSION_NUMBER>>8))
    {
		std::string msg = g_thisClass + ": Installed openssl library (0x" + std::to_string( version ) + ") is too different from the linked library (0x" + std::to_string( OPENSSL_VERSION_NUMBER) + ").";
		logWarning( msg );
    }

	logSettings();

	//Per OpenSSL mem.c file:
	//	- We provide flexible functions for exchanging memory-related functions at run-time, 
	//		but this must be done before any blocks are actually allocated; 
	//		or we'll run into huge problems when malloc/free pairs don't match etc.
	//
	//Internally, OpenSSL sets 'allow_customize' to 0 after first allocation, so 
	//	CRYPTO_set_mem_functions() fails when called again.
	//
	//Let's ensure we only call this once.
	if ( ! s_CRYPTO_set_mem_functions_done )
	{
		int temp = CRYPTO_set_mem_functions(malloc, realloc, free);	//VOXOX - JRT - 2013.05.13 - Fails (0), second time thru.?
//		if (CRYPTO_set_mem_functions(malloc, realloc, free) != 1)	//VOXOX - JRT - 2013.05.13 - Fails (0), second time thru.?
		if ( temp != 1 )
		{
			std::string msg = g_thisClass + ": Unable to set the memory allocation functions.";
			logWarning( msg );
			return 1;
		}

		s_CRYPTO_set_mem_functions_done = true;
	}

    // init "static" tls locks
    int locksSize = CRYPTO_num_locks();

    if (locksSize < 0 || locksSize > 512)
	{
		std::string msg = g_thisClass + ": Invalid CRYPTO_num_locks: " + std::to_string( locksSize );
		logWarning( msg );
		return 1;
	}

    if ( locksSize )
    {
		if ( !OpenSslInit::createLocks( locksSize ) )
		{
			std::string msg = g_thisClass + ": Cannot allocate static locks";
			logWarning( msg );
			return 1;
		}

		CRYPTO_set_id_callback     ( OpenSslInit::id_function );
		CRYPTO_set_locking_callback( OpenSslInit::lockingFunc );
    }
	//End static lock setup

    // set "dynamic" locks callbacks
	CRYPTO_set_dynlock_create_callback ( OpenSslInit::dynlockCreate  );
    CRYPTO_set_dynlock_lock_callback   ( OpenSslInit::dynlockFunc    );
    CRYPTO_set_dynlock_destroy_callback( OpenSslInit::dynlockDestroy );
	//End Dynamic lock setup

	logCompressionInfo( compression );

	//Now init OpenSSL.
    SSL_library_init();
    SSL_load_error_strings();
#ifndef OPENSSL_NO_ENGINE
    ENGINE_load_builtin_engines();
#endif

	return 0;
}

//---------------------------------------------------------------------------------------

void OpenSslInitializer::logSettings()
{
    long version = SSLeay();

#ifdef TLS_KERBEROS_SUPPORT
    bool kerberosSupport = true;
#else
    bool kerberosSupport = false;
#endif

#ifdef TLS_COMP_SUPPORT
    bool compSupport = true;
#else
    bool compSupport = false;
#endif

    // attempt to guess if the library was compiled with kerberos or compression
    // support from the cflags
    const char* cflags		= SSLeay_version(SSLEAY_CFLAGS);
    bool		unknown     = false;
    bool		libKerberos = false;
    bool		libZlib     = false;

    if (cflags==0 || strstr(cflags, "not available") != 0)
	{
        unknown = true;
	}
    else
    {
        if (strstr(cflags, "-DKRB5_") != 0)
            libKerberos = true;

        if (strstr(cflags, "-DZLIB") != 0)
            libZlib = true;
    }

	std::string logMsg;

	logMsg.clear();
	logMsg += g_thisClass + ": Compiled with openssl version ";
	logMsg += OPENSSL_VERSION_TEXT;
	logMsg += " (0x" + std::to_string( OPENSSL_VERSION_NUMBER );
	logMsg += "), kerberos support: ";
	logMsg += (kerberosSupport ? "on" : "off");
	logMsg += ", compression: ";
	logMsg += (compSupport ? "on" : "off");

	logInfo( logMsg );
	
	logMsg.clear();
	logMsg += g_thisClass + ": Installed openssl version ";
	logMsg += SSLeay_version(SSLEAY_VERSION);
	logMsg += " (0x" + std::to_string( version );
	logMsg += "), kerberos support: ";
	logMsg += (unknown ? "unknown" :(libKerberos ? "on" : "off"));
	logMsg += ", compression: ";
	logMsg += (unknown ? "unknown" :(libZlib ? "on" : "off"));
	logMsg += ", cflags: ";
	logMsg += cflags;

	logInfo( logMsg );

    if (!unknown)
    {
        if (libKerberos != kerberosSupport)
		{
			std::string msg;
			msg += g_thisClass + ": Openssl kerberos support mismatch: compiled kerberos: " ;
			msg += (kerberosSupport ? "on" : "off");
			msg += " library kerberos: " ;
			msg += (libKerberos ? "on" : "off");

			logInfo( msg );
		}
    }
}

//---------------------------------------------------------------------------------------

void OpenSslInitializer::logCompressionInfo( bool compression )
{
#if (OPENSSL_VERSION_NUMBER >= 0x00908000L)
    STACK_OF(SSL_COMP)* compMethods = SSL_COMP_get_compression_methods();

    if (compMethods == 0)
	{
		std::string msg = g_thisClass + ": Compression support is disabled in openssl.";
		logInfo( msg );
	}
    else if (!compression)
    {
		std::string msg = g_thisClass + ": Disabling compression.";
		logInfo( msg );
        sk_SSL_COMP_zero(compMethods);
    }
#endif
}

//---------------------------------------------------------------------------------------

void OpenSslInitializer::logInfo( const std::string& msgIn )
{
	//LOG_INFO( msgIn );		//TODO-LOG

	std::string msg = "INFO: " + msgIn + "\n";
	::OutputDebugStringA( msg.c_str() );
}

//---------------------------------------------------------------------------------------

void OpenSslInitializer::logWarning( const std::string& msgIn )
{
	//LOG_WARN ( msgIn );		//TODO-LOG

	std::string msg = "WARNING: " + msgIn + "\n";
	::OutputDebugStringA( msg.c_str() );
}

//-----------------------------------------------------------------------------

}	//namespace VoxoxOpenSsl
