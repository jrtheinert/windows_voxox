#include "stdafx.h"
#include "CppUnitTest.h"

#include "../OpenSslInitializer/OpenSslInitializer.h"
#include "Windows.h"	//For Sleep()

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace OpenSslUnitTests
{		
	TEST_CLASS(UnitTest1)
	{
	public:
		
		TEST_METHOD(TestInitAndTerminate)
		{
			bool compression = false;

			VoxoxOpenSsl::OpenSslInitializer::init( compression );

			Sleep( 2000 );

			VoxoxOpenSsl::OpenSslInitializer::terminate();
		}

	};
}