Jeff Theinert
2014.11.24

There are a couple existing C# libs that wrap OpenSSL.  

We have chosen not to use the for the following reasons:
 - They tend to be a couple versions of OpenSSL behind.

 - They appear to NOT be built with VS 2013, so we would
   have to include other VS version redistributables with
   our app, needlessly increasing download size.

 - We add extra logging so we confirm that the OpenSSL
   binaries were built as we want them.

 - We do not really use OpenSSL directly from C# since
   it is used in our native SIP and XMPP stacks, so we
   do not currently need much more than the initialization
   and termination functions.

Current version is OpenSSL 1.0.1i.

Some of the output from this project MUST be copied to the
managed OpenSSL project, so not all project developers need
to maintain OpenSSL.  These files are:
 - OpenSslInitializer.dll
 - libeay32.dll (OpenSSL binary)
 - ssleay32.dll (OpenSSL binary)

When these files are rebuilt/updated, they should be copied to
 Telcentris.Voxox.Dekstop/bin and/or 
 Telcentris.Voxox.Dekstop/bin/release (or /debug)
 as needed.



