﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Oct 2014
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Telcentris.Voxox.ManagedXmppApi.ManagedXmppDataTypes
{
    
//=============================================================================

public class ChatMsg
{
	//NOTE: These come from XMPP stack so do NOT change them, except to match changes in XMPP stack.
	public enum ChatMsgType
	{ 
		None				= 0,
		DeliveredReceipt	= 1,	
		ReadReceipt			= 2,
		ChatState			= 3,
		Msg					= 4,	//Implies ChatState
		Carbon				= 5,	//Implies Msg
	};

	public ChatMsg()
	{
	}

	//Properties ---------------------------------------------------------------------
	public	String		ToJid				{ get; set;     }    //Required for unmanaged class
	public	String		FromJid				{ get; set;     }    //Required for unmanaged class
	public	String		MsgId				{ get; set;     }    //Required for unmanaged class
	public	String		Body				{ get; set;     }    //Required for unmanaged class
	public	String		ChatState			{ get; set;     }    //Required for unmanaged class
	public	String		FromResource		{ get; set;     }    //Required for unmanaged class
	public	String		MyResource			{ get; set;     }    //Required for unmanaged class	//For use in handling Carbon Messages.
	public	String		ReceiptMsgId		{ get; set;     }    //Required for unmanaged class	//For use in handling Carbon Messages.
	public	ChatMsgType Type				{ get; set;     }    //Required for unmanaged class	//For use in handling Carbon Messages.
	
	//Misc --------------------------------------------------------------------

	public Boolean isDeliveredReceipt()			{ return Type == ChatMsgType.DeliveredReceipt;	}
	public Boolean isReadReceipt()				{ return Type == ChatMsgType.ReadReceipt;		}
	public Boolean isChatState()				{ return Type == ChatMsgType.ChatState;			}
	public Boolean isMsg()						{ return Type == ChatMsgType.Msg;				}
	public Boolean isCarbon()					{ return Type == ChatMsgType.Carbon;			}

	public String toString()
	{
		String result = "";

		result += "To: "			+ ToJid			+ ", ";
		result += "From: "			+ FromJid		+ ", ";
		result += "MsgId: "			+ MsgId			+ ", ";
		result += "Body: "			+ Body			+ ", ";
		result += "ChatState: "		+ ChatState		+ ", ";
		result += "Resource: "		+ FromResource	+ ", ";
		result += "MyResource: "	+ MyResource	+ ", ";
		result += "ReceiptMsgId: "	+ ReceiptMsgId	+ ", ";
		result += "Type: "			+ MyResource;

		return result;
	}
};

//=============================================================================


//=============================================================================

public class LogEntry
{
	public enum LogLevel 
	{
		Debug	= 0,
		Info	= 1,
		Warn	= 2,
		Error	= 3,
		Fatal	= 4
	};

	public LogEntry()
	{
	}

	//Properties ---------------------------------------------------------------------
	public	LogLevel		Level				{ get; set;     }    //Required for unmanaged class
	public	String			Component			{ get; set;     }    //Required for unmanaged class
	public	String			ClassName			{ get; set;     }    //Required for unmanaged class
	public	String			Message				{ get; set;     }    //Required for unmanaged class
	public	String			FileName			{ get; set;     }    //Required for unmanaged class
	public	Int32			LineNumber			{ get; set;     }    //Required for unmanaged class
	public	UInt64			ThreadId			{ get; set;     }    //Required for unmanaged class
//	public	Time			Time				{ get; set;     }    //Required for unmanaged class	//TODO convert from std::tm to C# equivalent

	//Methods
	Boolean	 hasFileName()
	{ 
		return !String.IsNullOrEmpty( FileName );	
	}

//	LogEntry operator=( const LogEntry& src );
};

//=============================================================================


//=============================================================================

public class PresenceUpdate
{
	//These are copied from native code.  DO NOT change them.
	public enum ShowType 
	{
		None     = 0,
		Offline  = 1,
		XA       = 2,
		Away     = 3,
		Dnd      = 4,
		Online   = 5,
		Chat     = 6,
	};

	public PresenceUpdate()
	{
	}

	//Properties ---------------------------------------------------------------------
	public	String		FromJid				{ get; set;     }    //Required for unmanaged class
	public	String		Status				{ get; set;     }    //Required for unmanaged class
	public	String		Nickname			{ get; set;     }    //Required for unmanaged class
	public	String		TimeSent			{ get; set;     }    //Required for unmanaged class
	public	Int32		Priority			{ get; set;     }    //Required for unmanaged class
	public	ShowType	Show				{ get; set;     }    //Required for unmanaged class
	public	Boolean		Available			{ get; set;     }    //Required for unmanaged class


	//Misc -----------------------------------------------------------------------
//	PresenceUpdate& operator=( const PresenceUpdate& src );

	public String toString()
	{
		String result = "";;

		result += "From: "		+ FromJid  + ", ";
		result += "Status: "	+ Status   + ", ";
		result += "NickName: "	+ Nickname + ", ";
		result += "TimeSent: "	+ TimeSent + ", ";
		result += "Priority: "	+ Priority.ToString() + ", ";
		result += "Show: "		+ Show.ToString()     + ", ";
		result += "Available: " + ( Available ? "True" : "False" );

		return result;
	}

};

//=============================================================================
	
public class XmppParameters
{
	//These come from native code.  DO NOT change them
	public enum TlsOptionType 
	{
		TLS_DISABLED,
		TLS_ENABLED,
		TLS_REQUIRED
	};

	//These come from native code.  DO NOT change them
	public enum AuthMechType
	{
		AUTH_MECH_NONE		  = 0,
		AUTH_MECH_PLAIN		  = 1,
//		AUTH_MECH_DIGEST_MD5  = 2,	//Does not work in LibJingle
		AUTH_MECH_SCRAM_SHA_1 = 3,
		AUTH_MECH_OAUTH2	  = 4,
	};

	public XmppParameters()
	{
		TlsOption			= TlsOptionType.TLS_DISABLED;
		AuthMechanism		= AuthMechType.AUTH_MECH_NONE;
		AllowPlainPassword	= false;
		Port				= 0;
		InitialShow			= PresenceUpdate.ShowType.None;
	}

	//Misc --------------------------------------------------------------------
//	XmppParameters& operator=( const XmppParameters& src );
	public String	toString()
	{
		String result = "";;

		result += "UserId: "			 + UserId					+ ", ";
		result += "Password: "			 + "<not displayed>"		+ ", ";
		result += "Status: "			 + Status					+ ", ";
		result += "InitialShow: "		 + InitialShow.ToString()	+ ", ";

		result += "Domain: "			 + Domain							+ ", ";
		result += "Server: "			 + Server							+ ", ";
		result += "ResourcePrefix: "	 + ResourcePrefix					+ ", ";
		result += "Port: "				 + Port.ToString()					+ ", ";
		result += "AllowPlainPassword: " + AllowPlainPassword.ToString()	+ ", ";
		result += "TlsOption: "			 + TlsOption.ToString()				+ ", ";
		result += "AuthMechanism: "		 + AuthMechanism					+ ", ";

		result += "UseProxy: "			 + UseProxy.ToString()				+ ", ";
		result += "Proxy Server: "		 + ProxyServer						+ ", ";
		result += "Proxy Port: "		 + ProxyPort.ToString()				+ ", ";

		return result;
	}

	//Properties ---------------------------------------------------------------------
	//User
	public String					UserId					{ get; set;     }    //Required for unmanaged class
	public String					Password				{ get; set;     }    //Required for unmanaged class
	public String					Status					{ get; set;     }    //Required for unmanaged class
	public PresenceUpdate.ShowType	InitialShow				{ get; set;     }    //Required for unmanaged class

	//Connection
	public String					Domain					{ get; set;     }    //Required for unmanaged class
	public String					Server					{ get; set;     }    //Required for unmanaged class
	public String					ResourcePrefix			{ get; set;     }    //Required for unmanaged class
	public Int32					Port					{ get; set;     }    //Required for unmanaged class
	public Boolean					AllowPlainPassword		{ get; set;     }    //Required for unmanaged class
	public TlsOptionType			TlsOption				{ get; set;     }    //Required for unmanaged class
	public AuthMechType				AuthMechanism			{ get; set;     }    //Required for unmanaged class

	public Boolean					UseProxy				{ get; set;		}
	public String					ProxyServer				{ get; set;		}
	public Int32					ProxyPort				{ get; set;		}

};


//=============================================================================
// XmppEvent class
//	- Event type and related data for XMPP events handled in UI layer.
//	- The data elements we get from the specific events in AppEventHandler
//		will be used to create this and pass to managed code.
//	- TODO: Consider just using this one class in unmanaged code as well.
//=============================================================================

public class XmppEventData
{
	//Which type of event occurred.  Allow listeners to select events they care about.
	//DO NOT REORDER these are based on native code
	public	enum EventType		
	{
		Unknown			= 0,	//So we can test that all events set type properly
		SignOn			= 1,
		SignOff			= 2,
		Disconnect		= 3,	//May not use.
		PresenceChange  = 4,
		IncomingMsg		= 5,
		Log				= 6,
	};


	public XmppEventData()	
	{
		Type = EventType.Unknown;
	}

	public  XmppEventData( EventType eventType)
	{
		Type = eventType;
	}

	//Properties ---------------------------------------------------------------------
	public	EventType		Type				{ get; set;     }    //Required for unmanaged class
	public	PresenceUpdate	PresenceUpdate		{ get; set;     }    //Required for unmanaged class
	public	ChatMsg			ChatMsg				{ get; set;     }    //Required for unmanaged class
	public	LogEntry		LogEntry			{ get; set;     }    //Required for unmanaged class

	//Disconnect Event extras
//	public	Boolean			ConnectionError			{ get; set;     }    //Required for unmanaged class
//	public	Strng			DisconnectMsg			{ get; set;     }    //Required for unmanaged class;


    //Convenience methods -----------------------------------------------------------------------------------------
	public	Boolean	isSignOnEvent()						{ return Type == EventType.SignOn;			}
	public	Boolean	isSignOffEvent()					{ return Type == EventType.SignOff;			}
	public	Boolean	isDisconnectEvent()					{ return Type == EventType.Disconnect;		}
	public	Boolean	isPresenceChangeEvent()				{ return Type == EventType.PresenceChange;	}
	public	Boolean	isIncomingMsgEvent()				{ return Type == EventType.IncomingMsg;		}
	public	Boolean	isLogEvent()						{ return Type == EventType.Log;				}
};

//=============================================================================

}	//namespace ManagedXmppDataTypes

