﻿
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Oct 2014
 */

#define USE_NEW_SERVER

using System;
using System.Diagnostics;		//For Debug
using System.Threading;		//For Sleep

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Telcentris.Voxox.ManagedXmppApi;
using Telcentris.Voxox.ManagedXmppApi.ManagedXmppDataTypes;


namespace ManagedXmppApi.Test
{

public class TestXmppEventHandler
{ 
	public TestXmppEventHandler( String name ) 
	{
		mName = name;
	}

	public void HandleEvent( Object sender, XmppEventArgs e )		//Same signature as XmppEventHandler defined in ManagedXmppApi.h
	{
		Console.WriteLine( "In HandleEvent() for instance name: " + mName );

		if ( e.Data.isSignOnEvent() )
		{
			Debug.WriteLine( ">>>>> XmppEvent: SignOn" );
		}
		else if ( e.Data.isSignOffEvent() )
		{
			Debug.WriteLine( ">>>>> XmppEvent: SignOff" );
		}
		else if ( e.Data.isDisconnectEvent() )
		{
			Debug.WriteLine( ">>>>> XmppEvent: Disconnect:  NOT IMPLEMEMENTED" );
		}
		else if ( e.Data.isPresenceChangeEvent() )
		{
			String msg = ">>>>> XmppEvent: Presence Update: " + e.Data.PresenceUpdate.toString();
			Debug.WriteLine( msg );
		}
		else if ( e.Data.isIncomingMsgEvent() )
		{
			String msg = ">>>>> XmppEvent: IncomingMsg: " + e.Data.ChatMsg.toString();
			Debug.WriteLine( msg );
		}
		else if ( e.Data.isLogEvent() )
		{
			String msg = ">>>>> " + e.Data.LogEntry.Message;
			Debug.WriteLine(  msg );
		}
		else
		{
			String msg = ">>>>> Unknown Event Type: " + e.Data.Type.ToString();	//New event type?
			Debug.WriteLine( msg );
		}
	}

	public String getName()
	{
		return mName;
	}

	private String mName;
};

[TestClass]
public class ManagedXmppApiUnitTest
{
#if USE_NEW_SERVER
	//User
	String	mUserId		= "7313";		//SSO companyUserId
	String	mPassword	= "jrtjrt";		//User login password

	//Connection
	String	mServer		= "qa.vx-jabber.voxox.com";

	//Test contact
	String	mToJid		= "7327@voxox.com";
#else
	//User
	String	mUserId		= "jrtheinert";	//Old Voxox account
	String	mPassword	= "jrtjrt1";	//User login password

	//Connection
	String	mServer		= "voxox.com";

	//Test contact
	String	mToJid		= "jrttest7b@voxox.com";
#endif


	//Connection
	String							mDomain				= "voxox.com";
	String							mResourcePrefix		= "voxox3.0";
	Int32							mPort				= 5222;
	XmppParameters.TlsOptionType	mTlsOption			= XmppParameters.TlsOptionType.TLS_DISABLED;
	XmppParameters.AuthMechType		mAuthMechanism		= XmppParameters.AuthMechType.AUTH_MECH_PLAIN;
	bool							mAllowPlainPassword	= true;

	XmppParameters mXmppParameters = new XmppParameters();

	XmppApi mXmppApi = null;

	XmppApi getApi()
	{
		if ( mXmppApi == null )
		{
			mXmppApi = XmppApi.getInstance();
		}

		return mXmppApi;
	}

	void initApi()
	{
		getApi();

		addEventHandlers();
	}

    void addEventHandlers()
    {
	    TestXmppEventHandler test1 = new TestXmppEventHandler( "Jeff" );
	    XmppEventHandler xmppDel1 = (XmppEventHandler)System.Delegate.CreateDelegate( typeof(XmppEventHandler), test1, "HandleEvent" );
	    getApi().addEventListener( xmppDel1 );

		//Uncomment to test with multiple listeners to demonstrate it works.
//	    TestXmppEventHandler test2 = new TestXmppEventHandler( "Pam" );
//	    XmppEventHandler xmppDel2 = (XmppEventHandler)System.Delegate.CreateDelegate( typeof(XmppEventHandler), test2, "HandleEvent" );
//	    getApi().addEventListener( xmppDel2 );
    }

	void wait( int seconds )
	{
		if ( seconds > 0)
		{
			for ( int x = 0; x < seconds; x++ )
			{
				Thread.Sleep( 1000 );
			}
		}
		else
		{
			while ( true )		//Wait indefinitely
			{
				Thread.Sleep( 1000 );
			}
		}
	}

	XmppParameters getXmppParameters()
	{
		if (( String.IsNullOrEmpty( mXmppParameters.UserId ) ) )
		{
			//User
			mXmppParameters.UserId				= mUserId;
			mXmppParameters.Password			= mPassword;

			//Connection
			mXmppParameters.Domain				= mDomain;
			mXmppParameters.Server				= mServer;
			mXmppParameters.ResourcePrefix		= mResourcePrefix;
			mXmppParameters.Port				= mPort;
			mXmppParameters.TlsOption			= mTlsOption;
			mXmppParameters.AuthMechanism		= mAuthMechanism;
			mXmppParameters.AllowPlainPassword	= mAllowPlainPassword;
		}
		return mXmppParameters;
	}


	[TestMethod]
	public void TestLoginAndDisconnect()
	{
		initApi();

		String status1 = "HI!";
		String status2 = "BRB!";

		//Get default parameters and add some local values
		XmppParameters xmppParameters = getXmppParameters();

		xmppParameters.Status	   = status1;
		xmppParameters.InitialShow = PresenceUpdate.ShowType.Online;

		getApi().login( xmppParameters );

		wait( 3 );
	
		getApi().updateMyStatus( PresenceUpdate.ShowType.Away, status2 );

		wait( 3 );

//		getApi().requestVCard( toJid );			//TODO: Does not work yet.
//
//		wait( 3 );

		getApi().sendChatState( mToJid, true );

		wait( 3 );

		getApi().sendChatState( mToJid, false );

		wait( 3 );

		getApi().sendChatState( mToJid, true );

		wait( 3 );

		getApi().sendChat( mToJid, "Test msg 1", "xxxx" );

		wait( 3 );

		getApi().sendChat( mToJid, "Test msg 2", "YYYY" );

		wait( 3 );

//		getApi().disconnect();		//Causes crash TODO
//
			
		wait( 30 );
//		wait( -1 );	//Forever
	}


}	//class ManagedXmppApiUnitTest

}	//namespace ManagedXmppApi.Test


	//void myCallback( const XmppEventData& eventData )
	//{
	//	if		( eventData.isSignOnEvent() )
	//	{
	//		OutputDebugStringA( ">>>> SignOn event.\n");
	//	}
	//	else if ( eventData.isSignOffEvent() )
	//	{
	//		OutputDebugStringA( ">>>> SignOff event.\n");
	//	}
	//	else if ( eventData.isPresenceChangeEvent() )
	//	{
	//		std::string data = eventData.getPresenceUpdate().toString();
	//		std::string msg = ">>>> Presence update event: " + data + "\n";
	//		OutputDebugStringA( msg.c_str() );
	//	}
	//	else if ( eventData.isIncomingMsgEvent() )
	//	{
	//		std::string data = eventData.getChatMsg().toString();
	//		std::string msg = ">>>> Incoming Chat event: " + data + "\n";
	//		OutputDebugStringA(  msg.c_str() );
	//	}
	//	else
	//	{
	//		OutputDebugStringA( ">>>> UNKNOWN XMPP EVENT.\n");
	//	}
	//}


