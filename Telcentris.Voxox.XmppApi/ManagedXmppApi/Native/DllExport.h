/**
* Voxox
* Copyright (C) 2004-2014 Voxox
*/

/**
* Small #def for dllimport /dllexport.  TODO: This will need to be expanded for Mac
*/

#pragma once

#ifdef BUILD_DLL
#define DLLEXPORT __declspec(dllexport)
#else
#define DLLEXPORT __declspec(dllimport)
#endif
