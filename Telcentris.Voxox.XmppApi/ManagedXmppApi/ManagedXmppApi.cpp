
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Oct 2014
 */

#include "stdafx.h"
#include "Conversion.h"

#include "ManagedXmppApi.h"
#include "Native/VoxoxXmppApi.h"

using System::IntPtr;
using namespace System::Runtime::InteropServices;	//For Marshal of callback ptr.

static GCHandle gch;								//To lock the DbChange callback in place.  We may need to revisit this.

//Since the C++/CLI namespace is like the C++ namespace, we need to declare several nested
//	namespaces to get what we want.
namespace Telcentris
{

namespace Voxox
{

namespace ManagedXmppApi 
{

//static 
XmppApi^ XmppApi::getInstance()
{
	return mInstance;
}

XmppApi::XmppApi()
{
	mNativeApi = new VoxoxXmppApi();

	setEventCallback();		//Use default
}

XmppApi::~XmppApi()
{
	delete mNativeApi;
	mNativeApi = nullptr;
}

//-----------------------------------------------------------------------------
//XMPP event related methods
//-----------------------------------------------------------------------------

void XmppApi::setEventCallback()
{
	//Get Callback to VoxoxXmppApi
	mEventDelegate = gcnew XmppEventDelegate( this, &XmppApi::eventCallback );
	gch = GCHandle::Alloc( mEventDelegate );
	IntPtr ip = Marshal::GetFunctionPointerForDelegate( mEventDelegate );
	XmppEventCallbackFunc cb = static_cast<XmppEventCallbackFunc>(ip.ToPointer());
	setEventCallback( cb );
}

void XmppApi::setEventCallback( XmppEventCallbackFunc callback )
{
//	if ( mAppEventHandler != nullptr )
//	{
//		mAppEventHandler->setCallback( callback );
//	}

	//Just use default callback
	mNativeApi->setCallbackFunc( callback );
}

//void XmppApi::setCallbackFunc( XmppEventCallbackFunc value )
//{
//	mNativeApi->setCallbackFunc( value );
//}

void XmppApi::eventCallback( const XmppEventData& dataIn )
{
	ManagedXmppDataTypes::XmppEventData^ data = Conversion::U2M_XmppEventData( dataIn );

	XmppEventArgs^ args = gcnew XmppEventArgs( data );
	onEvent( args );
}

void XmppApi::onEvent( XmppEventArgs^ e )
{
	mEventHandler( this, e );
}

void XmppApi::addEventListener( XmppEventHandler^ eventDelegate )
{
	mEventHandler += eventDelegate;
}

void XmppApi::removeEventListener( XmppEventHandler^ eventDelegate )
{
	mEventHandler -= eventDelegate;
}

//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
//Main XMPP API methods
//-----------------------------------------------------------------------------

void XmppApi::login( ManagedXmppDataTypes::XmppParameters^ paramsIn )
{
	XmppParameters params = Conversion::M2U_XmppParameters( paramsIn );
	mNativeApi->login( params );
}

void XmppApi::logoff()
{
	if ( mNativeApi )
		mNativeApi->logoff();
}

bool XmppApi::isConnected()
{
	return mNativeApi->isConnected();
}

void XmppApi::updateMyStatus( ManagedXmppDataTypes::PresenceUpdate::ShowType showIn, String^ statusIn )
{
	PresenceUpdate::Show show = (PresenceUpdate::Show) showIn;
	std::string status = Conversion::M2U_String( statusIn );

	mNativeApi->updateMyStatus( show, status );
}


//NOTE: I added isConnected() check to next couple methods because we were/are
//		get Access Violation exceptions when network is lost.
//		We see these only in Release builds, but not Debug builds.
//		Additionally, we only see it if SIP is running and the SIP stack
//		detects network loss and closes the SIP connection.
// Since we are currently replacing the SIP stack and the isConnected() check
//	appears to relieve the issue, we can revisit this later if we need to.
bool XmppApi::sendChat( String^ toJidIn, String^ bodyIn, String^ msgIdIn )
{
	bool result = false;

	if ( isConnected() )
	{
		std::string toJid = Conversion::M2U_String( toJidIn );
		std::string body  = Conversion::M2U_String( bodyIn  );
		std::string msgId = Conversion::M2U_String( msgIdIn );

		result = mNativeApi->sendChat( toJid, body, msgId );
	}

	return result;
}

bool XmppApi::sendChatState( String^ toJidIn, bool composing )
{
	bool result = false;

	if ( isConnected() )
	{
		std::string toJid = Conversion::M2U_String( toJidIn );
		result = mNativeApi->sendChatState( toJid, composing );
	}

	return result;
}

bool XmppApi::sendReadReceipt( String^ toJidIn, String^ msgIdIn )
{
	bool result = false;

	if ( isConnected() )
	{
		std::string toJid = Conversion::M2U_String( toJidIn );
		std::string msgId = Conversion::M2U_String( msgIdIn );

		result = mNativeApi->sendReadReceipt( toJid, msgId );
	}

	return result;
}

//May not need these.
//	void XmppApi::setAppEventHandler( AppEventHandler* value )
//{
//}

//	AppEventHandler*	XmppApi::getAppEventHandler()				//Used for Unit Tests
//{
//}

}	//namespace ManagedXmppApi 

}	//namespace Voxox

}	//namespace Telcentris
