
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Oct 2014
 */

#include "stdafx.h"
#include "Conversion.h"

using namespace System;

//Since the C++/CLI namespace is like the C++ namespace, we need to declare several nested
//	namespaces to get what we want.
namespace Telcentris
{

namespace Voxox
{

namespace ManagedXmppApi 
{

Conversion::Conversion()
{
}

//-----------------------------------------------------------------------------
//Static helpers - These may be better in a utility class, but OK here for now.
//-----------------------------------------------------------------------------
std::string Conversion::M2U_String(String^ textIn)
{
	using namespace System::Runtime::InteropServices;

	std::string result;

	if ( textIn != nullptr )
	{
		const char* chars = (const char*)(Marshal::StringToHGlobalAnsi(textIn)).ToPointer();

		result = chars;

		Marshal::FreeHGlobal(IntPtr((void*)chars));
	}

	return result;
}

//static
String^ Conversion::U2M_String( const std::string& textIn )
{
	using namespace System::Runtime::InteropServices;

	String^ result = gcnew String( textIn.c_str() );

	return result;
}

//-----------------------------------------------------------------------------
//Static - XMPP specific methods
//-----------------------------------------------------------------------------
//static 
ManagedXmppDataTypes::ChatMsg^ Conversion::U2M_ChatMsg( const ChatMsg& dataIn )
{
	ManagedXmppDataTypes::ChatMsg^ result = gcnew ManagedXmppDataTypes::ChatMsg;

	ManagedXmppDataTypes::ChatMsg::ChatMsgType msgType = (ManagedXmppDataTypes::ChatMsg::ChatMsgType) dataIn.getType();

	result->ToJid		 = U2M_String( dataIn.getToJid()		);
	result->FromJid		 = U2M_String( dataIn.getFromJid()		);
	result->FromResource = U2M_String( dataIn.getFromResource()	);
	result->MyResource   = U2M_String( dataIn.getMyResource()	);
	result->MsgId		 = U2M_String( dataIn.getMsgId()		);
	result->Body		 = U2M_String( dataIn.getBody()			);
	result->ChatState	 = U2M_String( dataIn.getChatState()	);
	result->ReceiptMsgId = U2M_String( dataIn.getReceiptMsgId()	);
	result->Type		 = msgType;

	return result;
}

//static 
ManagedXmppDataTypes::LogEntry^ Conversion::U2M_LogEntry( const LogEntry& dataIn )
{
	ManagedXmppDataTypes::LogEntry^ result = gcnew ManagedXmppDataTypes::LogEntry;

	ManagedXmppDataTypes::LogEntry::LogLevel level = (ManagedXmppDataTypes::LogEntry::LogLevel) dataIn.getLevel();

	result->ClassName	= U2M_String( dataIn.getClassName() );
	result->Component	= U2M_String( dataIn.getComponent() );
	result->FileName	= U2M_String( dataIn.getFileName()  );
	result->Level		= level;
	result->LineNumber	= dataIn.getLineNumber();
	result->Message		= U2M_String( dataIn.getMessage() );
	result->ThreadId	= dataIn.getThreadId();

	return result;
}

//static
ManagedXmppDataTypes::PresenceUpdate^ Conversion::U2M_PresenceUpdate( const PresenceUpdate& dataIn )
{
	ManagedXmppDataTypes::PresenceUpdate^ result = gcnew ManagedXmppDataTypes::PresenceUpdate;
	
	ManagedXmppDataTypes::PresenceUpdate::ShowType show = (ManagedXmppDataTypes::PresenceUpdate::ShowType) dataIn.getShow();

	result->FromJid	  = U2M_String( dataIn.getFromJid() );
	result->Status	  = U2M_String( dataIn.getStatus() );
	result->TimeSent  = U2M_String( dataIn.getTimeSent() );
	result->Priority  = dataIn.getPriority();
	result->Show	  = show;
	result->Available = dataIn.isAvailable();

	return result;
}

//Static
ManagedXmppDataTypes::XmppEventData^ Conversion::U2M_XmppEventData( const XmppEventData& dataIn )
{
	ManagedXmppDataTypes::XmppEventData^ result = gcnew ManagedXmppDataTypes::XmppEventData;

	ManagedXmppDataTypes::XmppEventData::EventType eventType = (ManagedXmppDataTypes::XmppEventData::EventType) dataIn.getEventType();

	result->Type			= eventType;
	result->PresenceUpdate	= U2M_PresenceUpdate( dataIn.getPresenceUpdate() );
	result->ChatMsg			= U2M_ChatMsg       ( dataIn.getChatMsg()		 );
	result->LogEntry		= U2M_LogEntry		( dataIn.getLogEntry()		 );

	return result;
}

//static
XmppParameters	Conversion::M2U_XmppParameters( ManagedXmppDataTypes::XmppParameters^ dataIn )
{
	XmppParameters dataOut;

	//Simple recasts, for clarity
	XmppParameters::AuthMechanism authMech  = (XmppParameters::AuthMechanism) dataIn->AuthMechanism;
	XmppParameters::TlsOption	  tlsOption = (XmppParameters::TlsOption    ) dataIn->TlsOption;
	PresenceUpdate::Show		  show      = (PresenceUpdate::Show         ) dataIn->InitialShow;

	dataOut.setAllowPlainPassword( dataIn->AllowPlainPassword );
	dataOut.setAuthMechanism     ( authMech	  )	;
	dataOut.setDomain			 ( M2U_String( dataIn->Domain ) );
	dataOut.setPassword			 ( M2U_String( dataIn->Password ) );
	dataOut.setPort				 ( dataIn->Port );
	dataOut.setResourcePrefix    ( M2U_String( dataIn->ResourcePrefix ) );
	dataOut.setServer            ( M2U_String( dataIn->Server		  ) );
	dataOut.setInitialShow		 ( show );
	dataOut.setStatus			 ( M2U_String( dataIn->Status		  ) );
	dataOut.setTlsOption		 ( tlsOption );
	dataOut.setUserId			 ( M2U_String( dataIn->UserId		  ) );

	dataOut.setUseProxy			 ( dataIn->UseProxy );
	dataOut.setProxyServer		 ( M2U_String( dataIn->ProxyServer	  ) );
	dataOut.setProxyPort		 ( dataIn->ProxyPort );

	return dataOut;
}


}	//namespace ManagedXmppApi 

}	//namespace Voxox

}	//Telcentris
