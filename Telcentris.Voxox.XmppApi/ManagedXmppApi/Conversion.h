
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Oct 2014
 */

#pragma once

#include "Native/DataTypes.h"

#include <string>

using System::String;

//Since the C++/CLI namespace is like the C++ namespace, we need to declare several nested
//	namespaces to get what we want.
namespace Telcentris
{

namespace Voxox
{

namespace ManagedXmppApi 
{


ref class Conversion
{
public:
	//Helper methods to convert data.
	static std::string								M2U_String( String^            textIn );
	static String^									U2M_String( const std::string& textIn );

	//Class conversion methods
	static ManagedXmppDataTypes::ChatMsg^			U2M_ChatMsg       ( const ChatMsg&        dataIn );
	static ManagedXmppDataTypes::LogEntry^			U2M_LogEntry	  ( const LogEntry&		  dataIn );
	static ManagedXmppDataTypes::PresenceUpdate^	U2M_PresenceUpdate( const PresenceUpdate& dataIn );
	static ManagedXmppDataTypes::XmppEventData^		U2M_XmppEventData ( const XmppEventData&  dataIn );

	static XmppParameters							M2U_XmppParameters( ManagedXmppDataTypes::XmppParameters^ dataIn );


private:
	Conversion();
};


}	//namespace ManagedXmppApi 

}	//namespace Voxox

}	//Telcentris

