
/* Copyright (C) Telcentris Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 * Written by J R Theinert, jeff.theinert@voxox.com, Oct 2014
 */


#pragma once

#include "Native/DataTypes.h"		//Mostly for XmppEventCallbackFunc

using System::EventArgs;
using System::Object;
using System::String;
using System::Runtime::InteropServices::UnmanagedFunctionPointerAttribute;
using System::Runtime::InteropServices::CallingConvention;

//using ManagedDataTypes::xxx;	//We INTENTIONALLY do not use the 'using ManagedDataTypes' so we are forced to distinguish between managed and unmanaged classes.

class VoxoxXmppApi;	//Fwd declaration
class XmppEventData;

//Since the C++/CLI namespace is like the C++ namespace, we need to declare several nested
//	namespaces to get what we want.
namespace Telcentris
{

namespace Voxox
{

namespace ManagedXmppApi 
{


//=============================================================================

public ref class XmppEventArgs : EventArgs
{
public:
	XmppEventArgs( ManagedXmppDataTypes::XmppEventData^ data )
	{
		mData = data;
	}

	property ManagedXmppDataTypes::XmppEventData^ Data
	{
		ManagedXmppDataTypes::XmppEventData^ get()	{ return mData; }
	}

private:
	ManagedXmppDataTypes::XmppEventData^ mData;
};

//Set proper calling convention for delegate used as callback to avoid stack corruption.
[UnmanagedFunctionPointerAttribute( CallingConvention::Cdecl)]	
delegate void XmppEventDelegate( const XmppEventData& );

public delegate void XmppEventHandler( Object^ sender, XmppEventArgs^ e );

//=============================================================================

//NOTEs about AppEventHandler:
//	MyAppEventHandler, derived from AppEventHandler is implemented as default in XMPP API.  
//	Override if you need to, but other code will need to change.
//	MyAppEventHandler needs you to supply the XmppEventCallbackFunc.
//	Additionally, the UI will provide a XmppEventCallbackFunc, which you should wrap in 
//	the local XmppEventCallbackFunc and pass the data thru.
//
//	Our local XmppEventCallbackFunc:
//	  - catch the native XmppEventData
//	  - marshal that to managed XmppEventData
//	  - call UI XmppEventCallbackFunc with marshalled data.
//

public ref class XmppApi
{
public:
	static XmppApi^ getInstance();
	~XmppApi();

	//Callback to handle XMPP events, including logging from the XMPP stack.
	void setEventCallback   ();
	void setEventCallback   ( XmppEventCallbackFunc callback );
	void eventCallback      ( const XmppEventData&  data );
	void addEventListener   ( XmppEventHandler^     eventDelegate );
	void removeEventListener( XmppEventHandler^     eventDelegate );

//	void setCallbackFunc	( XmppEventCallbackFunc value );			//We have default in native XMPP API, so do not really need this. 

	void onEvent			( XmppEventArgs^ e );		//XmppEvent handler


	//Main XMPP API methods
	void login( ManagedXmppDataTypes::XmppParameters^ paramsIn );
	void logoff();
	bool isConnected();

	void updateMyStatus ( ManagedXmppDataTypes::PresenceUpdate::ShowType show, String^ status );
	bool sendChat       ( String^ toJid, String^ body, String^ msgId );
	bool sendChatState  ( String^ toJid, bool composing );
	bool sendReadReceipt( String^ toJid, String^ msgId );
//	void requestVCard   ( String^ jid );

	//May not need these.
//	void setAppEventHandler( AppEventHandler* value );
//	AppEventHandler*	getAppEventHandler();				//Used for Unit Tests

private:
	XmppApi();

private:
	VoxoxXmppApi*			mNativeApi;
//	MyAppEventHandler*		mAppEventHandler;

	XmppEventDelegate^		mEventDelegate;
    event XmppEventHandler^	mEventHandler;

	static XmppApi^ mInstance = gcnew XmppApi();

};	//class XmppApi


}	//namespace ManagedXmppApi 

}	//namespace Voxox

}	//namespace Telcentris
